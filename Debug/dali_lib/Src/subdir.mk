################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../dali_lib/Src/dalistack.c \
../dali_lib/Src/dev_lightsensor.c \
../dali_lib/Src/dev_luminaire.c \
../dali_lib/Src/dev_occupancy.c \
../dali_lib/Src/dev_pushbutton.c \
../dali_lib/Src/device.c \
../dali_lib/Src/frame.c \
../dali_lib/Src/gear.c \
../dali_lib/Src/gear_colour.c \
../dali_lib/Src/gear_diag_maintenance.c \
../dali_lib/Src/gear_energy_report.c \
../dali_lib/Src/gear_led.c \
../dali_lib/Src/gear_mem_bank1_ext.c \
../dali_lib/Src/gear_power_supply.c \
../dali_lib/Src/gear_switching.c \
../dali_lib/Src/gw.c 

OBJS += \
./dali_lib/Src/dalistack.o \
./dali_lib/Src/dev_lightsensor.o \
./dali_lib/Src/dev_luminaire.o \
./dali_lib/Src/dev_occupancy.o \
./dali_lib/Src/dev_pushbutton.o \
./dali_lib/Src/device.o \
./dali_lib/Src/frame.o \
./dali_lib/Src/gear.o \
./dali_lib/Src/gear_colour.o \
./dali_lib/Src/gear_diag_maintenance.o \
./dali_lib/Src/gear_energy_report.o \
./dali_lib/Src/gear_led.o \
./dali_lib/Src/gear_mem_bank1_ext.o \
./dali_lib/Src/gear_power_supply.o \
./dali_lib/Src/gear_switching.o \
./dali_lib/Src/gw.o 

C_DEPS += \
./dali_lib/Src/dalistack.d \
./dali_lib/Src/dev_lightsensor.d \
./dali_lib/Src/dev_luminaire.d \
./dali_lib/Src/dev_occupancy.d \
./dali_lib/Src/dev_pushbutton.d \
./dali_lib/Src/device.d \
./dali_lib/Src/frame.d \
./dali_lib/Src/gear.d \
./dali_lib/Src/gear_colour.d \
./dali_lib/Src/gear_diag_maintenance.d \
./dali_lib/Src/gear_energy_report.d \
./dali_lib/Src/gear_led.d \
./dali_lib/Src/gear_mem_bank1_ext.d \
./dali_lib/Src/gear_power_supply.d \
./dali_lib/Src/gear_switching.d \
./dali_lib/Src/gw.d 


# Each subdirectory must supply rules for building sources it contributes
dali_lib/Src/%.o: ../dali_lib/Src/%.c dali_lib/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G0B1xx -c -I../dali_lib/Inc -I../dali_lowlevel/Inc -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

