/*! \mainpage notitle
 *
 * \section intro_sec Introduction
 *
 * Communication between DALI Stack and DALI bus. The DALI Low-Level-Driver
 * handles the voltage levels on the DALI bus and builds up the frames for
 * forwarding to the DALI Stack Library. The frames are checked for timing
 * violations and collisions with other members on the DALI bus. Frames
 * received by the DALI Stack Library are converted into the corresponding
 * voltage levels and switched with the help of a timer interrupt. \n The
 * Low-Level Driver has been implemented with the focus of minimum requirements
 * to hardware. \n
 * \n
 * \ref release_page
 *
 * \section requirements_sec Requirements
 *
 * 1* 16-bit Timer with a resolution of 1us \n
 * 1* external Interrupt (falling/rising edge) \n
 * 1* GPIO (Output)
 *
 * \section architect_sec Architecture
 *
 * \image html dalill_fluss.png
 *
 *
 * \subsection api_subsec Description of API
 * <a href="dali__ll_8h.html">click</a> 
 * \n
 * \section operation_sec Operation
 * 
 * \subsection init_subsec Initialisation
 * - create a bus line by assinging the required callbacks (set Bus HIGH/LOW, get Bus state) \n
 * for details see ::dalill_createBusLine \n 
 * - assign a temporary struct ::dalill_base_t with callbacks for forwarding frames,
 * timing Helper (called every 10 ms), get timer counter value, get/set timer period and
 * starting the timer by calling ::dalill_createBase \n
  * \n
 * <a href="dali.txt">example for initialisation dali_init()</a>
 * 
 * \subsection comm_subsec Communication
 * - frame format ::dalill_frame_t
 * - receiving frames ::fLLdalilltoDalilib
 * - sending frames ::dalill_send
 *
 */
 
/*! \page release_page Release Notes
 *
 * \section history_sec Update History
 *
 * \subsection v090_subsec V0.9.0
 * First official release.
*/
