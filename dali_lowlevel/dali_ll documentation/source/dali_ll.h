/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack Low-Level-Header
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/


#ifndef DALI_LL_H_
#define DALI_LL_H_


#ifdef	__cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"


/*! IEC 62386-101 Frame-Length info\n
 * 8-bit backward frame, answers to forward frames\n
 * 16-bit forward frame, communication with control gear\n
 * 24-bit forward frame, communication with control devices\n
 * 20-bit, 32-bit, reserved forward frames
 *
 * set max expected frame length (in bit)
 */
#define DALI_LL_MAX_FRAME_LENGTH	32

/*! time (in us) to compensate delays of voltage converter
 *  switching Bus-level to HIGH */
#define DALI_LL_TO_HIGH_OFFSET 	19

/*! time (in us) to compensate delays of voltage converter
 *  switching Bus-level to LOW */
#define DALI_LL_TO_LOW_OFFSET  -56

/*! Configure voltage level to corresponding voltage shifter hardware
 *  for receiving Low (0V on DALI Bus) */
#define DALI_BUS_RX_LOW  0
/*! Configure voltage level to corresponding voltage shifter hardware
 *  for receiving HIGH (16V on DALI Bus) */
#define DALI_BUS_RX_HIGH 1

/*! DALI bus state machine states */
typedef enum {
	BUS_IDLE,
	BUS_RECEIVE_DATA,           /*!< Receiving frame */
	BUS_SEND_DATA,     			/*!< Sending frame */
	BUS_RECEIVE_START_BIT,		/*!< Receive Startbit of frame */
	BUS_RECOVER					/*!< Collision Recovery */
} dalill_rx_tx_state_t;

/*! Status information of DALI bus power */
typedef enum {
	DALI_BUSPOWER_ON            = 0,
	DALI_BUSPOWER_OFF           = 1,   /*!< short Buspower interruption for 45ms .. 500ms */
	DALI_BUSPOWER_SYSTEMFAILURE = 2    /*!< long  Buspower interruption for more than 500ms */
} dalill_buspower_status_t;

/*! Status information for an interface */
typedef struct {
    dalill_buspower_status_t    bus_power_status;
    uint8_t                     interface_open;   /*!< 1 if interface is open */
    uint8_t                     transmit_pending; /*!< 1 if a frame is waiting to be transmitted */
} dalill_status_t;

/*! Frame structure between stack and low-level-driver */
typedef struct {
	// timestamp of last received bit in frame in 100usec ticks
	// will be set from low-level-driver
	// resolution [100us]
	uint32_t			timestamp;
    union {
        uint8_t         frameflags;
        struct {
            uint8_t priority;      /*!< priority of frame, 0=backward frame, 1..5=forward frame */
            uint8_t sendtwice;     /*!< 1 for frames that should be send twice */
            uint8_t tx;            /*!< 1 for frames that were transmitted */
            uint8_t error;         /*!< 1 if received or transmitted frame has error */
            uint8_t type;          /*!< 0=data is DALI frame data, 1= data is status information */
            uint8_t reserved;
        };
    };
	uint8_t             datalength;
    union {
        uint32_t      	 data;                 /*!< DALI data, valid if   type = 0 */
        uint8_t			 data_byte[4];
        dalill_status_t  status;               /*!< DALI status, valid if type = 1 */
    };
} dalill_frame_t;

typedef struct {
	uint8_t	 last_bit;
	uint16_t last_time_rx;                     /*!< time stamp of last received bit */
	uint8_t	 length;
	uint8_t  data[DALI_LL_MAX_FRAME_LENGTH/8];
} dalill_rx_man_frame_t;

typedef struct {
	uint8_t 	sendtwice;
	uint16_t 	settling_time;
	uint8_t	 	last_bit;
	uint8_t  	current_bit_number;
	uint16_t 	last_time_tx;                               /*!< time stamp of last sent bit */
	uint8_t	 	length;
	uint8_t 	data[(((DALI_LL_MAX_FRAME_LENGTH+2)/8))+1];
} dalill_tx_man_frame_t;

typedef uint8_t (*fLLgetBusState)(void);   /*!< get state of GPIO for DALI RX */
typedef void (*fLLsetBusStateHigh)(void);  /*!< set state of GPIO for DALI TX to HIGH */
typedef void (*fLLsetBusStateLow)(void);   /*!< set state of GPIO for DALI TX to LOW */

typedef struct {
	fLLgetBusState		getBusState;
	fLLsetBusStateHigh	setBusStateHigh;
	fLLsetBusStateLow	setBusStateLow;
} dalill_callbacks_t;

/*! struct of DALI bus line
 *  handles bits, times, states, etc. of each bus line */
typedef struct {
	uint8_t					enabled;
	dalill_callbacks_t		cb;
	uint16_t				last_stop_condition_time;	/*!< time stamp of last stop condition */
	uint16_t				last_settling_time;
	dalill_rx_tx_state_t    bus_state_recv;
	dalill_rx_tx_state_t    bus_state_transmit;
	dalill_status_t 		status;
	dalill_rx_man_frame_t 	rx_man_frame;
	dalill_tx_man_frame_t  	tx_man_frame;
	void*                   context;
} dalill_bus_t;

/*! send dali frames to dalilib */
typedef void (*fLLdalilltoDalilib)(void * p_context, dalill_frame_t* p_frame);
/*! called every 10 ms */
typedef void (*fLLdalilltimingHelper)(uint16_t uPeriod);
/*! get current value of timer counter */
typedef uint16_t (*fLLgetCurrentTimerVal)(void);
/*! get current value of timer period */
typedef uint16_t (*fLLgetTimerPeriod)(void);
/*! start timer */
typedef void (*fLLstartTimer)(void);
/*! set timer period without stopping or restarting timer */
typedef void (*fLLsetTimerPeriod)(uint16_t period);

/*! DALI Low Level main struct
 *  for handling timing and receiving frames */
typedef struct {
	fLLdalilltimingHelper  dalilltimingHelper;
	fLLdalilltoDalilib	   dalilltoDalilib;
	fLLgetCurrentTimerVal  getCurrentTimerVal;
	fLLgetTimerPeriod	   getTimerPeriod;
	fLLstartTimer		   startTimer;
	fLLsetTimerPeriod 	   setTimerPeriod;
} dalill_base_t;

/*! calling this function will assign the callbacks to set DALI bus LOW/HIGH
 *  and read current DALI bus state */
dalill_bus_t* dalill_createBusLine(fLLgetBusState getBusState,
		 fLLsetBusStateHigh setBusStateHigh, fLLsetBusStateLow setBusStateLow);
/*! calling this function will assign the callbacks for handling the timer
 *  (interrupt) and forwarding a DALI frame */
void dalill_createBase(dalill_base_t* pDalill_base);
/*! call this function in your GPIO ISR for falling and rising edge */
void dalill_interruptExt(dalill_bus_t* pDalill_bus);
/*! call this function in your timer ISR */
void dalill_timerInterrupt(void);
/*! call this function for sending a DALI frame */
uint8_t dalill_send(void * p_context, dalill_frame_t* p_frame);


#ifdef	__cplusplus
}
#endif

#endif /* DALI_LL_H_ */
