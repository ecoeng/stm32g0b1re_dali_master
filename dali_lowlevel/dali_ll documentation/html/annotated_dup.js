var annotated_dup =
[
    [ "_dalill_frame_t", "struct__dalill__frame__t.html", "struct__dalill__frame__t" ],
    [ "_dalill_status_t", "struct__dalill__status__t.html", "struct__dalill__status__t" ],
    [ "dali_ll_rx_man_frame", "structdali__ll__rx__man__frame.html", "structdali__ll__rx__man__frame" ],
    [ "dali_ll_tx_man_frame", "structdali__ll__tx__man__frame.html", "structdali__ll__tx__man__frame" ],
    [ "dalill_base", "structdalill__base.html", "structdalill__base" ],
    [ "dalill_bus", "structdalill__bus.html", "structdalill__bus" ],
    [ "dalill_callbacks", "structdalill__callbacks.html", "structdalill__callbacks" ]
];