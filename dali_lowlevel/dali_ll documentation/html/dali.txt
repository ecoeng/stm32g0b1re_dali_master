/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack Main
 * Description:   Communication between Low-Level-Driver, Application
 * and Stack/Library
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#include "dali.h"

dalilib_action_t    action;
uint32_t  			dali_time_ticker;
uint8_t             bDaliStackReady;
dalilib_instance_t  pDaliStackInstance;
dalilib_cfg_t       ctrl_gear_config;
uint32_t            actualLevel;


/******************************************************************************/
/* will be called by the DALI library if it wants to send a DALI message
 * to the driver
 * result: 0: success
 ******************************************************************************/
static uint8_t dali_send_callback(void * p_context, dalilib_frame_t* p_frame)
{
    uint8_t result = 0;

    dalill_send(p_context, (dalill_frame_t*)p_frame);

    return result;
}

/******************************************************************************/
/* forward low level frames to dalilib
*******************************************************************************/
void dalill_toDalilib(void * p_context, dalill_frame_t* p_frame)
{
	dalilib_receive(p_context, (dalilib_frame_t*)p_frame);
}

/******************************************************************************/
/* invoked by the DALI stack to notify the application that is ready
 * to receive the actions
*******************************************************************************/
static void dali_ready_callback(void)
{
    bDaliStackReady = 1;
}

/******************************************************************************/
/* invoked by the DALI stack to notify the application that is ready to receive the actions
*******************************************************************************/
static void dali_log_callback(char *pLog, uint8_t nLen)
{

}

/******************************************************************************/
/*  will be called by the DALI stack to tell the application  shall load the persistent variables
 ******************************************************************************/
static uint8_t dali_loadmemory_callback(void* pMemory, uint8_t nSize)
{
    uint8_t err = 0; //SUCCESS
    switch(ctrl_gear_config.mode)
    {
        case STACK_MODE_CONTROL_GEAR:
            // TODO:
        	//err = flash_read(pMemory, nSize, 0);
            break;
        case STACK_MODE_CONTROL_DEVICE:
            // TODO:
        	//err = flash_read(pMemory, nSize, 1);
            break;
        default:
        	break;
    }
    return (err);
}

/******************************************************************************/
/* will be called by the DALI library to tell the application shall save the persistent variables
*******************************************************************************/
static uint8_t dali_savememory_callback(void* pMemory, uint8_t nSize)
{
    uint8_t err = 0; //SUCCESS
    switch(ctrl_gear_config.mode)
    {
       case STACK_MODE_CONTROL_GEAR:
           // TODO:
    	   //err = flash_write(pMemory, nSize, 0);
            break;
        case STACK_MODE_CONTROL_DEVICE:
            // TODO:
        	//err = flash_write(pMemory, nSize, 1);
            break;
        default:
            break;
    }
    return (err);
}

/******************************************************************************/
/* will be called by the DALI library to notify the application about 'something'
*******************************************************************************/
static uint8_t dali_gear_reaction_callback(dalilib_react_ctrl_gear_t * p_args)
{
    switch(p_args->reactionCode)
    {
        case DALILIB_REACT_INTERNAL_CTRL_GEAR_OFF:
            {
                // hardware shall be set to 0, e.g. turn the lamp off
                // set internal actualLevel
                actualLevel = 0;

                // refresh actual level of the Stack
                action.actionType = DALILIB_CTRL_GEAR_ACTION;
                action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
                action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL;
                action.gearAct.value.actionValue = 0;

                dalilib_action(pDaliStackInstance, &action);
            }
            break;

        case DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL:
            {
                // hardware shall be set to reaction value

                // refresh actual level of the Stack
                action.actionType = DALILIB_CTRL_GEAR_ACTION;
                action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
                action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL;
                actualLevel = action.gearAct.value.actionValue = p_args->reactValue;

                dalilib_action(pDaliStackInstance, &action);
            }
            break;

        case DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_ACTUAL_LEVEL:
            {
                action.actionType = DALILIB_CTRL_GEAR_ACTION;
                action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
                action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL;
                action.gearAct.value.actionValue = actualLevel;

                dalilib_action(pDaliStackInstance, &action);

            }
            break;

        default:
		 	break;
    }

    if (actualLevel) {
    	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
    } else {
    	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
    }

    return (0);

}

/******************************************************************************/
/* will be called by the DALI library to notify the application about 'something'
*******************************************************************************/
static uint8_t dali_reaction_callback(dalilib_action_t * p_args)
{
    uint8_t res = 0;
    switch(p_args->actionType)
    {
        case DALILIB_CTRL_GEAR_REACTION:
                res = dali_gear_reaction_callback(&p_args->gearReact);
            break;

        default:
                res = 0xFF; //unsupported reaction type
            break;
    }

    return (res);
}

/******************************************************************************/
/* DALI stack configuration struct will be initialized
*******************************************************************************/
static void dali_create_gear_config(void)
{
    memset(&ctrl_gear_config, 0, sizeof(ctrl_gear_config));

    // stack mode
    ctrl_gear_config.mode = STACK_MODE_CONTROL_GEAR;
    ctrl_gear_config.vendor.control_device_mode = CTRL_DEV_MODE_NONE;

    // vendor information
    ctrl_gear_config.vendor.control_gear_phm = 10;
    ctrl_gear_config.vendor.control_gear_device_type = GEAR_TYPE_LED_MODULES;
    ctrl_gear_config.vendor.gtin[0] = 'M';
    ctrl_gear_config.vendor.gtin[1] = 'B';
    ctrl_gear_config.vendor.gtin[2] = 'S';
    ctrl_gear_config.vendor.gtin[3] = '0';
    ctrl_gear_config.vendor.gtin[4] = '1';
    ctrl_gear_config.vendor.gtin[5] = '7';

    ctrl_gear_config.vendor.firmware_ver_major = 0x01;
    ctrl_gear_config.vendor.firmware_ver_minor = 0x02;

    ctrl_gear_config.vendor.identify_number[0] = 0x01;
    ctrl_gear_config.vendor.identify_number[1] = 0x02;
    ctrl_gear_config.vendor.identify_number[2] = 0x03;
    ctrl_gear_config.vendor.identify_number[3] = 0x04;
    ctrl_gear_config.vendor.identify_number[4] = 0x08;
    ctrl_gear_config.vendor.identify_number[5] = 0x0A;
    ctrl_gear_config.vendor.identify_number[6] = 0x12;
    ctrl_gear_config.vendor.identify_number[7] = 0x12;

    ctrl_gear_config.vendor.hw_ver_major      = 0x10;
    ctrl_gear_config.vendor.hw_ver_minor      = 0x33;

    // callbacks
    ctrl_gear_config.callbacks.fPAppReady = dali_ready_callback;
    ctrl_gear_config.callbacks.fPAppSend = dali_send_callback;
    ctrl_gear_config.callbacks.fPAppLoadMem = dali_loadmemory_callback;
    ctrl_gear_config.callbacks.fPAppSaveMem = dali_savememory_callback;
    ctrl_gear_config.callbacks.fPAppReAction = dali_reaction_callback;
    ctrl_gear_config.callbacks.fPAppLog = dali_log_callback;


    /* LED vendor parameters */
    ctrl_gear_config.vendor.led_params.min_fast_fade_time = 1;

    // no address, shall be set to 0xFF
    ctrl_gear_config.vendor.shortAddress = 0x01;
}

/******************************************************************************/
/* application function
 * Here only the example call of the action functions is displayed.
 * The application should decide when the action functions should be called
*******************************************************************************/
void app_action(void)
{
     // if a hardware error occurs, the application must notify the stack
     action.actionType = DALILIB_CTRL_GEAR_ACTION;
     action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
     action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_GEAR_FAILURE;
     action.gearAct.value.actionValue = 1; // 1: error, 0: no error

     dalilib_action(pDaliStackInstance, &action);

     // if a led error occurs, the application must notify the stack
     action.actionType = DALILIB_CTRL_GEAR_ACTION;
     action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
     action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAMPFAILURE;
     action.gearAct.value.actionValue = 1; // 1: error, 0: no error

     dalilib_action(pDaliStackInstance, &action);

     // if a led actual level changed, the application can notify the stack
     action.actionType = DALILIB_CTRL_GEAR_ACTION;
     action.gearAct.gearActionType = DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR;
     action.gearAct.action.internalAction = DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL;
     action.gearAct.value.actionValue = 85500; //  85,50 % = 85,50 *  1000

     dalilib_action(pDaliStackInstance, &action);
}

/******************************************************************************/
/* timer every 10-20ms
*******************************************************************************/
void dalill_timingHelper(uint16_t uPeriod)
{
     dali_time_ticker += uPeriod/100;

     // DALI stack time ticker must have the resolution 100us
     dalilib_timingHelper(pDaliStackInstance, dali_time_ticker);
}

/******************************************************************************/
/* initialize the HAL driver and the DALI stack
*******************************************************************************/
void  dali_init(void)
{
	pDaliStackInstance = NULL;
	bDaliStackReady = 0;
	dali_time_ticker = 0;
	actualLevel = 0;
	dalill_base_t dalill_base;

   // create new DALI stack instance
   pDaliStackInstance = dalilib_createinstance();
   if (NULL == pDaliStackInstance)
   {
        // error
        return;
   }

  // create DALI stack config
  dali_create_gear_config();

  // create bus instance
  pDalill_bus_0 = dalill_createBusLine(&dalill_getBusState, &dalill_setBusStateHigh, &dalill_setBusStateLow);

  // add Low Level struct to DALI stack instance and vice versa
  ctrl_gear_config.context = pDalill_bus_0;
  pDalill_bus_0->context = pDaliStackInstance;

  // initialize DALI stack instance
  if (R_DALILIB_OK != dalilib_init(pDaliStackInstance , &ctrl_gear_config))
  {
      // error
      return;
  }

  // start DALI stack instance
  if (R_DALILIB_OK != dalilib_start(pDaliStackInstance))
  {
      // error
      return;
  }

  // initilise callbacks for DALI Low Level Driver
  dalill_base.dalilltimingHelper = &dalill_timingHelper;
  dalill_base.dalilltoDalilib = &dalill_toDalilib;
  dalill_base.getCurrentTimerVal = &dalill_getCurrentTimerVal;
  dalill_base.getTimerPeriod = &dalill_getTimerPeriod;
  dalill_base.setTimerPeriod = &dalill_setTimerPeriod;
  dalill_base.startTimer = &dalill_startTimer;

  dalill_createBase(&dalill_base);
}
