var struct__dalill__frame__t =
[
    [ "data", "struct__dalill__frame__t.html#a1e43bf7d608e87228b625cca2c04d641", null ],
    [ "data_byte", "struct__dalill__frame__t.html#acc64493e53db82d84e94c7cf19dac878", null ],
    [ "datalength", "struct__dalill__frame__t.html#a3aefb7897763d51c386443f695affa0d", null ],
    [ "error", "struct__dalill__frame__t.html#adc64ccb7538429fe78e3fe0139267370", null ],
    [ "frameflags", "struct__dalill__frame__t.html#a6f9f8767222cfac0214a2978c58f5ef2", null ],
    [ "priority", "struct__dalill__frame__t.html#a0ad043071ccc7a261d79a759dc9c6f0c", null ],
    [ "reserved", "struct__dalill__frame__t.html#acb7bc06bed6f6408d719334fc41698c7", null ],
    [ "sendtwice", "struct__dalill__frame__t.html#ac1b77ddbb87d685eec2f21c83f68b153", null ],
    [ "status", "struct__dalill__frame__t.html#aff874d140547ee00b2763ad54c399f86", null ],
    [ "timestamp", "struct__dalill__frame__t.html#ab20b0c7772544cf5d318507f34231fbe", null ],
    [ "tx", "struct__dalill__frame__t.html#a1b12e4ee55043211cb10d21bb128b196", null ],
    [ "type", "struct__dalill__frame__t.html#a1d127017fb298b889f4ba24752d08b8e", null ]
];