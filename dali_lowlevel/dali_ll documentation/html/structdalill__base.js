var structdalill__base =
[
    [ "dalilltimingHelper", "structdalill__base.html#a9e0c954454b376fe6d29fa5740fb68fe", null ],
    [ "dalilltoDalilib", "structdalill__base.html#a0c8593172dcb86f5b50d348e27b6b86d", null ],
    [ "getCurrentTimerVal", "structdalill__base.html#a096e1104c07fcbaee4fb5ccf97cbaaab", null ],
    [ "getTimerPeriod", "structdalill__base.html#a159995527db15e39720e04cfc2ea17f5", null ],
    [ "setTimerPeriod", "structdalill__base.html#affd51e485bb21a20b5308b942c6f8a7a", null ],
    [ "startTimer", "structdalill__base.html#af3be9e0f032e0657ad332b5c8d6b9d7f", null ]
];