var structdalill__bus =
[
    [ "bus_state_recv", "structdalill__bus.html#a0d71c74c209d89b74e3f86fb6bf3141c", null ],
    [ "bus_state_transmit", "structdalill__bus.html#add282d22a39b94caae42c2729fa736bd", null ],
    [ "cb", "structdalill__bus.html#a1bbc00524e295e8edad557b50ceb7d39", null ],
    [ "enabled", "structdalill__bus.html#a071e5007ca753b83d24505a6b6daba5c", null ],
    [ "last_settling_time", "structdalill__bus.html#a17523f74ac311b1bfa6743cc29b7e3a8", null ],
    [ "last_stop_condition_time", "structdalill__bus.html#a51efd9c9b9645bfa31e332c5c5513d6d", null ],
    [ "rx_man_frame", "structdalill__bus.html#a07e453c2b9276554d10c7cda08ccf9bc", null ],
    [ "status", "structdalill__bus.html#aff874d140547ee00b2763ad54c399f86", null ],
    [ "tx_man_frame", "structdalill__bus.html#ae29d00e928bcf15952ceb5181297fac0", null ]
];