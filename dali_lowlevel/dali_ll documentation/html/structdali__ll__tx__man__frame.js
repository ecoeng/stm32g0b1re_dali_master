var structdali__ll__tx__man__frame =
[
    [ "current_bit_number", "structdali__ll__tx__man__frame.html#ab2a604fd86290f28896ea77931f1ce1b", null ],
    [ "data", "structdali__ll__tx__man__frame.html#a114aacd2ddd81645ce22b5231d324c6d", null ],
    [ "last_bit", "structdali__ll__tx__man__frame.html#aa43d012d37cc78f93e1757643e7ec30a", null ],
    [ "last_time_tx", "structdali__ll__tx__man__frame.html#a9987ea4c2482b4897ba95ce0547ec7f1", null ],
    [ "length", "structdali__ll__tx__man__frame.html#ab2b3adeb2a67e656ff030b56727fd0ac", null ],
    [ "sendtwice", "structdali__ll__tx__man__frame.html#ac1b77ddbb87d685eec2f21c83f68b153", null ],
    [ "settling_time", "structdali__ll__tx__man__frame.html#aea6c052ec7577d9eb0842010dc0b80e3", null ]
];