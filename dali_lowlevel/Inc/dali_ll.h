/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack Low-Level-Header
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/


#ifndef DALI_LL_H_
#define DALI_LL_H_


#ifdef  __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include "stdint.h"
#include "string.h"


#define DALI_LOWLEVEL_CODE_VERSION_MAJOR 1
#define DALI_LOWLEVEL_CODE_VERSION_MINOR 8

#define MAXINSTANCES 3
#define MAXQUEUE    10

/********************************************************************************************
 * Attention :
 * the macro FRAMES_64BIT controls the possible length of proprietary frames.
 * If it is not define the maximal frame lenght is 32 Bit else 64 bits.
 * The macro correspondents directly with the macro API_FRAMES_64BIT in the DALI-Lib.
 * Both must have the same state.
 *
 * CAUTION : DO NOT CHANGE THIS IF YOU DON'T HAVE THE LIBRARY SOURCES !!!
 *
 ********************************************************************************************/

//#define FRAMES_64BIT 1 // make longer frames possible 32 or 64 bit

#ifdef FRAMES_64BIT
#define MAXFRAMELENGTH 64
#else
#define MAXFRAMELENGTH 32
#endif


// #define DEBUG_BIT_TIMING 1

/*! DALI bus state machine states */
typedef enum
{
  BUS_IDLE,
  BUS_RECEIVE_DATA,           /*!< Receiving frame */
  BUS_SEND_DATA,              /*!< Sending frame */
  BUS_END_OF_SENDING,         /*!< Sending a frame is completed */
  BUS_RECEIVE_START_BIT,      /*!< Receive Startbit of frame */
  BUS_SEND_START_BIT,         /*!< Send Startbit of frame */
  BUS_RECOVER,                /*!< Collision Recovery */
  BUS_COLLISION_AVOIDANCE,    /*!< check that Bus is not used */
  BUS_OFF,                    /*!< Buspower down >45ms */
  BUS_DELAY_BUS_POWER_ON,     /*!< Prevent spikes as PowerOn if Bus is open */
  BUS_SEND_BREAK,             /*!< Send break 1.3ms after collision detect */
  BUS_COLLISION_IN_READ,      /*!< collision detected while reading */
  BUS_TOGGLE_LAST_EDGE        /*!< if bus got to IDLE after last bit sent */
} dalill_state_t;

/*! BUS power status \n
 *  Status information of DALI bus power */
typedef enum
{
  DALI_BUSPOWER_ON            = 0,
  DALI_BUSPOWER_OFF           = 1,   /*!< short Buspower interruption for 45ms .. 500ms */
  DALI_BUSPOWER_SYSTEMFAILURE = 2    /*!< long  Buspower interruption for more than 500ms */
}bus_power_status_t;

/*! Driver busy status\n
 *  Status tells wether driver is free to start a new sent.
 *  maybe he is receiving or not ready sending the previous frame */

typedef enum
{
  DRIVER_IDLE      = 0,
  DRIVER_SENDING   = 1,
  DRIVER_RECEIVING = 2,
}driver_status_t;


/*! Status information for an interface */
typedef struct
{
  bus_power_status_t bus_power_status; /*!< BUS power status */
  uint8_t            interface_open;   /*!< 1 if interface is open */
  driver_status_t    driver_status;    /*!< Store if driver is busy  */
  uint8_t            collision_detect; /*!< 1 since last status found collision */
} dalill_status_t;

typedef struct
{
  uint16_t           value;            /*!< failed (Half-)Bit Timing */
  uint8_t            type;             /*!< 0 = timing violation, 1 = collision detected  */
  bus_power_status_t bus_power_status; /*!< BUS power status */
}dalill_error_t;

/*! Frame structure between stack and low-level-driver */
typedef struct
{
  // timestamp of last received bit in frame in 100usec ticks
  // will be set from low-level-driver
  // resolution [100us]
  uint32_t      timestamp;
  uint32_t      timestampStartOfRecv; // Store start of receiving for correct settling time computation
  union
  {
    uint8_t         frameflags;
    struct
    {
      uint8_t priority;      /*!< priority of frame, 0=backward frame, 1..5=forward frame */
      uint8_t sendtwice;     /*!< 1 for frames that should be send twice */
      uint8_t tx;            /*!< 1 for frames that were transmitted */
      uint8_t error;         /*!< 1 if received or transmitted frame has error */
      uint8_t type;          /*!< 0=data is DALI frame data, 1= data is status information */
      uint8_t reserved;
    };
  };
  uint8_t             datalength;
  // changing buffer for propriatary frames to 64 bit
  union
  {
    #ifndef FRAMES_64BIT
    uint32_t        data;          /*!< DALI data, valid if   type = 0 */
    uint8_t         data_byte[4];
    #else
    uint64_t        data;          /*!< DALI data, valid if   type = 0 */
    uint8_t         data_byte[8];
    #endif
    dalill_status_t status;       /*!< DALI status, valid if type = 1 */
    dalill_error_t  error_data;   /*!< DALI error data, valid if error = 1 and type = 1 (status) */
  };
} dalill_frame_t;

typedef struct
{
  uint8_t   last_bit;
  uint32_t  last_time_rx; /*!< time stamp of last received bit */
  uint8_t   length;
  uint8_t   pData[((MAXFRAMELENGTH + 8) / 8) * sizeof(uint8_t)]; // One more for frames on the limit
} dalill_rx_man_frame_t;

typedef struct
{
  uint8_t  sendtwice;
  uint32_t settling_time;
  uint8_t  last_bit;
  uint8_t  current_bit_number;
  uint32_t last_time_tx;        /*!< time stamp of last sent bit */
  uint8_t  length;
  uint8_t  pData[(((2*MAXFRAMELENGTH) + 8)/8) * sizeof(uint8_t)];
} dalill_tx_man_frame_t;

typedef uint8_t (*fLLgetBusState)(void);   /*!< get state of GPIO for DALI RX,
                                               logic level must be equal to bus level */
typedef void (*fLLsetBusStateHigh)(void);  /*!< set state of GPIO for DALI TX to HIGH */
typedef void (*fLLsetBusStateLow)(void);   /*!< set state of GPIO for DALI TX to LOW */

typedef struct
{
  fLLgetBusState     getBusState;
  fLLsetBusStateHigh setBusStateHigh;
  fLLsetBusStateLow  setBusStateLow;
} dalill_callbacks_t;


#ifdef DEBUG_BIT_TIMING
typedef struct
{
  uint32_t minBitTime;
  uint32_t maxBitTime;
  uint8_t  currentBit;
} timingstats_t;
#endif

/*! struct of DALI bus line
 *  handles bits, times, states, etc. of each bus line */
typedef struct
{
  uint8_t               enabled;
  dalill_callbacks_t    cb;
  uint16_t              last_timer_period;   /*!< last timer fire value */
  uint32_t              last_settling_time;
  dalill_state_t        bus_state;
  dalill_status_t       status;
  uint8_t               bufferOverflow;      /*!< driver detects a bufferoverflow */
  uint8_t               haveCollision;       /*!< driver detects a collison       */
  uint8_t               firstCollCall;       /*!< if first collision appears init rnd */
  uint8_t               isBackward;          /*!< this is a backward frame        */
  uint8_t               doNotOverCompensate; /*!< Do not overcompensate switching times */
  dalill_rx_man_frame_t rx_man_frame;
  dalill_tx_man_frame_t tx_man_frame;
  uint32_t              tick_cnt;            // TimerTickCnt for this instance
  uint32_t              tim_period;          //
  uint32_t              dalill_us_time;
  uint32_t              startOfRecv;         // Store start of receiving a frame
  uint8_t               commingFromInt;      // freshly got levelchangeinterrupt
  uint32_t              old_cnt_val;         // for timediff computation
  void*                 context;

  // variables for multidevice

  void                 *pInstances[MAXINSTANCES];
  void                 *pThisInstance;
  uint16_t             anzInstances;
  uint16_t             instancePoint;
  dalill_frame_t       lastFrame;

  // debug bit timings
  #ifdef DEBUG_BIT_TIMING
  timingstats_t timingStats;
  #endif
} dalill_bus_t;


typedef struct
{
  void * p_context_stackinst;
  dalill_frame_t rev_frame;
} dalill_queue_receive_t;

typedef struct
{
  void * p_send_context_stackinst;
  dalill_frame_t send_frame;
  void * api_instance; // dalilib_instance_t !
} dalill_queue_send_t;


/*! Prototypes for queueing */

uint8_t dalill_pushSendQueue(void * p_context, dalill_frame_t* p_frame,void *api_instance);
void dalill_pushRecvQueue(void * p_context, dalill_frame_t* p_frame);
void dalill_processQueues();
uint8_t dalill_isBusy();

/*! Prototypes for Multidevice */

int16_t addInstance(dalill_bus_t* pDalill_bus,void* pInstance);
int16_t otherInstances(dalill_bus_t* pDalill_bus,void *pInstance);
void   *giveNextInstance(dalill_bus_t* pDalill_bus);

/*! send dali frames to dalilib */
typedef void (*fLLdalilltoDalilib)(void * p_context, dalill_frame_t* p_frame);
/*! called every 10 ms */
typedef void (*fLLdalilltimingHelper)(void * p_context,uint32_t uPeriod);
/*! get current value of timer counter */
typedef uint32_t (*fLLgetCurrentTimerVal)(dalill_bus_t* pDalill_bus);
/*! get current value of timer period */
typedef uint32_t (*fLLgetTimerPeriod)(dalill_bus_t* pDalill_bus);
/*! start timer */
typedef void (*fLLstartTimer)(void);
/*! set timer period without stopping or restarting timer */
typedef void (*fLLsetTimerPeriod)(uint16_t period,dalill_bus_t* pDalill_bus);
/*! signal timer has data to a thread in OS-environment */
typedef void (*fLLDriverSignal)(void);
/*! enable/disable IRQ-Callback type */
typedef void (*fLLIRQ) (void);

/*! DALI Low Level main struct
 *  for handling timing and receiving frames */
typedef struct
{
  /*! 1 = debug mode active
   * status frame is sent for timing violations, collisions and bus failures */
  uint8_t debug_mode;

  /*! IEC 62386-101 Frame-Length info\n
   * 8-bit backward frame, answers to forward frames\n
   * 16-bit forward frame, communication with control gear\n
   * 24-bit forward frame, communication with control devices\n
   * 20-bit, 32-bit (default), reserved forward frames
   *
   * set max expected frame length (in bit)
   */
  uint8_t max_frame_length;

  /*! time (in us) to compensate delays of level shifter (receiver)
   *  switching Bus-level to HIGH for the receive process */
  int16_t rx_rcv_high_offset;

  /*! time (in us) to compensate delays of level shifter (receiver)
   *  switching Bus-level to LOW for the receive process */
  int16_t rx_rcv_low_offset;

  /*! time (in us) to compensate delays of level shifter (receiver)
   *  switching Bus-level to HIGH for the send process*/
  int16_t rx_high_offset;

  /*! time (in us) to compensate delays of level shifter (receiver)
   *  switching Bus-level to LOW for the send process*/
  int16_t rx_low_offset;

  /*! time (in us) to compensate delays of level shifter (transmitter)
   *  switching Bus-level to HIGH */
  int16_t tx_high_offset;

  /*! time (in us) to compensate delays of level shifter (transmitter)
   *  switching Bus-level to LOW */
  int16_t tx_low_offset;

  fLLdalilltimingHelper dalilltimingHelper;
  fLLdalilltoDalilib    dalilltoDalilib;
  fLLgetCurrentTimerVal getCurrentTimerVal;
  fLLgetTimerPeriod     getTimerPeriod;
  fLLstartTimer         startTimer;
  fLLsetTimerPeriod     setTimerPeriod;
  fLLDriverSignal       signalToThread;
  fLLIRQ                enableIRQ;
  fLLIRQ                disableIRQ;
} dalill_base_t;

/*! calling this function will assign the callbacks to set DALI bus LOW/HIGH
 *  and read current DALI bus state */
dalill_bus_t* dalill_createBusLine(fLLgetBusState getBusState,
     fLLsetBusStateHigh setBusStateHigh, fLLsetBusStateLow setBusStateLow);
/*! calling this function will assign the callbacks for handling the timer
 *  (interrupt) and forwarding a DALI frame */
void dalill_createBase(dalill_base_t* pDalill_base);
/*! call this function in your GPIO ISR for falling and rising edge */
void dalill_interruptExt(dalill_bus_t* pDalill_bus);
/*! call this function in your timer ISR */
void dalill_timerInterrupt2(dalill_bus_t* pDalill_bus);
/*! call this function for sending a DALI frame */
uint8_t dalill_send(void * p_context, dalill_frame_t* p_frame,void *who);
uint8_t dalill_send_hw(void * p_context, dalill_frame_t* p_frame);
void dalill_setAndStoreTimerPeriod(uint16_t uPeriod,dalill_bus_t* pDalill_bus);

#ifdef DEBUG_BIT_TIMING
void giveBitTimings(timingstats_t *stats,dalill_bus_t* pDalill_bus);
#endif

#ifdef  __cplusplus
}
#endif

#endif /* DALI_LL_H_ */
