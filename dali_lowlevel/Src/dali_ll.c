/*
 * dali_ll.c
 *
 *      Author: mbs
 */

#include "dali_ll.h"

#define MAX_BUS_LINES 2 /*!< currently only one supported */

#define DALI_LL_TIMER_RESOLUTION   1   // in us

#define DALI_BUS_RX_LOW  0
#define DALI_BUS_RX_HIGH 1

// Timings
#define DALI_LL_HALFBIT_TIME    417   // in us
#define DALI_LL_BIT_TIME        833   // in us

#define DALI_LL_TIME_BUS_POWER_DOWN      45000  // in us
#define DALI_LL_TIME_BUS_SYSTEMFAILURE   500000 // in us
#define DALI_LL_TIME_STOP_CONDITION      2400   // in us
#define DALI_LL_TIME_STOP_CONDITION_READ 1600   // in us
#define BUS_DELAY_BUS_POWER_ON_TIME      1000   // in us

#define DALI_LL_TIME_BREAK        1300  // in us 1200-1400 us
#define DALI_LL_TIME_RECOVER      4200  // in us 4000-4600 us
#define DALI_LL_RAND_RANGE        40    // random range for recover (* 10)

#define DALI_LL_TIME_STATUS_FRAME       10000 // in us
#define DALI_LL_TIME_SETTLING_INTERVAL  200   // in us

#define DALI_LL_HALFBIT_TIME_MIN 0
#define DALI_LL_HALFBIT_TIME_MAX 1
#define DALI_LL_BIT_TIME_MIN     2
#define DALI_LL_BIT_TIME_MAX     3

#define BY_TIMER_INTERRUPT       1
#define BY_EXT_INTERRUPT         0

#define MAX_COLLISION            0 // vorher 8 !!!
#define BACKWARD_FRAME_LEN       8

#define MIN_HALFBIT_LEN          2

#define SetBit(A,k)     ( A[(k/8)] |= (1 << (k%8)) )
#define CheckBit(A,k)    ( A[(k/8)] & (1 << (k%8)) )


/** DALI frame priority. */

static const uint16_t dalill_frame_priority[] = {
  5500,  //DALI_FRAME_PRIORITY_BACKWARD < settling time  5.5 - 10.5 msec
  13500, //DALI_FRAME_PRIORITY_1 < settling time 13.5 - 14.7 msec
  14900, //DALI_FRAME_PRIORITY_2 < settling time 14.9 - 16.1 msec
  16300, //DALI_FRAME_PRIORITY_3 < settling time 16.3 - 17.7 msec
  17900, //DALI_FRAME_PRIORITY_4 < settling time 17.9 - 19.3 msec
  19500  //DALI_FRAME_PRIORITY_5 < settling time 19.5 - 21.1 msec
};

static const uint16_t dalill_receiving_times[] = {
  320,   // 333,   // HALFBIT_TIME_MIN in us (<333,3 grey area)
  530,   // 500,   // HALFBIT_TIME_MAX in us (<666,7 grey area)
  657,   // (660) 667,   // BIT_TIME_MIN in us (>500   grey area)
  1030   // 1000,  // BIT_TIME_MAX in us (<1200  grey area)
};

static const uint16_t dalill_sending_times[] = {
  400,  // 360 HALFBIT_TIME_MIN in us (<356,7 grey area) 400
  470,  // 470,  // HALFBIT_TIME_MAX in us (<476,7 grey area) 433
  780,  // 720,  // BIT_TIME_MIN in us (>723,3   grey area) 800
  900   // 900,  // BIT_TIME_MAX in us (<943,3  grey area) 866
};

// #define DEBUG_OUT 1

#ifdef DEBUG_OUT
extern void SendString(char *str);
#include <stdio.h>
char oBuf[80]; // Only debug
#endif

/*! struct for every instance*/
dalill_bus_t dalill_bus_lines_g[MAX_BUS_LINES] = {0};

/*! main struct f */
dalill_base_t dalill_base;

/*! Queue in and out data, using no structs to be a littel bit faster */

static uint8_t frameForDalilib  = 0;
static uint8_t recvWritePointer = 0;
static uint8_t recvReadPointer  = 0;
static dalill_queue_receive_t receiveQ[MAXQUEUE];

static uint8_t frameToSend      = 0;

#ifdef DEBUG_BIT_TIMING
void giveBitTimings(timingstats_t *stats,dalill_bus_t* pDalill_bus)
{
  stats->currentBit = pDalill_bus->timingStats.currentBit;
  stats->maxBitTime = pDalill_bus->timingStats.maxBitTime;
  pDalill_bus->timingStats.maxBitTime = 0;
  stats->minBitTime = pDalill_bus->timingStats.minBitTime;
  pDalill_bus->timingStats.minBitTime = 0xFFFFFFFF;
}
#endif


static void dalill_sendToLibTxComplete(dalill_bus_t* pDalill_bus,uint8_t error);

//******************************************************
// Routines for queueing in/out from/to lowleveldriver
//******************************************************

/***********************************************************************************************************************
 * @brief  pushes data packet into sendinding queue
 *
 * @param[in]  p_context    - lowlevelintance to send
 * @param[in]  p_frame      - frame to send (to queue)
 * @param[in]  api_instance - api-instance which wants to send the frame
 *
 * @return     0-success, fail otherwise
 **********************************************************************************************************************/

uint8_t dalill_pushSendQueue(void * p_context, dalill_frame_t* p_frame,void *api_instance)
{
  return dalill_send(p_context,p_frame,api_instance);
}

/***********************************************************************************************************************
 * @brief  pushes data packet into the receive queue
 *
 * @param[in]  p_context - api instance which should receive the frame
 * @param[in]  p_frame   - frame to send up to the dali-api
 *
 **********************************************************************************************************************/

volatile uint32_t prq = 0;
volatile uint32_t qov = 0;

void dalill_pushRecvQueue(void * p_context, dalill_frame_t* p_frame)
{
  // Disable interrupts only for variables that can change during interrupt -> only UP-Queue
   
  if (frameForDalilib != MAXQUEUE)
  {
  	prq++;
    memcpy(&receiveQ[recvWritePointer].rev_frame, p_frame, sizeof(dalill_frame_t));
    receiveQ[recvWritePointer].p_context_stackinst = p_context;
    recvWritePointer++;
    if (recvWritePointer == MAXQUEUE)
    {
      recvWritePointer = 0;
    }
    frameForDalilib++;
    if (dalill_base.signalToThread != NULL)
    {
      dalill_base.signalToThread();
    }
  }
  else
  {
	  qov++;
  //  SendString("RCVQueue OVERFLOW\n");
  //   // QUEUEOVERFLOW !! Should not happen !!!
  }

  return;
}

/***********************************************************************************************************************
 * @brief  processing in and out queues and sending data to desired endpoints (ll-driver or api)
 *
 *
 **********************************************************************************************************************/

void dalill_processQueues()
{
  while (frameForDalilib && (DRIVER_IDLE == dalill_bus_lines_g[0].status.driver_status) )
  {
    if(receiveQ[recvReadPointer].rev_frame.type)
    {
      if (receiveQ[recvReadPointer].p_context_stackinst != NULL)
      {
        dalill_base.dalilltoDalilib(receiveQ[recvReadPointer].p_context_stackinst,&receiveQ[recvReadPointer].rev_frame);
      }
    }
    else
    {
      if (receiveQ[recvReadPointer].p_context_stackinst != NULL)
      {
        dalill_base.dalilltoDalilib(receiveQ[recvReadPointer].p_context_stackinst,&receiveQ[recvReadPointer].rev_frame);
      }
    }

    if (receiveQ[recvReadPointer].p_context_stackinst != NULL)
    {
      dalill_base.dalilltimingHelper(receiveQ[recvReadPointer].p_context_stackinst, receiveQ[recvReadPointer].rev_frame.timestamp);
    }

    recvReadPointer++;
    if (recvReadPointer == MAXQUEUE)
    {
      recvReadPointer = 0;
    }
    
    // Disable interrupts only for variables that can change during interrupt -> only UP-Queue
    
    if (dalill_base.disableIRQ != NULL)
    {
      dalill_base.disableIRQ();
    }
    frameForDalilib--;
    if (dalill_base.enableIRQ != NULL)
    {
      dalill_base.enableIRQ();
    }
  }
}

/***********************************************************************************************************************
 * @brief  test wether there is data in the queues
 *
 * @return 0 idle no data - != 0 otherwise
 *
 **********************************************************************************************************************/

uint8_t dalill_isBusy()
{
  return (frameToSend != 0) || (frameForDalilib != 0);
}

//******************************************************
// Routines for multidevce (one Bus->multiple devices)
//******************************************************

/***********************************************************************************************************************
 * @brief  adding a newinstance of the dali(highleve)-api
 *
 * @param[in]  p_dalill_bus - the desired low-level-instance for this api-instance
 * @param[in]  pInstance     - api-instance to add
 *
 **********************************************************************************************************************/

int16_t addInstance(dalill_bus_t* pDalill_bus,void* pInstance)
{
  uint16_t retVal;

  if (pDalill_bus->anzInstances >= MAXINSTANCES)
  {
    retVal = -1;
  }
  else
  {
    pDalill_bus->pInstances[pDalill_bus->anzInstances] = pInstance;
    pDalill_bus->anzInstances++;
    pDalill_bus->instancePoint = 0;
    retVal = 0;
  }

  return retVal;
}

/***********************************************************************************************************************
 * @brief  look wether there are any other api-instances on top of this lowleveldriver
 *
 * @param[in]  p_dalill_bus - the desired low-level-instance for this api-instance
 * @param[in]  pInstance     - asking instance (stored in thisInstance)
 *
 **********************************************************************************************************************/

int16_t otherInstances(dalill_bus_t* pDalill_bus,void *pInstance)
{
  uint16_t retVal;

  if (pDalill_bus->anzInstances)
  {
    pDalill_bus->pThisInstance = pInstance;
    pDalill_bus->instancePoint = 0;
    retVal = pDalill_bus->anzInstances - 1;
  }
  else
  {
    retVal = -1;
  }
  return retVal;
}

/***********************************************************************************************************************
 * @brief  returning all other instances (one after the other) different to previously stored "thisInstance"
 *
 * @param[in]  p_dalill_bus - the desired low-level-instance for this api-instance
 *
 **********************************************************************************************************************/

void *giveNextInstance(dalill_bus_t* pDalill_bus)
{
  void *retVal;

  if (pDalill_bus->anzInstances < 2)
  {
    retVal = (void*)-1;
  }
  else
  {
    if (pDalill_bus->pInstances[pDalill_bus->instancePoint] == pDalill_bus->pThisInstance)
    {
      pDalill_bus->instancePoint++;
    }
    if (pDalill_bus->instancePoint >= pDalill_bus->anzInstances)
    {
       retVal = (void *)-1;
    }
    else
    {
       retVal = pDalill_bus->pInstances[pDalill_bus->instancePoint];
       pDalill_bus->instancePoint++;
    }
  }
  return retVal;
}

//-------------------------------------------------------------------------------------

void dalill_setAndStoreTimerPeriod(uint16_t uPeriod,dalill_bus_t* pDalill_bus)
{
  pDalill_bus->last_timer_period = uPeriod;
  dalill_base.setTimerPeriod(uPeriod,pDalill_bus);
}



static inline uint32_t dalill_timeDiff(uint32_t pLast_time, uint32_t pCurrent_time)
{
  return (pCurrent_time - pLast_time);
}

uint32_t havNeg = 0;
int32_t negVal  = 0;

static void dalill_usTimeRefresh(uint8_t bTimer_interrupt,dalill_bus_t* pDalill_bus)
{
  if(bTimer_interrupt)
  {
    pDalill_bus->dalill_us_time += pDalill_bus->last_timer_period;
    pDalill_bus->old_cnt_val = 0;
  }
  else
  {
    uint32_t new_cnt_val = dalill_base.getCurrentTimerVal(pDalill_bus);
    pDalill_bus->dalill_us_time += (new_cnt_val - pDalill_bus->old_cnt_val);
    pDalill_bus->old_cnt_val = new_cnt_val;

    if ( ((int32_t) new_cnt_val - (int32_t) pDalill_bus->old_cnt_val) < 0)
    {
      negVal = ((int32_t) new_cnt_val - (int32_t) pDalill_bus->old_cnt_val);
      havNeg++;
    }
  }
}


static void dalill_reverseBytes(unsigned char *start, int size)
{
  unsigned char *lo = start;
  unsigned char *hi = start + size - 1;
  unsigned char swap;

  while (lo < hi)
  {
    swap = *lo;
    *lo++ = *hi;
    *hi-- = swap;
  }
}

static void dalill_encodeManchester(dalill_bus_t* pDalill_bus, uint8_t uFrame_length, uint8_t* pFrame)
{
  memset(pDalill_bus->tx_man_frame.pData, 0, (((2*dalill_base.max_frame_length) + 8) / 8)* sizeof(uint8_t));

  // add start-bit
  pDalill_bus->tx_man_frame.length = 1;
  SetBit(pDalill_bus->tx_man_frame.pData, pDalill_bus->tx_man_frame.length);
  pDalill_bus->tx_man_frame.length++;

  while (uFrame_length--)
  {
    if (CheckBit(pFrame, uFrame_length))
    {
      pDalill_bus->tx_man_frame.length++;
      SetBit(pDalill_bus->tx_man_frame.pData, pDalill_bus->tx_man_frame.length);
    }
    else
    {
      SetBit(pDalill_bus->tx_man_frame.pData, pDalill_bus->tx_man_frame.length);
      pDalill_bus->tx_man_frame.length++;
    }
    pDalill_bus->tx_man_frame.length++;
  }
}

dalill_bus_t* dalill_createBusLine(fLLgetBusState getBusState, fLLsetBusStateHigh setBusStateHigh, fLLsetBusStateLow setBusStateLow)
{
  dalill_bus_t* pDalill_bus = NULL;
  uint8_t idx;

  for (idx=0; idx < MAX_BUS_LINES; idx++)
  {
    if ( 0 == dalill_bus_lines_g[idx].enabled)
    {
      pDalill_bus = &dalill_bus_lines_g[idx];
      memset(pDalill_bus, 0, sizeof(dalill_bus_t));
      pDalill_bus->enabled = 1;
      pDalill_bus->status.bus_power_status = DALI_BUSPOWER_ON;
      pDalill_bus->bus_state = BUS_RECEIVE_START_BIT;
      pDalill_bus->bufferOverflow     = 0;
      pDalill_bus->haveCollision      = 0;
      pDalill_bus->firstCollCall      = 1;
      pDalill_bus->last_settling_time = 0; // UKL
      pDalill_bus->status.driver_status = DRIVER_IDLE;
      pDalill_bus->doNotOverCompensate = 0;
      pDalill_bus->tick_cnt           = 0;
      pDalill_bus->tim_period         = 10000;
      pDalill_bus->dalill_us_time     = 0;
      pDalill_bus->cb.getBusState     = getBusState;
      pDalill_bus->cb.setBusStateHigh = setBusStateHigh;
      pDalill_bus->cb.setBusStateLow  = setBusStateLow;
      pDalill_bus->cb.setBusStateHigh();
      pDalill_bus->anzInstances = 0;
      pDalill_bus->commingFromInt = 0;
      pDalill_bus->old_cnt_val = 0;
      #ifdef DEBUG_BIT_TIMING
      pDalill_bus->timingStats.currentBit = 0;
      pDalill_bus->timingStats.maxBitTime = 0;
      pDalill_bus->timingStats.minBitTime = 0xFFFFFFFF;
      #endif
      break;
    }
  }
  return pDalill_bus;
}


static void dalill_sendStatusFrame(dalill_bus_t* pDalill_bus)
{
  dalill_frame_t        dalill_frame;
  int16_t               instCnt;

  memset(&dalill_frame, 0, sizeof(dalill_frame_t));
  dalill_frame.timestamp = pDalill_bus->dalill_us_time/100;
  dalill_frame.error = 0;
  dalill_frame.type = 1;
  dalill_frame.status.interface_open = 1;
  dalill_frame.status.bus_power_status = pDalill_bus->status.bus_power_status;
  
  if (pDalill_bus->haveCollision)
  {
     dalill_frame.status.collision_detect = 1;
     pDalill_bus->haveCollision = 0;
  }
  else
  {
    dalill_frame.status.collision_detect = 0;
  }

  // send it to all instances

  for (instCnt = 0; instCnt < pDalill_bus->anzInstances ; instCnt++)
  {
    dalill_pushRecvQueue(pDalill_bus->pInstances[instCnt], &dalill_frame);
  }
}

static void dalill_sendDebugFrame(dalill_bus_t* pDalill_bus, uint8_t type, uint16_t value)
{
  dalill_frame_t        dalill_frame;
  uint16_t              instCnt;

  memset(&dalill_frame, 0, sizeof(dalill_frame_t));

  dalill_frame.timestamp = pDalill_bus->dalill_us_time/100;
  dalill_frame.type  = 1;
  dalill_frame.error = 1;
  dalill_frame.status.interface_open = 1;
  dalill_frame.error_data.type = type;
  dalill_frame.error_data.value = value;
  dalill_frame.error_data.bus_power_status = pDalill_bus->status.bus_power_status;

  // send to all other devices on this hardware

  for (instCnt = 0; instCnt < pDalill_bus->anzInstances ; instCnt++)
  {
    dalill_pushRecvQueue(pDalill_bus->pInstances[instCnt], &dalill_frame);
  }
}


static void dalill_collisionRecovery(dalill_bus_t* pDalill_bus)
{
  #ifdef ENABLE_RETRANS
  if (pDalill_bus->firstCollCall)
  {
    // do it only one time
    srand(pDalill_bus->dalill_us_time);
    pDalill_bus->firstCollCall = 0;
  }
  if (pDalill_bus->haveCollision < MAX_COLLISION) 
  {
    pDalill_bus->haveCollision++; // save and count collisions for sending it with status frame
    dalill_setAndStoreTimerPeriod(DALI_LL_TIME_BREAK + ((rand() % DALI_LL_RAND_RANGE) * 10),pDalill_bus);
    pDalill_bus->bus_state = BUS_RECOVER;
  }
  else
  #endif
  {
    pDalill_bus->bus_state = BUS_SEND_BREAK;
    pDalill_bus->last_settling_time      = 0;

    // Send statusframe early up, because no we signal "Frame not send"
    // which appears immedeately in the API

    pDalill_bus->haveCollision = 1;
    dalill_sendStatusFrame(pDalill_bus);
    dalill_sendToLibTxComplete(pDalill_bus,1);
  }
  //
  // 1300ms Break auf den Bus legen
  //
  pDalill_bus->cb.setBusStateLow();
  dalill_setAndStoreTimerPeriod(DALI_LL_TIME_BREAK - dalill_base.rx_rcv_high_offset,pDalill_bus);
  pDalill_bus->tx_man_frame.current_bit_number = 0;
}


/***********************************************************************************************************************
 * @brief  Will be called on external Interrupt
 *
 * @param[in]  pDalill_bus
 *
 **********************************************************************************************************************/
volatile uint32_t endOfSending = 0;
volatile uint32_t lastUtime = 0;
volatile uint32_t lastCurrentBit = 0;
volatile uint32_t bitNR = 0;
volatile uint32_t wasStartBit = 0;

void dalill_interruptExt(dalill_bus_t* pDalill_bus)
{
  uint8_t         uCurrent_bit;
  uint32_t        uTime_diff_last_bit = 0;
  uint16_t const *pTimings;
  uint16_t        timing;

  dalill_usTimeRefresh(BY_EXT_INTERRUPT,pDalill_bus);
  uCurrent_bit = pDalill_bus->cb.getBusState();
  uTime_diff_last_bit = dalill_timeDiff(pDalill_bus->rx_man_frame.last_time_rx, pDalill_bus->dalill_us_time);
  pDalill_bus->rx_man_frame.last_time_rx = pDalill_bus->dalill_us_time;

  if (pDalill_bus->bus_state == BUS_SEND_DATA)
  {
    if(uCurrent_bit)
    {
      uTime_diff_last_bit += dalill_base.rx_low_offset;
    }
    else
    {
      uTime_diff_last_bit -= dalill_base.rx_high_offset;
    }
  }
  else
  {
    if(uCurrent_bit)
    {
      uTime_diff_last_bit += dalill_base.rx_rcv_low_offset;
    }
    else
    {
      uTime_diff_last_bit -= dalill_base.rx_rcv_high_offset;
    }
  }

  // switch between allowed sending and receiving time ranges

  if (DRIVER_SENDING == pDalill_bus->status.driver_status)
  {
    pTimings = dalill_sending_times;
    pDalill_bus->commingFromInt = 0;
  }
  else
  {
    pDalill_bus->commingFromInt = 1;
    pTimings = dalill_receiving_times;
    if (uCurrent_bit) // see change 16/17 in dalill_timerInterrupt2
    {
      timing = DALI_LL_TIME_STOP_CONDITION_READ + pTimings[DALI_LL_HALFBIT_TIME_MIN];
    }
    else
    {
      timing = DALI_LL_TIME_STOP_CONDITION_READ;
    }
    dalill_setAndStoreTimerPeriod(timing,pDalill_bus);
  }

  switch (pDalill_bus->bus_state)
  {
    case BUS_SEND_DATA:
    case BUS_RECEIVE_DATA:

    #ifdef DEBUG_BIT_TIMING
    pDalill_bus->timingStats.currentBit = uCurrent_bit;
    if ((pDalill_bus->timingStats.maxBitTime < uTime_diff_last_bit) && (uTime_diff_last_bit < 2000))
    {
      pDalill_bus->timingStats.maxBitTime = uTime_diff_last_bit;
    }
    if (pDalill_bus->timingStats.minBitTime > uTime_diff_last_bit)
    {
      pDalill_bus->timingStats.minBitTime = uTime_diff_last_bit;
    }
    #endif

    // ignore Startbit if sending
    if ((pDalill_bus->bus_state != BUS_SEND_DATA) || (pDalill_bus->tx_man_frame.current_bit_number > 1))
    {
     
      // received half-bit

      if (pTimings[DALI_LL_HALFBIT_TIME_MIN] <= uTime_diff_last_bit
       && pTimings[DALI_LL_HALFBIT_TIME_MAX] >= uTime_diff_last_bit)
      {
        // prevent buffer overflow
        if ( (! pDalill_bus->bufferOverflow) && 
             ((dalill_base.max_frame_length * 2) < pDalill_bus->rx_man_frame.length) )
        {
          pDalill_bus->bufferOverflow = 1; 
        }
         
        pDalill_bus->rx_man_frame.last_bit = uCurrent_bit;

        // save 'half-bit' only if it is second half

        if(  (!(pDalill_bus->rx_man_frame.length & 0x01)) && (! pDalill_bus->bufferOverflow) )
        {
          pDalill_bus->rx_man_frame.pData[pDalill_bus->rx_man_frame.length/16] <<= 1;

          if (DALI_BUS_RX_LOW == pDalill_bus->rx_man_frame.last_bit)
          {
            pDalill_bus->rx_man_frame.pData[pDalill_bus->rx_man_frame.length/16] |= 0x01;
          }
        }
        pDalill_bus->rx_man_frame.length++;
        
        // received double half-bit
      }
      else
      {
        if ( (pTimings[DALI_LL_BIT_TIME_MIN] <= uTime_diff_last_bit) &&
             (pTimings[DALI_LL_BIT_TIME_MAX] >= uTime_diff_last_bit) &&
             ( !(pDalill_bus->rx_man_frame.length & 0x01) || (pDalill_bus->status.driver_status == DRIVER_SENDING) )
           )
        {
          // prevent buffer overflow
          if ( (! pDalill_bus->bufferOverflow) &&
               ((dalill_base.max_frame_length * 2) < pDalill_bus->rx_man_frame.length) )
          {
            pDalill_bus->bufferOverflow = 1; 
          }
           
          if( (1 < pDalill_bus->rx_man_frame.length) && (! pDalill_bus->bufferOverflow) )
          {
            pDalill_bus->rx_man_frame.pData[pDalill_bus->rx_man_frame.length/16] <<= 1;

            if (DALI_BUS_RX_LOW == pDalill_bus->rx_man_frame.last_bit)
            {
              pDalill_bus->rx_man_frame.pData[pDalill_bus->rx_man_frame.length/16] |= 0x01;
            }
          }
          pDalill_bus->rx_man_frame.length += 2;
          pDalill_bus->rx_man_frame.last_bit = uCurrent_bit;
        }
        else
        {
          // cancel receiving - timing violation!
          // but do not interrupt for backwardframe -> just signal 
          if (BUS_SEND_DATA == pDalill_bus->bus_state)
          {
            if (! pDalill_bus->isBackward)
            {
              memset(pDalill_bus->rx_man_frame.pData, 0, (dalill_base.max_frame_length/8) * sizeof(uint8_t));
              pDalill_bus->rx_man_frame.length = 0;

              if (dalill_base.debug_mode)
              {
                dalill_sendDebugFrame(pDalill_bus, 1, uTime_diff_last_bit);
              }
              // timing violation while sending => collision detected

              lastUtime = uTime_diff_last_bit;
              lastCurrentBit = uCurrent_bit;
              dalill_collisionRecovery(pDalill_bus);
            }
          }
          else
          {
            pDalill_bus->haveCollision = 1;

            lastUtime = uTime_diff_last_bit;
            lastCurrentBit = uCurrent_bit;
            bitNR = pDalill_bus->rx_man_frame.length;
            wasStartBit = 0;

            // pDalill_bus->status.driver_status = DRIVER_IDLE;
            // pDalill_bus->bus_state = BUS_IDLE;
            pDalill_bus->bus_state = BUS_COLLISION_IN_READ;
            pDalill_bus->rx_man_frame.length = 0;
          }

          if (dalill_base.debug_mode)
          {
            dalill_sendDebugFrame(pDalill_bus, 0, uTime_diff_last_bit);
          }
        }
      }
    }
    break;

    case BUS_RECEIVE_START_BIT:
      // +/- 11 to give it a bit more tolerance. For Probit 3.3.11 and 3.3.12 it is OK
      if (  ((pTimings[DALI_LL_HALFBIT_TIME_MIN] - 20u) <= uTime_diff_last_bit) &&
            ((pTimings[DALI_LL_HALFBIT_TIME_MAX] + 20u) >= uTime_diff_last_bit) )
      {
        pDalill_bus->rx_man_frame.last_bit = DALI_BUS_RX_LOW;
        memset(pDalill_bus->rx_man_frame.pData, 0, (dalill_base.max_frame_length/8) * sizeof(uint8_t));
        pDalill_bus->rx_man_frame.length = 0;
        pDalill_bus->bus_state = BUS_RECEIVE_DATA;
        pDalill_bus->status.driver_status = DRIVER_RECEIVING;   // do not sent while receiving
        pDalill_bus->startOfRecv = pDalill_bus->dalill_us_time; // store starting time
      }
      else
      {
        if (dalill_base.debug_mode)
        {
          dalill_sendDebugFrame(pDalill_bus, 1, uTime_diff_last_bit);
        }
        // cancel receiving - timing violation!

        lastUtime = uTime_diff_last_bit;
        lastCurrentBit = uCurrent_bit;
        bitNR = pDalill_bus->rx_man_frame.length;
        wasStartBit = 1;

        // this is normally a collision
        pDalill_bus->haveCollision = 1;
        // pDalill_bus->status.driver_status = DRIVER_IDLE;
        // pDalill_bus->bus_state = BUS_IDLE;
        pDalill_bus->bus_state = BUS_COLLISION_IN_READ;
        pDalill_bus->rx_man_frame.length = 0;
      }
    break;

    case BUS_COLLISION_AVOIDANCE:
    case BUS_SEND_START_BIT:
      //
      // only avoid collision if we have a forward frame
      // backward frames "need" the collison and sending
      // should not be interrupted
      //
      if (! pDalill_bus->isBackward) // UKL again build in
      {
        pDalill_bus->bus_state = BUS_RECEIVE_START_BIT;
        dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STOP_CONDITION_READ,pDalill_bus);
        pDalill_bus->tx_man_frame.current_bit_number = 0;

        dalill_sendToLibTxComplete(pDalill_bus,1);
        pDalill_bus->status.driver_status = DRIVER_RECEIVING; // do not sent while receiving
        pDalill_bus->last_settling_time = 0;
        pDalill_bus->commingFromInt = 1;
        // reset a possible last overflow from the last frame;

        pDalill_bus->bufferOverflow = 0;
      }
    break;

    case BUS_TOGGLE_LAST_EDGE:
      pDalill_bus->bus_state = BUS_END_OF_SENDING;
    break;

    case BUS_END_OF_SENDING:
      endOfSending++;
    break;

    case BUS_IDLE:
      pDalill_bus->status.driver_status = DRIVER_RECEIVING; // do not sent while receiving
      pDalill_bus->bus_state = BUS_RECEIVE_START_BIT;
      pDalill_bus->last_settling_time = 0;

      // reset a possible last overflow from the last frame;

      pDalill_bus->bufferOverflow = 0;
    break;

    case BUS_OFF:
      // pDalill_bus->bus_state = BUS_IDLE; Prevent Spikes ! 09.02.2021 ukl
      dalill_base.setTimerPeriod(BUS_DELAY_BUS_POWER_ON_TIME,pDalill_bus);
      pDalill_bus->bus_state = BUS_DELAY_BUS_POWER_ON;
      // pDalill_bus->status.bus_power_status = DALI_BUSPOWER_ON; // 09.02.2021 ukl
      pDalill_bus->status.driver_status    = DRIVER_IDLE;
    break;

    default:
    break;
  }
}

volatile uint32_t sendErr = 0;

static void dalill_sendToLibTxComplete(dalill_bus_t* pDalill_bus,uint8_t error)
{
  dalill_frame_t dalilib_frame;
  uint16_t       endCnt;
  uint16_t       instCnt;
  
  memset(&dalilib_frame, 0, sizeof(dalill_frame_t));

  dalilib_frame.timestamp = pDalill_bus->dalill_us_time/100;
  dalilib_frame.status.interface_open = 1;
  dalilib_frame.tx    = 1;
  dalilib_frame.error = error;
  dalill_pushRecvQueue(pDalill_bus->context, &dalilib_frame);
  pDalill_bus->last_settling_time = 0; // UKL
  if (! error)
  {
    pDalill_bus->status.driver_status = DRIVER_IDLE;
    endCnt = otherInstances(pDalill_bus,pDalill_bus->context);
    if (endCnt > 0)
    {
      pDalill_bus->lastFrame.timestamp = pDalill_bus->dalill_us_time / 100;
      for (instCnt = 0 ; instCnt < endCnt ; instCnt++)
      {
        dalill_pushRecvQueue(giveNextInstance(pDalill_bus),&pDalill_bus->lastFrame);
      }
    }
  }
  else
  {
    sendErr++;
  }
  pDalill_bus->tx_man_frame.current_bit_number = 0;
}

// function called by stack with dali frame

uint8_t dalill_send(void * p_context, dalill_frame_t* p_frame,void *who)
{
  dalill_bus_t* pDalill_bus = (dalill_bus_t*)p_context;
  uint8_t       result;
  
  pDalill_bus->context = who; // to address correct "send_ready"
  result = dalill_send_hw(p_context,p_frame);
  
  if (! result)
  {
    memcpy(&pDalill_bus->lastFrame,p_frame, sizeof(dalill_frame_t));
  }
  return 0; // result;
}

uint8_t dalill_send_hw(void * p_context, dalill_frame_t* p_frame)
{
  dalill_bus_t* pDalill_bus = (dalill_bus_t*)p_context;
  uint8_t result = 0;

  if (BACKWARD_FRAME_LEN == p_frame->datalength)
  {
    pDalill_bus->isBackward = 1;
  }
  else
  {
    pDalill_bus->isBackward = 0;
  }

  if ( ((DRIVER_IDLE != pDalill_bus->status.driver_status) || (pDalill_bus->haveCollision)) &&
       (! pDalill_bus->isBackward) )
  {
    result = 1;
    dalill_sendToLibTxComplete(pDalill_bus,1);
  }
  else
  {
    dalill_encodeManchester(pDalill_bus, p_frame->datalength, p_frame->data_byte);

    // convert priority 0-5 to the belonging times in us

    pDalill_bus->tx_man_frame.settling_time = dalill_frame_priority[p_frame->priority];
    pDalill_bus->tx_man_frame.sendtwice = p_frame->sendtwice;
    pDalill_bus->tx_man_frame.current_bit_number = 0;
    pDalill_bus->status.driver_status = DRIVER_SENDING;
    pDalill_bus->bus_state = BUS_COLLISION_AVOIDANCE;

    // correct the time if we were still longer in IDLE_STATE

    pDalill_bus->last_settling_time += dalill_base.getCurrentTimerVal(pDalill_bus);

    dalill_setAndStoreTimerPeriod(DALI_LL_TIME_SETTLING_INTERVAL,pDalill_bus);
  }
  return result;
}

static void dalill_sendToLib(dalill_bus_t* pDalill_bus)
{
  dalill_frame_t        dalilib_frame;
  uint16_t              instCnt;

  //
  // if we detect an bufferoverflow we silently discard the packet
  //
  
  if (! pDalill_bus->bufferOverflow)
  {
    // no bufferoverflow, frame is valid
     
    memset(&dalilib_frame, 0, sizeof(dalill_frame_t));

    dalilib_frame.timestamp = pDalill_bus->dalill_us_time/100;
    dalilib_frame.timestampStartOfRecv = pDalill_bus->startOfRecv / 100;
    dalilib_frame.status.interface_open = 1;
    dalilib_frame.datalength            = pDalill_bus->rx_man_frame.length/2;
    dalill_reverseBytes(pDalill_bus->rx_man_frame.pData, dalilib_frame.datalength/8);
    memcpy(dalilib_frame.data_byte, pDalill_bus->rx_man_frame.pData, dalilib_frame.datalength/8);
    pDalill_bus->rx_man_frame.length      = 0;

    if (pDalill_bus->haveCollision)
    {
      dalilib_frame.error = 1;
    }
    else
    {
      dalilib_frame.error = 0;
    }

    if ((dalilib_frame.datalength < 24) && (dalilib_frame.datalength != 8) && (dalilib_frame.datalength != 16))
    {
      pDalill_bus->last_settling_time       = 0;
    }
    else
    {
      pDalill_bus->last_settling_time       = DALI_LL_TIME_STOP_CONDITION_READ;
    }

    // send to all devices on this hardware

    for (instCnt = 0; instCnt < pDalill_bus->anzInstances ; instCnt++)
    {
      dalill_pushRecvQueue(pDalill_bus->pInstances[instCnt], &dalilib_frame);
    }
  }
  else
  {
    // detecting bufferoverlow, frame is not valid -> throw away
    pDalill_bus->bufferOverflow = 0;
  }
}

volatile uint32_t supressTimer = 0;

void dalill_timerInterrupt2(dalill_bus_t* pDalill_bus)
{
  int dalill_time_offset = 0;
  int runTimer;
  uint32_t buspower_off_time;

  runTimer = 1;

  if (pDalill_bus->commingFromInt)
  {
    pDalill_bus->commingFromInt = 0;
    if ((pDalill_bus->cb.getBusState() == DALI_BUS_RX_LOW) &&
        (pDalill_bus->bus_state == BUS_RECEIVE_START_BIT) )
    {
      pDalill_bus->dalill_us_time = pDalill_bus->dalill_us_time + dalill_base.getCurrentTimerVal(pDalill_bus);
      pDalill_bus->old_cnt_val = 0;
      supressTimer++;
      runTimer = 0;
    }
  }

  if (runTimer)
  {
    dalill_usTimeRefresh(BY_TIMER_INTERRUPT,pDalill_bus);

    switch (pDalill_bus->bus_state)
    {
      case BUS_IDLE:
        // send status frame to stack
        if (dalill_frame_priority[5] > pDalill_bus->last_settling_time)
        {
          pDalill_bus->last_settling_time += DALI_LL_TIME_STATUS_FRAME;
        }
        dalill_sendStatusFrame(pDalill_bus);
        dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
      break;

      case BUS_END_OF_SENDING:
      case BUS_TOGGLE_LAST_EDGE:
        pDalill_bus->bus_state = BUS_IDLE;
        pDalill_bus->status.driver_status = DRIVER_IDLE;
        pDalill_bus->last_settling_time += DALI_LL_TIME_STOP_CONDITION_READ;
        dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
      break;

      case BUS_RECEIVE_START_BIT:
        pDalill_bus->status.driver_status = DRIVER_IDLE;

        if (pDalill_bus->cb.getBusState() == DALI_BUS_RX_LOW)
        {
          buspower_off_time = dalill_timeDiff(pDalill_bus->rx_man_frame.last_time_rx, pDalill_bus->dalill_us_time);
          pDalill_bus->bus_state = BUS_OFF;
          dalill_setAndStoreTimerPeriod((DALI_LL_TIME_BUS_POWER_DOWN - buspower_off_time), pDalill_bus);
        }
        else
        {
          // timeout after receiving start-bit
          pDalill_bus->bus_state = BUS_IDLE;
        }
      break;

      case BUS_RECEIVE_DATA:
      case BUS_COLLISION_IN_READ:

        // if we get here, frame is complete (stop condition)

        if(DALI_LL_TIME_STOP_CONDITION_READ <= dalill_timeDiff(pDalill_bus->rx_man_frame.last_time_rx, pDalill_bus->dalill_us_time))
        {
          pDalill_bus->last_settling_time = 0; // UKL
          if ( (MIN_HALFBIT_LEN <= pDalill_bus->rx_man_frame.length) && // Achtung 17 war früher richtig, siehe Änderung in ext_int !
               (BUS_COLLISION_IN_READ != pDalill_bus->bus_state) )
          {
            dalill_sendToLib(pDalill_bus);
          }
          else
          {
            pDalill_bus->status.driver_status = DRIVER_IDLE;
            if (pDalill_bus->cb.getBusState() == DALI_BUS_RX_LOW)
            {
              pDalill_bus->bus_state = BUS_OFF;
            }
            else
            {
              pDalill_bus->haveCollision = 1;
              pDalill_bus->bus_state = BUS_IDLE;
            }
          }

          //if (BUS_COLLISION_AVOIDANCE == pDalill_bus->bus_state)
          //{
            //dalill_setAndStoreTimerPeriod(DALI_LL_TIME_SETTLING_INTERVAL,pDalill_bus);
          //}
          //else
          {
            // no answer to received frame? go to IDLE
            if (pDalill_bus->haveCollision)
            {
              dalill_sendStatusFrame(pDalill_bus);
            }
            pDalill_bus->rx_man_frame.length = 0;
            pDalill_bus->bus_state = BUS_IDLE;
            pDalill_bus->status.driver_status = DRIVER_IDLE; // do not sent while receiving
            dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
          }
        }
      break;

      case BUS_COLLISION_AVOIDANCE:
        // if highest priority is reached stop counting
        if (dalill_frame_priority[5] > pDalill_bus->last_settling_time)
        {
          pDalill_bus->last_settling_time += DALI_LL_TIME_SETTLING_INTERVAL;
          dalill_setAndStoreTimerPeriod(DALI_LL_TIME_SETTLING_INTERVAL,pDalill_bus);
        }

        if ((pDalill_bus->last_settling_time + (2 * DALI_LL_TIME_SETTLING_INTERVAL)) >=
             pDalill_bus->tx_man_frame.settling_time)
        {
          pDalill_bus->bus_state = BUS_SEND_START_BIT;
        }
      break;

      case BUS_SEND_DATA:
      case BUS_SEND_START_BIT:
        if (BUS_SEND_START_BIT == pDalill_bus->bus_state)
        {
          pDalill_bus->bus_state = BUS_SEND_DATA;
        }
        // check if frame complete and add stop condition
        if ( (pDalill_bus->tx_man_frame.length <= pDalill_bus->tx_man_frame.current_bit_number) &&
             (0 != pDalill_bus->tx_man_frame.length) )
        {

          // check if frame is a to be send twice
          if (pDalill_bus->tx_man_frame.sendtwice)
          {
            //---------------------------------------------------------------------
            // waiting 3 times stop_condtion_time :
            // one for the stop-bit and two time to get into the middle of the
            // waittime for the retransmission
            //---------------------------------------------------------------------

            pDalill_bus->cb.setBusStateHigh();    // be shure bus gets idle again
            dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STOP_CONDITION * 7,pDalill_bus);
            pDalill_bus->tx_man_frame.sendtwice = 0;
            pDalill_bus->tx_man_frame.current_bit_number = 0;
            pDalill_bus->rx_man_frame.length = 0;
          }
          else
          {
            // send frame completed
            // feedback to dali library about completion of transmitting frame

        	if (DALI_BUS_RX_HIGH == pDalill_bus->cb.getBusState())
        	{
              pDalill_bus->bus_state = BUS_END_OF_SENDING;
        	}
        	else
        	{
              pDalill_bus->cb.setBusStateHigh();    // be shure bus gets idle again
              pDalill_bus->bus_state = BUS_TOGGLE_LAST_EDGE;
        	}
            dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STOP_CONDITION_READ,pDalill_bus);
            dalill_sendToLibTxComplete(pDalill_bus,0);
            pDalill_bus->rx_man_frame.length = 0;
          }
          // clear received (echo) frame
          pDalill_bus->rx_man_frame.last_time_rx = pDalill_bus->dalill_us_time;
        }
        else
        {
          // send Bits
          if(CheckBit(pDalill_bus->tx_man_frame.pData, pDalill_bus->tx_man_frame.current_bit_number))
          {
            // HIGH
            pDalill_bus->cb.setBusStateHigh();
            pDalill_bus->tx_man_frame.last_bit = DALI_BUS_RX_HIGH;

            // do not compensate switching time if there is no switching

            if (! pDalill_bus->doNotOverCompensate)
            {
              dalill_time_offset = (uint16_t) dalill_base.tx_low_offset;
              pDalill_bus->doNotOverCompensate = 1;
            }
            else
            {
            dalill_time_offset = 0;
            pDalill_bus->doNotOverCompensate = 0;
            }
          }
          else
          {
            // LOW
            pDalill_bus->cb.setBusStateLow();
            pDalill_bus->tx_man_frame.last_bit = DALI_BUS_RX_LOW;
            dalill_time_offset = (-1) * (uint16_t) dalill_base.tx_high_offset;
            pDalill_bus->doNotOverCompensate = 0;
          }
          pDalill_bus->tx_man_frame.current_bit_number++;

          // check if next Bit is the same
          if (pDalill_bus->tx_man_frame.last_bit == CheckBit(pDalill_bus->tx_man_frame.pData, pDalill_bus->tx_man_frame.current_bit_number))
          {
            if (pDalill_bus->tx_man_frame.length <= pDalill_bus->tx_man_frame.current_bit_number+1)
            {
              // check that current Bit is not the last
              dalill_setAndStoreTimerPeriod(DALI_LL_HALFBIT_TIME + dalill_time_offset,pDalill_bus);
            }
            else
            {
              // next Bit is the same => wait 2 * half-bit-time
              dalill_setAndStoreTimerPeriod((2*DALI_LL_HALFBIT_TIME) + dalill_time_offset,pDalill_bus);
            }
            pDalill_bus->tx_man_frame.current_bit_number++;
           // single Bit
          }
          else
          {
            dalill_setAndStoreTimerPeriod(DALI_LL_HALFBIT_TIME + dalill_time_offset,pDalill_bus);
          }
        }
      break;

      case BUS_SEND_BREAK:
        pDalill_bus->cb.setBusStateHigh();
        pDalill_bus->bus_state = BUS_IDLE;
        pDalill_bus->status.driver_status = DRIVER_IDLE;
        dalill_setAndStoreTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
      break;

      case BUS_RECOVER:
        dalill_setAndStoreTimerPeriod(DALI_LL_TIME_SETTLING_INTERVAL,pDalill_bus);
        pDalill_bus->bus_state = BUS_SEND_DATA;

        if (DALI_BUS_RX_HIGH == pDalill_bus->cb.getBusState())
        {
          pDalill_bus->last_settling_time = DALI_LL_TIME_RECOVER;
        }
      break;

      case BUS_OFF:
        if (DALI_BUSPOWER_SYSTEMFAILURE != pDalill_bus->status.bus_power_status)
        {
          buspower_off_time = dalill_timeDiff(pDalill_bus->rx_man_frame.last_time_rx, pDalill_bus->dalill_us_time);
          if (DALI_LL_TIME_BUS_SYSTEMFAILURE < buspower_off_time)
          {
            pDalill_bus->status.bus_power_status = DALI_BUSPOWER_SYSTEMFAILURE;
          }
          else
          {
            if ( DALI_LL_TIME_BUS_POWER_DOWN <= buspower_off_time)
            {
              pDalill_bus->status.bus_power_status = DALI_BUSPOWER_OFF;
              dalill_base.setTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
            }
          }
        }
        dalill_sendStatusFrame(pDalill_bus);
      break;

      case BUS_DELAY_BUS_POWER_ON:
         // New state for power-spike supression if bus is open
         if (DALI_BUS_RX_HIGH == pDalill_bus->cb.getBusState())
         {
           dalill_base.setTimerPeriod(DALI_LL_TIME_STATUS_FRAME,pDalill_bus);
           pDalill_bus->bus_state = BUS_IDLE;
           pDalill_bus->status.bus_power_status = DALI_BUSPOWER_ON;         
         }
         else
         {
           pDalill_bus->bus_state = BUS_OFF;
         }         
      break;
      
      default:
      break;
    }
  }
}

void dalill_createBase(dalill_base_t* pDalill_base)
{
  memcpy(&dalill_base, pDalill_base, sizeof(dalill_base_t));
  dalill_base.startTimer();
}
