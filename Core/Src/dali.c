/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack Main
 * Description:   Communication between Low-Level-Driver, Application
 * and Stack/Library
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#include "dali.h"

// #define DO_NOT_RESPOND 1

#include <stdio.h>
char oBuf[250];

dalilib_action_t    action;
uint8_t             bDaliStackReady;
dalilib_instance_t  pDaliStackInstance;
dalilib_cfg_t       ctrl_device_config;

// TODO: Queue between Low-level and Lib

typedef uint8_t bool;
#define true 1
#define false !true

#define NEW_API 1
#define FLASH_USER_START_ADDR   ((uint8_t *) 0x0803F800)
#define FLASH_USER_NUM_PAGES    1
#define FLASH_PAGE_DIM          2048
#define NUM_OF_WORD_IN_PAGE     (FLASH_PAGE_DIM/4)

extern void dalill_toDalilib(void * p_context, dalill_frame_t* p_frame);

typedef enum
{
	LED_INIT = 0,
	LED_OFF,
	LED_ON
}LED_STATE;

typedef enum
{
	NO_REPEAT,
	REPEAT_RUNNIG_UP,
	REPEAT_RUNNIG_DOWN
}PUSHBTN_REPEAT_STATE;

LED_STATE 			 ledState;
PUSHBTN_REPEAT_STATE pushBtnRepeatState;


//Debugging only

extern UART_HandleTypeDef huart2;
void SendString(char *str)
{
	while (*str )
	{
		//ITM_SendChar(*str);
		HAL_UART_Transmit(&huart2, str, 1, 5);
		str++;
	}
	HAL_UART_Transmit(&huart2, "\r\n", 1, 5);
}




/******************************************************************************/
/* will be called by the DALI library if it wants to send a DALI message
 * to the driver
 * result: 0: success
 ******************************************************************************/

volatile uint32_t isNull = 0;

static uint8_t dali_send_callback(void * p_context, dalilib_frame_t* p_frame)
{
    uint8_t result = 0;

    #ifndef DO_NOT_RESPOND
    if(bDaliStackReady)
    {
      if (1) // p_frame->datalength != 0)
      {
        result = dalill_pushSendQueue(p_context, (dalill_frame_t* ) p_frame,pDaliStackInstance);
      }
      else
      {
        isNull++;
      }
    }
    #else
    /*static uint32_t sframeCnt = 0;
    sframeCnt++;
    sprintf(oBuf,"Would send: %ld len %d : %02X %02X %02X %02X\n",sframeCnt,p_frame->datalength,
                                                                          p_frame->data_byte[0],p_frame->data_byte[1],p_frame->data_byte[2],p_frame->data_byte[3]);
    SendString(oBuf);
    */
    #endif

    return result;
}

/******************************************************************************/
/* will be called by the DALI library if it wants to send a DALI message
 * to the driver
 * result: 0: success
 ******************************************************************************/

/******************************************************************************/
/* forward low level frames to dalilib
*******************************************************************************/

// these variables only for debugging not for correct working

uint32_t recvTime;
uint32_t lastTime = 0;
uint32_t diffTime;
uint32_t diffTimeKorr;
uint8_t  flag = 0;
uint32_t frameCnt = 0;
extern uint32_t sendErr;
extern uint32_t supressTimer;
uint16_t count23 = 0;
extern uint32_t negVal;
extern uint32_t havNeg;
extern uint32_t endOfSending;
extern uint32_t qov;

void dalill_toDalilib(void * p_context, dalill_frame_t* p_frame)
{
  #ifdef TIMINGTEST
  if ( ! ((dalilib_frame_t*)p_frame)->type)
  {
    if (p_frame->datalength)
    {
       flag++;
       recvTime = ((dalilib_frame_t*)p_frame)->timestamp;
       sprintf(oBuf,"T: %04ld D: %04ld\n",recvTime / 10,(recvTime - lastTime) / 10);
       SendString(oBuf);
       lastTime = recvTime;
       if (flag == 2)
       {
         SendString("----\n");
         flag = 0;
       }
    }
  }
  #endif
  #ifndef DO_NOT_RESPOND
  if ( ! ((dalilib_frame_t*)p_frame)->type)
  {
    if (p_frame->datalength)
    {
      if (p_frame->datalength == 23) count23++;
      frameCnt++;
      recvTime = ((dalilib_frame_t*)p_frame)->timestamp;
      diffTime = recvTime - lastTime;
      diffTimeKorr = ((dalilib_frame_t*)p_frame)->timestampStartOfRecv - lastTime;
      #if 0
      sprintf(oBuf,"Frame %4ld len %02d Err %d TDiff %04lu.%lu TKorr %04lu.%lu C23 %02d LITim %02ld wPiI %02ld SBitF %02ld Len %04ld Wiii %02ld\n",frameCnt,p_frame->datalength,p_frame->error,diffTime / 10,diffTime % 10,
                                                                                                                                               diffTimeKorr / 10,diffTimeKorr % 10,count23,lowInTimer,wPiI,sBitFailure,sBitTime,wIII);
                                                                       // p_frame->data_byte[0],p_frame->data_byte[1],p_frame->data_byte[2],p_frame->data_byte[3]);
      #else
      sprintf(oBuf,"Frame %4ld len %02d Err %d TDiff %04lu.%lu TKorr %04lu.%lu C23 %02d sendErr %02ld SupTim %02ld QOV %02ld Eos %ld\n",frameCnt,p_frame->datalength,p_frame->error,diffTime / 10,diffTime % 10,
                                                                                           diffTimeKorr / 10,diffTimeKorr % 10,count23,sendErr,supressTimer,qov,endOfSending);
      sendErr = 0;
      #endif

      lastTime = recvTime;
      SendString(oBuf);
    }
  }
  #endif
	dalilib_receive(p_context, (dalilib_frame_t*)p_frame);
}

/******************************************************************************/
/* application function
 * Here only the example call of the action functions is displayed.
 * The application should decide when the action functions should be called
*******************************************************************************/

// TEST VARIABLES

uint8_t TestRunning  = 0 ;
uint8_t TestStopping = 0;
uint8_t address = 0;
uint8_t waitForAnswer;
uint8_t haveError = 0;

R_DALILIB_RESULT app_action(DALILIB_CTRL_GEAR_ACTION_CODE_TYPE gearActionType,
 			    uint8_t gearAction)
{
	  action.actionType = DALILIB_CTRL_GEAR_ACTION;
    if (TestRunning)
    {
	    action.gearAct.adr.adrType = DALI_ADRTYPE_SHORT;

	    if (address == 0x40)
	    {
	      address--;
	    }
	    action.gearAct.adr.adr = address;
    }
    else
    {
	    action.gearAct.adr.adrType = DALI_ADRTYPE_BROADCAST;
    }
	  action.gearAct.gearActionType = gearActionType;

	switch(gearActionType)
	{
	default:
	case DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS:
			// empty action
  break;
	case DALILIB_ACT_TYPE_CTRL_GEAR_QUERIES:
		    action.gearAct.action.queriesAction = DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_PRESENT;
		break;
	case DALILIB_ACT_TYPE_CTRL_GEAR_LEVEL:
			action.gearAct.action.levelAction = gearAction;
		break;
    case DALILIB_ACT_TYPE_CTRL_GEAR_MEMORY:
	        action.gearAct.action.memoryAction = DALILIB_ACT_CTRL_GEAR_READ_MEMORY_CELL;
	        action.gearAct.value.memoryParams.memoryBankNr = 0;
	        action.gearAct.value.memoryParams.memoryCellIdx = gearAction;
	        SendString("Querying ctrl gear mem\n");
    	break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE:
      action.gearAct.action.initAction = (DALILIB_ACT_CTRL_GEAR_INITIALISE) gearAction;
      action.gearAct.value.actionValue = 0x02;
    break;
 	}

  return dalilib_action(pDaliStackInstance, &action);
}

/******************************************************************************/
/* invoked by the DALI stack to notify the application that is ready
 * to receive the actions
*******************************************************************************/
static void dali_ready_callback(void)
{
    bDaliStackReady = 1;
    pushBtnRepeatState = REPEAT_RUNNIG_DOWN;
    ledState = LED_INIT;
}


/******************************************************************************/
/* invoked by the DALI stack to notify the application that is ready to receive the actions
*******************************************************************************/
static void dali_log_callback(char *pLog, uint8_t nLen)
{
	   //sprintf(oBuf,"Masterlog : <%s>\n",pLog);
	   //SendString(oBuf);
}

/******************************************************************************/
/*  will be called by the DALI stack to tell the application  shall load the persistent variables
 ******************************************************************************/
static R_DALILIB_RESULT dali_loadmemory_callback(void* pMemory, uint16_t nSize)
{
    uint8_t err = 0; //SUCCESS
    switch(ctrl_device_config.mode)
    {
        case STACK_MODE_CONTROL_GEAR:
            // TODO:
        	//err = flash_read(pMemory, nSize, 0);
            break;
        case STACK_MODE_CONTROL_DEVICE:
            // TODO:
        	//err = flash_read(pMemory, nSize, 1);
            break;
        default:
        	break;
    }
    return (err);
}


/******************************************************************************/
/* will be called by the DALI library to tell the application shall save the persistent variables
*******************************************************************************/
static R_DALILIB_RESULT dali_savememory_callback(void* pMemory, uint16_t nSize)
{
    uint8_t err = 0; //SUCCESS
    switch(ctrl_device_config.mode)
    {
       case STACK_MODE_CONTROL_GEAR:
           // TODO:
    	   //err = flash_write(pMemory, nSize, 0);
            break;
        case STACK_MODE_CONTROL_DEVICE:
            // TODO:
        	//err = flash_write(pMemory, nSize, 1);
            break;
        default:
            break;
    }
    return (err);
}

extern dalill_bus_t* pDalill_bus_0;

/******************************************************************************/
/* will be called by the DALI library to notify the application about 'something'
*******************************************************************************/
static uint8_t dali_device_reaction_callback(dalilib_react_event_t *p_args)
{
	uint8_t gearAction = 0x00;

	// sprintf(oBuf,"Status in reaction %d\n",pDalill_bus_0->status.driver_status);
	// SendString(oBuf);

    switch(p_args->reactionCode)
    {
    	//case DALILIB_REACT_EVENT_PUSH_BUTTON_PRESSED:
    	case DALILIB_REACT_EVENT_PUSH_BUTTON_SHORT_PRESSED:
    	case DALILIB_REACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED:
    			gearAction = (ledState == LED_ON ?
    							   DALILIB_ACT_CTRL_GEAR_LEVEL_OFF :
								   DALILIB_ACT_CTRL_GEAR_LEVEL_RECALL_MAX_LEVEL );
    			ledState = (gearAction == DALILIB_ACT_CTRL_GEAR_LEVEL_OFF ? LED_OFF : LED_ON);

				if(ledState == LED_ON)
				{
					pushBtnRepeatState = REPEAT_RUNNIG_DOWN;
				}
				else
				{
					pushBtnRepeatState = REPEAT_RUNNIG_UP;
				}
    		break;

    	case DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_START:
    	case DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT:
    		{
    			if(LED_OFF == ledState && REPEAT_RUNNIG_UP == pushBtnRepeatState)
    			{
    				gearAction = DALILIB_ACT_CTRL_GEAR_LEVEL_ON_STEP_UP;
    				ledState = LED_ON;
    			}

    			else if(REPEAT_RUNNIG_UP == pushBtnRepeatState)
    			{
					gearAction = DALILIB_ACT_CTRL_GEAR_LEVEL_UP;
    			}
    			else if(REPEAT_RUNNIG_DOWN == pushBtnRepeatState)
    			{
					gearAction = DALILIB_ACT_CTRL_GEAR_LEVEL_DOWN;
    			}
    		}
			break;

    	case DALILIB_REACT_EVENT_PUSH_BUTTON_STUCK:
    	case DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP:
    		if(REPEAT_RUNNIG_UP == pushBtnRepeatState)
			{
    			pushBtnRepeatState = REPEAT_RUNNIG_DOWN;
			}
			else if(REPEAT_RUNNIG_DOWN == pushBtnRepeatState)
			{
				pushBtnRepeatState = REPEAT_RUNNIG_UP;
			}
    		break;

        default:
		 	break;
    }

    if (gearAction)
    {
		app_action(DALILIB_ACT_TYPE_CTRL_GEAR_LEVEL, gearAction);
		//app_action(DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS, 0x00);
    }

    return (0);
}


static char *printValueType(DALILIB_RESPONSE_VALUE_TYPE valueType)
{
  char *rc = NULL;

  switch (valueType)
  {
    case DALILIB_RESPONSE_VALUE_VALID:            rc = "DALILIB_RESPONSE_VALUE_VALID"; break;
    case DALILIB_RESPONSE_VALUE_INVALID:          rc = "DALILIB_RESPONSE_VALUE_INVALID"; break;
    case DALILIB_RESPONSE_VALUE_NO_ANSWER:        rc = "DALILIB_RESPONSE_VALUE_NO_ANSWER"; break;
    case DALILIB_RESPONSE_VALUE_REQUEST_SENT:     rc = "DALILIB_RESPONSE_VALUE_REQUEST_SENT"; break;
    case DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT: rc = "DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT"; break;
    case DALILIB_RESPONSE_VALUE_SCENE_NOT_USED:   rc = "DALILIB_RESPONSE_VALUE_SCENE_NOT_USED"; break;
    default:                                      rc = "???"; break;
  }

  return rc;
}
/******************************************************************************/
/* will be called by the DALI library to notify the application about 'something'
*******************************************************************************/

static uint8_t dali_gear_reaction_callback(dalilib_react_ctrl_gear_t * p_args)
{
    if(p_args->reactionCode == DALILIB_REACT_CTRL_GEAR_QUERY_STATUS)
    {
    	ledState = (p_args->gearStatus.lampOn ? LED_ON : LED_OFF);

		  if(ledState == LED_ON)
		  {
		  	pushBtnRepeatState = REPEAT_RUNNIG_DOWN;
	  	}
	  	else
	  	{
 	  		pushBtnRepeatState = REPEAT_RUNNIG_UP;
  		}
    }

    if(p_args->reactionCode == DALILIB_REACT_CTRL_GEAR_QUERIES)
    // if(p_args->reactionCode == DALILIB_REACT_CTRL_GEAR_QUERY_STATUS)
    {
 	   if (TestRunning || TestStopping)
 	   {
 		   if (p_args->valueType != DALILIB_RESPONSE_VALUE_VALID)
 		   {
 	 		   sprintf(oBuf,"Gear %02d is not present ! VALUE %d\n",address,p_args->valueType);
 	 		   SendString(oBuf);
 		     sprintf(oBuf,"ErrorValue for address %d : ",address);
 		     SendString(oBuf);
 	 		   SendString(printValueType(p_args->valueType));
 	 		   SendString("\n");

 	 		   if (p_args->valueType == DALILIB_RESPONSE_VALUE_NO_ANSWER)
 	 		   {
 	 		     if (address) address--;
 	 		   }
 	 		   else
 	 		   {
 	 		     haveError = 1;
 	 		   }
 		   }
 		   else
 		   {
 	 		   sprintf(oBuf,"Gear %02d is present !\n",address);
 	 		   SendString(oBuf);
 	       if (address) address--;
		   }
		   if (TestStopping) TestStopping = 0;

		   waitForAnswer = 0;
 	   }
    }
    else
    {
      sprintf(oBuf,"Other reaction type %d : ",p_args->reactionCode);
      SendString(oBuf);
      SendString(printValueType(p_args->valueType));
      SendString("\n");
      sprintf(oBuf,"Status in reactionCallback %d\n",pDalill_bus_0->status.driver_status);
      SendString(oBuf);
      waitForAnswer = 0;
    }
    return (0);
}

/******************************************************************************/
/* will be called by the DALI library to notify the application about 'something'
*******************************************************************************/


static uint8_t dali_reaction_callback(dalilib_action_t * p_args)
{
    uint8_t res = 0;
    switch(p_args->actionType)
    {
        case DALILIB_EVENT_ACTION:
                res = dali_device_reaction_callback(&p_args->eventReact);
            break;

        case DALILIB_CTRL_GEAR_REACTION:
        		res = dali_gear_reaction_callback(&p_args->gearReact);
        	break;

        case DALILIB_STATUS_REACTION:
            if (p_args->statusReact.reactionCode == DALILIB_REACT_STATUS_BUSPOWER)
            {
              switch(p_args->statusReact.reactValue)
              {
                case  DALILIB_BUSPOWER_ON:
                  SendString("Reaction status value (APPCLTRL): BUSPOWER_ON\n");
                break;

                case DALILIB_BUSPOWER_OFF:
                  SendString("Reaction status value (APPCTRL): BUSPOWER_OFF\n");
                break;

                case DALILIB_BUSPOWER_SYSTEMFAILURE:
                  SendString("Reaction status value (APPCTRL): BUSPOWER_SYSTEMFAILURE\n");
                break;

                default:
                  SendString("Reaction status value (APCCTRL): ILLEGAL BUSPOWERVALUE !!!\n");
                break;
              }
            }

            if (p_args->statusReact.reactionCode == DALILIB_REACT_STATUS_BUSCOLLISION)
            {
              switch(p_args->statusReact.reactValue)
              {
                case DALILIB_BUSCOLLISION_DETECTED:
                  SendString("Bus-collision detected\n");
                break;

                case DALILIB_BUSCOLLISION_NONE:
                  SendString("Bus-collision gone\n");
                break;
              }
            }
        break;


        default:
             res = 0xFF; //unsupported reaction type
        break;
    }

    return (res);
}
/******************************************************************************/
/* DALI stack configuration structure will be initialized
*******************************************************************************/
static void dali_create_application_controller_config(void)
{
    memset(&ctrl_device_config, 0, sizeof(ctrl_device_config));

    // stack mode
    ctrl_device_config.mode = STACK_MODE_CONTROL_DEVICE;
    ctrl_device_config.vendor.control_device_mode = CTRL_DEV_MODE_MULTI;

    // vendor information
    ctrl_device_config.vendor.gtin[0] = 'M';
    ctrl_device_config.vendor.gtin[1] = 'B';
    ctrl_device_config.vendor.gtin[2] = 'S';
    ctrl_device_config.vendor.gtin[3] = '0';
    ctrl_device_config.vendor.gtin[4] = '1';
    ctrl_device_config.vendor.gtin[5] = '7';

    ctrl_device_config.vendor.firmware_ver_major = 0x01;
    ctrl_device_config.vendor.firmware_ver_minor = 0x02;

    ctrl_device_config.vendor.identify_number[0] = 0x01;
    ctrl_device_config.vendor.identify_number[1] = 0x02;
    ctrl_device_config.vendor.identify_number[2] = 0x03;
    ctrl_device_config.vendor.identify_number[3] = 0x04;
    ctrl_device_config.vendor.identify_number[4] = 0x08;
    ctrl_device_config.vendor.identify_number[5] = 0x0A;
    ctrl_device_config.vendor.identify_number[6] = 0x12;
    ctrl_device_config.vendor.identify_number[7] = 0x12;

    ctrl_device_config.vendor.hw_ver_major      = 0x10;
    ctrl_device_config.vendor.hw_ver_minor      = 0x33;

    // callback functions
    ctrl_device_config.callbacks.fPAppReady    = dali_ready_callback;
    ctrl_device_config.callbacks.fPAppSend     = dali_send_callback;
    ctrl_device_config.callbacks.fPAppLoadMem  = dali_loadmemory_callback;
    ctrl_device_config.callbacks.fPAppSaveMem  = dali_savememory_callback;
    ctrl_device_config.callbacks.fPAppReAction = dali_reaction_callback;
    ctrl_device_config.callbacks.fPAppLog      = dali_log_callback;

    // no address, shall be set to 0xFF
    ctrl_device_config.vendor.shortAddress = 0x00;
}


/******************************************************************************/
/* called every 10 ms
*******************************************************************************/

uint8_t entPrell = 0;

void dalill_timingHelper(void *pInstance,uint32_t dali_time_ticker)
{
	static uint16_t toggleCnt = 0;

	if (toggleCnt == 100)
	{
	   toggleCnt = 1;
	   toggleOrangeLed();
  }
	else
	{
	  toggleCnt++;
  }

	if (entPrell)
	{
	  entPrell++;
	}
	if (entPrell == 25) entPrell = 0;

  dalilib_timingHelper(pInstance, dali_time_ticker);
}

uint8_t haveData = 0;

void signalToThread()
{
  haveData = 1;
}

/******************************************************************************/
/* initialize the HAL driver and the DALI stack
*******************************************************************************/
void dali_init(dalill_bus_t* pDalill_bus,dalill_bus_t *pDalill_bus2)
{
   dalill_base_t dalill_base;

   memset(&dalill_base,0,sizeof(dalill_base_t));

   pDaliStackInstance = NULL;
   bDaliStackReady = 0;
	// dali_time_ticker = 0;

   // create new DALI stack instance
   pDaliStackInstance = dalilib_createinstance();
   sprintf(oBuf,"ApiInstance is %p\n",pDaliStackInstance);
   if (NULL == pDaliStackInstance)
   {
        // error
        return;
   }
   addInstance(pDalill_bus,pDaliStackInstance);

  // create and configure DALI stack as single application controller
  dali_create_application_controller_config();

  // add Low Level structure to DALI stack instance and vice versa
  ctrl_device_config.context = pDalill_bus;
  pDalill_bus->context = pDaliStackInstance;

  // initialize DALI stack instance
  if (R_DALILIB_OK != dalilib_init(pDaliStackInstance , &ctrl_device_config))
  {
      // error
      return;
  }

  // start DALI stack instance
  if (R_DALILIB_OK != dalilib_start(pDaliStackInstance))
  {
      // error
      return;
  }


  // initialize callback functions for DALI Low Level Driver
  dalill_base.debug_mode         = 0;
  dalill_base.max_frame_length   = MAXFRAMELENGTH;

  // Offsets for "Dali-Click-Board" levelshifter. Must be adapted for actual hardware

  dalill_base.rx_rcv_high_offset = 50;
  dalill_base.rx_rcv_low_offset  = 64;
  dalill_base.rx_high_offset     = 90;   // 56 // 62;   // 60 DALI 2 click
  dalill_base.rx_low_offset      = 22;   // 18;   //29 42;   // 60 DALI 2 click
  dalill_base.tx_high_offset     = 48;   // 35 DALI 2 click
  dalill_base.tx_low_offset      = 34;   // 35 DALI 2 click
  dalill_base.dalilltimingHelper = &dalill_timingHelper;
  dalill_base.dalilltoDalilib    = &dalill_toDalilib;
  dalill_base.getCurrentTimerVal = &dalill_getCurrentTimerVal;
  dalill_base.getTimerPeriod     = &dalill_getTimerPeriod;
  dalill_base.setTimerPeriod     = &dalill_setTimerPeriod;
  dalill_base.startTimer         = &dalill_startTimer;
  dalill_base.signalToThread     = &signalToThread;
  dalill_base.enableIRQ          = NULL;
  dalill_base.disableIRQ         = NULL;

  dalill_createBase(&dalill_base);
}


void dali_main(void)
{
  int      oFlag = 0;
  uint8_t  result;
  uint16_t errCnt = 0;

  TestRunning = 1;

	SendString("Starting Dali stack (MASTER 53) !\n");
	sprintf(oBuf,"Sizeofdriver status %d %d\n",sizeof(driver_status_t),sizeof(uint8_t));
	SendString(oBuf);

	while(1)
	{
	  if (dalill_isBusy())
	  {
		  dalill_processQueues(); // new API !
	  }

		if (dalill_getButtonState() && (!entPrell))
		{
		  entPrell = 1;
      #if 1
		  if (!TestRunning)
		  {
		    waitForAnswer = 0;
		    TestStopping = 0;
        address = 0x3F;
		    TestRunning = 1;
		  }
		  else
		  {
		    TestStopping = 1;
		    TestRunning  = 0;
		    waitForAnswer = 0;
		  }
      #endif

      while(dalill_getButtonState());
		}

    #if 1
    if (haveError)
    {
      haveError = 0;
    }
    //
    // if test is running we sent "QUERY CONTROL GEAR PRESENT" to all adresses
    //
    if (TestRunning && !waitForAnswer && !haveError)
    {
      result = app_action(DALILIB_ACT_TYPE_CTRL_GEAR_QUERIES,0x00);
      // result = app_action(DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE,DALILIB_ACT_CTRL_GEAR_INIT_INITIALISE);
      if (R_DALILIB_OK == result)
      {
        sprintf(oBuf,"Sending %d (%d)\n",address,errCnt);
        SendString(oBuf);
        errCnt = 0;
        waitForAnswer = 1;
        oFlag = 0;

        // app_action(DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS,0x00);

        if (address == 0)
        {
         	TestRunning--;
         	if (! TestRunning)
         	{
            TestStopping = 1;
            SendString("Test completed\n");
          }
          else
          {
            address = 0x40;
           }
         }
       }
       else
       {
         if (! oFlag)
         {
           sprintf(oBuf,"Cannot sent %d error %d\n",address,result);
           SendString(oBuf);
           oFlag = 1;
         }
       errCnt++;
     }
   }
   #endif


      #if 0
       if (TestRunning && ! waitForAnswer)
       {
          app_action(DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE,DALILIB_ACT_CTRL_GEAR_INIT_INITIALISE);
          // sendQueryStatus = 0;
          // address       = 1; // 34;
          //app_action(DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS,0x00);
          // TestRunning = 0;

          // SendString("Sending Query Status\n");
          toggleGreenLed();
          waitForAnswer = 1;
          TestStopping  = 0;
          TestRunning   = 0;

          while(dalill_getButtonState());
        }
        #endif

	}
}
