/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack HAL
 * Description:   Abstraction Layer between DALI-low-level driver and uC Timer, Interrupts, etc.
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/


#include "dali_ll_hal.h"

extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;

extern dalill_bus_t dalill_bus_lines_g[];

// uint32_t myTicks = 0;
// uint32_t timPeriod = 10000;

#define SINGLE_TIMER

#define TIMVAL 10

uint32_t dalill_getCurrentTimerVal(dalill_bus_t* pDalill_bus)
{
	  return htim3.Instance->CNT;
}

uint32_t dalill_getTimerPeriod(dalill_bus_t* pDalill_bus)
{
    return htim3.Init.Period+1;
}

void dalill_startTimer()
{
	HAL_TIM_Base_Start_IT(&htim3);
  __HAL_TIM_SET_AUTORELOAD(&htim3,TIMVAL - 1);
}

void dalill_setTimerPeriod(uint16_t uPeriod,dalill_bus_t* pDalill_bus)
{
  __HAL_TIM_SET_AUTORELOAD(&htim3, uPeriod + dalill_getCurrentTimerVal(pDalill_bus) - 1);
}

/*!  Set/Read GPIOs of Bus 0 */
uint8_t dalill_getBusState1()
{
	//return HAL_GPIO_ReadPin(DALI_RX_GPIO_Port, DALI_RX_Pin);
	// DALI 2 click
	return HAL_GPIO_ReadPin(DALI_RX1_GPIO_Port, DALI_RX1_Pin)?0:1;
}


void dalill_setBusStateHigh1()
{
	//HAL_GPIO_WritePin(DALI_TX_GPIO_Port, DALI_TX_Pin, GPIO_PIN_SET);
	// DALI 2 click
	HAL_GPIO_WritePin(DALI_TX1_GPIO_Port, DALI_TX1_Pin, GPIO_PIN_RESET);
}

void dalill_setBusStateLow1()
{
	//HAL_GPIO_WritePin(DALI_TX_GPIO_Port, DALI_TX_Pin, GPIO_PIN_RESET);
	// DALI 2 click
	HAL_GPIO_WritePin(DALI_TX1_GPIO_Port, DALI_TX1_Pin, GPIO_PIN_SET);
}

void toggleRedLed()
{
	if (HAL_GPIO_ReadPin(LED_RED_GPIO_Port, LED_RED_Pin))
	{
		HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
	}
}

void setRedLed()
{
  HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
}

void resetRedLed()
{
  HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
}

void setGreenLed()
{
	HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
}

void resetGreenLed()
{
	HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
}

void toggleGreenLed()
{
	if (HAL_GPIO_ReadPin(LED_GREEN_GPIO_Port, LED_GREEN_Pin))
	{
		HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
	}
}

void toggleOrangeLed()
{
	if (HAL_GPIO_ReadPin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin))
	{
		HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_SET);
	}
}

void setOrangeLed()
{
	HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_SET);
}

void resetOrangeLed()
{
	HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_RESET);
}


void toggleBlueLed()
{
	if (HAL_GPIO_ReadPin(LED_BLUE_GPIO_Port, LED_BLUE_Pin))
	{
		HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
	}
}

void FlashOn()
{
    HAL_GPIO_WritePin(FLASH_RED_GPIO_Port, FLASH_RED_Pin, GPIO_PIN_SET);
}

void FlashOff()
{
    HAL_GPIO_WritePin(FLASH_RED_GPIO_Port, FLASH_RED_Pin, GPIO_PIN_RESET);
}

uint8_t dalill_getButtonState()
{
	return HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin)?0:1;
}

