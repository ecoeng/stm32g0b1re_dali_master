/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack HAL-Header
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef DALI_LL_HAL_H_
#define DALI_LL_HAL_H_

#include "stdint.h"
#include "main.h"
#include "dali_ll.h"

void boardLedOn(void);
void boardLedOff(void);

uint32_t dalill_getCurrentTimerVal(dalill_bus_t* pDalill_bus);
uint32_t dalill_getTimerPeriod(dalill_bus_t* pDalill_bus);
void dalill_startTimer(void);
void dalill_setTimerPeriod(uint16_t period,dalill_bus_t* pDalill_bus);

uint8_t dalill_getBusState1(void);
// uint8_t dalill_getButtonState1(void);
void dalill_setBusStateHigh1(void);
void dalill_setBusStateLow1(void);

uint8_t dalill_getBusState2(void);
// uint8_t dalill_getButtonState2(void);
void dalill_setBusStateHigh2(void);
void dalill_setBusStateLow2(void);


void toggleRedLed(void);
void toggleGreenLed(void);
void setGreenLed();
void resetGreenLed();
void toggleOrangeLed(void);
void setOrangeLed();
void resetOrangeLed();
void toggleBlueLed(void);
void FlashOn();
void FlashOff();

uint8_t dalill_getButtonState();
#endif /* DALI_LL_HAL_H_ */
