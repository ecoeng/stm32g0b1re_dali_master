/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack Main-Header
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef DALI_H_
#define DALI_H_

#include "main.h"
#include "dali_ll_hal.h"
#include "dali_ll.h"
#include "stdint.h"
#include "libdali.h"

void dali_init(dalill_bus_t* pDalill_bus,dalill_bus_t* pDalill_bus2);
void push_button_interrupt(void);
void dali_main(void);

#endif /* DALI_H_ */
