/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                IEC 62386-101,102,103
 *                Part:   Control-Gear: 207,209,301,303,304,
 *                Version 2.0
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef LIBDALI_H_
#define LIBDALI_H_


#ifdef  __cplusplus
extern "C" {
#endif

// #define API_FRAMES_64BIT 1 // make longer frames possible 32 or 64 bit

/* Defines to activate D4i Extensions.
   Uncomment to activate the respective DALI Parts.
   Requires recompiling the library on change! */
// #define DALILIB_GEAR_POWER_SUPPLY_PRESENT      //!< Activate DALI Part 250 - Integrated Bus Power Supply
// #define DALILIB_GEAR_MEM_BANK1_EXT_PRESENT     //!< Activate DALI Part 251 - Memory Bank 1 Extension
// #define DALILIB_GEAR_ENERGY_REPORT_PRESENT     //!< Activate DALI Part 252 - Energy Reporting
// #define DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT  //!< Activate DALI Part 253 - Diagnostics and Maintenance
// #define DALILIB_DEV_LUMINAIRE_PRESENT          //!< Activate DALI Part 351 - Luminaire-mounted Control Devices


/**********************************************************************
 * DALI-Library structs/enums/defines
 **********************************************************************/

/*! DALI result codes
 * - defined between 0..127 \n
   - advanced result codes only for stack internal between 128..255 */
typedef uint8_t  R_DALILIB_RESULT;

// DALI-Library result codes
#define R_DALILIB_OK                                   0  //!< successfull
#define R_DALILIB_NOT_STARTED                          1  //!< stack instance not started
#define R_DALILIB_INVALID_INSTANCE                     2  //!< invalid stack instance

#define R_DALILIB_INTERFACE_NOT_READY                  3  //!< not connected with HAL-Driver

#define R_DALILIB_NOT_INITIALISED                      4  //!< stack instance not initialized
#define R_DALILIB_CALLBACK_FAILED                      5  //!< one or more required callback function not defined
#define R_DALILIB_MODE_FAILURE                         6  //!< stack mode not yet set
#define R_DALILIB_BUSY                                 7  //!< stack is busy
#define R_DALILIB_NOT_SUPPORTED_ACTION                 8  /*!< not supported action code
                                                               or invalid action code for the stack mode */

#define R_DALILIB_FRAMING_ERROR                        9  
#define R_DALILIB_FRAME_NOT_SUPPORTED                  10 /*!< invalid DALI stack frame received \n
                                                               not correct converted from HAL-Driver frame to DALI-Stack frame */ 
#define R_DALILIB_FRAME_NOT_SEND                       11 //!< frame not yet send
#define R_DALILIB_RESPONSE_TIMEOUT                     12 /*!< response time out
                                                               control gear/device not connected or \n
                                                               response is NO */
#define R_DALILIB_RESPONSE_FRAME_NOT_ACCEPTED          13 //!< unexpected response frame received

#define R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED          14 //!< received control gear command not supported
#define R_DALILIB_CTRL_GEAR_INVALID_ADDRESS            15 //!< control gear address is invalid
#define R_DALILIB_CTRL_GEAR_INVALID_FF                 16 //!< received control gear forward frame  is invalid
#define R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED       17 //!< control gear action code not supported

#define R_DALILIB_APPLICATION_CONTROLLER_DISABLED      18 //!< 
#define R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED        19 //!< received control device command not supported
#define R_DALILIB_CTRL_DEVICE_INVALID_ADDRESS          20 //!< control device address is invalid
#define R_DALILIB_CTRL_DEVICE_INVALID_INSTANCE_ADDRESS 21 //!< control device instance address is invalid

#define R_DALILIB_INSTANCE_DISABLED                    22 //!< event can not triggered because instance is not active
#define R_DALILIB_CTRL_DEVICE_INVALID_SCHEME           23 //!< invalid event scheme
#define R_DALILIB_CTRL_DEVICE_INVALID_EVENT_MESSAGE    24 //!< received event message is invalid
#define R_DALILIB_CTRL_DEVICE_IGNORE_EVENT             25 //!< event filter is not set for this event
#define R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN            26 //!< received unknown event
#define R_DALILIB_STACK_TERMINATED                     27 //!< stack instance stopped

#define R_DALILIB_GEAR_DEVICE_TYPE_NOT_SUPPORTED       28 //!< control gear device type not supported
#define R_DALILIB_INPUT_DEVICE_TYPE_NOT_SUPPORTED      29 //!< input device type not supported
#define R_DALILIB_INPUT_DEVICE_RESOLUTION_INVALID      30 //!< 
#define R_DALILIB_BUS_FAILURE                          31 //!< bus power is off or system failure is true
#define R_DALILIB_CTRL_DEVICE_QUIESCENT_MODE_ENABLED   32 //!< control device quiescent mode is enabled (see part 103, 9.9.3)
#define R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS      33 //!< control gear action parameter invalid
#define R_DALILIB_PERSISTENT_MEM_DATA_SIZE_TOO_SMALL   34 //!< defined persistent memory size is smaller than required
#define R_DALILIB_VARIABLE_OUT_OF_BOUNDS               35 //!< the value of a variable is outside it's defined range
#define R_DALILIB_MAX                                 127 //!< 


// DALI - Library instance type
// Instance specific control block to pass into the DALI API calls.
typedef void*  dalilib_instance_t;

// current timestamp resolution [100us]
typedef uint32_t  dalilib_time;


//! Status information of DALI bus power
typedef enum
{
  DALILIB_BUSPOWER_ON             = 0,  //!< Buspower is on
  DALILIB_BUSPOWER_OFF            = 1,  //!< short Buspower interruption for 45ms .. 500ms
  DALILIB_BUSPOWER_SYSTEMFAILURE  = 2   //!< long  Buspower interruption for more than 500ms
}dalilib_buspower_status_t;

//! Status information of DALI bus collisions
typedef enum
{
  DALILIB_BUSCOLLISION_NONE     = 0,  //!< No bus collisions detected
  DALILIB_BUSCOLLISION_DETECTED = 1,  //!< A bus collision has been detected
}dalilib_buscollision_status_t;

//! Status information for an interface
typedef struct
{
  dalilib_buspower_status_t bus_power_status;  //!< DALI bus power status
  uint8_t                   interface_open;    //!< 1 if interface is open
  uint8_t                   transmit_pending;  //!< 1 if a frame is waiting to be transmitted
  uint8_t                   collision_detect;  //!< 1 if collision detected
}dalilib_status_t;

//! Frame structure between stack and low-level-driver

typedef struct
{
  uint32_t  timestamp; /*! timestamp of last received bit in frame in 100usec ticks \n
                             will be set from low-level-driver \n
                             resolution [100us] */
  uint32_t  timestampStartOfRecv; // Store start of receiving for correct settling time computation
  union
  {
    uint8_t frameflags;
    struct
    {
      uint8_t priority;   //!< priority of frame, 0=backward frame, 1..5=forward frame
      uint8_t sendtwice;  //!< 1 for frames that should be send twice
      uint8_t tx;         //!< 1 for frames that were transmitted
      uint8_t error;      //!< 1 if received or transmitted frame has error
      uint8_t type;       //!< 0=data is DALI frame data, 1= data is status information
      uint8_t reserved;
    };
  };
  uint8_t datalength;
  union
  {
    #ifndef API_FRAMES_64BIT
    uint32_t data;          //!< DALI data, valid if   type = 0
    uint8_t  data_byte[4];
    #else
    uint64_t data;
    uint8_t  data_byte[8];
    #endif    
    dalilib_status_t status;        //!< DALI status, valid if type = 1
  };
}dalilib_frame_t;

// dali library info structure

typedef struct
{
  uint32_t vendorID;
}dalilib_info_t;

/// dali library version structure

typedef struct 
{
  uint8_t code_version_minor;
  uint8_t code_version_major;
  uint8_t api_version_minor; 
  uint8_t api_version_major;
}dalilib_version_t;

/*! describes the address of a DALI unit */

typedef enum
{
  DALI_ADRTYPE_SHORT = 0,  //!< short address
  DALI_ADRTYPE_GRP,        //!< group address
  DALI_ADRTYPE_NBROADCAST, //!< broadcast not addressed
  DALI_ADRTYPE_BROADCAST,
  DALI_ADRTYPE_SPEC,       //!< special command
  DALI_ADRTYPE_RESERVED
}DALI_ADRTYPE;

/*! this flag describes whether the answer is valid or not \n
 *  the request could not be sent \n
    when it is the scene requests, returns whether the scene is set or not */
    
typedef enum
{
  DALILIB_RESPONSE_VALUE_VALID = 0,
  DALILIB_RESPONSE_VALUE_NO_ANSWER,
  DALILIB_RESPONSE_VALUE_REQUEST_SENT,
  DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT,
  DALILIB_RESPONSE_VALUE_SCENE_NOT_USED,
  DALILIB_RESPONSE_VALUE_INVALID,
}DALILIB_RESPONSE_VALUE_TYPE;


typedef struct
{
  DALI_ADRTYPE adrType; //!< address type
  uint8_t      adr;     /*!< used, if adrType == DALI_ADRTYPE_SHORT \n
                             used, value[1..16], if adrType == DALI_ADRTYPE_GRP \n
                             unused, if adrType == DALI_ADRTYPE_BROADCAST */
}gear_adr_t;

/*! for standard instance commands, the method of instance addressing used by the transmitter */
typedef enum
{
  DALI_INSTANCE_ADRTYPE_NONE = 0,        //!< not instance addressing
  DALI_INSTANCE_ADRTYPE_NUMBER,          //!< Addressing Instance number
  DALI_INSTANCE_ADRTYPE_GRP,             //!< Addressing Instance group
  DALI_INSTANCE_ADRTYPE_TYPE,            //!< Addressing Instance type
  DALI_INSTANCE_ADRTYPE_NUMBER_LEVEL,    //!< Addressing Feature on instance number level
  DALI_INSTANCE_ADRTYPE_GRP_LEVEL,       //!< Addressing Feature on instance group level
  DALI_INSTANCE_ADRTYPE_TYPE_LEVEL,      //!< Addressing Feature on instance type level
  DALI_INSTANCE_ADRTYPE_BROADCAST_LEVEL, //!< Addressing Feature on instance broadcast level
  DALI_INSTANCE_ADRTYPE_BROADCAST,       //!< Addressing Instance broadcast
  DALI_INSTANCE_ADRTYPE_DEVICE_LEVEL,    //!< Addressing Feature on device level
  DALI_INSTANCE_ADRTYPE_DEVICE,          //!< Addressing Device
  DALI_INSTANCE_ADRTYPE_RESERVED
}DALI_INSTANCE_ADRTYPE;

/*! Input device instance types \n
 *  enumeration for control device instance type \n
 *  supported input device types: \n
 *  - INPUT_DEVICE_INST_0 \n
 *  - INPUT_DEVICE_INST_PUSH_BUTTONS \n
 *  - INPUT_DEVICE_INST_OCCUPANCY_SENSOR \n
    - INPUT_DEVICE_INST_LIGHT_SENSOR */
    
typedef enum
{
  //! IEC-62386-103 - Control Devices
  INPUT_DEVICE_INST_0                      = 0,
  //! IEC-62386-301 - Push Buttons
  INPUT_DEVICE_INST_PUSH_BUTTONS           = 1,
  //! IEC-62386-302 - Absolute Input Devices
  INPUT_DEVICE_INST_ABSOLUTE_INPUT_DEVICE  = 2,
  //! IEC-62386-303 - Occupancy Sensor
  INPUT_DEVICE_INST_OCCUPANCY_SENSOR       = 3,
  //! IEC-62386-304 - Light Sensor
  INPUT_DEVICE_INST_LIGHT_SENSOR           = 4,
  //! IEC-62386-305 - Color Sensor
  INPUT_DEVICE_INST_COLOR_SENSOR           = 5
}INPUT_DEVICE_INSTANCE_TYPE;

//! is applied when the stack mode is set as the control device

typedef enum
{
  CTRL_DEV_MODE_NONE = 0,
  /*! Single-master application controller \n
      A single-master application controller is not intended to share \n
      the bus with other control devices. */
  CTRL_DEV_MODE_SINGLE,
  //! Multi-master application controller
  CTRL_DEV_MODE_MULTI,
  //! Input device
  CTRL_DEV_MODE_INPUT
}CTRL_DEVICE_MODE;

typedef struct
{
  DALI_ADRTYPE          adrType;
  uint8_t               adr;     /*!< used, if adrType == DALI_ADRTYPE_SHORT \n
                                      used, value[1..32], if adrType == DALI_ADRTYPE_GRP \n
                                      unused, if adrType == DALI_ADRTYPE_BROADCAST */
  DALI_INSTANCE_ADRTYPE instanceAdrType;
  uint8_t               instance;        /*!< depends on instanceAdrType
                                           [instancenumber, instancegroup, instancetype] */
}device_adr_t;


typedef struct
{
  void*    pMemory;
  uint16_t nSize;
}dalilib_react_pers_memory_t;

/**********************************************************************
 * action codes
 * DALILIB_ACT_XXXX
 *  - action codes from application to DALI-library
 * reaction codes
 * DALILIB_REACT_XXXX
 *  - action codes from DALI-library to application
 **********************************************************************/

//----------------------------------------------------------------------
// control gear action code types
//----------------------------------------------------------------------
/*! To use the "dalilib_action" function, this parameter must be set. 
    This will determine the control gear action the stack will perform. */
typedef enum
{
  /*! action codes, if stack mode equal STACK_MODE_CONTROL_GEAR \n
      internal action codes between application and DALI-Library */
  DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR,
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // action codes, if stack mode equal STACK_MODE_CONTROL_DEVICE
  //----------------------------------------------------------------------
  DALILIB_ACT_TYPE_CTRL_GEAR_LEVEL,
  DALILIB_ACT_TYPE_CTRL_GEAR_SCENE,
  DALILIB_ACT_TYPE_CTRL_GEAR_GROUP,
  /*! Each control gear shall expose its status as a combination of device
   *  properties. The control gear status can be queried using this action
   *  code. \n
   *  The stack will generate DALI forward frames and pass them on to 
   *  HAL-Driver. The answer will be the status, which is formed by a
   *  combination of control gear properties. see ctrl_gear_status_t. */
  DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS,
  DALILIB_ACT_TYPE_CTRL_GEAR_QUERIES,

  // get information about the manufacturer of the control gear
  DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR,
  DALILIB_ACT_TYPE_CTRL_GEAR_CONFIG,
  DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE,
  DALILIB_ACT_TYPE_CTRL_GEAR_MEMORY,

  //! action code for Fluorescent lamps
  DALILIB_ACT_TYPE_CTRL_GEAR_FLUORESCENT,
  //! action codes for LED-Modules
  DALILIB_ACT_TYPE_CTRL_GEAR_LED,
  //! action codes for Colour Control
  DALILIB_ACT_TYPE_CTRL_GEAR_COLOUR,
  //! action codes for Switching-Modules
  DALILIB_ACT_TYPE_CTRL_GEAR_SWITCHING,

  DALILIB_ACT_TYPE_CTRL_GEAR_LAST
}DALILIB_CTRL_GEAR_ACTION_CODE_TYPE;

//----------------------------------------------------------------------
// control gear action codes
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// action codes, if stack mode equal STACK_MODE_CONTROL_GEAR
// internal action codes between application and DALI-Library
//----------------------------------------------------------------------
/*! If a control gear detects status changes, the action codes can
 *  transfer the status to stack. \n
 *  No DALI forward frames are sent with the internal action codes.
    Only the internal status is changed. */
typedef enum
{
  //! application can request the lamp failure variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAMPFAILURE,
  //! application can set the lamp failure variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAMPFAILURE,
  //! application can request the actual level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_ACTUAL_LEVEL,
  //! application transmits the actual level to the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
  //! application transmits raw value of the actual level to the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL_RAW_VALUE,
  //! application can request the gear failure variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_GEAR_FAILURE,
  //! application can set the control gear failure variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_GEAR_FAILURE,
  //! application can request the power on level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_POWER_ON_LEVEL,
  //! application can set the power on level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_POWER_ON_LEVEL,
  //! application can request the system failure level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SYSTEM_FAILURE_LEVEL,
  //! application can set the system failure level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SYSTEM_FAILURE_LEVEL,
  //! application can request the last light level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAST_LIGHT_LEVEL,
  //! application can set the last light level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAST_LIGHT_LEVEL,
  //! application can request the target level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_TARGET_LEVEL,
  //! application can set the target level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_TARGET_LEVEL,
  //! application can request the last active level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAST_ACTIVE_LEVEL,
  //! application can set the last active level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAST_ACTIVE_LEVEL,
  //! application can request the high resolution dimming variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_HIGH_RES_DIMMING,
  //! application can set the high resolution dimming variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_HIGH_RES_DIMMING,
  //! application can request the minimum level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_MIN_LEVEL,
  //! application can set the minimum level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_MIN_LEVEL,
  //! application can request the maximum level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_MAX_LEVEL,
  //! application can set the maximum level variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_MAX_LEVEL,
  //! application can request the fade rate variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_RATE,
  //! application can set the fade rate variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_RATE,
  //! application can request the fade time variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_TIME,
  //! application can set the fade time variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_TIME,
  //! application can request the extended fade time base variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_BASE,
  //! application can set the extended fade time base variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_EXT_FADE_TIME_BASE,
  //! application can request the extended fade time multiple variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_MULTIPLE,
  //! application can set the extended fade time multiple variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_EXT_FADE_TIME_MULTIPLE,
  //! application can request the operating mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_OPERATING_MODE,
  //! application can set the operating mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_OPERATING_MODE,
  //! application can request the short address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SHORT_ADDRESS,
  //! application can set the short address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SHORT_ADDRESS,
  //! application can request the random address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_RANDOM_ADDRESS,
  //! application can set the random address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_RANDOM_ADDRESS,
  //! application can request the search address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SEARCH_ADDRESS,
  //! application can set the search address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SEARCH_ADDRESS,
  //! application can request the gear groups variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_GEAR_GROUPS,
  //! application can set the gear groups variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_GEAR_GROUPS,
  //! application can request the scene variable with the given scene index of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SCENE,
  //! application can set the scene array of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SCENE,
  //! application can request the initialisation state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_INITIALISATION_STATE,
  //! application can set the initialisation state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_INITIALISATION_STATE,
  //! application can request the write enable state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_WRITE_ENABLE_STATE,
  //! application can set the write enable state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_WRITE_ENABLE_STATE,
  //! application can request the limit error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LIMIT_ERROR,
  //! application can set the limit error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LIMIT_ERROR,
  //! application can request the fade running variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_RUNNING,
  //! application can set the fade running variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_RUNNING,
  //! application can request the reset state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_RESET_STATE,
  //! application can set the reset state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_RESET_STATE,
  //! application can request the power cycle seen variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_POWER_CYCLE_SEEN,
  //! application can set the power cycle seen variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_POWER_CYCLE_SEEN,
  //! application can request the DTR0 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR0,
  //! application can set the DTR0 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR0,
  //! application can request the DTR1 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR1,
  //! application can set the DTR1 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR1,
  //! application can request the DTR2 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR2,
  //! application can set the DTR2 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR2,
  //! application can request the enable device type variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_ENABLE_DEVICE_TYPE,
  //! application can set the enable device type variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ENABLE_DEVICE_TYPE,
  //! application can request the LED fast fade time variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_FAST_FADE_TIME,
  //! application can set the LED fast fade time variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_FAST_FADE_TIME,
  //! application can request the LED dimming curve variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_DIMMING_CURVE,
  //! application can set the LED dimming curve variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_DIMMING_CURVE,
  //! application can request the LED failure status variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_FAILURE_STATUS,
  //! application can set the LED failure status variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_FAILURE_STATUS,
  //! application can request the LED reference value variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_REFERENCE_VALUE,
  //! application can set the LED reference value after measurement
  DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_REFERENCE_VALUE,
  //! application can request the Switching reference value variable of the library
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SWITCHING_GET_REFERENCE_VALUE,
  //! application can set the Switching reference value after measurement
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SWITCHING_REFERENCE_VALUE,
  //! application can initiate saving persistent variables to NVM
  DALILIB_ACT_INTERNAL_CTRL_GEAR_SAVE_PERSISTENT,
  //! application can stop Colour calibration
  DALILIB_ACT_INTERNAL_CTRL_GEAR_COLOUR_STOP_CALIBRATION,
  /*! application can write a memory cell \n
      The cell must be of memory type RAM-RO, RAM-RW, NVM-RO or NVM-RW. \n
      The application provides the required parameters in memoryParams of
      the action. \n
      See Di4 extensions (DALI Parts 250, 251, 252, 253, 351) for further
      information. */
  DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL,
  /*! application can request the default values of persistent memory stored
      in a buffer. Triggers a gear reaction of type
      DALILIB_REACT_INTERNAL_CTRL_GEAR_DEFAULT_PERS_MEMORY. */
  DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DEFAULT_PERS_MEMORY
}DALILIB_ACT_INTERNAL_CTRL_GEAR;

/*! If a application controller (control device) wants to control the
 *  level of a control gear, it can perform these action codes.
 *  The stack will generate DALI forward frames and pass them on 
    to HAL-Driver. */
typedef enum
{
  //! direct arc power control
  DALILIB_ACT_CTRL_GEAR_LEVEL_DAPC = 0,
  //! direct arc power control raw value
  DALILIB_ACT_CTRL_GEAR_LEVEL_DAPC_RAW_VALUE,
  DALILIB_ACT_CTRL_GEAR_LEVEL_OFF,
  DALILIB_ACT_CTRL_GEAR_LEVEL_UP,
  DALILIB_ACT_CTRL_GEAR_LEVEL_DOWN,
  DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_UP,
  DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_DOWN,
  DALILIB_ACT_CTRL_GEAR_LEVEL_RECALL_MAX_LEVEL,
  DALILIB_ACT_CTRL_GEAR_LEVEL_RECALL_MIN_LEVEL,
  DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_DOWN_AND_OFF,
  DALILIB_ACT_CTRL_GEAR_LEVEL_ON_STEP_UP,
  DALILIB_ACT_CTRL_GEAR_LEVEL_GOTO_LAST_ACTIVE_LEVEL
}DALILIB_ACT_CTRL_GEAR_LEVEL;

/*! If a application controller (control device) wants to manage
 *  the scene of a control gear, it can perform these action codes.
 *  The stack will generate DALI forward frames and pass them on 
    to HAL-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_SCENE_GOTO_SCENE = 0,
  DALILIB_ACT_CTRL_GEAR_SCENE_SET_SCENE,
  DALILIB_ACT_CTRL_GEAR_SCENE_REMOVE_FROM_SCENE
}DALILIB_ACT_CTRL_GEAR_SCENE;

/*! If a application controller (control device) wants to manage
 *  the groups of a control gear, it can perform these action codes.
 *  The stack will generate DALI forward frames and pass them on 
    to HAL-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_GROUP_ADD_TO_GROUP = 0,
  DALILIB_ACT_CTRL_GEAR_GROUP_REMOVE_FROM_GROUP
}DALILIB_ACT_CTRL_GEAR_GROUP;

/*! Queries are used to retrieve property values from a control gear.
 *  The addressed control gear  returns the queried property value in
 *  a backward frame. \n
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_PRESENT = 0,
  DALILIB_ACT_CTRL_GEAR_QUERY_MISSING_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_VERSION_NR,
  DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR0,
  DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_DEVICE_TYPE,
  DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_PHYSICAL_MINIMUM,
  DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR1,
  DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR2,
  DALILIB_ACT_CTRL_GEAR_QUERY_OPERATING_MODE,
  DALILIB_ACT_CTRL_GEAR_QUERY_LIGHT_SOURCE_TYPE,
  DALILIB_ACT_CTRL_GEAR_QUERY_FADE_TIME_RATE,
  DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_NEXT_DEVICE_TYPE,
  DALILIB_ACT_CTRL_GEAR_QUERY_EXT_FADE_TIME,
  DALILIB_ACT_CTRL_GEAR_QUERY_ACTUAL_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_MAX_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_MIN_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_POWER_ON_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_SYSTEM_FAILURE_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_SCENE_LEVEL,
  DALILIB_ACT_CTRL_GEAR_QUERY_GROUP,
  DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_H,
  DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_M,
  DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_L
}DALILIB_ACT_CTRL_GEAR_QUERIES;

/*! With these action codes, information about the manufacturer can
 *  be requested from a control gear. \n
 *  The stack will generate DALI forward frames and pass them to
    Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_VENDOR_GTIN = 0,
  DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MAJOR,
  DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MINOR,
  DALILIB_ACT_CTRL_GEAR_VENDOR_IDENTIFY_NR,
  DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MAJOR,
  DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MINOR,
  DALILIB_ACT_CTRL_GEAR_VENDOR_DALI_NORM_VERSION
}DALILIB_ACT_CTRL_GEAR_VENDOR;

/*! Device configuration instructions are used to change the
 *  configuration and/or the mode of operation of the control gear. \n
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_CONFIG_RESET = 0,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SAVE_PERS_VARIABLES,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_OPERATING_MODE,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_MAX_LEVEL,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_MIN_LEVEL,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_SYSTEM_FAILURE_LEVEL,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_POWER_ON_LEVEL,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_FADE_TIME,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_FADE_RATE,
  DALILIB_ACT_CTRL_GEAR_CONFIG_SET_EXT_FADE_TIME,
  DALILIB_ACT_CTRL_GEAR_RESET_MEMORY_BANK
}DALILIB_ACT_CTRL_GEAR_CONFIG;

/*! Device initiliaze instructions are used to change the configuration
 *  and/or the mode of operation of the control gear. \n
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_INIT_TERMINATE = 0,
  DALILIB_ACT_CTRL_GEAR_INIT_INITIALISE,
  DALILIB_ACT_CTRL_GEAR_INIT_RANDOMISE,
  DALILIB_ACT_CTRL_GEAR_INIT_WITHDRAW,
  DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_H,
  DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_M,
  DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_L,
  DALILIB_ACT_CTRL_GEAR_INIT_SET_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_GEAR_INIT_PROG_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_GEAR_INIT_ENABLE_DEVICE_TYPE,
  DALILIB_ACT_CTRL_GEAR_INIT_IDENTIFY_DEVICE,
  // reaction code for next actions DALILIB_REACT_CTRL_GEAR_INITIALISE
  DALILIB_ACT_CTRL_GEAR_INIT_COMPARE,
  DALILIB_ACT_CTRL_GEAR_INIT_VERIFY_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_GEAR_INIT_QUERY_SHORT_ADDRESS
}DALILIB_ACT_CTRL_GEAR_INITIALISE;

/*! Determines which control gear must be initialized. */
typedef enum
{
  DALILIB_CTRL_GEAR_INIT_WITH_SHORTADDRESS = 0,
  DALILIB_CTRL_GEAR_INIT_WITHOUT_SHORTADDRESS,
  DALILIB_CTRL_GEAR_INIT_ALL,
  DALILIB_CTRL_GEAR_NONE
}DALILIB_CTRL_GEAR_INITIALISE_VALUE;

/*! This is used to specify which storage action is to be performed. \n
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_READ_MEMORY_CELL = 0,
  DALILIB_ACT_CTRL_GEAR_READ_NEXT_MEMORY_CELL,
  DALILIB_ACT_CTRL_GEAR_WRITE_MEMORY_CELL,
  DALILIB_ACT_CTRL_GEAR_WRITE_NEXT_MEMORY_CELL
}DALILIB_ACT_CTRL_GEAR_MEMORY;

/*! The LED actions are defined here. \n
 *  The stack will generate DALI forward frames for the LED modules
 *  and pass them on to Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_LED_REFERENCE_SYSTEM_POWER,
  DALILIB_ACT_CTRL_GEAR_LED_ENABLE_CURRENT_PROTECTOR,
  DALILIB_ACT_CTRL_GEAR_LED_DISABLE_CURRENT_PROTECTOR,
  DALILIB_ACT_CTRL_GEAR_LED_SELECT_DIMMING_CURVE,
  DALILIB_ACT_CTRL_GEAR_LED_SET_FAST_FADE_TIME,

  // reaction code for next actions DALILIB_REACT_CTRL_GEAR_LED
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_GEAR_TYPE,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_DIMMING_CURVE,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_POSSIBLE_OPERATING_MODES,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_FEATURES,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_FAILURE_STATUS,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_REFERENCE_RUNNING,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_PROTECTOR_ENABLED,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_OPERATING_MODE,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_FAST_FADE_TIME,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_MIN_FAST_FADE_TIME,
  DALILIB_ACT_CTRL_GEAR_LED_QUERY_EXT_VERSION_NUMBER,
}DALILIB_ACT_CTRL_GEAR_LED;

/*! The Colour Control actions are defined here. \n
 *  The stack will generate DALI forward frames for Colour Control
 *  and pass them on to Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_X_COORDINATE,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_Y_COORDINATE,
  DALILIB_ACT_CTRL_GEAR_COLOUR_ACTIVATE,
  DALILIB_ACT_CTRL_GEAR_COLOUR_X_COORDINATE_STEP_UP,
  DALILIB_ACT_CTRL_GEAR_COLOUR_X_COORDINATE_STEP_DOWN,
  DALILIB_ACT_CTRL_GEAR_COLOUR_Y_COORDINATE_STEP_UP,
  DALILIB_ACT_CTRL_GEAR_COLOUR_Y_COORDINATE_STEP_DOWN,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_COLOUR_TEMPERATURE,
  DALILIB_ACT_CTRL_GEAR_COLOUR_TEMPERATURE_STEP_COOLER,
  DALILIB_ACT_CTRL_GEAR_COLOUR_TEMPERATURE_STEP_WARMER,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_PRIMARY_N_DIMLEVEL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_RGB_DIMLEVEL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_WAF_DIMLEVEL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_RGBWAF_CONTROL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_COPY_REPORT_TO_TEMP,
  DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_TY_PRIMARY_N,
  DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_XY_COORDINATE_PRIMARY_N,
  DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_TEMPERATURE_LIMIT,
  DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_GEAR_FEATURES_STATUS,
  DALILIB_ACT_CTRL_GEAR_COLOUR_ASSIGN_COLOUR_TO_LINKED_CHANNEL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_START_AUTO_CALIBRATION,

    // reaction code for next actions DALILIB_REACT_CTRL_GEAR_COLOUR
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_GEAR_FEATURES_STATUS,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_STATUS,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_TYPE_FEATURES,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_VALUE,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_RGBWAF_CONTROL,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_ASSIGNED_COLOUR,
  DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_EXT_VERSION_NUMBER
}DALILIB_ACT_CTRL_GEAR_COLOUR;

/*! The Swichting function actions are defined here. \n
 *  The stack will generate DALI forward frames for the Swichting 
    function and pass them on to Low-Level-Driver. */
typedef enum
{
  DALILIB_ACT_CTRL_GEAR_SWITCHING_REFERENCE_SYSTEM_POWER,                 //!< Command 224
  DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD,    //!< Command 225
  DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD,   //!< Command 226
  DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD,  //!< Command 227
  DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD, //!< Command 228
  DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME,       //!< Command 229

  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_FEATURES,                         //!< Command 240
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_SWITCH_STATUS,                    //!< Command 241
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD,           //!< Command 242
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD,          //!< Command 243
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD,         //!< Command 244
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD,        //!< Command 245
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_ERROR_HOLD_OFF_TIME,              //!< Command 246
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_GEAR_TYPE,                        //!< Command 247
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_REFERENCE_RUNNING,                //!< Command 249
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED,     //!< Command 250
  DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_EXTENDED_VERSION_NUMBER,          //!< Command 255
}DALILIB_ACT_CTRL_GEAR_SWITCHING;

/*! The fluorescent actions are defined here. \n
  *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver. */
typedef enum
{
  // reaction code for this action DALILIB_REACT_CTRL_GEAR_FLUORESCENT
  DALILIB_ACT_CTRL_GEAR_FLUORESCENT_QUERY_EXT_VERSION_NUMBER,
}DALILIB_ACT_CTRL_GEAR_FLUORESCENT;

//----------------------------------------------------------------------
// control gear reaction codes
//----------------------------------------------------------------------
typedef enum
{
  //! reserved reaction code
  DALILIB_REACT_CTRL_GEAR_NONE = 0,

  //----------------------------------------------------------------------
  // reaction codes, if stack mode equal STACK_MODE_CONTROL_GEAR
  // internal action codes between application and DALI-Library
  //----------------------------------------------------------------------
  /*! DALI-library can query the application for a lamp failure.
      The application has to use DALILIB_ACT_CTRL_GEAR_SET_LAMPFAILURE as
      answer code */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_LAMPFAILURE,
  //! DALI-library transmit off command to the application
  DALILIB_REACT_INTERNAL_CTRL_GEAR_OFF,
  //! DALI-library transmit the actual level to the application
  DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
  /*! DALI-library can query the application for the actual level.
      The application has to use DALILIB_ACT_CTRL_GEAR_SET_ACTUAL_LEVEL 
      as answer code. */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_ACTUAL_LEVEL,
  /*! DALI-library can query the application for the gear status. The 
      application has to use DALILIB_ACT_CTRL_GEAR_SET_GEAR_FAILURE 
      as answer code. */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_GEAR_FAILURE,
  /*! DALI-Library notifies the application to start the LED reference
      measurement */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_START,
  /*! DALI-Library notifies the application to stop the reference 
      measurement after 15 minute */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_STOP,
  /*! DALI-Library notifies the application to start Colour Control
      calibration */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_COLOUR_CALIBRATION_START,
  /*! DALI-Library notifies the application to stop Colour Control
      calibration */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_COLOUR_CALIBRATION_STOP,
  /*! DALI-Library notifies the application about a value change of a memory cell
      The application provides the required parameters in memoryParams of
      the action. \n */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_MEMORY_CELL_CHANGED,
  /*! DALI-Library passes a buffer holding the default values of persistent memory.
     The application may trigger this reaction with the internal action
     DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DEFAULT_PERS_MEMORY. \n */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_DEFAULT_PERS_MEMORY,
  /*! DALI-Library responds to the application requesting the lamp failure variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAMPFAILURE,
  /*! DALI-Library responds to the application requesting the actual level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_ACTUAL_LEVEL,
  /*! DALI-Library responds to the application requesting the control gear failure variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_GEAR_FAILURE,
  /*! DALI-Library responds to the application requesting the power on level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_POWER_ON_LEVEL,
  /*! DALI-Library responds to the application requesting the system failure level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SYSTEM_FAILURE_LEVEL,
  /*! DALI-Library responds to the application requesting the last light level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_LIGHT_LEVEL,
  /*! DALI-Library responds to the application requesting the target level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_TARGET_LEVEL,
  /*! DALI-Library responds to the application requesting the last active level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_ACTIVE_LEVEL,
  /*! DALI-Library responds to the application requesting the high resolution dimming variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_HIGH_RES_DIMMING,
  /*! DALI-Library responds to the application requesting the minimum level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MIN_LEVEL,
  /*! DALI-Library responds to the application requesting the maximum level variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MAX_LEVEL,
  /*! DALI-Library responds to the application requesting the fade rate variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_RATE,
  /*! DALI-Library responds to the application requesting the fade time variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_TIME,
  /*! DALI-Library responds to the application requesting the extended fade time base variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_BASE,
  /*! DALI-Library responds to the application requesting the extended fade time multiple variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_MULTIPLE,
  /*! DALI-Library responds to the application requesting the operating mode variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_OPERATING_MODE,
  /*! DALI-Library responds to the application requesting the short address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SHORT_ADDRESS,
  /*! DALI-Library responds to the application requesting the random address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_RANDOM_ADDRESS,
  /*! DALI-Library responds to the application requesting the search address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SEARCH_ADDRESS,
  /*! DALI-Library responds to the application requesting the gear groups variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_GEAR_GROUPS,
  /*! DALI-Library responds to the application requesting the scene variable with the given scene index of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SCENE,
  /*! DALI-Library responds to the application requesting the initialisation state variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_INITIALISATION_STATE,
  /*! DALI-Library responds to the application requesting the write enable state variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_WRITE_ENABLE_STATE,
  /*! DALI-Library responds to the application requesting the limit error variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LIMIT_ERROR,
  /*! DALI-Library responds to the application requesting the fade running variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_RUNNING,
  /*! DALI-Library responds to the application requesting the reset state variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_RESET_STATE,
  /*! DALI-Library responds to the application requesting the power cycle seen variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_POWER_CYCLE_SEEN,
  /*! DALI-Library responds to the application requesting the DTR0 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR0,
  /*! DALI-Library responds to the application requesting the DTR1 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR1,
  /*! DALI-Library responds to the application requesting the DTR2 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR2,
  /*! DALI-Library responds to the application requesting the enable device type variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_ENABLE_DEVICE_TYPE,
  /*! DALI-Library responds to the application requesting the LED fast fade time variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_FAST_FADE_TIME,
  /*! DALI-Library responds to the application requesting the LED dimming curve variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_DIMMING_CURVE,
  /*! DALI-Library responds to the application requesting the LED failure status variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_FAILURE_STATUS,
  /*! DALI-Library responds to the application requesting the LED reference value variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_REFERENCE_VALUE,
  /*! DALI-Library responds to the application requesting the Switching reference value variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_GEAR_SWITCHNG_GET_REFERENCE_VALUE,
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // reaction codes, if stack mode equal STACK_MODE_CONTROL_DEVICE
  //----------------------------------------------------------------------
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_LEVEL with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_LEVEL,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_SCENE with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_SCENE,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_GROUP with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_GROUP,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_QUERY_STATUS,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_QUERIES with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_QUERIES,
  /*! stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_VENDOR with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_VENDOR,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_CONFIG with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_CONFIG,
  /*! Stack notifies the application of a reaction to the request
      DALILIB_ACT_CTRL_GEAR_INITIALISE with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_INITIALISE,
  /*! Stack notifies the application of a reaction to the requests
      DALILIB_ACT_CTRL_GEAR_MEMORY with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_MEMORY,
  /*! Stack notifies the application of a reaction to the requests
      DALILIB_ACT_CTRL_GEAR_LED with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_LED,
  /*! Stack notifies the application of a reaction to the requests
      DALILIB_ACT_CTRL_GEAR_COLOUR with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_COLOUR,
  /*! Stack notifies the application of a reaction to the requests
      DALILIB_ACT_CTRL_GEAR_SWITCHING with this reaction code. */
  DALILIB_REACT_CTRL_GEAR_SWITCHING,
  /*! Stack notifies the application of the response to the requests 
      DALILIB_ACT_CTRL_GEAR_FLUORESCENT with this response code. */
  DALILIB_REACT_CTRL_GEAR_FLUORESCENT,

  DALILIB_REACT_CTRL_GEAR_LAST
}DALILIB_CTRL_GEAR_REACTION_CODES;

/*! Control gear scene action struct contains following information: \n
 *  - scene index [0..15] \n
 *  - scene value [0%..100%] \n
 *  scene value is multiplied by the factor 1000 to represent the decimal places \n
    e.g. 2,521% => 2,521 * 1000 = 2521*/
typedef struct
{
  uint8_t   sceneIndex; //!< index start 0
  uint32_t  sceneLevel;
}dalilib_act_ctrl_gear_scene_t;

/*! Control gear memory action struct contains following information:
 *  - memory bank number \n
 *  - memory cell index \n
    - value for the selected cell, used for write memory action */
typedef struct
{
  uint8_t  memoryBankNr;
  uint8_t  memoryCellIdx;
  uint8_t  value;
}dalilib_act_ctrl_gear_memory_t;

/*! control gear action struct contains following information: \n
 *   - control gear action type \n
 *   - control gear address \n
 *   - control gear action \n
 *   - control gear action value */
 
typedef struct
{
  DALILIB_CTRL_GEAR_ACTION_CODE_TYPE  gearActionType;
  gear_adr_t                          adr;
  union
  {
    uint8_t                           reserved;
    DALILIB_ACT_INTERNAL_CTRL_GEAR    internalAction;
    DALILIB_ACT_CTRL_GEAR_CONFIG      configAction;
    DALILIB_ACT_CTRL_GEAR_QUERIES     queriesAction;
    DALILIB_ACT_CTRL_GEAR_LEVEL       levelAction;
    DALILIB_ACT_CTRL_GEAR_SCENE       sceneAction;
    DALILIB_ACT_CTRL_GEAR_GROUP       groupAction;
    DALILIB_ACT_CTRL_GEAR_INITIALISE  initAction;
    DALILIB_ACT_CTRL_GEAR_VENDOR      vendorAction;
    DALILIB_ACT_CTRL_GEAR_MEMORY      memoryAction;
    DALILIB_ACT_CTRL_GEAR_LED         ledAction;
    DALILIB_ACT_CTRL_GEAR_COLOUR      colourAction;
    DALILIB_ACT_CTRL_GEAR_SWITCHING   switchingAction;
    DALILIB_ACT_CTRL_GEAR_FLUORESCENT fluorescentAction;
  }action;
  union
  {
    /*! action value depends on the action type \n
        if it is a light level value [0%..100%],
        it is multiplied by the factor 1000 to represent
        the decimal places \n
        e.g. 2,521% => 2,521 * 1000 = 2521 */
    uint32_t                       actionValue;
    /*! action raw value is valid only, if internalAction is
        DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL_RAW_VALUE
        or levelAction is DALILIB_ACT_CTRL_GEAR_LEVEL_DAPC_RAW_VALUE.
        It is set to the raw light level value. */
    uint8_t                        rawValue;
    dalilib_act_ctrl_gear_scene_t  sceneParams;
    dalilib_act_ctrl_gear_memory_t memoryParams;
  }value;
}dalilib_act_ctrl_gear_t;

/*! DALILIB_REACT_CTRL_GEAR_QUERY_STATUS*/
typedef struct
{
  uint8_t controlGearFailure; //!< [FALSE|TRUE]
  uint8_t lampFailure;        //!< [FALSE|TRUE]
  uint8_t lampOn;             //!< [FALSE|TRUE]
  uint8_t limitError;         //!< [FALSE|TRUE]
  uint8_t fadeRunning;        //!< [FALSE|TRUE]
  uint8_t resetState;         //!< [FALSE|TRUE]
  uint8_t shortAddress;       //!< [FALSE|TRUE]
  uint8_t powerCycleSeen;     //!< [FALSE|TRUE]
}ctrl_gear_status_t;

/*! Control gear reaction struct contains following information: \n
 *  - control gear reaction code \n
 *  - response value type \n
 *  - control gear reaction \n
    - control gear reaction value */
typedef struct
{
  DALILIB_CTRL_GEAR_REACTION_CODES reactionCode;
  DALILIB_RESPONSE_VALUE_TYPE      valueType;
  union
  {
    struct
    {
      /*! reaction value depends on the reaction type \n
          if it is a light level value [0%..100%], it is
          multiplied by the factor 1000 to represent the decimal
          places \n
          e.g. 2,521% => 2,521 * 1000 = 2521 */
      uint32_t reactValue;
      /*! reaction raw value depends on the reaction type \n
          if reaction value is a light level value [0%..100%],
          it is set to the raw light level value. */
      uint8_t  reactRawValue;
    };
    /*! the memory value contains information about a value change
        of a memory cell. \n
        gearReactionType: DALILIB_REACT_INTERNAL_CTRL_GEAR_MEMORY_CELL_CHANGED \n
        will be set. */
    dalilib_act_ctrl_gear_memory_t memoryValue;
    /*! contains a pointer to the buffer data of persistent memory and it's size. \n
        gearReactionType: DALILIB_REACT_INTERNAL_CTRL_GEAR_DEFAULT_PERS_MEMORY, \n
        will be set. */
    dalilib_react_pers_memory_t persMemory;
    /*! the manufacturer's global trade item number is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_GTIN \n 
        will be executed. */
    uint8_t gtin[6];
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MAJOR \n
        will be executed.*/
    uint8_t firmware_ver_major;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MINOR \n
        will be executed.*/
    uint8_t firmware_ver_minor;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_IDENTIFY_NR \n
        will be executed.*/
    uint8_t identify_number[8];
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MAJOR \n
        will be executed.*/
    uint8_t hardware_ver_major;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MINOR \n
        will be executed.*/
    uint8_t hardware_ver_minor;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_DALI_NORM_VERSION \n
        will be executed.*/
    uint8_t dali_norm_version;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS
        will be executed.*/
    ctrl_gear_status_t gearStatus;
  };
}dalilib_react_ctrl_gear_t;

/**********************************************************************
 * control device structs/enums/defines
 **********************************************************************/

//----------------------------------------------------------------------
// control device action codes
//----------------------------------------------------------------------
/*! To use the "dalilib_action" function, this parameter must be set.
 *  This will determine the control device action the stack will 
    perform. */
typedef enum
{
    //! action codes only between application and DALI-Library
    DALILIB_ACT_TYPE_INTERNAL_CTRL_DEVICE = 0,
    //! action codes for the other application controller devices
    DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER,

    //! action codes for the input devices
    DALILIB_ACT_TYPE_INPUT_DEVICE,

    //! input device actions for particular device push button
    DALILIB_ACT_TYPE_PUSH_BUTTON,

    //! input device actions for particular device occupancy sensor
    DALILIB_ACT_TYPE_OCCUPANCY_SENSOR,

    //! input device actions for particular device light sensor
    DALILIB_ACT_TYPE_LIGHT_SENSOR,

    DALILIB_ACT_TYPE_CTRL_DEV_LAST
}DALILIB_CTRL_DEVICE_ACTION_CODE_TYPE;

//----------------------------------------------------------------------
// internal control device action codes
//----------------------------------------------------------------------
/*! If a control device detects status changes, the action codes can 
 *  transfer the status to stack. \n
 *  No DALI forward frames are sent with the internal action codes.
    Only the internal status is changed. */
    
typedef enum
{
  // action codes for a input device (Push Buttons)
  DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_SHORT_TIMER = 0,
  DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_DOUBLE_TIMER,
  DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_REPEAT_TIMER,
  DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_STUCK_TIMER,

  // action codes for input device (Light Sensor)
  DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_REPORT_TIMER,
  DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_HYSTERESIS,
  DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_DEADTIME,
  DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_HYSTERESIS_MIN,

  // action codes for input device (Occupancy Sensor)
  DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_HOLD_TIMER,
  DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_REPORT_TIMER,
  DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_DEADTIME,

  /*! application can write a memory cell \n
      The cell must be of memory type RAM-RO, RAM-RW, NVM-RO or NVM-RW. \n
      The application provides the required parameters in memoryParams of
      the action. \n
      See Di4 extensions (DALI Parts 250, 251, 252, 253, 351) for further
      information. */
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_WRITE_MEMORY_CELL,
  /*! application can request the default values of persistent memory stored
      in a buffer. Triggers a device reaction of type
      DALILIB_REACT_INTERNAL_CTRL_DEVICE_DEFAULT_PERS_MEMORY. */
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DEFAULT_PERS_MEMORY,
  //! application can request the short address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_SHORT_ADDRESS,
  //! application can set the short address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_SHORT_ADDRESS,
  //! application can request the device groups variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DEVICE_GROUPS,
  //! application can set the device groups variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DEVICE_GROUPS,
  //! application can request the random address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_RANDOM_ADDRESS,
  //! application can set the random address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_RANDOM_ADDRESS,
  //! application can request the operating mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_OPERATING_MODE,
  //! application can set the operating mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_OPERATING_MODE,
  //! application can request the application active variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_ACTIVE,
  //! application can set the application active variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_APPLICATION_ACTIVE,
  //! application can request the power cycle notification variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_NOTIFICATION,
  //! application can set the power cycle notification variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_POWER_CYCLE_NOTIFICATION,
  //! application can request the search address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_SEARCH_ADDRESS,
  //! application can set the search address variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_SEARCH_ADDRESS,
  //! application can request the DTR0 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR0,
  //! application can set the DTR0 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR0,
  //! application can request the DTR1 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR1,
  //! application can set the DTR1 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR1,
  //! application can request the DTR2 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR2,
  //! application can set the DTR2 variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR2,
  //! application can request the quiescent mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_QUIESCENT_MODE,
  //! application can set the quiescent mode variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_QUIESCENT_MODE,
  //! application can request the write enable state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_WRITE_ENABLE_STATE,
  //! application can set the write enable state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_WRITE_ENABLE_STATE,
  //! application can request the power cycle seen variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_SEEN,
  //! application can set the power cycle seen variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_POWER_CYCLE_SEEN,
  //! application can request the initialisation state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_INITIALISATION_STATE,
  //! application can set the initialisation state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_INITIALISATION_STATE,
  //! application can request the application controller error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_CONTROLLER_ERROR,
  //! application can set the application controller error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_APPLICATION_CONTROLLER_ERROR,
  //! application can request the input device error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_INPUT_DEVICE_ERROR,
  //! application can set the input device error variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_INPUT_DEVICE_ERROR,
  //! application can request the reset state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_RESET_STATE,
  //! application can set the reset state variable of the library
  DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_RESET_STATE
}DALILIB_ACT_INTERNAL_CTRL_DEVICE;

//----------------------------------------------------------------------
// control device action codes
//----------------------------------------------------------------------
/*! With these action codes other control devices can be managed. To
 *  implement these action codes, the stack should be configured as 
 *  multi-master application controller (CTRL_DEV_MODE_MULTI). \n 
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver */
typedef enum
{
  // initialise action codes
  // reaction code for next actions DALILIB_REACT_CTRL_DEVICE_INITIALISE
  DALILIB_ACT_CTRL_DEVICE_INIT_TERMINATE = 0,
  DALILIB_ACT_CTRL_DEVICE_INIT_INITIALISE,
  DALILIB_ACT_CTRL_DEVICE_INIT_RANDOMISE,
  DALILIB_ACT_CTRL_DEVICE_INIT_WITHDRAW,
  DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_H,
  DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_M,
  DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_L,
  DALILIB_ACT_CTRL_DEVICE_INIT_PROG_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_DEVICE_INIT_COMPARE,
  DALILIB_ACT_CTRL_DEVICE_INIT_VERIFY_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_DEVICE_INIT_QUERY_SHORT_ADDRESS,

  // configuration action codes
  // reaction code: DALILIB_REACT_CTRL_DEVICE_CONFIG
  DALILIB_ACT_CTRL_DEVICE_RESET,
  DALILIB_ACT_CTRL_DEVICE_RESET_MEM_BANK,
  DALILIB_ACT_CTRL_DEVICE_RESET_POWER_CYCLE_SEEN,
  DALILIB_ACT_CTRL_DEVICE_SET_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_DEVICE_ENABLE_APPLICATION_CONTROLLER,
  DALILIB_ACT_CTRL_DEVICE_DISABLE_APPLICATION_CONTROLLER,
  DALILIB_ACT_CTRL_DEVICE_SET_OPERATING_MODE,
  DALILIB_ACT_CTRL_DEVICE_ADD_TO_DEVICE_GROUPS,
  DALILIB_ACT_CTRL_DEVICE_REMOVE_FROM_DEVICE_GROUPS,
  DALILIB_ACT_CTRL_DEVICE_START_QUIESCENT_MODE,
  DALILIB_ACT_CTRL_DEVICE_STOP_QUIESCENT_MODE,
  DALILIB_ACT_CTRL_DEVICE_ENABLE_POWER_CYCLE_NOTIFICATION,
  DALILIB_ACT_CTRL_DEVICE_DISABLE_POWER_CYCLE_NOTIFICATION,
  DALILIB_ACT_CTRL_DEVICE_SAVE_PERS_VARIABLES,
  // device queries
  // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS
  DALILIB_ACT_CTRL_DEVICE_QUERY_STATUS,
  // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERIES
  DALILIB_ACT_CTRL_DEVICE_QUERY_MISSING_SHORT_ADDRESS,
  DALILIB_ACT_CTRL_DEVICE_QUERY_VERSION_NUMBER,
  DALILIB_ACT_CTRL_DEVICE_QUERY_NUMBER_OF_INSTANCES,
  DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR0,
  DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR1,
  DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR2,
  DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_H,
  DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_M,
  DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_L,
  DALILIB_ACT_CTRL_DEVICE_QUERY_OPERATING_MODE,
  DALILIB_ACT_CTRL_DEVICE_QUERY_QUIESCENT_MODE,
  DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_GROUPS,
  DALILIB_ACT_CTRL_DEVICE_QUERY_POWER_CYCLE_NOTIFICATION,
  DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_CAPABILITIES,

  // vendor information action codes (memory bank 0)
  // reaction code: DALILIB_REACT_CTRL_DEVICE_VENDOR
  DALILIB_ACT_CTRL_DEVICE_VENDOR_GTIN,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MAJOR,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MINOR,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_IDENTIFY_NR,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MAJOR,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MINOR,
  DALILIB_ACT_CTRL_DEVICE_VENDOR_DALI_NORM_VERSION,

  // memory action codes
  // reaction code: DALILIB_REACT_CTRL_DEVICE_MEMORY
  DALILIB_ACT_CTRL_DEVICE_MEMORY_READ,
  DALILIB_ACT_CTRL_DEVICE_MEMORY_READ_NEXT,
  DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE,
  DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE_NEXT,
}DALILIB_ACT_CTRL_DEVICE_CONTROLLER;

/*! The input device can be managed with these action codes. 
 *  To implement these action codes, the stack should be configured as
 *  multi-master application controller (CTRL_DEV_MODE_MULTI). 
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver */
typedef enum
{
  // instance control instructions
  // reaction code: DALILIB_REACT_CTRL_DEVICE_CONFIG
  DALILIB_ACT_INPUT_DEVICE_SET_EVENT_PRIORITY = 0,
  DALILIB_ACT_INPUT_DEVICE_ENABLE_INSTANCE,
  DALILIB_ACT_INPUT_DEVICE_DISABLE_INSTANCE,
  DALILIB_ACT_INPUT_DEVICE_SET_PRIMARY_GROUP,
  DALILIB_ACT_INPUT_DEVICE_SET_GROUP_1,
  DALILIB_ACT_INPUT_DEVICE_SET_GROUP_2,
  DALILIB_ACT_INPUT_DEVICE_SET_EVENT_SCHEME,
  DALILIB_ACT_INPUT_DEVICE_SET_EVENT_FILTER,
  // instance queries
  // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERIES
  DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_TYPE,
  DALILIB_ACT_INPUT_DEVICE_QUERY_RESOLUTION,
  // reaction code: DALILIB_REACT_CTRL_DEVICE_INSTANCE_STATUS
  DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_STATUS,
  // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERIES
  DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_PRIORITY,
  DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_PRIMARY_GROUP,
  DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_1,
  DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_2,
  DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_SCHEME,
  DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE,
  DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE_LATCH,
  DALILIB_ACT_INPUT_DEVICE_QUERY_FEATURE_TYPE,
  DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_1,
  DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_2,
  DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_3,
}DALILIB_ACT_INPUT_DEVICE;

/*!  The push buttons can be managed with these action codes. \n
 *  To implement these action codes, the stack should be configured as
 *  multi-master application controller (CTRL_DEV_MODE_MULTI). 
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver */
typedef enum
{
  //! Set short timer
  DALILIB_ACT_PUSH_BUTTON_SET_SHORT_TIMER = 0,
  //! Set short double timer
  DALILIB_ACT_PUSH_BUTTON_SET_DOUBLE_TIMER,
  //! Set repeat timer
  DALILIB_ACT_PUSH_BUTTON_SET_REPEAT_TIMER,
  //! Set stuck timer
  DALILIB_ACT_PUSH_BUTTON_SET_STUCK_TIMER,
  //! Query short timer
  DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER,
  //! Query short timer min
  DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER_MIN,
  //! Query short double timer
  DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER,
  //! Query short double timer min
  DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER_MIN,
  //! Query repeat timer
  DALILIB_ACT_PUSH_BUTTON_QUERY_REPEAT_TIMER,
  //! Query stuck timer
  DALILIB_ACT_PUSH_BUTTON_QUERY_STUCK_TIMER,
}DALILIB_ACT_PUSH_BUTTON;

/*! The occupancy sensor can be managed with these action codes. \n
 *  To implement these action codes, the stack should be configured as
 *  multi-master application controller (CTRL_DEV_MODE_MULTI). 
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver */
typedef enum
{
  //! Once a movement detected event has been sent.
  DALILIB_ACT_OCCUPANCY_SENSOR_CATCH_MOVEMENT = 0,
  //! If the hold timer is implemented, shall set the hold timer
  DALILIB_ACT_OCCUPANCY_SENSOR_SET_HOLD_TIMER,
  //! Set the repeat timer
  DALILIB_ACT_OCCUPANCY_SENSOR_SET_REPORT_TIMER,
  //! Set the dead time timer
  DALILIB_ACT_OCCUPANCY_SENSOR_SET_DEADTIME_TIMER,
  //! Cancel hold timer
  DALILIB_ACT_OCCUPANCY_SENSOR_CANCEL_HOLD_TIMER,
  //! Query the dead time timer
  DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_DEADTIME_TIMER,
  //! Query the hold timer
  DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_HOLD_TIMER,
  //! Query the report timer
  DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_REPORT_TIMER,
  //! Query catching status
  DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_CATCHING,
}DALILIB_ACT_OCCUPANCY_SENSOR;

/*! The light sensor can be managed with these action codes. \n
 *  To implement these action codes, the stack should be configured as
 *  multi-master application controller (CTRL_DEV_MODE_MULTI). 
 *  The stack will generate DALI forward frames and pass them to 
    Low-Level-Driver */
typedef enum
{
  //! Set the report timer
  DALILIB_ACT_LIGHT_SENSOR_SET_REPORT_TIMER = 0,
  //! Set hysteresis
  DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS,
  //! Set the dead time timer
  DALILIB_ACT_LIGHT_SENSOR_SET_DEADTIME_TIMER,
  //! Set hysteresis min
  DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS_MIN,
  //! Query the hysteresis
  DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS_MIN,
  //! Query the dead time timer
  DALILIB_ACT_LIGHT_SENSOR_QUERY_DEADTIME_TIMER,
  //! Query the report timer
  DALILIB_ACT_LIGHT_SENSOR_QUERY_REPORT_TIMER,
  //! Query the hysteresis
  DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS,
}DALILIB_ACT_LIGHT_SENSOR;

//----------------------------------------------------------------------
// control device reaction codes
//----------------------------------------------------------------------
typedef enum
{
  DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS = 0,
  DALILIB_REACT_CTRL_DEVICE_CAPABILITIES,
  DALILIB_REACT_CTRL_DEVICE_QUERIES,
  DALILIB_REACT_CTRL_DEVICE_CONFIG,
  DALILIB_REACT_CTRL_DEVICE_INITIALISE,
  DALILIB_REACT_CTRL_DEVICE_VENDOR,
  DALILIB_REACT_CTRL_DEVICE_INSTANCE_STATUS,
  DALILIB_REACT_CTRL_DEVICE_MEMORY,
  DALILIB_REACT_PUSH_BUTTON,
  DALILIB_REACT_OCCUPANCY_SENSOR,
  DALILIB_REACT_LIGHT_SENSOR,

  DALILIB_REACT_EVENT_START,

  //----------------------------------------------------------------------
  // internal action codes between DALI-Library (->) and application
  //----------------------------------------------------------------------
  /*! DALI-Library passes a buffer holding the default values of persistent memory.
     The application may trigger this reaction with the internal action
     DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DEFAULT_PERS_MEMORY. \n */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_DEFAULT_PERS_MEMORY,

  //----------------------------------------------------------------------
  // reaction codes, if stack mode equal STACK_MODE_CONTROL_DEVICE
  // internal action codes between DALI-Library (->) and application
  //----------------------------------------------------------------------
  /*! DALI-Library responds to the application requesting the short address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_SHORT_ADDRESS,
  /*! DALI-Library responds to the application requesting the device groups variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DEVICE_GROUPS,
  /*! DALI-Library responds to the application requesting the random address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_RANDOM_ADDRESS,
  /*! DALI-Library responds to the application requesting the operating mode variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_OPERATING_MODE,
  /*! DALI-Library responds to the application requesting the application active variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_ACTIVE,
  /*! DALI-Library responds to the application requesting the power cycle notification variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_NOTIFICATION,
  /*! DALI-Library responds to the application requesting the search address variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_SEARCH_ADDRESS,
  /*! DALI-Library responds to the application requesting the DTR0 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR0,
  /*! DALI-Library responds to the application requesting the DTR1 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR1,
  /*! DALI-Library responds to the application requesting the DTR2 variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR2,
  /*! DALI-Library responds to the application requesting the quiescent mode variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_QUIESCENT_MODE,
  /*! DALI-Library responds to the application requesting the write enable mode variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_WRITE_ENABLE_STATE,
  /*! DALI-Library responds to the application requesting the power cycle seen variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_SEEN,
  /*! DALI-Library responds to the application requesting the initialisation state variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_INITIALISATION_STATE,
  /*! DALI-Library responds to the application requesting the application controller error variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_CONTROLLER_ERROR,
  /*! DALI-Library responds to the application requesting the input device error variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_INPUT_DEVICE_ERROR,
  /*! DALI-Library responds to the application requesting the reset state variable of the library */
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_RESET_STATE,

  DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_SHORT_TIMER,
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_DOUBLE_TIMER,
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_REPEAT_TIMER,
  DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_STUCK_TIMER,



  // event message reaction codes

  //! The button is released
  DALILIB_REACT_EVENT_PUSH_BUTTON_RELEASED,
  //! The button is pressed
  DALILIB_REACT_EVENT_PUSH_BUTTON_PRESSED,
  //! The button is pressed and released
  DALILIB_REACT_EVENT_PUSH_BUTTON_SHORT_PRESSED,
  //! The button is double pressed
  DALILIB_REACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED,
  //! The button is pressed without releasing it
  DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_START,
  //! The button is still pressed
  DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT,
  //! Following a long press start condition, the button is released
  DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP,
  //! The button has been stuck and is now released
  DALILIB_REACT_EVENT_PUSH_BUTTON_FREE,
  //! The button has been pressed for a very long time and is assumed stuck
  DALILIB_REACT_EVENT_PUSH_BUTTON_STUCK,

  // occupancy sensor reaction codes
  //! No movement detected
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT,
  //! Movement detected.
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT,
  //! The area has become vacant.
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_VACANT,
  //! The area is still vacant.
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_STILL_VACANT,
  //! The area has become occupied.
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_OCCUPIED,
  //! The area is still occupied.
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_STILL_OCCUPIED,
  //! The current event is triggered by a presence based sensor
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_PRESENCE_SENSOR,
  //! The current event is triggered by a movement based sensor
  DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT_SENSOR,

  // light sensor reaction codes
  //! An illuminance level report
  DALILIB_REACT_EVENT_LIGHT_SENSOR_ILLUMINANCE_LEVEL_REPORT,

  //! raw event will be transmitted on application
  DALILIB_REACT_EVENT_UNKNOWN,

  DALILIB_REACT_EVENT_END,
}DALILIB_CTRL_DEVICE_REACTION_CODES;

typedef enum
{
  DALILIB_CTRL_DEVICE_INIT_WITH_SHORTADDRESS = 0,
  DALILIB_CTRL_DEVICE_INIT_WITHOUT_SHORTADDRESS,
  DALILIB_CTRL_DEVICE_INIT_ALL,
  DALILIB_CTRL_DEVICE_INIT_NONE
}DALILIB_CTRL_DEVICE_INITIALISE_VALUE;

/*! Control device memory action struct contains following information:
 *  - memory bank number \n
 *  - emory cell index \n
    - value for the selected cell, used for write memory action */
typedef struct
{
  uint8_t memoryBankNr;
  uint8_t memoryCellIdx;
  uint8_t value;
}dalilib_act_ctrl_device_memory_t;

/*! Control device action structs contains following information:
 *  - control device action type \n
 *  - control device address \n
 *  - control device action \n
    - control device action value */
typedef struct
{
  DALILIB_CTRL_DEVICE_ACTION_CODE_TYPE deviceActionType;
  device_adr_t                         adr;
  //! action code
  union
  {
    uint8_t                            reserved;
    DALILIB_ACT_INTERNAL_CTRL_DEVICE   internalAction;
    DALILIB_ACT_CTRL_DEVICE_CONTROLLER controllerAction;
    DALILIB_ACT_INPUT_DEVICE           inputDeviceAction;
    DALILIB_ACT_PUSH_BUTTON            pushButtonAction;
    DALILIB_ACT_OCCUPANCY_SENSOR       occupancySensorAction;
    DALILIB_ACT_LIGHT_SENSOR           lightSensorAction;
  };
  union
  {
    uint32_t                         actionValue;
    //! used if action code is equal to "memory action code"
    dalilib_act_ctrl_device_memory_t memoryParams;
  };
}dalilib_act_ctrl_device_t;

// DALILIB_REACT_CTRL_DEVICE_INSTANCE_STATUS
typedef struct
{
  uint8_t instanceError;  //!< [FALSE|TRUE]
  uint8_t instanceActive; //!< [FALSE|TRUE]
}ctrl_device_instance_status_t;

// DALILIB_REACT_CTRL_DEVICE_CAPABILITIES
typedef struct
{
  uint8_t applicationCtrlPresent; // [FALSE|TRUE]
  uint8_t numberOfInstance;       // [FALSE|TRUE]
}ctrl_device_capabilities_t;

// DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS
typedef struct
{
  uint8_t inputDeviceError;   // [FALSE|TRUE]
  uint8_t quiescentMode;      // [FALSE|TRUE]
  uint8_t shortAddress;       // [FALSE|TRUE]
  uint8_t applicationActive;  // [FALSE|TRUE]
  uint8_t applicationCtrlErr; // [FALSE|TRUE]
  uint8_t powerCycleSeen;     // [FALSE|TRUE]
  uint8_t resetState;         // [FALSE|TRUE]
}ctrl_device_status_t;

// List of event scheme
/*! An input device instance shall use the selected event source
    addressing scheme as defined in eventscheme table. */
typedef enum
{
  //! Instance addressing, using instance type and number.
  EVENT_SCHEME_INSTANCE = 0,
  //! Device addressing, using short address and instance type.
  EVENT_SCHEME_DEVICE,
  //! Device/Instance addressing, using short address and instance number.
  EVENT_SCHEME_DEVICE_INSTANCE,
  //! Device group addressing, using device group and instance type.
  EVENT_SCHEME_DEVICE_GROUP,
  //! Instance group addressing, using instance group and type.
  EVENT_SCHEME_INSTANCE_GROUP,
  EVENT_SCHEME_RESERVED
}CTRL_DEVICE_EVENT_SCHEME;

/*! Control device reaction struct contains following information: \n
 *  - control device reaction code \n
 *  - response value type \n
 *  - control device reaction \n
 *  - control device address \n
    - control device reaction value */
typedef struct
{
  DALILIB_CTRL_DEVICE_REACTION_CODES reactionCode;
  device_adr_t                       adr;
  DALILIB_RESPONSE_VALUE_TYPE        valueType;
  union
  {
    /*! reaction value is depends on the reaction type \n
        if it is a light level value [0%..100%], it is 
        multiplied by the factor 1000 to represent the decimal
        places \n
        e.g. 2,521% => 2,521 * 1000 = 2521 */
    uint32_t reactValue;
    /*! the manufacturer's global trade item number is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_GTIN \n 
        will be executed. */
    uint8_t  gtin[6];
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MAJOR \n
        will be executed.*/
    uint8_t  firmware_ver_major;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MINOR \n
        will be executed.*/
    uint8_t  firmware_ver_minor;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_IDENTIFY_NR \n
        will be executed.*/
    uint8_t  identify_number[8];
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MAJOR \n
        will be executed.*/
    uint8_t  hardware_ver_major;
    /*! the manufacturer's firmware version is returned
        by a control gear. To request this information, the
        action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MINOR \n
        will be executed.*/
    uint8_t  hardware_ver_minor;
    /*! the DALI version is returned by a control device. 
        To request this information, the action function
        with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER \n
        vendorAction: DALILIB_ACT_CTRL_GEAR_VENDOR_DALI_NORM_VERSION \n
        will be executed.*/
    uint8_t  dali_norm_version;
    /*! the control device status is returned by a control device. 
        To request this information, the action function
        with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER \n
        vendorAction: DALILIB_ACT_CTRL_DEVICE_QUERY_STATUS \n
        will be executed.*/
    ctrl_device_status_t  deviceStatus;
    /*! each control device shall expose its features as a 
        combination of device capabilities. To request this
        information, the action function with the action code \n
        gearActionType: DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER \n
        vendorAction: DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_CAPABILITIES \n
        will be executed.*/
    ctrl_device_capabilities_t  deviceCapabilities;
    /*! each instance shall expose its status as a combination of 
        instance properties. To request this information, the 
        action function with the action code \n 
        deviceActionType: DALILIB_ACT_TYPE_INPUT_DEVICE \n
        inputDeviceAction: DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_STATUS \n
        will be executed. */
    ctrl_device_instance_status_t  deviceInstanceStatus;
    /*! contains a pointer to the buffer data of persistent memory and it's size. \n
        deviceReactionType: DALILIB_REACT_INTERNAL_CTRL_DEVICE_DEFAULT_PERS_MEMORY \n
        will be set. */
    dalilib_react_pers_memory_t persMemory;
  };
}dalilib_react_ctrl_device_t;

// event message types
/*! The input device events are triggered by these action codes.
 *  The stack will generate DALI forward frames and pass them to
    Low-Level-Driver. */
typedef enum
{
  DALILIB_EVENT_TYPE_PUSH_BUTTON,
  DALILIB_EVENT_TYPE_OCCUPANCY_SENSOR,
  DALILIB_EVENT_TYPE_LIGHT_SENSOR
}DALILIB_EVENT_TYPE;

/*! The push buttons events are triggered by these action codes. \n
 *  The stack will generate DALI forward frames and pass them to
    Low-Level-Driver. */
typedef enum
{
  //! The button is released
  DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_RELEASED,
  //! The button is pressed
  DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_PRESSED,
  //! Idle event to give timings a chance, called from timinghelper
  DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_IDLE,
  //! The button is released
  DALILIB_ACT_EVENT_PUSH_BUTTON_RELEASED,
  //! The button is pressed
  DALILIB_ACT_EVENT_PUSH_BUTTON_PRESSED,
  //! The button is pressed and released
  DALILIB_ACT_EVENT_PUSH_BUTTON_SHORT_PRESSED,
  //! The button is double pressed
  DALILIB_ACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED,
  //! The button is pressed without releasing it
  DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_START,
  //! The button is still pressed
  DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT,
  //! Following a long press start condition, the button is released
  DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP,
  //! The button has been stuck and is now released
  DALILIB_ACT_EVENT_PUSH_BUTTON_FREE,
  //! The button has been pressed for a very long time and is assumed stuck
  DALILIB_ACT_EVENT_PUSH_BUTTON_STUCK,
}DALILIB_ACT_EVENT_PUSH_BUTTON;

/*! The occupancy sensor events are triggered by these action codes. \n
 *  The stack will generate DALI forward frames and pass them to
    Low-Level-Driver. */
typedef enum
{
  /*! Area is vacant and no movement detected.
   *  only use for presence sensor
   * */
  DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_VACANT,
  /*! Area is vacant and movement detected.
   *  only use for presence sensor
   * */
  DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_OCCUPIED,
  /*! Area is occupied and no movement detected. */
  DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT,
  /*! Area is occupied and movement detected. */
  DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_MOVEMENT,

  /*! No movement detected. \n
      Corresponding trigger is the 'No movement' trigger
      Reserved: Do not used!
  */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT,
  /*! Movement detected. \n
      Corresponding trigger is the 'Movement' trigger
      Reserved: Do not used!
   */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT,
  /*! The area has become vacant. \n
      Corresponding trigger is the 'Vacant' trigger
    Reserved: Do not used!
   */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_VACANT,
  /*! The area is still vacant. \n
      The event occurs at regular intervals as long as
      the vacant condition holds. \n
      Corresponding trigger is the 'Repeat' trigger
      Reserved: Do not used!
  */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_STILL_VACANT,
  /*! The area has become occupied. \n
      Corresponding trigger is the 'Occupied' trigger
      Reserved: Do not used!
  */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_OCCUPIED,
  /*! The area is still occupied. \n
      The event occurs at regular intervals as long as
      the occupied condition holds. \n
      Corresponding trigger is the 'Repeat' trigger
      Reserved: Do not used!
  */
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_STILL_OCCUPIED,
  //! The current event is triggered by a presence based sensor
  //! Reserved: Do not used!
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_PRESENCE_SENSOR,
  //! The current event is triggered by a movement based sensor
  //! Reserved: Do not used!
  DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT_SENSOR,
}DALILIB_ACT_EVENT_OCCUPANCY_SENSOR;

/*! The light sensor events are triggered by these action codes. \n
 *  The stack will generate DALI forward frames and pass them to
    Low-Level-Driver. */
typedef enum
{
  //! The Illuminance level event.
  DALILIB_ACT_INTERNAL_EVENT_LIGHT_SENSOR_ILLUMINANCE_LEVEL,
}DALILIB_ACT_EVENT_LIGHT_SENSOR;

typedef struct
{
  //! Light sensor events
  DALILIB_ACT_EVENT_LIGHT_SENSOR type;
  //! Range[0..1023] (10 - Bit)
  uint16_t                        illuminanceLevel;
}dalilib_light_sensor_event_t;

/*! This structure must be filled if an input device triggers an event and wants to send this event.
 *  Event action structs struct contains following information: \n
 *  - event type \n
 *  - device short address \n
    - input device event action codes */
typedef struct
{
  DALILIB_EVENT_TYPE type;
  device_adr_t       adr;
  union
  {
    DALILIB_ACT_EVENT_PUSH_BUTTON      pushButtonEvent;
    DALILIB_ACT_EVENT_OCCUPANCY_SENSOR occupancySensorEvent;
    dalilib_light_sensor_event_t       lightSensorEvent;
  };
}dalilib_act_event_t;

/*! Event message reaction struct contains following information: \n
 *  - event source addressing scheme \n
 *  - depending on event scheme the following fields are filled out: \n
 *  -- instance type of the input device \n
 *  -- instance number \n
 *  -- instance group \n
 *  -- short address of the input device \n
    -- group address of the input device */
typedef struct
{
  CTRL_DEVICE_EVENT_SCHEME   eventScheme;
  INPUT_DEVICE_INSTANCE_TYPE instanceType;
  uint8_t                    instanceNumber;
  uint8_t                    instanceGroup;
  uint8_t                    shortAddress;
  uint8_t                    deviceGroup;
}ctrl_device_event_message_t;


/*! Event reaction struct contains following information: \n
 *  - event reaction code, if event source addressing scheme != EVENT_SCHEME_DEVICE_INSTANCE \n
 *  - raw event info, if event source addressing scheme == EVENT_SCHEME_DEVICE_INSTANCE \n
    - event message reaction struct */
typedef struct
{
  union
  {
    DALILIB_CTRL_DEVICE_REACTION_CODES  reactionCode;
    uint16_t                            rawEventInfo; // 10 bit event number and/or event data
  };
  ctrl_device_event_message_t        eventMessage;
}dalilib_react_event_t;

typedef enum
{
  DALILIB_REACT_STATUS_BUSPOWER = 1,
  DALILIB_REACT_STATUS_BUSCOLLISION,
}DALILIB_STATUS_REACTION_CODES;

/*! Status reaction struct contains following information: \n
 *  - status reaction code \n
    - status reaction value */
typedef struct
{
  DALILIB_STATUS_REACTION_CODES reactionCode;
  union
  {
    uint8_t                       reactValue;
    dalilib_buspower_status_t     buspowerValue;
    dalilib_buscollision_status_t buscollisionValue;
  };
}dalilib_react_status_t;

/*! Raw frame action struct contains following information: \n
 *  - frame length in bits \n
 *  - flag indicating if the frame shall be sent twice
    - raw data of the frame */
typedef struct
{
  uint8_t   datalength;
  uint8_t   sendTwice;
  union
  {
    uint64_t    data;
    uint8_t     data_byte[8];
  };
}dalilib_act_raw_frame_t;

typedef enum
{
  DALILIB_REACT_RAW_FRAME_SEND,
  DALILIB_REACT_RAW_FRAME_RECEIVE,
}DALILIB_RAW_FRAME_REACTION_CODES;

/*! Raw frame struct contains following information: \n
 *  - time stamp \n
 *  - frame length in bits \n
    - raw data of the frame */
typedef struct
{
  uint32_t  timestamp; //<! timestamp of last received bit in frame in 100usec ticks \n
  uint8_t   datalength;
  union
  {
    uint64_t    data;
    uint8_t     data_byte[8];
  };
}dalilib_raw_frame_t;

/*! Raw frame reaction struct contains following information: \n
 *  - raw frame reaction code \n
 *  - response value type \n
    - raw frame */
typedef struct
{
  DALILIB_RAW_FRAME_REACTION_CODES   reactionCode;
  DALILIB_RESPONSE_VALUE_TYPE        valueType;

  dalilib_raw_frame_t                frame;
}dalilib_react_raw_frame_t;

/*! To use the "dalilib_action" function, this parameter must be set. 
    This will determine the action the stack will perform. */
typedef enum
{
  DALILIB_ACTION_NONE = 0,
  DALILIB_CTRL_GEAR_ACTION,       
  DALILIB_CTRL_GEAR_REACTION,
  DALILIB_CTRL_DEVICE_ACTION,
  DALILIB_CTRL_DEVICE_REACTION,  
  DALILIB_EVENT_ACTION,          //!< input device event actions
  DALILIB_GW_ACTION,             //!< gateway actions/reactions
  DALILIB_STATUS_REACTION,       //!< common status reaction, e.g. system failure
  DALILIB_RAW_FRAME_ACTION,      //!< send a raw frame (proprietary forward frame)
  DALILIB_RAW_FRAME_REACTION,    //!< receive a raw frame (proprietary forward frame)
  DALILIB_ACTION_MAX
}DALILIB_ACTION_TYPE;

/**********************************************************************
 * DALI-library re/action main struct
 **********************************************************************/

/*! action struct contains following information: \n
 *  - action type \n
 *  - sub action type \n
 *  - action value \n
 *  - control gear/device address */
typedef struct
{
  DALILIB_ACTION_TYPE actionType;
  union
  {
    dalilib_act_ctrl_gear_t     gearAct;
    dalilib_react_ctrl_gear_t   gearReact;
    dalilib_act_ctrl_device_t   deviceAct;
    dalilib_react_ctrl_device_t deviceReact;
    dalilib_act_event_t         eventAct;
    dalilib_react_event_t       eventReact;
    dalilib_react_status_t      statusReact;
    dalilib_act_raw_frame_t     rawFrameAct;
    dalilib_react_raw_frame_t   rawFrameReact;
  };
}dalilib_action_t;

/*! Control gear device types
 *  enumeration for control gear device types \n
 *  supported control gear device types: \n
 *  - GEAR_TYPE_FLUORESCENT_LAMPS \n
 *  - GEAR_TYPE_LED_MODULES \n
 *  - GEAR_TYPE_SWITCHING_FUNCTION \n
 *  - GEAR_TYPE_COLOUR_CONTROL */
typedef enum
{
  //! IEC 62386-201 - Fluorescent lamps
  GEAR_TYPE_FLUORESCENT_LAMPS = 0x00,
  //! IEC 62386-202 - Self-contained emergency lighting
  GEAR_TYPE_SELF_CONTAINED_EMERGENCY_LIGHTING = 0x01,
  //! IEC 62386-203 - Dischage lamps (excluding fluorescent lamps)
  GEAR_TYPE_DISCHARGE_LAMPS = 0x02,
  //! IEC 62386-204 - Low voltage halogen lamps
  GEAR_TYPE_LOW_VOLTAGE_HALOGEN_LAMPS = 0x03,
  //! IEC 62386-205 - Supply Voltage controller for incandescent lamps
  GEAR_TYPE_SUPPLY_VOLTAGE_CONTROLLER_FOR_INCANDESCENT_LAMPS = 0x04,
  //! IEC 62386-206 - Conversion from digital into D.C. voltage
  GEAR_TYPE_CONVERSION_FROM_DIGITAL_INTO_D_C_VOLTAGE = 0x05,
  //! IEC 62386-207 - LED modules
  GEAR_TYPE_LED_MODULES = 0x06,
  //! IEC 62386-208 - Switching function
  GEAR_TYPE_SWITCHING_FUNCTION = 0x07,
  //! IEC 62386-209 - Colour control
  GEAR_TYPE_COLOUR_CONTROL = 0x08,
  //! IEC 62386-210 - Control Gear sequencer
  GEAR_TYPE_CONTROL_GEAR_SEQUENCER = 0x09,
  //! DALI Part 250 - Integrated Bus Power Supply
  GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY = 0x31,
  //! DALI Part 251 - Memory Bank 1 Extension
  GEAR_TYPE_CONTROL_MEM_BANK1_EXT_SUPPLY = 0x32,
  //! DALI Part 252 - Energy Reporting
  GEAR_TYPE_CONTROL_GEAR_ENERGY_REPORTING = 0x33,
  //! DALI Part 253 - Diagnostics and Maintenance
  GEAR_TYPE_CONTROL_GEAR_DIAG_MAINTENANCE = 0x34,
  //! No further device type/feature implemented
  GEAR_TYPE_NO_FURTHER_DEVICE_TYPE = 0xFE,
  //! Control gear supporting more than one device type/feature
  GEAR_TYPE_SUPPORT_MORE_ONE_DEVICE_TYPE = 0xFF
}CTRL_GEAR_DEVICE_TYPE;

/*! DALI stack mode */
typedef enum
{
  STACK_MODE_UNKNOWN = 0x00,
  //! stack will be start as control gear
  STACK_MODE_CONTROL_GEAR,
  //! stack will be start as application controller/input device
  STACK_MODE_CONTROL_DEVICE,
  //! stack will be start as gateway
  STACK_MODE_GATEWAY
}STACK_MODE;

//----------------------------------------------------------------------
// DALI-Stack Callback functions
//----------------------------------------------------------------------
//! is called when the stack is successfully started and ready
typedef void (*fAppReady)(void);

/*! mandatory callback function must be set before the stack start \n
 *  stack sends a DALI frame
 * \param[in] context low level counterpart
 * \param[in] pFrame DALI frame in stack format
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppSend)( void* context, dalilib_frame_t* pFrame);

/*! mandatory deprecated callback function must be set before the stack start \n
 *  the application will be triggered to load the persistent data
 * \param[out] pMemory buffer pointer
 * \param[in] nSize size of persistent data
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppLoadMem)(void* pMemory, uint16_t nSize);

/*! mandatory deprecated callback function must be set before the stack start \n
 *  the application will be triggered to save the persistent data
 * \param[in] pMemory buffer pointer
 * \param[in] nSize size of persistent data
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppSaveMem)(void* pMemory, uint16_t nSize);

/*! mandatory callback function must be set before the stack start \n
 *  the application will be triggered to load the persistent data of a module
 * \param[in] A unique identifier of the module to load
 * \param[out] pMemory buffer pointer
 * \param[in] nSize size of persistent data
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppLoadModuleMem)(uint8_t id, void* pMemory, uint16_t nSize);

/*! mandatory callback function must be set before the stack start \n
 *  the application will be triggered to save the persistent data of a module
 * \param[in] A unique identifier of the module to load
 * \param[in] pMemory buffer pointer
 * \param[in] nSize size of persistent data
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppSaveModuleMem)(uint8_t id, void* pMemory, uint16_t nSize);

/*! mandatory callback function must be set before the stack start \n
 *  is called when the stack wants to notify the application of the events
 * \param[in] pAction DALI stack action struct pointer
 * \return    R_DALILIB_RESULT R_DALILIB_*** */
typedef R_DALILIB_RESULT (*fAppReAction)(dalilib_action_t* pAction);

/*! the application will be triggered to log
 * \param[in] pLog log text
 * \param[in] nLen log text lengtha */
typedef void (*fAppLog)(char *pLog, uint8_t nLen);


/*! persistent parameter for input device push buttons 
    (IEC 62386-301:2016) */
typedef struct
{
  /*! shortest time of physically recognized short presses \n
   *  in number of increments of 20 ms \n
   *  can be set to 10 but only values starting from 25 are used */
    uint8_t  pushButtonShortMin;   //!< [10, 0xFF]
  /*! shortest configurable time for detecting a double press \n
   *  in number of increments of 20 ms \n */
    uint8_t  pushButtonDoubleMin;  //!< [10, 100]
}dalilib_vendor_input_dev_push_button_params_t;

/*! input device occupancy sensor type
    (IEC 62386-303:2016) */
#define OCCUPANCY_MOVEMENT_SENSOR  0x01
#define OCCUPANCY_PRESENCE_SENSOR  0x10
typedef struct
{
  uint8_t  type;  //!< [OCCUPANCY_MOVEMENT_SENSOR|OCCUPANCY_PRESENCE_SENSOR]
}dalilib_vendor_input_dev_occupancy_sensor_type_t;

/*! persistent parameter for input device light sensor
    (IEC 62386-304:2016) */
typedef struct
{
  uint8_t lightSensorReport;        //!< [0, 255]
  uint8_t lightSensorDeadtime;      //!< [0, 255]
  uint8_t lightSensorHysteresisMin; //!< [0, 255]
  uint8_t lightSensorHysteresis;    //!< [0, 25]
}dalilib_vendor_input_dev_light_sensor_params_t;

/*! configuration parameters for control device extension
 *  Luminaire-mounted Control Devices (DALI Part 351) */
typedef struct
{
  /*! Activates application controller arbitration
      Value 0x00: off \n
      Value 0x01: on */
  uint8_t applicationControllerArbitrationActive;
  /*! Determines type of device (Type A - Type D)
      Value 0x00: true \n
      Value 0x01: false */
  uint8_t isBusPoweredDevice;
  /*! Indicates the maximum current consumed from the bus power supply
      Range Type A: 0 - 2
      Range Type B/C/D: 0 - 46
      Resolution: 1 mA */
  uint8_t maxCurrentConsumed;
  /*! Indicates the maximum average power consumed from the AUX power supply
      Range Type A: 0 - 20
      Range Type B: 0 - 10
      Range Type C/D: 0 - 30, DALI_MASK: unknown
      Resolution: 0,1 W */
  uint8_t maxPowerConsumed;
}dalilib_vendor_luminaire_t;

/*! persistent parameter for control gear device type LED modules
   (IEC 62386-207:2009) */
typedef struct
{
  uint8_t min_fast_fade_time;       //!< [1..27]
  // gear type params
  uint8_t integrated_power_supply;  //!< [FALSE|TRUE]
  uint8_t integrated_led_module;    //!< [FALSE|TRUE]
  uint8_t ac_supply;                //!< [FALSE|TRUE]
  uint8_t dc_supply;                //!< [FALSE|TRUE]
  // possible operating modes
  uint8_t pwm_possible;             //!< [FALSE|TRUE]
  uint8_t am_possible;              //!< [FALSE|TRUE]
  uint8_t regulated_output;         //!< [FALSE|TRUE]
  uint8_t high_current_pulse;       //!< [FALSE|TRUE]
  // features
  // if a feature set to true, it can be query
  uint8_t short_circuit;            //!<  [FALSE|TRUE]
  uint8_t open_circuit;             //!<  [FALSE|TRUE]
  uint8_t load_decrease;            //!<  [FALSE|TRUE]
  uint8_t load_increase;            //!<  [FALSE|TRUE]
  uint8_t overcurrent_protection;   //!<  [FALSE|TRUE]
  uint8_t thermal_shutdown;         //!<  [FALSE|TRUE]
  uint8_t thermal_overload;         //!<  [FALSE|TRUE]
  // reference measurement option
  uint8_t referencing;              //!<  [FALSE|TRUE]
}dalilib_vendor_led_params_t;

/* persistent parameter for control gear device type Colour Control
    (IEC 62386-209:2009) */
typedef struct
{
  uint16_t xCoordinate;
  uint16_t yCoordinate;
}xyCoordinate_vendor_value_t;

//! xy-Coordinate and Primary N colour type specific persistent variables
typedef struct {
  uint16_t xCoordinatePrime[6];
  uint16_t yCoordinatePrime[6];
  uint16_t tyPrime[6];
}xyCoord_primaryN_vendor_t;

typedef enum {
  COLOUR_SPACE_SRGB = 0,
  COLOUR_SPACE_ADOBE_RGB,
  COLOUR_SPACE_CUSTOM
}colour_space_t;

typedef struct {
  colour_space_t              colourSpaceType;
  double                      colourSpaceValue[3][2]; //!< RGB, xy
  xyCoordinate_vendor_value_t value;
  xyCoord_primaryN_vendor_t   primary;
}xyCoordinate_vendor_t;

//! Colour Temperature colour type specific persistent variables
typedef struct {
  uint16_t ColourTemperatureCoolest;
  uint16_t ColourTemperatureWarmest;
  uint16_t ColourTemperaturePhyCoolest;
  uint16_t ColourTemperaturePhyWarmest;
}colourTemperature_range_vendor_t;

typedef struct {
  uint16_t                         value;
  colourTemperature_range_vendor_t range;
}colourTemperature_vendor_t;

typedef struct {
  uint16_t                  primeNdimlevel[6];
  xyCoord_primaryN_vendor_t primary;
}primaryN_vendor_t;

typedef struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint8_t white;
  uint8_t amber;
  uint8_t free;
}rgbwaf_vendor_dimlevel_t;

typedef struct {
  rgbwaf_vendor_dimlevel_t dimlevel;
  uint8_t                  assignedColour[6];
}rgbwaf_vendor_t;

typedef union
{
  xyCoordinate_vendor_value_t xyCoordinateValue;
  uint16_t                    ColourTemperature;
  uint16_t                    PrimeNdimlevel[6];
  rgbwaf_vendor_dimlevel_t    rgbwafDimlevel;
}colour_vendor_value_t;

typedef struct {
  uint8_t xyCoordinate :1;
  uint8_t temperature  :1;
  uint8_t primaryN     :3; //!< number of supported primary colours
  uint8_t RGBWAF       :3; //!< number of supported RGBWAF channels
}colour_type_features_t;

typedef struct {
  uint8_t automaticActivation        :1;
  uint8_t reserved                   :5;
  uint8_t autoCalibration            :1;
  uint8_t restorationAutoCalibration :1;
}colour_gear_features_status_t;

/*! persistent parameter for control gear device type Colour Control
    (IEC 62386-209:2009) */
typedef struct
{
  uint8_t                       powerOnColourType;
  colour_vendor_value_t         powerOnColourValue;
  uint8_t                       systemFailureColourType;
  colour_vendor_value_t         systemFailureColourValue;
  colour_gear_features_status_t featuresStatus;
  colour_type_features_t        colourTypeFeatures;
  union
  {
    xyCoordinate_vendor_t      xyCoordinateParam;
    colourTemperature_vendor_t temperatureParam;
    primaryN_vendor_t          primaryNParam;
    rgbwaf_vendor_t            rgbwafDimlevel;
  };
}dalilib_vendor_colour_params_t;

/*! persistent parameter for control gear device type Switching function
    (IEC 62386-208:2009) */
typedef struct
{
  // features
  // if a feature set to true, it can be query
  uint8_t load_failure;           //!< [FALSE|TRUE]
  uint8_t threashold_config;      //!< [FALSE|TRUE]
  uint8_t failure_delay;          //!< [FALSE|TRUE]
  uint8_t referencing;            //!< [FALSE|TRUE]
  uint8_t phy_choice;             //!< [FALSE|TRUE]
  // gear type params
  uint8_t electrical_switch;      //!< [FALSE|TRUE]
  uint8_t output_open;            //!< [FALSE|TRUE]
  uint8_t output_closed;          //!< [FALSE|TRUE]
  uint8_t overvoltage_protection; //!< [FALSE|TRUE]
  uint8_t inrush_current_limiter; //!< [FALSE|TRUE]
}dalilib_vendor_switching_params_t;

/*! configuration parameters for control gear extension
 * Integrated Bus Power Supply (DALI Part 250) */
typedef struct
{
  /* Memory bank 201 default values can be configured here. To internally
   * write a memory cell, internal gear action
   * DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL can be used. */

  /*! Guaranteed supply current of integrated bus power supply [mA]
      Range 0x32 - maximum supply current: valid */
  uint8_t guaranteedCurrent;
  /*! Maximum supply current of integrated bus power supply [mA]
  Range Guaranteed supply current, 0xFA: valid */
  uint8_t maxCurrent;
  /*! The initial status of integrated bus power supply
  Value 0x00: off
  Value 0x01: on */
  uint8_t status;
}dalilib_vendor_power_supply_t;

/*! configuration parameters for control gear extension
 * Energy Reporting (DALI Part 252) */
typedef struct
{
  /* Memory bank 201 default values can be configured here. To internally
   * write a memory cell, internal gear action
   * DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL can be used. */

  /*! Scale factor for measured Active Energy values in this memory bank,
      expressed as power of 10, e.g. \n
      scale factor =  10^(ScaleFactorForActiveEnergy) * 1 Wh \n
      example: -3 denotes milli, \n
               +3 denotes kilo \n
      Range 0 - 6: valid
      Range 0xFA - 0xFF: valid */
  uint8_t scaleFactorForActiveEnergy;
  /*! Scale factor for measured Active Power values in this memory bank,
      expressed as power of 10, e.g. \n
      scale factor =  10^(ScaleFactorForActivePower) * 1 W \n
      example: -3 denotes milli, \n
               +3 denotes kilo \n
      Range 0 - 6: valid
      Range 0xFA - 0xFF: valid */
  uint8_t ScaleFactorForActivePower;
  /*! The initial Active Power \n
      The scale factor is defined by ScaleFactorForActivePower. \n
      The value should reflect the actual situation as soon as possible. \n
      Range 0 - 0xFFFFFFFD: valid \n
      Value DALI_TMASK: valid */
  uint32_t activePower;
}dalilib_vendor_energy_reporting_t;

/*! configuration parameters for control gear extension
 * Maintenance and Diagnostics (DALI Part 253) */
typedef struct
{
  /*! Lower threshold for the RMS value of external supply voltage
      Scaling factor and unit: 0.1 Vrms \n
      controlGearExternalSupplyUndervoltageThreshold shall be lower than
      the lower end of the specified input voltage range of the control gear
      and the value of the controlGearExternalSupplyUndervoltageThreshold
      is such that lifetime and/or performance of the control gear could be
      affected if the ControlGearExternalSupplyVoltage is lower than the
      threshold. */
  uint16_t controlGearExternalSupplyUndervoltageThreshold;
  /*! Upper threshold for the RMS value of external supply voltage
      Scaling factor and unit: 0.1 Vrms \n
      controlGearExternalSupplyOvervoltageThreshold shall be higher than
      the higher end of the specified input voltage range of the control gear
      and the value of the controlGearExternalSupplyOvervoltageThreshold
      is such that lifetime and/or performance of the control gear could be
      affected if the ControlGearExternalSupplyVoltage is higher than the
      threshold. */
  uint16_t controlGearExternalSupplyOvervoltageThreshold;
  /*! Upper threshold for the control gear output power */
  uint16_t controlGearOutputPowerThreshold;
  /*! Upper threshold for the control gear derating temperature
      The value of the controlGearThermalDeratingThreshold is such that lifetime
      and/or performance of the control gear could be affected if the
      ControlGearTemperature is higher than the threshold. */
  uint16_t controlGearThermalDeratingThreshold;
  /*! Upper threshold for the control gear shutdown temperature
      The value of the controlGearThermalShutdownThreshold is such that lifetime
      and/or performance of the light source could be affected if the
      controlGearTemperature is higher than the threshold.
      controlGearThermalShutdownThreshold shall be higher than
      controlGearThermalDeratingThreshold. */
  uint16_t controlGearThermalShutdownThreshold;
  /*! Upper threshold for the lighting source derating temperature
      The value of the lightSourceThermalDeratingThreshold is such that lifetime
      and/or performance of the light source could be affected if the
      ControlGearTemperature is higher than the threshold. */
  uint16_t lightSourceThermalDeratingThreshold;
  /*! Upper threshold for the control gear shutdown temperature
      The value of the lightSourceThermalShutdownThreshold is such that lifetime
      and/or performance of the light source could be affected if the
      controlGearTemperature is higher than the threshold.
      lightSourceThermalShutdownThreshold shall be higher than
      lightSourceThermalDeratingThreshold. */
  uint16_t lightSourceThermalShutdownThreshold;

  /* Memory bank 205 default values can be configured here. To internally
   * write a memory cell, internal gear action
   * DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL can be used. */

  /*! RMS value of external supply voltage
      Range 0 - 0xFFFD: valid \n
      Value DALI_TMASK: valid \n
      Value DALI_MASK: valid \n
      Scaling factor and unit: 0.1 Vrms \n
      The value should reflect the actual situation as soon as possible. */
  uint16_t controlGearExternalSupplyVoltage;
  /*! Frequency of external supply voltage
      Range 0 - 0xFD: valid \n
      Range DALI_TMASK: valid \n
      Range DALI_MASK: valid \n
      Scaling factor and unit: 1 Hz \n
      The value should reflect the actual situation as soon as possible. \n
      Indication as follows: 0 in case of 0 Hz (pure DC or rectified AC voltage). */
  uint8_t controlGearExternalSupplyVoltageFrequency;
  /*! Scale factor for control gear power
      Range 0 - 100: valid \n
      Range DALI_TMASK: valid \n
      Range DALI_MASK: valid \n
      Scaling factor and unit: 0.01 \n
      The value should reflect the actual situation as soon as possible. \n
      NOTE ControlGearPowerFactor = 100 means: the control gear has a power
      factor of 1.00 */
  uint8_t controlGearPowerFactor;
  /*! Indicates the internal temperature of the control gear
      Range 0 - 0xFD: valid \n
      Range DALI_TMASK: valid \n
      Scaling factor and unit: 1 °C \n
      The value should reflect the actual situation as soon as possible. \n
      Offset value: 60 \n
      NOTE Example: A value of 60 means 0 °C, a value of 0 means – 60 °C. */
  uint8_t controlGearTemperature;
  /*! Control gear output current in % related to the nominal output current
      setting of the control gear
      Range 0 - 100: valid \n
      Range DALI_TMASK: valid \n
      Scaling factor and unit: 1 % \n
      The value should reflect the actual situation as soon as possible. */
  uint8_t controlGearOutputCurrentPercent;

  /* Memory bank 206 default values can be configured here. To internally
   * write a memory cell, internal gear action
   * DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL can be used. */

  /*! Indicates the actual control gear output voltage
      Range 0 - 0xFFFD: valid \n
      Range DALI_TMASK: valid \n
      Scaling factor and unit: 0.1 V \n
      The value should reflect the actual situation as soon as possible. */
  uint16_t lightSourceVoltage;
  /*! Indicates the actual control gear output current
      Range 0 - 0xFFFD: valid \n
      Range DALI_TMASK: valid \n
      Scaling factor and unit: 0.001 A \n
      The value should reflect the actual situation as soon as possible. */
  uint16_t lightSourceCurrent;
  /*! Indicates the temperature of the light source
      Range 0 - 0xFD: valid \n
      Range DALI_TMASK: valid \n
      Range DALI_MASK: valid \n
      Scaling factor and unit: 1 °C \n
      The value should reflect the actual situation as soon as possible. \n
      Offset value: 60 \n
      NOTE Example: A value of 60 means 0 °C, a value of 0 means – 60 °C. */
  uint8_t lightSourceTemperature;
}dalilib_vendor_diag_maintenance_t;

/*! Vendor params struct contains information about the manufacturer
 * firmware version, identify number and hardwareversion.
 * It contains the control gear/device parameters.
 */
typedef struct
{
  /*! The bytes "GTIN" shall contain the Global Trade Item Number (GTIN),
  // e.g. the EAN, in binary. \n
  // The bytes shall be stored most significant first and filled with leading zeroes.*/
  uint8_t gtin[6];  //!< Unique identification number

  /*! The bytes ("firmware version") shall contain the firmware version
      of the bus unit.*/
  uint8_t firmware_ver_major;
  uint8_t firmware_ver_minor;

  /*! The bytes shall contain 64 bits of an identification number of the bus unit,
      prefereably the serial number.
      The identification number shall be stored with least significant byte in 
      "identification number byte 7" and unused bits shall be filled with 0.*/
  uint8_t identify_number[8];

  /*! The 2-bytes ("hardware version") shall contain the hardware version of
      the bus unit.*/
  uint8_t hw_ver_major;
  uint8_t hw_ver_minor;

  //----------------------------------------------------------------------------
  // parameter for the control gear
  //----------------------------------------------------------------------------
  //! calculate dimming values for every call of timingHelper not just for full steps
  uint8_t control_gear_high_res_dimming; // [TRUE, FALSE]

  //! physical minimum level of the control gear
  double  control_gear_phm;

  //! control gear device type
  CTRL_GEAR_DEVICE_TYPE  control_gear_device_type;
  union
  {
    dalilib_vendor_led_params_t       led_params;
    dalilib_vendor_switching_params_t switching_params;
    dalilib_vendor_colour_params_t    colour_params;
  };

  //! DALI Part 250 - Integrated Bus Power Supply
  dalilib_vendor_power_supply_t     power_supply_params;
  //! DALI Part 252 - Energy Reporting
  dalilib_vendor_energy_reporting_t energy_reporting_params;
  //! DALI Part 253 - Diagnostics and Maintenance
  dalilib_vendor_diag_maintenance_t diag_maintenance_params;

  //----------------------------------------------------------------------------
  // parameter for the control device
  //----------------------------------------------------------------------------
  //! control device mode,set 0 if not used
  CTRL_DEVICE_MODE  control_device_mode;

  //! used, if control_device_mode = CTRL_DEV_MODE_INPUT
  INPUT_DEVICE_INSTANCE_TYPE  control_device_instance_type;
  uint8_t                     control_device_instance_number;
  uint8_t                     control_device_resolution;      //!< [1, 255]
  union
  {
    dalilib_vendor_input_dev_push_button_params_t    push_btn;
    dalilib_vendor_input_dev_occupancy_sensor_type_t occupancy_sensor;
    dalilib_vendor_input_dev_light_sensor_params_t   light_sensor;
  };

  //! DALI Part 351 - Luminaire-mounted Control Devices
  dalilib_vendor_luminaire_t luminaire_params;

  uint8_t  shortAddress; //!< not used: DALI_MASK (0xFF)
}dalilib_vendor_params_t;


/*! Internal params struct contains information about internal stack parameters.
 */
typedef struct
{
  uint8_t   acceptActionsOnSystemFailure;  //!< [FALSE|TRUE]
  uint8_t   savePersistentOnInit;          //!< [FALSE|TRUE]
  uint8_t   savePersistentModules;         //!< [FALSE|TRUE]
  /*! Reduces the delay between the change of a persistent variable and
   *  the execution of the save persistent memory callback function.
   *  The default delay is 29 seconds. */
  uint8_t   savePersistentDelayReduction;  //!< [0, 29] in seconds
}dalilib_internal_params_t;

/*! the stack communicates with the application via the callback functions. \n
 *  callback functions are triggered by the stack. \n
    the mandatory callback functions must be set before the stack start.*/
typedef struct
{
  fAppReady         fPAppReady;
  fAppSend          fPAppSend;
  fAppReAction      fPAppReAction;
  fAppLoadMem       fPAppLoadMem;
  fAppSaveMem       fPAppSaveMem;
  fAppLoadModuleMem fPAppLoadModuleMem;
  fAppSaveModuleMem fPAppSaveModuleMem;
  fAppLog           fPAppLog;
}dalilib_callbacks_t;


//! some stack configuration parameters must be transmitted to DALI-Stack
typedef struct
{
  STACK_MODE                mode;      //!< stack mode
  dalilib_callbacks_t       callbacks; //!< callback functions pointer
  dalilib_vendor_params_t   vendor;    //!< manufacturer information
  dalilib_internal_params_t internal;  //!< internal stack configuration parameters
  void*                     context;   /*!< this is passed with the send
                                         callback function, so that 
                                         the application can access
                                         HAL driver instance. */
}dalilib_cfg_t;


//----------------------------------------------------------------------
// DALI-Stack functions
//----------------------------------------------------------------------

/*! DALI-Stack will create a new instance \n
 *  max. DALI stack instance at the moment 2
 *  \return dalilib_instance_t  (NULL == error) */
dalilib_instance_t dalilib_createinstance(void);

/*! starts the created DALI instance \n
 *  \param[in] pInstance pointer of a stack instance
 *  \return R_DALILIB_RESULT R_DALILIB_*** */
R_DALILIB_RESULT dalilib_start(dalilib_instance_t pInstance);

/*! configure dalilib_instance_t
 *  \param[in] pInstance pointer of a stack instance
 *  \param[in] pStackConfig pointer of a stack configuration struct
 *  \return    R_DALILIB_RESULT R_DALILIB_*** */
R_DALILIB_RESULT dalilib_init(dalilib_instance_t pInstance, dalilib_cfg_t* pStackConfig);

/*! configure dalilib_instance_t
 *  \param[in] pInstance pointer of a stack instance
 *  \param[in] nNumber memorybank number
 *  \param[in] nSize size of the memorybank
 *  \param[in] pOtherBank pointer to memory area for the memorybank
 *  \return    void*** */
R_DALILIB_RESULT dalilib_create_other_membank(dalilib_instance_t pInstance,uint8_t nNumber,uint8_t nSize,uint8_t * pOtherBank);

// DALI-Stack action function
R_DALILIB_RESULT dalilib_action(dalilib_instance_t pInstance, dalilib_action_t* pAction);

/*! will be called, when the application receives a dali-frame 
 *  \param[in] pInstance pointer of a stack instance
 *  \param[in] pFrame DALI frame in stack format */
void dalilib_receive(dalilib_instance_t pInstance, dalilib_frame_t* pFrame);

/*! external timer function, called every 10ms 
 *  \param[in] pInstance pointer of a stack instance
 *  \param[in] ticker pointer of a stack instance (resolution 100us) */
void dalilib_timingHelper(dalilib_instance_t pInstance, dalilib_time ticker);

/*! Get DALI stack info
 *  \param[in]  pInstance pointer of a stack instance
 *  \param[out] pInfo pointer of info struct
 *  \return     R_DALILIB_RESULT R_DALILIB_*** */
R_DALILIB_RESULT dalilib_infoGet(dalilib_instance_t pInstance, dalilib_info_t* pInfo);

/*! Get DALI stack version
 *  \param[in]  pInstance pointer of a stack instance
 *  \param[out] pVersion pointer of info struct
 *  \return     R_DALILIB_RESULT R_DALILIB_*** */
R_DALILIB_RESULT dalilib_versionGet(dalilib_instance_t pInstance, dalilib_version_t* pVersion);

/*! stop the stack instance  
 *  \param[in] pInstance pointer of a stack instance
 *  \return    R_DALILIB_RESULT R_DALILIB_*** */
R_DALILIB_RESULT dalilib_stop(dalilib_instance_t pInstance);


#ifdef  __cplusplus
}
#endif

#endif /* LIBDALI_H_ */
