/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _ERROR_H_
#define _ERROR_H_

#ifdef __cplusplus
extern "C" {
#endif


// intern result codes
#define R_INTERN_ACTION                                     (R_DALILIB_MAX + 1)
#define R_MEM_NOT_DEFINED                                   (R_DALILIB_MAX + 2)
#define R_INVALID_MEM_DATA_SIZE                             (R_DALILIB_MAX + 3)
#define R_FRAME_IS_NOT_FOR_ME                               (R_DALILIB_MAX + 4)
#define R_IGNORE_FRAME                                      (R_DALILIB_MAX + 5)
#define R_NO_MORE_FRAMES                                    (R_DALILIB_MAX + 6)
#define R_EXT_INPUT_DEVICE_FRAME                            (R_DALILIB_MAX + 7)
#define R_GEAR_DEVICE_TYPE_NOT_SUPPORTED                    (R_DALILIB_MAX + 8)
#define R_SUPPRESS_REACTION                                 (R_DALILIB_MAX + 9)
#define R_ABORT_ACTION                                      (R_DALILIB_MAX + 10)
#define R_SEND_AGAIN                                        (R_DALILIB_MAX + 11)
#define R_SKIP_NEXT_SEND_FRAME                              (R_DALILIB_MAX + 12)
#ifdef __cplusplus
}
#endif


#endif // _ERROR_H_
