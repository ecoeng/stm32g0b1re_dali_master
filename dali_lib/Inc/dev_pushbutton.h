/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                IEC 62386-301
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DEV_PUSH_BUTTON_H_
#define _DEV_PUSH_BUTTON_H_

#ifdef __cplusplus
    extern "C" {
#endif

#define INPUT_DEVICE_PUSH_BUTTON_RESOLUTION           1
#define INPUT_DEVICE_PUSH_BUTTON_EVENT_PRIO_DEFAULT   3
#define INPUT_DEVICE_PUSH_BUTTON_EVENT_FILTER_DEFAULT 0xF4 // 1111 0100b

#define INPUT_DEVICE_PUSH_BUTTON_DOUBLE_TIMER_DEFAULT  0
#define INPUT_DEVICE_PUSH_BUTTON_REPEAT_TIMER_DEFAULT  8
#define INPUT_DEVICE_PUSH_BUTTON_STUCK_TIMER_DEFAULT  20

// enum for the push button commands
typedef enum _PUSH_BTN_CMD
{
  PUSH_BTN_CMD_SET_SHORT_TIMER,
  PUSH_BTN_CMD_SET_DOUBLE_TIMER,
  PUSH_BTN_CMD_SET_REPEAT_TIMER,
  PUSH_BTN_CMD_SET_STUCK_TIMER,

  PUSH_BTN_CMD_QUERY_SHORT_TIMER,
  PUSH_BTN_CMD_QUERY_SHORT_TIMER_MIN,
  PUSH_BTN_CMD_QUERY_DOUBLE_TIMER,
  PUSH_BTN_CMD_QUERY_DOUBLE_TIMER_MIN,
  PUSH_BTN_CMD_QUERY_REPEAT_TIMER,
  PUSH_BTN_CMD_QUERY_STUCK_TIMER,
  PUSH_BTN_CMD_LAST,
}PUSH_BTN_CMD;

typedef enum {
  BUTTON_IDLE,
  BUTTON_RELEASED,
  BUTTON_PRESSED,
  BUTTON_SHORT_PRESS,
  BUTTON_DOUBLE_PRESS,
  BUTTON_LONG_PRESS_START,
  BUTTON_LONG_PRESS_REPEAT,
  BUTTON_LONG_PRESS_STOP,
  BUTTON_FREE,
  BUTTON_STUCK
}BUTTON_FUNCTION;

typedef enum {
  RELEASED,
  PRESSED
}BUTTON_STATE;

// unit of timer ms
typedef struct _push_buttons_pers_variables_t
{
  uint8_t tShortMin;  // [10, 0xFF]
  uint8_t tShort;     // [tShortMin, 0xFF]
  uint8_t tDoubleMin; // [10,100]
  uint8_t tDouble;    // 0,[tDoubleMin,100]
  uint8_t tRepeat;    // [5,100]
  uint8_t tStuck;     // [5,0xFF]
}push_buttons_pers_variables_t;

typedef struct _push_buttons_variables_t
{
  uint8_t         bIsButtonStuck;
  BUTTON_FUNCTION currentFunction;
  BUTTON_FUNCTION lastFunction;
  uint32_t        timePressed;
  uint32_t        timeReleased;
  uint32_t        timeLongPressRepeat;
  BUTTON_STATE    state;
  BUTTON_STATE    lastButtonState;
}push_buttons_variables_t;


#ifdef __cplusplus
    }
#endif


#endif // _DEV_PUSH_BUTTON_H_
