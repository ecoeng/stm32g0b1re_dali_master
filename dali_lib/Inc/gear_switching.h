/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear - SWITCHING function (device type 7) 
 *          IEC 62386-208
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_SWITCHING_H_
#define _GEAR_SWITCHING_H_

#ifdef __cplusplus
extern "C" {
#endif

/*


#define CTRL_GEAR_LED_REFERENCING_TIME_MAX  (15 * 60 * 1000) // 15 min

#define MAX_FAST_FADE_TIME          27
#define FAST_FADE_TIME_FACTOR       25
*/
#define CTRL_GEAR_SWITCHING_REFERENCING_TIME_MAX  (15 * 60 * 1000) // 15 min

#define CTRL_GEAR_SWITCHING_EXTENDED_VERSION_NUMBER   1

#define LOAD_FAILURE_FLAG           0x01
#define THREASHOLD_CONFIG_FLAG      0x08
#define FAILURE_DELAY_FLAG          0x10
#define REFERENCING_FLAG            0x40
#define PHY_CHOICE_FLAG             0x80

#define ELECTRICAL_SWITCH_FLAG      0x01
#define OUTPUT_OPEN_FLAG            0x02
#define OUTPUT_CLOSED_FLAG          0x04
#define VOLTAGE_PROTECTION_FLAG     0x08
#define INRUSH_CURRENT_LIMITER_FLAG 0x10


// enum for the switching commands
typedef enum _SWITCHING_CMD
{
  SWITCHING_REFERENCE_SYSTEM_POWER,                 // Command 224
  SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD,    // Command 225
  SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD,   // Command 226
  SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD,  // Command 227 
  SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD, // Command 228
  SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME,       // Command 229
  SWITCHING_QUERY_FEATURES,                         // Command 240
  SWITCHING_QUERY_SWITCH_STATUS,                    // Command 241
  SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD,           // Command 242
  SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD,          // Command 243
  SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD,         // Command 244
  SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD,        // Command 245
  SWITCHING_QUERY_ERROR_HOLD_OFF_TIME,              // Command 246
  SWITCHING_QUERY_GEAR_TYPE,                        // Command 247
  SWITCHING_QUERY_REFERENCE_RUNNING,                // Command 249
  SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED,     // Command 250
  SWITCHING_QUERY_EXTENDED_VERSION_NUMBER,          // Command 255
  SWITCHING_ENABLE_DEVICE_TYPE_7,                   // Command 272
  SWITCHING_CMD_LAST
}SWITCHING_CMD;


// switching persistent variables
typedef struct _switching_pers_variables_t
{
  uint8_t     phm;               // physical min level (254)
  uint8_t     minLevel;          // default: 254 (1-maxLevel)
  uint8_t     maxLevel;          // default: 254 (minLevel-254)
  uint8_t     upSwitchOnThr;     // default: 1 ( 1-254, 255 = disabled)
  uint8_t     upSwitchOffThr;    // default: 255 (1-254, 255 = disabled)
  uint8_t     downSwitchOnThr;   // default: 255 (0-254, 255 = disabled)
  uint8_t     downSwitchOffThr;  // default: 0 (0-254, 255 = disabled)
  uint8_t     errorHoldOffTimer; // default: 0 (0-255, value * 10 seconds)
  uint8_t     features;          // (x00x x0xx)
  uint8_t     gearType;          // (xxxx x000)
  uint8_t     switchStatus;      // default x000 000x (xxxx 000x)
  uint8_t     referenceValue;
  uint8_t     extendedVersionNumber;
  uint8_t     deviceType;
}switching_pers_variables_t;

// switching variables
typedef struct _switching_variables_t
{
  // used, if "reference running"
  uint8_t     referenceRunning;
  uint8_t     referenceStartTime;
}switching_variables_t;

#ifdef __cplusplus
}
#endif


#endif // _GEAR_SWITCHING_H_
