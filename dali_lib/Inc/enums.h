/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef ENUMS_H_
#define ENUMS_H_

// Received frame types
typedef enum _FRAME_TYPE
{
  FRAME_TYPE_DALI = 0x00,
  FRAME_TYPE_STATUS,
  FRAME_TYPE_UNKNOWN
}FRAME_TYPE;

typedef enum _STACK_STATUS
{
  STACK_STATUS_INIT = 0,        // stack initialise
  STACK_STATUS_STARTING,        // stack starting...
  STACK_STATUS_WAIT_INTERFACE,  // wait ready frame of the low level driver
  STACK_STATUS_IDLE,            // stack wait commands/frames
  STACK_STATUS_WAIT_CONF,       // stack wait confirmation frame for last send frame
  STACK_STATUS_WAIT_RESPONSE,   // stack wait response frame for last send frame
  STACK_STATUS_RETRY,           // used if last frame not send
  STACK_STATUS_TERMINATED,
}STACK_STATUS;

typedef enum _FRAME_STATUS
{
  FRAME_WITHOUT_RESPONSE,
  FRAME_WITH_RESPONSE
}FRAME_STATUS;

typedef enum _SEND_FRAME_TYPE
{
  SEND_FRAME_TYPE_CTRL_GEAR,
  SEND_FRAME_TYPE_CTRL_DEV,
  SEND_FRAME_TYPE_CTRL_EVENT,
  SEND_FRAME_TYPE_RAW,
}SEND_FRAME_TYPE;

// stack internal action code
typedef enum
{
  DALILIB_ACT_TYPE_STACK_INTERNAL_CTRL_DEVICE = DALILIB_ACT_TYPE_CTRL_DEV_LAST + 1
}DALILIB_STACK_INTERNAL_ACTION_CODE_TYPE;

/* Memory bank 0 contains information about the control gear/device.
 * Memory bank 0 shall be implemented in all control gear/device.
 */
typedef struct _ctrl_mem_bank0_t
{
  // Address of last accessible memory location
  uint8_t      address_last_cell; // stack intern
  // Query answer shall be NO
  uint8_t      reserved;       // stack intern

  // Number of last accessible memory bank
  uint8_t      number_last_bank;

  // The bytes “GTIN” shall contain the Global Trade Item  Number (GTIN),
  // e.g. the EAN, in binary.
  // The bytes shall be stored most significant first and  filled with leading zeroes.
  uint8_t      gtin[6];        // Unique identification number

  // The bytes (“firmware version”) shall contain the firmware version
  // of the bus unit.
  uint8_t      firmware_ver_major;
  uint8_t      firmware_ver_minor;

  // The bytes shall contain 64 bits of an identification number of the bus unit,
  // prefereably the serial number.
  // The identification number shall be stored with least significant byte in “identification
  // number byte 7” and unused bits shall be filled with 0.
  uint8_t      identify_number[8];

  // The 2-bytes (“hardware version”) shall contain the hardware version of
  // the bus unit.
  uint8_t      hw_ver_major;
  uint8_t      hw_ver_minor;

  // The byte shall contain the implemented IEC 62386-101 version number of the
  // bus unit.
  uint8_t      ver_101_number;

  // The byte shall contain the implemented IEC 62386-102 version number of the
  // bus unit. If no control gear is implemented, the version number shall be 0xFF.
  uint8_t      ver_102_number;

  // The byte shall contain the implemented IEC 62386-103 version number of the
  // bus unit. If no control device is implemented, the version number shall be 0xFF.
  uint8_t      ver_103_number;

  // Number of logical control device units
  // range [0,64]
  uint8_t      number_log_ctrl_device;
  // Number of logical control gear units
  // range [1,64]
  uint8_t      number_log_ctrl_gear;

  // Index number of this logical control gear/device unit
  // range [0,(number_log_ctrl_gear||number_log_ctrl_device)-1]
  uint8_t      index_log_control_unit;
}ctrl_mem_bank0_t;


typedef struct _dali_ctrl_mem_bank0_t
{
  union
  {
    uint8_t           memBank0[255];
    ctrl_mem_bank0_t  ctrlMemBank0;
  };
}dali_ctrl_mem_bank0_t;

typedef struct _dali_ctrl_mem_bank_others_t
{
  uint8_t  is_used[MAXMEMBANKS];
  uint8_t* memBank[MAXMEMBANKS];
} dali_ctrl_mem_bank_others_t;

#endif /* ENUMS_H_ */
