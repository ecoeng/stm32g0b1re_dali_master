/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear - LED modules (device type 6) 
 *          IEC 62386-207
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_LED_H_
#define _GEAR_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_EXTENDED_VERSION_NUMBER   1

#define CTRL_GEAR_LED_REFERENCING_TIME_MAX  (15 * 60 * 1000) // 15 min

#define MAX_FAST_FADE_TIME        27
#define FAST_FADE_TIME_FACTOR     25

#define SHORT_CIRCUIT_FLAG        0x01
#define OPEN_CIRCUIT_FLAG         0x02
#define LOAD_DECREASE_FLAG        0x04
#define LOAD_INCREASE_FLAG        0x08
#define CURRENT_PROTECTOR_FLAG    0x10
#define THERMAL_SHUT_DOWN_FLAG    0x20
#define THERMAL_OVERLOAD_FLAG     0x40
#define REFERENCE_FLAG            0x80

#define PWM_FLAG                  0x01
#define AM_FLAG                   0x02
#define REGULATED_OUTPUT_FLAG     0x04
#define HIGH_CURRENT_PULSE_FLAG   0x08

#define INTEGRATED_POWER_SUPPLY_FLAG  0x01
#define INTEGRATED_LED_MODULE_FLAG    0x02
#define AC_SUPPLY_FLAG                0x04
#define DC_SUPPLY_FLAG                0x08


// enum for the led commands
typedef enum _LED_CMD
{
  LED_REFERENCE_SYSTEM_POWER,
  LED_ENABLE_CURRENT_PROTECTOR,
  LED_DISABLE_CURRENT_PROTECTOR,
  LED_SELECT_DIMMING_CURVE,
  LED_STORE_DTR_AS_FFT,
  LED_QUERY_GEAR_TYPE,
  LED_QUERY_DIMMING_CURVE,
  LED_QUERY_POSSIBLE_OPERATING_MODES,
  LED_QUERY_FEATURES,
  LED_QUERY_FAILRE_STATUS,
  LED_QUERY_SHORT_CIRCUIT,
  LED_QUERY_OPEN_CIRCUIT,
  LED_QUERY_LOAD_DECREASE,
  LED_QUERY_LOAD_INCREASE,
  LED_QUERY_CURRENT_PROTECTOR_ACTIVE,
  LED_QUERY_THERMAL_SHUT_DOWN,
  LED_QUERY_THERMAL_OVERLOAD,
  LED_QUERY_REFERENCE_RUNNING,
  LED_QUERY_MEASUREMENT_FAILED,
  LED_QUERY_CURRENT_PROTECTOR_ENABLED,
  LED_QUERY_OPERATING_MODE,
  LED_QUERY_FFT,
  LED_QUERY_MIN_FFT,
  LED_QUERY_EXTENDED_VERSION_NUMBER,
  LED_CMD_LAST
}LED_CMD;

typedef enum
{
  CURRENT_PROTECTOR_DISABLED = 0,
  CURRENT_PROTECTOR_ENABLED,
  CURRENT_PROTECTOR_ACTIVE,
}CURRENT_PROTECTOR;

// dimming curve
typedef enum _DIMMING_CURVE
{
  DIMMING_CURVE_STANDARD = 0,
  DIMMING_CURVE_LINEAR   = 1
}DIMMING_CURVE;

// led persistent variables
typedef struct _led_pers_variables_t
{
  uint8_t             min_fft;
  uint8_t             fft;
  uint8_t             gearType;
  uint8_t             operatingModes;    // possible Operating Modes
  uint8_t             features;
  uint8_t             failureStatus;
  uint8_t             operatingMode;
  DIMMING_CURVE       dimmingCurve;
  uint8_t             extendedVersionNumber;
  uint8_t             deviceType;
  uint8_t             referenceValue;
  CURRENT_PROTECTOR   currentProtectorStatus;
}led_pers_variables_t;

// led variables
typedef struct _led_variables_t
{
  // used, if "reference running"
  uint8_t    referenceRunning;
  uint8_t    referenceStartTime;
}led_variables_t;


#ifdef __cplusplus
}
#endif


#endif // _GEAR_LED_H_
