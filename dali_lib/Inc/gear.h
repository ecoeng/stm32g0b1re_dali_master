/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *          IEC 62386-102
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_H_
#define _GEAR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define DALI_CTRL_GEAR_CMD_MASK         0xFF

#define DALI_CTRL_GEAR_SELECT_BIT_MASK  0x01 // selection bit mask

#define DALI_CTRL_GEAR_BROADCAST_ADR    0x7F // broadcast address mask ( without selectionbit )
#define DALI_CTRL_GEAR_NBROADCAST_ADR   0x7E

#define DALI_CTRL_GEAR_GROUPADR_MASK_1  0x40 // group address mask ( without selectionbit )
#define DALI_CTRL_GEAR_GROUPADR_MASK_2  0x0F // group address mask ( without selectionbit )

#define DALI_CTRL_GEAR_SHORTADR_MASK        0x3F // short address mask ( without selectionbit )

#define DALI_CTRL_GEAR_FF_VALUE_MASK        0xFF
#define DALI_CTRL_GEAR_FF_SCENENR_MASK      0xFF
#define DALI_CTRL_GEAR_FF_GROUPNR_MASK      0xFF
#define DALI_CTRL_GEAR_FF_OPCODE_BYTE       0xFF

#define DALI_CTRL_GEAR_MAX_GROUP            0x10  // 16 groups

// group commands range
#define DALI_CTRL_GEAR_ADD_FROM_GRP_START       0x60
#define DALI_CTRL_GEAR_ADD_FROM_GRP_END         0x6F

#define DALI_CTRL_GEAR_REMOVE_FROM_GRP_START    0x70
#define DALI_CTRL_GEAR_REMOVE_FROM_GRP_END      0x7F

// scene commands range
#define DALI_CTRL_GEAR_GOTO_SCENE_START     0x10
#define DALI_CTRL_GEAR_GOTO_SCENE_END       0x1F

#define DALI_CTRL_GEAR_REMOVE_SCENE_START   0x50
#define DALI_CTRL_GEAR_REMOVE_SCENE_END     0x5F

#define DALI_CTRL_GEAR_SET_SCENE_START      0x40
#define DALI_CTRL_GEAR_SET_SCENE_END        0x4F

#define DALI_CTRL_GEAR_QUERY_SCENE_START    0xB0
#define DALI_CTRL_GEAR_QUERY_SCENE_END      0xBF

#define DIMM_DIRECTION_UP                   0
#define DIMM_DIRECTION_DOWN                 1

#define DIMM_TYPE_DIMMTIME                  0
#define DIMM_TYPE_DIMMRATE                  1
#define DIMM_RATE_TIME                      (200 * 1000) // us

#define DALI_GEAR_SPECIAL_COMMAND_START     0xA0
#define DALI_GEAR_SPECIAL_COMMAND_END       0xCB

#define DALI_GEAR_RESERVED_COMMAND_START    0xCC
#define DALI_GEAR_RESERVED_COMMAND_END      0xFB

#define DALI_FADE_TIME_RATE_FACTOR_MAX      0xF
#define DALI_EX_FADE_FACTOR_MAX             0x4F

#define DALI_CTRL_GEAR_INITIALISE_TIME_MAX       (15 * 60 * 1000) // 15min

#define DALI_CTRL_GEAR_RESET_TIME_MAX            300   // 300ms
#define DALI_CTRL_GEAR_MEM_RESET_TIME_MAX        10000 // 10s

#define DALI_CTRL_GEAR_SAVE_PERS_TIME_MAX        300   // 300ms

#define DALI_CTRL_GEAR_IDENTIFY_DEVICE_TIME_MAX  10000   // 10s

#define DALI_CTRL_LAMP_FAILURE_TIMEOUT           30000 // 30s

#define DALI_CTRL_GEAR_RANDOM_RANGE_MIN          0x000000
#define DALI_CTRL_GEAR_RANDOM_RANGE_MAX          0xFFFFFE

#define DALI_CTRL_GEAR_DTR_0                     0xA3

#define DALI_CTRL_GEAR_EXT_CMD_START             0xE0
#define DALI_CTRL_GEAR_EXT_CMD_END               0xFE

#define DALI_CTRL_GEAR_POWER_ON_LEVEL_TIMEOUT    660 // 660ms

#define DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE     0x02 // memory bank lock byte, see 102/103 9.10.2
#define DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK   0x55 // mask to unlock selected memory bank, see 102/103 9.10.2

// Masks for gearPersistentChangedFlags
#define DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK                0x0001
#define DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK               0x0002
#define DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK                  0x0004
#define DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK                  0x0008
#define DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK                 0x0010
#define DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK                0x0020
#define DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK                   0x0040
#define DALI_CTRL_GEAR_PERS_SWITCHING_MASK                    0x0080
#define DALI_CTRL_GEAR_PERS_COLOUR_MASK                       0x0100
#define DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK                0x0200
#define DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK                 0x0400
#define DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK                0x0800
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK  0x1000
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK  0x2000
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK  0x4000

#if defined(DALILIB_GEAR_POWER_SUPPLY_PRESENT) || \
    defined(DALILIB_GEAR_MEM_BANK1_EXT_PRESENT) || \
    defined(DALILIB_GEAR_ENERGY_REPORT_PRESENT)
#define DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
#endif

// enum for the control gear commands
typedef enum _GEAR_CMD
{
  GEAR_CMD_NONE = 0,
  GEAR_CMD_DAPC,
  GEAR_CMD_OFF,
  GEAR_CMD_UP,
  GEAR_CMD_DOWN,
  GEAR_CMD_STEP_UP,
  GEAR_CMD_STEP_DOWN,
  GEAR_CMD_RECALL_MAX_LEVEL,
  GEAR_CMD_RECALL_MIN_LEVEL,
  GEAR_CMD_STEP_DOWN_AND_OFF,
  GEAR_CMD_ON_AND_STEP_UP,
  GEAR_CMD_ENABLE_DAPC_SEQUENCE,
  GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL,
  GEAR_CMD_GO_TO_SCENE,
  GEAR_CMD_RESET,
  GEAR_CMD_STORE_ACTUAL_LEVEL_IN_DTR0,
  GEAR_CMD_SAVE_PERSISTENT_VARIABLES,
  GEAR_CMD_SET_OPERATING_MODE,
  GEAR_CMD_RESET_MEMORY_BANK,
  GEAR_CMD_IDENTIFY_DEVICE,
  GEAR_CMD_SET_MAX_LEVEL,
  GEAR_CMD_SET_MIN_LEVEL,
  GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL,
  GEAR_CMD_SET_POWER_ON_LEVEL,
  GEAR_CMD_SET_FADE_TIME,
  GEAR_CMD_SET_FADE_RATE,
  GEAR_CMD_SET_EXTENDED_FADE_TIME,
  GEAR_CMD_SET_SCENE,
  GEAR_CMD_REMOVE_FROM_SCENE,
  GEAR_CMD_ADD_TO_GROUP,
  GEAR_CMD_REMOVE_FROM_GROUP,
  GEAR_CMD_SET_SHORT_ADDRESS,
  GEAR_CMD_ENABLE_WRITE_MEMORY,

  GEAR_CMD_QUERY_STATUS,
  GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT,
  GEAR_CMD_QUERY_LAMP_FAILURE,
  GEAR_CMD_QUERY_LAMP_POWER_ON,
  GEAR_CMD_QUERY_LIMIT_ERROR,
  GEAR_CMD_QUERY_RESET_STATE,
  GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS,
  GEAR_CMD_QUERY_VERSION_NUMBER,
  GEAR_CMD_QUERY_CONTENT_DTR0,
  GEAR_CMD_QUERY_DEVICE_TYPE,
  GEAR_CMD_QUERY_PHYSICAL_MINIMUM,
  GEAR_CMD_QUERY_POWER_FAILURE,
  GEAR_CMD_QUERY_CONTENT_DTR1,
  GEAR_CMD_QUERY_CONTENT_DTR2,
  GEAR_CMD_QUERY_OPERATING_MODE,
  GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE,
  GEAR_CMD_QUERY_ACTUAL_LEVEL,
  GEAR_CMD_QUERY_MAX_LEVEL,
  GEAR_CMD_QUERY_MIN_LEVEL,
  GEAR_CMD_QUERY_POWER_ON_LEVEL,
  GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL,
  GEAR_CMD_QUERY_FADE_TIME_RATE,
  GEAR_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE,
  GEAR_CMD_QUERY_NEXT_DEVICE_TYPE,
  GEAR_CMD_QUERY_EXTENDED_FADE_TIME,
  GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE,
  GEAR_CMD_QUERY_SCENE_LEVEL,
  GEAR_CMD_QUERY_GROUPS_0_7,
  GEAR_CMD_QUERY_GROUPS_8_15,
  GEAR_CMD_QUERY_RANDOM_ADDRESS_H,
  GEAR_CMD_QUERY_RANDOM_ADDRESS_M,
  GEAR_CMD_QUERY_RANDOM_ADDRESS_L,
  GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0,
  GEAR_CMD_READ_MEMORY_CELL,
  GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER,
  
  GEAR_CMD_TERMINATE,
  GEAR_CMD_DTR0,
  GEAR_CMD_INITIALISE,
  GEAR_CMD_RANDOMISE,
  GEAR_CMD_COMPARE,
  GEAR_CMD_WITHDRAW,
  GEAR_CMD_PING,
  GEAR_CMD_SEARCHADDRH,
  GEAR_CMD_SEARCHADDRM,
  GEAR_CMD_SEARCHADDRL,
  GEAR_CMD_PROGRAM_SHORT_ADDRESS,
  GEAR_CMD_VERIFY_SHORT_ADDRESS,
  GEAR_CMD_QUERY_SHORT_ADDRESS,
  GEAR_CMD_ENABLE_DEVICE_TYPE,
  GEAR_CMD_DTR1,
  GEAR_CMD_DTR2,
  GEAR_CMD_WRITE_MEMORY_LOCATION,
  GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY,
  GEAR_CMD_LAST
}GEAR_CMD;

typedef enum _CTRL_GEAR_INITIALISE_STATE
{
  CTRL_GEAR_INITIALISE_STATE_DISABLED,
  CTRL_GEAR_INITIALISE_STATE_ENABLED,
  CTRL_GEAR_INITIALISE_STATE_WITHDRAWN
}CTRL_GEAR_INITIALISE_STATE;

typedef enum _CTRL_GEAR_WRITE_ENABLE_STATE
{
  CTRL_GEAR_WRITE_ENABLE_STATE_DISABLED,
  CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED
}CTRL_GEAR_WRITE_ENABLE_STATE;

typedef enum _CTRL_GEAR_DEVICE_IDENTIFY_STATE
{
  CTRL_GEAR_DEVICE_IDENTIFY_STATE_OFF,
  CTRL_GEAR_DEVICE_IDENTIFY_STATE_ON
}CTRL_GEAR_DEVICE_IDENTIFY_STATE;


/**********************************************************************
 * Control Gear structs/enums/defines
 **********************************************************************/
#define DALI_CTRL_GEAR_MAX_SCENES           16     // 16 scenes
#define DALI_CTRL_GEAR_NORMAL_OPERTING_MODE 0x00


#define FLUORESCENT_LAMP_VERSION    2  // 2.0

typedef struct _flamp_pers_variables_t
{
  uint8_t      extendedVersionNumber;
  uint8_t      deviceType;
}flamp_pers_variables_t;

// persistent configuration variables of the control gear
typedef struct ctrl_gear_pers_variables_t
{
  // physical minimum level corresponding to the minimum light output
  uint8_t      highResDimming;
  double       phm;                              // [1,254]
  uint8_t      lastLightLevel;                   // [0,minLevel..maxLevel]
  uint8_t      powerOnLevel;                     // [0..255]
  uint8_t      systemFailureLevel;               // [0..255]
  uint8_t      minLevel;                         // [phm..maxLevel]
  uint8_t      maxLevel;                         // [minLevel..0xFE]
  uint8_t      fadeRate;                         // [1..15]
  uint8_t      fadeTime;                         // [0..15]
  uint8_t      extFadeTimeBase;                  // [0..15]
  uint8_t      extFadeTimeMultiple;              // [0..4]
  uint8_t      shortAddress;                     // [0..63,255] 255: no Address:
  uint32_t     randomAddress;                    // [0..0xFF FF FF]
  // supported only DALI_CTRL_GEAR_NORMAL_OPERTING_MODE
  uint8_t      operatingMode;                    // [0, 0x80..0xFF]
  uint16_t     gearGroups;                       // [0..0xFF FF]
  uint8_t      scene[DALI_CTRL_GEAR_MAX_SCENES]; // [0..0xFF]
  CTRL_GEAR_DEVICE_TYPE  gearDeviceType;
}dali_ctrl_gear_pers_variables_t;

// persistent memory of the control gear
typedef struct
{
  dali_ctrl_gear_pers_variables_t   gearPersVariables;
  union
  {
    flamp_pers_variables_t      fluorescentPersVariables;
    led_pers_variables_t        ledPersVariables;
    colour_pers_variables_t     colourPersVariables;
    switching_pers_variables_t  switchingPersVariables;
  };
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
  dali_ctrl_mem_bank1_NVM_t         gearMemBank1NVM;   // Memory bank 1 extension non-volatile random access memory cells (part 251)
  #endif
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
  dali_ctrl_mem_bank201_NVM_t       gearMemBank201NVM; // Integrated bus power supply non-volatile memory cells (part 250)
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
  dali_ctrl_mem_bank202_NVM_t       gearMemBank202NVM; // Energy Reporting non-volatile memory cells (part 252)
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
  dali_ctrl_mem_bank205_NVM_t       gearMemBank205NVM; // Diagnostics and Maintenance non-volatile memory cells (part 253)
  dali_ctrl_mem_bank206_NVM_t       gearMemBank206NVM; // Diagnostics and Maintenance non-volatile memory cells (part 253)
  dali_ctrl_mem_bank207_NVM_t       gearMemBank207NVM; // Diagnostics and Maintenance non-volatile memory cells (part 253)
  #endif
}dali_ctrl_gear_pers_mem_t;

// volatile configuration variables of the control gear
typedef struct ctrl_gear_variables_t
{
  uint8_t      actualLevel;
  uint8_t      targetLevel;
  uint8_t      lastActiveLevel;
  uint32_t     searchAddress;
  uint8_t      initialisationState;
  uint8_t      writeEnableState;
  uint8_t      controlGearFailure;
  uint8_t      lampFailure;
  uint8_t      lampOn;
  uint8_t      limitError;
  uint8_t      fadeRunning;
  uint8_t      resetState;
  uint8_t      powerCycleSeen;
  uint8_t      DTR0;
  uint8_t      DTR1;
  uint8_t      DTR2;
  uint8_t      enableDeviceType;
  union
  {
    led_variables_t         ledVariables;
    switching_variables_t   switchingVariables;
    colour_variables_t      colourVariables;
  };
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
  power_supply_variables_t      powerSupplyVariables;
  #endif
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
  mem_bank1_ext_variables_t     memBank1ExtVariables;
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
  energy_reporting_variables_t  energyReportingVariables;
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
  diag_maintenance_variables_t  diagMaintenaceVariables;
  #endif
}dali_ctrl_gear_variables_t;

typedef struct ctrl_gear_dimming_t
{
  uint32_t        startFadeTimestamp; // [100us]
  uint8_t         startDimmLevel;
  uint32_t        fadeTimestamp;      // [100us]
  uint32_t        stepTime;           // [us]
  uint8_t         dimmDirection;      // DIMM_DIRECTION_UP,DIMM_DIRECTION_DOWN
  uint8_t         dimmType;
  uint8_t         afterDimmOff;       // 1: after dimming curve, control gear set off
}dali_ctrl_gear_dimming_t;

// helper pointers of the control gear
typedef dali_ctrl_gear_pers_variables_t*  PCtrlGearPersistentVariables;
typedef dali_ctrl_gear_variables_t*       PCtrlGearVariables;
typedef dali_ctrl_mem_bank0_t*            PCtrlGearMemoryBank0;
typedef dali_ctrl_mem_bank_others_t*      PCtrlGearMemoryBanksOther;

// control gear timer function
typedef void (*fCtrlGearTimingHelper)     (void* pInstance);


// instance variables for extended device type support
typedef struct
{
  gear_adr_t            adr;
  uint8_t               idx;

}dali_ctrl_gear_device_types_t;

// instance variables of the control gear
typedef struct ctrl_gear_instance_t
{
  void*                             pInst;                  // PDaliLibInstance
  dali_ctrl_gear_pers_mem_t         gearPersistent;
  dali_ctrl_gear_variables_t        gearVariables;
  dali_ctrl_gear_dimming_t          gearDimm;
  dali_ctrl_mem_bank0_t             gearMemBank0;
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
  dali_ctrl_mem_bank1_RAM_t         gearMemBank1RAM;        // Memory bank 1 extension volatile random access memory cells (part 251)
  #endif
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
  dali_ctrl_gear_mem_bank201_RAM_t  gearMemBank201RAM;      // Integrated bus power supply volatile random access memory cells (part 250)
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
  dali_ctrl_mem_bank202_RAM_t       gearMemBank202RAM;      // Energy Reporting volatile random access memory cells (part 252)
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
  dali_ctrl_mem_bank205_RAM_t       gearMemBank205RAM;      // Diagnostics and Maintenance volatile random access memory cells (part 253)
  dali_ctrl_mem_bank206_RAM_t       gearMemBank206RAM;      // Diagnostics and Maintenance volatile random access memory cells (part 253)
  dali_ctrl_mem_bank207_RAM_t       gearMemBank207RAM;      // Diagnostics and Maintenance volatile random access memory cells (part 253)
  #endif
  dali_ctrl_mem_bank_others_t       gearMemBanksOther;      // membanks different to 0
  
  // used, if "INITIALISE" frame received
  uint32_t                          gearInitialiseStartTime;
  // used, if "RESET" frame received
  uint32_t                          gearResetStartTime;
  uint8_t                           gearResetRunning;
  // used, if "RESET MEMORY BANK - X"  frame received
  uint32_t                          gearResetMemoryStartTime;
  uint8_t                           gearResetMemoryRunning;
  // used, if "SAVE PERSISTENT VARIABLES"  frame received
  uint32_t                          gearSavePersistentStartTime;
  uint8_t                           gearSavePersistentRunning;
  // used, if persistent variables changed but not stored yet
  uint32_t                          gearPersistentChangedTime;
  uint16_t                          gearPersistentChangedFlags;
  // used, if "IDENTIFY DEVICE" frame received
  uint32_t                          gearIdentifyDeviceStartTime;
  uint8_t                           gearIdentifyRunning;
  uint8_t                           gearIdentifyDeviceState;
  // used, if "lampFailure" TRUE
  uint32_t                          gearLampFailureStatusTimer;
  // used, by "after power on"
  uint32_t                          gearPowerOnLevelTime;
  uint8_t                           gearPowerOnRunning;
  // used, by "system failure"
  uint8_t                           gearSystemFailure;
  // used, by "bus power flag" 0: Power On 1: Power Off
  uint8_t                           gearBusPower;
  // used, by "collision detection"
  dalilib_buscollision_status_t     gearBusCollision;
  // used, by "Enable DAPC Sequence"
  uint32_t                          gearEnableDAPCStartTime;
  uint8_t                           gearEnableDAPCRunning;
  #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
  // used, if "QUERY (NEXT) DEVICE TYPE" frame received and device type extensions present
  dali_ctrl_gear_device_types_t     gearDeviceTypeQuery;
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
  // used by device type extension Diagnostics and Maintenance
  uint32_t                          gearLastTime;
  // used by device type extension Diagnostics and Maintenance
  uint32_t                          gearOpDiff;
  // used by device type extension Diagnostics and Maintenance
  uint32_t                          gearOnDiff;
  // used by device type extension Diagnostics and Maintenance
  uint8_t                           gearPowerOnCounted;
  // used by device type extension Diagnostics and Maintenance
  uint8_t                           gearLampOnCounted;
  // used by device type extension Diagnostics and Maintenance
  uint8_t                           gearShortCircuit;
  // used by device type extension Diagnostics and Maintenance
  uint8_t                           gearOpenCircuit;
  #endif
  // control gear timer function
  fCtrlGearTimingHelper             fpGearTimingHelper;
}dali_ctrl_gear_instance_t;

typedef dali_ctrl_gear_instance_t*      PCtrlGearInstance;

// control gear command structure
typedef struct
{
  GEAR_CMD        eGearCmd;
  uint8_t         selectorBit;    // 0: last array element or DAPC, 1: normal commands, 2: special commands
  uint8_t         opcodeByte;      
  uint8_t         answer;         // 0: no answer, 1: wait on answer
  uint8_t         twice;          // 0: send command normal, 1: send command double
}ctrl_gear_commands_t;


//------------------------------------------------------------------
// gear.c:
//------------------------------------------------------------------

#ifdef __cplusplus
}
#endif


#endif // _GEAR_H_
