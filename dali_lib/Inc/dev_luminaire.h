/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control device extension
 *                Luminaire-mounted Control Devices
 *                DALI Part 351
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DEVICE_LUMINAIRE_H_
#define _DEVICE_LUMINAIRE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER   201

// memory bank default values
#define CTRL_DEVICE_MEMORY_BANK_201_ADDRESS_LAST_CELL         0x07
#define CTRL_DEVICE_MEMORY_BANK_201_VERSION                   0x01
// memory bank cell addresses
#define CTRL_DEVICE_MEMORY_BANK_201_VERSION_BYTE              0x03
#define CTRL_DEVICE_MEMORY_BANK_201_TYPE_OF_DEVICE_BYTE       0x04
#define CTRL_DEVICE_MEMORY_BANK_201_MAX_CURRENT_CONSUMED_BYTE 0x05
#define CTRL_DEVICE_MEMORY_BANK_201_MAX_POWER_CONSUMED_BYTE   0x06
#define CTRL_DEVICE_MEMORY_BANK_201_APP_CTRL_ARBITRATION_BYTE 0x07

#define CTRL_DEVICE_LUMINAIRE_TYPE_A   0x00
#define CTRL_DEVICE_LUMINAIRE_TYPE_B   0x01
#define CTRL_DEVICE_LUMINAIRE_TYPE_C   0x02
#define CTRL_DEVICE_LUMINAIRE_TYPE_D   0x03

#define CTRL_DEVICE_LUMINAIRE_FIRST_QUERY_DELAY     30000  // in ms
#define CTRL_DEVICE_LUMINAIRE_REPEATING_QUERY_DELAY 900000 // in ms


/* Memory bank 201 contains information about luminaire-mounted control devices.
 * Memory bank 201 shall be active if luminaire-mounted control device is present.
 * dali_ctrl_device_mem_bank201_RAM_t contains volatile random access memory cells of memory bank 201,
 * non-volatile memory cells are stored in dali_ctrl_device_mem_bank201_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;

}dali_ctrl_device_mem_bank201_RAM_t;

/* Memory bank 201 contains information about luminaire-mounted control devices.
 * Memory bank 201 shall be active if luminaire-mounted control device is present.
 * dali_ctrl_mem_bank201_NVM_t contains non-volatile memory cells of memory bank 201,
 * volatile random access memory cells are stored in dali_ctrl_mem_bank201_RAM_t.
 */
typedef struct
{
  uint8_t     applicationControllerArbitration;
}dali_ctrl_device_mem_bank201_NVM_t;

#ifdef __cplusplus
}
#endif


#endif // _DEVICE_LUMINAIRE_H_
