/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear extension Diagnostics
 *                and Maintenance (device type 52)
 *                DALI Part 253
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_DIAG_MAINTENANCE_H_
#define _GEAR_DIAG_MAINTENANCE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_DIAG_MAINTENANCE_EXTENDED_VERSION_NUMBER    0x08  // bits 0-1 minor and bits 2-7 major extended version number
#define CTRL_GEAR_DIAG_MAINTENANCE_FIRST_MEMORY_BANK_NUMBER   205
#define CTRL_GEAR_DIAG_MAINTENANCE_SECOND_MEMORY_BANK_NUMBER  206
#define CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER   207

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_205_ADDRESS_LAST_CELL                   0x1C
#define CTRL_GEAR_MEMORY_BANK_205_VERSION                             0x01
// memory bank cell addresses
#define CTRL_GEAR_MEMORY_BANK_205_VERSION_BYTE                        0x03
#define CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE           0x04
#define CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_STOP_BYTE            0x07
#define CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE            0x08
#define CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_STOP_BYTE             0x0A
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_START_BYTE       0x0B
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_END_BYTE         0x0C
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_FREQ_BYTE        0x0D
#define CTRL_GEAR_MEMORY_BANK_205_POWER_FACTOR_BYTE                   0x0E
#define CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_BYTE              0x0F
#define CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_COUNT_BYTE        0x10
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_BYTE        0x11
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_COUNT_BYTE  0x12
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_BYTE         0x13
#define CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_COUNT_BYTE   0x14
#define CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_BYTE             0x15
#define CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_COUNT_BYTE       0x16
#define CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_BYTE               0x17
#define CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_COUNT_BYTE         0x18
#define CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_BYTE               0x19
#define CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_COUNT_BYTE         0x1A
#define CTRL_GEAR_MEMORY_BANK_205_TEMPERATURE_BYTE                    0x1B
#define CTRL_GEAR_MEMORY_BANK_205_OUTPUT_CURRENT_PERCENT_BYTE         0x1C

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_206_ADDRESS_LAST_CELL                   0x20
#define CTRL_GEAR_MEMORY_BANK_206_VERSION                             0x01
// memory bank cell addresses
#define CTRL_GEAR_MEMORY_BANK_206_VERSION_BYTE                        0x03
#define CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE 0x04
#define CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_STOP_BYTE  0x06
#define CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_START_BYTE            0x07
#define CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_STOP_BYTE             0x09
#define CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE       0x0A
#define CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_STOP_BYTE        0x0D
#define CTRL_GEAR_MEMORY_BANK_206_ON_TIME_START_BYTE                  0x0E
#define CTRL_GEAR_MEMORY_BANK_206_ON_TIME_STOP_BYTE                   0x11
#define CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_START_BYTE                  0x12
#define CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_STOP_BYTE                   0x13
#define CTRL_GEAR_MEMORY_BANK_206_CURRENT_START_BYTE                  0x14
#define CTRL_GEAR_MEMORY_BANK_206_CURRENT_STOP_BYTE                   0x15
#define CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_BYTE              0x16
#define CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_COUNT_BYTE        0x17
#define CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_BYTE                  0x18
#define CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_COUNT_BYTE            0x19
#define CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_BYTE                   0x1A
#define CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_COUNT_BYTE             0x1B
#define CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_BYTE               0x1C
#define CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_COUNT_BYTE         0x1D
#define CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_BYTE               0x1E
#define CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_COUNT_BYTE         0x1F
#define CTRL_GEAR_MEMORY_BANK_206_THEMTERATURE_BYTE                   0x20

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_207_ADDRESS_LAST_CELL                   0x07
#define CTRL_GEAR_MEMORY_BANK_207_VERSION_BYTE                        0x03
#define CTRL_GEAR_MEMORY_BANK_207_LIFE_OF_LUMINAIRE_BYTE              0x04
#define CTRL_GEAR_MEMORY_BANK_207_INTERNAL_REF_TEMPERATURE_BYTE       0x05
#define CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_START_BYTE      0x06
#define CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_STOP_BYTE       0x07



#define CTRL_GEAR_DIAG_MAINTENANCE_START_COUNTER_THRESHOLD  6000 // in 100us

// enum for the diagnostics and maintenance commands
typedef enum
{
  DIAG_MAINTENANCE_QUERY_EXTENDED_VERSION_NUMBER,
  DIAG_MAINTENANCE_CMD_LAST
}DIAG_MAINTENANCE_CMD;


// diagnostics and maintenance variables
typedef struct
{
  uint8_t      extendedVersionNumber;
  uint8_t      deviceType;
}diag_maintenance_variables_t;

/* Memory bank 205 contains information about control gear diagnostics and maintenance.
 * Memory bank 205 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank205_RAM_t contains volatile random access memory cells of memory bank 205,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank205_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
  uint8_t     controlGearExternalSupplyVoltage[2];
  uint8_t     controlGearExternalSupplyVoltageFrequency;
  uint8_t     controlGearPowerFactor;
  uint8_t     controlGearOverallFailureCondition;
  uint8_t     controlGearExternalSupplyUndervoltage;
  uint8_t     controlGearExternalSupplyOvervoltage;
  uint8_t     controlGearOutputPowerLimitation;
  uint8_t     controlGearThermalDerating;
  uint8_t     controlGearThermalShutdown;
  uint8_t     controlGearTemperature;
  uint8_t     controlGearOutputCurrentPercent;
}dali_ctrl_mem_bank205_RAM_t;

/* Memory bank 205 contains information about control gear diagnostics and maintenance.
 * Memory bank 205 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank205_NVM_t contains non-volatile memory cells of memory bank 205,
 * volatile random access memory cells are stored in dali_ctrl_mem_bank205_RAM_t.
 */
typedef struct
{
  uint8_t     controlGearOperatingTime[4];
  uint8_t     controlGearStartCounter[3];
  uint8_t     controlGearOverallFailureConditionCounter;
  uint8_t     controlGearExternalSupplyUndervoltageCounter;
  uint8_t     controlGearExternalSupplyOvervoltageCounter;
  uint8_t     controlGearOutputPowerLimitationCounter;
  uint8_t     controlGearThermalDeratingCounter;
  uint8_t     controlGearThermalShutdownCounter;
}dali_ctrl_mem_bank205_NVM_t;

/* Memory bank 206 contains information about light source diagnostics and maintenance.
 * Memory bank 206 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank206_RAM_t contains volatile random access memory cells of memory bank 206,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank206_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
  uint8_t     lightSourceVoltage[2];
  uint8_t     lightSourceCurrent[2];
  uint8_t     lightSourceOverallFailureCondition;
  uint8_t     lightSourceShortCircuit;
  uint8_t     lightSourceOpenCircuit;
  uint8_t     lightSourceThermalDerating;
  uint8_t     lightSourceThermalShutdown;
  uint8_t     lightSourceTemperature;
}dali_ctrl_mem_bank206_RAM_t;

/* Memory bank 206 contains information about light source diagnostics and maintenance.
 * Memory bank 206 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank206_NVM_t contains non-volatile memory cells of memory bank 206,
 * volatile random access memory cells are stored in dali_ctrl_mem_bank206_RAM_t.
 */
typedef struct
{
  uint8_t     lightSourceStartCounterResettable[3];
  uint8_t     lightSourceStartCounter[3];
  uint8_t     lightSourceOnTimeResettable[4];
  uint8_t     lightSourceOnTime[4];
  uint8_t     lightSourceOverallFailureConditionCounter;
  uint8_t     lightSourceShortCircuitCounter;
  uint8_t     lightSourceOpenCircuitCounter;
  uint8_t     lightSourceThermalDeratingCounter;
  uint8_t     lightSourceThermalShutdownCounter;
}dali_ctrl_mem_bank206_NVM_t;

/* Memory bank 207 contains information about luminaire maintenance data.
 * Memory bank 207 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank207_RAM_t contains volatile random access memory cells of memory bank 207,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank207_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
}dali_ctrl_mem_bank207_RAM_t;

/* Memory bank 207 contains information about luminaire maintenance data.
 * Memory bank 207 shall be active if diagnostics and maintenance is present.
 * dali_ctrl_mem_bank207_NVM_t contains non-volatile memory cells of memory bank 207,
 * volatile random access memory cells are stored in dali_ctrl_mem_bank207_RAM_t.
 */
typedef struct
{
  uint8_t     ratedMedianUsefulLifeOfLuminaire;
  uint8_t     internalControlGearReferenceTemperature;
  uint8_t     ratedMedianUsefulLightSourceStarts[2];
}dali_ctrl_mem_bank207_NVM_t;

#ifdef __cplusplus
}
#endif


#endif // _GEAR_DIAG_MAINTENANCE_H_
