/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear - Colour Control (device type 8)
 *          IEC 62386-209
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_COLOUR_H_
#define _GEAR_COLOUR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_COLOUR_CALIBRATION_TIME_MAX  (15 * 60 * 1000) // 15 min

// enum for the colour control commands
typedef enum
{
  COLOUR_CMD_SET_TEMP_XCOORDINATE,
  COLOUR_CMD_SET_TEMP_YCOORDINATE,
  COLOUR_CMD_ACTIVATE,
  COLOUR_CMD_XCOORDINATE_STEP_UP,
  COLOUR_CMD_XCOORDINATE_STEP_DOWN,
  COLOUR_CMD_YCOORDINATE_STEP_UP,
  COLOUR_CMD_YCOORDINATE_STEP_DOWN,
  COLOUR_CMD_SET_TEMP_COLOUR_TEMPERATURE,
  COLOUR_CMD_COLOUR_TEMPERATURE_STEP_COOLER,
  COLOUR_CMD_COLOUR_TEMPERATURE_STEP_WARMER,
  COLOUR_CMD_SET_TEMP_PRIMARY_N_DIMLEVEL,
  COLOUR_CMD_SET_TEMP_RGB_DIMLEVEL,
  COLOUR_CMD_SET_TEMP_WAF_DIMLVEL,
  COLOUR_CMD_SET_TEMP_RGBWAF_CONTROL,
  COLOUR_CMD_COPY_REPORT_TO_TEMP,
  COLOUR_CMD_STORE_TY_PRIMARY_N,
  COLOUR_CMD_STORE_XY_COORDINATE_PRIMARY_N,
  COLOUR_CMD_STORE_COLOUR_TEMPERATURE_LIMIT,
  COLOUR_CMD_STORE_GEAR_FEATURES_STATUS,
  COLOUR_CMD_ASSIGN_COLOUR_TO_LINKED_CHANNEL,
  COLOUR_CMD_START_AUTO_CALIBRATION,
  COLOUR_CMD_QUERY_GEAR_FEATURES_STATUS,
  COLOUR_CMD_QUERY_COLOUR_STATUS,
  COLOUR_CMD_QUERY_COLOUR_TYPE_FEAUTRES,
  COLOUR_CMD_QUERY_COLOUR_VALUE,
  COLOUR_CMD_QUERY_RGBWAF_CONTROL,
  COLOUR_CMD_QUERY_ASSIGNED_COLOUR,
  COLOUR_CMD_QUERY_EXTENTED_VERSION_NUMBER,
  COLOUR_CMD_LAST
}COLOUR_CMD;

#define DALI_GEAR_COLOUR_MAX_SCENES      16     // 16 scenes

typedef struct {
  uint16_t xCoordinate;
  uint16_t yCoordinate;
}xyCoordinate_value_t;

typedef struct {
  xyCoordinate_value_t value;
  xyCoordinate_value_t report;
  xyCoordinate_value_t temp;
}xyCoordinate_volatile_t;

//! xy-Coordinate and Primary N colour type specific persistent variables
typedef struct {
  colour_space_t       colourSpaceType;
  uint16_t             colourSpaceValue[3][2];
  uint16_t             xCoordinatePrime[6];
  uint16_t             yCoordinatePrime[6];
  uint16_t             tyPrime[6];
}xyCoord_primaryN_pers_t;

//! xy-Coordinate colour type specific variables
typedef struct {
  xyCoordinate_volatile_t volatileValues;
  xyCoord_primaryN_pers_t persistentValues;
  xyCoordinate_vendor_t   vendorValues;
}xyCoordinate_t;


typedef struct {
  uint16_t tempColourTemperature;
  uint16_t reportColourTemperature;
  uint16_t ColourTemperature;
}colourTemperature_volatile_t;

//! Colour Temperature colour type specific persistent variables
typedef struct {
  uint16_t ColourTemperatureCoolest;
  uint16_t ColourTemperatureWarmest;
  uint16_t ColourTemperaturePhyCoolest;
  uint16_t ColourTemperaturePhyWarmest;
}colourTemperature_pers_t;

//! Colour Temperature colour type specific variables
typedef struct {
  colourTemperature_volatile_t volatileValues;
  colourTemperature_pers_t     persistentValues;
  colourTemperature_vendor_t   vendorValues;
}colourTemperature_t;

typedef struct {
  xyCoordinate_value_t temp;
  uint16_t             tempPrimeNdimlevel[6];
  uint16_t             reportPrimeNdimlevel[6];
  uint16_t             primeNdimlevel[6];
}primaryN_volatile_t;

//! Primary N colour type specific variables
typedef struct {
  primaryN_volatile_t     volatileValues;
  xyCoord_primaryN_pers_t persistentValues;
  primaryN_vendor_t       vendorValues;
}primaryN_t;

typedef struct {
  uint8_t  RGBWAFcontrol;
  uint8_t  red;
  uint8_t  green;
  uint8_t  blue;
  uint8_t  white;
  uint8_t  amber;
  uint8_t  free;
}rgbwaf_dimlevel_t;

typedef struct {
  rgbwaf_dimlevel_t dimlevel;
  rgbwaf_dimlevel_t tempDimlevel;
  rgbwaf_dimlevel_t reportDimlevel;
}rgbwaf_volatile_t;

typedef struct {
    uint8_t assignedColour[6];
}rgbwaf_pers_t;

typedef struct {
    uint8_t xyCoordinateOutOfRange :1;
    uint8_t temperatureOutOfRange  :1;
    uint8_t calibrationRunning     :1;
    uint8_t calibrationsucceeded   :1;
    uint8_t xyCoordinateActive     :1;
    uint8_t temperatureActive      :1;
    uint8_t primaryNactive         :1;
    uint8_t RGBWAFactive           :1;
}colour_status_t;

// colour variables
typedef struct
{
  dalilib_time    calibrationStartTime;
  uint8_t         tempColourType;
  uint8_t         reportColourType;
  colour_status_t colourStatus;

  //! Colour Type specific variables
  union
  {
    xyCoordinate_volatile_t      xyCoordinate;
    colourTemperature_volatile_t colourTemperature;
    primaryN_volatile_t          primaryN;
    rgbwaf_volatile_t            rgbwaf;
  };
}colour_variables_t;

typedef union
{
  xyCoordinate_value_t xyCoordinateValue;
  uint16_t             ColourTemperature;
  uint16_t             PrimeNdimlevel[6];
  rgbwaf_dimlevel_t    rgbwafDimlevel;
}colour_value_t;

// colour persistent variables
typedef struct
{
  // all Colour types
  uint8_t        sceneColourType[DALI_GEAR_COLOUR_MAX_SCENES];
  colour_value_t sceneColourValue[DALI_GEAR_COLOUR_MAX_SCENES];
  uint8_t        powerOnColourType;
  colour_value_t powerOnColourValue;
  uint8_t        SystemFailureColourType;
  colour_value_t SystemFailureColourValue;
  uint8_t        gearFeatureStatus;
  uint8_t        colourTypeFeatures;
  union
  {
    xyCoord_primaryN_pers_t  xyCoordinate;
    colourTemperature_pers_t colourTemperature;
    xyCoord_primaryN_pers_t  primaryN;
    uint8_t                  assignedColour[6]; //!< RGBWAF
  };
}colour_pers_variables_t;


#ifdef __cplusplus
}
#endif


#endif // _GEAR_COLOUR_H_
