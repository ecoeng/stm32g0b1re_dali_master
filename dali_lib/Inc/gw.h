/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GATEWAY_H_
#define _GATEWAY_H_

#ifdef __cplusplus
extern "C" {
#endif


//----------------------------------------------------------------------
// gateway API functions, structs, enumarations and constants
//----------------------------------------------------------------------
typedef struct
{
	uint8_t		shortAddress;
}dali_ctrl_gw_cfg_t;



#ifdef __cplusplus
}
#endif


#endif // _GATEWAY_H_
