/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear extension Energy
 *                Reporting (device type 51)
 *                DALI Part 252
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_ENERGY_REPORTING_H_
#define _GEAR_ENERGY_REPORTING_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_ENERGY_REPORTING_EXTENDED_VERSION_NUMBER  0x08  // bits 0-1 minor and bits 2-7 major extended version number
#define CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER 202
// TODO implement optional memory banks 203, 204

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_202_ADDRESS_LAST_CELL   0x0F
#define CTRL_GEAR_MEMORY_BANK_202_VERSION             0x01
// memory bank cell addresses
#define CTRL_GEAR_MEMORY_BANK_202_VERSION_BYTE              0x03
#define CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_POWER_BYTE   0x04
#define CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_ENERGY_BYTE  0x0B
#define CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE         0x05
#define CTRL_GEAR_MEMORY_BANK_202_ENERGY_END_BYTE           0x0A
#define CTRL_GEAR_MEMORY_BANK_202_POWER_START_BYTE          0x0C
#define CTRL_GEAR_MEMORY_BANK_202_POWER_END_BYTE            0x0F


// enum for the energy reporting extension commands
typedef enum
{
  ENERGY_REPORTING_QUERY_EXTENDED_VERSION_NUMBER,
  ENERGY_REPORTING_CMD_LAST
}ENERGY_REPORTING_CMD;


// energy reporting extension variables
typedef struct
{
  uint8_t      extendedVersionNumber;
  uint8_t      deviceType;
}energy_reporting_variables_t;

/* Memory bank 202 contains information about active energy and power of the control gear.
 * Memory bank 202 shall be active if energy reporting is present.
 * dali_ctrl_mem_bank202_RAM_t contains volatile random access memory cells of memory bank 202,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank202_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
  uint8_t     activePower[4];
}dali_ctrl_mem_bank202_RAM_t;

/* Memory bank 202 contains information about active energy and power of the control gear.
 * Memory bank 202 shall be active if energy reporting is present.
 * dali_ctrl_mem_bank202_NVM_t contains non-volatile memory cells of memory bank 202,
 * volatile random access memory cells are stored in dali_ctrl_mem_bank202_RAM_t.
 */
typedef struct
{
  // Active energy
  uint8_t     activeEnergy[6];
}dali_ctrl_mem_bank202_NVM_t;

#ifdef __cplusplus
}
#endif


#endif // _GEAR_ENERGY_REPORTING_H_
