 /*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef DEFINES_H_
#define DEFINES_H_

//#define DALILIB_DEMO

#define DALILIB_DEMO_TIME        (1000 * 60 * 120) // 2h

#define DALILIB_CODE_VERSION_MAJOR 1
#define DALILIB_CODE_VERSION_MINOR 8
#define DALILIB_API_VERSION_MAJOR  1
#define DALILIB_API_VERSION_MINOR  6

#define DALILIB_VENDOR_ID 0x11223344

#define MAX_STACK_INSTANCE 3

#define MEM_BANK_SIZE      0xFF
// Frame-Flags
#define FRAME_TYPE_DALI_FRAME_FLAG  0x00
#define FRAME_TYPE_DALI_STATUS_FLAG 0x01

// Received DALI-frame data types
#define DALI_RESPONSE_FRAME            0x08
#define DALI_CTRL_GEAR_FORWARD_FRAME   0x10
#define DALI_CTRL_DEVICE_FORWARD_FRAME 0x18
#define DALI_RESERVED_FORWARD_FRAME_1  0x14
#define DALI_RESERVED_FORWARD_FRAME_2  0x20

// Timeout of the send confirmation by the low-level driver
#define DALI_CONFIRMATION_TIMEOUT 300  // ms
// Timeout of an expected DALI response frame
#define DALI_RESPONSE_TIMEOUT     13   // ms
// Timeout of the second frame of a send-twice command
#define DALI_SEND_TWICE_TIMEOUT   102   // ms

// Time before saving persistent var is executed, after receiving a new frame
#define DALI_SAVE_PERSISTENT_TIME 0  // ms; 0 = off
// Time delay before saving persistent variables, after a persistent variable changed
#define DALI_SAVE_PERS_DELAY_MAX ((uint32_t)29000)            // 29000ms

#define DALI_RESPONSE_VALUE_MASK  0xFF

#define COUNTOF(arr) (sizeof arr / sizeof arr[0])

#ifndef MAX
#define MAX(a,b)  (((a) > (b)) ? (a) : (b))
#endif

// DALI Constants
#define DALI_MASK  0xFF
#define DALI_MASK2 0xFFFF
#define DALI_YES   DALI_MASK

#define NOT_USED_PARAM 0x00

#ifndef TRUE
#define TRUE  1
#endif
#define FALSE 0

// DALI Frames priority
#define FRAME_PRIO_BF_P0 0
#define FRAME_PRIO_FF_P1 1
#define FRAME_PRIO_FF_P2 2
#define FRAME_PRIO_FF_P3 3
#define FRAME_PRIO_FF_P4 4
#define FRAME_PRIO_FF_P5 5
#define FRAME_PRIO_FF_P6 6
#define FRAME_PRIO_FF_P7 7

// DALI Frame length
#define FRAME_BF_LEN           0x08
#define FRAME_CTRL_GEAR_FF_LEN 0x10
#define FRAME_CTRL_DEV_FF_LEN  0x18

// Frame buffer length
#ifndef API_FRAMES_64BIT
#define FRAME_BUFFER_LEN       32 // bits
#else
#define FRAME_BUFFER_LEN       64 // bits
#endif
#define MIN_RAW_FRAME_LEN      40 // bits

#define MAX_DEVICE_TYPES 5

// MAXIMUM NUMBER OF ADDITIONAL MEMORYBANKS
#define MAXMEMBANKS 21        // means 0 + 20 -> 21
                              // additional are adressed from 1 to MAX-1

// MEMORY START & STOP BYTES
#define MEM_START "SDALI20"  // 7 - Byte
#define MEM_STOP  "EDALI"    // 5 - Byte

#define MEM_START_SIZE        7
#define MEM_STOP_SIZE         5
#define MEM_DATA_SIZE_SIZE    2 // RAW DATA SIZE
#define MEM_BLOCK_DATA_SIZE   2 // size of memory block data, i.e. blockId and version

#define MEM_SIZE          512
#define MEM_DATA_MAX_SIZE (MEM_SIZE + (MEM_START_SIZE + MEM_DATA_SIZE_SIZE + MEM_STOP_SIZE))

#define MSG_QUEUE_SIZE    8

#endif /* DEFINES_H_ */
