/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear extension Integrated
 *                Bus Power Supply (device type 49)
 *                DALI Part 250
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_POWER_SUPPLY_H_
#define _GEAR_POWER_SUPPLY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_POWER_SUPPLY_EXTENDED_VERSION_NUMBER  0x08  // bits 0-1 minor and bits 2-7 major extended version number
#define CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER       201

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_201_ADDRESS_LAST_CELL     0x06
#define CTRL_GEAR_MEMORY_BANK_201_VERSION               1
// memory bank cell addresses
#define CTRL_GEAR_MEMORY_BANK_201_STATUS_BYTE           0x06


// enum for the integrated power supply commands
typedef enum
{
  POWER_SUPPLY_QUERY_ACTIVE_POWER_SUPPLY,
  POWER_SUPPLY_QUERY_EXTENDED_VERSION_NUMBER,
  POWER_SUPPLY_CMD_LAST
}POWER_SUPPLY_CMD;

// integrated power supply variables
typedef struct
{
  uint8_t      extendedVersionNumber;
  uint8_t      deviceType;
}power_supply_variables_t;

/* Memory bank 201 contains information about the integrated bus power supply of the control gear.
 * Memory bank 201 shall be active if an integrated bus power supply is present.
 * dali_ctrl_mem_bank201_RAM_t contains volatile random access memory cells of memory bank 201,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank201_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
}dali_ctrl_gear_mem_bank201_RAM_t;

/* Memory bank 201 contains information about the integrated bus power supply of the control gear.
 * Memory bank 201 shall be active if an integrated bus power supply is present.
 * dali_ctrl_mem_bank201_NVM_t contains non-volatile memory cells of memory bank 201,
 * volatile memory cells are stored in dali_ctrl_mem_bank201_RAM_t.
 */
typedef struct
{
  // Status of integrated bus power supply
  uint8_t     status;
}dali_ctrl_mem_bank201_NVM_t;

#ifdef __cplusplus
}
#endif


#endif // _GEAR_POWER_SUPPLY_H_
