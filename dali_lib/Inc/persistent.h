/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Persistent memory blocks
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _PERSISTENT_H_
#define _PERSISTENT_H_

#ifdef __cplusplus
    extern "C" {
#endif

// memory block Ids are shared with gear block Ids
#define DALI_CTRL_DEVICE_PERS_VARS_OPERATING_ID             1
#define DALI_CTRL_DEVICE_PERS_VARS_CONFIG_ID                2
#define DALI_CTRL_DEVICE_PERS_INST_OPERATING_ID             3
#define DALI_CTRL_DEVICE_PERS_INST_CONFIG_ID                4
#define DALI_CTRL_DEVICE_PERS_LUMINAIRE_ID                  5
#define DALI_CTRL_GEAR_PERS_VARS_FREQUENT_ID                101
#define DALI_CTRL_GEAR_PERS_VARS_OPERATING_ID               102
#define DALI_CTRL_GEAR_PERS_VARS_CONFIG_ID                  103
#define DALI_CTRL_GEAR_PERS_FLUORESCENT_ID                  104
#define DALI_CTRL_GEAR_PERS_LED_FREQUENT_ID                 105
#define DALI_CTRL_GEAR_PERS_LED_OPERATING_ID                106
#define DALI_CTRL_GEAR_PERS_LED_CONFIG_ID                   107
#define DALI_CTRL_GEAR_PERS_SWITCHING_ID                    108
#define DALI_CTRL_GEAR_PERS_COLOUR_ID                       109
#define DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_ID                110
#define DALI_CTRL_GEAR_PERS_POWER_SUPPLY_ID                 111
#define DALI_CTRL_GEAR_PERS_ENERGY_REPORT_ID                112
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_ID  113
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_ID  114
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_ID  115

// version of the memory block data structure
#define DALI_CTRL_DEVICE_PERS_VARS_OPERATING_VERSION             1
#define DALI_CTRL_DEVICE_PERS_VARS_CONFIG_VERSION                1
#define DALI_CTRL_DEVICE_PERS_INST_OPERATING_VERSION             1
#define DALI_CTRL_DEVICE_PERS_INST_CONFIG_VERSION                1
#define DALI_CTRL_DEVICE_PERS_LUMINAIRE_VERSION                  1
#define DALI_CTRL_GEAR_PERS_VARS_FREQUENT_VERSION                1
#define DALI_CTRL_GEAR_PERS_VARS_OPERATING_VERSION               1
#define DALI_CTRL_GEAR_PERS_VARS_CONFIG_VERSION                  1
#define DALI_CTRL_GEAR_PERS_FLUORESCENT_VERSION                  1
#define DALI_CTRL_GEAR_PERS_LED_FREQUENT_VERSION                 1
#define DALI_CTRL_GEAR_PERS_LED_OPERATING_VERSION                1
#define DALI_CTRL_GEAR_PERS_LED_CONFIG_VERSION                   1
#define DALI_CTRL_GEAR_PERS_SWITCHING_VERSION                    1
#define DALI_CTRL_GEAR_PERS_COLOUR_VERSION                       1
#define DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_VERSION                1
#define DALI_CTRL_GEAR_PERS_POWER_SUPPLY_VERSION                 1
#define DALI_CTRL_GEAR_PERS_ENERGY_REPORT_VERSION                1
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_VERSION  1
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_VERSION  1
#define DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_VERSION  1


// persistent control device variables operating block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t  operatingMode;     // [0x00, 0x80..0xFF]
  uint8_t  applicationActive; // [TRUE, FALSE]
}dali_ctrl_device_pers_variables_operating_block_t;

// persistent control device variables config block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t  shortAddress;                 // [0..63,255] 255: no address
  uint32_t deviceGroups;                 // [0..0xFF FF FF FF]
  uint32_t randomAddress;                // [0..0x00 FF FF FF]
  uint8_t  numberOfInstance;             // [0..32]
  uint8_t  applicationControllerPresent; // [TRUE, FALSE]
  uint8_t  powerCycleNotification;       // [TRUE, FALSE]
}dali_ctrl_device_pers_variables_config_block_t;

// persistent control device instance variables operating block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t  instanceActive; // [TRUE, FALSE]
}dali_ctrl_device_inst_pers_variables_operating_block_t;

// persistent control device instance variables operating block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t  instanceGroup0;                  // [0..31, 255]
  uint8_t  instanceGroup1;                  // [0..31, 255]
  uint8_t  instanceGroup2;                  // [0..31, 255]
  uint8_t  resolution;                      // [1..255]
  uint8_t  instanceNumber;                  // [0..numberOfInstance-1]
  uint32_t eventFilter;                     // [0..0xFF FF FF]
  uint8_t  eventScheme;                     // [0..4] CTRL_DEVICE_EVENT_SCHEME
  uint8_t  eventPriority;                   // [2..5]
  INPUT_DEVICE_INSTANCE_TYPE  instanceType; // [0..31]
}dali_ctrl_device_inst_pers_variables_config_block_t;

// persistent luminaire-mounted control device variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     applicationControllerArbitration; // [0x00, 0xFE, 0xFF]
}dali_ctrl_device_luminaire_pers_variables_block_t;

// persistent control gear variables frequently changed block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t      lastLightLevel;                   // [0, minLevel..maxLevel]
}dali_ctrl_gear_pers_variables_frequent_block_t;

// persistent control gear variables operating block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t      fadeRate;                         // [1..15]
  uint8_t      fadeTime;                         // [0..15]
  uint8_t      extFadeTimeBase;                  // [0..15]
  uint8_t      extFadeTimeMultiple;              // [0..4]
  uint8_t      operatingMode;                    // [0x00, 0x80..0xFF]
}dali_ctrl_gear_pers_variables_operating_block_t;

// persistent control gear variables config block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t      highResDimming;                   // [TRUE, FALSE]
  double       phm;                              // [1, 254]
  uint8_t      powerOnLevel;                     // [0..255]
  uint8_t      systemFailureLevel;               // [0..255]
  uint8_t      minLevel;                         // [phm..maxLevel]
  uint8_t      maxLevel;                         // [minLevel..0xFE]
  uint8_t      shortAddress;                     // [0..63,255] 255: no Address:
  uint32_t     randomAddress;                    // [0..0xFF FF FF]
  uint16_t     gearGroups;                       // [0..0xFF FF]
  uint8_t      scene[16];                        // [0..0xFF]
  CTRL_GEAR_DEVICE_TYPE  gearDeviceType;
}dali_ctrl_gear_pers_variables_config_block_t;

// persistent control gear fluorescent lamp variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t      extendedVersionNumber;  // [0..2]
  uint8_t      deviceType;             // [0]
}dali_ctrl_gear_flamp_pers_variables_block_t;

// persistent control gear led variables frequently changed block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t             failureStatus;          // [0, 12, 32, 44, 64, 76, 96, 108, 128, 140, 160, 172, 192, 204, 224, 236]
  CURRENT_PROTECTOR   currentProtectorStatus; // [0..2]
}dali_ctrl_gear_led_pers_variables_frequent_block_t;

// persistent control gear led variables operating block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t             fft;            // [0, min_fft..27]
  uint8_t             operatingMode;  // [0x00, 0x80..0xFF]
  uint8_t             referenceValue;
}dali_ctrl_gear_led_pers_variables_operating_block_t;

// persistent control gear led variables config block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t             min_fft;               // [1..27]
  uint8_t             gearType;              // [0x00..0x0F]
  uint8_t             operatingModes;        // [0x00..0x0F]
  uint8_t             features;              // [0x00..0xFF]
  DIMMING_CURVE       dimmingCurve;          // [0..1]
  uint8_t             extendedVersionNumber; // [0..2]
  uint8_t             deviceType;            // [6]
}dali_ctrl_gear_led_pers_variables_config_block_t;

// persistent control gear colour variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t        sceneColourType[16];
  colour_value_t sceneColourValue[16];
  uint8_t        powerOnColourType;        // [0x10, 0x20, 0x40, 0x80]
  colour_value_t powerOnColourValue;       // [0..65535]
  uint8_t        SystemFailureColourType;  // [0x10, 0x20, 0x40, 0x80]
  colour_value_t SystemFailureColourValue; // [0..65535]
  uint8_t        gearFeatureStatus;        // (xx00 000x)
  uint8_t        colourTypeFeatures;
}dali_ctrl_gear_colour_pers_variables_block_t;

// persistent control gear switching variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     phm;                   // [254]
  uint8_t     minLevel;              // [1..maxLevel]
  uint8_t     maxLevel;              // [minLevel..0xFE]
  uint8_t     upSwitchOnThr;         // [1..255]
  uint8_t     upSwitchOffThr;        // [1..255]
  uint8_t     downSwitchOnThr;       // [0..255]
  uint8_t     downSwitchOffThr;      // [0..255]
  uint8_t     errorHoldOffTimer;     // [0..255]
  uint8_t     features;              // (xx0x x00x)
  uint8_t     gearType;              // [0x00..0x1F]
  uint8_t     switchStatus;          // [0..255]
  uint8_t     referenceValue;
  uint8_t     extendedVersionNumber; // [0..2]
  uint8_t     deviceType;            // [7]
}dali_ctrl_gear_switching_pers_variables_block_t;

// persistent control gear mem bank 1 extension variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     gtin[6];                 // [0..255]
  uint8_t     identify_number[8];      // [0..255]
  uint8_t     content_format_id[2];    // [0x0003]
  uint8_t     year_manufactured;       // [0..255]
  uint8_t     week_manufactured;       // [0..255]
  uint8_t     nom_input_power[2];      // [0..255]
  uint8_t     min_dim_level_power[2];  // [0..255]
  uint8_t     nom_min_ac_voltage[2];   // [0..255]
  uint8_t     nom_max_ac_voltage[2];   // [0..255]
  uint8_t     nom_light_output[3];     // [0..255]
  uint8_t     cri;                     // [0..255]
  uint8_t     cct[2];                  // [0..255]
  uint8_t     light_distribution_type; // [0..5]
  char        luminaire_color[24];
  char        identify[60];
}dali_ctrl_gear_mem_bank1_ext_pers_variables_block_t;

// persistent control gear power supply extension variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     status; //[0, 1]
}dali_ctrl_gear_power_supply_pers_variables_block_t;

// persistent control gear energy reporting extension variables block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     activeEnergy[6]; // [0..255]
}dali_ctrl_gear_energy_report_pers_variables_block_t;

// persistent control gear diagnostics and maintenance extension memory bank 205 block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     controlGearOperatingTime[4];                  // [0..255]
  uint8_t     controlGearStartCounter[3];                   // [0..255]
  uint8_t     controlGearOverallFailureConditionCounter;    // [0..255]
  uint8_t     controlGearExternalSupplyUndervoltageCounter; // [0..255]
  uint8_t     controlGearExternalSupplyOvervoltageCounter;  // [0..255]
  uint8_t     controlGearOutputPowerLimitationCounter;      // [0..255]
  uint8_t     controlGearThermalDeratingCounter;            // [0..255]
  uint8_t     controlGearThermalShutdownCounter;            // [0..255]
}dali_ctrl_gear_diag_maintenance_pers_mem_bank205_t;

// persistent control gear diagnostics and maintenance extension memory bank 206 block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     lightSourceStartCounterResettable[3];      // [0..255]
  uint8_t     lightSourceStartCounter[3];                // [0..255]
  uint8_t     lightSourceOnTimeResettable[4];            // [0..255]
  uint8_t     lightSourceOnTime[4];                      // [0..255]
  uint8_t     lightSourceOverallFailureConditionCounter; // [0..255]
  uint8_t     lightSourceShortCircuitCounter;            // [0..255]
  uint8_t     lightSourceOpenCircuitCounter;             // [0..255]
  uint8_t     lightSourceThermalDeratingCounter;         // [0..255]
  uint8_t     lightSourceThermalShutdownCounter;         // [0..255]
}dali_ctrl_gear_diag_maintenance_pers_mem_bank206_t;

// persistent control gear diagnostics and maintenance extension memory bank 207 block
typedef struct
{
  // meta data
  uint8_t  id;
  uint8_t  version;

  // payload
  uint8_t     ratedMedianUsefulLifeOfLuminaire;        // [0..255]
  uint8_t     internalControlGearReferenceTemperature; // [0..255]
  uint8_t     ratedMedianUsefulLightSourceStarts[2];   // [0..255]
}dali_ctrl_gear_diag_maintenance_pers_mem_bank207_t;


#ifdef __cplusplus
    }
#endif

#endif // _DALISTACK_H_
