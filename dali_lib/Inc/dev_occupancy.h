/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                IEC 62386-303
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DEV_OCCUPANCY_SENSOR__H_
#define _DEV_OCCUPANCY_SENSOR__H_

#ifdef __cplusplus
    extern "C" {
#endif

#define INPUT_DEVICE_OCCUPANCY_SENSOR_RESOLUTION                 2
#define INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_PRIO_DEFAULT         4
#define INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_FILTER_DEFAULT    0x03 // 0000 0011b

#define INPUT_DEVICE_OCCUPANCY_SENSOR_DEADTIME_DEFAULT           2
#define INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_DEFAULT              90
#define INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_DEFAULT            20

#define INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_INCR               50 // [ms] 50 ms
#define INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_INCR            10000 // [ms] 10 s
#define INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_INCR           1000 // [ms] 1 s

#define INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_DEFAULT           100 // [ms] 100 ms
#define INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_DEFAULT        900000 // [ms] 15 min.
#define INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_DEFAULT       20000 // [ms] 20 s

#define INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_MIN                 0 // [ms] 0 s
#define INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MIN              1000 // [ms] 1 s
#define INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_MIN            1000 // [ms] 1 s

#define INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_MAX             12750 // [ms] 12,75 s
#define INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MAX           2538000 // [ms] 42,3 min
#define INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_MAX          255000 // [ms] 4 min 15 s


// enum for the occupancy sensor commands
typedef enum _OCCUPANCY_SENSOR_CMD
{
  OCCUPANCY_CMD_CATCH_MOVEMENT = 0,
  OCCUPANCY_CMD_SET_HOLD_TIMER,
  OCCUPANCY_CMD_SET_REPORT_TIMER,
  OCCUPANCY_CMD_SET_DEADTIME_TIMER,
  OCCUPANCY_CMD_CANCEL_HOLD_TIMER,

  OCCUPANCY_CMD_QUERY_DEADTIME_TIMER,
  OCCUPANCY_CMD_QUERY_HOLD_TIMER,
  OCCUPANCY_CMD_QUERY_REPORT_TIMER,
  OCCUPANCY_CMD_QUERY_CATCHING,
  OCCUPANCY_CMD_LAST,
}OCCUPANCY_SENSOR_CMD;

typedef enum {
  NO_MOVEMENT,
  MOVEMENT
}MOVEMENT_STATE;

typedef enum {
  VACANT,
  OCCUPIED
}AREA_STATE;

typedef struct{
  MOVEMENT_STATE movementState;
  MOVEMENT_STATE lastMovementState;
  AREA_STATE     areaState;
  AREA_STATE     lastAreaState;
}state_t;

// unit of timer ms
typedef struct _occupancy_sensor_pers_variables_t
{
  uint8_t tDeadtime; // [0, 255]
  uint8_t tHold;     // [0, 254]
  uint8_t tReport;   // [0, 255]
}occupancy_sensor_pers_variables_t;

typedef struct _occupancy_sensor_variables_t
{
  uint8_t  bCatching;              // [TRUE,FALSE]
  uint8_t  bPhysicalSensorFailure; // [TRUE,FALSE]
  uint8_t  sensor_type;            // [OCCUPANCY_MOVEMENT_SENSOR|OCCUPANCY_PRESENCE_SENSOR]
  uint32_t timeDead;
  uint32_t timeHold;
  uint32_t timeReport;
  state_t  state;
  uint32_t timeLastMovementEvent;  // used for check the hold timer, time stamp for last movement event [ms]
  uint32_t timeLastEvent;          // time stamp for last event [ms]
}occupancy_sensor_variables_t;

#ifdef __cplusplus
    }
#endif


#endif // _DEV_OCCUPANCY_SENSOR__H_
