/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                Particular requirements for control gear extension Memory Bank
 *                1 Extension (device type 50)
 *                DALI Part 251
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _GEAR_MEM_BANK1_EXT_H_
#define _GEAR_MEM_BANK1_EXT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CTRL_GEAR_MEM_BANK1_EXT_EXTENDED_VERSION_NUMBER  0x08  // bits 0-1 minor and bits 2-7 major extended version number
#define CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER       1

// memory bank default values
#define CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL     0x77
#define CTRL_GEAR_MEMORY_BANK_1_CONTENT_FORMAT_ID0    0x00
#define CTRL_GEAR_MEMORY_BANK_1_CONTENT_FORMAT_ID1    0x03


// enum for the memory bank 1 extension commands
typedef enum
{
  MEM_BANK1_EXT_QUERY_EXTENDED_VERSION_NUMBER,
  MEM_BANK1_EXT_CMD_LAST
}MEM_BANK1_EXT_CMD;


// memory bank 1 extension variables
typedef struct
{
  uint8_t      extendedVersionNumber;
  uint8_t      deviceType;
}mem_bank1_ext_variables_t;

/* Memory bank 1 contains additional OEM information about the control gear.
 * Memory bank 1 shall be active if memory bank 1 extension is present.
 * dali_ctrl_mem_bank1_RAM_t contains volatile random access memory cells of memory bank 1,
 * non-volatile memory cells are stored in dali_ctrl_mem_bank1_NVM_t.
 */
typedef struct
{
  // Lock byte
  uint8_t     lockByte;
}dali_ctrl_mem_bank1_RAM_t;

/* Memory bank 1 contains additional OEM information about the control gear.
 * Memory bank 1 shall be active if memory bank 1 extension is present.
 * dali_ctrl_mem_bank1_NVM_t contains non-volatile memory cells of memory bank 1,
 * volatile memory cells are stored in dali_ctrl_mem_bank1_RAM_t.
 */
typedef struct
{
  // Luminaire manufacturer GTIN with manufacturer specific prefix to derive manufacturer name
  uint8_t     gtin[6];
  // Luminaire identification number
  uint8_t     identify_number[8];
  // Content Format ID
  uint8_t     content_format_id[2];
  // Luminaire year of manufacture
  uint8_t     year_manufactured;
  // Luminaire week of manufacture
  uint8_t     week_manufactured;
  // Nominal Input Power
  uint8_t     nom_input_power[2];
  // Power at minimum dim level
  uint8_t     min_dim_level_power[2];
  // Nominal Minimum AC mains voltage
  uint8_t     nom_min_ac_voltage[2];
  // Nominal Maximum AC mains voltage
  uint8_t     nom_max_ac_voltage[2];
  // Nominal light output
  uint8_t     nom_light_output[3];
  // CRI
  uint8_t     cri;
  // CCT
  uint8_t     cct[2];
  // Light Distribution Type
  uint8_t     light_distribution_type;
  // Luminaire color
  char        luminaire_color[24];
  // Luminaire identification
  char        identify[60];
}ctrl_gear_mem_bank1_NVM_t;

// Memory bank 1 direct access struct
typedef struct
{
  union
  {
    // The first 3 cells of memory bank 1 are stored in volatile memory
    uint8_t                    data[(CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL + 1) - 3];
    ctrl_gear_mem_bank1_NVM_t  ctrl;
  };
}dali_ctrl_mem_bank1_NVM_t;

#ifdef __cplusplus
}
#endif


#endif // _GEAR_MEM_BANK1_EXT_H_
