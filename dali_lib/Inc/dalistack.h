/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DALISTACK_H_
#define _DALISTACK_H_

#ifdef __cplusplus
    extern "C" {
#endif

#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<stdarg.h>
#include<stdlib.h>
#include<math.h>

#include "libdali.h"
#include "defines.h"
#include "enums.h"
#include "error.h"
#include "gw.h"
#include "gear_led.h"
#include "gear_colour.h"
#include "gear_switching.h"
#include "gear_power_supply.h"
#include "gear_mem_bank1_ext.h"
#include "gear_energy_reporting.h"
#include "gear_diag_maintenance.h"
#include "gear.h"
#include "dev_pushbutton.h"
#include "dev_occupancy.h"
#include "dev_lightsensor.h"
#include "dev_luminaire.h"
#include "device.h"
#include "persistent.h"

typedef struct _send_frame_t
{
  SEND_FRAME_TYPE type;
  FRAME_STATUS    status;
  dalilib_frame_t frame;
  int8_t          cmdCounter;
  uint8_t         cmds[MSG_QUEUE_SIZE+1];
  uint8_t         cmdValues[MSG_QUEUE_SIZE+1];
  union
  {
    gear_adr_t   lastGearAdr;
    device_adr_t lastDeviceAdr;
  }adr;
  union
  {
    GEAR_CMD eGearCmd;
    DEV_CMD  eDevCmd;
  }lastCmd;
  uint8_t lastActionType;
  uint8_t lastAction;
}send_frame_t;

typedef struct _recv_frame_t
{
  uint8_t        bWaitTwice;
  dalilib_frame_t   frame;
}recv_frame_t;

// instance of the control gear/device/gateway
typedef struct ctrl_instance_t
{
  dali_ctrl_gear_instance_t   gearInstance;
  dali_ctrl_device_instance_t deviceInstance;
}dali_ctrl_instance_t;


typedef struct instance_t
{
  uint8_t              isUsed;         // 0: not used, 1: used
  uint8_t              instanceID;    // unique instance ID for each DALI-library instance
  STACK_STATUS         status;
  dalilib_time         runTime;       // instance run time, used for demo
  dalilib_time         currentTime;
  dalilib_time         waitTimeout;   // timer for tx-confirmation, response frame
  send_frame_t         last_tx_frame; // last transmitted frame
  recv_frame_t         last_rx_frame; // last received frame
  dalilib_cfg_t        config;
  dali_ctrl_instance_t ctrlInst;
}dali_instance_t;

typedef dali_instance_t* PDaliLibInstance;

//------------------------------------------------------------------
// dalistack.c:
//------------------------------------------------------------------
R_DALILIB_RESULT dali_cb_send(PDaliLibInstance pInst, send_frame_t* frame);
void             dali_cb_ready(PDaliLibInstance pInst);
R_DALILIB_RESULT dali_cb_save_mem(PDaliLibInstance pInst,void* pMem,uint16_t nSize);
R_DALILIB_RESULT dali_cb_save_mem_block(PDaliLibInstance pInst,
                                        void*            pMem,
                                        uint16_t         nSize,
                                        uint8_t          blockId);
R_DALILIB_RESULT dali_cb_load_mem(PDaliLibInstance pInst,void* pMem);
R_DALILIB_RESULT dali_cb_load_mem_block(PDaliLibInstance pInst,
                                        uint8_t          blockId);
R_DALILIB_RESULT dali_cb_reaction(PDaliLibInstance pInst, dalilib_action_t* pAction);
void             dali_cb_log(PDaliLibInstance pInst, R_DALILIB_RESULT errorNo, char *pLogTxt);
uint32_t         dali_difftime (uint32_t newtime, uint32_t oldtime);
void             dali_init_other_membanks(dali_ctrl_mem_bank_others_t* pOtherBanks);
R_DALILIB_RESULT dali_create_mem(PDaliLibInstance pInst,
                                 void*            pRawMem,
                                 uint16_t         nRawSize,
                                 uint8_t*         pDaliMem);
//------------------------------------------------------------------
// frame.c:
//------------------------------------------------------------------
R_DALILIB_RESULT frame_encode_ctrl_device_command_spec(ctrl_device_commands_t* pDevCmd,
                                                       uint8_t                 instanceByte,
                                                       uint8_t                 value,
                                                       uint8_t                 prio,
                                                       dalilib_frame_t*        pFF);
R_DALILIB_RESULT frame_encode_ctrl_device_command(ctrl_device_commands_t* pDevCmd,
                                                  device_adr_t*           pDeviceAdr,
                                                  uint8_t                 prio,
                                                  dalilib_frame_t*       pFF);
R_DALILIB_RESULT frame_encode_ctrl_gear_spec(ctrl_gear_commands_t* pGearCmd,
                                             uint8_t               value,
                                             uint8_t               prio,
                                             dalilib_frame_t*      pFF);
R_DALILIB_RESULT frame_encode_ctrl_gear(ctrl_gear_commands_t* pGearCmd,
                                        gear_adr_t*           pGearAdr,
                                        uint8_t               reserved,
                                        dalilib_frame_t*      pFF);
R_DALILIB_RESULT frame_process_dali_frame(PDaliLibInstance pInst,dalilib_frame_t* pFrame);
R_DALILIB_RESULT frame_process_status_frame(PDaliLibInstance pInst,dalilib_frame_t* pFrame);
FRAME_TYPE       frame_get_type(dalilib_frame_t* pFrame);

//------------------------------------------------------------------
// gear.c:
//------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_init(PDaliLibInstance pInst);
R_DALILIB_RESULT ctrl_gear_response_timeout(PDaliLibInstance            pInst,
                                            send_frame_t*               pFrame,
                                            DALILIB_RESPONSE_VALUE_TYPE valueType);
R_DALILIB_RESULT ctrl_gear_process_response_frame(PDaliLibInstance pInst,
                                                  dalilib_frame_t* pResponseFrame);
R_DALILIB_RESULT ctrl_gear_process_forward_frame(PCtrlGearInstance pCtrlInst,
                                                 dalilib_frame_t*  pForwardFrame);
R_DALILIB_RESULT ctrl_gear_action(PDaliLibInstance         pInst,
                                  dalilib_act_ctrl_gear_t* pAct,
                                  send_frame_t*            pNewSendFrame);
R_DALILIB_RESULT ctrl_gear_send_more(PDaliLibInstance pInst,
                                     send_frame_t*    pNewSendFrame);
R_DALILIB_RESULT ctrl_gear_set_systemfailure(PCtrlGearInstance pCtrlInst, uint8_t bFailure);
R_DALILIB_RESULT ctrl_gear_check_twice(PDaliLibInstance      pInst,
                                            dalilib_frame_t*      pForwardFrame,
                                            ctrl_gear_commands_t* pGearCmd);
R_DALILIB_RESULT ctrl_gear_apply_mem_block(PCtrlGearInstance pCtrlInst,
                                           uint8_t*          pDaliMem);

//------------------------------------------------------------------
// device.c:
//------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_init(PDaliLibInstance pInst);
R_DALILIB_RESULT ctrl_device_action(PCtrlDeviceInstance        pCtrlInst,
                                    dalilib_act_ctrl_device_t* pAct,
                                    send_frame_t*              pNewSendFrame);
R_DALILIB_RESULT ctrl_device_process_forward_frame(PCtrlDeviceInstance pCtrlInst,
                                                   dalilib_frame_t*    pForwardFrame);
R_DALILIB_RESULT ctrl_device_process_send_failed(PDaliLibInstance            pInst,
                                                 send_frame_t*               pFrame,
                                                 DALILIB_RESPONSE_VALUE_TYPE valueType);
R_DALILIB_RESULT ctrl_device_process_response_timeout(PDaliLibInstance            pInst,
                                                      send_frame_t*               pFrame,
                                                      DALILIB_RESPONSE_VALUE_TYPE valueType);
R_DALILIB_RESULT ctrl_device_process_response_frame(PDaliLibInstance pInst,
                                                    dalilib_frame_t* pResponseFrame);
R_DALILIB_RESULT ctrl_device_event_action(PCtrlDeviceInstance  pCtrlInst,
                                          dalilib_act_event_t* pAct,
                                          send_frame_t* pNewSendFrame);
R_DALILIB_RESULT ctrl_device_send_more(PDaliLibInstance pInst,
                                       send_frame_t*    pNewSendFrame);
R_DALILIB_RESULT ctrl_device_set_systemfailure(PCtrlDeviceInstance pCtrlInst,
                                               uint8_t             bFailure);
R_DALILIB_RESULT ctrl_input_device_create_event(PDaliLibInstance pInst,
                                                send_frame_t* pNewSendFrame,
                                                device_adr_t* pAdr,
                                                uint16_t*     pEventInfo);
R_DALILIB_RESULT ctrl_device_check_twice(PDaliLibInstance        pInst,
                                              dalilib_frame_t*        pForwardFrame,
                                              ctrl_device_commands_t* pDevCmd);
R_DALILIB_RESULT ctrl_device_apply_mem_block(PCtrlDeviceInstance pCtrlInst,
                                             uint8_t*            pDaliMem);

//------------------------------------------------------------------
// dev_pushbutton.c:
//------------------------------------------------------------------
R_DALILIB_RESULT        dev_pushbtn_eventinfo(PCtrlDeviceInstance           pCtrlInst,
                                              DALILIB_ACT_EVENT_PUSH_BUTTON pushButtonActionCode,
                                              uint16_t*                     pPushButtonEvent);
R_DALILIB_RESULT        dev_pushbtn_opcode(DALILIB_ACT_PUSH_BUTTON pushButtonActionCode,
                                           ctrl_device_commands_t* pDevCmd);
R_DALILIB_RESULT        dev_pushbtn_reactioncode(uint32_t                            frameData,
                                                 DALILIB_CTRL_DEVICE_REACTION_CODES* pPushButtonReActionCode);
uint8_t                 dev_pushbtn_get_error(PCtrlDeviceInstance pCtrlInst);
ctrl_device_commands_t* dev_pushbtn_cmd(uint8_t opcodeByte);
R_DALILIB_RESULT        dev_pushbtn_process_forward(PCtrlDeviceInstance     pCtrlInst,
                                                    ctrl_device_commands_t* pDevCmd,
                                                    uint8_t*                pRespValue);
void                    dev_pushbtn_by_reset(PCtrlDevInstPersistentVariables pInstPers);
void                    dev_pushbtn_init_persistent(PCtrlDevInstPersistentVariables pInstPers,
                                                   uint8_t                          shortMin,
                                                   uint8_t                          doubleMin);
uint8_t                dev_pushbtn_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers);
void                   dev_pushbtn_timingHelper(PCtrlDeviceInstance pCtrlInst);

//------------------------------------------------------------------
// dev_occupancy.c:
//------------------------------------------------------------------
R_DALILIB_RESULT        dev_occupancy_eventinfo(PCtrlDeviceInstance                pCtrlInst,
                                                DALILIB_ACT_EVENT_OCCUPANCY_SENSOR occupancySensorActionCode,
                                                uint16_t*                          pOccupancySensorEvent);
R_DALILIB_RESULT        dev_occupancy_opcode(DALILIB_ACT_OCCUPANCY_SENSOR occupancySensorActionCode,
                                             ctrl_device_commands_t*      pDevCmd);
R_DALILIB_RESULT        dev_occupancy_reactioncode(uint32_t                            frameData,
                                                   DALILIB_CTRL_DEVICE_REACTION_CODES* pOccupancySensorReActionCode);
uint8_t                 dev_occupancy_get_error(PCtrlDeviceInstance pCtrlInst);
ctrl_device_commands_t* dev_occupancy_cmd(uint8_t opcodeByte);
R_DALILIB_RESULT        dev_occupancy_process_forward(PCtrlDeviceInstance     pCtrlInst,
                                                      ctrl_device_commands_t* pDevCmd,
                                                      uint8_t*                pRespValue);
void                    dev_occupancy_by_reset(PCtrlDevVariables pVariables, PCtrlDevInstPersistentVariables pInstPers);
void                    dev_occupancy_init_persistent(PCtrlDevVariables pVariables, PCtrlDevInstPersistentVariables pInstPers);
uint8_t                 dev_occupancy_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers);
void                    dev_occupancy_timingHelper(PCtrlDeviceInstance pCtrlInst);

//------------------------------------------------------------------
// dev_lightsensor.c:
//------------------------------------------------------------------
R_DALILIB_RESULT        dev_lightsensor_eventinfo(PCtrlDeviceInstance          pCtrlInst,
                                                  dalilib_light_sensor_event_t lightSensorEvent,
                                                  uint16_t*                    pLightSensorEvent);
R_DALILIB_RESULT        dev_lightsensor_opcode(DALILIB_ACT_LIGHT_SENSOR lightSensorActionCode,
                                               ctrl_device_commands_t*  pDevCmd);
R_DALILIB_RESULT        dev_lightsensor_reactioncode(uint32_t                            frameData,
                                                     DALILIB_CTRL_DEVICE_REACTION_CODES* pLightSensorReActionCode);
uint8_t                 dev_lightsensor_get_error(PCtrlDeviceInstance pCtrlInst);
ctrl_device_commands_t* dev_lightsensor_cmd(uint8_t opcodeByte);
R_DALILIB_RESULT        dev_lightsensor_process_forward(PCtrlDeviceInstance     pCtrlInst,
                                                        ctrl_device_commands_t* pDevCmd,
                                                        uint8_t*                pRespValue);
void                    dev_lightsensor_by_reset(PCtrlDevVariables pVariables, PCtrlDevInstPersistentVariables pInstPers);
R_DALILIB_RESULT        dev_lightsensor_init_persistent(PCtrlDevVariables pVariables, PCtrlDevInstPersistentVariables pInstPers);
uint8_t                 dev_lightsensor_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers);
void                    dev_lightsensor_timingHelper(PCtrlDeviceInstance pCtrlInst);

//------------------------------------------------------------------
// gear_led.c:
//------------------------------------------------------------------
R_DALILIB_RESULT      gear_led_process_forward(PCtrlGearInstance      pCtrlInst,
                                               dalilib_frame_t*       pForwardFrame,
                                               ctrl_gear_commands_t** pGearCmd,
                                               gear_adr_t*            pReceivedAdr,
                                               uint8_t*               pRespValue);
ctrl_gear_commands_t* gear_led_get_cmd(LED_CMD eCmd);
void                  gear_led_init_after_power_cycle(dali_ctrl_gear_pers_mem_t*   pGearPers,
                                                      PCtrlGearVariables           pGearVariables);
void                  gear_led_by_reset(dali_ctrl_gear_pers_mem_t*   pGearPers);
void                  gear_led_init_persistent(PDaliLibInstance             pInst,
                                               dali_ctrl_gear_pers_mem_t*   pGearPers);
uint16_t              gear_led_get_fft(PCtrlGearInstance pCtrlInst);
uint8_t               gear_led_percent2level(uint32_t percentvalue);
uint32_t              gear_led_level2percent(double level);
uint8_t               gear_led_check_nvm_variables(dali_ctrl_gear_pers_mem_t*   pPersistent);
R_DALILIB_RESULT      gear_led_save_mem_block_pers_vars_frequent(PDaliLibInstance           pInst,
                                                                 dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_led_save_mem_block_pers_vars_operating(PDaliLibInstance           pInst,
                                                                  dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_led_save_mem_block_pers_vars_config(PDaliLibInstance           pInst,
                                                               dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_led_apply_mem_block_pers_vars_frequent(PCtrlGearInstance pCtrlInst,
                                                                  uint8_t*          pDaliMem);
R_DALILIB_RESULT      gear_led_apply_mem_block_pers_vars_operating(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t*          pDaliMem);
R_DALILIB_RESULT      gear_led_apply_mem_block_pers_vars_config(PCtrlGearInstance pCtrlInst,
                                                                uint8_t*          pDaliMem);

//------------------------------------------------------------------
// gear_switching.c:
//------------------------------------------------------------------
R_DALILIB_RESULT      gear_switching_process_forward(PCtrlGearInstance      pCtrlInst,
                                                     dalilib_frame_t*       pForwardFrame,
                                                     ctrl_gear_commands_t** pGearCmd,
                                                     gear_adr_t*            pReceivedAdr,
                                                     uint8_t*               pRespValue);
ctrl_gear_commands_t* gear_switching_get_cmd(SWITCHING_CMD eCmd);
void                  gear_switching_init_after_power_cycle (dali_ctrl_gear_pers_mem_t*   pGearPers,
                                                             PCtrlGearVariables           pGearVariables);
void                  gear_switching_by_reset(dali_ctrl_gear_pers_mem_t*   pPersistent);
void                  gear_switching_init_persistent(PDaliLibInstance             pInst,
                                                     dali_ctrl_gear_pers_mem_t*   pGearPers);
uint8_t               gear_switching_check_nvm_variables(dali_ctrl_gear_pers_mem_t*   pPersistentVariables);
R_DALILIB_RESULT      gear_switching_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                              dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_switching_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                               uint8_t*          pDaliMem);

//------------------------------------------------------------------
// gear_colour.c:
//------------------------------------------------------------------
R_DALILIB_RESULT      gear_colour_process_forward(PCtrlGearInstance      pCtrlInst,
                                                  dalilib_frame_t*       pForwardFrame,
                                                  ctrl_gear_commands_t** pGearCmd,
                                                  gear_adr_t*            pReceivedAdr,
                                                  uint8_t*               pRespValue);
R_DALILIB_RESULT gear_colour_process_forward_querys(PCtrlGearInstance      pCtrlInst,
                                                    dalilib_frame_t*       pForwardFrame,
                                                    ctrl_gear_commands_t** ppGearCmd,
                                                    gear_adr_t*            pReceivedAdr,
                                                    uint8_t*               pRespValue);
R_DALILIB_RESULT gear_colour_process_forward_level(PCtrlGearInstance      pCtrlInst,
                                                   dalilib_frame_t*       pForwardFrame,
                                                   ctrl_gear_commands_t** ppGearCmd,
                                                   gear_adr_t*            pReceivedAdr,
                                                   uint8_t*               pRespValue);
R_DALILIB_RESULT gear_colour_process_forward_configuration(PCtrlGearInstance      pCtrlInst,
                                                           dalilib_frame_t*       pForwardFrame,
                                                           ctrl_gear_commands_t** ppGearCmd,
                                                           gear_adr_t*            pReceivedAdr,
                                                           uint8_t*               pRespValue);
ctrl_gear_commands_t* gear_colour_get_cmd(COLOUR_CMD eCmd);
void                  gear_colour_init_after_power_cycle(dali_ctrl_gear_pers_mem_t*   pGearPers,
                                                         PCtrlGearVariables           pGearVariables);
void                  gear_colour_by_reset(dali_ctrl_gear_pers_mem_t*   pGearPers,
                                           PCtrlGearVariables           pGearVariables);
void                  gear_colour_init_persistent(PDaliLibInstance             pInst,
                                                  dali_ctrl_gear_pers_mem_t*   pGearPers);
uint8_t               gear_colour_check_nvm_variables(dali_ctrl_gear_pers_mem_t* pPersistentMemory);
R_DALILIB_RESULT      gear_colour_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                           dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_colour_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                            uint8_t*          pDaliMem);

//------------------------------------------------------------------
// gear_power_supply.c:
//------------------------------------------------------------------
#ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
R_DALILIB_RESULT      gear_power_supply_process_forward(PCtrlGearInstance      pCtrlInst,
                                                        dalilib_frame_t*       pForwardFrame,
                                                        ctrl_gear_commands_t** pGearCmd,
                                                        gear_adr_t*            pReceivedAdr,
                                                        uint8_t*               pRespValue);
void                  gear_power_supply_init_after_power_cycle(PCtrlGearInstance  pCtrlInst);
void                  gear_power_supply_init_persistent(PDaliLibInstance              pInst,
                                                        dali_ctrl_gear_pers_mem_t* pgearPersistent);
R_DALILIB_RESULT      gear_power_supply_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                          uint8_t           nMembank);
R_DALILIB_RESULT      gear_power_supply_read_memory_bank201(PCtrlGearInstance  pCtrlInst,
                                                            uint8_t*           pRespValue);
R_DALILIB_RESULT      gear_power_supply_write_memory_bank201(PCtrlGearInstance     pCtrlInst,
                                                             ctrl_gear_commands_t* pGearCmd,
                                                             uint8_t               reqValue,
                                                             uint8_t*              pRespValue);
R_DALILIB_RESULT      gear_power_supply_write_memory_bank201_direct(PCtrlGearInstance pCtrlInst,
                                                                    uint8_t           nCell,
                                                                    uint8_t           reqValue);
R_DALILIB_RESULT      gear_power_supply_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                                 dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT      gear_power_supply_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                                  uint8_t*          pDaliMem);
#endif /* DALILIB_GEAR_POWER_SUPPLY_PRESENT */

//------------------------------------------------------------------
// gear_mem_bank1_ext.c:
//------------------------------------------------------------------
#ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
R_DALILIB_RESULT gear_mem_bank1_ext_process_forward(PCtrlGearInstance      pCtrlInst,
                                                    dalilib_frame_t*       pForwardFrame,
                                                    ctrl_gear_commands_t** ppGearCmd,
                                                    gear_adr_t*            pReceivedAdr,
                                                    uint8_t*               pRespValue);
void             gear_mem_bank1_ext_init_after_power_cycle(PCtrlGearInstance pCtrlInst);
void             gear_mem_bank1_ext_init_persistent(PDaliLibInstance              pInst,
                                                    dali_ctrl_gear_pers_mem_t*    pGearPersistent);
R_DALILIB_RESULT gear_mem_bank1_ext_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                      uint8_t           nMembank);
R_DALILIB_RESULT gear_mem_bank1_ext_read_memory_bank1(PCtrlGearInstance     pCtrlInst,
                                                      uint8_t*              pRespValue);
R_DALILIB_RESULT gear_mem_bank1_ext_write_memory_bank1(PCtrlGearInstance     pCtrlInst,
                                                       ctrl_gear_commands_t* pGearCmd,
                                                       uint8_t               reqValue,
                                                       uint8_t*              pRespValue);
R_DALILIB_RESULT gear_mem_bank1_ext_write_memory_bank1_direct(PCtrlGearInstance pCtrlInst,
                                                              uint8_t           nCell,
                                                              uint8_t           reqValue);
R_DALILIB_RESULT gear_mem_bank1_ext_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                             dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT gear_mem_bank1_ext_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                              uint8_t*          pDaliMem);
#endif /* DALILIB_GEAR_MEM_BANK1_EXT_PRESENT */

//------------------------------------------------------------------
// gear_energy_reporting.c:
//------------------------------------------------------------------
#ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
R_DALILIB_RESULT gear_energy_reporting_process_forward(PCtrlGearInstance      pCtrlInst,
                                                       dalilib_frame_t*       pForwardFrame,
                                                       ctrl_gear_commands_t** ppGearCmd,
                                                       gear_adr_t*            pReceivedAdr,
                                                       uint8_t*               pRespValue);
void             gear_energy_reporting_init_after_power_cycle(PCtrlGearInstance pCtrlInst);
R_DALILIB_RESULT gear_energy_reporting_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                         uint8_t           nMembank);
R_DALILIB_RESULT gear_energy_reporting_read_memory_bank202(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue);
R_DALILIB_RESULT gear_energy_reporting_write_memory_bank202(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue);
R_DALILIB_RESULT gear_energy_reporting_write_memory_bank202_direct(PCtrlGearInstance  pCtrlInst,
                                                                   uint8_t            nCell,
                                                                   uint8_t            reqValue);
R_DALILIB_RESULT gear_energy_report_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                             dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT gear_energy_report_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                              uint8_t*          pDaliMem);
#endif /* DALILIB_GEAR_ENERGY_REPORT_PRESENT */

//------------------------------------------------------------------
// gear_diag_maintenance.c:
//------------------------------------------------------------------
#ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
R_DALILIB_RESULT gear_diag_maintenance_process_forward(PCtrlGearInstance      pCtrlInst,
                                                       dalilib_frame_t*       pForwardFrame,
                                                       ctrl_gear_commands_t** ppGearCmd,
                                                       gear_adr_t*            pReceivedAdr,
                                                       uint8_t*               pRespValue);
void             gear_diag_maintenance_init_after_power_cycle(PCtrlGearInstance pCtrlInst);
void             gear_diag_maintenance_init_persistent(PDaliLibInstance              pInst,
                                           dali_ctrl_gear_pers_mem_t*    pGearPersistent);
R_DALILIB_RESULT gear_diag_maintenance_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                         uint8_t           nMembank);
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank205(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank205(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank205_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue);
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank206(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank206(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank206_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue);
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank207(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank207(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue);
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank207_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue);
R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank205(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank206(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank207(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent);
R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank205(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem);
R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank206(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem);
R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank207(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem);
void             gear_diag_maintenance_timingHelper(void* pCtrlInstance);
#endif /* DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT */

//------------------------------------------------------------------
// dev_luminaire.c:
//------------------------------------------------------------------
#ifdef DALILIB_DEV_LUMINAIRE_PRESENT
void             dev_luminaire_init_after_power_cycle(PCtrlDeviceInstance pCtrlInst);
R_DALILIB_RESULT dev_luminaire_init_persistent(PDaliLibInstance                   pInst,
                                               dali_ctrl_device_persistent_mem_t* pDevicePersistent);
R_DALILIB_RESULT dev_luminaire_reset_memory_bank(PCtrlDeviceInstance pCtrlInst,
                                                 uint8_t             nMembank);
R_DALILIB_RESULT dev_luminaire_read_memory_bank201(PCtrlDeviceInstance  pCtrlInst,
                                                   uint8_t*             pRespValue);
R_DALILIB_RESULT dev_luminaire_write_memory_bank201_direct(PCtrlDeviceInstance  pCtrlInst,
                                                           uint8_t              nCell,
                                                           uint8_t              reqValue);
R_DALILIB_RESULT dev_luminaire_write_memory_bank201(PCtrlDeviceInstance     pCtrlInst,
                                                    ctrl_device_commands_t* pDevCmd,
                                                    uint8_t                 reqValue,
                                                    uint8_t*                pRespValue);
R_DALILIB_RESULT device_luminaire_save_mem_block_pers_vars(PDaliLibInstance                   pInst,
                                                           dali_ctrl_device_persistent_mem_t* pDevicePersistent);
R_DALILIB_RESULT device_luminaire_apply_mem_block_pers_vars(PCtrlDeviceInstance pCtrlInst,
                                                            uint8_t*            pDaliMem);
void             dev_luminaire_timingHelper(PCtrlDeviceInstance pCtrlInst);
#endif /* DALILIB_DEV_LUMINAIRE_PRESENT */
//------------------------------------------------------------------
// gw.c:
//------------------------------------------------------------------

R_DALILIB_RESULT gw_init(void);


#ifdef __cplusplus
    }
#endif

#endif // _DALISTACK_H_
