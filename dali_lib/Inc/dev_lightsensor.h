/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                IEC 62386-303
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DEV_LIGHT_SENSOR__H_
#define _DEV_LIGHT_SENSOR__H_

#ifdef __cplusplus
    extern "C" {
#endif

#define INPUT_DEVICE_LIGHT_SENSOR_EVENT_PRIO_DEFAULT   5
#define INPUT_DEVICE_LIGHT_SENSOR_EVENT_FILTER_DEFAULT 0x01 // 0000 0001b

#define INPUT_DEVICE_LIGHT_SENSOR_DEADTIME_DEFAULT     30
#define INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT       30
#define INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_DEFAULT    5
#define INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_MAX       25
#define INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_HIGH_MAX  0xFF

#define INPUT_DEVICE_LIGHT_SENSOR_RESOLUTION           10

#define INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_INCR          50     // [ms] 50 ms
#define INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_INCR        1000   // [ms] 1 s

#define INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_DEFAULT       1500   // [ms] 1,5 s
#define INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_DEFAULT     30000  // [ms] 30 s

#define INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_MIN           0      // [ms] 0 s
#define INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MIN         1000   // [ms] 1 s

#define INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_MAX           12750  // [ms] 12,75 s
#define INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MAX         255000 // [ms] 4 min 15 s

// enum for the light sensor commands
typedef enum _LIGHT_SENSOR_CMD
{
  LIGHT_CMD_SET_REPORT_TIMER = 0,
  LIGHT_CMD_SET_HYSTERESIS,
  LIGHT_CMD_SET_DEADTIME_TIMER,
  LIGHT_CMD_SET_HYSTERESIS_MIN,

  LIGHT_CMD_QUERY_HYSTERESIS_MIN,
  LIGHT_CMD_QUERY_DEADTIME_TIMER,
  LIGHT_CMD_QUERY_REPORT_TIMER,
  LIGHT_CMD_QUERY_HYSTERESIS,
  LIGHT_CMD_LAST,
}LIGHT_SENSOR_CMD;

// unit of timer ms
typedef struct _light_sensor_pers_variables_t
{
  uint8_t tReport;        // [0, 255]
  uint8_t tDeadtime;      // [0, 255]
  uint8_t tHysteresisMin; // [0, 255]
  uint8_t tHysteresis;    // [0, 25]
}light_sensor_pers_variables_t;

typedef struct _light_sensor_variables_t
{
  uint16_t hysteresisBand;     // [0,max(InputValue)]
  uint16_t hysteresisBandHigh; // [hysteresisBandLow,max(InputValue)]
  uint16_t hysteresisBandLow;  // [0,hysteresisBandHigh]
  uint32_t timeDead;           // [ms]
  uint32_t timeReport;         // [ms]
  uint32_t timeLastEvent;      // time stamp for last event [ms]
}light_sensor_variables_t;

#ifdef __cplusplus
    }
#endif


#endif // _DEV_LIGHT_SENSOR__H_
