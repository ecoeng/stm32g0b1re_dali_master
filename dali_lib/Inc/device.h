/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *                IEC 62386-103
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 --------------------------------------------------------------------------<M*/

#ifndef _DEVICE_H_
#define _DEVICE_H_

#ifdef __cplusplus
    extern "C" {
#endif

#define DALI_CTRL_DEVICE_FF_VALUE_MASK    0xFF

#define DALI_CTRL_DEVICE_BROADCAST_ADR    0x7F // broadcast address mask ( without event-bit )
#define DALI_CTRL_DEVICE_NBROADCAST_ADR   0x7E

#define DALI_CTRL_DEVICE_GROUPADR_MASK_1  0x40 // group address mask ( without event-bit )
#define DALI_CTRL_DEVICE_GROUPADR_MASK_2  0x1F // group address mask ( without event-bit )

#define DALI_CTRL_DEVICE_SHORTADR_MASK    0x3F // short address mask ( without event-bit )

#define DALI_CTRL_DEVICE_GROUPS_MAX       32
#define DALI_CTRL_DEVICE_INST_GROUPS_MAX  32

#define DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE 0x00

#define DALI_CTRL_DEVICE_RANDOM_RANGE_MIN 0x000000
#define DALI_CTRL_DEVICE_RANDOM_RANGE_MAX 0xFFFFFE

#define DALI_CTRL_DEVICE_INITIALISE_TIME_MAX (15 * 60 * 1000) // 15min

#define DALI_CTRL_DEVICE_PING_INTERVALL_TIME (9.5 * 60 * 1000) // 9min 30sec

#define DALI_CTRL_DEVICE_IDENTIFY_DEVICE_TIME_MAX   300 // 300ms
#define DALI_CTRL_DEVICE_RESET_TIME_MAX             300 // 300ms
#define DALI_CTRL_DEVICE_MEM_RESET_TIME_MAX       10000 // 10s

#define DALI_CTRL_DEVICE_SAVE_PERS_TIME_MAX 300              // 300ms
#define DALI_CTRL_DEVICE_QUIESCENT_TIME_MAX (15 * 60 * 1000) // 15min

#define DALI_CTRL_DEVICE_SEND_TESTFRAME_RETRY_DELAY 10 // 10ms

#define DALI_CTRL_DEVICE_INPUT_BYTE_MAX 8

#define DALI_CTRL_DEVICE_POWER_NOTIFICATION_BYTE 0x7F7

#define DALI_CTRL_DEVICE_POWER_NOTIFICATION_CMD  0xEE // use only internal

#define DALI_CTRL_DEVICE_DEFAULT_INSTANCE_BYTE   0xFE

#define DALI_CTRL_DEVICE_FIRST_INSTANCE_CMD_OPCODE  0x61
#define DALI_CTRL_DEVICE_LAST_INSTANCE_CMD_OPCODE   0x92

// Masks for devicePersistentChangedFlags
#define DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK      0x01
#define DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK         0x02
#define DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK      0x04
#define DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK         0x08
#define DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK           0x10


// enum for the control device commands
typedef enum _DEV_CMD
{
  DEV_CMD_NONE = 0,
  
  DEV_CMD_IDENTIFY_DEVICE,
  DEV_CMD_RESET_POWER_CYCLE_SEEN,        
  DEV_CMD_RESET,                
  DEV_CMD_RESET_MEMORY_BANK,          
  DEV_CMD_SET_SHORT_ADDRESS,          
  DEV_CMD_ENABLE_WRITE_MEMORY,        
  DEV_CMD_ENABLE_APPLICATION_CONTROLLER,    
  DEV_CMD_DISABLE_APPLICATION_CONTROLLER,    
  DEV_CMD_SET_OPERATING_MODE,          
  DEV_CMD_ADD_TO_DEVICE_GROUPS_0_15,      
  DEV_CMD_ADD_TO_DEVICE_GROUPS_16_31,      
  DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_0_15,    
  DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_16_31,  
  DEV_CMD_START_QUIESCENT_MODE,        
  DEV_CMD_STOP_QUIESCENT_MODE,        
  DEV_CMD_ENABLE_POWER_CYCLE_NOTIFICATION,  
  DEV_CMD_DISABLE_POWER_CYCLE_NOTIFICATION,  
  DEV_CMD_SAVE_PERSISTENT_VARIABLES,      
  DEV_CMD_QUERY_DEVICE_STATUS,        
  DEV_CMD_QUERY_APPLICATION_CONTROLLER_ERROR,  
  DEV_CMD_QUERY_INPUT_DEVICE_ERROR,      
  DEV_CMD_QUERY_MISSING_SHORT_ADDRESS,    

  DEV_CMD_QUERY_VERSION_NUMBER,        
  DEV_CMD_QUERY_NUMBER_OF_INSTANCES,      
  DEV_CMD_QUERY_CONTENT_DTR0,          
  DEV_CMD_QUERY_CONTENT_DTR1,          
  DEV_CMD_QUERY_CONTENT_DTR2,          
  DEV_CMD_QUERY_RANDOM_ADDRESS_H,        
  DEV_CMD_QUERY_RANDOM_ADDRESS_M,        
  DEV_CMD_QUERY_RANDOM_ADDRESS_L,        
  DEV_CMD_READ_MEMORY_LOCATION,        
  DEV_CMD_READ_MEMORY_CELL,
  DEV_CMD_QUERY_APPLICATION_CONTROL_ENABLED,  
  DEV_CMD_QUERY_OPERATING_MODE,        
  DEV_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE,  
  DEV_CMD_QUERY_QUIESCENT_MODE,        
  DEV_CMD_QUERY_DEVICE_GROUPS_0_7,      
  DEV_CMD_QUERY_DEVICE_GROUPS_8_15,      
  DEV_CMD_QUERY_DEVICE_GROUPS_16_23,      
  DEV_CMD_QUERY_DEVICE_GROUPS_24_31,      
  DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION,    
  DEV_CMD_QUERY_DEVICE_CAPABILITIES,      
  DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER,    
  DEV_CMD_QUERY_RESET_STATE,          

  DEV_CMD_SET_EVENT_PRIORITY,          
  DEV_CMD_ENABLE_INSTANCE,          
  DEV_CMD_DISABLE_INSTANCE,          
  DEV_CMD_SET_PRIMARY_INSTANCE_GROUP,    
  DEV_CMD_SET_INSTANCE_GROUP_1,      
  DEV_CMD_SET_INSTANCE_GROUP_2,      
  DEV_CMD_SET_EVENT_SCHEME,        
  DEV_CMD_SET_EVENT_FILTER,        

  DEV_CMD_QUERY_INSTANCE_TYPE,      
  DEV_CMD_QUERY_RESOLUTION,        
  DEV_CMD_QUERY_INSTANCE_ERROR,      
  DEV_CMD_QUERY_INSTANCE_STATUS,      
  DEV_CMD_QUERY_EVENT_PRIORITY,      
  DEV_CMD_QUERY_INSTANCE_ENABLED,      
  DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP,  
  DEV_CMD_QUERY_INSTANCE_GROUP_1,      
  DEV_CMD_QUERY_INSTANCE_GROUP_2,      
  DEV_CMD_QUERY_EVENT_SCHEME,        
  DEV_CMD_QUERY_INPUT_VALUE,        
  DEV_CMD_QUERY_INPUT_VALUE_LATCH,    
  DEV_CMD_QUERY_FEATURE_TYPE,        
  DEV_CMD_QUERY_NEXT_FEATURE_TYPE,    
  DEV_CMD_QUERY_EVENT_FILTER_0_7,      
  DEV_CMD_QUERY_EVENT_FILTER_8_15,    
  DEV_CMD_QUERY_EVENT_FILTER_16_23,

  DEV_CMD_TERMINATE,            
  DEV_CMD_INITIALISE,            
  DEV_CMD_RANDOMISE,            
  DEV_CMD_COMPARE,            
  DEV_CMD_WITHDRAW,            
  DEV_CMD_SEARCHADDRH,          
  DEV_CMD_SEARCHADDRM,          
  DEV_CMD_SEARCHADDRL,          
  DEV_CMD_PROGRAM_SHORT_ADDRESS,      
  DEV_CMD_VERIFY_SHORT_ADDRESS,      
  DEV_CMD_QUERY_SHORT_ADDRESS,      
  DEV_CMD_WRITE_MEMORY_LOCATION,      
  DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY,
  DEV_CMD_DTR0,              
  DEV_CMD_DTR1,              
  DEV_CMD_DTR2,              
  DEV_CMD_SEND_TESTFRAME,          
  DEV_CMD_DIRECT_WRITE_MEMORY,      
  DEV_CMD_DTR1_DTR0,            
  DEV_CMD_DTR2_DTR1,            

  DEV_CMD_TESTFRAME,

  DEV_CMD_LAST
}DEV_CMD;


typedef enum _CTRL_DEVICE_QUIESCENT_MODE
{
  QUIESCENT_MODE_DISABLED = 0,
  QUIESCENT_MODE_ENABLED
}CTRL_DEVICE_QUIESCENT_MODE;

typedef enum _WRITE_ENABLE_STATE
{
  WRITE_ENABLE_STATE_DISABLED = 0,
  WRITE_ENABLE_STATE_ENABLED
}WRITE_ENABLE_STATE;

typedef enum _POWER_CYCLE_NOTIFY
{
  POWER_CYCLE_NOTIFY_DISABLED = 0,
  POWER_CYCLE_NOTIFY_ENABLED
}POWER_CYCLE_NOTIFY;

typedef enum _CTRL_DEVICE_INITIALISE_STATE
{
  CTRL_DEVICE_INITIALISE_STATE_DISABLED = 0,
  CTRL_DEVICE_INITIALISE_STATE_ENABLED,
  CTRL_DEVICE_INITIALISE_STATE_WITHDRAWN
}CTRL_DEVICE_INITIALISE_STATE;

// persistent control device variables
typedef struct ctrl_device_pers_variables_t
{
  uint8_t  shortAddress;     // [0..63,255] 255: no address
  uint32_t deviceGroups;     // [0..0xFF FF FF FF]
  uint32_t randomAddress;    // [0..0xFF FF FF]
  uint8_t  numberOfInstance; // [0..32]
  // supported only DALI_CTRL_DEVICE_NORM_OPERTING_MODE
  uint8_t  operatingMode;    // [0, 0x80..0xFF]
  uint8_t  applicationActive;
  uint8_t  applicationControllerPresent;
  uint8_t  powerCycleNotification;
}dali_ctrl_device_pers_variables_t;

typedef struct _ctrl_device_variables_t
{
  uint32_t searchAddress;
  uint8_t  DTR0;
  uint8_t  DTR1;
  uint8_t  DTR2;
  uint8_t  quiescentMode;
  uint8_t  writeEnableState;
  uint8_t  powerCycleSeen;
  uint8_t  initialisationState;
  uint8_t  applicationControllerError;
  uint8_t  inputDeviceError;
  uint8_t  resetState;
  union
  {
    push_buttons_variables_t     pushBtnVariables;
    occupancy_sensor_variables_t occupancySensorVariables;
    light_sensor_variables_t     lightSensorVariables;
  };
}dali_ctrl_device_variables_t;

// persistent control device instance variables
typedef struct ctrl_device_inst_pers_variables_t
{
  uint8_t  instanceGroup0; // [0..31,255]
  uint8_t  instanceGroup1; // [0..31,255]
  uint8_t  instanceGroup2; // [0..31,255]
  uint8_t  instanceActive; // [TRUE|FALSE]
  uint8_t  resolution;     // [1..255]
  uint8_t  instanceNumber; // [0..numberOfInstance-1]
  uint32_t eventFilter;    // [0..0xFF FF FF]
  uint8_t  eventScheme;    // [0..4] CTRL_DEVICE_EVENT_SCHEME
  uint8_t  eventPriority;  // [2..5]
  INPUT_DEVICE_INSTANCE_TYPE  instanceType;    // [0..31]
  union{
    push_buttons_pers_variables_t     pushBtnPersVariables;
    occupancy_sensor_pers_variables_t occupancySensorPersVariables;
    light_sensor_pers_variables_t     lightSensorPersVariables;
  };
}dali_ctrl_device_inst_pers_variables_t;

typedef struct _ctrl_device_inst_variables_t
{
  union
  {
    uint64_t inputValue;
    uint8_t  inputValue_byte[DALI_CTRL_DEVICE_INPUT_BYTE_MAX];
  };
  uint8_t instanceError;    // [TRUE|FALSE]
  uint8_t instanceErrorByte;  // Detailed error information is instance type specific and is described in Parts 3xx
}dali_ctrl_device_inst_variables_t;

typedef struct
{
  uint8_t priority:3;     // PPP
  uint8_t repetitions:2;  // RR
  uint8_t frameType:1;    // A
  uint8_t transaction:1;  // T
  uint8_t check:1;        // C
}dali_test_frame_request_t;


// helper pointers of the control device/instance

typedef dali_ctrl_device_pers_variables_t*      PCtrlDevPersistentVariables;
typedef dali_ctrl_device_variables_t*           PCtrlDevVariables;
typedef dali_ctrl_device_inst_pers_variables_t* PCtrlDevInstPersistentVariables;
typedef dali_ctrl_device_inst_variables_t*      PCtrlDevInstVariables;
typedef dali_ctrl_mem_bank0_t*                  PCtrlDevMemoryBank0;
typedef dali_ctrl_mem_bank_others_t*            PCtrlDevMemoryBanksOther;

// control device/instance timer function

typedef void (*fCtrlDevTimingHelper)(void* pInstance);

// persistent memory of the control device
typedef struct ctrl_device_persistent_mem_t
{

  dali_ctrl_device_pers_variables_t      persVariables;
  dali_ctrl_device_inst_pers_variables_t instPersVariables;
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
  dali_ctrl_device_mem_bank201_NVM_t     deviceMemBank201NVM; // Luminaire-mounted Control Devices non-volatile memory cells (part 351)
  #endif
}dali_ctrl_device_persistent_mem_t;

// instance variables of the control device/instance
typedef struct ctrl_device_instance_t
{
  void*                                   pInst;          // PDaliLibInstance
  dali_ctrl_device_persistent_mem_t       devicePersistent;
  dali_ctrl_device_variables_t            deviceVariables;
  dali_ctrl_device_inst_variables_t       deviceInstVariables;
  dali_ctrl_mem_bank0_t                   deviceMemBank0;
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
  dali_ctrl_device_mem_bank201_RAM_t      deviceMemBank201RAM;
  #endif
  dali_ctrl_mem_bank_others_t             deviceMemBanksOther; // membanks different to 0
  
  uint32_t                                deviceLastPingTime;
  // used, if "INITIALISE" frame received
  uint32_t                                deviceInitialiseStartTime;
  // used, if "RESET" frame received
  uint32_t                                deviceResetStartTime;
  uint8_t                                 deviceResetRunning;
  // used, if "RESET MEMORY BANK - X"  frame received
  uint32_t                                deviceResetMemoryStartTime;
  uint8_t                                 deviceResetMemoryRunning;
  // used, if "SAVE PERSISTENT VARIABLES"  frame received
  uint32_t                                deviceSavePersistentStartTime;
  uint8_t                                 deviceSavePersistentRunning;
  // used, if persistent variables changed but not stored yet
  uint32_t                                devicePersistentChangedTime;
  uint8_t                                 devicePersistentChangedFlags;
  // used, if "IDENTIFY DEVICE" frame received
  uint32_t                                deviceIdentifyDeviceStartTime;
  uint8_t                                 deviceIdentifyRunning;
  // used, if "quiescentMode" enabled
  uint32_t                                deviceQuiescentTime;
  // used if "SEND TESTFRAME" frame received
  uint32_t                                deviceTestframeTime;
  // used, if "powerCycleNotification" enabled
  uint8_t                                 devicePowerCycleNotificationSend; // 0: not send, 1: already send
  uint32_t                                devicePowerCycleNotificationRandomTime;
  uint32_t                                devicePowerCycleNotificationTime;
  // used to send/receive answer for "QUERY APPLICATION CONTROLLER ENABLED"
  uint32_t                                deviceLastQueryAppCtrlEnabled;
  // used, by "system failure"
  uint8_t                                 deviceSystemFailure;
  // used, by "bus power flag" 0: Power On 1: Power Off
  uint8_t                                 deviceBusPower;
  // used, by "collision detection"
  dalilib_buscollision_status_t           deviceBusCollision;
  // control device timer function
  fCtrlDevTimingHelper                    fpDeviceTimingHelper;
}dali_ctrl_device_instance_t;

typedef dali_ctrl_device_instance_t* PCtrlDeviceInstance;

// control device command structure

typedef struct
{
  uint16_t eDevCmd;
  uint8_t  instanceByte;    
  uint8_t  opcodeByte;      
  uint8_t  answer;       // 0: no answer, 1: wait on answer
  uint8_t  twice;        // 0: send command normal, 1: send command double
}ctrl_device_commands_t;

//------------------------------------------------------------------
// device.c:
//------------------------------------------------------------------

#ifdef __cplusplus
    }
#endif


#endif // _DEVICE_H_
