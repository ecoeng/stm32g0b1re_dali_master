/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * device.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"
#include "device.h"

#define LIGHT_SENSOR_EVENT_FILTER_ILLUMINANCE_LEVEL_ENABLED  0x01 // Bit-0

// eDevCmd,                      inst, opcode,  a, t
static ctrl_device_commands_t lightsensor_commands_m[] =
{
  {LIGHT_CMD_SET_REPORT_TIMER,    0x00,0x30,0,1},
  {LIGHT_CMD_SET_HYSTERESIS,      0x00,0x31,0,1},
  {LIGHT_CMD_SET_DEADTIME_TIMER,  0x00,0x32,0,1},
  {LIGHT_CMD_SET_HYSTERESIS_MIN,  0x00,0x33,0,1},

  {LIGHT_CMD_QUERY_HYSTERESIS_MIN,0x00,0x3C,1,0},
  {LIGHT_CMD_QUERY_DEADTIME_TIMER,0x00,0x3D,1,0},
  {LIGHT_CMD_QUERY_REPORT_TIMER,  0x00,0x3E,1,0},
  {LIGHT_CMD_QUERY_HYSTERESIS,    0x00,0x3F,1,0},

  {LIGHT_CMD_LAST,                0x00,0x00,0,0},
};


/******************************************************************************
* calculate and set the dead time
*******************************************************************************/
static void lightsensor_calc_dead_time(light_sensor_variables_t*      pLightSensor,
                                       light_sensor_pers_variables_t* pLightSensorPersVariables)
{
  uint32_t deadTime = 0;

  if (pLightSensorPersVariables->tDeadtime <= 0)
  {
    pLightSensor->timeDead = deadTime; // disabled
    return;
  }

  deadTime = pLightSensorPersVariables->tDeadtime * INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_INCR;

  if (deadTime > INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_MAX)
  {
    deadTime = INPUT_DEVICE_LIGHT_SENSOR_DEAD_T_MAX;
  }

  pLightSensor->timeDead = deadTime;
}


/******************************************************************************
 * calculate and set the report time
*******************************************************************************/
static void lightsensor_calc_report_time(light_sensor_variables_t*      pLightSensor,
                                         light_sensor_pers_variables_t* pLightSensorPersVariables)
{
  uint32_t reportTime = 0;

  if (pLightSensorPersVariables->tReport <= 0)
  {
    pLightSensor->timeReport = reportTime; // disabled
    return;
  }

  reportTime = pLightSensorPersVariables->tReport * INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_INCR;

  if (reportTime > INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MAX)
  {
    reportTime = INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MAX;
  }

  if (reportTime < INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MIN)
  {
    reportTime = INPUT_DEVICE_LIGHT_SENSOR_REPORT_T_MIN;
  }

  if (reportTime < pLightSensor->timeDead)
  {
    reportTime = pLightSensor->timeDead;
  }

  pLightSensor->timeReport = reportTime;
}


/******************************************************************************
 * calculate the hysteresis band
*******************************************************************************/
static uint16_t lightsensor_set_hysteresisband(PCtrlDeviceInstance            pCtrlInst,
                                               light_sensor_pers_variables_t* pLightSensorPersVariables,
                                               uint16_t                       illuminanceLevel)
{
  uint16_t hysteresisBand = 0;

  hysteresisBand = ((illuminanceLevel *
                     pCtrlInst->devicePersistent.instPersVariables.resolution *
                     pLightSensorPersVariables->tHysteresis)/100);

  if (hysteresisBand > pLightSensorPersVariables->tHysteresisMin)
  {
    hysteresisBand = pLightSensorPersVariables->tHysteresisMin;
  }

  return hysteresisBand;
}

/******************************************************************************
 * called when light sensor state changes
*******************************************************************************/
static R_DALILIB_RESULT lightsensor_interrupt(PCtrlDeviceInstance pCtrlInst,
                                              uint16_t            illuminanceLevel)
{
  uint8_t  bSendEvent = 0;

  light_sensor_variables_t*      pLightSensor              = &pCtrlInst->deviceVariables.lightSensorVariables;
  light_sensor_pers_variables_t* pLightSensorPersVariables = &pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables;

  if (illuminanceLevel > pLightSensor->hysteresisBandHigh ||
      illuminanceLevel < pLightSensor->hysteresisBandLow )
  {
    bSendEvent = 1;
  }

  if (!bSendEvent &&
      0 == pLightSensor->hysteresisBandHigh &&
      0 == pLightSensor->hysteresisBandLow  &&
      0 != illuminanceLevel)
  {
    bSendEvent = 1;
  }

  if (bSendEvent)
  {
    // is dead time set?
    if (pLightSensorPersVariables->tDeadtime > 0)
    {
      // dead time expired?
      if (dali_difftime(((PDaliLibInstance)pCtrlInst->pInst)->currentTime, pLightSensor->timeLastEvent) < pLightSensor->timeDead)
      {
        // dead time is not expired,
        // event shall not send
        bSendEvent = 0;
      }
    }
  }

  if (bSendEvent)
  {
    pLightSensor->hysteresisBand = lightsensor_set_hysteresisband(pCtrlInst, pLightSensorPersVariables, illuminanceLevel);

    if (illuminanceLevel > pLightSensor->hysteresisBandHigh)
    {
      pLightSensor->hysteresisBandHigh = illuminanceLevel;
      pLightSensor->hysteresisBandLow = MAX(illuminanceLevel-pLightSensor->hysteresisBand, 0);
    }
    else if (illuminanceLevel < pLightSensor->hysteresisBandLow)
    {
      pLightSensor->hysteresisBandLow = illuminanceLevel;
      pLightSensor->hysteresisBandHigh = MAX(illuminanceLevel + pLightSensor->hysteresisBand, illuminanceLevel);
    }

    lightsensor_calc_report_time(pLightSensor,pLightSensorPersVariables);
    lightsensor_calc_dead_time(pLightSensor,pLightSensorPersVariables);
  }

  return (bSendEvent ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT);
}

//----------------------------------------------------------------------
// Here it is checked whether the event filter flag is set or not
// Bit-0: Illuminance level event enabled?   default: 1
// Bit-1: Reserved               default: 0
// Bit-2: Reserved               default: 0
// Bit-3: Reserved               default: 0
// Bit-4: Reserved               default: 0
// Bit-5: Reserved               default: 0
// Bit-6: Reserved               default: 0
// Bit-7: Reserved               default: 0
//
// return: R_DALILIB_OK :
//                  - event flag bit is set
//         R_DALILIB_CTRL_DEVICE_IGNORE_EVENT :
//                  - event flag bit is not set
//         R_DALILIB_NOT_SUPPORTED_ACTION :
//                  - unknown/not supported event action
//-----------------------------------------------------------------------
static R_DALILIB_RESULT lightsensor_check_event(PCtrlDeviceInstance pCtrlInst,
                                                uint8_t             lightSensorActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_NOT_SUPPORTED_ACTION;

  if (DALILIB_ACT_INTERNAL_EVENT_LIGHT_SENSOR_ILLUMINANCE_LEVEL ==
      lightSensorActionCode)
  {
     rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
           LIGHT_SENSOR_EVENT_FILTER_ILLUMINANCE_LEVEL_ENABLED) ?
             R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
  }

  return (rc);
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t dev_lightsensor_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers)
{
  if (INPUT_DEVICE_LIGHT_SENSOR_EVENT_PRIO_DEFAULT != pInstPers->eventPriority)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_LIGHT_SENSOR_EVENT_FILTER_DEFAULT != pInstPers->eventFilter)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT != pInstPers->lightSensorPersVariables.tReport)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_LIGHT_SENSOR_DEADTIME_DEFAULT != pInstPers->lightSensorPersVariables.tDeadtime)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_DEFAULT != pInstPers->lightSensorPersVariables.tHysteresis)
  {
    return (FALSE);
  }
  switch(pInstPers->resolution)
  {
    case 7:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 1)
      {
        return (FALSE);
      }
    break;
    
    case 8:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 2)
      {
        return (FALSE);
      }
    break;
    
    case 9:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 5)
      {
        return (FALSE);
      }
    break;
    
    case 10:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 10)
      {
        return (FALSE);
      }
    break;
    
    case 11:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 20)
      {
        return (FALSE);
      }
    break;
    
    case 12:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 40)
      {
        return (FALSE);
      }
    break;
    
    case 13:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 81)
      {
        return (FALSE);
      }
    break;
    
    case 14:
      if (pInstPers->lightSensorPersVariables.tHysteresisMin != 163)
      {
        return (FALSE);
      }
    break;
    
    default:
      {
        if (1 <= pInstPers->resolution && pInstPers->resolution <= 6)
        {
          if(pInstPers->lightSensorPersVariables.tHysteresisMin != 0)
          {
            return (FALSE);
          }
        }
        else if (pInstPers->resolution >= 15)
        {
          if(pInstPers->lightSensorPersVariables.tHysteresisMin != 255)
          {
            return (FALSE);
          }
        }
      }
    break;

  }

  return (TRUE);
}

//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void dev_lightsensor_by_reset(PCtrlDevVariables pVariables, PCtrlDevInstPersistentVariables pInstPers)
{
  pInstPers->eventPriority = INPUT_DEVICE_LIGHT_SENSOR_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = INPUT_DEVICE_LIGHT_SENSOR_EVENT_FILTER_DEFAULT;

  pInstPers->lightSensorPersVariables.tReport = INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT;
  pInstPers->lightSensorPersVariables.tDeadtime = INPUT_DEVICE_LIGHT_SENSOR_DEADTIME_DEFAULT;
  pInstPers->lightSensorPersVariables.tHysteresisMin = INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT;
  pInstPers->lightSensorPersVariables.tHysteresis = INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_DEFAULT;

  switch(pInstPers->resolution)
  {
    case 7:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 1;
    break;
    
    case 8:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 2;
    break;
    
    case 9:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 5;  
    break;
    
    case 10:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 10;
    break;
    
    case 11:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 20;
    break;

    case 12:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 40;
    break;
    
    case 13:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 81;
    break;
    
    case 14:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 163;
    break;
    
    default:
      {
        if (1 <= pInstPers->resolution && pInstPers->resolution <= 6)
        {
          pInstPers->lightSensorPersVariables.tHysteresisMin = 0;
        }
        else if (pInstPers->resolution >= 15)
        {
          pInstPers->lightSensorPersVariables.tHysteresisMin = 255;
        }
      }
    break;
  }

  lightsensor_calc_dead_time(&pVariables->lightSensorVariables, &pInstPers->lightSensorPersVariables);
  lightsensor_calc_report_time(&pVariables->lightSensorVariables, &pInstPers->lightSensorPersVariables);
}

//----------------------------------------------------------------------
// initialize the default variable of the input device light sensor
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_lightsensor_init_persistent(PCtrlDevVariables pVariables,
                         PCtrlDevInstPersistentVariables pInstPers)
{
  pInstPers->instanceType = INPUT_DEVICE_INST_LIGHT_SENSOR;
  pInstPers->eventPriority = INPUT_DEVICE_LIGHT_SENSOR_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = INPUT_DEVICE_LIGHT_SENSOR_EVENT_FILTER_DEFAULT; // 0000 0011b

  pInstPers->lightSensorPersVariables.tReport = INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT;
  pInstPers->lightSensorPersVariables.tDeadtime = INPUT_DEVICE_LIGHT_SENSOR_DEADTIME_DEFAULT;
  pInstPers->lightSensorPersVariables.tHysteresisMin = INPUT_DEVICE_LIGHT_SENSOR_REPORT_DEFAULT;
  pInstPers->lightSensorPersVariables.tHysteresis = INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_DEFAULT;

  switch(pInstPers->resolution)
  {
    case 7:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 1;
    break;
    
    case 8:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 2;
    break;
    
    case 9:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 5;
    break;
    
    case 10:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 10;
    break;
    
    case 11:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 20;
    break;
    
    case 12:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 40;
    break;
    
    case 13:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 81;
    break;
    
    case 14:
      pInstPers->lightSensorPersVariables.tHysteresisMin = 163;
    break;
    
    default:
      {
        if (1 <= pInstPers->resolution && pInstPers->resolution <= 6)
        {
          pInstPers->lightSensorPersVariables.tHysteresisMin = 0;
        }
        else if (pInstPers->resolution >= 15)
        {
          pInstPers->lightSensorPersVariables.tHysteresisMin = 255;
        }
      }
      break;
  }

  lightsensor_calc_dead_time(&pVariables->lightSensorVariables, &pInstPers->lightSensorPersVariables);
  lightsensor_calc_report_time(&pVariables->lightSensorVariables, &pInstPers->lightSensorPersVariables);

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// result event information for light sensor
// (Light sensor DIN 62386-304)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_lightsensor_eventinfo(PCtrlDeviceInstance pCtrlInst,
                                           dalilib_light_sensor_event_t lightSensorEvent,
                       uint16_t *pLightSensorEvent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  DALILIB_ACT_EVENT_LIGHT_SENSOR lightSensorActionCode = lightSensorEvent.type;

  rc = lightsensor_check_event(pCtrlInst, lightSensorActionCode);
  if (R_DALILIB_OK != rc)
  {
    return (rc);
  }
  // event messages
  // An illuminance level event,
  // passing the actual illuminance level along.
  if (DALILIB_ACT_INTERNAL_EVENT_LIGHT_SENSOR_ILLUMINANCE_LEVEL == lightSensorActionCode)
  {
    rc = lightsensor_interrupt(pCtrlInst, lightSensorEvent.illuminanceLevel);
  }
  else
  {
    rc = R_DALILIB_NOT_SUPPORTED_ACTION;
  }

  if (R_DALILIB_OK == rc)
  {
    *pLightSensorEvent                                            = lightSensorEvent.illuminanceLevel;
    pCtrlInst->deviceInstVariables.inputValue                     = lightSensorEvent.illuminanceLevel;
    pCtrlInst->deviceVariables.lightSensorVariables.timeLastEvent = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
  }

  return (rc);
}

//----------------------------------------------------------------------
// result action code for event information
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_lightsensor_reactioncode(uint32_t frameData,
                      DALILIB_CTRL_DEVICE_REACTION_CODES *pLightSensorReActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint16_t eventInfo = 0x0000;

  *pLightSensorReActionCode = DALILIB_REACT_EVENT_UNKNOWN;

  eventInfo = (frameData & 0x01); // 00 0000 0001b

  if(0x01 == eventInfo)
  {
    *pLightSensorReActionCode = DALILIB_REACT_EVENT_LIGHT_SENSOR_ILLUMINANCE_LEVEL_REPORT;
  }
  else
  {
    rc = R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
  }

  return (rc);
}


//----------------------------------------------------------------------
// result opcode byte for light sensor command
// (Light sensor DIN 62386-304)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_lightsensor_opcode(DALILIB_ACT_LIGHT_SENSOR lightSensorActionCode,
                      ctrl_device_commands_t *pDevCmd)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  LIGHT_SENSOR_CMD lightSensorCmd = LIGHT_CMD_LAST;
  switch(lightSensorActionCode)
  {
    // Set the report timer
    case DALILIB_ACT_LIGHT_SENSOR_SET_REPORT_TIMER:
      lightSensorCmd = LIGHT_CMD_SET_REPORT_TIMER;
    break;
    // Set the hysteresis
    case DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS:
      lightSensorCmd = LIGHT_CMD_SET_HYSTERESIS;
    break;
    // Set the dead time timer
    case DALILIB_ACT_LIGHT_SENSOR_SET_DEADTIME_TIMER:
      lightSensorCmd = LIGHT_CMD_SET_DEADTIME_TIMER;
    break;
    // Set the hysteresis min
    case DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS_MIN:
      lightSensorCmd = LIGHT_CMD_SET_HYSTERESIS_MIN;
    break;
  
    // Query hysteresis min
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS_MIN:
      lightSensorCmd = LIGHT_CMD_QUERY_HYSTERESIS_MIN;
    break;
    // Query the dead time timer
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_DEADTIME_TIMER:
      lightSensorCmd = LIGHT_CMD_QUERY_DEADTIME_TIMER;
    break;
    // Query the report timer
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_REPORT_TIMER:
      lightSensorCmd = LIGHT_CMD_QUERY_REPORT_TIMER;
    break;
    // Query hysteresis
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS:
      lightSensorCmd = LIGHT_CMD_QUERY_HYSTERESIS;
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  if (R_DALILIB_OK == rc && LIGHT_CMD_LAST != lightSensorCmd)
  {
    pDevCmd->opcodeByte = lightsensor_commands_m[lightSensorCmd].opcodeByte;
    pDevCmd->twice = lightsensor_commands_m[lightSensorCmd].twice;
    pDevCmd->eDevCmd = lightSensorCmd;
    pDevCmd->answer = lightsensor_commands_m[lightSensorCmd].answer;
  }
  return (rc);
}

//----------------------------------------------------------------------
// search light sensor command for instance byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
ctrl_device_commands_t* dev_lightsensor_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;
  for (idx = 0; idx < COUNTOF(lightsensor_commands_m); idx++)
  {
    if (lightsensor_commands_m[idx].instanceByte == 0x00 &&
        lightsensor_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&lightsensor_commands_m[idx]);
    }
  }
  return (NULL);
}


//----------------------------------------------------------------------
// process the instance configuration forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_lightsensor_process_forward(PCtrlDeviceInstance pCtrlInst,
                              ctrl_device_commands_t* pDevCmd,
                           uint8_t *pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  light_sensor_pers_variables_t* pLightSensorPersistentVariables   = NULL;

  pLightSensorPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables;
  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case LIGHT_CMD_SET_REPORT_TIMER:
      {
        pLightSensorPersistentVariables->tReport = pCtrlInst->deviceVariables.DTR0;
        lightsensor_calc_report_time(&pCtrlInst->deviceVariables.lightSensorVariables, pLightSensorPersistentVariables);
        rc = R_DALILIB_OK;
      }
    break;
    
    case LIGHT_CMD_SET_HYSTERESIS:
      {
        if (pCtrlInst->deviceVariables.DTR0 <= INPUT_DEVICE_LIGHT_SENSOR_HYSTERESIS_MAX)
        {
          pLightSensorPersistentVariables->tHysteresis = pCtrlInst->deviceVariables.DTR0;
        }
        rc = R_DALILIB_OK;
      }
    break;
    
    case LIGHT_CMD_SET_DEADTIME_TIMER:
      {
        pLightSensorPersistentVariables->tDeadtime = pCtrlInst->deviceVariables.DTR0;
        lightsensor_calc_dead_time(&pCtrlInst->deviceVariables.lightSensorVariables, pLightSensorPersistentVariables);
        lightsensor_calc_report_time(&pCtrlInst->deviceVariables.lightSensorVariables, pLightSensorPersistentVariables);
        rc = R_DALILIB_OK;
      }
    break;
    
    case LIGHT_CMD_SET_HYSTERESIS_MIN:
      {
        pLightSensorPersistentVariables->tHysteresisMin = pCtrlInst->deviceVariables.DTR0;
        rc = R_DALILIB_OK;
      }
    break;
    
    case LIGHT_CMD_QUERY_HYSTERESIS_MIN:
      {
        *pRespValue = pLightSensorPersistentVariables->tHysteresisMin;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case LIGHT_CMD_QUERY_DEADTIME_TIMER:
      {
        *pRespValue = pLightSensorPersistentVariables->tDeadtime;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case LIGHT_CMD_QUERY_REPORT_TIMER:
      {
        *pRespValue = pLightSensorPersistentVariables->tReport;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case LIGHT_CMD_QUERY_HYSTERESIS:
      {
        *pRespValue = pLightSensorPersistentVariables->tHysteresis;
        pDevCmd->answer = TRUE;
      }
    break;
    
    default:
        rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// light sensor timer function
//----------------------------------------------------------------------
void dev_lightsensor_timingHelper(PCtrlDeviceInstance pCtrlInst)
{
  PDaliLibInstance  pInst     = pCtrlInst->pInst;
  dalilib_time     currentTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;

  light_sensor_pers_variables_t* pLightSensorPersistentVariables   = NULL;
  light_sensor_variables_t*      pLightSensorVariables       = NULL;

  pLightSensorPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables;
  pLightSensorVariables = &pCtrlInst->deviceVariables.lightSensorVariables;

  send_frame_t newSendFrame;
  device_adr_t deviceAdr;
  uint16_t     eventValue = 0;

  memset(&newSendFrame, 0, sizeof(newSendFrame));
  memset(&deviceAdr, 0, sizeof(deviceAdr));

  if (NULL == pInst)
  {
    return;
  }

  newSendFrame.frame.priority   = pCtrlInst->devicePersistent.instPersVariables.eventPriority;
  newSendFrame.frame.datalength = FRAME_CTRL_DEV_FF_LEN;
  newSendFrame.type         = SEND_FRAME_TYPE_CTRL_EVENT;

  deviceAdr.adr         = pCtrlInst->devicePersistent.persVariables.shortAddress;
  deviceAdr.adrType       = DALI_ADRTYPE_SHORT;
  deviceAdr.instanceAdrType   = DALI_INSTANCE_ADRTYPE_TYPE;
  deviceAdr.instance       = pCtrlInst->devicePersistent.instPersVariables.instanceType;

  // is report time set?
  if (pLightSensorPersistentVariables->tReport > 0)
  {
    // is dead time set?
    if (pLightSensorPersistentVariables->tDeadtime > 0)
    {
      // dead time expired?
      if (dali_difftime(currentTime, pLightSensorVariables->timeLastEvent) < pLightSensorVariables->timeDead)
      {
        // dead time is not expired,
        // event shall not send
        return;
      }
    }

    if (dali_difftime(currentTime, pLightSensorVariables->timeLastEvent) < pLightSensorVariables->timeReport)
    {
      // report time is not expired,
      // event shall not send
      return;
    }

    eventValue = (uint16_t)pCtrlInst->deviceInstVariables.inputValue;

    pLightSensorVariables->timeLastEvent = currentTime;
    ctrl_input_device_create_event(pInst, &newSendFrame, &deviceAdr, &eventValue);
    dali_cb_send(pInst, &newSendFrame );
  }
}
