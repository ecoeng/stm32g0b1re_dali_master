/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_power_supply.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"

#ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT


// eGearPowerSupplyCmd,                           selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t power_supply_commands_m[] =
{
  {(GEAR_CMD) POWER_SUPPLY_QUERY_ACTIVE_POWER_SUPPLY,       1,       0xFE,  1, 0},

  {(GEAR_CMD) POWER_SUPPLY_QUERY_EXTENDED_VERSION_NUMBER,   1,       0xFF,  1, 0},
  {(GEAR_CMD) LED_CMD_LAST,                                 0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search power supply command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* power_supply_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(power_supply_commands_m); idx++)
  {
    if (power_supply_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&power_supply_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// initialize the memory after external power cycle
//----------------------------------------------------------------------
void gear_power_supply_init_after_power_cycle(PCtrlGearInstance pCtrlInst)
{
  // initialize power supply variables
  pCtrlInst->gearVariables.powerSupplyVariables.extendedVersionNumber = CTRL_GEAR_POWER_SUPPLY_EXTENDED_VERSION_NUMBER;
  pCtrlInst->gearVariables.powerSupplyVariables.deviceType = GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY;

  // initialize memory bank 201 RAM memory cells
  pCtrlInst->gearMemBank201RAM.lockByte = DALI_MASK;
}


//----------------------------------------------------------------------
// initialize the default values of persistent memory
//----------------------------------------------------------------------
void gear_power_supply_init_persistent(PDaliLibInstance              pInst,
                                       dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  (void)pInst;

  // initialize memory bank 201 NVM memory cells
  pGearPersistent->gearMemBank201NVM.status = 0x01;
}

//----------------------------------------------------------------------
// reset power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_power_supply_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                     uint8_t           nMembank)
{
  // check whether to reset memory bank 201
  if ((CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank201RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank201RAM.lockByte = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_power_supply_read_memory_bank201(PCtrlGearInstance     pCtrlInst,
                                                       uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_201_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_201_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank201RAM.lockByte;
      break;

      case 0x03: // Version of the memory bank
        *pRespValue = CTRL_GEAR_MEMORY_BANK_201_VERSION;
      break;

      case 0x04: // Guaranteed supply current
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.power_supply_params.guaranteedCurrent;
      break;

      case 0x05: // Maximum supply current
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.power_supply_params.maxCurrent;
      break;

      case 0x06: // Status of integrated bus power supply
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank201NVM.status;
      break;

      default:
        rc = R_IGNORE_FRAME;
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_power_supply_write_memory_bank201_direct(PCtrlGearInstance  pCtrlInst,
                                                               uint8_t            nCell,
                                                               uint8_t            reqValue)
{
  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_201_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE:
      // write memory cell
      pCtrlInst->gearMemBank201RAM.lockByte = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_201_STATUS_BYTE:
      // check value range
      if (0x01 < reqValue)
      {
        return (R_IGNORE_FRAME);
      }
      // write memory cell
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank201NVM.status)
      {
        pCtrlInst->gearPersistent.gearMemBank201NVM.status = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    default:
      // cell not writable
      return (R_IGNORE_FRAME);
    break;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// write power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_power_supply_write_memory_bank201(PCtrlGearInstance     pCtrlInst,
                                                        ctrl_gear_commands_t* pGearCmd,
                                                        uint8_t               reqValue,
                                                        uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;
  uint8_t           offset;
  dalilib_action_t  action;

  offset = pCtrlInst->gearVariables.DTR0;

  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_201_ADDRESS_LAST_CELL < offset)
  {
    rc = R_IGNORE_FRAME;
  }

  // check whether memory bank is unlocked
  if ((R_DALILIB_OK == rc) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != offset) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK != pCtrlInst->gearMemBank201RAM.lockByte))
  {
    rc = R_IGNORE_FRAME;
  }

  // check value change
  if ((CTRL_GEAR_MEMORY_BANK_201_STATUS_BYTE == offset) &&
      (pCtrlInst->gearPersistent.gearMemBank201NVM.status != reqValue))
  {
    // notify application
    memset(&action, 0, sizeof(action));
    action.actionType = DALILIB_CTRL_GEAR_REACTION;
    action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_MEMORY_CELL_CHANGED;
    action.gearReact.memoryValue.memoryBankNr = CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER;
    action.gearReact.memoryValue.memoryCellIdx = CTRL_GEAR_MEMORY_BANK_201_STATUS_BYTE;
    action.gearReact.memoryValue.value = reqValue;
    action.gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
    dali_cb_reaction(pCtrlInst->pInst, &action);
  }

  // write memory bank cell
  if (R_DALILIB_OK == rc)
  {
    rc = gear_power_supply_write_memory_bank201_direct(pCtrlInst, offset, reqValue);
  }

  // check whether to reply
  if ((R_DALILIB_OK == rc) &&
      (GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (rc);
}

R_DALILIB_RESULT gear_power_supply_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                            dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  dali_ctrl_gear_power_supply_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_POWER_SUPPLY_ID;
  block.version = DALI_CTRL_GEAR_PERS_POWER_SUPPLY_VERSION;

  // populate memory block
  block.status       = pGearPersistent->gearMemBank201NVM.status;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_POWER_SUPPLY_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_power_supply_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                             uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  char                                                buf[128];
  dali_ctrl_mem_bank201_NVM_t*                        pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank201NVM;
  dali_ctrl_gear_power_supply_pers_variables_block_t *pBlock = (dali_ctrl_gear_power_supply_pers_variables_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->status = pBlock->status;

  // check value range
  if (0x01 < pPersistentVariables->status)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable status is out of bounds. Value: %u Range: [0, 1]",
             pPersistentVariables->status);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->status = 0x01;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the power supply forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_power_supply_process_forward(PCtrlGearInstance      pCtrlInst,
                                                   dalilib_frame_t*       pForwardFrame,
                                                   ctrl_gear_commands_t** ppGearCmd,
                                                   gear_adr_t*            pReceivedAdr,
                                                   uint8_t*               pRespValue)
{
  R_DALILIB_RESULT      rc          = R_DALILIB_OK;

  (void)pReceivedAdr;

  *ppGearCmd = power_supply_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case POWER_SUPPLY_QUERY_ACTIVE_POWER_SUPPLY:
      *pRespValue = pCtrlInst->gearPersistent.gearMemBank201NVM.status;
    break;

    case POWER_SUPPLY_QUERY_EXTENDED_VERSION_NUMBER:
      *pRespValue = pCtrlInst->gearVariables.powerSupplyVariables.extendedVersionNumber;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_power_supply_get_cmd(POWER_SUPPLY_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(power_supply_commands_m); idx++)
  {
    if ((POWER_SUPPLY_CMD)power_supply_commands_m[idx].eGearCmd == eCmd)
    {
      return (&power_supply_commands_m[idx]);
    }
  }

  return (NULL);
}

#endif /* DALILIB_GEAR_POWER_SUPPLY_PRESENT */
