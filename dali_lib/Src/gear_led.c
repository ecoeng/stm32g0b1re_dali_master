/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_led.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"
#include "gear.h"

// eGearLEDCmd,                          selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t led_commands_m[] =
{
  {(GEAR_CMD) LED_REFERENCE_SYSTEM_POWER,          1,       0xE0,  0, 1},
  {(GEAR_CMD) LED_ENABLE_CURRENT_PROTECTOR,        1,       0xE1,  0, 1},
  {(GEAR_CMD) LED_DISABLE_CURRENT_PROTECTOR,       1,       0xE2,  0, 1},
  {(GEAR_CMD) LED_SELECT_DIMMING_CURVE,            1,       0xE3,  0, 1},
  {(GEAR_CMD) LED_STORE_DTR_AS_FFT,                1,       0xE4,  0, 1},
  {(GEAR_CMD) LED_QUERY_GEAR_TYPE,                 1,       0xED,  1, 0},
  {(GEAR_CMD) LED_QUERY_DIMMING_CURVE,             1,       0xEE,  1, 0},
  {(GEAR_CMD) LED_QUERY_POSSIBLE_OPERATING_MODES,  1,       0xEF,  1, 0},
  {(GEAR_CMD) LED_QUERY_FEATURES,                  1,       0xF0,  1, 0},
  {(GEAR_CMD) LED_QUERY_FAILRE_STATUS,             1,       0xF1,  1, 0},
  {(GEAR_CMD) LED_QUERY_SHORT_CIRCUIT,             1,       0xF2,  1, 0},
  {(GEAR_CMD) LED_QUERY_OPEN_CIRCUIT,              1,       0xF3,  1, 0},
  {(GEAR_CMD) LED_QUERY_LOAD_DECREASE,             1,       0xF4,  1, 0},
  {(GEAR_CMD) LED_QUERY_LOAD_INCREASE,             1,       0xF5,  1, 0},
  {(GEAR_CMD) LED_QUERY_CURRENT_PROTECTOR_ACTIVE,  1,       0xF6,  1, 0},
  {(GEAR_CMD) LED_QUERY_THERMAL_SHUT_DOWN,         1,       0xF7,  1, 0},
  {(GEAR_CMD) LED_QUERY_THERMAL_OVERLOAD,          1,       0xF8,  1, 0},
  {(GEAR_CMD) LED_QUERY_REFERENCE_RUNNING,         1,       0xF9,  1, 0},
  {(GEAR_CMD) LED_QUERY_MEASUREMENT_FAILED,        1,       0xFA,  1, 0},
  {(GEAR_CMD) LED_QUERY_CURRENT_PROTECTOR_ENABLED, 1,       0xFB,  1, 0},
  {(GEAR_CMD) LED_QUERY_OPERATING_MODE,            1,       0xFC,  1, 0},
  {(GEAR_CMD) LED_QUERY_FFT,                       1,       0xFD,  1, 0},
  {(GEAR_CMD) LED_QUERY_MIN_FFT,                   1,       0xFE,  1, 0},
  {(GEAR_CMD) LED_QUERY_EXTENDED_VERSION_NUMBER,   1,       0xFF,  1, 0},
  {(GEAR_CMD) LED_CMD_LAST,                        0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search LED command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* led_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(led_commands_m); idx++)
  {
    if (led_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&led_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// return failure status flag
//----------------------------------------------------------------------
static uint8_t led_failure_status(led_pers_variables_t* pLEDPersVar,
                                  led_variables_t*      pLEDVar,
                                  uint8_t               statusFlag)
{
  uint8_t flag = 0x00;

  (void)pLEDVar;

  // check, if this option supported
  if (pLEDPersVar->features & statusFlag)
  {
    if (pLEDPersVar->failureStatus & statusFlag)
    {
      flag = DALI_YES;
    }
  }

  return (flag);
}

//----------------------------------------------------------------------
// calculate renew the physical minimum level
//----------------------------------------------------------------------
static void led_calc_phm(PCtrlGearInstance pCtrlInst,
                         DIMMING_CURVE     dimmingCurve)
{
  double ret = 0.00;
  double factor = 84.333333333333333;

  if (DIMMING_CURVE_STANDARD == dimmingCurve)
  {
    // convert linear phm level to percent
    ret = ((pCtrlInst->gearPersistent.gearPersVariables.phm / 254 )* 100 );
    // convert percent to standard level
    pCtrlInst->gearPersistent.gearPersVariables.phm =  (log10(ret) * factor ) + factor + 1;
  }
  if (DIMMING_CURVE_LINEAR   == dimmingCurve)
  {
    // convert standard phm level to percent
    ret = (pow(10, ( (pCtrlInst->gearPersistent.gearPersVariables.phm - 1 ) / factor ) - 1 ));
    // convert percent to linear level
    ret = (254 * ret) / 100;
    pCtrlInst->gearPersistent.gearPersVariables.phm = ret;
  }
}

//----------------------------------------------------------------------
// convert uint8_t to percent*1000
//----------------------------------------------------------------------
uint32_t gear_led_level2percent(double level)
{
  return (uint32_t)(round( ((level * 100 * 1000 ) / 254.0 ) ));
}


//----------------------------------------------------------------------
// convert percent*1000 to uint8_t
//----------------------------------------------------------------------
uint8_t gear_led_percent2level(uint32_t percentvalue)
{
  return (uint8_t)round( (percentvalue * 254.0 ) / (100*1000) );
}

//----------------------------------------------------------------------
// return fast fade time [ms]
//----------------------------------------------------------------------
uint16_t gear_led_get_fft(PCtrlGearInstance pCtrlInst)
{
  return (pCtrlInst->gearPersistent.ledPersVariables.fft * FAST_FADE_TIME_FACTOR);
}

//----------------------------------------------------------------------
// initialize the variable after external power cycle
//----------------------------------------------------------------------
void gear_led_init_after_power_cycle (dali_ctrl_gear_pers_mem_t*   pGearPers,
                                      PCtrlGearVariables           pGearVariables)
{
  (void)pGearVariables;

  pGearPers->ledPersVariables.failureStatus &= 0x80;
  pGearPers->ledPersVariables.operatingMode &= 0x10;
  pGearPers->ledPersVariables.currentProtectorStatus = CURRENT_PROTECTOR_DISABLED;
}


//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void gear_led_by_reset(dali_ctrl_gear_pers_mem_t*   pPersistent)
{
  pPersistent->ledPersVariables.fft = 0x00;
  pPersistent->ledPersVariables.dimmingCurve = DIMMING_CURVE_STANDARD;
  pPersistent->ledPersVariables.operatingMode &= 0x10;
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t gear_led_check_nvm_variables(dali_ctrl_gear_pers_mem_t*   pPersistent)
{
  if (0 != pPersistent->ledPersVariables.fft)
  {
    return (FALSE);
  }
  if (0 != pPersistent->ledPersVariables.dimmingCurve)
  {
    return (FALSE);
  }
  if (pPersistent->ledPersVariables.operatingMode & 0x10)
  {
    return (FALSE);
  }

  return (TRUE);
}

//----------------------------------------------------------------------
// initialize the default variable of the input device push button
//----------------------------------------------------------------------
void gear_led_init_persistent(PDaliLibInstance             pInst,
                              dali_ctrl_gear_pers_mem_t*   pGearPers)
{
  pGearPers->ledPersVariables.min_fft          = pInst->config.vendor.led_params.min_fast_fade_time;

  // gear type
  pGearPers->ledPersVariables.gearType    = 0x00;
  if (pInst->config.vendor.led_params.integrated_power_supply)
  {
    pGearPers->ledPersVariables.gearType  |= INTEGRATED_POWER_SUPPLY_FLAG;
  }
  if (pInst->config.vendor.led_params.integrated_led_module)
  {
    pGearPers->ledPersVariables.gearType  |= INTEGRATED_LED_MODULE_FLAG;
  }
  if (pInst->config.vendor.led_params.ac_supply)
  {
    pGearPers->ledPersVariables.gearType  |= AC_SUPPLY_FLAG;
  }
  if (pInst->config.vendor.led_params.dc_supply)
  {
    pGearPers->ledPersVariables.gearType  |= DC_SUPPLY_FLAG;
  }

  // possible operating modes
  pGearPers->ledPersVariables.operatingModes      = 0x00;
  if (pInst->config.vendor.led_params.pwm_possible)
  {
    pGearPers->ledPersVariables.operatingModes    |= PWM_FLAG;
  }
  if (pInst->config.vendor.led_params.am_possible)
  {
    pGearPers->ledPersVariables.operatingModes    |= AM_FLAG;
  }
  if (pInst->config.vendor.led_params.regulated_output)
  {
    pGearPers->ledPersVariables.operatingModes    |= REGULATED_OUTPUT_FLAG;
  }
  if (pInst->config.vendor.led_params.high_current_pulse)
  {
    pGearPers->ledPersVariables.operatingModes    |= HIGH_CURRENT_PULSE_FLAG;
  }

  // features
  pGearPers->ledPersVariables.features          = 0x00;
  if (pInst->config.vendor.led_params.short_circuit)
  {
    pGearPers->ledPersVariables.features        |= SHORT_CIRCUIT_FLAG;
  }
  if (pInst->config.vendor.led_params.open_circuit)
  {
    pGearPers->ledPersVariables.features        |= OPEN_CIRCUIT_FLAG;
  }
  if (pInst->config.vendor.led_params.load_decrease)
  {
    pGearPers->ledPersVariables.features        |= LOAD_DECREASE_FLAG;
  }
  if (pInst->config.vendor.led_params.load_increase)
  {
    pGearPers->ledPersVariables.features        |= LOAD_INCREASE_FLAG;
  }
  if (pInst->config.vendor.led_params.overcurrent_protection)
  {
    pGearPers->ledPersVariables.features        |= CURRENT_PROTECTOR_FLAG;
    pGearPers->ledPersVariables.currentProtectorStatus = CURRENT_PROTECTOR_ENABLED;
  }
  if (pInst->config.vendor.led_params.thermal_shutdown)
  {
    pGearPers->ledPersVariables.features        |= THERMAL_SHUT_DOWN_FLAG;
  }
  if (pInst->config.vendor.led_params.thermal_overload)
  {
    pGearPers->ledPersVariables.features        |= THERMAL_OVERLOAD_FLAG;
  }
  if (pInst->config.vendor.led_params.referencing)
  {
    pGearPers->ledPersVariables.features        |= REFERENCE_FLAG;
  }

  pGearPers->ledPersVariables.extendedVersionNumber = CTRL_GEAR_EXTENDED_VERSION_NUMBER;
  pGearPers->ledPersVariables.deviceType            = GEAR_TYPE_LED_MODULES;
}

R_DALILIB_RESULT gear_led_save_mem_block_pers_vars_frequent(PDaliLibInstance           pInst,
                                                            dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  dali_ctrl_gear_led_pers_variables_frequent_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_LED_FREQUENT_ID;
  block.version = DALI_CTRL_GEAR_PERS_LED_FREQUENT_VERSION;

  // populate memory block
  block.failureStatus          = pGearPersistent->ledPersVariables.failureStatus;
  block.currentProtectorStatus = pGearPersistent->ledPersVariables.currentProtectorStatus;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_LED_FREQUENT_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_led_save_mem_block_pers_vars_operating(PDaliLibInstance           pInst,
                                                             dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_gear_led_pers_variables_operating_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_LED_OPERATING_ID;
  block.version = DALI_CTRL_GEAR_PERS_LED_OPERATING_VERSION;

  // populate memory block
  block.fft            = pGearPersistent->ledPersVariables.fft;
  block.operatingMode  = pGearPersistent->ledPersVariables.operatingMode;
  block.referenceValue = pGearPersistent->ledPersVariables.referenceValue;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_LED_OPERATING_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_led_save_mem_block_pers_vars_config(PDaliLibInstance           pInst,
                                                          dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                 rc = R_DALILIB_OK;
  dali_ctrl_gear_led_pers_variables_config_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_LED_CONFIG_ID;
  block.version = DALI_CTRL_GEAR_PERS_LED_CONFIG_VERSION;

  // populate memory block
  block.min_fft               = pGearPersistent->ledPersVariables.min_fft;
  block.gearType              = pGearPersistent->ledPersVariables.gearType;
  block.operatingModes        = pGearPersistent->ledPersVariables.operatingModes;
  block.features              = pGearPersistent->ledPersVariables.features;
  block.dimmingCurve          = pGearPersistent->ledPersVariables.dimmingCurve;
  block.extendedVersionNumber = pGearPersistent->ledPersVariables.extendedVersionNumber;
  block.deviceType            = pGearPersistent->ledPersVariables.deviceType;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_LED_CONFIG_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_led_apply_mem_block_pers_vars_frequent(PCtrlGearInstance pCtrlInst,
                                                             uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT      rc = R_DALILIB_OK;
  char                  buf[192];
  led_pers_variables_t* pPersistentVariables = &pCtrlInst->gearPersistent.ledPersVariables;
  PDaliLibInstance      pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_gear_led_pers_variables_frequent_block_t *pBlock = (dali_ctrl_gear_led_pers_variables_frequent_block_t*)pDaliMem;

  pPersistentVariables->failureStatus          = pBlock->failureStatus;
  pPersistentVariables->currentProtectorStatus = pBlock->currentProtectorStatus;
  // copy mem block

  // check value range
  if (0   != pPersistentVariables->failureStatus &&
      12  != pPersistentVariables->failureStatus &&
      32  != pPersistentVariables->failureStatus &&
      44  != pPersistentVariables->failureStatus &&
      64  != pPersistentVariables->failureStatus &&
      76  != pPersistentVariables->failureStatus &&
      96  != pPersistentVariables->failureStatus &&
      108 != pPersistentVariables->failureStatus &&
      128 != pPersistentVariables->failureStatus &&
      140 != pPersistentVariables->failureStatus &&
      160 != pPersistentVariables->failureStatus &&
      172 != pPersistentVariables->failureStatus &&
      192 != pPersistentVariables->failureStatus &&
      204 != pPersistentVariables->failureStatus &&
      224 != pPersistentVariables->failureStatus &&
      236 != pPersistentVariables->failureStatus)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable failureStatus is out of bounds. Value: %u Range: [0, 12, 32, 44, 64, 76, 96, 108, 128, 140, 160, 172, 192, 204, 224, 236]",
             pPersistentVariables->failureStatus);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->failureStatus = 0x00;
  }
  if (2  < pPersistentVariables->currentProtectorStatus)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable currentProtectorStatus is out of bounds. Value: %u Range: [0..2]",
             pPersistentVariables->currentProtectorStatus);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->currentProtectorStatus = (pInst->config.vendor.led_params.overcurrent_protection ? CURRENT_PROTECTOR_ENABLED : CURRENT_PROTECTOR_DISABLED);
  }

  return (rc);
}

R_DALILIB_RESULT gear_led_apply_mem_block_pers_vars_operating(PCtrlGearInstance pCtrlInst,
                                                              uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT      rc = R_DALILIB_OK;
  char                  buf[128];
  led_pers_variables_t* pPersistentVariables = &pCtrlInst->gearPersistent.ledPersVariables;
  dali_ctrl_gear_led_pers_variables_operating_block_t *pBlock = (dali_ctrl_gear_led_pers_variables_operating_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->fft            = pBlock->fft;
  pPersistentVariables->operatingMode  = pBlock->operatingMode;
  pPersistentVariables->referenceValue = pBlock->referenceValue;

  // check value range
  if (0 != pPersistentVariables->fft &&
      (27 < pPersistentVariables->fft ||
          pPersistentVariables->min_fft  > pPersistentVariables->fft))
  {
    snprintf(buf, sizeof(buf),
             "dali library variable fft (fastFadeTime) is out of bounds. Value: %u Range: [0, min_fft..27]",
             pPersistentVariables->fft);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->fft = 0x00;
  }
  if (0x00 < pPersistentVariables->operatingMode &&
      0x80 > pPersistentVariables->operatingMode)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable operatingMode is out of bounds. Value: %u Range: [0x00, 0x80..0xFF]",
             pPersistentVariables->operatingMode);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->operatingMode = DALI_CTRL_GEAR_NORMAL_OPERTING_MODE;
  }

  return (rc);
}

R_DALILIB_RESULT gear_led_apply_mem_block_pers_vars_config(PCtrlGearInstance pCtrlInst,
                                                           uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT      rc = R_DALILIB_OK;
  char                  buf[128];
  led_pers_variables_t* pPersistentVariables = &pCtrlInst->gearPersistent.ledPersVariables;
  PDaliLibInstance      pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_gear_led_pers_variables_config_block_t *pBlock = (dali_ctrl_gear_led_pers_variables_config_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->min_fft               = pBlock->min_fft;
  pPersistentVariables->gearType              = pBlock->gearType;
  pPersistentVariables->operatingModes        = pBlock->operatingModes;
  pPersistentVariables->features              = pBlock->features;
  pPersistentVariables->dimmingCurve          = pBlock->dimmingCurve;
  pPersistentVariables->extendedVersionNumber = pBlock->extendedVersionNumber;
  pPersistentVariables->deviceType            = pBlock->deviceType;

  // check value range
  if (27 < pPersistentVariables->min_fft &&
      1 > pPersistentVariables->min_fft)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable min_fft is out of bounds. Value: %u Range: [1..27]",
             pPersistentVariables->min_fft);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->min_fft = pInst->config.vendor.led_params.min_fast_fade_time;
  }
  if (0x0F < pPersistentVariables->gearType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable gearType is out of bounds. Value: %u Range: [0x00..0x0F]",
             pPersistentVariables->gearType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->gearType    = 0x00;
    if (pInst->config.vendor.led_params.integrated_power_supply)
    {
      pPersistentVariables->gearType  |= INTEGRATED_POWER_SUPPLY_FLAG;
    }
    if (pInst->config.vendor.led_params.integrated_led_module)
    {
      pPersistentVariables->gearType  |= INTEGRATED_LED_MODULE_FLAG;
    }
    if (pInst->config.vendor.led_params.ac_supply)
    {
      pPersistentVariables->gearType  |= AC_SUPPLY_FLAG;
    }
    if (pInst->config.vendor.led_params.dc_supply)
    {
      pPersistentVariables->gearType  |= DC_SUPPLY_FLAG;
    }
  }
  if (0x0F < pPersistentVariables->operatingModes)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable operatingModes is out of bounds. Value: %u Range: [0x00..0x0F]",
             pPersistentVariables->operatingModes);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->operatingModes      = 0x00;
    if (pInst->config.vendor.led_params.pwm_possible)
    {
      pPersistentVariables->operatingModes    |= PWM_FLAG;
    }
    if (pInst->config.vendor.led_params.am_possible)
    {
      pPersistentVariables->operatingModes    |= AM_FLAG;
    }
    if (pInst->config.vendor.led_params.regulated_output)
    {
      pPersistentVariables->operatingModes    |= REGULATED_OUTPUT_FLAG;
    }
    if (pInst->config.vendor.led_params.high_current_pulse)
    {
      pPersistentVariables->operatingModes    |= HIGH_CURRENT_PULSE_FLAG;
    }
  }
  if (1 < pPersistentVariables->dimmingCurve)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable dimmingCurve is out of bounds. Value: %u Range: [0..1]",
             pPersistentVariables->dimmingCurve);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->dimmingCurve = DIMMING_CURVE_STANDARD;
  }
  if (2 < pPersistentVariables->extendedVersionNumber)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable extendedVersionNumber is out of bounds. Value: %u Range: [0..2]",
             pPersistentVariables->extendedVersionNumber);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->extendedVersionNumber = CTRL_GEAR_EXTENDED_VERSION_NUMBER;
  }
  if (GEAR_TYPE_LED_MODULES != pPersistentVariables->deviceType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable deviceType is out of bounds. Value: %u Range: [6]",
             pPersistentVariables->deviceType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->deviceType = GEAR_TYPE_LED_MODULES;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the LED forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_led_process_forward(PCtrlGearInstance      pCtrlInst,
                                          dalilib_frame_t*       pForwardFrame,
                                          ctrl_gear_commands_t** ppGearCmd,
                                          gear_adr_t*            pReceivedAdr,
                                          uint8_t*               pRespValue)
{
  R_DALILIB_RESULT      rc          = R_IGNORE_FRAME;
  led_pers_variables_t* pLEDPersVar = NULL;
  led_variables_t*      pLEDVar     = NULL;

  (void)pReceivedAdr;

  pLEDPersVar = &pCtrlInst->gearPersistent.ledPersVariables;
  pLEDVar = &pCtrlInst->gearVariables.ledVariables;

  *ppGearCmd = led_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  // ignore the forward frames during the measurement
  if (pLEDVar->referenceRunning)
  {
    if ((LED_CMD)(*ppGearCmd)->eGearCmd > LED_REFERENCE_SYSTEM_POWER &&
        (LED_CMD)(*ppGearCmd)->eGearCmd <= LED_STORE_DTR_AS_FFT)
    {
      rc = R_IGNORE_FRAME;
      return (rc);
    }
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case LED_REFERENCE_SYSTEM_POWER:
      {
        // check, if this reference supported
        if (pLEDPersVar->features & REFERENCE_FLAG)
        {
          if (CURRENT_PROTECTOR_ACTIVE == pLEDPersVar->currentProtectorStatus)
          {
            if (0 == (pLEDPersVar->failureStatus & REFERENCE_FLAG))
            {
              pLEDPersVar->failureStatus |= REFERENCE_FLAG;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
          else 
          {
            if ((pLEDPersVar->features & CURRENT_PROTECTOR_FLAG) || 
                (pLEDPersVar->features & LOAD_DECREASE_FLAG) || 
                (pLEDPersVar->features & LOAD_INCREASE_FLAG))
            {
              pLEDVar->referenceRunning = TRUE;
              pLEDVar->referenceStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              dalilib_action_t action;

              memset( &action, 0, sizeof(action));

              action.actionType = DALILIB_CTRL_GEAR_REACTION;
              action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_START;
              dali_cb_reaction(pCtrlInst->pInst, &action);
            }
          }
        }
      }
    break;

    case LED_ENABLE_CURRENT_PROTECTOR:
      {
        // check, if this current protector supported
        if (pLEDPersVar->features & CURRENT_PROTECTOR_FLAG)
        {
          // not executed reference measurement
          if (0x00 != pLEDPersVar->referenceValue)
          {
            if (CURRENT_PROTECTOR_ENABLED != pLEDPersVar->currentProtectorStatus)
            {
              pLEDPersVar->currentProtectorStatus = CURRENT_PROTECTOR_ENABLED;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
        }
      }
    break;

    case LED_DISABLE_CURRENT_PROTECTOR:
      {
        // check, if this current protector supported
        if (pLEDPersVar->features & CURRENT_PROTECTOR_FLAG)
        {
          if (CURRENT_PROTECTOR_DISABLED != pLEDPersVar->currentProtectorStatus)
          {
            pLEDPersVar->currentProtectorStatus = CURRENT_PROTECTOR_DISABLED;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case LED_SELECT_DIMMING_CURVE:
      {
        if (pCtrlInst->gearVariables.DTR0 == 0 || pCtrlInst->gearVariables.DTR0 == 1)
        {
          if (pLEDPersVar->dimmingCurve != pCtrlInst->gearVariables.DTR0)
          {
            pLEDPersVar->dimmingCurve = (DIMMING_CURVE)pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
            led_calc_phm(pCtrlInst, pLEDPersVar->dimmingCurve);
          }
        }
      }
    break;

    case LED_STORE_DTR_AS_FFT:
      {
        if (pCtrlInst->gearVariables.DTR0 == 0 ||
            (pCtrlInst->gearVariables.DTR0 >= pLEDPersVar->min_fft &&
             pCtrlInst->gearVariables.DTR0 <= MAX_FAST_FADE_TIME))
        {
          if (pCtrlInst->gearVariables.DTR0 != pLEDPersVar->fft)
          {
            pLEDPersVar->fft = pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
        else
        {
          if (pCtrlInst->gearVariables.DTR0 > 0 &&
              pCtrlInst->gearVariables.DTR0 < pLEDPersVar->min_fft)
          {
            if (pLEDPersVar->min_fft != pLEDPersVar->fft)
            {
              pLEDPersVar->fft = pLEDPersVar->min_fft;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
          else
          {
            if (pCtrlInst->gearVariables.DTR0 > MAX_FAST_FADE_TIME)
            {
              if (MAX_FAST_FADE_TIME != pLEDPersVar->fft)
              {
                pLEDPersVar->fft = MAX_FAST_FADE_TIME;
                pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK;
                if (0 == pCtrlInst->gearPersistentChangedTime)
                {
                  pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
                }
              }
            }
          }
        }
      }
    break;

    case LED_QUERY_GEAR_TYPE:
      {
        *pRespValue = pLEDPersVar->gearType;
        rc = R_DALILIB_OK;
      }
    break;

    case LED_QUERY_DIMMING_CURVE:
      {
        *pRespValue = pLEDPersVar->dimmingCurve;
        rc = R_DALILIB_OK;
      }
    break;

    case LED_QUERY_POSSIBLE_OPERATING_MODES:
      {
        *pRespValue = pLEDPersVar->operatingModes;
        rc = R_DALILIB_OK;
      }
    break;

    case LED_QUERY_FEATURES:
      {
        *pRespValue = pLEDPersVar->features;
        rc = R_DALILIB_OK;
      }
    break;

    case LED_QUERY_FAILRE_STATUS:
      {
        *pRespValue = pLEDPersVar->failureStatus;
        rc = R_DALILIB_OK;
      }
    break;

    case LED_QUERY_SHORT_CIRCUIT:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, SHORT_CIRCUIT_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_OPEN_CIRCUIT:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, OPEN_CIRCUIT_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_LOAD_DECREASE:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, LOAD_DECREASE_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_LOAD_INCREASE:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, LOAD_INCREASE_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_CURRENT_PROTECTOR_ACTIVE:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, CURRENT_PROTECTOR_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_THERMAL_SHUT_DOWN:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, THERMAL_SHUT_DOWN_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_THERMAL_OVERLOAD:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, THERMAL_OVERLOAD_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_REFERENCE_RUNNING:
      {
        // check, if this option supported
        if (pLEDPersVar->features & REFERENCE_FLAG)
        {
          if (pLEDVar->referenceRunning)
          {
            rc = R_DALILIB_OK;
            *pRespValue = DALI_YES;
          }
        }
      }
    break;

    case LED_QUERY_MEASUREMENT_FAILED:
      {
        if (led_failure_status(pLEDPersVar, pLEDVar, REFERENCE_FLAG))
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    case LED_QUERY_CURRENT_PROTECTOR_ENABLED:
      {
        // check, if this current protector supported
        if (pLEDPersVar->features & CURRENT_PROTECTOR_FLAG)
        {
          rc = R_DALILIB_OK;
          *pRespValue = pLEDPersVar->currentProtectorStatus;
        }
      }
    break;

    case LED_QUERY_OPERATING_MODE:
      {
        rc = R_DALILIB_OK;
        *pRespValue = pLEDPersVar->operatingModes;
      }
    break;

    case LED_QUERY_FFT:
      {
        rc = R_DALILIB_OK;
        *pRespValue = pLEDPersVar->fft;
      }
    break;

    case LED_QUERY_MIN_FFT:
      {
        rc = R_DALILIB_OK;
        *pRespValue = pLEDPersVar->min_fft;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_led_get_cmd(LED_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(led_commands_m); idx++)
  {
    if ((LED_CMD)led_commands_m[idx].eGearCmd == eCmd)
    {
      return (&led_commands_m[idx]);
    }
  }

  return (NULL);
}

