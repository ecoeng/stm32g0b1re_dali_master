/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * main.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"

static dali_instance_t instances_g[MAX_STACK_INSTANCE];
static char szLogMsg_m[64+1];

//----------------------------------------------------------------------
// Time between newtime and oldtime in DALI ticks.
// return: [ms]
//----------------------------------------------------------------------
uint32_t dali_difftime (uint32_t newtime, uint32_t oldtime)
{
  if (newtime < oldtime)
  {
    /* overflow */
    /* UINT32_MAX / 100 because timestamps come in 100µs              */
    /* LL time works in µs and so it wraps if reached UINT32_MAX      */
    /* here we count only until (UINT32_MAX / 100) if the wrap occurs */

    return (( newtime + ( (UINT32_MAX / 100) - oldtime))/10);
  }
  return ((newtime - oldtime)/10);
}

//----------------------------------------------------------------------
// reset the stack status
//----------------------------------------------------------------------
static R_DALILIB_RESULT dali_reset_stack_status(PDaliLibInstance pInst)
{
  pInst->status = STACK_STATUS_IDLE;
  memset(&pInst->last_tx_frame, 0, sizeof(pInst->last_tx_frame));

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// reaction functions
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// started callback function will be called
//----------------------------------------------------------------------
void dali_cb_ready(PDaliLibInstance pInst)
{
  if (NULL != pInst->config.callbacks.fPAppReady)
  {
    (void)(pInst->config.callbacks.fPAppReady)();
  }
}

//----------------------------------------------------------------------
// dali frame will be send through callback function
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_cb_send(PDaliLibInstance pInst, send_frame_t* pFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;

  memcpy(&pInst->last_tx_frame, pFrame, sizeof(pInst->last_tx_frame));
  if (NULL != pInst->config.callbacks.fPAppSend)
  {
    rc = (pInst->config.callbacks.fPAppSend)(pInst->config.context, &pFrame->frame);

    if (R_DALILIB_OK==rc)
    {
      pInst->status = STACK_STATUS_WAIT_CONF;
      pInst->waitTimeout = pInst->currentTime;
    }
    else
    {
      rc = R_DALILIB_BUSY;
    }
  }

  return (rc);
}


//----------------------------------------------------------------------
// This function is called by the stack to trigger an action on application
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_cb_reaction(PDaliLibInstance pInst, dalilib_action_t* pAction)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;

  if (NULL != pInst->config.callbacks.fPAppReAction)
  {
    rc = (pInst->config.callbacks.fPAppReAction)(pAction);
  }

  return (rc);
}


//----------------------------------------------------------------------
// create dali memory block
// struct of dali memory block:
//    MEM_START[7]+MEM_DATA_SIZE[1]+MEM_RAW_MAX_SIZE+MEM_STOP[5]
// unused byte will be filled with 0xFF
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_create_mem(PDaliLibInstance pInst,
                                 void*            pRawMem,
                                 uint16_t         nRawSize,
                                 uint8_t*         pDaliMem)
{
  (void)pInst;

  if (nRawSize >= MEM_SIZE)
  {
    dali_cb_log(pInst, R_DALILIB_PERSISTENT_MEM_DATA_SIZE_TOO_SMALL, "dali library MEM_SIZE too small");
    return (R_INVALID_MEM_DATA_SIZE);
  }
  if(nRawSize <= 0)
  {
    return (R_INVALID_MEM_DATA_SIZE);
  }

  memset( pDaliMem, 0xFF, MEM_DATA_MAX_SIZE);

  // put start byte
  memcpy( pDaliMem, MEM_START, MEM_START_SIZE);

  // put data size bytes
  pDaliMem[MEM_START_SIZE] = nRawSize >> 8;
  pDaliMem[MEM_START_SIZE + 1] = nRawSize;

  memcpy( pDaliMem + MEM_START_SIZE + MEM_DATA_SIZE_SIZE, pRawMem, nRawSize);

  // put stop byte
  memcpy( pDaliMem + MEM_START_SIZE + MEM_DATA_SIZE_SIZE + nRawSize, MEM_STOP, MEM_STOP_SIZE);

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// persistent parameters will be saved
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_cb_save_mem(PDaliLibInstance pInst,
                                  void*            pMem,
                                  uint16_t         nSize)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;
  uint8_t          daliMem[MEM_DATA_MAX_SIZE];    // persistent data memory block

  if (NULL != pInst->config.callbacks.fPAppSaveMem)
  {
    rc = dali_create_mem(pInst, pMem, nSize, daliMem);
    if (R_DALILIB_OK == rc)
    {
      rc = (pInst->config.callbacks.fPAppSaveMem)(&daliMem, MEM_DATA_MAX_SIZE);
    }
  }
  return (rc);
}

R_DALILIB_RESULT dali_cb_save_mem_block(PDaliLibInstance pInst,
                                        void*            pMem,
                                        uint16_t         nSize,
                                        uint8_t          blockId)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;

  if (NULL != pInst->config.callbacks.fPAppSaveModuleMem)
  {
    if (nSize >= MEM_SIZE)
    {
      dali_cb_log(pInst, R_DALILIB_PERSISTENT_MEM_DATA_SIZE_TOO_SMALL, "dali library MEM_SIZE too small");
      rc = R_INVALID_MEM_DATA_SIZE;
    }
    if(nSize <= 0)
    {
      rc = R_INVALID_MEM_DATA_SIZE;
    }

    if (R_DALILIB_OK == rc)
    {
      rc = (pInst->config.callbacks.fPAppSaveModuleMem)(blockId, pMem, nSize);
    }
  }
  return (rc);
}

static R_DALILIB_RESULT dali_check_mem(uint8_t* pMem)
{
  uint16_t nDataSize = 0;

  if (0 == memcmp(pMem, MEM_START, MEM_START_SIZE))
  {
    nDataSize = (pMem[MEM_START_SIZE] << 8) | pMem[MEM_START_SIZE + 1];
    if (0 == memcmp(pMem + (MEM_START_SIZE + MEM_DATA_SIZE_SIZE + nDataSize), MEM_STOP, MEM_STOP_SIZE))
    {
      return (R_DALILIB_OK);
    }
  }
  return (R_MEM_NOT_DEFINED);
}

//----------------------------------------------------------------------
// persistent parameters will be load
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_cb_load_mem(PDaliLibInstance pInst,
                                  void*            pMem)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;
  uint8_t          daliMem[MEM_DATA_MAX_SIZE];    // persistent data memory block
  uint16_t         nDataSize = 0;

  if (NULL != pInst->config.callbacks.fPAppLoadMem)
  {
    rc = (pInst->config.callbacks.fPAppLoadMem)(&daliMem, MEM_DATA_MAX_SIZE);

    if (R_DALILIB_OK == rc)
    {
      rc = dali_check_mem(daliMem);
    }
    if (R_DALILIB_OK == rc)
    {
      // apply raw data
      nDataSize = (daliMem[MEM_START_SIZE] << 8) | daliMem[MEM_START_SIZE + 1];
      memcpy(pMem, &daliMem[MEM_START_SIZE + MEM_DATA_SIZE_SIZE], nDataSize);
    }
  }
  return (rc);
}

R_DALILIB_RESULT dali_cb_load_mem_block(PDaliLibInstance pInst,
                                        uint8_t          blockId)
{
  R_DALILIB_RESULT rc = R_DALILIB_CALLBACK_FAILED;
  uint8_t          daliMem[MEM_DATA_MAX_SIZE];    // persistent data memory block

  if (NULL != pInst->config.callbacks.fPAppLoadModuleMem)
  {
    rc = (pInst->config.callbacks.fPAppLoadModuleMem)(blockId, &daliMem, MEM_DATA_MAX_SIZE);
  }

  if (R_DALILIB_OK == rc)
  {
    switch(pInst->config.mode)
    {
      case STACK_MODE_CONTROL_GEAR:
        rc = ctrl_gear_apply_mem_block(&pInst->ctrlInst.gearInstance, daliMem);
      break;

      case STACK_MODE_CONTROL_DEVICE:
        rc = ctrl_device_apply_mem_block(&pInst->ctrlInst.deviceInstance, daliMem);
      break;

      default:
        // no-op
      break;
    }
  }

  return (rc);
}

//----------------------------------------------------------------------
// log function
//----------------------------------------------------------------------
void dali_cb_log(PDaliLibInstance pInst, R_DALILIB_RESULT errorNo, char *pLogTxt)
{
  if (NULL != pInst->config.callbacks.fPAppLog)
  {
    memset( szLogMsg_m, 0, sizeof(szLogMsg_m));

    if (NULL != pLogTxt)
    {
      snprintf(szLogMsg_m, sizeof(szLogMsg_m)-1,"%s <error:%d>", pLogTxt, errorNo);
    }
    else
    {
      snprintf(szLogMsg_m, sizeof(szLogMsg_m)-1,"DALI-Library Log <error:%d>", errorNo);
    }
    (void)(pInst->config.callbacks.fPAppLog)(szLogMsg_m, strlen(szLogMsg_m));
  }
}


//----------------------------------------------------------------------
// This function is called by application for the library info
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_infoGet(dalilib_instance_t pInstance, dalilib_info_t* pInfo)
{
  (void)pInstance;

  pInfo->vendorID = DALILIB_VENDOR_ID;
  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// This function is called by application for the library version info
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_versionGet(dalilib_instance_t pInstance, dalilib_version_t* pVersion)
{
  (void)pInstance;

  pVersion->api_version_major = DALILIB_API_VERSION_MAJOR;
  pVersion->api_version_minor = DALILIB_API_VERSION_MINOR;
  pVersion->code_version_major = DALILIB_CODE_VERSION_MAJOR;
  pVersion->code_version_minor = DALILIB_CODE_VERSION_MINOR;

  return (R_DALILIB_OK);
}

static R_DALILIB_RESULT dali_check_actioncodes(STACK_MODE        mode,
                                               CTRL_DEVICE_MODE  devMode,
                                               dalilib_action_t* pAct)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(mode)
  {
    case STACK_MODE_CONTROL_GEAR:
      {
        if (DALILIB_CTRL_GEAR_ACTION != pAct->actionType)
        {
          // control gear can't send forward frames
          if (DALILIB_RAW_FRAME_ACTION != pAct->actionType)
          {
            rc = R_DALILIB_NOT_SUPPORTED_ACTION;
          }
        }
        else
        {
          if (pAct->gearAct.gearActionType != DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR)
          {
            // control gear can't send forward frames
            rc = R_DALILIB_NOT_SUPPORTED_ACTION;
          }
        }
      }
    break;
    
    case STACK_MODE_CONTROL_DEVICE:
      {
        if (CTRL_DEV_MODE_SINGLE == devMode)
        {
          if (DALILIB_CTRL_GEAR_ACTION != pAct->actionType &&
              DALILIB_RAW_FRAME_ACTION != pAct->actionType)
          {
            // single application controller need send only
            // control gear commands and raw frames, other
            // application controller does not exists on bus.
            rc = R_DALILIB_NOT_SUPPORTED_ACTION;
          }
        }
        else
        {
          if (CTRL_DEV_MODE_INPUT == devMode)
          {
            if (DALILIB_CTRL_DEVICE_ACTION == pAct->actionType &&
              DALILIB_ACT_TYPE_INTERNAL_CTRL_DEVICE == pAct->deviceAct.deviceActionType)
            {
              rc = R_DALILIB_OK;
            }
            else
            {
              if (DALILIB_EVENT_ACTION != pAct->actionType &&
                  DALILIB_RAW_FRAME_ACTION != pAct->actionType)
              {
                // input device can send only event messages and raw frames
                rc = R_DALILIB_NOT_SUPPORTED_ACTION;
              }
            }
          }
          else
          {
            if (CTRL_DEV_MODE_MULTI == devMode)
            {
              if (DALILIB_EVENT_ACTION == pAct->actionType)
              {
                // application controller can't create event messages
                rc = R_DALILIB_NOT_SUPPORTED_ACTION;
              }
            }
          }
        }
      }
    break;
    
    case STACK_MODE_GATEWAY:
    default:
      {
        // not implemented
        rc = R_DALILIB_NOT_SUPPORTED_ACTION;
      }
    break;
  }
  return (rc);
}

static R_DALILIB_RESULT dali_raw_frame_action(PDaliLibInstance         pInst,
                                              dalilib_act_raw_frame_t* pRawFrameAct,
                                              send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  (void)pInst;

  // check length
  if (MIN_RAW_FRAME_LEN > pRawFrameAct->datalength ||
      FRAME_BUFFER_LEN < pRawFrameAct->datalength)
  {
    rc = R_DALILIB_FRAME_NOT_SUPPORTED;
  }

  if (R_DALILIB_OK == rc)
  {
    #ifndef API_FRAMES_64BIT
    pNewSendFrame->frame.data = (uint32_t) pRawFrameAct->data;
    #else
    pNewSendFrame->frame.data = pRawFrameAct->data;
    #endif
    pNewSendFrame->frame.datalength = pRawFrameAct->datalength;
    pNewSendFrame->frame.priority = FRAME_PRIO_FF_P5;
    if (pRawFrameAct->sendTwice)
    {
      pNewSendFrame->frame.sendtwice = 0x01;
    }
    pNewSendFrame->type = SEND_FRAME_TYPE_RAW;
    pNewSendFrame->status = FRAME_WITHOUT_RESPONSE;
  }

  return rc;
}

//----------------------------------------------------------------------
// raw frame time out
// - no answer
// - forward frame not send
//----------------------------------------------------------------------
R_DALILIB_RESULT dali_raw_frame_process_response_timeout(PDaliLibInstance            pInst,
                                                         DALILIB_RESPONSE_VALUE_TYPE valueType)
{
  dalilib_action_t action;

  memset(&action, 0, sizeof(action));
  action.actionType = DALILIB_RAW_FRAME_REACTION;
  action.rawFrameReact.reactionCode = DALILIB_REACT_RAW_FRAME_SEND;
  action.rawFrameReact.valueType = valueType;

  // notify the application
  // transmit the response values
  dali_cb_reaction(pInst, &action);

  return (R_DALILIB_OK);
}

static R_DALILIB_RESULT dali_process_response_timeout(PDaliLibInstance            pInst,
                                                      send_frame_t*               pFrame,
                                                      DALILIB_RESPONSE_VALUE_TYPE valueType)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pFrame->type)
  {
    case SEND_FRAME_TYPE_CTRL_GEAR:
      rc = ctrl_gear_response_timeout(pInst, pFrame, valueType);
    break;

    case SEND_FRAME_TYPE_CTRL_DEV:
      rc = ctrl_device_process_response_timeout(pInst, pFrame, valueType);
    break;

    case SEND_FRAME_TYPE_RAW:
      rc = dali_raw_frame_process_response_timeout(pInst, valueType);
    break;

    default:
    break;
  }

  return (rc);
}

static R_DALILIB_RESULT dali_process_send_failed(PDaliLibInstance            pInst,
                                                 send_frame_t*               pFrame,
                                                 DALILIB_RESPONSE_VALUE_TYPE valueType)
{
R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pFrame->type)
  {
    case SEND_FRAME_TYPE_CTRL_DEV: // special behavior on failed send
      rc = ctrl_device_process_send_failed(pInst, pFrame, valueType);
    break;

    default:// default behavior on failed send
      rc = dali_process_response_timeout(pInst, pFrame, valueType);
    break;
  }


  return (rc);
}

//----------------------------------------------------------------------
// This function is called by the application to trigger an action
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_action(dalilib_instance_t pInstance, dalilib_action_t* pAction)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;
  send_frame_t newSendFrame;

  memset(&newSendFrame, 0, sizeof(newSendFrame));
  if (STACK_STATUS_TERMINATED == pInst->status)
  {
    //ignore all actions
    //demo-time is out or stack stopped
    return (R_DALILIB_STACK_TERMINATED);
  }

  if (STACK_STATUS_IDLE != pInst->status)
  {
    return (R_DALILIB_BUSY);
  }

  if (FALSE == pInst->config.internal.acceptActionsOnSystemFailure)
  {
    if ((STACK_MODE_CONTROL_GEAR == pInst->config.mode &&
          (pInst->ctrlInst.gearInstance.gearSystemFailure ||
           DALILIB_BUSPOWER_ON != pInst->ctrlInst.gearInstance.gearBusPower)) ||
        (STACK_MODE_CONTROL_DEVICE == pInst->config.mode &&
          (pInst->ctrlInst.deviceInstance.deviceSystemFailure ||
           DALILIB_BUSPOWER_ON != pInst->ctrlInst.deviceInstance.deviceBusPower)))
    {
      return (R_DALILIB_BUS_FAILURE);
    }
  }

  // check if action codes for this stack mode available
  rc = dali_check_actioncodes(pInst->config.mode,
                              pInst->config.vendor.control_device_mode,
                              pAction);
  if (R_DALILIB_OK != rc)
  {
    return (rc);
  }
  switch(pAction->actionType)
  {
    case DALILIB_CTRL_GEAR_ACTION:
      {
        rc = ctrl_gear_action(pInst, &pAction->gearAct, &newSendFrame);
        newSendFrame.type = SEND_FRAME_TYPE_CTRL_GEAR;
        if (R_DALILIB_OK == rc)
        {
          newSendFrame.lastActionType = pAction->gearAct.gearActionType;
          newSendFrame.lastAction = pAction->gearAct.action.reserved;
        }
      }
    break;
    
    case DALILIB_CTRL_DEVICE_ACTION:
      {
        rc = ctrl_device_action(&pInst->ctrlInst.deviceInstance, &pAction->deviceAct, &newSendFrame);
        newSendFrame.type = SEND_FRAME_TYPE_CTRL_DEV;
        if (R_DALILIB_OK == rc)
        {
          newSendFrame.lastActionType = pAction->deviceAct.deviceActionType;
          newSendFrame.lastAction = pAction->deviceAct.reserved;
        }
      }
    break;
    
    case DALILIB_EVENT_ACTION:
      {
        rc = ctrl_device_event_action(&pInst->ctrlInst.deviceInstance, &pAction->eventAct, &newSendFrame);
        newSendFrame.type = SEND_FRAME_TYPE_CTRL_EVENT;
      }
    break;
    
    case DALILIB_RAW_FRAME_ACTION:
      {
        rc = dali_raw_frame_action(pInst, &pAction->rawFrameAct, &newSendFrame);
        newSendFrame.type = SEND_FRAME_TYPE_RAW;
      }
    break;

    case DALILIB_GW_ACTION:
    default:
      {
        rc = R_DALILIB_NOT_SUPPORTED_ACTION;
      }
    break;
  }
  if (R_DALILIB_OK == rc)
  {
    rc = dali_cb_send(pInst, &newSendFrame);
  }

  if (R_INTERN_ACTION == rc)
  {
    rc = R_DALILIB_OK;
  }

  return (rc);
}


//----------------------------------------------------------------------
// external timer ticker resolution[100us]
// ca. ever 10ms
//----------------------------------------------------------------------
void dalilib_timingHelper(dalilib_instance_t pInstance, dalilib_time ticker)
{
  #ifdef DALILIB_DEMO
  static uint32_t lastTime = 0;
  #endif

  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;

  if (pInst->status < STACK_STATUS_IDLE || STACK_STATUS_TERMINATED == pInst->status)
  {
    // stack is not started or stopped.
    return;
  }
  pInst->currentTime = ticker;

  #ifdef DALILIB_DEMO
  if (0 == pInst->runTime)
  {
    pInst->runTime = ticker / 100;
  }
  else
  {
	  pInst->runTime += dali_difftime(ticker,lastTime);
	  lastTime = ticker;
  }

  if (pInst->runTime >= DALILIB_DEMO_TIME)
  {
    dalilib_stop(pInstance);
    return;
  }
  #endif

  // wait confirmation frame of last send frame
  if (STACK_STATUS_WAIT_CONF == pInst->status)
  {
    if (dali_difftime(pInst->currentTime, pInst->waitTimeout) >= DALI_CONFIRMATION_TIMEOUT)
    {
      dali_cb_log(pInst, R_DALILIB_FRAME_NOT_SEND,"dali library confirmation timeout");
      if (R_SEND_AGAIN != dali_process_response_timeout(pInst,
                                                        &pInst->last_tx_frame,
                                                        DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT))
      {
        dali_reset_stack_status(pInst);
      }
    }
  }
  // wait response frame of last forward frame
  if (STACK_STATUS_WAIT_RESPONSE == pInst->status)
  {
    if (dali_difftime(pInst->currentTime, pInst->waitTimeout) >= DALI_RESPONSE_TIMEOUT)
    {
      dali_cb_log(pInst, R_DALILIB_RESPONSE_TIMEOUT,"dali library response timeout");
      if (R_SEND_AGAIN != dali_process_response_timeout(pInst,
                                                        &pInst->last_tx_frame,
                                                        DALILIB_RESPONSE_VALUE_NO_ANSWER))
      {
        dali_reset_stack_status(pInst);
      }
    }
  }
  if (NULL != pInst->ctrlInst.gearInstance.fpGearTimingHelper)
  {
    (pInst->ctrlInst.gearInstance.fpGearTimingHelper)(&pInst->ctrlInst.gearInstance);
  }
  if (NULL != pInst->ctrlInst.deviceInstance.fpDeviceTimingHelper)
  {
    (pInst->ctrlInst.deviceInstance.fpDeviceTimingHelper)(&pInst->ctrlInst.deviceInstance);
  }
}

static R_DALILIB_RESULT dali_send_next_frame(PDaliLibInstance pInst)
{
  pInst->status = STACK_STATUS_IDLE;
  R_DALILIB_RESULT rc = R_NO_MORE_FRAMES;
  if (pInst->last_tx_frame.cmdCounter > 0)
  {
    send_frame_t newSendFrame;
    memcpy(&newSendFrame, &pInst->last_tx_frame, sizeof(send_frame_t));
    if (SEND_FRAME_TYPE_CTRL_GEAR == pInst->last_tx_frame.type)
    {
      if (R_DALILIB_OK == ctrl_gear_send_more(pInst, &newSendFrame))
      {
        rc = dali_cb_send(pInst, &newSendFrame);
      }
    }
    else
    {
      if (SEND_FRAME_TYPE_CTRL_DEV == pInst->last_tx_frame.type)
      {
        if (R_DALILIB_OK == ctrl_device_send_more(pInst, &newSendFrame))
        {
          rc = dali_cb_send(pInst, &newSendFrame);
        }
      }
    }
    if (R_DALILIB_OK == rc)
    {
      pInst->last_tx_frame.cmdCounter--;
    }
    if (R_IGNORE_FRAME == rc)
    {
      rc = R_DALILIB_OK;
    }
  }
  return (rc);
}

//----------------------------------------------------------------------
// This function is called,if the application has received dali-frame
//   - through application      (e.g. Renesas Application)
//   - through serial interface (e.g. UGW)
//----------------------------------------------------------------------
void dalilib_receive(dalilib_instance_t pInstance, dalilib_frame_t* pFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;
  send_frame_t     unansweredTxFrame;

  if (STACK_STATUS_TERMINATED == pInst->status)
  {
    //ignore all frames
    //demo-time is out
    return;
  }
  // stack wait on low level driver interface
  if (pInst->status <= STACK_STATUS_WAIT_INTERFACE)
  {
    if (FRAME_TYPE_STATUS == frame_get_type(pFrame))
    {
      frame_process_status_frame(pInst, pFrame);
      if (STACK_STATUS_IDLE == pInst->status)
      {
        dali_cb_ready(pInst);
        dali_cb_log(pInst, R_DALILIB_OK,"dali library is ready :)");
      }
    }
    else
    {
      // ignore frame
      dali_cb_log(pInst, R_DALILIB_INTERFACE_NOT_READY,"dali library wait on interface");
    }
    return;
  }
  //check TX-Flag
  if (pFrame->tx)
  {
    if (STACK_STATUS_WAIT_CONF == pInst->status)
    {
      if (pFrame->error)
      {
        rc = dali_process_response_timeout(pInst, &pInst->last_tx_frame, DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT);
        if (R_SEND_AGAIN != rc)
        {
          dali_reset_stack_status(pInst);
        }
        return;
      }
      if (FRAME_WITH_RESPONSE == pInst->last_tx_frame.status)
      {
        pInst->status = STACK_STATUS_WAIT_RESPONSE;
        if (pFrame->timestamp > pInst->currentTime)
        {
          pInst->waitTimeout = pFrame->timestamp;
        }
        else
        {
          pInst->waitTimeout = pInst->currentTime;
        }
        return;
      }
      // reset control device power notification flag
      if (SEND_FRAME_TYPE_CTRL_DEV == pInst->last_tx_frame.type &&
          pInst->last_tx_frame.cmds[0] == DALI_CTRL_DEVICE_POWER_NOTIFICATION_CMD &&
          0 == pInst->ctrlInst.deviceInstance.devicePowerCycleNotificationSend)
      {
        pInst->ctrlInst.deviceInstance.devicePowerCycleNotificationSend = 1;
      }
      rc = dali_send_next_frame(pInst);
      if (R_DALILIB_OK != rc)
      {
        if (R_NO_MORE_FRAMES == rc)
        {
          rc = dali_process_response_timeout(pInst, &pInst->last_tx_frame, DALILIB_RESPONSE_VALUE_REQUEST_SENT);
        }
        else
        {
          rc = dali_process_send_failed(pInst, &pInst->last_tx_frame, DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT);
        }
        if (R_SEND_AGAIN != rc)
        {
          dali_reset_stack_status(pInst);
        }
      }
    }
    return;
  }
  if (pFrame->error)
  {
    // ignore frame
    // received frame with error
    return;
  }
  switch(frame_get_type(pFrame))
  {
    case FRAME_TYPE_DALI:
      {
        // check whether to dismiss frame
        if (DALI_CTRL_GEAR_FORWARD_FRAME == pFrame->datalength ||
            DALI_CTRL_DEVICE_FORWARD_FRAME == pFrame->datalength ||
            DALI_RESPONSE_FRAME == pFrame->datalength)
        {
          // check for control gear
          if ((DALI_CTRL_DEVICE_FORWARD_FRAME == pFrame->datalength ||
               DALI_RESPONSE_FRAME == pFrame->datalength) &&
               STACK_MODE_CONTROL_GEAR == pInst->config.mode)
          {
            //ignore frame, a frame for control device
            //ignore frame, control gear can't process a response frame
            return;
          }
          // check for single master application controller
          if (DALI_RESPONSE_FRAME != pFrame->datalength &&
              STACK_MODE_CONTROL_DEVICE == pInst->config.mode &&
              CTRL_DEV_MODE_SINGLE == pInst->config.vendor.control_device_mode)
          {
            //ignore frame, this frame is for multi master application controller or
            //is for input device.
            //Single master application controller can receive only backward frames (8-Bit)
            return;
          }
          // check for input device
          if (DALI_CTRL_DEVICE_FORWARD_FRAME != pFrame->datalength &&
              STACK_MODE_CONTROL_DEVICE == pInst->config.mode &&
              CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
          {
            //ignore frame, a input device can receive only 24-bit frames
            return;
          }
        }
        else
        {
          // reset twice-flag
          pInst->last_rx_frame.bWaitTwice = 0;
          if (MIN_RAW_FRAME_LEN > pFrame->datalength ||
              FRAME_BUFFER_LEN < pFrame->datalength)
          {
            //ignore frame, frame is too short/long
            return;
          }
        }
        // check if stack expected a backward frame but received a forward frame
        if ((STACK_STATUS_WAIT_RESPONSE == pInst->status) &&
            (DALI_RESPONSE_FRAME != pFrame->datalength))
        {
          unansweredTxFrame = pInst->last_tx_frame;
        }
        else
        {
          memset(&unansweredTxFrame, 0, sizeof(unansweredTxFrame));
        }

        //process frame
        rc = frame_process_dali_frame(pInst, pFrame);

        if (R_DALILIB_OK == rc && pInst->last_tx_frame.cmdCounter > 0)
        {
          rc = dali_send_next_frame(pInst);
        }
        else
        {
          if (R_SKIP_NEXT_SEND_FRAME == rc)
          {
            rc = R_DALILIB_OK;
          }
        }
        if (0 != unansweredTxFrame.frame.datalength)
        {
          // notify application
          dali_process_response_timeout(pInst, &unansweredTxFrame, DALILIB_RESPONSE_VALUE_NO_ANSWER);
          if (STACK_STATUS_WAIT_RESPONSE == pInst->status)
          {
            dali_reset_stack_status(pInst);
          }
        }
      }
    break;
    
    case FRAME_TYPE_STATUS:
      {
        rc = frame_process_status_frame(pInst, pFrame);
      }
    break;
  
    default:
      rc = R_DALILIB_FRAME_NOT_SUPPORTED;
    break;
  }
  if (R_DALILIB_OK != rc && rc != R_NO_MORE_FRAMES)
  {
    // received frame can't process!
    dali_reset_stack_status(pInst);
  }
}

//----------------------------------------------------------------------
// initialize the stack
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_init(dalilib_instance_t pInstance, dalilib_cfg_t* pStackConfig)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;

  if (NULL == pInstance)
  {
    return (R_DALILIB_INVALID_INSTANCE);
  }
  pInst->config.mode = pStackConfig->mode;
  pInst->status = STACK_STATUS_INIT;

  memset(&pInst->last_tx_frame, 0, sizeof(pInst->last_tx_frame));
  memset(&pInst->last_rx_frame, 0, sizeof(pInst->last_rx_frame));

  memcpy(&pInst->config, pStackConfig, sizeof(pInst->config));

  // check the callback functions, if the all required callback functions exists
  if (NULL == pStackConfig->callbacks.fPAppSend     ||
      NULL == pStackConfig->callbacks.fPAppReAction ||
      NULL == pStackConfig->callbacks.fPAppLoadMem  ||
      NULL == pStackConfig->callbacks.fPAppSaveMem  )
  {
    return (R_DALILIB_CALLBACK_FAILED);
  }
  switch(pInst->config.mode)
  {
    case STACK_MODE_CONTROL_GEAR:
      {
        rc = ctrl_gear_init(pInst);
      }
    break;
    
    case STACK_MODE_CONTROL_DEVICE:
      {
        rc = ctrl_device_init(pInst);
      }
    break;
    
    case STACK_MODE_GATEWAY:
      {
        rc = R_DALILIB_OK;
      }
    break;
    
    default:
      rc = R_DALILIB_MODE_FAILURE;
    break;
  }
  if (R_DALILIB_OK == rc)
  {
    pInst->status = STACK_STATUS_STARTING;
  }

  return (rc);
}


//----------------------------------------------------------------------
// DALI-Stack will be create new instance
// result: instance pointer : SUCCESS
//         NULL             : FAILURE
//----------------------------------------------------------------------
dalilib_instance_t dalilib_createinstance(void)
{
  dali_instance_t* pAppInstance = NULL;
  uint8_t idx;
  for (idx=0; idx < MAX_STACK_INSTANCE; idx++)
  {
    if ( 0 == instances_g[idx].isUsed)
    {
      pAppInstance             = &instances_g[idx];
      memset(pAppInstance, 0, sizeof(dali_instance_t));
      pAppInstance->isUsed      = 1;
      pAppInstance->instanceID = idx + 1;
      break;
    }
  }

  return (pAppInstance);
}


//----------------------------------------------------------------------
// stack will be stop
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_stop(dalilib_instance_t pInstance)
{
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;

  if (NULL != pInstance)
  {
    memset(pInst, 0, sizeof(dali_instance_t));
    pInst->isUsed  = 0;
    pInst->status = STACK_STATUS_TERMINATED;
  }

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// stack will be start
//----------------------------------------------------------------------
R_DALILIB_RESULT dalilib_start(dalilib_instance_t pInstance)
{
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;

  if (STACK_STATUS_STARTING !=  pInst->status)
  {
    return (R_DALILIB_NOT_STARTED);
  }

  // wait on interface status frame
  pInst->status = STACK_STATUS_WAIT_INTERFACE;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// initialize structure for other membanks than 0
//----------------------------------------------------------------------

void dali_init_other_membanks(dali_ctrl_mem_bank_others_t* pOtherBanks)
{
  uint16_t nMemCnt;

  for (nMemCnt = 1 ; nMemCnt < MAXMEMBANKS ; nMemCnt++)
  {
    pOtherBanks->is_used[nMemCnt] = FALSE;
    pOtherBanks->memBank[nMemCnt] = NULL;
  }
}

//----------------------------------------------------------------------
// setup other membanks than 0
//----------------------------------------------------------------------
                           
R_DALILIB_RESULT dalilib_create_other_membank(dalilib_instance_t pInstance,uint8_t nNumber,uint8_t nSize,uint8_t *pOtherBank)
{
  PDaliLibInstance pInst = (PDaliLibInstance)pInstance;
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  
  if ( (nNumber > 0) && (nNumber <= MAXMEMBANKS) && (NULL != pOtherBank) )
  {
    pOtherBank[0] = nSize - 1; // last usable byte is stored in byte 0
  
    switch(pInst->config.mode)
    {
      case STACK_MODE_CONTROL_GEAR:
        {
          pInst->ctrlInst.gearInstance.gearMemBanksOther.is_used[nNumber] = TRUE;
          pInst->ctrlInst.gearInstance.gearMemBanksOther.memBank[nNumber] = pOtherBank;
        } 
      break;
    
      case STACK_MODE_CONTROL_DEVICE:
        {
          pInst->ctrlInst.deviceInstance.deviceMemBanksOther.is_used[nNumber] = TRUE;
          pInst->ctrlInst.deviceInstance.deviceMemBanksOther.memBank[nNumber] = pOtherBank;
        }
      break;
    
      default:
      break;
    }
  }
  else
  {
    rc = R_DALILIB_NOT_INITIALISED;
  }
  return(rc);
}
