/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"
#include <math.h>

// control gear commands

#define _CTRL_GEAR_CMDS_
#ifdef _CTRL_GEAR_CMDS_
// first array element must be DAPC - command
// eGearCmd,                        selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t gear_commands_m[] =
{
  {GEAR_CMD_DAPC,                             0,          0,  0, 0},
  {GEAR_CMD_OFF,                              1,       0x00,  0, 0},
  {GEAR_CMD_UP,                               1,       0x01,  0, 0},
  {GEAR_CMD_DOWN,                             1,       0x02,  0, 0},
  {GEAR_CMD_STEP_UP,                          1,       0x03,  0, 0},
  {GEAR_CMD_STEP_DOWN,                        1,       0x04,  0, 0},
  {GEAR_CMD_RECALL_MAX_LEVEL,                 1,       0x05,  0, 0},
  {GEAR_CMD_RECALL_MIN_LEVEL,                 1,       0x06,  0, 0},
  {GEAR_CMD_STEP_DOWN_AND_OFF,                1,       0x07,  0, 0},
  {GEAR_CMD_ON_AND_STEP_UP,                   1,       0x08,  0, 0},
  {GEAR_CMD_ENABLE_DAPC_SEQUENCE,             1,       0x09,  0, 0},
  {GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL,          1,       0x0A,  0, 0},
  {GEAR_CMD_GO_TO_SCENE,                      1,       0x10,  0, 0},
  {GEAR_CMD_RESET,                            1,       0x20,  0, 1},
  {GEAR_CMD_STORE_ACTUAL_LEVEL_IN_DTR0,       1,       0x21,  0, 1},
  {GEAR_CMD_SAVE_PERSISTENT_VARIABLES,        1,       0x22,  0, 1},
  {GEAR_CMD_SET_OPERATING_MODE,               1,       0x23,  0, 1},
  {GEAR_CMD_RESET_MEMORY_BANK,                1,       0x24,  0, 1},
  {GEAR_CMD_IDENTIFY_DEVICE,                  1,       0x25,  0, 1},
  {GEAR_CMD_SET_MAX_LEVEL,                    1,       0x2A,  0, 1},
  {GEAR_CMD_SET_MIN_LEVEL,                    1,       0x2B,  0, 1},
  {GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL,         1,       0x2C,  0, 1},
  {GEAR_CMD_SET_POWER_ON_LEVEL,               1,       0x2D,  0, 1},
  {GEAR_CMD_SET_FADE_TIME,                    1,       0x2E,  0, 1},
  {GEAR_CMD_SET_FADE_RATE,                    1,       0x2F,  0, 1},
  {GEAR_CMD_SET_EXTENDED_FADE_TIME,           1,       0x30,  0, 1},
  {GEAR_CMD_SET_SCENE,                        1,       0x40,  0, 1},
  {GEAR_CMD_REMOVE_FROM_SCENE,                1,       0x50,  0, 1},
  {GEAR_CMD_ADD_TO_GROUP,                     1,       0x60,  0, 1},
  {GEAR_CMD_REMOVE_FROM_GROUP,                1,       0x70,  0, 1},
  {GEAR_CMD_SET_SHORT_ADDRESS,                1,       0x80,  0, 1},
  {GEAR_CMD_ENABLE_WRITE_MEMORY,              1,       0x81,  0, 1},
  {GEAR_CMD_QUERY_STATUS,                     1,       0x90,  1, 0},
  {GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT,       1,       0x91,  1, 0},
  {GEAR_CMD_QUERY_LAMP_FAILURE,               1,       0x92,  1, 0},
  {GEAR_CMD_QUERY_LAMP_POWER_ON,              1,       0x93,  1, 0},
  {GEAR_CMD_QUERY_LIMIT_ERROR,                1,       0x94,  1, 0},
  {GEAR_CMD_QUERY_RESET_STATE,                1,       0x95,  1, 0},
  {GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS,      1,       0x96,  1, 0},
  {GEAR_CMD_QUERY_VERSION_NUMBER,             1,       0x97,  1, 0},
  {GEAR_CMD_QUERY_CONTENT_DTR0,               1,       0x98,  1, 0},
  {GEAR_CMD_QUERY_DEVICE_TYPE,                1,       0x99,  1, 0},
  {GEAR_CMD_QUERY_PHYSICAL_MINIMUM,           1,       0x9A,  1, 0},
  {GEAR_CMD_QUERY_POWER_FAILURE,              1,       0x9B,  1, 0},
  {GEAR_CMD_QUERY_CONTENT_DTR1,               1,       0x9C,  1, 0},
  {GEAR_CMD_QUERY_CONTENT_DTR2,               1,       0x9D,  1, 0},
  {GEAR_CMD_QUERY_OPERATING_MODE,             1,       0x9E,  1, 0},
  {GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE,          1,       0x9F,  1, 0},
  {GEAR_CMD_QUERY_ACTUAL_LEVEL,               1,       0xA0,  1, 0},
  {GEAR_CMD_QUERY_MAX_LEVEL,                  1,       0xA1,  1, 0},
  {GEAR_CMD_QUERY_MIN_LEVEL,                  1,       0xA2,  1, 0},
  {GEAR_CMD_QUERY_POWER_ON_LEVEL,             1,       0xA3,  1, 0},
  {GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL,       1,       0xA4,  1, 0},
  {GEAR_CMD_QUERY_FADE_TIME_RATE,             1,       0xA5,  1, 0},
  {GEAR_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE, 1,       0xA6,  1, 0},
  {GEAR_CMD_QUERY_NEXT_DEVICE_TYPE,           1,       0xA7,  1, 0},
  {GEAR_CMD_QUERY_EXTENDED_FADE_TIME,         1,       0xA8,  1, 0},
  {GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE,       1,       0xAA,  1, 0},
  {GEAR_CMD_QUERY_SCENE_LEVEL,                1,       0xB0,  1, 0},
  {GEAR_CMD_QUERY_GROUPS_0_7,                 1,       0xC0,  1, 0},
  {GEAR_CMD_QUERY_GROUPS_8_15,                1,       0xC1,  1, 0},
  {GEAR_CMD_QUERY_RANDOM_ADDRESS_H,           1,       0xC2,  1, 0},
  {GEAR_CMD_QUERY_RANDOM_ADDRESS_M,           1,       0xC3,  1, 0},
  {GEAR_CMD_QUERY_RANDOM_ADDRESS_L,           1,       0xC4,  1, 0},
  {GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0,   1,       0xC5,  1, 0},
  {GEAR_CMD_READ_MEMORY_CELL,                 1,       0xC5,  1, 0},
  {GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER,    1,       0xFF,  1, 0},
  {GEAR_CMD_TERMINATE,                        2,       0xA1,  0, 0},
  {GEAR_CMD_DTR0,                             2,       0xA3,  0, 0},
  {GEAR_CMD_INITIALISE,                       2,       0xA5,  0, 1},
  {GEAR_CMD_RANDOMISE,                        2,       0xA7,  0, 1},
  {GEAR_CMD_COMPARE,                          2,       0xA9,  1, 0},
  {GEAR_CMD_WITHDRAW,                         2,       0xAB,  0, 0},
  {GEAR_CMD_PING,                             2,       0xAD,  0, 0},
  {GEAR_CMD_SEARCHADDRH,                      2,       0xB1,  0, 0},
  {GEAR_CMD_SEARCHADDRM,                      2,       0xB3,  0, 0},
  {GEAR_CMD_SEARCHADDRL,                      2,       0xB5,  0, 0},
  {GEAR_CMD_PROGRAM_SHORT_ADDRESS,            2,       0xB7,  0, 0},
  {GEAR_CMD_VERIFY_SHORT_ADDRESS,             2,       0xB9,  1, 0},
  {GEAR_CMD_QUERY_SHORT_ADDRESS,              2,       0xBB,  1, 0},
  {GEAR_CMD_ENABLE_DEVICE_TYPE,               2,       0xC1,  0, 0},
  {GEAR_CMD_DTR1,                             2,       0xC3,  0, 0},
  {GEAR_CMD_DTR2,                             2,       0xC5,  0, 0},
  {GEAR_CMD_WRITE_MEMORY_LOCATION,            2,       0xC7,  1, 0},
  {GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY,   2,       0xC9,  0, 0},
  {GEAR_CMD_LAST,                             0,       0x00,  0, 0}
};
#endif //_CTRL_GEAR_CMDS_

/**********************************************************************
 * internal helper functions
 **********************************************************************/
//----------------------------------------------------------------------
// generate random address
//----------------------------------------------------------------------
static uint32_t gear_create_random_address(PCtrlGearInstance pCtrlInst)
{
  uint32_t randomAddress = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[0] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[1] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[2] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[3] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[4] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.gtin[5] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[0] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[1] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[2] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[3] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[4] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[5] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[6] +
    pCtrlInst->gearMemBank0.ctrlMemBank0.identify_number[7] +
    ((PDaliLibInstance)pCtrlInst->pInst)->instanceID;
  srand(randomAddress);
  return (rand() % DALI_CTRL_GEAR_RANDOM_RANGE_MAX);
}


//----------------------------------------------------------------------
// add device type to sorted array of present device types
//----------------------------------------------------------------------
#ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
static R_DALILIB_RESULT gear_add_device_type(PCtrlGearInstance pCtrlInst,
                                             uint8_t*          pDeviceTypes,
                                             size_t            size,
                                             uint8_t           deviceType)
{
  uint8_t idx = 0;

  (void)pCtrlInst;

  // find first free slot
  while (GEAR_TYPE_NO_FURTHER_DEVICE_TYPE != pDeviceTypes[idx])
  {
    idx++;
    if (size <= idx)
    {
      return (R_INVALID_MEM_DATA_SIZE);
    }
  }

  // shift device types greater than the new device type
  while ((idx > 0) &&
         (pDeviceTypes[idx - 1] > deviceType))
  {
    pDeviceTypes[idx] = pDeviceTypes[idx - 1];
    idx--;
  }

  // insert new device type (insertion sort)
  pDeviceTypes[idx] = deviceType;

  return (R_DALILIB_OK);
}
#endif /* DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT */


//----------------------------------------------------------------------
// calculate the next actual level
//----------------------------------------------------------------------
static double gear_calc_actual_level(PCtrlGearInstance pCtrlInst)
{
  uint32_t newLevel = pCtrlInst->gearDimm.startDimmLevel * 1000;
  uint32_t diffTime = dali_difftime(((PDaliLibInstance)pCtrlInst->pInst)->currentTime, pCtrlInst->gearDimm.startFadeTimestamp);
  uint32_t stepValue = (uint32_t)((diffTime*1000.0) / pCtrlInst->gearDimm.stepTime)*1000;

  if (DIMM_DIRECTION_UP ==  pCtrlInst->gearDimm.dimmDirection)
  {
    newLevel += stepValue;

    if (newLevel >= (pCtrlInst->gearPersistent.gearPersVariables.maxLevel * 1000))
    {
        return (pCtrlInst->gearPersistent.gearPersVariables.maxLevel);
    }
  }
  else
  {
    newLevel -= stepValue;

    if ( (newLevel <= (pCtrlInst->gearPersistent.gearPersVariables.minLevel * 1000)) || newLevel > (254 * 1000))
    {
        return (pCtrlInst->gearPersistent.gearPersVariables.minLevel);
    }
  }

  return (newLevel / 1000.0);
}

//----------------------------------------------------------------------
// get dimming curve
//----------------------------------------------------------------------
static DIMMING_CURVE getDimmingCurve(PCtrlGearInstance pCtrlInst)
{
  if (GEAR_TYPE_LED_MODULES == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    return (pCtrlInst->gearPersistent.ledPersVariables.dimmingCurve);
  }

  return (DIMMING_CURVE_STANDARD);
}

//----------------------------------------------------------------------
// calculate the steptime [us]
//----------------------------------------------------------------------
static uint32_t gear_calc_dimmtime(uint8_t  fadeTime,
                                   uint8_t  useFft,
                                   uint32_t fft,
                                   uint8_t  extFadeTimeBase,
                                   uint8_t  extFadeTimeMultiple,
                                   uint8_t  actualLevel,
                                   uint8_t  targetLevel)
{
  double ret = 0.00;
  double factor = 0.5000;
  uint8_t diffLevel = abs(targetLevel - actualLevel);

  if (0 != fadeTime)
  {
    ret = pow(2, fadeTime);
    ret = sqrt(ret);
    ret = (factor) * (ret) * 1; // second
    ret *= 1000; // ms
  }
  else if (useFft)
  {
    ret = fft;
  }
  else // extended fadetime
  {
    switch(extFadeTimeMultiple)
    {
      case 1: // 100 ms
        ret = 100 * (extFadeTimeBase + 1);
      break;
      case 2: // 1s
        ret = 1000 * (extFadeTimeBase + 1);
      break;
      case 3: // 10 s
        ret = 10 * 1000 * (extFadeTimeBase + 1);
      break;
      case 4: // 1min
        ret = 60 * 1000 * (extFadeTimeBase + 1);
      break;
      default:
        ret = 0;
      break;
    }
  }

  ret = ((ret*1000)/diffLevel);

  return (uint32_t)(ceil(ret));
}


//----------------------------------------------------------------------
// calculate the steptime [us] and target level
//----------------------------------------------------------------------
static uint32_t gear_calc_dimmrate(PCtrlGearInstance pCtrlInst, uint8_t dimmDirection)
{
  double ret = 0;
  double factor = 506.00;
  uint8_t diffLevel = 0;
  int16_t tempTargetLevel;

  ret = pow(2,pCtrlInst->gearPersistent.gearPersVariables.fadeRate);
  ret = sqrt(ret);
  ret = round(factor / ret); // step/second

  diffLevel = (uint32_t)ret/5; // steps/200 ms

  if (pCtrlInst->gearVariables.fadeRunning)
  {
    // reset fadeTimestamp => restart 200 ms counter
    pCtrlInst->gearDimm.fadeTimestamp = 0;
  }

  if (DIMM_DIRECTION_UP == dimmDirection)
  {
    tempTargetLevel = pCtrlInst->gearVariables.actualLevel + diffLevel; // target in 200ms
    if (pCtrlInst->gearPersistent.gearPersVariables.maxLevel < tempTargetLevel)
    {
      tempTargetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
      pCtrlInst->gearVariables.limitError = TRUE;
    }
  }
  else
  {
    tempTargetLevel = pCtrlInst->gearVariables.actualLevel - diffLevel; // target in 200ms
    if (pCtrlInst->gearPersistent.gearPersVariables.minLevel > tempTargetLevel)
    {
      tempTargetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
      pCtrlInst->gearVariables.limitError = TRUE;
    }
  }

  pCtrlInst->gearVariables.targetLevel = (uint8_t)tempTargetLevel;

  if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
  {
    pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
    pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
    if (0 == pCtrlInst->gearPersistentChangedTime)
    {
      pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    }
  }


  if (0 != pCtrlInst->gearVariables.targetLevel)
  {
      pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
  }

  return (uint32_t)(round((180 * 1000)/diffLevel));
}


//----------------------------------------------------------------------
// convert uint8_t to percent*1000
//----------------------------------------------------------------------
static uint32_t gear_level2percent(DIMMING_CURVE dimmingCurve, double level)
{
  uint32_t ret = 0;

  if (0 == (uint8_t)level)
  {
    return 0;
  }
  if (DIMMING_CURVE_STANDARD == dimmingCurve)
  {
    // simplyfied calculation of percentage value of log-dimming-curve
    //1000 * 10^( (level-1)/(253/3) -1)
    ret = (uint32_t)round(pow(1.02767953,round(level))*97.3066);
  }
  else
  {
    return gear_led_level2percent(level);
  }

  return ret;
}


//----------------------------------------------------------------------
// convert percent*1000 to uint8_t
//----------------------------------------------------------------------
static uint8_t gear_percent2level(DIMMING_CURVE dimmingCurve, uint32_t percentvalue)
{
  uint8_t ret = 0;

  if (DIMMING_CURVE_STANDARD == dimmingCurve)
  {
    if (0 == percentvalue)
    {
      ret = 0;
    }
    else
    {
      // reversed simplyfied calculation of gear_level2percent()
      ret = (uint8_t)round(36.6*(log(percentvalue)-4.58));
    }
  }
  else
  {
    return gear_led_percent2level(percentvalue);
  }

  return ret;
}


//----------------------------------------------------------------------
// execute control gear reaction
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_reaction(PCtrlGearInstance                pCtrlInst,
                                      DALILIB_CTRL_GEAR_REACTION_CODES reactionCode,
                                      uint32_t                         value)
{
  dalilib_action_t action;

  memset(&action, 0, sizeof(action));

  action.actionType = DALILIB_CTRL_GEAR_REACTION;
  action.gearReact.reactionCode = reactionCode;
  action.gearReact.reactValue = value;
  action.gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;

  if (DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL         == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_ACTUAL_LEVEL         == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_POWER_ON_LEVEL       == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SYSTEM_FAILURE_LEVEL == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_LIGHT_LEVEL     == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_TARGET_LEVEL         == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_ACTIVE_LEVEL    == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MIN_LEVEL            == reactionCode ||
      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MAX_LEVEL            == reactionCode)
  {
    action.gearReact.reactRawValue = gear_percent2level(getDimmingCurve(pCtrlInst), value);
  }

  return (dali_cb_reaction(pCtrlInst->pInst, &action));
}


//----------------------------------------------------------------------
// stop dimming process
//----------------------------------------------------------------------
static void gear_stop_dimming(PCtrlGearInstance pCtrlInst)
{
  if (pCtrlInst->gearDimm.afterDimmOff)
  {
    pCtrlInst->gearVariables.targetLevel = 0;
    gear_reaction(pCtrlInst, DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL, 0x00);
  }
  pCtrlInst->gearDimm.afterDimmOff = 0;
  pCtrlInst->gearVariables.fadeRunning = FALSE;
  pCtrlInst->gearDimm.startFadeTimestamp = 0;
  pCtrlInst->gearDimm.startDimmLevel = 255;
  pCtrlInst->gearDimm.fadeTimestamp = 0;
  pCtrlInst->gearDimm.stepTime = 0;
  pCtrlInst->gearDimm.dimmDirection = 0;
  pCtrlInst->gearDimm.dimmType = 0;
}

static uint32_t gear_get_future_fade_time(PCtrlGearInstance pCtrlInst, uint8_t* pUseFutureFadeTime)
{
  uint32_t rc = 0;

  *pUseFutureFadeTime = 0x00;
  if (0x00 != pCtrlInst->gearPersistent.gearPersVariables.fadeTime)
  {
    return (0x00);
  }
  switch(pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    case GEAR_TYPE_LED_MODULES:
      rc = gear_led_get_fft(pCtrlInst);
      if (0x00 != rc)
      {
        *pUseFutureFadeTime = 0x01;
      }
      return (rc);
    default: 
      return (0);
  }
}


//----------------------------------------------------------------------
// start dimming process
//----------------------------------------------------------------------
static void gear_start_dimming(PCtrlGearInstance pCtrlInst,
                               uint8_t           dimmType,
                               uint8_t           dimmDirection,
                               uint8_t           useFft,
                               uint32_t          fft)
{
  pCtrlInst->gearDimm.stepTime = 0;
  pCtrlInst->gearDimm.startFadeTimestamp = 0;
  pCtrlInst->gearDimm.fadeTimestamp = 0;

  pCtrlInst->gearDimm.dimmDirection = dimmDirection;
  pCtrlInst->gearDimm.dimmType = dimmType;

  // skip Light Level 0 to MinLevel
  if (pCtrlInst->gearPersistent.gearPersVariables.minLevel > pCtrlInst->gearVariables.actualLevel)
  {
    pCtrlInst->gearVariables.actualLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
    gear_reaction(pCtrlInst,
                  DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                  gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.actualLevel));
  }

  if (DIMM_TYPE_DIMMRATE == dimmType)
  {
    pCtrlInst->gearDimm.stepTime = gear_calc_dimmrate(pCtrlInst, dimmDirection);
  }
  else
  {
    pCtrlInst->gearDimm.stepTime = gear_calc_dimmtime(pCtrlInst->gearPersistent.gearPersVariables.fadeTime,
                                                      useFft,
                                                      fft,
                                                      pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase,
                                                      pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple,
                                                      pCtrlInst->gearVariables.actualLevel,
                                                      pCtrlInst->gearVariables.targetLevel);
  }

  pCtrlInst->gearVariables.fadeRunning = TRUE;
}

//----------------------------------------------------------------------
// check the received frame address, is for me?
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_check_address(PCtrlGearInstance pCtrlInst, gear_adr_t* pReceivedAddress)
{
  if (DALI_ADRTYPE_SPEC ==  pReceivedAddress->adrType)
  {
    return (R_DALILIB_OK);
  }

  if (DALI_ADRTYPE_BROADCAST == pReceivedAddress->adrType)
  {
    return (R_DALILIB_OK);
  }

  if (DALI_ADRTYPE_NBROADCAST == pReceivedAddress->adrType)
  {
    if (DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
    {
      return (R_DALILIB_OK);
    }
  }
  if (DALI_ADRTYPE_GRP == pReceivedAddress->adrType)
  {
    uint8_t grpBit = pCtrlInst->gearPersistent.gearPersVariables.gearGroups >> pReceivedAddress->adr;
    if (grpBit & 0x01)
    {
      return (R_DALILIB_OK);
    }
  }

  if (DALI_ADRTYPE_SHORT == pReceivedAddress->adrType)
  {
    if (pReceivedAddress->adr == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
    {
      return (R_DALILIB_OK);
    }
  }

  return (R_FRAME_IS_NOT_FOR_ME);
}


//----------------------------------------------------------------------
// search control gear command in gear_commands_m array
// cmdType: 0: last array element/DAPC
//      1: normal commands
//      2: special commands
//----------------------------------------------------------------------
static ctrl_gear_commands_t* gear_get_cmd_struct_by_byte(uint8_t cmdType, uint8_t opcodeByte)
{
  uint16_t idx;

  // if opcodeByte == scenes and groups commands
  if (1 == cmdType)
  {
    // add group
    if (opcodeByte >= DALI_CTRL_GEAR_ADD_FROM_GRP_START &&
        opcodeByte <= DALI_CTRL_GEAR_ADD_FROM_GRP_END)
    {
      opcodeByte = DALI_CTRL_GEAR_ADD_FROM_GRP_START;
    } 
    else 
    {
      // remove group
      if (opcodeByte >= DALI_CTRL_GEAR_REMOVE_FROM_GRP_START &&
          opcodeByte <= DALI_CTRL_GEAR_REMOVE_FROM_GRP_END)
      {
        opcodeByte = DALI_CTRL_GEAR_REMOVE_FROM_GRP_START;
      }
      else
      {
        // goto scene
        if (opcodeByte >= DALI_CTRL_GEAR_GOTO_SCENE_START &&
            opcodeByte <= DALI_CTRL_GEAR_GOTO_SCENE_END)
        {
          opcodeByte = DALI_CTRL_GEAR_GOTO_SCENE_START;
        }
        else
        {
          // remove scene
          if (opcodeByte >= DALI_CTRL_GEAR_REMOVE_SCENE_START &&
              opcodeByte <= DALI_CTRL_GEAR_REMOVE_SCENE_END)
          {
            opcodeByte = DALI_CTRL_GEAR_REMOVE_SCENE_START;
          }
          else
          {
            // set scene
            if (opcodeByte >= DALI_CTRL_GEAR_SET_SCENE_START &&
                opcodeByte <= DALI_CTRL_GEAR_SET_SCENE_END)
            {
              opcodeByte = DALI_CTRL_GEAR_SET_SCENE_START;
            }
            else
            {
              // query scene
              if (opcodeByte >= DALI_CTRL_GEAR_QUERY_SCENE_START &&
                  opcodeByte <= DALI_CTRL_GEAR_QUERY_SCENE_END)
              {
                opcodeByte = DALI_CTRL_GEAR_QUERY_SCENE_START;
              }
            }
          }
        }
      }
    }
  }


  for (idx = 0; idx < COUNTOF(gear_commands_m); idx++)
  {
    if (gear_commands_m[idx].selectorBit == cmdType &&
        gear_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&gear_commands_m[idx]);
    }
  }

  return (NULL);
}


//----------------------------------------------------------------------
// search control gear command in gear_commands_m array
//----------------------------------------------------------------------
static ctrl_gear_commands_t* gear_get_cmd_struct_by_cmd(GEAR_CMD gear_cmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(gear_commands_m); idx++)
  {  
    if (gear_commands_m[idx].eGearCmd == gear_cmd)
    {
      return (&gear_commands_m[idx]);
    }
  }

  return (NULL);
}


//----------------------------------------------------------------------
// parse address and address-type of the received forward-frame
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_parse_address(dalilib_frame_t* pForwardFrame, gear_adr_t *pGearAdr)
{
  uint8_t rawAddress = (pForwardFrame->data>>8);

  if (rawAddress >= DALI_GEAR_SPECIAL_COMMAND_START &&
      rawAddress <= DALI_GEAR_SPECIAL_COMMAND_END)
  {
    pGearAdr->adr = rawAddress;
    pGearAdr->adrType = DALI_ADRTYPE_SPEC;
    return (R_DALILIB_OK);
  }

  if (rawAddress >= DALI_GEAR_RESERVED_COMMAND_START &&
      rawAddress <= DALI_GEAR_RESERVED_COMMAND_END)
  {
    pGearAdr->adr = rawAddress;
    pGearAdr->adrType = DALI_ADRTYPE_RESERVED;
    return (R_DALILIB_CTRL_GEAR_INVALID_FF);
  }

  rawAddress >>= 1; // remove selection bit

  // check for broadcast
  if (rawAddress == DALI_CTRL_GEAR_BROADCAST_ADR)
  {
    pGearAdr->adrType = DALI_ADRTYPE_BROADCAST;
    return (R_DALILIB_OK);
  }
  // check for Nbroadcast
  if (rawAddress == DALI_CTRL_GEAR_NBROADCAST_ADR)
  {
    pGearAdr->adrType = DALI_ADRTYPE_NBROADCAST;
    return (R_DALILIB_OK);
  }

  // check for group address
  if (rawAddress & (uint8_t)DALI_CTRL_GEAR_GROUPADR_MASK_1)
  {
    uint8_t tmpadr = rawAddress;
    tmpadr >>= 4; // 00000100

    if (tmpadr == (uint8_t)0x04)
    {
      pGearAdr->adrType = DALI_ADRTYPE_GRP;
      pGearAdr->adr = (rawAddress & (uint8_t)DALI_CTRL_GEAR_GROUPADR_MASK_2);
      return (R_DALILIB_OK);
    }
  }

  // check for short address
  if ((rawAddress & (uint8_t)DALI_CTRL_GEAR_SHORTADR_MASK) || rawAddress == 0x00)
  {
    pGearAdr->adr = (rawAddress & (uint8_t)DALI_CTRL_GEAR_SHORTADR_MASK);
    pGearAdr->adrType = DALI_ADRTYPE_SHORT;
    return (R_DALILIB_OK);
  }

  return (R_DALILIB_CTRL_GEAR_INVALID_ADDRESS);
}


//----------------------------------------------------------------------
// parse dali command of the received forward-frame
//----------------------------------------------------------------------
static ctrl_gear_commands_t* gear_parse_command(dalilib_frame_t* pForwardFrame, gear_adr_t* pGearAdr)
{
  // check for DAPC ( selection bit = 0 )
  if (!((pForwardFrame->data>>8) &  (uint8_t)DALI_CTRL_GEAR_SELECT_BIT_MASK))
  {
    return (&gear_commands_m[0]);
  }

  uint8_t opcodeByte = pForwardFrame->data;
  uint8_t cmdType = 1; // normal command

  if (DALI_ADRTYPE_SPEC == pGearAdr->adrType)
  {
    opcodeByte = pGearAdr->adr;
    cmdType = 2; // special command
  }

  ctrl_gear_commands_t* pGearCmd = gear_get_cmd_struct_by_byte(cmdType, opcodeByte);

  return (pGearCmd);
}

//----------------------------------------------------------------------
// check the writeEnableState variable
//----------------------------------------------------------------------
static CTRL_GEAR_WRITE_ENABLE_STATE gear_check_writeEnableState(PCtrlGearVariables pVariables,
                                                                GEAR_CMD           eGearCmd)
{
  (void)pVariables;

  switch(eGearCmd)
  {
    case GEAR_CMD_WRITE_MEMORY_LOCATION:
    case GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
    case GEAR_CMD_DTR0:
    case GEAR_CMD_DTR1:
    case GEAR_CMD_DTR2:
    case GEAR_CMD_QUERY_CONTENT_DTR0:
    case GEAR_CMD_QUERY_CONTENT_DTR1:
    case GEAR_CMD_QUERY_CONTENT_DTR2:
        return (CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED);
    default:
    break;
  }

  return (CTRL_GEAR_WRITE_ENABLE_STATE_DISABLED);
}


//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
static uint8_t gear_check_nvm_variables(dali_ctrl_gear_pers_mem_t*   pPersistent,
                                        PCtrlGearVariables           pGearVariables)
{
  uint8_t rc = FALSE;
  uint8_t sceneCntr = 0;
  PCtrlGearPersistentVariables pPeristensVariables = &pPersistent->gearPersVariables;

  (void)pGearVariables;

  if ((0xFE == pPeristensVariables->powerOnLevel) &&
      (0xFE == pPeristensVariables->systemFailureLevel) &&
      (pPeristensVariables->minLevel == (uint8_t)pPeristensVariables->phm) &&
      (0xFE == pPeristensVariables->maxLevel) &&
      (0x07 ==  pPeristensVariables->fadeRate) &&
      (0x00 == pPeristensVariables->fadeTime) &&
      (0x00 ==  pPeristensVariables->extFadeTimeBase) &&
      (0x00 ==  pPeristensVariables->extFadeTimeMultiple) &&
      (0x00FFFFFF == pPeristensVariables->randomAddress) &&
      (0x0000 == pPeristensVariables->gearGroups))
  {
    rc = TRUE;
    for (sceneCntr = 0; sceneCntr < DALI_CTRL_GEAR_MAX_SCENES; sceneCntr++)
    {
      if (DALI_MASK != pPeristensVariables->scene[sceneCntr])
      {
        rc = (FALSE);
      }
    }
    if (TRUE == rc)
    {
      switch(pPeristensVariables->gearDeviceType)
      {
        case GEAR_TYPE_LED_MODULES:
          rc = (gear_led_check_nvm_variables(pPersistent));
        break;
        case GEAR_TYPE_SWITCHING_FUNCTION:
          rc = (gear_switching_check_nvm_variables(pPersistent));
        break;
        default:
        break;
      }
    }
  }

  return (rc);
}


//----------------------------------------------------------------------
// reset memory bank - X
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                               uint8_t           nMembank)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  (void)pCtrlInst;
  (void)nMembank;

  // memory bank 0 will be not reset.

  // reset control gear extensions memory banks, if present
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    rc = gear_power_supply_reset_memory_bank(pCtrlInst, nMembank);
  #endif
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    rc = gear_mem_bank1_ext_reset_memory_bank(pCtrlInst, nMembank);
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    rc = gear_energy_reporting_reset_memory_bank(pCtrlInst, nMembank);
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    rc = gear_diag_maintenance_reset_memory_bank(pCtrlInst, nMembank);
  #endif

  // TODO: reset other memory banks

  return (rc);
}


//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_init_by_reset(PCtrlGearVariables           pVariables,
                                           dali_ctrl_gear_pers_mem_t*   pPersistent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t sceneCntr = 0;
  PCtrlGearPersistentVariables pPersistentVariables = &pPersistent->gearPersVariables;

  // initialize the RAM - Parameters
  pVariables->actualLevel      = 0xFE;
  pVariables->targetLevel      = 0xFE;
  pVariables->lastActiveLevel  = 0xFE;
  pVariables->searchAddress    = 0x00FFFFFF;
  pVariables->writeEnableState = CTRL_GEAR_WRITE_ENABLE_STATE_DISABLED;
  pVariables->limitError       = FALSE;
  pVariables->fadeRunning      = FALSE;
  pVariables->resetState       = TRUE;
  pVariables->powerCycleSeen   = FALSE;

  // initialize the persistent (NVM) Parameters
  pPersistentVariables->lastLightLevel      = 0xFE;
  pPersistentVariables->powerOnLevel        = 0xFE;
  pPersistentVariables->systemFailureLevel  = 0xFE;
  pPersistentVariables->minLevel            = (uint32_t)pPersistentVariables->phm;
  pPersistentVariables->maxLevel            = 0xFE;
  pPersistentVariables->fadeRate            = 0x07;
  pPersistentVariables->fadeTime            = 0x00;
  pPersistentVariables->extFadeTimeBase     = 0x00;
  pPersistentVariables->extFadeTimeMultiple = 0x00;
  pPersistentVariables->randomAddress       = 0x00FFFFFF;
  pPersistentVariables->gearGroups          = 0x0000;

  for (sceneCntr = 0; sceneCntr < DALI_CTRL_GEAR_MAX_SCENES; sceneCntr++)
  {
    pPersistentVariables->scene[sceneCntr] = DALI_MASK;
  }

  switch(pPersistentVariables->gearDeviceType)
  {
    case GEAR_TYPE_LED_MODULES:
      gear_led_by_reset(pPersistent);
    break;
    case GEAR_TYPE_SWITCHING_FUNCTION:
      gear_switching_by_reset(pPersistent);
    break;
    case GEAR_TYPE_COLOUR_CONTROL:
      gear_colour_by_reset(pPersistent, pVariables);
    break;
    default:
    break;
  }

  return (rc);
}


//----------------------------------------------------------------------
// initialize the variable after external power cycle
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_init_after_power_cycle(PCtrlGearInstance pCtrlInst)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PCtrlGearVariables pVariables = NULL;
  PCtrlGearPersistentVariables pPersistentVariables = NULL;

  pVariables = &pCtrlInst->gearVariables;
  pPersistentVariables = &pCtrlInst->gearPersistent.gearPersVariables;

  // initialize the RAM - Parameters
  pVariables->actualLevel         = 0x00;
  pVariables->targetLevel         = 0x00;
  pVariables->lastActiveLevel     = pPersistentVariables->maxLevel;
  pVariables->searchAddress       = 0x00FFFFFF;
  pVariables->initialisationState = CTRL_GEAR_INITIALISE_STATE_DISABLED;
  pVariables->writeEnableState    = CTRL_GEAR_WRITE_ENABLE_STATE_DISABLED;
  pVariables->controlGearFailure  = FALSE;
  pVariables->lampFailure         = FALSE;
  pVariables->lampOn              = FALSE;
  pVariables->limitError          = FALSE;
  pVariables->fadeRunning         = FALSE;
  pVariables->resetState          = TRUE;
  pVariables->powerCycleSeen      = TRUE;
  pVariables->DTR0                = 0x00;
  pVariables->DTR1                = 0x00;
  pVariables->DTR2                = 0x00;

  if (DALI_MASK == pPersistentVariables->powerOnLevel)
  {
    pVariables->targetLevel       = pPersistentVariables->lastLightLevel;
  }
  else
  {
    pVariables->targetLevel       = pPersistentVariables->powerOnLevel;
  }

  switch(pPersistentVariables->gearDeviceType)
  {
    case GEAR_TYPE_LED_MODULES:
      gear_led_init_after_power_cycle(&pCtrlInst->gearPersistent, pVariables);
    break;
    case GEAR_TYPE_SWITCHING_FUNCTION:
      gear_switching_init_after_power_cycle(&pCtrlInst->gearPersistent, pVariables);
    break;
    case GEAR_TYPE_COLOUR_CONTROL:
      gear_colour_init_after_power_cycle(&pCtrlInst->gearPersistent, pVariables);
    break;
    default:
    break;
  }

  // initialize control gear extensions
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    gear_power_supply_init_after_power_cycle(pCtrlInst);
  #endif
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    gear_mem_bank1_ext_init_after_power_cycle(pCtrlInst);
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    gear_energy_reporting_init_after_power_cycle(pCtrlInst);
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    gear_diag_maintenance_init_after_power_cycle(pCtrlInst);
  #endif

  return (rc);
}


/**********************************************************************
 * Backward Frame Functions
 **********************************************************************/

//----------------------------------------------------------------------
// process the response status frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_status(uint8_t           value,
                                                     dalilib_action_t* pAct)
{
  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_QUERY_STATUS;

  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.controlGearFailure = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.lampFailure = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.lampOn = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.limitError = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.fadeRunning = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.resetState = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.shortAddress = TRUE;
  }
  value >>= 1;
  if (value & 0x01)
  {
    pAct->gearReact.gearStatus.powerCycleSeen = TRUE;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response levels frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_levels(GEAR_CMD          eGearCmd,
                                                     uint8_t           value,
                                                     dalilib_action_t* pAct)
{
  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_QUERIES;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = gear_level2percent(DIMMING_CURVE_STANDARD, value);

  if (GEAR_CMD_QUERY_SCENE_LEVEL == eGearCmd)
  {
    if (DALI_MASK == value)
    {
      pAct->gearReact.valueType = DALILIB_RESPONSE_VALUE_SCENE_NOT_USED;
    }
  }

  return(R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response init frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_initialise(GEAR_CMD          eGearCmd,
                                                         uint8_t           value,
                                                         dalilib_action_t* pAct)
{
  (void)eGearCmd;

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_INITIALISE;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = value;

  return(R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response config frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_config(GEAR_CMD          eGearCmd,
                                                     uint8_t           value,
                                                     dalilib_action_t* pAct)
{
  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_QUERIES;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;

  switch(eGearCmd)
  {
    case GEAR_CMD_QUERY_PHYSICAL_MINIMUM:
      pAct->gearReact.reactValue = value;
    break;
    case GEAR_CMD_QUERY_GROUPS_8_15:
      pAct->gearReact.reactValue = value;
      pAct->gearReact.reactValue <<= 8;
    break;
    case GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT:
      pAct->gearReact.reactValue = (DALI_MASK == value ? TRUE : FALSE);
    break;
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_H:
      pAct->gearReact.reactValue = value;
      pAct->gearReact.reactValue <<= 16;
    break;
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_M:
      pAct->gearReact.reactValue = value;
      pAct->gearReact.reactValue <<= 8;
    break;
    case GEAR_CMD_QUERY_GROUPS_0_7:
    case GEAR_CMD_QUERY_VERSION_NUMBER:
    case GEAR_CMD_QUERY_DEVICE_TYPE:
    case GEAR_CMD_QUERY_NEXT_DEVICE_TYPE:
    case GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_OPERATING_MODE:
    case GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE:
    case GEAR_CMD_QUERY_FADE_TIME_RATE:
    case GEAR_CMD_QUERY_EXTENDED_FADE_TIME:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_L:
    case GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER:
    default:
      pAct->gearReact.reactValue = value;
    break;
  }

  return(R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response vendor frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_vendor(GEAR_CMD          eGearCmd,
                                                     uint8_t           value,
                                                     dalilib_action_t* pAct,
                                                     send_frame_t*     pLastTxFrame)
{
  static uint8_t cellCntr = 0;
  static uint8_t cellInfo[8];

  (void)eGearCmd;

  if (0 == cellCntr)
  {
    memset(&cellInfo, 0, sizeof(cellInfo));
  }
  cellInfo[cellCntr++] = value;

  if (pLastTxFrame->cmdCounter <= 0 &&
      pLastTxFrame->lastActionType == DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR)
  {
    cellCntr = 0;
    pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_VENDOR;
    pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
    switch(pLastTxFrame->lastAction)
    {
      case DALILIB_ACT_CTRL_GEAR_VENDOR_GTIN:
          memcpy(&pAct->gearReact.gtin, &cellInfo, sizeof(pAct->gearReact.gtin));
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MAJOR:
          pAct->gearReact.firmware_ver_major = value;
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MINOR:
          pAct->gearReact.firmware_ver_minor = value;
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_IDENTIFY_NR:
          memcpy(&pAct->gearReact.identify_number, &cellInfo, sizeof(pAct->gearReact.identify_number));
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MAJOR:
          pAct->gearReact.hardware_ver_major = value;
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MINOR:
          pAct->gearReact.hardware_ver_minor = value;
      break;
      
      case DALILIB_ACT_CTRL_GEAR_VENDOR_DALI_NORM_VERSION:
          pAct->gearReact.dali_norm_version = value;
      break;
      
      default:
      break;
    }
  }

  return(R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response memory frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_memory(uint8_t           value,
                                                     dalilib_action_t* pAct,
                                                     send_frame_t*     pLastTxFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_MEMORY;

  if (0 >= pLastTxFrame->cmdCounter)
  {
    pAct->gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
    pAct->gearReact.reactValue = value;
  }
  else
  {
    if (DALILIB_ACT_CTRL_GEAR_WRITE_MEMORY_CELL == pLastTxFrame->lastAction)
    {
      if (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == value)
      {
        // answer confirms unlocking selected memory bank
        rc = R_SUPPRESS_REACTION;
      }
      else
      {
        pAct->gearReact.valueType = DALILIB_RESPONSE_VALUE_INVALID;
        pAct->gearReact.reactValue = value;
        rc = R_ABORT_ACTION;
      }
    }
  }

  return (rc);
}


//----------------------------------------------------------------------
// create a new forward frame for the control gear
// will be trigger from application controller (control device)
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_create_ff(PDaliLibInstance      pInst,
                                       ctrl_gear_commands_t* pExtGearCmd,
                                       uint32_t              cmd,
                                       send_frame_t*         pNewSendFrame,
                                       gear_adr_t*           pAdr,
                                       uint8_t               frameValue)
{
  R_DALILIB_RESULT r_res = R_DALILIB_OK;
  ctrl_gear_commands_t* pGearCmd = NULL;

  // check the control gear target address
  if (DALI_ADRTYPE_SHORT == pAdr->adrType)
  {
    if (pAdr->adr > 63)
    {
      return (R_DALILIB_CTRL_GEAR_INVALID_ADDRESS);
    }
  }
  else
  {
    if (DALI_ADRTYPE_GRP == pAdr->adrType)
    {
      if (pAdr->adr > 0x15)
      {
        return (R_DALILIB_CTRL_GEAR_INVALID_ADDRESS);
      }
    }
  }
  if (NULL == pExtGearCmd)
  {
    pGearCmd = gear_get_cmd_struct_by_cmd((GEAR_CMD)cmd);
    if (NULL == pGearCmd)
    {
      return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
    }
  }
  else // extended control gear frames
  {
    pGearCmd = pExtGearCmd;
  }
  switch (pGearCmd->eGearCmd)
  {
    case GEAR_CMD_DAPC:
      pGearCmd->opcodeByte = frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
      // with enable DAPC
      //pGearCmd->opcodeByte = pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1];
      //r_res = frame_encode_ctrl_gear(pGearCmd, &pInst->last_tx_frame.adr.lastGearAdr, FRAME_PRIO_FF_P3,
      //                 &pNewSendFrame->frame);
    break;

    case GEAR_CMD_QUERY_SCENE_LEVEL:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_QUERY_SCENE_START + frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_GO_TO_SCENE:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_GOTO_SCENE_START + frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_REMOVE_FROM_SCENE:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_REMOVE_SCENE_START + frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_SET_SCENE:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_SET_SCENE_START +
                             pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1];
      r_res = frame_encode_ctrl_gear(pGearCmd, &pInst->last_tx_frame.adr.lastGearAdr,
                                     FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_ADD_TO_GROUP:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_ADD_FROM_GRP_START + frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_REMOVE_FROM_GROUP:
      pGearCmd->opcodeByte = DALI_CTRL_GEAR_REMOVE_FROM_GRP_START + frameValue;
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_DTR0:
    case GEAR_CMD_DTR1:
    case GEAR_CMD_DTR2:
      memcpy(&pNewSendFrame->adr, pAdr, sizeof(gear_adr_t) );
      r_res = frame_encode_ctrl_gear_spec(pGearCmd, frameValue,FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_SET_OPERATING_MODE:
    case GEAR_CMD_RESET_MEMORY_BANK:
    case GEAR_CMD_SET_MAX_LEVEL:
    case GEAR_CMD_SET_MIN_LEVEL:
    case GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL:
    case GEAR_CMD_SET_POWER_ON_LEVEL:
    case GEAR_CMD_SET_FADE_TIME:
    case GEAR_CMD_SET_FADE_RATE:
    case GEAR_CMD_SET_EXTENDED_FADE_TIME:
    case GEAR_CMD_SET_SHORT_ADDRESS:
      r_res = frame_encode_ctrl_gear(pGearCmd, &pInst->last_tx_frame.adr.lastGearAdr,
                                     FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    case GEAR_CMD_WITHDRAW:
    case GEAR_CMD_QUERY_SHORT_ADDRESS:
    case GEAR_CMD_COMPARE:
    case GEAR_CMD_RANDOMISE:
    case GEAR_CMD_TERMINATE:
    case GEAR_CMD_INITIALISE:
    case GEAR_CMD_SEARCHADDRH:
    case GEAR_CMD_SEARCHADDRM:
    case GEAR_CMD_SEARCHADDRL:
    case GEAR_CMD_PROGRAM_SHORT_ADDRESS:
    case GEAR_CMD_VERIFY_SHORT_ADDRESS:
    case GEAR_CMD_WRITE_MEMORY_LOCATION:
    case GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
    case GEAR_CMD_ENABLE_DEVICE_TYPE:
      memcpy(&pNewSendFrame->adr, pAdr, sizeof(gear_adr_t) );
      r_res = frame_encode_ctrl_gear_spec(pGearCmd, frameValue,FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;

    // commands only with address parameter
    case GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0:
    case GEAR_CMD_READ_MEMORY_CELL:
    default:
      r_res = frame_encode_ctrl_gear(pGearCmd, pAdr, FRAME_PRIO_FF_P3, &pNewSendFrame->frame);
    break;
  }
  if (R_DALILIB_OK == r_res)
  {
    pNewSendFrame->lastCmd.eGearCmd = pGearCmd->eGearCmd;
    memcpy(&pNewSendFrame->adr.lastGearAdr, pAdr, sizeof(gear_adr_t));
    pNewSendFrame->type = SEND_FRAME_TYPE_CTRL_GEAR;
    pNewSendFrame->status = FRAME_WITHOUT_RESPONSE;
    if (pGearCmd->answer)
    {
      pNewSendFrame->status = FRAME_WITH_RESPONSE;
    }
  }

  return (r_res);
}

R_DALILIB_RESULT gear_save_mem_block_pers_vars_frequent(PDaliLibInstance           pInst,
                                                        dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                               rc = R_DALILIB_OK;
  dali_ctrl_gear_pers_variables_frequent_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_VARS_FREQUENT_ID;
  block.version = DALI_CTRL_GEAR_PERS_VARS_FREQUENT_VERSION;

  // populate memory block
  block.lastLightLevel = pGearPersistent->gearPersVariables.lastLightLevel;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_VARS_FREQUENT_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_save_mem_block_pers_vars_operating(PDaliLibInstance           pInst,
                                                         dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                               rc = R_DALILIB_OK;
  dali_ctrl_gear_pers_variables_operating_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_VARS_OPERATING_ID;
  block.version = DALI_CTRL_GEAR_PERS_VARS_OPERATING_VERSION;

  // populate memory block
  block.fadeRate            = pGearPersistent->gearPersVariables.fadeRate;
  block.fadeTime            = pGearPersistent->gearPersVariables.fadeTime;
  block.extFadeTimeBase     = pGearPersistent->gearPersVariables.extFadeTimeBase;
  block.extFadeTimeMultiple = pGearPersistent->gearPersVariables.extFadeTimeMultiple;
  block.operatingMode       = pGearPersistent->gearPersVariables.operatingMode;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_VARS_OPERATING_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_save_mem_block_pers_vars_config(PDaliLibInstance           pInst,
                                                      dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                             rc = R_DALILIB_OK;
  dali_ctrl_gear_pers_variables_config_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_VARS_CONFIG_ID;
  block.version = DALI_CTRL_GEAR_PERS_VARS_CONFIG_VERSION;

  // populate memory block
  block.highResDimming     = pGearPersistent->gearPersVariables.highResDimming;
  block.phm                = pGearPersistent->gearPersVariables.phm;
  block.powerOnLevel       = pGearPersistent->gearPersVariables.powerOnLevel;
  block.systemFailureLevel = pGearPersistent->gearPersVariables.systemFailureLevel;
  block.minLevel           = pGearPersistent->gearPersVariables.minLevel;
  block.maxLevel           = pGearPersistent->gearPersVariables.maxLevel;
  block.shortAddress       = pGearPersistent->gearPersVariables.shortAddress;
  block.randomAddress      = pGearPersistent->gearPersVariables.randomAddress;
  block.gearGroups         = pGearPersistent->gearPersVariables.gearGroups;
  block.gearDeviceType     = pGearPersistent->gearPersVariables.gearDeviceType;
  memcpy(block.scene, pGearPersistent->gearPersVariables.scene, sizeof(block.scene));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_VARS_CONFIG_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_save_mem_block_flamp_pers_vars(PDaliLibInstance           pInst,
                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                            rc = R_DALILIB_OK;
  dali_ctrl_gear_flamp_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_FLUORESCENT_ID;
  block.version = DALI_CTRL_GEAR_PERS_FLUORESCENT_VERSION;

  // populate memory block
  block.extendedVersionNumber     = pGearPersistent->fluorescentPersVariables.extendedVersionNumber;
  block.deviceType                = pGearPersistent->fluorescentPersVariables.deviceType;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_FLUORESCENT_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_save_mem_blocks(PDaliLibInstance           pInst,
                                      dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (pInst->config.internal.savePersistentModules)
  {
    if (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK)
    {
      rc = gear_save_mem_block_pers_vars_frequent(pInst, pGearPersistent);
    }

    if (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK)
    {
      rc = gear_save_mem_block_pers_vars_operating(pInst, pGearPersistent);
    }

    if (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK)
    {
      rc = gear_save_mem_block_pers_vars_config(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK))
    {
      rc = gear_save_mem_block_flamp_pers_vars(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK))
    {
      rc = gear_led_save_mem_block_pers_vars_frequent(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK))
    {
      rc = gear_led_save_mem_block_pers_vars_operating(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK))
    {
      rc = gear_led_save_mem_block_pers_vars_config(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_COLOUR_MASK))
    {
      rc = gear_colour_save_mem_block_pers_vars(pInst, pGearPersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_SWITCHING_MASK))
    {
      rc = gear_switching_save_mem_block_pers_vars(pInst, pGearPersistent);
    }

    #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK))
    {
      rc = gear_mem_bank1_ext_save_mem_block_pers_vars(pInst, pGearPersistent);
    }
    #endif

    #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK))
    {
      rc = gear_power_supply_save_mem_block_pers_vars(pInst, pGearPersistent);
    }
    #endif

    #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK))
    {
      rc = gear_energy_report_save_mem_block_pers_vars(pInst, pGearPersistent);
    }
    #endif

    #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK))
    {
      rc = gear_diag_maintenance_save_mem_block_memory_bank205(pInst, pGearPersistent);
    }
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK))
    {
      rc = gear_diag_maintenance_save_mem_block_memory_bank206(pInst, pGearPersistent);
    }
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.gearInstance.gearPersistentChangedFlags & DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK))
    {
      rc = gear_diag_maintenance_save_mem_block_memory_bank207(pInst, pGearPersistent);
    }
    #endif
  }
  else
  {
    // preserve backwards compatibility
    rc = dali_cb_save_mem(pInst, pGearPersistent, sizeof(*pGearPersistent));
  }

  return (rc);
}

//----------------------------------------------------------------------
// initialize the default variable of the control gear
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_init_persistent(PDaliLibInstance           pInst,
                                             dali_ctrl_gear_pers_mem_t* pPersistentMemory)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t sceneCntr = 0;
  PCtrlGearPersistentVariables   pPersistentVariables = NULL;

  pPersistentVariables = &pPersistentMemory->gearPersVariables;

  memset(pPersistentMemory, 0, sizeof(*pPersistentMemory));

  // persistent variables
  pPersistentVariables->highResDimming = pInst->config.vendor.control_gear_high_res_dimming;
  pPersistentVariables->phm = (1 > pInst->config.vendor.control_gear_phm) ? 1 :
                                pInst->config.vendor.control_gear_phm;
  pPersistentVariables->gearDeviceType = pInst->config.vendor.control_gear_device_type;

  pPersistentVariables->maxLevel = 0xFE;
  pPersistentVariables->minLevel = (uint32_t)pPersistentVariables->phm;
  pPersistentVariables->powerOnLevel = 0xFE;
  pPersistentVariables->systemFailureLevel  = 0xFE;
  pPersistentVariables->lastLightLevel      = 0xFE;

  pPersistentVariables->shortAddress = pInst->config.vendor.shortAddress;

  pPersistentVariables->gearGroups = 0x0000;
  pPersistentVariables->randomAddress = 0x00FFFFFF;

  for (sceneCntr = 0; sceneCntr < DALI_CTRL_GEAR_MAX_SCENES; sceneCntr++)
  {
    pPersistentVariables->scene[sceneCntr] = 0xFF;
  }

  pPersistentVariables->fadeRate = 7;
  pPersistentVariables->fadeTime = 0;
  pPersistentVariables->extFadeTimeBase      = 0x00;
  pPersistentVariables->extFadeTimeMultiple  = 0x00;

  switch(pPersistentVariables->gearDeviceType)
  {
    case GEAR_TYPE_FLUORESCENT_LAMPS:
      pPersistentMemory->fluorescentPersVariables.extendedVersionNumber = FLUORESCENT_LAMP_VERSION;
      pPersistentMemory->fluorescentPersVariables.deviceType = GEAR_TYPE_FLUORESCENT_LAMPS;
    break;

    case GEAR_TYPE_NO_FURTHER_DEVICE_TYPE:
    break;

    case GEAR_TYPE_LED_MODULES:
      gear_led_init_persistent(pInst, pPersistentMemory);
    break;

    case GEAR_TYPE_SWITCHING_FUNCTION:
      gear_switching_init_persistent(pInst, pPersistentMemory);
    break;

    case GEAR_TYPE_COLOUR_CONTROL:
      gear_colour_init_persistent(pInst, pPersistentMemory);
    break;

    default:
      rc = (R_DALILIB_GEAR_DEVICE_TYPE_NOT_SUPPORTED);
    break;
  }

  // persistent memory of device type extensions
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    gear_power_supply_init_persistent(pInst, pPersistentMemory);
  #endif
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    gear_mem_bank1_ext_init_persistent(pInst, pPersistentMemory);
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    gear_diag_maintenance_init_persistent(pInst, pPersistentMemory);
  #endif

  return (rc);
}

/**********************************************************************
 * Functions for the received forward frames of the control gear
 **********************************************************************/

//----------------------------------------------------------------------
// process the forward configuration frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_configuration(PCtrlGearInstance     pCtrlInst,
                                                           ctrl_gear_commands_t* pGearCmd,
                                                           uint8_t               reqValue,
                                                           uint8_t*              pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  (void)pRespValue;

  switch(pGearCmd->eGearCmd)
  {
    case GEAR_CMD_RESET:
      {
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearIdentifyRunning = FALSE;
        pCtrlInst->gearResetRunning = TRUE;
        pCtrlInst->gearResetStartTime  = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        rc = gear_init_by_reset(&pCtrlInst->gearVariables, &pCtrlInst->gearPersistent);
        pCtrlInst->gearResetRunning = FALSE;
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_STORE_ACTUAL_LEVEL_IN_DTR0:
      {
        pCtrlInst->gearVariables.DTR0 = pCtrlInst->gearVariables.actualLevel;
      }
    break;

    case GEAR_CMD_SAVE_PERSISTENT_VARIABLES:
      {
        if ( TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK |
                                                 DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK |
                                                 DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK |
                                                 DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK |
                                                 DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK |
                                                 DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK |
                                                 DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK |
                                                 DALI_CTRL_GEAR_PERS_COLOUR_MASK |
                                                 DALI_CTRL_GEAR_PERS_SWITCHING_MASK |
                                                 DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK |
                                                 DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK |
                                                 DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK |
                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK |
                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK |
                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
        rc = gear_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->gearPersistent);
      }
    break;

    case GEAR_CMD_SET_OPERATING_MODE:
      {
        if (DALI_CTRL_GEAR_NORMAL_OPERTING_MODE == pCtrlInst->gearVariables.DTR0)
        {
          if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.operatingMode)
          {
            pCtrlInst->gearPersistent.gearPersVariables.operatingMode = pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case GEAR_CMD_RESET_MEMORY_BANK:
      {
        pCtrlInst->gearResetMemoryRunning = TRUE;
        rc = gear_reset_memory_bank(pCtrlInst, pCtrlInst->gearVariables.DTR0);
        pCtrlInst->gearResetMemoryStartTime  = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    break;

    case GEAR_CMD_IDENTIFY_DEVICE:
      {
        if ( TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearIdentifyDeviceStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        pCtrlInst->gearIdentifyRunning = TRUE;
      }
    break;

    case GEAR_CMD_SET_MAX_LEVEL:
      {
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        pCtrlInst->gearVariables.limitError = FALSE;

        if (pCtrlInst->gearPersistent.gearPersVariables.minLevel >= pCtrlInst->gearVariables.DTR0)
        {
          if (pCtrlInst->gearPersistent.gearPersVariables.minLevel != pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.maxLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
        else
        {
          if (pCtrlInst->gearVariables.DTR0 == DALI_MASK)
          {
            if (0xFE != pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
            {
              pCtrlInst->gearPersistent.gearPersVariables.maxLevel = 0xFE;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
          else
          {
            if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
            {
              pCtrlInst->gearPersistent.gearPersVariables.maxLevel = pCtrlInst->gearVariables.DTR0;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
        }
        if (pCtrlInst->gearVariables.actualLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          } 
          pCtrlInst->gearVariables.limitError = TRUE;
          gear_reaction(pCtrlInst,
                        DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                        gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
        }
      }
    break;

    case GEAR_CMD_SET_MIN_LEVEL:
      {
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
        }
        pCtrlInst->gearVariables.limitError = FALSE;

        if (pCtrlInst->gearVariables.DTR0 <= pCtrlInst->gearPersistent.gearPersVariables.phm)
        {
          if ((uint32_t)pCtrlInst->gearPersistent.gearPersVariables.phm != pCtrlInst->gearPersistent.gearPersVariables.minLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.minLevel = (uint32_t)pCtrlInst->gearPersistent.gearPersVariables.phm;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
        else
        {
          if (pCtrlInst->gearVariables.DTR0 >= pCtrlInst->gearPersistent.gearPersVariables.maxLevel ||
              pCtrlInst->gearVariables.DTR0 == DALI_MASK)
          {
            if (pCtrlInst->gearPersistent.gearPersVariables.maxLevel != pCtrlInst->gearPersistent.gearPersVariables.minLevel)
            {
              pCtrlInst->gearPersistent.gearPersVariables.minLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
          else
          {
            if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.minLevel)
            {
              pCtrlInst->gearPersistent.gearPersVariables.minLevel = pCtrlInst->gearVariables.DTR0;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
        }

        if (0x00 == pCtrlInst->gearVariables.actualLevel)
        {
          break;
        }
        else 
        {
          if (pCtrlInst->gearVariables.actualLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel)
          {
            pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
            if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
            {
              pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
            if (0 != pCtrlInst->gearVariables.targetLevel)
            {
              pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
            }
            pCtrlInst->gearVariables.limitError = TRUE;
            gear_reaction(pCtrlInst,
                          DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                          gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
          }
        }
      }
    break;

    case GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL:
      {
        if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_SET_POWER_ON_LEVEL:
      {
        if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_SET_FADE_TIME:
        {
          if ((pCtrlInst->gearVariables.DTR0 > DALI_FADE_TIME_RATE_FACTOR_MAX ?
               DALI_FADE_TIME_RATE_FACTOR_MAX : pCtrlInst->gearVariables.DTR0) != pCtrlInst->gearPersistent.gearPersVariables.fadeTime)
          {
            pCtrlInst->gearPersistent.gearPersVariables.fadeTime = (pCtrlInst->gearVariables.DTR0 > DALI_FADE_TIME_RATE_FACTOR_MAX ?
                                                     DALI_FADE_TIME_RATE_FACTOR_MAX : pCtrlInst->gearVariables.DTR0);
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
    break;

    case GEAR_CMD_SET_FADE_RATE:
      {
        if (pCtrlInst->gearVariables.DTR0 > DALI_FADE_TIME_RATE_FACTOR_MAX )
        {
          if (DALI_FADE_TIME_RATE_FACTOR_MAX != pCtrlInst->gearPersistent.gearPersVariables.fadeRate)
          {
            pCtrlInst->gearPersistent.gearPersVariables.fadeRate = DALI_FADE_TIME_RATE_FACTOR_MAX;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
        else
        {
          if (pCtrlInst->gearVariables.DTR0 == 0 )
          {
            if (1 != pCtrlInst->gearPersistent.gearPersVariables.fadeRate)
            {
              pCtrlInst->gearPersistent.gearPersVariables.fadeRate = 1;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
          else
          {
            if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.fadeRate)
            {
              pCtrlInst->gearPersistent.gearPersVariables.fadeRate = pCtrlInst->gearVariables.DTR0;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
        }
      }
    break;

    case GEAR_CMD_SET_EXTENDED_FADE_TIME:
      {
        if (pCtrlInst->gearVariables.DTR0 > DALI_EX_FADE_FACTOR_MAX)
        {
          if (0x00 != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase ||
              0x00 != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
          {
            pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase = 0x00;
            pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple = 0x00;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
        else
        {
          if ((pCtrlInst->gearVariables.DTR0 & 0x0F)        != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase ||
              ((pCtrlInst->gearVariables.DTR0 >> 4) & 0x07) != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
          {
            pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase = (pCtrlInst->gearVariables.DTR0 & 0x0F);
            pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple = ((pCtrlInst->gearVariables.DTR0 >> 4) & 0x07);
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case GEAR_CMD_SET_SCENE:
      {
        uint8_t sceneIdx = reqValue - pGearCmd->opcodeByte;
        if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx])
        {
          pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_REMOVE_FROM_SCENE:
      {
        uint8_t sceneIdx = reqValue - pGearCmd->opcodeByte;
        if (DALI_MASK != pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx])
        {
          pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] = DALI_MASK;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_ADD_TO_GROUP:
      {
        uint8_t groupIdx = reqValue - pGearCmd->opcodeByte;
        if ((0x01 << groupIdx) != ((0x01 << groupIdx) & pCtrlInst->gearPersistent.gearPersVariables.gearGroups))
        {
          pCtrlInst->gearPersistent.gearPersVariables.gearGroups |= (0x01 << groupIdx);
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_REMOVE_FROM_GROUP:
      {
        uint8_t groupIdx = reqValue - pGearCmd->opcodeByte;
        if (~(0x01 << groupIdx) != (~(0x01 << groupIdx) & pCtrlInst->gearPersistent.gearPersVariables.gearGroups))
        {
          pCtrlInst->gearPersistent.gearPersVariables.gearGroups &= ~(0x01 << groupIdx);
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_SET_SHORT_ADDRESS:
      {
        uint8_t newAdr = 0x00;
        if (pCtrlInst->gearVariables.DTR0 == DALI_MASK)
        {
          if (pCtrlInst->gearVariables.DTR0 != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
          {
            pCtrlInst->gearPersistent.gearPersVariables.shortAddress = pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          break;
        }
        // DTR-0 = 1xxxxxxxb
        // DTR-0 = xxxxxxx0b
        if ((pCtrlInst->gearVariables.DTR0 & 0x80) ||
            !(pCtrlInst->gearVariables.DTR0 & 0x01))
        {
          break;
        }
        newAdr = pCtrlInst->gearVariables.DTR0 >> 1;
        if ((newAdr & 0x3F) != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          pCtrlInst->gearPersistent.gearPersVariables.shortAddress = (newAdr & 0x3F);
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_ENABLE_WRITE_MEMORY:
      {
        pCtrlInst->gearVariables.writeEnableState = CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED;
      }
    break;

    default:
      {
        rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
      }
    break;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the forward initialise frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_special(PCtrlGearInstance     pCtrlInst,
                                                     ctrl_gear_commands_t* pGearCmd,
                                                     uint8_t               reqValue,
                                                     uint8_t*              pRespValue)
{
  R_DALILIB_RESULT rc =R_DALILIB_OK;
  uint8_t          nMemBank;
  uint8_t          doIncrement;
  uint8_t          offset;
  
  switch(pGearCmd->eGearCmd)
  {
    case GEAR_CMD_INITIALISE:
      {
        // all control gear
        if (reqValue == 0x00 &&
            CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          pCtrlInst->gearVariables.initialisationState = CTRL_GEAR_INITIALISE_STATE_ENABLED;
        }
        else
        {
          // only not short address
          if (reqValue == 0xFF && DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress &&
              CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
          {
            pCtrlInst->gearVariables.initialisationState = CTRL_GEAR_INITIALISE_STATE_ENABLED;
          }
          else
          {
            // only for me
            // check for valid address
                // DTR-0 = 1xxxxxxxb
                // DTR-0 = xxxxxxx0b
            if ((reqValue & 0x80) || !(reqValue & 0x01))
            {
              // ignore frame
              rc = R_IGNORE_FRAME;
              break;
            }
            else
            {
              if (pCtrlInst->gearPersistent.gearPersVariables.shortAddress == (reqValue >> 1) &&
                  CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
              {
                pCtrlInst->gearVariables.initialisationState = CTRL_GEAR_INITIALISE_STATE_ENABLED;
              }
            }
          }
        }
        // start or prolong the initialisation state
        pCtrlInst->gearInitialiseStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    break;

    case GEAR_CMD_TERMINATE:
      {
        pCtrlInst->gearVariables.initialisationState = CTRL_GEAR_INITIALISE_STATE_DISABLED;
        pCtrlInst->gearIdentifyRunning = FALSE;
      }
    break;

    case GEAR_CMD_DTR0:
      {
        pCtrlInst->gearVariables.DTR0 = reqValue;
      }
    break;

    case GEAR_CMD_RANDOMISE:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        uint32_t randomAddress = gear_create_random_address(pCtrlInst);
        if (randomAddress != pCtrlInst->gearPersistent.gearPersVariables.randomAddress)
        {
          pCtrlInst->gearPersistent.gearPersVariables.randomAddress = randomAddress;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_COMPARE:
      {
        rc = R_IGNORE_FRAME;
        if (CTRL_GEAR_INITIALISE_STATE_ENABLED == pCtrlInst->gearVariables.initialisationState)
        {
          if (pCtrlInst->gearPersistent.gearPersVariables.randomAddress <= pCtrlInst->gearVariables.searchAddress)
          {
            *pRespValue = DALI_YES;
            rc = R_DALILIB_OK;
          }
        }
      }
    break;

    case GEAR_CMD_WITHDRAW:
      {
        pCtrlInst->gearIdentifyRunning = FALSE;
        rc = R_IGNORE_FRAME;
        if (CTRL_GEAR_INITIALISE_STATE_ENABLED == pCtrlInst->gearVariables.initialisationState)
        {
          if (pCtrlInst->gearPersistent.gearPersVariables.randomAddress == pCtrlInst->gearVariables.searchAddress)
          {
            pCtrlInst->gearVariables.initialisationState = CTRL_GEAR_INITIALISE_STATE_WITHDRAWN;
            rc = R_DALILIB_OK;
          }
        }
      }
    break;
  
    case GEAR_CMD_SEARCHADDRH:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        uint8_t *pAdr = (uint8_t*)&pCtrlInst->gearVariables.searchAddress;
        pAdr[2] = reqValue;
      }
    break;

    case GEAR_CMD_SEARCHADDRM:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        uint8_t *pAdr = (uint8_t*)&pCtrlInst->gearVariables.searchAddress;
        pAdr[1] = reqValue;
      }
    break;

    case GEAR_CMD_SEARCHADDRL:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        uint8_t *pAdr = (uint8_t*)&pCtrlInst->gearVariables.searchAddress;
        pAdr[0] = reqValue;
      }
    break;

    case GEAR_CMD_PROGRAM_SHORT_ADDRESS:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        if (pCtrlInst->gearPersistent.gearPersVariables.randomAddress != pCtrlInst->gearVariables.searchAddress)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        if (reqValue == DALI_MASK)
        {
          if (reqValue != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
          {
            pCtrlInst->gearPersistent.gearPersVariables.shortAddress = reqValue;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          break;
        }
        // check for valid address
        if (  (reqValue & 0x80) || !(reqValue & 0x01) )
        {
          break;
        }
        uint8_t newAdr = reqValue >> 1;
        if ((newAdr & 0x3F) != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          pCtrlInst->gearPersistent.gearPersVariables.shortAddress = (newAdr & 0x3F);
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_VERIFY_SHORT_ADDRESS:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        // check for valid address
        if ((reqValue & 0x80) || !(reqValue & 0x01))
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        uint8_t data = reqValue >> 1;
        if (data == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          *pRespValue = DALI_YES;
        }
        else
        {
          // NO
          rc = R_IGNORE_FRAME;
        }
      }
    break;

    case GEAR_CMD_QUERY_SHORT_ADDRESS:
      {
        if (CTRL_GEAR_INITIALISE_STATE_DISABLED == pCtrlInst->gearVariables.initialisationState)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        if (pCtrlInst->gearPersistent.gearPersVariables.randomAddress != pCtrlInst->gearVariables.searchAddress)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        if (DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          *pRespValue = DALI_MASK;
        }
        else
        {
          *pRespValue = (pCtrlInst->gearPersistent.gearPersVariables.shortAddress << 1) | 0x01;
        }
      }
    break;

    case GEAR_CMD_ENABLE_DEVICE_TYPE:
      {
        switch(reqValue)
        {
          case GEAR_TYPE_SUPPORT_MORE_ONE_DEVICE_TYPE:
          case GEAR_TYPE_NO_FURTHER_DEVICE_TYPE:
            // If data equals MASK or 254, the command shall be ignored.
            // ignore frame
          break;
          
          case GEAR_TYPE_LED_MODULES:
          case GEAR_TYPE_SWITCHING_FUNCTION:
          case GEAR_TYPE_FLUORESCENT_LAMPS:
          case GEAR_TYPE_COLOUR_CONTROL:
          case GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY:
          case GEAR_TYPE_CONTROL_MEM_BANK1_EXT_SUPPLY:
          case GEAR_TYPE_CONTROL_GEAR_ENERGY_REPORTING:
          case GEAR_TYPE_CONTROL_GEAR_DIAG_MAINTENANCE:
            pCtrlInst->gearVariables.enableDeviceType = reqValue;
          break;
          
          default:
            //ignore frame
          break;
        }
      }
    break;

    case GEAR_CMD_DTR1:
      {
        pCtrlInst->gearVariables.DTR1 = reqValue;
      }
    break;

    case GEAR_CMD_DTR2:
      {
        pCtrlInst->gearVariables.DTR2 = reqValue;
      }
    break;

    case GEAR_CMD_WRITE_MEMORY_LOCATION:
    case GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
      {
        nMemBank = pCtrlInst->gearVariables.DTR1;
        doIncrement = TRUE;

        if (CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED != pCtrlInst->gearVariables.writeEnableState)
        {
          // write enable state disabled: ignore frame, don't increment DTR0
          rc = R_IGNORE_FRAME;
          break;
        }

        switch(nMemBank)
        {
          case 0:
            // bank not writable: ignore frame, increment DTR0
            rc = R_IGNORE_FRAME;
          break;

          case CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
              rc = gear_mem_bank1_ext_write_memory_bank1(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
              rc = gear_power_supply_write_memory_bank201(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
              rc = gear_energy_reporting_write_memory_bank202(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_FIRST_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_write_memory_bank205(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_SECOND_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_write_memory_bank206(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_write_memory_bank207(pCtrlInst, pGearCmd, reqValue, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          default:
            // check whether desired membank exists
            if ((MAXMEMBANKS > nMemBank) &&
                (pCtrlInst->gearMemBanksOther.is_used[nMemBank]))
            {
              offset = pCtrlInst->gearVariables.DTR0;
              if ( (2 < offset) && (offset <= pCtrlInst->gearMemBanksOther.memBank[nMemBank][0]) )
              {
                pCtrlInst->gearMemBanksOther.memBank[nMemBank][offset] = reqValue;
                *pRespValue = reqValue;
              }
              else
              {
                // cell not implemented/writable: ignore frame, increment DTR0
                rc = R_IGNORE_FRAME;
              }
            }
            else
            {
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            }
          break;
        }

        // increment DTR0
        if (doIncrement &&
            pCtrlInst->gearVariables.DTR0 < 0xFF)
        {
          pCtrlInst->gearVariables.DTR0 += 1;
        }
      }
    break;

    default:
      {
        rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
      }
    break;
  }

  return(rc);
}


//----------------------------------------------------------------------
// process the forward status frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_status(PCtrlGearInstance     pCtrlInst,
                                                    ctrl_gear_commands_t* pGearCmd,
                                                    uint8_t*              pValue,
                                                    gear_adr_t*           pReceivedAdr)
{
  R_DALILIB_RESULT rc =R_DALILIB_OK;

  // set "NO" answer flag
  pGearCmd->answer = FALSE;

  switch(pGearCmd->eGearCmd)
  {
    case GEAR_CMD_QUERY_STATUS:
      {
        uint8_t status = 0x00;
        status = pCtrlInst->gearVariables.powerCycleSeen;
        status <<= 1;
        if (DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          status |= 0x01;
        }
        status <<= 1;
        status |= pCtrlInst->gearVariables.resetState;
        status <<= 1;
        status |= pCtrlInst->gearVariables.fadeRunning;
        status <<= 1;
        status |= pCtrlInst->gearVariables.limitError;
        status <<= 1;
        status |= pCtrlInst->gearVariables.lampOn;
        status <<= 1;
        status |= pCtrlInst->gearVariables.lampFailure;
        status <<= 1;
        status |= pCtrlInst->gearVariables.controlGearFailure;

        *pValue = status;
        pGearCmd->answer = TRUE;
      }
    break;

    case GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE:
      {
        // Application will be update the status value.
        gear_reaction(pCtrlInst, DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_GEAR_FAILURE, NOT_USED_PARAM );

        if (pCtrlInst->gearVariables.controlGearFailure)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_LAMP_FAILURE:
      {
        // Application will be update the status value.
        gear_reaction(pCtrlInst, DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_LAMPFAILURE, NOT_USED_PARAM );
        if (pCtrlInst->gearVariables.lampFailure)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_LAMP_POWER_ON:
      {
        // Application will be update the status value.
        gear_reaction(pCtrlInst, DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_ACTUAL_LEVEL, NOT_USED_PARAM );
        if (pCtrlInst->gearVariables.lampOn)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;;
        }
      }
    break;

    case GEAR_CMD_QUERY_LIMIT_ERROR:
      {
        if (pCtrlInst->gearVariables.limitError)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_RESET_STATE:
      {
        if (pCtrlInst->gearVariables.resetState)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_POWER_FAILURE:
      {
        if (pCtrlInst->gearVariables.powerCycleSeen)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT:
      {
        if (DALI_MASK != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
        if (DALI_ADRTYPE_NBROADCAST == pReceivedAdr->adrType &&
            DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    case GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS:
      {
        if (DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
        {
          pGearCmd->answer = TRUE;
          *pValue = DALI_YES;
        }
      }
    break;

    default:
      {
        rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
      }
    break;
  }

  return(rc);
}

//----------------------------------------------------------------------
// process the forward level frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_levels(PCtrlGearInstance     pCtrlInst,
                                                    ctrl_gear_commands_t* pGearCmd,
                                                    uint32_t              data,
                                                    uint8_t*              pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  (void)pRespValue;

  switch(pGearCmd->eGearCmd)
  {
    case GEAR_CMD_DAPC: // Absolute level instructions using the fade time
      {
        if (TRUE == pCtrlInst->gearEnableDAPCRunning)
        {
          pCtrlInst->gearEnableDAPCStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
        uint8_t useFutureFadeTime = 0x00;
        uint32_t futureFadeTime = 0;
        uint8_t requestLevel = (data & DALI_CTRL_GEAR_FF_VALUE_MASK);
        pCtrlInst->gearVariables.targetLevel = requestLevel;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearDimm.afterDimmOff = 0;
        if (DALI_MASK == requestLevel)
        {
          gear_stop_dimming(pCtrlInst);
          pCtrlInst->gearEnableDAPCRunning = FALSE;
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;

        if (pCtrlInst->gearVariables.targetLevel == pCtrlInst->gearVariables.actualLevel)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        if ((pCtrlInst->gearVariables.targetLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
             pCtrlInst->gearVariables.targetLevel > 0x00) ||
            pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.limitError = TRUE;
        }

        futureFadeTime = gear_get_future_fade_time(pCtrlInst, &useFutureFadeTime);

        if ((0x00 == requestLevel) &&
            // use fade ?
            (0 != pCtrlInst->gearPersistent.gearPersVariables.fadeTime ||
             0 != useFutureFadeTime ||
             0 != futureFadeTime ||
             0 != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple))
        {
          // dimming-time will be calculate without time between minLevel and 0x00
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
          pCtrlInst->gearDimm.afterDimmOff = 1;
        }
        if (requestLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel && requestLevel >= 0x01)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
        }

        if (requestLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel && requestLevel <= 0xFE)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
        }

        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != requestLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }

        if (TRUE == pCtrlInst->gearEnableDAPCRunning)
        {
          gear_start_dimming(pCtrlInst,
                             DIMM_TYPE_DIMMTIME,
                             pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearVariables.actualLevel ?
                               DIMM_DIRECTION_UP : DIMM_DIRECTION_DOWN,
                             useFutureFadeTime,
                             futureFadeTime);
          pCtrlInst->gearDimm.stepTime = DIMM_RATE_TIME;
          break;
        }
        // go to new Value without fade
        if (0 == pCtrlInst->gearPersistent.gearPersVariables.fadeTime &&
            0 == useFutureFadeTime &&
            0 == futureFadeTime &&
            0 == pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
        {
          gear_reaction(pCtrlInst,
                        DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                        (pCtrlInst->gearDimm.afterDimmOff ? 0 : 
                          gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel)));
        }
        else
        {
          // start dimming
          gear_start_dimming(pCtrlInst,
                             DIMM_TYPE_DIMMTIME,
                             pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearVariables.actualLevel ?
                               DIMM_DIRECTION_UP : DIMM_DIRECTION_DOWN,
                             useFutureFadeTime,
                             futureFadeTime);
        }
      }
    break;

    case GEAR_CMD_OFF:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.targetLevel = 0x00;
        pCtrlInst->gearVariables.limitError = FALSE;
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_OFF,
                      NOT_USED_PARAM);
      }
    break;

    case GEAR_CMD_UP:
      {
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;

        if (pCtrlInst->gearVariables.actualLevel == 0x00 ||
            pCtrlInst->gearVariables.actualLevel == pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          break;
        }

        //Dimmen nutzt eine 200ms Überblendzeit mit der ausgewählten Überblendgeschwindigkeit.
        //targetLevel muss auf der Grundlage von "actualLevel" und der ausgewählten Überblendegeschwindigkeit
        //berechnet werden.
        gear_start_dimming(pCtrlInst, DIMM_TYPE_DIMMRATE, DIMM_DIRECTION_UP,0,0);
      }
    break;

    case GEAR_CMD_DOWN:
      {
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;

        if (pCtrlInst->gearVariables.actualLevel == 0x00 ||
            pCtrlInst->gearVariables.actualLevel == pCtrlInst->gearPersistent.gearPersVariables.minLevel)
        {
          break;
        }

        //Dimmen nutzt eine 200ms Überblendzeit mit der ausgewählten Überblendgeschwindigkeit.
        //targetLevel muss uaf der Grundlage von "actualLevel" und der ausgewählten Überblendegeschwindigkeit
        //berechnet werden.
        gear_start_dimming(pCtrlInst, DIMM_TYPE_DIMMRATE, DIMM_DIRECTION_DOWN,0,0);
      }
    break;

    case GEAR_CMD_STEP_UP:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
        if (0x00 == pCtrlInst->gearVariables.targetLevel)
        {
          //ignore frame
          break;
        }
        if (DALI_MASK == pCtrlInst->gearVariables.targetLevel)
        {
          //ignore frame
          break;
        }
        if (pCtrlInst->gearVariables.targetLevel >= pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
            pCtrlInst->gearVariables.targetLevel < pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.targetLevel += 1;
        }
        if (pCtrlInst->gearVariables.targetLevel == pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.limitError = TRUE;
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
        }
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_STEP_DOWN:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        if (0x00 == pCtrlInst->gearVariables.targetLevel)
        {
          break;
        }
        if (DALI_MASK == pCtrlInst->gearVariables.targetLevel)
        {
          //ignore frame
          break;
        }
        if (pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
            pCtrlInst->gearVariables.targetLevel <= pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.targetLevel -= 1;
        }
        if (pCtrlInst->gearVariables.targetLevel == pCtrlInst->gearPersistent.gearPersVariables.minLevel)
        {
          pCtrlInst->gearVariables.limitError = TRUE;
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
        }
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_RECALL_MAX_LEVEL:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_RECALL_MIN_LEVEL:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_STEP_DOWN_AND_OFF:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        if (0x00 == pCtrlInst->gearVariables.targetLevel)
        {
          // ignore frame
          break;
        }
        if (DALI_MASK == pCtrlInst->gearVariables.targetLevel)
        {
          // ignore frame
          break;
        }
        if (pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
            pCtrlInst->gearVariables.targetLevel <= pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.targetLevel -= 1;
        }
        if (pCtrlInst->gearVariables.targetLevel == pCtrlInst->gearPersistent.gearPersVariables.minLevel)
        {
          pCtrlInst->gearVariables.targetLevel = 0x00;
          gear_reaction(pCtrlInst,
                        DALILIB_REACT_INTERNAL_CTRL_GEAR_OFF,
                        NOT_USED_PARAM);
        }
        else
        {
          gear_reaction(pCtrlInst,
                        DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                        gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
        }
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
      }
    break;

    case GEAR_CMD_ON_AND_STEP_UP:
      {
        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
          if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
          {
            pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          if (0 != pCtrlInst->gearVariables.targetLevel)
          {
            pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
          }
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;

        if (DALI_MASK == pCtrlInst->gearVariables.targetLevel)
        {
          //ignore frame
          break;
        }
        if (0x00 == pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
        }
        else
        {
          if (pCtrlInst->gearVariables.targetLevel >= pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
              pCtrlInst->gearVariables.targetLevel < pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
          {
            pCtrlInst->gearVariables.targetLevel += 1;
          }
        }

        if (pCtrlInst->gearVariables.targetLevel >= pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.limitError = TRUE;
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
        }
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
      }
    break;

    case GEAR_CMD_ENABLE_DAPC_SEQUENCE:
      {
        if (TRUE == pCtrlInst->gearEnableDAPCRunning || TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          // ignore frame
          rc = R_IGNORE_FRAME;
          break;
        }
        pCtrlInst->gearEnableDAPCRunning = TRUE;
        pCtrlInst->gearEnableDAPCStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        // ignore frame
        rc = R_IGNORE_FRAME;
      }
    break;

    case GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL:
      {
        uint8_t useFutureFadeTime = 0x00;
        uint32_t futureFadeTime = 0;

        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.limitError = FALSE;
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;
        pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.lastActiveLevel;
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearVariables.actualLevel)
        {
          if (pCtrlInst->gearVariables.targetLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
              pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
          {
            pCtrlInst->gearVariables.limitError = TRUE;
          }
          if (0x00 == pCtrlInst->gearVariables.lastActiveLevel)
          {
            // dimmin-time will be calculate without time between minLevel and 0x00
            pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
            pCtrlInst->gearDimm.afterDimmOff = 1;
          }
          if (pCtrlInst->gearVariables.lastActiveLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
              pCtrlInst->gearVariables.lastActiveLevel >= 0x01)
          {
            pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
          }

          if (pCtrlInst->gearVariables.lastActiveLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel &&
              pCtrlInst->gearVariables.lastActiveLevel <= 0xFE)
          {
            pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
          }

          futureFadeTime = gear_get_future_fade_time(pCtrlInst, &useFutureFadeTime);

          if (0 == pCtrlInst->gearPersistent.gearPersVariables.fadeTime &&
              0 == useFutureFadeTime &&
              0 == futureFadeTime &&
              0 == pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
          {
            gear_reaction(pCtrlInst,
                          DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                          gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
          }
          else
          {
            //Dimmvorgang von "actulLevel" zu "tragetLevel" muss mit dem Gebrauch
            // der gewählten Überblendzeit starten
            gear_start_dimming(pCtrlInst,
                               DIMM_TYPE_DIMMTIME,
                               pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearVariables.actualLevel ?
                                 DIMM_DIRECTION_UP : DIMM_DIRECTION_DOWN,
                               useFutureFadeTime,
                               futureFadeTime);
          }
        }
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case GEAR_CMD_GO_TO_SCENE:
      {
        uint8_t useFutureFadeTime = 0x00;
        uint32_t futureFadeTime = 0;
        uint8_t sceneCmd = (data & DALI_CTRL_GEAR_FF_SCENENR_MASK);
        uint8_t sceneIdx = sceneCmd - pGearCmd->opcodeByte;

        if (TRUE == pCtrlInst->gearVariables.fadeRunning)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearVariables.actualLevel;
          gear_stop_dimming(pCtrlInst);
        }
        pCtrlInst->gearVariables.powerCycleSeen = FALSE;

        if (DALI_MASK == pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx])
        {
          break;
        }
        pCtrlInst->gearEnableDAPCRunning = FALSE;
        pCtrlInst->gearPowerOnRunning = FALSE;
        pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx];
        pCtrlInst->gearVariables.limitError = FALSE;

        futureFadeTime = gear_get_future_fade_time(pCtrlInst, &useFutureFadeTime);
        if (pCtrlInst->gearVariables.targetLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
            pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          pCtrlInst->gearVariables.limitError = TRUE;
        }
        if (0x00 == pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx])
        {
          // dimmin-time will be calculate without time between minLevel and 0x00
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
          pCtrlInst->gearDimm.afterDimmOff = 1;
        }
        if (pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] < pCtrlInst->gearPersistent.gearPersVariables.minLevel &&
            pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] >= 0x01)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
        }

        if (pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] > pCtrlInst->gearPersistent.gearPersVariables.maxLevel &&
            pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx] <= 0xFE)
        {
          pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
        }

        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel  = pCtrlInst->gearVariables.targetLevel;
        }

        if (0 == pCtrlInst->gearPersistent.gearPersVariables.fadeTime &&
            0 == useFutureFadeTime &&
            0 == futureFadeTime &&
            0 == pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
        {
          gear_reaction(pCtrlInst,
                        DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                        (pCtrlInst->gearDimm.afterDimmOff ? 0 :
                          gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel)));
        }
        else
        {
          gear_start_dimming(pCtrlInst,
                             DIMM_TYPE_DIMMTIME,
                             pCtrlInst->gearVariables.targetLevel > pCtrlInst->gearVariables.actualLevel ?
                               DIMM_DIRECTION_UP : DIMM_DIRECTION_DOWN,
                             useFutureFadeTime,
                             futureFadeTime);
        }
      }
    break;

    default:
      {
        rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
      }
    break;
  }

  return(rc);
}


//----------------------------------------------------------------------
// process the forward status frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_querys(PCtrlGearInstance     pCtrlInst,
                                                    ctrl_gear_commands_t* pGearCmd,
                                                    uint8_t*              pRespValue,
                                                    uint32_t              data,
                                                    gear_adr_t*           pReceivedAdr)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t          nMemBank;
  uint8_t          doIncrement;

  switch(pGearCmd->eGearCmd)
  {
    // control gear status frames
    case GEAR_CMD_QUERY_STATUS:
    case GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT:
    case GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE:
    case GEAR_CMD_QUERY_LAMP_FAILURE:
    case GEAR_CMD_QUERY_LAMP_POWER_ON:
    case GEAR_CMD_QUERY_LIMIT_ERROR:
    case GEAR_CMD_QUERY_RESET_STATE:
    case GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_POWER_FAILURE:
      {
        rc = gear_process_forward_status(pCtrlInst, pGearCmd, pRespValue, pReceivedAdr);
      }
    break;
    
    case GEAR_CMD_QUERY_VERSION_NUMBER:
      {
        *pRespValue = pCtrlInst->gearMemBank0.memBank0[0x16];
      }
    break;
    
    case GEAR_CMD_QUERY_CONTENT_DTR0:
      {
        *pRespValue = pCtrlInst->gearVariables.DTR0;
      }
    break;
    
    case GEAR_CMD_QUERY_DEVICE_TYPE:
      {
        // check if control gear extensions present
        #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
          *pRespValue = GEAR_TYPE_SUPPORT_MORE_ONE_DEVICE_TYPE;
          pCtrlInst->gearDeviceTypeQuery.adr = *pReceivedAdr;
          pCtrlInst->gearDeviceTypeQuery.idx = 0;
        #else
          // single device type present
          *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType;
        #endif /* DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT */
      }
    break;

    case GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType;
      }
    break;
    
    case GEAR_CMD_QUERY_NEXT_DEVICE_TYPE:
      {
        // check whether any control gear extensions present
        #ifndef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
          // single device type present
          rc = R_IGNORE_FRAME;
        #else
          // check address byte
          if ((pCtrlInst->gearDeviceTypeQuery.adr.adrType != pReceivedAdr->adrType) ||
              (pCtrlInst->gearDeviceTypeQuery.adr.adr != pReceivedAdr->adr))
          {
            // not directly preceded by QUERY DEVICE TYPE or QUERY NEXT DEVICE TYPE
            rc = R_IGNORE_FRAME;
          }
          else
          {
            // gather device types
            uint8_t          deviceTypes[MAX_DEVICE_TYPES];

            memset(&deviceTypes, GEAR_TYPE_NO_FURTHER_DEVICE_TYPE, sizeof(deviceTypes));
            gear_add_device_type(pCtrlInst, deviceTypes, sizeof(deviceTypes), pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType);
            #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
              gear_add_device_type(pCtrlInst, deviceTypes, sizeof(deviceTypes), GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY);
            #endif // DALILIB_GEAR_POWER_SUPPLY_PRESENT
            #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
              gear_add_device_type(pCtrlInst, deviceTypes, sizeof(deviceTypes), GEAR_TYPE_CONTROL_MEM_BANK1_EXT_SUPPLY);
            #endif // DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
            #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
              gear_add_device_type(pCtrlInst, deviceTypes, sizeof(deviceTypes), GEAR_TYPE_CONTROL_GEAR_ENERGY_REPORTING);
            #endif // DALILIB_GEAR_ENERGY_REPORT_PRESENT

            if ((MAX_DEVICE_TYPES <= pCtrlInst->gearDeviceTypeQuery.idx) ||
                (GEAR_TYPE_NO_FURTHER_DEVICE_TYPE ==
                  deviceTypes[pCtrlInst->gearDeviceTypeQuery.idx]))
            {
              // all device types already reported
              *pRespValue = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
            }
            else
            {
              // report next device type
              *pRespValue = deviceTypes[pCtrlInst->gearDeviceTypeQuery.idx];
              // increment extended device type index
              pCtrlInst->gearDeviceTypeQuery.idx++;
            }
          }
        #endif /* !DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT */
      }
    break;
    
    case GEAR_CMD_QUERY_PHYSICAL_MINIMUM:
      {
        *pRespValue = (uint8_t)ceil(pCtrlInst->gearPersistent.gearPersVariables.phm);
      }
    break;
    
    case GEAR_CMD_QUERY_CONTENT_DTR1:
      {
        *pRespValue = pCtrlInst->gearVariables.DTR1;
      }
    break;
    
    case GEAR_CMD_QUERY_CONTENT_DTR2:
      {
        *pRespValue = pCtrlInst->gearVariables.DTR2;
      }
    break;
    
    case GEAR_CMD_QUERY_OPERATING_MODE:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.operatingMode;
      }
    break;
    
    case GEAR_CMD_QUERY_ACTUAL_LEVEL:
      {
        if (TRUE == pCtrlInst->gearIdentifyRunning)
        {
          *pRespValue = pCtrlInst->gearVariables.targetLevel;
          break;
        }
        // get from application current actualLevel
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_ACTUAL_LEVEL,
                      NOT_USED_PARAM);

        if (pCtrlInst->gearVariables.lampFailure)
        {
          *pRespValue = DALI_MASK;
        }
        else
        {
          *pRespValue = pCtrlInst->gearVariables.actualLevel;
        }
      }
    break;
    
    case GEAR_CMD_QUERY_MAX_LEVEL:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.maxLevel;
      }
    break;
    
    case GEAR_CMD_QUERY_MIN_LEVEL:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.minLevel;
      }
    break;
    
    case GEAR_CMD_QUERY_POWER_ON_LEVEL:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel;
      }
    break;
    
    case GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel;
      }
    break;
    
    case GEAR_CMD_QUERY_FADE_TIME_RATE:
      {
        uint8_t time_rate = 0x00;

        time_rate = pCtrlInst->gearPersistent.gearPersVariables.fadeTime;
        time_rate <<= 4;
        time_rate |= pCtrlInst->gearPersistent.gearPersVariables.fadeRate;
        *pRespValue = time_rate;
      }
    break;
    
    case GEAR_CMD_QUERY_EXTENDED_FADE_TIME:
      {
        uint8_t ex_fade_time = 0x00;

        ex_fade_time = pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple;
        ex_fade_time <<= 4;
        ex_fade_time |= (pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase == 0 ? 0 :
          pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase);
        *pRespValue = ex_fade_time;
      }
    break;
    
    case GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER:
      {
        rc = R_IGNORE_FRAME;
        switch(pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
        {
          case GEAR_TYPE_LED_MODULES:
            if (pCtrlInst->gearVariables.enableDeviceType == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
            {
              *pRespValue = pCtrlInst->gearPersistent.ledPersVariables.extendedVersionNumber;
              rc = R_DALILIB_OK;
            }
          break;

          case GEAR_TYPE_SWITCHING_FUNCTION:
            if (pCtrlInst->gearVariables.enableDeviceType == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
            {
              *pRespValue = pCtrlInst->gearPersistent.switchingPersVariables.extendedVersionNumber;
              rc = R_DALILIB_OK;
            }
          break;

          case GEAR_TYPE_FLUORESCENT_LAMPS:
            if (pCtrlInst->gearVariables.enableDeviceType == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
            {
              *pRespValue = pCtrlInst->gearPersistent.fluorescentPersVariables.extendedVersionNumber;
              rc = R_DALILIB_OK;
            }
          break;

          default:
            //ignore frame
          break;
        }
        pCtrlInst->gearVariables.enableDeviceType = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
      }
    break;
    
    case GEAR_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE:
      {
        if (0x80 < pCtrlInst->gearPersistent.gearPersVariables.operatingMode )
        {
          *pRespValue = DALI_YES;
        }
        else
        {
          rc = R_IGNORE_FRAME;
        }
      }
    break;
    
    case GEAR_CMD_QUERY_SCENE_LEVEL:
      {
        uint8_t sceneCmd = (data & DALI_CTRL_GEAR_FF_SCENENR_MASK);
        uint8_t sceneIdx = sceneCmd - pGearCmd->opcodeByte;

        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.scene[sceneIdx];
      }
    break;
    
    case GEAR_CMD_QUERY_GROUPS_0_7:
      {
        *pRespValue = pCtrlInst->gearPersistent.gearPersVariables.gearGroups;
      }
    break;
    
    case GEAR_CMD_QUERY_GROUPS_8_15:
      {
        *pRespValue = (pCtrlInst->gearPersistent.gearPersVariables.gearGroups>>8);
      }
    break;
    
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_H:
      {
        *pRespValue = (pCtrlInst->gearPersistent.gearPersVariables.randomAddress >> 16 & 0xFF);
      }
    break;

    case GEAR_CMD_QUERY_RANDOM_ADDRESS_M:
      {
        *pRespValue = (pCtrlInst->gearPersistent.gearPersVariables.randomAddress >> 8 & 0xFF);
      }
    break;

    case GEAR_CMD_QUERY_RANDOM_ADDRESS_L:
      {
        *pRespValue = (pCtrlInst->gearPersistent.gearPersVariables.randomAddress & 0xFF);
      }
    break;

    case GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0:
      {
        nMemBank = pCtrlInst->gearVariables.DTR1;
        doIncrement = TRUE;

        // check memory bank
        switch(nMemBank)
        {
          case 0:
            if ((0x01 == pCtrlInst->gearVariables.DTR0) ||  // reserved, answer NO
                (pCtrlInst->gearVariables.DTR0 > pCtrlInst->gearMemBank0.memBank0[0]))
            {
              // cell not implemented: ignore frame, increment DTR0
              rc = R_IGNORE_FRAME;
            }
            else
            {
              *pRespValue = pCtrlInst->gearMemBank0.memBank0[pCtrlInst->gearVariables.DTR0];
            }
          break;

          case CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
              rc = gear_mem_bank1_ext_read_memory_bank1(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;


          case CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
              rc = gear_power_supply_read_memory_bank201(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
              rc = gear_energy_reporting_read_memory_bank202(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_FIRST_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_read_memory_bank205(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_SECOND_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_read_memory_bank206(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          case CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER:
            #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
              rc = gear_diag_maintenance_read_memory_bank207(pCtrlInst, pRespValue);
            #else
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            #endif
          break;

          default:
            if ((MAXMEMBANKS <= nMemBank) ||
                (!pCtrlInst->gearMemBanksOther.is_used[nMemBank]))
            {
              // bank not implemented: ignore frame, don't increment DTR0
              rc = R_IGNORE_FRAME;
              doIncrement = FALSE;
            }
            else
            {
              if (pCtrlInst->gearVariables.DTR0 > pCtrlInst->gearMemBanksOther.memBank[nMemBank][0])
              {
                // cell not implemented: ignore frame, increment DTR0
                rc = R_IGNORE_FRAME;
              }
              else
              {
                *pRespValue = pCtrlInst->gearMemBanksOther.memBank[nMemBank][pCtrlInst->gearVariables.DTR0];
              }
            }
          break;
        }

        // increment DTR0
        if (doIncrement &&
            pCtrlInst->gearVariables.DTR0 < 0xFF)
        {
          pCtrlInst->gearVariables.DTR0 += 1;
        }
      }
    break;

    default:
      {
        rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
      }
    break;
  }

  return (rc);
}
/**********************************************************************
 * Functions for the control device (application controller)
 **********************************************************************/

//----------------------------------------------------------------------
// actions between application and DALI-Library
// triggered from application
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_internal(PCtrlGearInstance        pCtrlInst,
                                             dalilib_act_ctrl_gear_t* pAct,
                                             send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT          rc = R_INTERN_ACTION;
  dali_ctrl_gear_pers_mem_t persistentMemory;
  uint8_t                   daliMem[MEM_DATA_MAX_SIZE]; // buffer of persistent data memory block
  uint8_t                   actionLevel;
  dalilib_action_t          action;

  (void)pNewSendFrame;

  switch(pAct->action.internalAction)
  {
    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAMPFAILURE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAMPFAILURE,
                    pCtrlInst->gearVariables.lampFailure);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAMPFAILURE:
      //start timer polling lamp status
      pCtrlInst->gearLampFailureStatusTimer = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      pCtrlInst->gearVariables.lampFailure = (pAct->value.actionValue) ? TRUE : FALSE;
      pCtrlInst->gearVariables.lampOn = FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_ACTUAL_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_ACTUAL_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.actualLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      // check value range
      if (actionLevel != 0 &&
          (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
           actionLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel))
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->gearVariables.lampOn = actionLevel ? TRUE : FALSE;
        pCtrlInst->gearVariables.actualLevel = actionLevel;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL_RAW_VALUE:
      // check value range
      if (pAct->value.rawValue != 0 &&
          (pAct->value.rawValue < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
           pAct->value.rawValue > pCtrlInst->gearPersistent.gearPersVariables.maxLevel))
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->gearVariables.lampOn = pAct->value.rawValue ? TRUE : FALSE;
        pCtrlInst->gearVariables.actualLevel = pAct->value.rawValue;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_GEAR_FAILURE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_GEAR_FAILURE,
                    pCtrlInst->gearVariables.controlGearFailure);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_GEAR_FAILURE:
      pCtrlInst->gearVariables.controlGearFailure = (pAct->value.actionValue) ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_POWER_ON_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_POWER_ON_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_POWER_ON_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel)
      {
        pCtrlInst->gearPersistent.gearPersVariables.powerOnLevel = actionLevel;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SYSTEM_FAILURE_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SYSTEM_FAILURE_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SYSTEM_FAILURE_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel)
      {
        pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel = actionLevel;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAST_LIGHT_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_LIGHT_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAST_LIGHT_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
      {
        // check value range
        if (actionLevel != 0 &&
            (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
             actionLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel))
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = actionLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_TARGET_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_TARGET_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_TARGET_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      // check value range
      if (actionLevel != 0 &&
          (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
           actionLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel))
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->gearVariables.targetLevel = actionLevel;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LAST_ACTIVE_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LAST_ACTIVE_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.lastActiveLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LAST_ACTIVE_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      // check value range
      if (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
          actionLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->gearVariables.lastActiveLevel = actionLevel;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_HIGH_RES_DIMMING:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_HIGH_RES_DIMMING,
                    pCtrlInst->gearPersistent.gearPersVariables.highResDimming);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_HIGH_RES_DIMMING:
      if ((!pAct->value.actionValue && pCtrlInst->gearPersistent.gearPersVariables.highResDimming) ||
          (pAct->value.actionValue && !pCtrlInst->gearPersistent.gearPersVariables.highResDimming))
      {
        pCtrlInst->gearPersistent.gearPersVariables.highResDimming = pAct->value.actionValue ? TRUE : FALSE;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_MIN_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MIN_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearPersistent.gearPersVariables.minLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_MIN_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.minLevel)
      {
        // check value range
        if (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.phm ||
            actionLevel > pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.minLevel = actionLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_MAX_LEVEL:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_MAX_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearPersistent.gearPersVariables.maxLevel));
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_MAX_LEVEL:
      actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.actionValue);
      if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
      {
        // check value range
        if (actionLevel < pCtrlInst->gearPersistent.gearPersVariables.minLevel ||
            actionLevel == DALI_MASK)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.maxLevel = actionLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_RATE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_RATE,
                    pCtrlInst->gearPersistent.gearPersVariables.fadeRate);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_RATE:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.fadeRate)
      {
        // check value range
        if (15 < (uint8_t)pAct->value.actionValue ||
            1  > (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.fadeRate = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_TIME:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_TIME,
                    pCtrlInst->gearPersistent.gearPersVariables.fadeTime);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_TIME:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.fadeTime)
      {
        // check value range
        if (15 < (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.fadeTime = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_BASE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_BASE,
                    pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_EXT_FADE_TIME_BASE:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase)
      {
        // check value range
        if (15 < (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeBase = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_MULTIPLE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_EXT_FADE_TIME_MULTIPLE,
                    pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_EXT_FADE_TIME_MULTIPLE:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple)
      {
        // check value range
        if (4 < (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.extFadeTimeMultiple = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_OPERATING_MODE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_OPERATING_MODE,
                    pCtrlInst->gearPersistent.gearPersVariables.operatingMode);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_OPERATING_MODE:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.operatingMode)
      {
        // check value range
        if (0x00 < (uint8_t)pAct->value.actionValue &&
            0x80 > (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.operatingMode = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SHORT_ADDRESS:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SHORT_ADDRESS,
                    pCtrlInst->gearPersistent.gearPersVariables.shortAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SHORT_ADDRESS:
      if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.shortAddress)
      {
        // check value range
        if (0x63       < (uint8_t)pAct->value.actionValue &&
            DALI_MASK != (uint8_t)pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.shortAddress = (uint8_t)pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_RANDOM_ADDRESS:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_RANDOM_ADDRESS,
                    pCtrlInst->gearPersistent.gearPersVariables.randomAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_RANDOM_ADDRESS:
      if (pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.randomAddress)
      {
        // check value range
        if (0x00FFFFFF < pAct->value.actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->gearPersistent.gearPersVariables.randomAddress = pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SEARCH_ADDRESS:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SEARCH_ADDRESS,
                    pCtrlInst->gearVariables.searchAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SEARCH_ADDRESS:
      // check value range
      if (0x00FFFFFF < pAct->value.actionValue)
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->gearVariables.searchAddress = pAct->value.actionValue;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_GEAR_GROUPS:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_GEAR_GROUPS,
                    pCtrlInst->gearPersistent.gearPersVariables.gearGroups);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_GEAR_GROUPS:
      if ((uint16_t)pAct->value.actionValue != pCtrlInst->gearPersistent.gearPersVariables.gearGroups)
      {
        pCtrlInst->gearPersistent.gearPersVariables.gearGroups = (uint16_t)pAct->value.actionValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_SCENE:
      // check index range
      if (DALI_CTRL_GEAR_MAX_SCENES <= pAct->value.sceneParams.sceneIndex)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_SCENE,
                      pCtrlInst->gearPersistent.gearPersVariables.scene[pAct->value.sceneParams.sceneIndex]);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_SCENE:
      // check index range
      if (DALI_CTRL_GEAR_MAX_SCENES <= pAct->value.sceneParams.sceneIndex)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        actionLevel = gear_percent2level(getDimmingCurve(pCtrlInst), pAct->value.sceneParams.sceneLevel);
        if (actionLevel != pCtrlInst->gearPersistent.gearPersVariables.scene[pAct->value.sceneParams.sceneIndex])
        {
          pCtrlInst->gearPersistent.gearPersVariables.scene[pAct->value.sceneParams.sceneIndex] = actionLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_INITIALISATION_STATE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_INITIALISATION_STATE,
                    pCtrlInst->gearVariables.initialisationState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_INITIALISATION_STATE:
      switch (pAct->value.actionValue)
      {
        case CTRL_GEAR_INITIALISE_STATE_DISABLED:
          pCtrlInst->gearVariables.initialisationState = pAct->value.actionValue;
          // stop the initialisation state
          pCtrlInst->gearInitialiseStartTime = 0;
        break;

        case CTRL_GEAR_INITIALISE_STATE_ENABLED:
          pCtrlInst->gearVariables.initialisationState = pAct->value.actionValue;
          // start or prolong the initialisation state
          pCtrlInst->gearInitialiseStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        break;

        case CTRL_GEAR_INITIALISE_STATE_WITHDRAWN:
          pCtrlInst->gearVariables.initialisationState = pAct->value.actionValue;
        break;

        default:
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        break;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_WRITE_ENABLE_STATE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_WRITE_ENABLE_STATE,
                    pCtrlInst->gearVariables.writeEnableState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_WRITE_ENABLE_STATE:
      pCtrlInst->gearVariables.writeEnableState = pAct->value.actionValue ?
          CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED : CTRL_GEAR_WRITE_ENABLE_STATE_DISABLED;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_LIMIT_ERROR:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_LIMIT_ERROR,
                    pCtrlInst->gearVariables.limitError);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_LIMIT_ERROR:
      pCtrlInst->gearVariables.limitError = pAct->value.actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_FADE_RUNNING:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_FADE_RUNNING,
                    pCtrlInst->gearVariables.fadeRunning);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_FADE_RUNNING:
      pCtrlInst->gearVariables.fadeRunning = pAct->value.actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_RESET_STATE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_RESET_STATE,
                    pCtrlInst->gearVariables.resetState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_RESET_STATE:
      pCtrlInst->gearVariables.resetState = pAct->value.actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_POWER_CYCLE_SEEN:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_POWER_CYCLE_SEEN,
                    pCtrlInst->gearVariables.powerCycleSeen);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_POWER_CYCLE_SEEN:
      pCtrlInst->gearVariables.powerCycleSeen = pAct->value.actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR0:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR0,
                    pCtrlInst->gearVariables.DTR0);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR0:
      pCtrlInst->gearVariables.DTR0 = (uint8_t)pAct->value.actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR1:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR1,
                    pCtrlInst->gearVariables.DTR1);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR1:
      pCtrlInst->gearVariables.DTR1 = (uint8_t)pAct->value.actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DTR2:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_DTR2,
                    pCtrlInst->gearVariables.DTR2);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_DTR2:
      pCtrlInst->gearVariables.DTR2 = (uint8_t)pAct->value.actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_ENABLE_DEVICE_TYPE:
      // notify application
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_GET_ENABLE_DEVICE_TYPE,
                    pCtrlInst->gearVariables.enableDeviceType);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SET_ENABLE_DEVICE_TYPE:
      pCtrlInst->gearVariables.enableDeviceType = pAct->value.actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_FAST_FADE_TIME:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_FAST_FADE_TIME,
                      pCtrlInst->gearPersistent.ledPersVariables.fft);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_FAST_FADE_TIME:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.ledPersVariables.fft)
        {
          // check value range
          if (0 != (uint8_t)pAct->value.actionValue &&
              (27 < (uint8_t)pAct->value.actionValue ||
               pCtrlInst->gearPersistent.ledPersVariables.min_fft > (uint8_t)pAct->value.actionValue))
          {
            rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
          }
          else
          {
            pCtrlInst->gearPersistent.ledPersVariables.fft = (uint8_t)pAct->value.actionValue;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_DIMMING_CURVE:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_DIMMING_CURVE,
                      pCtrlInst->gearPersistent.ledPersVariables.dimmingCurve);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_DIMMING_CURVE:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        if (DIMMING_CURVE_LINEAR >= pAct->value.actionValue)
        {
          if (pCtrlInst->gearPersistent.ledPersVariables.dimmingCurve != pAct->value.actionValue)
          {
            pCtrlInst->gearPersistent.ledPersVariables.dimmingCurve = (DIMMING_CURVE)pAct->value.actionValue;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_FAILURE_STATUS:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_FAILURE_STATUS,
                      pCtrlInst->gearPersistent.ledPersVariables.failureStatus);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_SET_FAILURE_STATUS:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        if ((uint8_t)pAct->value.actionValue != pCtrlInst->gearPersistent.ledPersVariables.failureStatus)
        {
          // check value range
          if (0   != (uint8_t)pAct->value.actionValue &&
              12  != (uint8_t)pAct->value.actionValue &&
              32  != (uint8_t)pAct->value.actionValue &&
              44  != (uint8_t)pAct->value.actionValue &&
              64  != (uint8_t)pAct->value.actionValue &&
              76  != (uint8_t)pAct->value.actionValue &&
              96  != (uint8_t)pAct->value.actionValue &&
              108 != (uint8_t)pAct->value.actionValue &&
              128 != (uint8_t)pAct->value.actionValue &&
              140 != (uint8_t)pAct->value.actionValue &&
              160 != (uint8_t)pAct->value.actionValue &&
              172 != (uint8_t)pAct->value.actionValue &&
              192 != (uint8_t)pAct->value.actionValue &&
              204 != (uint8_t)pAct->value.actionValue &&
              224 != (uint8_t)pAct->value.actionValue &&
              236 != (uint8_t)pAct->value.actionValue)
          {
            rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
          }
          else
          {
            pCtrlInst->gearPersistent.ledPersVariables.failureStatus = (uint8_t)pAct->value.actionValue;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_GET_REFERENCE_VALUE:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_LED_GET_REFERENCE_VALUE,
                      pCtrlInst->gearPersistent.ledPersVariables.referenceValue);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_LED_REFERENCE_VALUE:
      if (GEAR_TYPE_LED_MODULES != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        if (pAct->value.actionValue != pCtrlInst->gearPersistent.ledPersVariables.referenceValue)
        {
          pCtrlInst->gearPersistent.ledPersVariables.referenceValue = pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SWITCHING_GET_REFERENCE_VALUE:
      if (GEAR_TYPE_SWITCHING_FUNCTION != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        // notify application
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SWITCHNG_GET_REFERENCE_VALUE,
                      pCtrlInst->gearPersistent.ledPersVariables.referenceValue);
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SWITCHING_REFERENCE_VALUE:
      if (GEAR_TYPE_SWITCHING_FUNCTION != pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
      }
      else
      {
        if (pAct->value.actionValue != pCtrlInst->gearPersistent.switchingPersVariables.referenceValue)
        {
          pCtrlInst->gearPersistent.switchingPersVariables.referenceValue = pAct->value.actionValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_SAVE_PERSISTENT:
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK |
                                               DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK |
                                               DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK |
                                               DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK |
                                               DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK |
                                               DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK |
                                               DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK |
                                               DALI_CTRL_GEAR_PERS_COLOUR_MASK |
                                               DALI_CTRL_GEAR_PERS_SWITCHING_MASK |
                                               DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK |
                                               DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK |
                                               DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK |
                                               DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK |
                                               DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK |
                                               DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
      gear_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->gearPersistent);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_COLOUR_STOP_CALIBRATION:
      pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationsucceeded = TRUE;
      pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationRunning   = FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_WRITE_MEMORY_CELL:
      switch(pAct->value.memoryParams.memoryBankNr)
      {
        case 0:
          // Membank 0 is readonly
          rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
        break;

        case CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
            if (R_IGNORE_FRAME == gear_mem_bank1_ext_write_memory_bank1_direct(pCtrlInst,
                                                                               pAct->value.memoryParams.memoryCellIdx,
                                                                               pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        case CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
            if (R_IGNORE_FRAME == gear_power_supply_write_memory_bank201_direct(pCtrlInst,
                                                                                pAct->value.memoryParams.memoryCellIdx,
                                                                                pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        case CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
            if (R_IGNORE_FRAME == gear_energy_reporting_write_memory_bank202_direct(pCtrlInst,
                                                                                    pAct->value.memoryParams.memoryCellIdx,
                                                                                    pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        case CTRL_GEAR_DIAG_MAINTENANCE_FIRST_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
            if (R_IGNORE_FRAME == gear_diag_maintenance_write_memory_bank205_direct(pCtrlInst,
                                                                                    pAct->value.memoryParams.memoryCellIdx,
                                                                                    pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        case CTRL_GEAR_DIAG_MAINTENANCE_SECOND_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
            if (R_IGNORE_FRAME == gear_diag_maintenance_write_memory_bank206_direct(pCtrlInst,
                                                                                    pAct->value.memoryParams.memoryCellIdx,
                                                                                    pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        case CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
            if (R_IGNORE_FRAME == gear_diag_maintenance_write_memory_bank207_direct(pCtrlInst,
                                                                                    pAct->value.memoryParams.memoryCellIdx,
                                                                                    pAct->value.memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        default:
          // application already has pointer to user defined memory banks
          rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
        break;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_GEAR_GET_DEFAULT_PERS_MEMORY:
      // get default values of persistent memory
      rc = gear_init_persistent(pCtrlInst->pInst, &persistentMemory);
      // store in buffer
      if (R_DALILIB_OK == rc)
      {
        rc = dali_create_mem(pCtrlInst->pInst, &persistentMemory, sizeof(persistentMemory), daliMem);
      }
      // notify application
      if (R_DALILIB_OK == rc)
      {
        memset(&action, 0, sizeof(action));
        action.actionType = DALILIB_CTRL_GEAR_REACTION;
        action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_DEFAULT_PERS_MEMORY;
        action.gearReact.persMemory.pMemory = &daliMem;
        action.gearReact.persMemory.nSize = MEM_DATA_MAX_SIZE;
        action.gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
        dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear config
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_config(PDaliLibInstance         pInst,
                                           dalilib_act_ctrl_gear_t* pAct,
                                           send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.configAction)
  {
    case DALILIB_ACT_CTRL_GEAR_CONFIG_RESET:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_RESET, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SAVE_PERS_VARIABLES:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_SAVE_PERSISTENT_VARIABLES, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_OPERATING_MODE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_OPERATING_MODE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_MAX_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.actionValue));
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_MAX_LEVEL;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_MIN_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.actionValue));
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_MIN_LEVEL;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_SYSTEM_FAILURE_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.actionValue));
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_POWER_ON_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.actionValue));
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_POWER_ON_LEVEL;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_FADE_TIME:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_FADE_TIME;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_FADE_RATE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_FADE_RATE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_CONFIG_SET_EXT_FADE_TIME:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_EXTENDED_FADE_TIME;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_RESET_MEMORY_BANK:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_RESET_MEMORY_BANK;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear level
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_level(PDaliLibInstance         pInst,
                                          dalilib_act_ctrl_gear_t* pAct,
                                          send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.levelAction)
  {
    case DALILIB_ACT_CTRL_GEAR_LEVEL_DAPC:
      {
        /* rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DAPC_SEQUENCE, pNewSendFrame, &pAct->adr, 0 );
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DAPC;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = gear_percent2level(pAct->value.actionValue);
        */
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DAPC, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.actionValue));
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_DAPC_RAW_VALUE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DAPC, pNewSendFrame, &pAct->adr, pAct->value.rawValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_OFF:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_OFF, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_UP:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_UP, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_DOWN:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DOWN, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_UP:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_STEP_UP, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_DOWN:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_STEP_DOWN, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_RECALL_MAX_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_RECALL_MAX_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_RECALL_MIN_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_RECALL_MIN_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_STEP_DOWN_AND_OFF:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_STEP_DOWN_AND_OFF, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_ON_STEP_UP:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_ON_AND_STEP_UP, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LEVEL_GOTO_LAST_ACTIVE_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear scene
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_scene(PDaliLibInstance         pInst,
                                          dalilib_act_ctrl_gear_t* pAct,
                                          send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.sceneAction)
  {
    case DALILIB_ACT_CTRL_GEAR_SCENE_GOTO_SCENE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_GO_TO_SCENE, pNewSendFrame,
                            &pAct->adr, pAct->value.sceneParams.sceneIndex);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SCENE_SET_SCENE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            gear_percent2level(DIMMING_CURVE_STANDARD, pAct->value.sceneParams.sceneLevel));
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_SET_SCENE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = pAct->value.sceneParams.sceneIndex;
        pNewSendFrame->cmdCounter++;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SCENE_REMOVE_FROM_SCENE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_REMOVE_FROM_SCENE, pNewSendFrame, &pAct->adr,
                            pAct->value.sceneParams.sceneIndex);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear group
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_group(PDaliLibInstance         pInst,
                                          dalilib_act_ctrl_gear_t* pAct,
                                          send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.groupAction)
  {
    case DALILIB_ACT_CTRL_GEAR_GROUP_ADD_TO_GROUP:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_ADD_TO_GROUP, pNewSendFrame, &pAct->adr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_GROUP_REMOVE_FROM_GROUP:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_REMOVE_FROM_GROUP, pNewSendFrame, &pAct->adr,
                            pAct->value.actionValue);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear queries
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_queries(PDaliLibInstance         pInst,
                                            dalilib_act_ctrl_gear_t* pAct,
                                            send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.queriesAction)
  {
    case DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_PRESENT:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_MISSING_SHORT_ADDRESS:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_VERSION_NR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_VERSION_NUMBER, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR0:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_CONTENT_DTR0, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_DEVICE_TYPE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_DEVICE_TYPE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_PHYSICAL_MINIMUM:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_PHYSICAL_MINIMUM, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR1:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_CONTENT_DTR1, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CONTENT_DTR2:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_CONTENT_DTR2, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_OPERATING_MODE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_OPERATING_MODE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_LIGHT_SOURCE_TYPE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_FADE_TIME_RATE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_FADE_TIME_RATE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_CTRL_GEAR_NEXT_DEVICE_TYPE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_NEXT_DEVICE_TYPE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_EXT_FADE_TIME:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_EXTENDED_FADE_TIME, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_SCENE_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_SCENE_LEVEL, pNewSendFrame, &pAct->adr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_GROUP:
      {
        if (pAct->value.actionValue <= 7 )
        {
          rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_GROUPS_0_7, pNewSendFrame, &pAct->adr, 0 );
        }
        else
        {
          rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_GROUPS_8_15, pNewSendFrame, &pAct->adr, 0 );
        }
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_ACTUAL_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_ACTUAL_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_MAX_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_MAX_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_MIN_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_MIN_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_POWER_ON_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_POWER_ON_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_SYSTEM_FAILURE_LEVEL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_H:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_RANDOM_ADDRESS_H, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_M:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_RANDOM_ADDRESS_M, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_QUERY_RANDOM_ADDRESS_L:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_RANDOM_ADDRESS_L, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear memory bank 0
// contains about manufacturer information
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_vendor(PDaliLibInstance         pInst,
                                           dalilib_act_ctrl_gear_t* pAct,
                                           send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.vendorAction)
  {
    case DALILIB_ACT_CTRL_GEAR_VENDOR_GTIN:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 3);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MAJOR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 9);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_FW_VER_MINOR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 10);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_IDENTIFY_NR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 11);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MAJOR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x13);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_HW_VER_MINOR:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x14);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_VENDOR_DALI_NORM_VERSION:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x16);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear memory bank cell
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_memory(PDaliLibInstance         pInst,
                                           dalilib_act_ctrl_gear_t* pAct,
                                           send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->action.vendorAction)
  {
    case DALILIB_ACT_CTRL_GEAR_READ_MEMORY_CELL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr,
                            pAct->value.memoryParams.memoryCellIdx);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_READ_MEMORY_CELL;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.memoryParams.memoryBankNr;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_READ_NEXT_MEMORY_CELL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_READ_MEMORY_CELL, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_WRITE_MEMORY_CELL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR1, pNewSendFrame, &pAct->adr,
                            pAct->value.memoryParams.memoryBankNr);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_WRITE_MEMORY_LOCATION;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.memoryParams.value;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR0;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.memoryParams.memoryCellIdx;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_WRITE_MEMORY_LOCATION;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_WRITE_MEMORY;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR0;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_WRITE_NEXT_MEMORY_CELL:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_WRITE_MEMORY_LOCATION, pNewSendFrame, &pAct->adr,
                            pAct->value.memoryParams.value);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for control gear initialise
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_initialise(PDaliLibInstance         pInst,
                                               dalilib_act_ctrl_gear_t* pAct,
                                               send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  gear_adr_t dummyAdr;
  dummyAdr.adrType = DALI_ADRTYPE_SHORT;
  dummyAdr.adr = 0x00;

  switch(pAct->action.initAction)
  {
    case DALILIB_ACT_CTRL_GEAR_INIT_TERMINATE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_TERMINATE, pNewSendFrame, &dummyAdr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_INITIALISE:
      {
        uint8_t value = 0;
        switch(pAct->value.actionValue)
        {
          case DALILIB_CTRL_GEAR_INIT_WITH_SHORTADDRESS:
            value = pAct->adr.adr;
          break;
          
          case DALILIB_CTRL_GEAR_INIT_WITHOUT_SHORTADDRESS:
            value = 0xFF;
          break;
          
          case DALILIB_CTRL_GEAR_INIT_ALL:
            value = 0x00;
          break;
          
          default:
            value = pAct->value.actionValue;
          break;
        }
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_INITIALISE, pNewSendFrame, &pAct->adr, value);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_RANDOMISE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_RANDOMISE, pNewSendFrame, &dummyAdr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_WITHDRAW:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_WITHDRAW, pNewSendFrame, &dummyAdr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_H:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_SEARCHADDRH, pNewSendFrame, &dummyAdr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_M:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_SEARCHADDRM, pNewSendFrame, &dummyAdr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_SEARCHADDR_L:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_SEARCHADDRL, pNewSendFrame, &dummyAdr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_SET_SHORT_ADDRESS:
      {
        uint8_t new_adr = pAct->value.actionValue;
        new_adr <<= 1;
        new_adr |= 0x01;
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, new_adr);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_SET_SHORT_ADDRESS;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_PROG_SHORT_ADDRESS:
      {
        uint8_t new_adr = pAct->value.actionValue;
        new_adr <<= 1;
        new_adr |= 0x01;
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_PROGRAM_SHORT_ADDRESS, pNewSendFrame, &dummyAdr, new_adr);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_ENABLE_DEVICE_TYPE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DEVICE_TYPE, pNewSendFrame, &pAct->adr,
                            pAct->value.actionValue);
      }
    break;

    // response code for next actions DALILIB_ACT_CTRL_GEAR_INITIALISE_RESPONSE
    case DALILIB_ACT_CTRL_GEAR_INIT_COMPARE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_COMPARE, pNewSendFrame, &dummyAdr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_VERIFY_SHORT_ADDRESS:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_VERIFY_SHORT_ADDRESS, pNewSendFrame, &dummyAdr,
                            pAct->value.actionValue);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_QUERY_SHORT_ADDRESS:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_SHORT_ADDRESS, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_INIT_IDENTIFY_DEVICE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_IDENTIFY_DEVICE, pNewSendFrame, &pAct->adr, 0);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}


//----------------------------------------------------------------------
// actions for fluorescent lamps
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_fluorescent(PDaliLibInstance         pInst,
                                                dalilib_act_ctrl_gear_t* pAct,
                                                send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DEVICE_TYPE, pNewSendFrame,
                      &pAct->adr, GEAR_TYPE_FLUORESCENT_LAMPS);
  pNewSendFrame->cmdCounter = 0;

  if (DALILIB_ACT_CTRL_GEAR_FLUORESCENT_QUERY_EXT_VERSION_NUMBER == pAct->action.fluorescentAction)
  {
    pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for LED Modules
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_led(PDaliLibInstance pInst,
                    dalilib_act_ctrl_gear_t* pAct,
                    send_frame_t* pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DEVICE_TYPE, pNewSendFrame,
                      &pAct->adr, GEAR_TYPE_LED_MODULES);
  pNewSendFrame->cmdCounter = 0;

  switch(pAct->action.ledAction)
  {
    case DALILIB_ACT_CTRL_GEAR_LED_REFERENCE_SYSTEM_POWER:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_REFERENCE_SYSTEM_POWER;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_ENABLE_CURRENT_PROTECTOR:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_ENABLE_CURRENT_PROTECTOR;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_DISABLE_CURRENT_PROTECTOR:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_DISABLE_CURRENT_PROTECTOR;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_SELECT_DIMMING_CURVE:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_SELECT_DIMMING_CURVE;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_LED_MODULES;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_SET_FAST_FADE_TIME:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_STORE_DTR_AS_FFT;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_LED_MODULES;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_GEAR_TYPE:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_GEAR_TYPE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_DIMMING_CURVE:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_DIMMING_CURVE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_POSSIBLE_OPERATING_MODES:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_POSSIBLE_OPERATING_MODES;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_FEATURES:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_FEATURES;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_FAILURE_STATUS:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_FAILRE_STATUS;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_REFERENCE_RUNNING:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_REFERENCE_RUNNING;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_PROTECTOR_ENABLED:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_CURRENT_PROTECTOR_ENABLED;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_OPERATING_MODE:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_OPERATING_MODE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_FAST_FADE_TIME:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_FFT;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_MIN_FAST_FADE_TIME:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_MIN_FFT;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_LED_QUERY_EXT_VERSION_NUMBER:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = LED_QUERY_EXTENDED_VERSION_NUMBER;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for LED Modules
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_colour(PDaliLibInstance         pInst,
                                           dalilib_act_ctrl_gear_t* pAct,
                                           send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DEVICE_TYPE, pNewSendFrame, &pAct->adr,
                      GEAR_TYPE_COLOUR_CONTROL);
  pNewSendFrame->cmdCounter = 0;

  switch(pAct->action.colourAction)
  {
    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_X_COORDINATE:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_XCOORDINATE;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_Y_COORDINATE:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_YCOORDINATE;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_ACTIVATE:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_ACTIVATE;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_X_COORDINATE_STEP_UP:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_XCOORDINATE_STEP_UP;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_X_COORDINATE_STEP_DOWN:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_XCOORDINATE_STEP_DOWN;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_Y_COORDINATE_STEP_UP:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_YCOORDINATE_STEP_UP;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_Y_COORDINATE_STEP_DOWN:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_YCOORDINATE_STEP_DOWN;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_COLOUR_TEMPERATURE:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_COLOUR_TEMPERATURE;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_TEMPERATURE_STEP_COOLER:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_COLOUR_TEMPERATURE_STEP_COOLER;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_TEMPERATURE_STEP_WARMER:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_COLOUR_TEMPERATURE_STEP_WARMER;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_PRIMARY_N_DIMLEVEL:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_PRIMARY_N_DIMLEVEL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR2;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 16;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_RGB_DIMLEVEL:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_RGB_DIMLEVEL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR2;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 16;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_WAF_DIMLEVEL:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_WAF_DIMLVEL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR2;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 16;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_SET_TEMP_RGBWAF_CONTROL:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_SET_TEMP_RGBWAF_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_COPY_REPORT_TO_TEMP:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_COPY_REPORT_TO_TEMP;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_TY_PRIMARY_N:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_STORE_TY_PRIMARY_N;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR2;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 16;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_XY_COORDINATE_PRIMARY_N:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR2, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_STORE_XY_COORDINATE_PRIMARY_N;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_TEMPERATURE_LIMIT:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_STORE_COLOUR_TEMPERATURE_LIMIT;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR2;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 16;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->value.actionValue >> 8;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_STORE_GEAR_FEATURES_STATUS:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_STORE_GEAR_FEATURES_STATUS;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_ASSIGN_COLOUR_TO_LINKED_CHANNEL:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_ASSIGN_COLOUR_TO_LINKED_CHANNEL;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_START_AUTO_CALIBRATION:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_START_AUTO_CALIBRATION;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_GEAR_FEATURES_STATUS:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_GEAR_FEATURES_STATUS;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_STATUS:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_COLOUR_STATUS;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_TYPE_FEATURES:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_COLOUR_TYPE_FEAUTRES;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_COLOUR_VALUE:
      rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_COLOUR_VALUE;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = GEAR_CMD_ENABLE_DEVICE_TYPE;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = GEAR_TYPE_COLOUR_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_RGBWAF_CONTROL:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_RGBWAF_CONTROL;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_ASSIGNED_COLOUR:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_ASSIGNED_COLOUR;
    break;

    case DALILIB_ACT_CTRL_GEAR_COLOUR_QUERY_EXT_VERSION_NUMBER:
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = COLOUR_CMD_QUERY_EXTENTED_VERSION_NUMBER;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// actions for Switching Function
// triggered from application controller
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_action_switching(PDaliLibInstance         pInst,
                                              dalilib_act_ctrl_gear_t* pAct,
                                              send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  rc = gear_create_ff(pInst, NULL, GEAR_CMD_ENABLE_DEVICE_TYPE, pNewSendFrame, &pAct->adr,
                      GEAR_TYPE_SWITCHING_FUNCTION);
  pNewSendFrame->cmdCounter = 0;

  switch(pAct->action.switchingAction)
  {
    case DALILIB_ACT_CTRL_GEAR_SWITCHING_REFERENCE_SYSTEM_POWER:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_REFERENCE_SYSTEM_POWER;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = GEAR_TYPE_SWITCHING_FUNCTION;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = GEAR_TYPE_SWITCHING_FUNCTION;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = GEAR_TYPE_SWITCHING_FUNCTION;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = GEAR_TYPE_SWITCHING_FUNCTION;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->value.actionValue);
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = GEAR_CMD_ENABLE_DEVICE_TYPE;
        pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = GEAR_TYPE_SWITCHING_FUNCTION;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_FEATURES:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_FEATURES;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_SWITCH_STATUS:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_SWITCH_STATUS;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_ERROR_HOLD_OFF_TIME:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_ERROR_HOLD_OFF_TIME;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_GEAR_TYPE:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_GEAR_TYPE;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_REFERENCE_RUNNING:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_REFERENCE_RUNNING;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED;
      }
    break;

    case DALILIB_ACT_CTRL_GEAR_SWITCHING_QUERY_EXTENDED_VERSION_NUMBER:
      {
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = SWITCHING_QUERY_EXTENDED_VERSION_NUMBER;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// initialize the memory bank 0 of the control gear
//----------------------------------------------------------------------
static void gear_init_membank0_data(PDaliLibInstance pInst)
{
  PCtrlGearMemoryBank0           pGearMemBank0        = NULL;

  pGearMemBank0 = &pInst->ctrlInst.gearInstance.gearMemBank0;

  memset(pGearMemBank0, 0, sizeof(pInst->ctrlInst.gearInstance.gearMemBank0));

  // memory bank 0
  pGearMemBank0->ctrlMemBank0.address_last_cell = 0x1A;
  pGearMemBank0->ctrlMemBank0.reserved          = 0x00;
  pGearMemBank0->ctrlMemBank0.number_last_bank  = 0;

  // check device type extensions for memory banks
  #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    pGearMemBank0->ctrlMemBank0.number_last_bank  = CTRL_GEAR_POWER_SUPPLY_MEMORY_BANK_NUMBER;
  #endif
  #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    pGearMemBank0->ctrlMemBank0.number_last_bank  = CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER;
  #endif
  #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    pGearMemBank0->ctrlMemBank0.number_last_bank  = CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER;
  #endif
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    pGearMemBank0->ctrlMemBank0.number_last_bank  = CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER;
  #endif

  memcpy(pGearMemBank0->ctrlMemBank0.gtin,
         pInst->config.vendor.gtin,
         sizeof(pGearMemBank0->ctrlMemBank0.gtin));

  pGearMemBank0->ctrlMemBank0.firmware_ver_major = pInst->config.vendor.firmware_ver_major;
  pGearMemBank0->ctrlMemBank0.firmware_ver_minor = pInst->config.vendor.firmware_ver_minor;

  memcpy(pGearMemBank0->ctrlMemBank0.identify_number,
         pInst->config.vendor.identify_number,
         sizeof(pGearMemBank0->ctrlMemBank0.identify_number));

  pGearMemBank0->ctrlMemBank0.hw_ver_major      = pInst->config.vendor.hw_ver_major;
  pGearMemBank0->ctrlMemBank0.hw_ver_minor      = pInst->config.vendor.hw_ver_minor;

  pGearMemBank0->ctrlMemBank0.ver_101_number   = 0x08;
  pGearMemBank0->ctrlMemBank0.ver_102_number   = 0x08;
  pGearMemBank0->ctrlMemBank0.ver_103_number   = 0x08;

  pGearMemBank0->ctrlMemBank0.number_log_ctrl_device = 0x00;
  pGearMemBank0->ctrlMemBank0.number_log_ctrl_gear   = 0x01;

  pGearMemBank0->ctrlMemBank0.index_log_control_unit   = 0x00;
}

//----------------------------------------------------------------------
// process the received base forward dali frame (Part-102)
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_forward_frame(PCtrlGearInstance      pCtrlInst,
                                                   dalilib_frame_t*       pForwardFrame,
                                                   ctrl_gear_commands_t** ppGearCmd,
                                                   gear_adr_t*            pReceivedAdr,
                                                   uint8_t*               pResponseValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  *ppGearCmd = gear_parse_command(pForwardFrame, pReceivedAdr);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  // check resetState
  pCtrlInst->gearVariables.resetState = gear_check_nvm_variables(&pCtrlInst->gearPersistent,
                                                                 &pCtrlInst->gearVariables);

  // check writeEnableState
  if (CTRL_GEAR_WRITE_ENABLE_STATE_ENABLED == pCtrlInst->gearVariables.writeEnableState)
  {
    pCtrlInst->gearVariables.writeEnableState = gear_check_writeEnableState(&pCtrlInst->gearVariables,
                                                                            (GEAR_CMD)(*ppGearCmd)->eGearCmd);
  }
  // check, reference measurement status
  if (GEAR_TYPE_LED_MODULES == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.ledVariables.referenceRunning)
    {
      if ((*ppGearCmd)->eGearCmd == GEAR_CMD_TERMINATE)
      {
        pCtrlInst->gearVariables.ledVariables.referenceRunning = FALSE;
      }
      if ((*ppGearCmd)->eGearCmd < GEAR_CMD_QUERY_STATUS ||
          (*ppGearCmd)->eGearCmd > GEAR_CMD_READ_MEMORY_CELL)
      {
        rc = R_IGNORE_FRAME;
        return (rc);
      }
    }
  }
  if (GEAR_TYPE_SWITCHING_FUNCTION == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.switchingVariables.referenceRunning)
    {
      if ((*ppGearCmd)->eGearCmd == GEAR_CMD_TERMINATE)
      {
        pCtrlInst->gearVariables.switchingVariables.referenceRunning = FALSE;
      }
      if ((*ppGearCmd)->eGearCmd < GEAR_CMD_QUERY_STATUS ||
          (*ppGearCmd)->eGearCmd > GEAR_CMD_READ_MEMORY_CELL)
      {
        rc = R_IGNORE_FRAME;
        return (rc);
      }
    }
  }
  if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationRunning)
    {
      if ((*ppGearCmd)->eGearCmd == GEAR_CMD_TERMINATE)
      {
        pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationRunning = FALSE;
      }
    }
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    // control gear level frames
    case GEAR_CMD_DAPC:
    case GEAR_CMD_OFF:
    case GEAR_CMD_UP:
    case GEAR_CMD_DOWN:
    case GEAR_CMD_STEP_UP:
    case GEAR_CMD_STEP_DOWN:
    case GEAR_CMD_RECALL_MAX_LEVEL:
    case GEAR_CMD_RECALL_MIN_LEVEL:
    case GEAR_CMD_STEP_DOWN_AND_OFF:
    case GEAR_CMD_ON_AND_STEP_UP:
    case GEAR_CMD_ENABLE_DAPC_SEQUENCE:
    case GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL:
    case GEAR_CMD_GO_TO_SCENE:
      {
        if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
        {
          rc = gear_colour_process_forward_level(pCtrlInst, pForwardFrame, ppGearCmd,
                                                 pReceivedAdr, pResponseValue);
        }

        rc = gear_process_forward_levels(pCtrlInst, *ppGearCmd,
                                         pForwardFrame->data, pResponseValue);
      }
    break;

    // control gear configuration frames
    case GEAR_CMD_SET_SCENE:
    case GEAR_CMD_REMOVE_FROM_SCENE:
    case GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL:
    case GEAR_CMD_SET_POWER_ON_LEVEL:
      rc = gear_process_forward_configuration(pCtrlInst, *ppGearCmd,
                                              (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK),
                                              pResponseValue);

      if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        gear_colour_process_forward_configuration(pCtrlInst, pForwardFrame, ppGearCmd,
                                                  pReceivedAdr, pResponseValue);
      }
    break;

    case GEAR_CMD_RESET:
    case GEAR_CMD_STORE_ACTUAL_LEVEL_IN_DTR0:
    case GEAR_CMD_SAVE_PERSISTENT_VARIABLES:
    case GEAR_CMD_SET_OPERATING_MODE:
    case GEAR_CMD_RESET_MEMORY_BANK:
    case GEAR_CMD_IDENTIFY_DEVICE:
    case GEAR_CMD_SET_MAX_LEVEL:
    case GEAR_CMD_SET_MIN_LEVEL:
    case GEAR_CMD_SET_FADE_RATE:
    case GEAR_CMD_SET_FADE_TIME:
    case GEAR_CMD_SET_EXTENDED_FADE_TIME:
    case GEAR_CMD_ADD_TO_GROUP:
    case GEAR_CMD_REMOVE_FROM_GROUP:
    case GEAR_CMD_SET_SHORT_ADDRESS:
    case GEAR_CMD_ENABLE_WRITE_MEMORY:
      {
        rc = gear_process_forward_configuration(pCtrlInst, *ppGearCmd,
                                                (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK),
                                                pResponseValue);
      }
    break;

    // control gear query frames
    case GEAR_CMD_QUERY_POWER_ON_LEVEL:
    case GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL:
    case GEAR_CMD_QUERY_SCENE_LEVEL:
      rc = gear_process_forward_querys(pCtrlInst, *ppGearCmd,
                                       pResponseValue, pForwardFrame->data,
                                       pReceivedAdr);

      if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
      {
        gear_colour_process_forward_querys(pCtrlInst, pForwardFrame, ppGearCmd,
                                           pReceivedAdr, pResponseValue);
      }
    break;

    case GEAR_CMD_QUERY_STATUS:
    case GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT:
    case GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE:
    case GEAR_CMD_QUERY_LAMP_FAILURE:
    case GEAR_CMD_QUERY_LAMP_POWER_ON:
    case GEAR_CMD_QUERY_LIMIT_ERROR:
    case GEAR_CMD_QUERY_RESET_STATE:
    case GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_VERSION_NUMBER:
    case GEAR_CMD_QUERY_CONTENT_DTR0:
    case GEAR_CMD_QUERY_DEVICE_TYPE:
    case GEAR_CMD_QUERY_NEXT_DEVICE_TYPE:
    case GEAR_CMD_QUERY_PHYSICAL_MINIMUM:
    case GEAR_CMD_QUERY_POWER_FAILURE:
    case GEAR_CMD_QUERY_CONTENT_DTR1:
    case GEAR_CMD_QUERY_CONTENT_DTR2:
    case GEAR_CMD_QUERY_OPERATING_MODE:
    case GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE:
    case GEAR_CMD_QUERY_ACTUAL_LEVEL:
    case GEAR_CMD_QUERY_MAX_LEVEL:
    case GEAR_CMD_QUERY_MIN_LEVEL:
    case GEAR_CMD_QUERY_FADE_TIME_RATE:
    case GEAR_CMD_QUERY_EXTENDED_FADE_TIME:
    case GEAR_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE:
    case GEAR_CMD_QUERY_GROUPS_0_7:
    case GEAR_CMD_QUERY_GROUPS_8_15:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_H:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_M:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_L:
    case GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0:
    case GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER:
      {
        rc = gear_process_forward_querys(pCtrlInst, *ppGearCmd,
                                         pResponseValue, pForwardFrame->data,
                                         pReceivedAdr);
      }
    break;

    // control gear initialization frames
    case GEAR_CMD_TERMINATE:
    case GEAR_CMD_DTR0:
    case GEAR_CMD_INITIALISE:
    case GEAR_CMD_RANDOMISE:
    case GEAR_CMD_COMPARE:
    case GEAR_CMD_WITHDRAW:
    case GEAR_CMD_SEARCHADDRH:
    case GEAR_CMD_SEARCHADDRM:
    case GEAR_CMD_SEARCHADDRL:
    case GEAR_CMD_PROGRAM_SHORT_ADDRESS:
    case GEAR_CMD_VERIFY_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_SHORT_ADDRESS:
    case GEAR_CMD_ENABLE_DEVICE_TYPE:
    case GEAR_CMD_DTR1:
    case GEAR_CMD_DTR2:
    case GEAR_CMD_WRITE_MEMORY_LOCATION:
    case GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
      {
        rc = gear_process_forward_special(pCtrlInst, *ppGearCmd,
                                          (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK),
                                          pResponseValue);
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  // reset address of query (next) device type
  #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
    if ((GEAR_CMD_QUERY_DEVICE_TYPE != (*ppGearCmd)->eGearCmd) &&
        (GEAR_CMD_QUERY_NEXT_DEVICE_TYPE != (*ppGearCmd)->eGearCmd) &&
        (DALI_ADRTYPE_RESERVED != pCtrlInst->gearDeviceTypeQuery.adr.adrType))
    {
      pCtrlInst->gearDeviceTypeQuery.adr.adrType = DALI_ADRTYPE_RESERVED;
    }
  #endif
  // reset enable device type
  if ((GEAR_CMD_ENABLE_DEVICE_TYPE != (*ppGearCmd)->eGearCmd) && (R_DALILIB_OK == rc) &&
      (GEAR_TYPE_NO_FURTHER_DEVICE_TYPE != pCtrlInst->gearVariables.enableDeviceType))
  {
    pCtrlInst->gearVariables.enableDeviceType = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the response fluorescent lamp frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_fluorescent(PDaliLibInstance  pInst,
                                                          GEAR_CMD          eGearCmd,
                                                          uint8_t           responseValue,
                                                          dalilib_action_t* pAct)
{
  (void)pInst;

  if (eGearCmd != GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_FLUORESCENT;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response LED frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_led(PDaliLibInstance  pInst,
                                                  LED_CMD           eLEDCmd,
                                                  uint8_t           responseValue,
                                                  dalilib_action_t* pAct)
{
  (void)pInst;

  if (eLEDCmd < LED_QUERY_GEAR_TYPE || eLEDCmd >= LED_CMD_LAST)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_LED;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response COLOUR frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_colour(PDaliLibInstance  pInst,
                                                     COLOUR_CMD        eCOLOURCmd,
                                                     uint8_t           responseValue,
                                                     dalilib_action_t* pAct)
{
  (void)pInst;

  if (eCOLOURCmd < COLOUR_CMD_QUERY_GEAR_FEATURES_STATUS || eCOLOURCmd >= COLOUR_CMD_LAST)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_COLOUR;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response Switching frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_switching(PDaliLibInstance  pInst,
                                                        SWITCHING_CMD     eSwitchingCmd,
                                                        uint8_t           responseValue,
                                                        dalilib_action_t* pAct)
{
  (void)pInst;

  if (eSwitchingCmd < SWITCHING_QUERY_GEAR_TYPE || eSwitchingCmd >= SWITCHING_CMD_LAST)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  pAct->gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_SWITCHING;
  pAct->gearReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->gearReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response base dali frame
//----------------------------------------------------------------------
static R_DALILIB_RESULT gear_process_response_base(PDaliLibInstance  pInst,
                                                   GEAR_CMD          eGearCmd,
                                                   uint8_t           value,
                                                   dalilib_action_t* pAction)
{
  R_DALILIB_RESULT   rc = R_DALILIB_OK;

  if (pInst->last_tx_frame.lastCmd.eGearCmd >= GEAR_CMD_LAST)
  {
    rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
  }

  switch(eGearCmd)
  {
    // control gear status frames
    case GEAR_CMD_QUERY_STATUS:
    // case GEAR_CMD_QUERY_LAMP_FAILURE:   // contains in query status
    // case GEAR_CMD_QUERY_LAMP_POWER_ON:  // contains in query status
    // case GEAR_CMD_QUERY_LIMIT_ERROR:    // contains in query status
    // case GEAR_CMD_QUERY_RESET_STATE:    // contains in query status
    // case GEAR_CMD_QUERY_POWER_FAILURE:      // contains in query status
    // case GEAR_CMD_QUERY_CONTROL_GEAR_FAILURE: //  contains in query status
      {
        rc = gear_process_response_status(value, pAction);
      }
    break;

    // control gear level frames
    case GEAR_CMD_QUERY_ACTUAL_LEVEL:
    case GEAR_CMD_QUERY_MAX_LEVEL:
    case GEAR_CMD_QUERY_MIN_LEVEL:
    case GEAR_CMD_QUERY_POWER_ON_LEVEL:
    case GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL:
    case GEAR_CMD_QUERY_SCENE_LEVEL:
      {
        rc = gear_process_response_levels(eGearCmd, value, pAction);
      }
    break;

    // control gear config frames
    case GEAR_CMD_QUERY_EXTENDED_VERSION_NUMBER:
    case GEAR_CMD_QUERY_VERSION_NUMBER:
    case GEAR_CMD_QUERY_CONTENT_DTR0:
    case GEAR_CMD_QUERY_DEVICE_TYPE:
    case GEAR_CMD_QUERY_NEXT_DEVICE_TYPE:
    case GEAR_CMD_QUERY_MISSING_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_PHYSICAL_MINIMUM:
    case GEAR_CMD_QUERY_CONTENT_DTR1:
    case GEAR_CMD_QUERY_CONTENT_DTR2:
    case GEAR_CMD_QUERY_OPERATING_MODE:
    // case GEAR_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE:
    case GEAR_CMD_QUERY_LIGHT_SOURCE_TYPE:
    case GEAR_CMD_QUERY_FADE_TIME_RATE:
    case GEAR_CMD_QUERY_EXTENDED_FADE_TIME:
    case GEAR_CMD_QUERY_GROUPS_0_7:
    case GEAR_CMD_QUERY_GROUPS_8_15:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_H:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_M:
    case GEAR_CMD_QUERY_RANDOM_ADDRESS_L:
    case GEAR_CMD_QUERY_CONTROL_GEAR_PRESENT:
      {
        rc = gear_process_response_config(eGearCmd, value, pAction);
      }
    break;

    case GEAR_CMD_VERIFY_SHORT_ADDRESS:
    case GEAR_CMD_QUERY_SHORT_ADDRESS:
    case GEAR_CMD_COMPARE:
      {
        rc = gear_process_response_initialise(eGearCmd, value, pAction);
      }
    break;

    case GEAR_CMD_READ_MEMORY_LOCATION_DTR1_DTR0:
      {
        rc = gear_process_response_vendor(eGearCmd, value, pAction, &pInst->last_tx_frame);
      }
    break;

    case GEAR_CMD_READ_MEMORY_CELL:
    case GEAR_CMD_WRITE_MEMORY_LOCATION:
      {
        rc = gear_process_response_memory(value, pAction, &pInst->last_tx_frame);
      }
    break;

    // request for this response not found
    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

R_DALILIB_RESULT gear_load_mem_blocks(PDaliLibInstance            pInst,
                                      dali_ctrl_gear_pers_mem_t*  pGearPersistent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (pInst->config.internal.savePersistentModules)
  {
    // load general gear persistent memory
    rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK); // load before frequent vars
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK);
    }
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK); // might need config vars
    }

    // load device type module persistent memory
    if (R_DALILIB_OK == rc)
    {
      switch(pGearPersistent->gearPersVariables.gearDeviceType)
      {
        case GEAR_TYPE_FLUORESCENT_LAMPS:
          rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_FLUORESCENT_ID);
        break;

        case GEAR_TYPE_LED_MODULES:
          rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_LED_FREQUENT_ID);
          if (R_DALILIB_OK == rc)
          {
            rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_LED_OPERATING_ID);
          }
          if (R_DALILIB_OK == rc)
          {
            rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_LED_CONFIG_ID);
          }
        break;

        case GEAR_TYPE_SWITCHING_FUNCTION:
          rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_SWITCHING_ID);
        break;

        case GEAR_TYPE_COLOUR_CONTROL:
          rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_COLOUR_ID);
        break;

        default:
          rc = R_DALILIB_CALLBACK_FAILED;
        break;
      }
    }

    // load D4i extensions persistent memory
    #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_ID);
    }
    #endif
    #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_POWER_SUPPLY_ID);
    }
    #endif
    #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_ENERGY_REPORT_ID);
    }
    #endif
    #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_ID);
    }
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_ID);
    }
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_ID);
    }
    #endif
  }
  else
  {
    // preserve backwards compatibility
    rc = dali_cb_load_mem(pInst, pGearPersistent);
  }

  return (rc);
}

R_DALILIB_RESULT gear_apply_mem_block_pers_vars_frequent(PCtrlGearInstance pCtrlInst,
                                                         uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_gear_pers_variables_t*                   pPersistentVariables = &pCtrlInst->gearPersistent.gearPersVariables;
  dali_ctrl_gear_pers_variables_frequent_block_t*    pBlock = (dali_ctrl_gear_pers_variables_frequent_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->lastLightLevel = pBlock->lastLightLevel;

  // check value range
  if (0                              != pPersistentVariables->lastLightLevel &&
      pPersistentVariables->maxLevel  < pPersistentVariables->lastLightLevel &&
      pPersistentVariables->minLevel  > pPersistentVariables->lastLightLevel)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable lastLightLevel is out of bounds. Value: %u Range: [0, minLevel..maxLevel]",
             pPersistentVariables->lastLightLevel);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->lastLightLevel = 0xFE;
  }

  return (rc);
}

R_DALILIB_RESULT gear_apply_mem_block_pers_vars_operating(PCtrlGearInstance pCtrlInst,
                                                          uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_gear_pers_variables_t*                   pPersistentVariables = &pCtrlInst->gearPersistent.gearPersVariables;
  dali_ctrl_gear_pers_variables_operating_block_t*   pBlock = (dali_ctrl_gear_pers_variables_operating_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->fadeRate            = pBlock->fadeRate;
  pPersistentVariables->fadeTime            = pBlock->fadeTime;
  pPersistentVariables->extFadeTimeBase     = pBlock->extFadeTimeBase;
  pPersistentVariables->extFadeTimeMultiple = pBlock->extFadeTimeMultiple;
  pPersistentVariables->operatingMode       = pBlock->operatingMode;

  // check value range
  if (15 < pPersistentVariables->fadeRate ||
      1  > pPersistentVariables->fadeRate)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable fadeRate is out of bounds. Value: %u Range: [1..15]",
             pPersistentVariables->fadeRate);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->fadeRate = 0x07;
  }
  if (15 < pPersistentVariables->fadeTime)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable fadeTime is out of bounds. Value: %u Range: [0..15]",
             pPersistentVariables->fadeTime);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->fadeTime = 0x00;
  }
  if (15 < pPersistentVariables->extFadeTimeBase)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable extFadeTimeBase is out of bounds. Value: %u Range: [0..15]",
             pPersistentVariables->extFadeTimeBase);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->extFadeTimeBase = 0x00;
  }
  if (4 < pPersistentVariables->extFadeTimeMultiple)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable extFadeTimeMultiple is out of bounds. Value: %u Range: [0..15]",
             pPersistentVariables->extFadeTimeMultiple);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->extFadeTimeMultiple = 0x00;
  }
  if (0x00 < pPersistentVariables->operatingMode &&
      0x80 > pPersistentVariables->operatingMode)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable operatingMode is out of bounds. Value: %u Range: [0x00, 0x80..0xFF]",
             pPersistentVariables->operatingMode);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->operatingMode = DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE;
  }

  return (rc);
}

R_DALILIB_RESULT gear_apply_mem_block_pers_vars_config(PCtrlGearInstance pCtrlInst,
                                                         uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_gear_pers_variables_t*                   pPersistentVariables = &pCtrlInst->gearPersistent.gearPersVariables;
  PDaliLibInstance                                   pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_gear_pers_variables_config_block_t *pBlock = (dali_ctrl_gear_pers_variables_config_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->highResDimming     = pBlock->highResDimming;
  pPersistentVariables->phm                = pBlock->phm;
  pPersistentVariables->powerOnLevel       = pBlock->powerOnLevel;
  pPersistentVariables->systemFailureLevel = pBlock->systemFailureLevel;
  pPersistentVariables->minLevel           = pBlock->minLevel;
  pPersistentVariables->maxLevel           = pBlock->maxLevel;
  pPersistentVariables->shortAddress       = pBlock->shortAddress;
  pPersistentVariables->randomAddress      = pBlock->randomAddress;
  pPersistentVariables->gearGroups         = pBlock->gearGroups;
  pPersistentVariables->gearDeviceType     = pBlock->gearDeviceType;
  memcpy(pPersistentVariables->scene, pBlock->scene, sizeof(pPersistentVariables->scene));

  // check value range
  if (0x01  < pPersistentVariables->highResDimming)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable highResDimming is out of bounds. Value: %u Range: [TRUE, FALSE]",
             pPersistentVariables->highResDimming);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->highResDimming = pInst->config.vendor.control_gear_high_res_dimming;
  }
  if (254  < pPersistentVariables->phm ||
      1    > pPersistentVariables->phm)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable phm is out of bounds. Value: %.2f Range: [1, 254]",
             pPersistentVariables->phm);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->phm = (1 > pInst->config.vendor.control_gear_phm) ? 1 :
                                pInst->config.vendor.control_gear_phm;
  }
  if (pPersistentVariables->maxLevel  < pPersistentVariables->minLevel ||
      pPersistentVariables->phm       > pPersistentVariables->minLevel)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable minLevel is out of bounds. Value: %u Range: [phm..maxLevel]",
             pPersistentVariables->minLevel);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->minLevel = (uint32_t)pPersistentVariables->phm;
  }
  if (0xFE                            < pPersistentVariables->maxLevel ||
      pPersistentVariables->minLevel  > pPersistentVariables->maxLevel)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable maxLevel is out of bounds. Value: %u Range: [minLevel..0xFE]",
             pPersistentVariables->maxLevel);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->maxLevel = 0xFE;
  }
  if (0x63       < pPersistentVariables->shortAddress &&
      DALI_MASK != pPersistentVariables->shortAddress)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable shortAddress is out of bounds. Value: %u Range: [0x00..0x63, 0xFF]",
             pPersistentVariables->shortAddress);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->shortAddress = pInst->config.vendor.shortAddress;
  }
  if (0x00FFFFFF < pPersistentVariables->randomAddress)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable randomAddress is out of bounds. Value: %lu Range: [0x00..0x00FFFFFF]",
             pPersistentVariables->randomAddress);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->randomAddress = 0x00FFFFFF;
  }

  return (rc);
}

R_DALILIB_RESULT gear_apply_mem_block_flamp_pers_variables(PCtrlGearInstance pCtrlInst,
                                                           uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT        rc = R_DALILIB_OK;
  char                    buf[128];
  flamp_pers_variables_t* pPersistentVariables = &pCtrlInst->gearPersistent.fluorescentPersVariables;
  dali_ctrl_gear_flamp_pers_variables_block_t *pBlock = (dali_ctrl_gear_flamp_pers_variables_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->extendedVersionNumber = pBlock->extendedVersionNumber;
  pPersistentVariables->deviceType            = pBlock->deviceType;

  // check value range
  if (2 < pPersistentVariables->extendedVersionNumber)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable extendedVersionNumber is out of bounds. Value: %u Range: [0..2]",
             pPersistentVariables->extendedVersionNumber);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->extendedVersionNumber = FLUORESCENT_LAMP_VERSION;
  }
  if (GEAR_TYPE_FLUORESCENT_LAMPS != pPersistentVariables->deviceType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable deviceType is out of bounds. Value: %u Range: [0]",
             pPersistentVariables->deviceType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->deviceType = GEAR_TYPE_FLUORESCENT_LAMPS;
  }

  return (rc);
}


/**********************************************************************
 * Public Functions
 **********************************************************************/

R_DALILIB_RESULT ctrl_gear_apply_mem_block(PCtrlGearInstance pCtrlInst,
                                           uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT            rc = R_DALILIB_OK;
  uint8_t                     blockId   = pDaliMem[0];
  uint8_t                     version   = pDaliMem[1];

  switch(blockId)
  {
    case DALI_CTRL_GEAR_PERS_VARS_FREQUENT_ID:
      if (version == 1)
      {
        rc = gear_apply_mem_block_pers_vars_frequent(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_VARS_OPERATING_ID:
      if (version == 1)
      {
        gear_apply_mem_block_pers_vars_operating(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_VARS_CONFIG_ID:
      if (version == 1)
      {
        rc = gear_apply_mem_block_pers_vars_config(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_FLUORESCENT_ID:
      if (version == 1)
      {
        rc = gear_apply_mem_block_flamp_pers_variables(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_LED_FREQUENT_ID:
      if (version == 1)
      {
        rc = gear_led_apply_mem_block_pers_vars_frequent(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_LED_OPERATING_ID:
      if (version == 1)
      {
        rc = gear_led_apply_mem_block_pers_vars_operating(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_LED_CONFIG_ID:
      if (version == 1)
      {
        rc = gear_led_apply_mem_block_pers_vars_config(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_SWITCHING_ID:
      if (version == 1)
      {
        rc = gear_switching_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_COLOUR_ID:
      if (version == 1)
      {
        rc = gear_colour_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
    case DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_ID:
      if (version == 1)
      {
        rc = gear_mem_bank1_ext_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    #endif

    #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
    case DALI_CTRL_GEAR_PERS_POWER_SUPPLY_ID:
      if (version == 1)
      {
        gear_power_supply_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    #endif

    #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
    case DALI_CTRL_GEAR_PERS_ENERGY_REPORT_ID:
      if (version == 1)
      {
        rc = gear_energy_report_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    #endif

    #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    case DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_ID:
      if (version == 1)
      {
        rc = gear_diag_maintenance_apply_mem_block_memory_bank205(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_ID:
      if (version == 1)
      {
        rc = gear_diag_maintenance_apply_mem_block_memory_bank206(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_ID:
      if (version == 1)
      {
        rc = gear_diag_maintenance_apply_mem_block_memory_bank206(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    #endif

    default:
      rc = R_MEM_NOT_DEFINED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// System failure triggered by HAL-Driver over status frame.
// Set system failure level to target level or start power on process.
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_set_systemfailure(PCtrlGearInstance pCtrlInst, uint8_t bFailure)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (pCtrlInst->gearSystemFailure)
  {
    // bus power on after system failure
    if (!bFailure)
    {
      // add only for bus powered devices
        //gear_init_after_power_cycle(pCtrlInst);
      pCtrlInst->gearPowerOnRunning = TRUE;
      // set lamp on the off, during power on level time out
      gear_reaction(pCtrlInst, DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL, 0x00);
    }
  }
  else
  {
    // system failure
    if (bFailure && DALI_MASK != pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel)
    {
      pCtrlInst->gearVariables.targetLevel = pCtrlInst->gearPersistent.gearPersVariables.systemFailureLevel;
      if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
      {
        pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
      if (0 != pCtrlInst->gearVariables.targetLevel)
      {
        pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
      }
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst),
                    pCtrlInst->gearVariables.targetLevel));
    }
  }
  pCtrlInst->gearSystemFailure = bFailure;

  return (rc);
}

R_DALILIB_RESULT ctrl_gear_check_twice(PDaliLibInstance      pInst,
                                            dalilib_frame_t*      pForwardFrame,
                                            ctrl_gear_commands_t* pGearCmd)
{
  // is twice-Frame
  if (pGearCmd->twice)
  {
    // first twice frame
    if (0 == pInst->last_rx_frame.bWaitTwice)
    {
      // wait next frame
      pInst->last_rx_frame.bWaitTwice = 1;
      memcpy(&pInst->last_rx_frame.frame, pForwardFrame, sizeof(dalilib_frame_t));
      return (R_IGNORE_FRAME);
    }
    // check if valid second twice frame within settling time
    if (pInst->last_rx_frame.frame.datalength == pForwardFrame->datalength &&
        pInst->last_rx_frame.frame.data == pForwardFrame->data             &&
        pForwardFrame->priority <= FRAME_PRIO_FF_P6 &&
        dali_difftime(pForwardFrame->timestampStartOfRecv, pInst->last_rx_frame.frame.timestamp) <= DALI_SEND_TWICE_TIMEOUT)
    {
      // twice frames success received
    }
    else // new first twice frame, wait next frame
    {
      pInst->last_rx_frame.bWaitTwice = 1;
      memcpy(&pInst->last_rx_frame.frame, pForwardFrame, sizeof(dalilib_frame_t));
      return (R_IGNORE_FRAME);
    }
  }
  pInst->last_rx_frame.bWaitTwice = 0;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the received forward dali frame
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_process_forward_frame(PCtrlGearInstance pCtrlInst,
                                                 dalilib_frame_t*  pForwardFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t opcodeByte = 0x00;
  uint8_t selectorBit = 0x00;
  gear_adr_t receivedAdr;
  ctrl_gear_commands_t* pGearCmd = NULL;
  uint8_t  responseValue = 0x00;

  PDaliLibInstance pInst = pCtrlInst->pInst;

  memset(&receivedAdr, 0, sizeof(receivedAdr));
  rc = gear_parse_address(pForwardFrame, &receivedAdr);

  if (R_DALILIB_OK != rc)
  {
    pCtrlInst->gearVariables.enableDeviceType = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
    #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
      pCtrlInst->gearDeviceTypeQuery.adr.adrType = DALI_ADRTYPE_RESERVED;
    #endif
    // reset twice frame wait flag
    pInst->last_rx_frame.bWaitTwice = 0;
    return (rc);
  }

  rc = gear_check_address(pCtrlInst, &receivedAdr);
  if (R_DALILIB_OK != rc)
  {
    pCtrlInst->gearVariables.enableDeviceType = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
    #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
      pCtrlInst->gearDeviceTypeQuery.adr.adrType = DALI_ADRTYPE_RESERVED;
    #endif
    // reset twice frame wait flag
    pInst->last_rx_frame.bWaitTwice = 0;
    //ignore frame, this frame is not for me
    return (rc);
  }
  // save time of last valid frame, for saving persistent data after DALI_SAVE_PERSISTENT_TIME
  pCtrlInst->gearSavePersistentStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
  pCtrlInst->gearSavePersistentRunning = TRUE;

  opcodeByte = pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE;
  selectorBit = ((pForwardFrame->data>>8) &  DALI_CTRL_GEAR_SELECT_BIT_MASK);

  if (opcodeByte >= DALI_CTRL_GEAR_EXT_CMD_START &&
      opcodeByte <= DALI_CTRL_GEAR_EXT_CMD_END &&
      DALI_ADRTYPE_SPEC != receivedAdr.adrType &&
      selectorBit == 0x01)
  {
    rc = R_DALILIB_FRAME_NOT_SUPPORTED;
    switch(pCtrlInst->gearVariables.enableDeviceType)
    {
      case GEAR_TYPE_LED_MODULES:
        if (GEAR_TYPE_LED_MODULES == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
        {
          rc = gear_led_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        }
        else
        {
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        }
      break;

      case GEAR_TYPE_SWITCHING_FUNCTION:
        if (GEAR_TYPE_SWITCHING_FUNCTION == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
        {
          rc = gear_switching_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        }
        else
        {
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        }
      break;

      case GEAR_TYPE_COLOUR_CONTROL:
        if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
        {
          rc = gear_colour_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        }
        else
        {
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        }
      break;

      case GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY:
        #ifdef DALILIB_GEAR_POWER_SUPPLY_PRESENT
          rc = gear_power_supply_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        #else
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        #endif
      break;

      case GEAR_TYPE_CONTROL_MEM_BANK1_EXT_SUPPLY:
        #ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT
          rc = gear_mem_bank1_ext_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        #else
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        #endif
      break;

      case GEAR_TYPE_CONTROL_GEAR_ENERGY_REPORTING:
        #ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT
          rc = gear_energy_reporting_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        #else
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        #endif
      break;

      case GEAR_TYPE_CONTROL_GEAR_DIAG_MAINTENANCE:
        #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
          rc = gear_diag_maintenance_process_forward(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
        #else
          rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
        #endif
      break;

      default:
        rc = R_GEAR_DEVICE_TYPE_NOT_SUPPORTED;
      break;
    }
    // reset enable device type
    pCtrlInst->gearVariables.enableDeviceType = GEAR_TYPE_NO_FURTHER_DEVICE_TYPE;
    // reset address of device type queries
    #ifdef DALI_CTRL_GEAR_ANY_EXTENSION_PRESENT
      pCtrlInst->gearDeviceTypeQuery.adr.adrType = DALI_ADRTYPE_RESERVED;
    #endif
  }
  else
  {
    rc = gear_process_forward_frame(pCtrlInst, pForwardFrame, &pGearCmd, &receivedAdr, &responseValue);
  }
  if (R_IGNORE_FRAME == rc)
  {
    return (R_DALILIB_OK);
  }
  if (rc == R_DALILIB_OK && TRUE == pGearCmd->answer)
  {
    send_frame_t newSendFrame;

    memset(&newSendFrame, 0, sizeof(newSendFrame));

    newSendFrame.frame.priority = FRAME_PRIO_BF_P0;
    newSendFrame.frame.datalength = FRAME_BF_LEN;
    newSendFrame.frame.data = responseValue;
    newSendFrame.type = SEND_FRAME_TYPE_CTRL_GEAR;
    dali_cb_send(pInst, &newSendFrame );
  }

  return (rc);
}


//----------------------------------------------------------------------
// frame time out
// - no answer
// - control gear address does not exists on bus
// - forward frame not sent
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_response_timeout(PDaliLibInstance            pInst,
                                            send_frame_t*               pFrame,
                                            DALILIB_RESPONSE_VALUE_TYPE valueType)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  dalilib_action_t action;

  memset( &action, 0, sizeof(action));
  action.actionType = DALILIB_CTRL_GEAR_REACTION;
  action.gearReact.valueType = valueType;

  switch(pFrame->lastActionType)
  {
    case DALILIB_ACT_TYPE_CTRL_GEAR_CONFIG:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_CONFIG;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_LEVEL:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_LEVEL;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_SCENE:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_SCENE;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_GROUP:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_GROUP;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_QUERY_STATUS;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_QUERIES:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_QUERIES;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_VENDOR;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_INITIALISE;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_MEMORY:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_MEMORY;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_LED:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_LED;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_COLOUR:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_COLOUR;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_SWITCHING:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_SWITCHING;
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_FLUORESCENT:
      {
        action.gearReact.reactionCode = DALILIB_REACT_CTRL_GEAR_FLUORESCENT;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_ACTION_NOT_SUPPORTED;
    break;
  }

  // notify the application
  // transmit the response values
  if (R_DALILIB_OK == rc)
  {
    dali_cb_reaction(pInst, &action);
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the response dali frame
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_process_response_frame(PDaliLibInstance pInst,
                                                  dalilib_frame_t* pResponseFrame)
{
  R_DALILIB_RESULT   rc = R_DALILIB_OK;
  GEAR_CMD           eGearCmd = GEAR_CMD_LAST;
  dalilib_action_t   action;
  uint8_t            value = 0;

  memset( &action, 0, sizeof(action));
  action.actionType = DALILIB_CTRL_GEAR_REACTION;

  eGearCmd = (GEAR_CMD)pInst->last_tx_frame.lastCmd.eGearCmd;
  value = (pResponseFrame->data & DALI_RESPONSE_VALUE_MASK);

  switch(pInst->last_tx_frame.lastActionType)
  {
    // response for LED forward frames
    case DALILIB_ACT_TYPE_CTRL_GEAR_LED:
      {
        rc = gear_process_response_led(pInst, (LED_CMD)eGearCmd, value, &action);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_COLOUR:
      {
        rc = gear_process_response_colour(pInst, (COLOUR_CMD)eGearCmd, value, &action);
      }
    break;

    // response for Switching forward frames
    case DALILIB_ACT_TYPE_CTRL_GEAR_SWITCHING:
      {
          rc = gear_process_response_switching(pInst, (SWITCHING_CMD)eGearCmd, value, &action);
      }
    break;

    // response for Fluorescent lamp frames
    case DALILIB_ACT_TYPE_CTRL_GEAR_FLUORESCENT:
      {
        rc = gear_process_response_fluorescent(pInst, eGearCmd, value, &action);
      }
    break;

    default:
      rc = gear_process_response_base(pInst, eGearCmd, value, &action);
    break;
  }

  // notify the application
  // transmit the response values
  if (R_DALILIB_OK   == rc ||
      R_ABORT_ACTION == rc)
  {
    dali_cb_reaction(pInst, &action);
  }

  if (R_SUPPRESS_REACTION == rc)
  {
    rc = R_DALILIB_OK;
  }

  return (rc);
}


//----------------------------------------------------------------------
// control gear timer function
//----------------------------------------------------------------------
void ctrl_gear_timingHelper(void* pCtrlInstance)
{
  PCtrlGearInstance pCtrlInst = (PCtrlGearInstance)pCtrlInstance;
  PDaliLibInstance  pInst = pCtrlInst->pInst;

  if (NULL == pInst)
  {
    return;
  }
  // check, reference measurement
  if (GEAR_TYPE_LED_MODULES == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.ledVariables.referenceRunning)
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->gearVariables.ledVariables.referenceStartTime)
          > CTRL_GEAR_LED_REFERENCING_TIME_MAX)
      {
        pCtrlInst->gearVariables.ledVariables.referenceRunning = FALSE;
        dalilib_action_t action;

        memset(&action, 0, sizeof(action));

        action.actionType = DALILIB_CTRL_GEAR_REACTION;
        action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_STOP;
        dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    }
  }
  if (GEAR_TYPE_SWITCHING_FUNCTION == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.switchingVariables.referenceRunning)
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->gearVariables.switchingVariables.referenceStartTime)
          > CTRL_GEAR_SWITCHING_REFERENCING_TIME_MAX)
      {
        pCtrlInst->gearVariables.switchingVariables.referenceRunning = FALSE;
        dalilib_action_t action;

        memset(&action, 0, sizeof(action));

        action.actionType = DALILIB_CTRL_GEAR_REACTION;
        action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_STOP;
        dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    }
  }
  if (GEAR_TYPE_COLOUR_CONTROL == pCtrlInst->gearPersistent.gearPersVariables.gearDeviceType)
  {
    if (pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationRunning)
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->gearVariables.colourVariables.calibrationStartTime)
          > CTRL_GEAR_COLOUR_CALIBRATION_TIME_MAX)
      {
        pCtrlInst->gearVariables.colourVariables.colourStatus.calibrationRunning = FALSE;
        dalilib_action_t action;

        memset(&action, 0, sizeof(action));

        action.actionType = DALILIB_CTRL_GEAR_REACTION;
        action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_COLOUR_CALIBRATION_STOP;
        dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    }
  }
  // power on time
  if (TRUE == pCtrlInst->gearPowerOnRunning)
  {
    // first called
    if (0 == pCtrlInst->gearPowerOnLevelTime)
    {
      pCtrlInst->gearPowerOnLevelTime = pInst->currentTime;
    }

    if (dali_difftime(pInst->currentTime, pCtrlInst->gearPowerOnLevelTime) >
          DALI_CTRL_GEAR_POWER_ON_LEVEL_TIMEOUT)
    {
      pCtrlInst->gearPowerOnRunning = FALSE;
      pCtrlInst->gearVariables.actualLevel = pCtrlInst->gearVariables.targetLevel;
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.actualLevel));
    }
  }

  //identify device
  if (TRUE == pCtrlInst->gearIdentifyRunning)
  {
    static uint8_t iTicker = 0;
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearIdentifyDeviceStartTime) >
          DALI_CTRL_GEAR_IDENTIFY_DEVICE_TIME_MAX)
    {
      pCtrlInst->gearIdentifyDeviceState = CTRL_GEAR_DEVICE_IDENTIFY_STATE_OFF;
      pCtrlInst->gearIdentifyRunning = FALSE;
      pCtrlInst->gearIdentifyDeviceStartTime = 0;
      iTicker = 0;
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel ));
    }
    else if (iTicker >= 10)
    {
      iTicker = 0;
      if (CTRL_GEAR_DEVICE_IDENTIFY_STATE_OFF == pCtrlInst->gearIdentifyDeviceState)
      {
        pCtrlInst->gearIdentifyDeviceState = CTRL_GEAR_DEVICE_IDENTIFY_STATE_ON;
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      (CTRL_GEAR_INITIALISE_STATE_ENABLED == pCtrlInst->gearVariables.initialisationState ?
                        (100 * 1000) : pCtrlInst->gearPersistent.gearPersVariables.maxLevel));
      }
      else
      {
        pCtrlInst->gearIdentifyDeviceState = CTRL_GEAR_DEVICE_IDENTIFY_STATE_OFF;
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      (CTRL_GEAR_INITIALISE_STATE_ENABLED == pCtrlInst->gearVariables.initialisationState ?
                        (0) : pCtrlInst->gearPersistent.gearPersVariables.minLevel));
      }
    }
    iTicker++;
  }
  // initialise time
  if (TRUE == pCtrlInst->gearVariables.initialisationState ||
      CTRL_GEAR_INITIALISE_STATE_WITHDRAWN == pCtrlInst->gearVariables.initialisationState)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearInitialiseStartTime) >
          DALI_CTRL_GEAR_INITIALISE_TIME_MAX)
    {
      pCtrlInst->gearVariables.initialisationState = FALSE;
      pCtrlInst->gearInitialiseStartTime = 0;
    }
    return;
  }

  // Save persistent variables
  if ((TRUE == pCtrlInst->gearSavePersistentRunning) && DALI_SAVE_PERSISTENT_TIME)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearSavePersistentStartTime) >
          DALI_SAVE_PERSISTENT_TIME) // unsigned zero comparison with >= operator
    {
      // executing command SAVE PERSISTENT VARIABLES
      pCtrlInst->gearSavePersistentRunning = FALSE;
      gear_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->gearPersistent);
    }
  }
  if (0 < pCtrlInst->gearPersistentChangedTime &&
      dali_difftime(pInst->currentTime, pCtrlInst->gearPersistentChangedTime ) >=
        DALI_SAVE_PERS_DELAY_MAX - ((PDaliLibInstance)pCtrlInst->pInst)->config.internal.savePersistentDelayReduction * 1000)
  {
    // persistent variables changed, but not stored yet
    pCtrlInst->gearPersistentChangedTime = 0;
    gear_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->gearPersistent);
  }

  // Reset time
  if (TRUE == pCtrlInst->gearResetRunning)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearResetStartTime) >= DALI_CTRL_GEAR_RESET_TIME_MAX)
    {
      pCtrlInst->gearResetRunning = FALSE;
    }
    return;
  }
  // Reset memory bank time
  if (TRUE == pCtrlInst->gearResetMemoryRunning)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearResetMemoryStartTime) >=
          DALI_CTRL_GEAR_MEM_RESET_TIME_MAX)
    {
      pCtrlInst->gearResetMemoryRunning = FALSE;
    }
    return;
  }
  // query lamp failure
  if (TRUE == pCtrlInst->gearVariables.lampFailure)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearLampFailureStatusTimer) >=
          DALI_CTRL_LAMP_FAILURE_TIMEOUT)
    {
      pCtrlInst->gearLampFailureStatusTimer = pInst->currentTime;
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_QUERY_LAMPFAILURE,
                    NOT_USED_PARAM);
      return;
    }
  }
  // enable DAPC Sequence
  if (TRUE == pCtrlInst->gearEnableDAPCRunning)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->gearEnableDAPCStartTime) > DIMM_RATE_TIME)
    {
      pCtrlInst->gearEnableDAPCRunning = FALSE;
      gear_stop_dimming(pCtrlInst);
      return;
    }
  }
  // fadeTime
  if (TRUE == pCtrlInst->gearVariables.fadeRunning)
  {
    // first timestamp
    if (0 == pCtrlInst->gearDimm.fadeTimestamp)
    {
      pCtrlInst->gearDimm.fadeTimestamp = pInst->currentTime;
      pCtrlInst->gearDimm.startFadeTimestamp = pInst->currentTime;
      pCtrlInst->gearDimm.startDimmLevel = pCtrlInst->gearVariables.actualLevel;
    }

    // check the dimmtime duration for the fadeRate 200ms
    if (DIMM_TYPE_DIMMRATE == pCtrlInst->gearDimm.dimmType)
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->gearDimm.startFadeTimestamp) >= DIMM_RATE_TIME)
      {
        gear_stop_dimming(pCtrlInst);
        return;
      }
    }
    double newLevel = gear_calc_actual_level(pCtrlInst);

    if (DIMM_DIRECTION_UP == pCtrlInst->gearDimm.dimmDirection)
    {
      if (newLevel >= pCtrlInst->gearVariables.targetLevel ||
          newLevel >= pCtrlInst->gearPersistent.gearPersVariables.maxLevel)
      {
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst),
                                         pCtrlInst->gearVariables.targetLevel));
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        gear_stop_dimming(pCtrlInst);
        return;
      }
    }
    else // DIMM_DIRECTION_DOWN
    {
      if (newLevel <= pCtrlInst->gearVariables.targetLevel ||
          newLevel <= pCtrlInst->gearPersistent.gearPersVariables.minLevel)
      {
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), pCtrlInst->gearVariables.targetLevel));
        if (pCtrlInst->gearVariables.targetLevel != pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel)
        {
          pCtrlInst->gearPersistent.gearPersVariables.lastLightLevel = pCtrlInst->gearVariables.targetLevel;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        if (0 != pCtrlInst->gearVariables.targetLevel)
        {
          pCtrlInst->gearVariables.lastActiveLevel = pCtrlInst->gearVariables.targetLevel;
        }
        // wait at least 1 * stepTime before stop dimming
        if (dali_difftime(pInst->currentTime, pCtrlInst->gearDimm.startFadeTimestamp) >=
              (ceil(pCtrlInst->gearDimm.stepTime/1000)))
        {
          gear_stop_dimming(pCtrlInst);
        }
        return;
      }
    }
    // High Resolution Dimming
    if (pCtrlInst->gearPersistent.gearPersVariables.highResDimming)
    {
      gear_reaction(pCtrlInst,
                    DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                    gear_level2percent(getDimmingCurve(pCtrlInst), newLevel));
      pCtrlInst->gearVariables.actualLevel = (uint32_t)round(newLevel);
    }
    // Standard Dimming
    else
    {
      if ((uint8_t)round(newLevel) != pCtrlInst->gearVariables.actualLevel)
      {
        gear_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL,
                      gear_level2percent(getDimmingCurve(pCtrlInst), newLevel));

        pCtrlInst->gearDimm.fadeTimestamp = pInst->currentTime;
        pCtrlInst->gearVariables.actualLevel = (uint32_t)round(newLevel);
      }
    }
  }
  else
  {
    pCtrlInst->gearDimm.fadeTimestamp = 0;
  }

  // call control gear extensions' timing helper
  #ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT
    gear_diag_maintenance_timingHelper(pCtrlInstance);
  #endif
}

//----------------------------------------------------------------------
// will be called, if cmdCounter >= 0
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_send_more(PDaliLibInstance pInst,
                                     send_frame_t*    pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t nextCmd = pInst->last_tx_frame.cmds[pInst->last_tx_frame.cmdCounter-1];

  switch(pInst->last_tx_frame.lastActionType)
  {
    case DALILIB_ACT_TYPE_CTRL_GEAR_LED:
      {
        ctrl_gear_commands_t* pLEDCmd = gear_led_get_cmd((LED_CMD)nextCmd);
        if (NULL == pLEDCmd)
        {
          // command is not a LED specific command
          pLEDCmd = gear_get_cmd_struct_by_cmd((GEAR_CMD)nextCmd);
        }
        if (NULL != pLEDCmd)
        {
          rc = gear_create_ff(pInst,
                              pLEDCmd,
                              GEAR_CMD_NONE,
                              pNewSendFrame,
                              &pInst->last_tx_frame.adr.lastGearAdr,
                              pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
        else
        {
          rc = R_DALILIB_FRAME_NOT_SEND;
        }
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_COLOUR:
      {
        ctrl_gear_commands_t* pColourCmd = gear_colour_get_cmd((COLOUR_CMD)nextCmd);
        if (NULL == pColourCmd)
        {
          // command is not a colour specific command
          pColourCmd = gear_get_cmd_struct_by_cmd((GEAR_CMD)nextCmd);
        }
        if (NULL != pColourCmd)
        {
          rc = gear_create_ff(pInst,
                              pColourCmd,
                              GEAR_CMD_NONE,
                              pNewSendFrame,
                              &pInst->last_tx_frame.adr.lastGearAdr,
                              pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
        else
        {
          rc = R_DALILIB_FRAME_NOT_SEND;
        }
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_SWITCHING:
      {
        ctrl_gear_commands_t* pSwitchingCmd = gear_switching_get_cmd((SWITCHING_CMD)nextCmd);
        if (NULL != pSwitchingCmd)
        {
          rc = gear_create_ff(pInst,
                              pSwitchingCmd,
                              GEAR_CMD_NONE,
                              pNewSendFrame,
                              &pInst->last_tx_frame.adr.lastGearAdr,
                              pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
        else
        {
          rc = R_DALILIB_FRAME_NOT_SEND;
        }
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_FLUORESCENT:
    default:
      rc = gear_create_ff(pInst,
                          NULL,
                          nextCmd,
                          pNewSendFrame,
                          &pInst->last_tx_frame.adr.lastGearAdr,
                          pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// control gear action functions entry
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_action(PDaliLibInstance         pInst,
                                  dalilib_act_ctrl_gear_t* pAct,
                                  send_frame_t*            pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR != pAct->gearActionType)
  {
    if (FALSE == pInst->ctrlInst.deviceInstance.devicePersistent.persVariables.applicationActive)
    {
      return (R_DALILIB_APPLICATION_CONTROLLER_DISABLED);
    }
    if (QUIESCENT_MODE_ENABLED == pInst->ctrlInst.deviceInstance.deviceVariables.quiescentMode)
    {
      return (R_DALILIB_CTRL_DEVICE_QUIESCENT_MODE_ENABLED);
    }
  }

  switch(pAct->gearActionType)
  {
    // stack intern actions, called by application
    case DALILIB_ACT_TYPE_INTERNAL_CTRL_GEAR:
      {
        rc = gear_action_internal(&pInst->ctrlInst.gearInstance, pAct, pNewSendFrame);
      }
    break;

    // application controller actions for control gear(s)
    case DALILIB_ACT_TYPE_CTRL_GEAR_CONFIG:
      {
        rc = gear_action_config(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_LEVEL:
      {
        rc = gear_action_level(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_SCENE:
      {
        rc = gear_action_scene(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_GROUP:
      {
        rc = gear_action_group(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_QUERY_STATUS:
      {
        rc = gear_create_ff(pInst, NULL, GEAR_CMD_QUERY_STATUS, pNewSendFrame, &pAct->adr, 0);
      }
    break;
    case DALILIB_ACT_TYPE_CTRL_GEAR_QUERIES:
      {
        rc = gear_action_queries(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_VENDOR:
      {
        rc = gear_action_vendor(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_INITIALISE:
      {
        rc = gear_action_initialise(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_MEMORY:
      {
        rc = gear_action_memory(pInst,pAct,pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_LED:
      {
        rc = gear_action_led(pInst, pAct, pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_COLOUR:
      {
        rc = gear_action_colour(pInst, pAct, pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_SWITCHING:
      {
          rc = gear_action_switching(pInst, pAct, pNewSendFrame);
      }
    break;

    case DALILIB_ACT_TYPE_CTRL_GEAR_FLUORESCENT:
      {
        rc = gear_action_fluorescent(pInst, pAct, pNewSendFrame);
      }
    break;

    default:
      dali_cb_log(pInst, rc, "not supported gear action");
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// initialize the variable of the control gear
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_gear_init(PDaliLibInstance pInst)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  pInst->ctrlInst.gearInstance.pInst = pInst;

  // time function pointer initialise
  pInst->ctrlInst.gearInstance.fpGearTimingHelper = ctrl_gear_timingHelper;

  // the values initialised from the application
  gear_init_membank0_data(pInst);

  // load the persistent variables from flash memory
  rc = gear_load_mem_blocks(pInst, &pInst->ctrlInst.gearInstance.gearPersistent);
  if (R_DALILIB_OK != rc)
  {
    // first hardware start or the application can't load the saved data
    gear_init_persistent(pInst, &pInst->ctrlInst.gearInstance.gearPersistent);
    if (FALSE == pInst->config.internal.savePersistentOnInit)
    {
      rc = R_DALILIB_OK;
    }
    else
    {
      // save recently initialized persistent memory
      pInst->ctrlInst.gearInstance.gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_VARS_FREQUENT_MASK |
                                                                 DALI_CTRL_GEAR_PERS_VARS_OPERATING_MASK |
                                                                 DALI_CTRL_GEAR_PERS_VARS_CONFIG_MASK |
                                                                 DALI_CTRL_GEAR_PERS_FLUORESCENT_MASK |
                                                                 DALI_CTRL_GEAR_PERS_LED_FREQUENT_MASK |
                                                                 DALI_CTRL_GEAR_PERS_LED_OPERATING_MASK |
                                                                 DALI_CTRL_GEAR_PERS_LED_CONFIG_MASK |
                                                                 DALI_CTRL_GEAR_PERS_COLOUR_MASK |
                                                                 DALI_CTRL_GEAR_PERS_SWITCHING_MASK |
                                                                 DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK |
                                                                 DALI_CTRL_GEAR_PERS_POWER_SUPPLY_MASK |
                                                                 DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK |
                                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK |
                                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK |
                                                                 DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
      rc = gear_save_mem_blocks(pInst, &pInst->ctrlInst.gearInstance.gearPersistent);
    }
  }

  // initialize other membanks
  dali_init_other_membanks(&pInst->ctrlInst.gearInstance.gearMemBanksOther);

  if (R_DALILIB_OK == rc)
  {
    // initialise the control gear
    gear_init_after_power_cycle (&pInst->ctrlInst.gearInstance);
    pInst->ctrlInst.gearInstance.gearPowerOnRunning = TRUE;
    // set lamp on the off, during power on level time out
    gear_reaction(&pInst->ctrlInst.gearInstance,
                  DALILIB_REACT_INTERNAL_CTRL_GEAR_SET_ACTUAL_LEVEL, 0x00);
  }

  return (rc);
}


