/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_energy_reporting.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"

#ifdef DALILIB_GEAR_ENERGY_REPORT_PRESENT


// eGearEnergyReportingCmd,                           selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t energy_reporting_commands_m[] =
{
  {(GEAR_CMD) ENERGY_REPORTING_QUERY_EXTENDED_VERSION_NUMBER,   1,       0xFF,  1, 0},
  {(GEAR_CMD) ENERGY_REPORTING_CMD_LAST,                        0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search energy reporting command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* energy_reporting_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(energy_reporting_commands_m); idx++)
  {
    if (energy_reporting_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&energy_reporting_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// initialize the memory after external power cycle
//----------------------------------------------------------------------
void gear_energy_reporting_init_after_power_cycle(PCtrlGearInstance pCtrlInst)
{
  dalilib_vendor_energy_reporting_t*  pConfig = NULL;

  pConfig = &((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.energy_reporting_params;

  // init variables
  pCtrlInst->gearVariables.energyReportingVariables.extendedVersionNumber = CTRL_GEAR_ENERGY_REPORTING_EXTENDED_VERSION_NUMBER;
  pCtrlInst->gearVariables.energyReportingVariables.deviceType = GEAR_TYPE_CONTROL_GEAR_ENERGY_REPORTING;

  // initialize memory bank 202 RAM memory cells
  pCtrlInst->gearMemBank202RAM.lockByte = DALI_MASK;
  pCtrlInst->gearMemBank202RAM.activePower[0] = pConfig->activePower >> 24;
  pCtrlInst->gearMemBank202RAM.activePower[1] = pConfig->activePower >> 16;
  pCtrlInst->gearMemBank202RAM.activePower[2] = pConfig->activePower >> 8;
  pCtrlInst->gearMemBank202RAM.activePower[3] = pConfig->activePower;
}

//----------------------------------------------------------------------
// reset energy reporting memory banks
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_energy_reporting_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                         uint8_t           nMembank)
{
  // check whether to reset memory bank 202
  if ((CTRL_GEAR_ENERGY_REPORTING_FIRST_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank202RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank202RAM.lockByte = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read memory bank 202
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_energy_reporting_read_memory_bank202(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_202_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_202_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank202RAM.lockByte;
      break;

      case CTRL_GEAR_MEMORY_BANK_202_VERSION_BYTE:
        *pRespValue = CTRL_GEAR_MEMORY_BANK_202_VERSION;
      break;

      case CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_POWER_BYTE:
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.energy_reporting_params.ScaleFactorForActivePower;
      break;

      case CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_ENERGY_BYTE:
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.energy_reporting_params.scaleFactorForActiveEnergy;
      break;

      default:
        if ((CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE >= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_202_ENERGY_END_BYTE   <= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank202NVM.
              activeEnergy[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE)];
        }
        else
        {
          if ((CTRL_GEAR_MEMORY_BANK_202_POWER_START_BYTE >= pCtrlInst->gearVariables.DTR0) &&
              (CTRL_GEAR_MEMORY_BANK_202_POWER_END_BYTE   <= pCtrlInst->gearVariables.DTR0))
          {
            *pRespValue = pCtrlInst->gearMemBank202RAM.
                activePower[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_202_POWER_START_BYTE)];
          }
        }
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write memory bank 202
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_energy_reporting_write_memory_bank202_direct(PCtrlGearInstance  pCtrlInst,
                                                                   uint8_t            nCell,
                                                                   uint8_t            reqValue)

{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  // internal memory cell access
  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_202_ADDRESS_LAST_CELL)
  {
    switch (nCell)
    {
      case 0x00: // Address of last addressable memory location
      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        pCtrlInst->gearMemBank202RAM.lockByte = reqValue;
      break;


      case CTRL_GEAR_MEMORY_BANK_202_VERSION_BYTE:
      case CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_POWER_BYTE:
      case CTRL_GEAR_MEMORY_BANK_202_SCALE_FACTOR_ENERGY_BYTE:
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      default:
        rc = R_IGNORE_FRAME;
        if ((CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE >= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_202_ENERGY_END_BYTE   <= pCtrlInst->gearVariables.DTR0))
        {
          // write memory cell
          if (reqValue != pCtrlInst->gearPersistent.gearMemBank202NVM.
                            activeEnergy[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE)])
          {
            pCtrlInst->gearPersistent.gearMemBank202NVM.
              activeEnergy[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_202_ENERGY_START_BYTE)] =
                reqValue;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
          rc = R_DALILIB_OK;
          break;
        }
        if ((CTRL_GEAR_MEMORY_BANK_202_POWER_START_BYTE >= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_202_POWER_END_BYTE   <= pCtrlInst->gearVariables.DTR0))
        {
          // write memory cell
          pCtrlInst->gearMemBank202RAM.
            activePower[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_202_POWER_START_BYTE)] =
              reqValue;
          rc = R_DALILIB_OK;
          break;
        }
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// write memory bank 202
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_energy_reporting_write_memory_bank202(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue)
{
  uint8_t           nCell;

  nCell = pCtrlInst->gearVariables.DTR0;

  // write memory bank cell
  // check lock byte cell offset
  if(DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell)
  {
    // not writable
    return (R_IGNORE_FRAME);
  }
  // write lock byte memory cell
  pCtrlInst->gearMemBank202RAM.lockByte = reqValue;

  // check whether to reply
  if ((GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (R_DALILIB_OK);
}

R_DALILIB_RESULT gear_energy_report_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                             dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_gear_energy_report_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_ENERGY_REPORT_ID;
  block.version = DALI_CTRL_GEAR_PERS_ENERGY_REPORT_VERSION;

  // populate memory block
  memcpy(block.activeEnergy, pGearPersistent->gearMemBank202NVM.activeEnergy, sizeof(block.activeEnergy));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_ENERGY_REPORT_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_ENERGY_REPORT_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_energy_report_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                              uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                     rc = R_DALILIB_OK;
  dali_ctrl_mem_bank202_NVM_t*                         pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank202NVM;
  dali_ctrl_gear_energy_report_pers_variables_block_t *pBlock = (dali_ctrl_gear_energy_report_pers_variables_block_t*)pDaliMem;

  // copy mem block
  memcpy(pPersistentVariables->activeEnergy, pBlock->activeEnergy, sizeof(pPersistentVariables->activeEnergy));

  return (rc);
}


//----------------------------------------------------------------------
// process the energy reporting forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_energy_reporting_process_forward(PCtrlGearInstance      pCtrlInst,
                                                       dalilib_frame_t*       pForwardFrame,
                                                       ctrl_gear_commands_t** ppGearCmd,
                                                       gear_adr_t*            pReceivedAdr,
                                                       uint8_t*               pRespValue)
{
  R_DALILIB_RESULT      rc          = R_DALILIB_OK;

  (void)pReceivedAdr;

  *ppGearCmd = energy_reporting_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case POWER_SUPPLY_QUERY_EXTENDED_VERSION_NUMBER:
      *pRespValue = pCtrlInst->gearVariables.energyReportingVariables.extendedVersionNumber;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_energy_reporting_get_cmd(ENERGY_REPORTING_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(energy_reporting_commands_m); idx++)
  {
    if ((ENERGY_REPORTING_CMD)energy_reporting_commands_m[idx].eGearCmd == eCmd)
    {
      return (&energy_reporting_commands_m[idx]);
    }
  }

  return (NULL);
}

#endif /* DALILIB_GEAR_ENERGY_REPORT_PRESENT */
