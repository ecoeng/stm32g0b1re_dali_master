/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * device.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"
#include "device.h"

#define OCCUPANCY_SENSOR_EVENT_FILTER_OCCUPIED_TRIGGER    0x01 // Bit-0
#define OCCUPANCY_SENSOR_EVENT_FILTER_VACANT_TRIGGER      0x02 // Bit-1
#define OCCUPANCY_SENSOR_EVENT_FILTER_REPEAT_TRIGGER      0x04 // Bit-2
#define OCCUPANCY_SENSOR_EVENT_FILTER_MOVEMENT_TRIGGER    0x08 // Bit-3
#define OCCUPANCY_SENSOR_EVENT_FILTER_NO_MOVEMENT_TRIGGER 0x10 // Bit-4

// eDevCmd,                      inst, opcode,  a, t
static ctrl_device_commands_t occupancysensor_commands_m[] =
{
  {OCCUPANCY_CMD_CATCH_MOVEMENT,      0x00,0x20,0,0},
  {OCCUPANCY_CMD_SET_HOLD_TIMER,      0x00,0x21,0,1},
  {OCCUPANCY_CMD_SET_REPORT_TIMER,    0x00,0x22,0,1},
  {OCCUPANCY_CMD_SET_DEADTIME_TIMER,  0x00,0x23,0,1},
  {OCCUPANCY_CMD_CANCEL_HOLD_TIMER,   0x00,0x24,0,0},

  {OCCUPANCY_CMD_QUERY_DEADTIME_TIMER,0x00,0x2C,1,0},
  {OCCUPANCY_CMD_QUERY_HOLD_TIMER,    0x00,0x2D,1,0},
  {OCCUPANCY_CMD_QUERY_REPORT_TIMER,  0x00,0x2E,1,0},
  {OCCUPANCY_CMD_QUERY_CATCHING,      0x00,0x2F,1,0},

  {OCCUPANCY_CMD_LAST,                0x00,0x00,0,0},
};

//----------------------------------------------------------------------
// Here it is checked whether the event filter flag is set or not
// Bit-0: Occupied trigger enabled?     default: 1
// Bit-1: Vacant trigger enabled?       default: 1
// Bit-2: Repeat trigger enabled?       default: 0
// Bit-3: Movement trigger enabled?     default: 0
// Bit-4: No movement trigger enabled?     default: 0
// Bit-5: Reserved               default: 0
// Bit-6: Reserved               default: 0
// Bit-7: Reserved               default: 0
//
// return: R_DALILIB_OK :
//                  - event flag bit is set
//         R_DALILIB_CTRL_DEVICE_IGNORE_EVENT :
//                  - event flag bit is not set
//         R_DALILIB_NOT_SUPPORTED_ACTION :
//                  - unknown/not supported event action
//-----------------------------------------------------------------------
static R_DALILIB_RESULT occupancy_check_event(PCtrlDeviceInstance pCtrlInst,
                                              uint8_t             occupancySensorActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(occupancySensorActionCode)
  {
    // event messages
    // No movement detected.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT:
      rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
            OCCUPANCY_SENSOR_EVENT_FILTER_NO_MOVEMENT_TRIGGER) ?
            R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    // Movement detected.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT:
      rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
            OCCUPANCY_SENSOR_EVENT_FILTER_MOVEMENT_TRIGGER) ?
            R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    // The area has become vacant.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_VACANT:
      rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
            OCCUPANCY_SENSOR_EVENT_FILTER_VACANT_TRIGGER) ?
            R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    // The area has become occupied.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_OCCUPIED:
      rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
            OCCUPANCY_SENSOR_EVENT_FILTER_OCCUPIED_TRIGGER) ?
            R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    // The area is still vacant.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_STILL_VACANT:
    // The area is still occupied.
    case DALILIB_ACT_EVENT_OCCUPANCY_SENSOR_STILL_OCCUPIED:
      rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter &
            OCCUPANCY_SENSOR_EVENT_FILTER_REPEAT_TRIGGER) ?
            R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

/******************************************************************************
* calculate and set the dead time
*******************************************************************************/
static void occupancy_calc_dead_time(occupancy_sensor_variables_t*      pOccupancySensor,
                                     occupancy_sensor_pers_variables_t* pOccupancySensorPersistentVariables)
{
  uint32_t deadTime = 0;

  if (pOccupancySensorPersistentVariables->tDeadtime <= 0)
  {
    pOccupancySensor->timeDead = deadTime; // disabled
    return;
  }

  deadTime = pOccupancySensorPersistentVariables->tDeadtime * INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_INCR;

  if (deadTime > INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_MAX)
  {
    deadTime = INPUT_DEVICE_OCCUPANCY_SENSOR_DEAD_T_MAX;
  }

  pOccupancySensor->timeDead = deadTime;
}

/******************************************************************************
* calculate and set the hold time
*******************************************************************************/
static void occupancy_calc_hold_time(occupancy_sensor_variables_t*      pOccupancySensor,
                                     occupancy_sensor_pers_variables_t* pOccupancySensorPersistentVariables)
{
  uint32_t holdTime = 0;

  holdTime = pOccupancySensorPersistentVariables->tHold * INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_INCR;

  if (holdTime > INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MAX)
  {
    holdTime = INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MAX;
  }

  // The minimum time in case “tHold” equals 0 shall be 1 s.
  if (holdTime <  INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MIN)
  {
    holdTime = INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_T_MIN;
  }

  pOccupancySensor->timeHold = holdTime;
}

/******************************************************************************
 * calculate and set the report time
*******************************************************************************/
static void occupancy_calc_report_time(occupancy_sensor_variables_t*      pOccupancySensor,
                                       occupancy_sensor_pers_variables_t* pOccupancySensorPersistentVariables)
{
  uint32_t reportTime = 0;

  if (pOccupancySensorPersistentVariables->tReport <= 0)
  {
    pOccupancySensor->timeReport = reportTime; // disabled
    return;
  }

  reportTime = pOccupancySensorPersistentVariables->tReport * INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_INCR;

  if (reportTime > INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_MAX)
  {
    reportTime = INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_T_MAX;
  }

  if (reportTime < pOccupancySensor->timeDead)
  {
    reportTime = pOccupancySensor->timeDead;
  }

  pOccupancySensor->timeReport = reportTime;
}


static void occupancy_build_event_information(occupancy_sensor_variables_t* pOccupancySensor,
                                              uint8_t                       bRepeat,
                                              uint16_t*                     pOccupancySensorEvent)
{
  uint16_t event_information = 0x00;

  *pOccupancySensorEvent = 0x00;

  // 00 0000 ----b
  // Bit: 3
  // Sensor type
  if (OCCUPANCY_MOVEMENT_SENSOR == pOccupancySensor->sensor_type)
  {
    event_information = 1;
  }
  else
  {
    event_information = 0;
  }
  event_information <<= 1;

  // Bit: 2
  // Repeat event
  event_information |= bRepeat;
  event_information <<= 1;

  // Bit: 1
  // Occupied or Vacant
  event_information |= (OCCUPIED == pOccupancySensor->state.lastAreaState ? 1 : 0);
  event_information <<= 1;

  // Bit: 0
  // Movement or No Movement
  event_information |= (MOVEMENT == pOccupancySensor->state.lastMovementState ? 1 : 0);

  *pOccupancySensorEvent = event_information;
}

//----------------------------------------------------------------------
// return: 0: No send event
//         1: Send event
//----------------------------------------------------------------------
static uint8_t occupancy_movement_sensor_interrupt(PCtrlDeviceInstance           pCtrlInst,
                                                   occupancy_sensor_variables_t* pOccupancySensor)
{
  dalilib_time    currentTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;

    if (MOVEMENT == pOccupancySensor->state.movementState)
    {
      if (VACANT == pOccupancySensor->state.lastAreaState)
      {
        pOccupancySensor->state.lastAreaState = OCCUPIED;
        pOccupancySensor->state.lastMovementState = MOVEMENT;
        pCtrlInst->deviceInstVariables.inputValue = 0xFF;

        pOccupancySensor->timeLastMovementEvent = currentTime; // re-trigger hold timer

        return (1);
      }
    }
    else // NO MOVEMENT
    {
      if (MOVEMENT == pOccupancySensor->state.lastMovementState)
      {
        pOccupancySensor->state.lastAreaState = OCCUPIED;
        pCtrlInst->deviceInstVariables.inputValue = 0xAA;

        return (1);
      }
    }

    return (0);
}

//----------------------------------------------------------------------
// return: 0: No send event
//         1: Send event
//----------------------------------------------------------------------
static uint8_t occupancy_presence_sensor_interrupt(PCtrlDeviceInstance           pCtrlInst,
                                                   occupancy_sensor_variables_t* pOccupancySensor)
{
  if (pOccupancySensor->state.movementState == pOccupancySensor->state.lastMovementState &&
      pOccupancySensor->state.areaState == pOccupancySensor->state.lastAreaState)
  {
    return (0);
  }

  if (VACANT == pOccupancySensor->state.lastAreaState)
  {
    if (VACANT == pOccupancySensor->state.areaState)
    {
      if (MOVEMENT == pOccupancySensor->state.movementState)
      {
          pCtrlInst->deviceInstVariables.inputValue = 0x55;
      }
      else // NO_MOVEMENT
      {
          pCtrlInst->deviceInstVariables.inputValue = 0x00;
      }
      pOccupancySensor->state.lastMovementState = pOccupancySensor->state.movementState;
    }
    else // OCCUPIED
    {
      if (MOVEMENT == pOccupancySensor->state.lastMovementState)
      {
          pCtrlInst->deviceInstVariables.inputValue = 0xFF;
      }
      else // NO_ MOVEMENT
      {
          pCtrlInst->deviceInstVariables.inputValue = 0xAA;
      }
      pOccupancySensor->state.lastAreaState = pOccupancySensor->state.areaState;
    }
  }
  else // (OCCUPIED == pOccupancySensor->state.lastAreaState)
  {
    if (OCCUPIED == pOccupancySensor->state.areaState)
    {
      if (MOVEMENT == pOccupancySensor->state.movementState)
      {
          pCtrlInst->deviceInstVariables.inputValue = 0xFF;
      }
      else // NO_MOVEMENT
      {
          pCtrlInst->deviceInstVariables.inputValue = 0xAA;
      }
      pOccupancySensor->state.lastMovementState = pOccupancySensor->state.movementState;
    }
    else // VACANT
    {
      if (MOVEMENT == pOccupancySensor->state.lastMovementState)
      {
          pCtrlInst->deviceInstVariables.inputValue = 0x55;
      }
      else // NO_ MOVEMENT
      {
          pCtrlInst->deviceInstVariables.inputValue = 0x00;
      }
      pOccupancySensor->state.lastAreaState = pOccupancySensor->state.areaState;
    }
  }

  return (1);
}

/******************************************************************************/
/* called when occupancy sensor state changes
*******************************************************************************/
static R_DALILIB_RESULT occupancy_interrupt(PCtrlDeviceInstance pCtrlInst,
                                            uint16_t*           pOccupancySensorEvent)
{
  uint16_t occupancyEvent = 0;
  uint8_t  bSendEvent = 0;

  occupancy_sensor_variables_t*  pOccupancySensor = &pCtrlInst->deviceVariables.occupancySensorVariables;

  if (OCCUPANCY_MOVEMENT_SENSOR == pOccupancySensor->sensor_type)
  {
    bSendEvent = occupancy_movement_sensor_interrupt(pCtrlInst, pOccupancySensor);
  }
  else
  {
    bSendEvent = occupancy_presence_sensor_interrupt(pCtrlInst, pOccupancySensor);
  }

  if (bSendEvent)
  {
    // is dead time set?
    if (pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables.tDeadtime > 0)
    {
      // dead time expired?
      if (dali_difftime(((PDaliLibInstance)pCtrlInst->pInst)->currentTime, pOccupancySensor->timeLastEvent) < pOccupancySensor->timeDead)
      {
        // dead time is not expired,
        // event shall not send
        bSendEvent = 0;
      }
    }
  }

  if (bSendEvent)
  {
    *pOccupancySensorEvent = 0;
    occupancy_build_event_information(pOccupancySensor, 0, &occupancyEvent);
    *pOccupancySensorEvent = occupancyEvent;
    pOccupancySensor->timeLastEvent = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
  }

  return (bSendEvent ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT);
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t dev_occupancy_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers)
{
  uint8_t retVal;
  
  if ( (INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_PRIO_DEFAULT != pInstPers->eventPriority) ||
       (INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_FILTER_DEFAULT != pInstPers->eventFilter) ||
       (INPUT_DEVICE_OCCUPANCY_SENSOR_DEADTIME_DEFAULT != pInstPers->occupancySensorPersVariables.tDeadtime) ||
       (INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_DEFAULT != pInstPers->occupancySensorPersVariables.tHold) ||
       (INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_DEFAULT != pInstPers->occupancySensorPersVariables.tReport) )
  {
    retVal = FALSE;
  }
  else
  {
    retVal = TRUE;
  }

  return (retVal);
}

//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void dev_occupancy_by_reset(PCtrlDevVariables               pVariables,
                            PCtrlDevInstPersistentVariables pInstPers)
{
  pInstPers->eventPriority = INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_FILTER_DEFAULT;

  pInstPers->occupancySensorPersVariables.tDeadtime = INPUT_DEVICE_OCCUPANCY_SENSOR_DEADTIME_DEFAULT;
  pInstPers->occupancySensorPersVariables.tHold = INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_DEFAULT;
  pInstPers->occupancySensorPersVariables.tReport = INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_DEFAULT;

  occupancy_calc_dead_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
  occupancy_calc_hold_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
  occupancy_calc_report_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
}

//----------------------------------------------------------------------
// initialize the default variable of the input device occupancy sensor
//----------------------------------------------------------------------
void dev_occupancy_init_persistent(PCtrlDevVariables               pVariables,
                                   PCtrlDevInstPersistentVariables pInstPers)
{
  pInstPers->instanceType = INPUT_DEVICE_INST_OCCUPANCY_SENSOR;
  pInstPers->resolution = INPUT_DEVICE_OCCUPANCY_SENSOR_RESOLUTION;
  pInstPers->eventPriority = INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = INPUT_DEVICE_OCCUPANCY_SENSOR_EVENT_FILTER_DEFAULT; // 0000 0011b

  pInstPers->occupancySensorPersVariables.tDeadtime = INPUT_DEVICE_OCCUPANCY_SENSOR_DEADTIME_DEFAULT;
  pInstPers->occupancySensorPersVariables.tHold = INPUT_DEVICE_OCCUPANCY_SENSOR_HOLD_DEFAULT;
  pInstPers->occupancySensorPersVariables.tReport = INPUT_DEVICE_OCCUPANCY_SENSOR_REPORT_DEFAULT;

  occupancy_calc_dead_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
  occupancy_calc_hold_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
  occupancy_calc_report_time(&pVariables->occupancySensorVariables, &pInstPers->occupancySensorPersVariables);
}


//----------------------------------------------------------------------
// result event information for occupancy sensor
// (Occupancy sensor DIN 62386-303)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_occupancy_eventinfo(PCtrlDeviceInstance                pCtrlInst,
                                         DALILIB_ACT_EVENT_OCCUPANCY_SENSOR occupancySensorActionCode,
                                         uint16_t*                          pOccupancySensorEvent)
{
  R_DALILIB_RESULT         rc = R_DALILIB_OK;
  uint8_t                         bInternal = TRUE;
  occupancy_sensor_variables_t*   pOccupancySensor = &pCtrlInst->deviceVariables.occupancySensorVariables;

  switch(occupancySensorActionCode)
  {
    // internal event messages
    case DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_VACANT:
      {
        pOccupancySensor->state.areaState = VACANT;
      }
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_OCCUPIED:
      {
        pOccupancySensor->state.areaState = OCCUPIED;
      }
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT:
      {
        pOccupancySensor->state.movementState = NO_MOVEMENT;
      }
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_OCCUPANCY_SENSOR_MOVEMENT:
      {
        pOccupancySensor->state.movementState = MOVEMENT;
      }
    break;
    
    default:
      {
        rc = R_DALILIB_NOT_SUPPORTED_ACTION;
        bInternal = FALSE;
      }
    break;
  }

  if (R_DALILIB_OK == rc)
  {
    rc = occupancy_interrupt(pCtrlInst, pOccupancySensorEvent);
  }

  if (!bInternal)
  {
    rc = occupancy_check_event(pCtrlInst, occupancySensorActionCode);
  }

  return rc;
}

//----------------------------------------------------------------------
// result action code for event information
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_occupancy_reactioncode(uint32_t                            frameData,
                                            DALILIB_CTRL_DEVICE_REACTION_CODES* pOccupancySensorReActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint16_t eventInfo = 0x0000;

  *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_UNKNOWN;

  eventInfo = (frameData & 0x01); // 00 0000 ---Xb

  if (0x00 == eventInfo)
  {
    *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_NO_MOVEMENT;
  } 
  else 
  {
    if(0x01 == eventInfo)
    {
      *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT;
    }
  }
  
  if (DALILIB_REACT_EVENT_UNKNOWN != *pOccupancySensorReActionCode)
  {
    // event found
    return (rc);
  }
  eventInfo = (frameData & 0x06); // 00 0000 -XX-b
  if(0x00 == eventInfo)
  {
    *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_VACANT;
  } 
  else
  {
    if(0x04 == eventInfo)
    {
      *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_STILL_VACANT;
    }
    else
    {
      if(0x02 == eventInfo)
      {
        *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_OCCUPIED;
      }
      else 
      {
        if(0x06 == eventInfo)
        {
          *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_STILL_OCCUPIED;
        }
      }
    }
  }
  
  if (DALILIB_REACT_EVENT_UNKNOWN != *pOccupancySensorReActionCode)
  {
    // event found
    return (rc);
  }
  eventInfo = (frameData & 0x08); // 00 0000 X---b
  if(0x00 == eventInfo)
  {
    *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_PRESENCE_SENSOR;
  }
  else
  {
    if(0x08 == eventInfo)
    {
      *pOccupancySensorReActionCode = DALILIB_REACT_EVENT_OCCUPANCY_SENSOR_MOVEMENT_SENSOR;
    }
  }
  return (rc);
}


//----------------------------------------------------------------------
// result opcode byte for occupancy sensor command
// (Occupancy sensor DIN 62386-303)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_occupancy_opcode(DALILIB_ACT_OCCUPANCY_SENSOR occupancySensorActionCode,
                    ctrl_device_commands_t *pDevCmd)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  OCCUPANCY_SENSOR_CMD occupancySensorCmd = OCCUPANCY_CMD_LAST;
  switch(occupancySensorActionCode)
  {
    // Once a movement detected event has been sent.
    case DALILIB_ACT_OCCUPANCY_SENSOR_CATCH_MOVEMENT:
      occupancySensorCmd = OCCUPANCY_CMD_CATCH_MOVEMENT;
    break;
    // If the hold timer is implemented, shall set the hold timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_HOLD_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_SET_HOLD_TIMER;
    break;
    // Set the repeat timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_REPORT_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_SET_REPORT_TIMER;
    break;
    // Set the dead time timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_DEADTIME_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_SET_DEADTIME_TIMER;
    break;
    // Cancel hold timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_CANCEL_HOLD_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_CANCEL_HOLD_TIMER;
    break;
    // Query the dead time timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_DEADTIME_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_QUERY_DEADTIME_TIMER;
    break;
    // Query hold timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_HOLD_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_QUERY_HOLD_TIMER;
    break;
    // Query the report timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_REPORT_TIMER:
      occupancySensorCmd = OCCUPANCY_CMD_QUERY_REPORT_TIMER;
    break;
    // Query catching status
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_CATCHING:
      occupancySensorCmd = OCCUPANCY_CMD_QUERY_CATCHING;
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  if (R_DALILIB_OK == rc && OCCUPANCY_CMD_LAST != occupancySensorCmd)
  {
    pDevCmd->opcodeByte = occupancysensor_commands_m[occupancySensorCmd].opcodeByte;
    pDevCmd->twice = occupancysensor_commands_m[occupancySensorCmd].twice;
    pDevCmd->eDevCmd = occupancySensorCmd;
    pDevCmd->answer = occupancysensor_commands_m[occupancySensorCmd].answer;
  }
  return (rc);
}

uint8_t dev_occupancy_get_error(PCtrlDeviceInstance pCtrlInst)
{
  uint8_t errorByte = 0xFF;

  if (TRUE == pCtrlInst->deviceVariables.occupancySensorVariables.bPhysicalSensorFailure)
  {
    errorByte = 0x01;
  }

  return (errorByte);
}


//----------------------------------------------------------------------
// search occupancy sensor command for instance byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
ctrl_device_commands_t* dev_occupancy_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;
  for (idx = 0; idx < COUNTOF(occupancysensor_commands_m); idx++)
  {
    if (occupancysensor_commands_m[idx].instanceByte == 0x00 &&
        occupancysensor_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&occupancysensor_commands_m[idx]);
    }
  }
  return (NULL);
}


//----------------------------------------------------------------------
// process the instance configuration forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_occupancy_process_forward(PCtrlDeviceInstance    pCtrlInst,
                                              ctrl_device_commands_t* pDevCmd,
                                              uint8_t*                pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  occupancy_sensor_pers_variables_t* pOccupancySensorPersistentVariables   = NULL;
  occupancy_sensor_variables_t* pOccupancySensorVariables = NULL;

  pOccupancySensorPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables;
  pOccupancySensorVariables = &pCtrlInst->deviceVariables.occupancySensorVariables;
  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case OCCUPANCY_CMD_CATCH_MOVEMENT:
      {
        rc = R_IGNORE_FRAME;
        if (!(pCtrlInst->devicePersistent.instPersVariables.eventFilter &
              OCCUPANCY_SENSOR_EVENT_FILTER_MOVEMENT_TRIGGER))
        {
          pOccupancySensorVariables->bCatching = TRUE;
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    case OCCUPANCY_CMD_SET_HOLD_TIMER:
      {
        rc = R_IGNORE_FRAME;
        if (pCtrlInst->deviceVariables.DTR0 <= 254)
        {
          pOccupancySensorPersistentVariables->tHold = pCtrlInst->deviceVariables.DTR0;
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    case OCCUPANCY_CMD_SET_REPORT_TIMER:
      {
        pOccupancySensorPersistentVariables->tReport = pCtrlInst->deviceVariables.DTR0;
        rc = R_DALILIB_OK;
      }
    break;
    
    case OCCUPANCY_CMD_SET_DEADTIME_TIMER:
      {
        pOccupancySensorPersistentVariables->tDeadtime = pCtrlInst->deviceVariables.DTR0;
        rc = R_DALILIB_OK;
      }
    break;
    
    case OCCUPANCY_CMD_CANCEL_HOLD_TIMER:
      {
        rc = R_IGNORE_FRAME;
        // TODO!
        // timer running ?
        // yes, shall clear the timer and generate a 'vacant' trigger
        if (DALI_MASK != pOccupancySensorPersistentVariables->tHold)
        {
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    case OCCUPANCY_CMD_QUERY_DEADTIME_TIMER:
      {
        *pRespValue = pOccupancySensorPersistentVariables->tDeadtime;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case OCCUPANCY_CMD_QUERY_HOLD_TIMER:
      {
        if (DALI_MASK == pOccupancySensorPersistentVariables->tHold)
        {
          *pRespValue = DALI_MASK;
        }
        else
        {
          *pRespValue = pOccupancySensorPersistentVariables->tHold;
        }
        pDevCmd->answer = TRUE;
      }
    break;
    
    case OCCUPANCY_CMD_QUERY_REPORT_TIMER:
      {
        *pRespValue = pOccupancySensorPersistentVariables->tReport;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case OCCUPANCY_CMD_QUERY_CATCHING:
      {
        rc = R_IGNORE_FRAME;
        if (TRUE == pOccupancySensorVariables->bCatching)
        {
          *pRespValue = DALI_MASK;
          pDevCmd->answer = TRUE;
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// occupancy sensor timer function
//----------------------------------------------------------------------
void dev_occupancy_timingHelper(PCtrlDeviceInstance pCtrlInst)
{
  PDaliLibInstance pInst       = pCtrlInst->pInst;
  dalilib_time     currentTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;

  occupancy_sensor_pers_variables_t* pOccupancySensorPersistentVariables   = NULL;
  occupancy_sensor_variables_t*      pOccupancySensorVariables             = NULL;

  pOccupancySensorPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables;
  pOccupancySensorVariables           = &pCtrlInst->deviceVariables.occupancySensorVariables;

  send_frame_t newSendFrame;
  device_adr_t deviceAdr;
  uint16_t     eventValue = 0;

  memset(&newSendFrame, 0, sizeof(newSendFrame));
  memset(&deviceAdr, 0, sizeof(deviceAdr));

  if (NULL == pInst)
  {
    return;
  }

  // is dead time set?
  if (pOccupancySensorPersistentVariables->tDeadtime > 0)
  {
    // dead time expired?
    if (dali_difftime(currentTime, pOccupancySensorVariables->timeLastEvent) < pOccupancySensorVariables->timeDead)
    {
      // dead time is not expired,
      // event shall not send
      return;
    }
  }

  newSendFrame.frame.priority   = pCtrlInst->devicePersistent.instPersVariables.eventPriority;
  newSendFrame.frame.datalength = FRAME_CTRL_DEV_FF_LEN;
  newSendFrame.type         = SEND_FRAME_TYPE_CTRL_EVENT;

  deviceAdr.adr         = pCtrlInst->devicePersistent.persVariables.shortAddress;
  deviceAdr.adrType       = DALI_ADRTYPE_SHORT;
  deviceAdr.instanceAdrType   = DALI_INSTANCE_ADRTYPE_TYPE;
  deviceAdr.instance       = pCtrlInst->devicePersistent.instPersVariables.instanceType;

  if (OCCUPANCY_MOVEMENT_SENSOR == pOccupancySensorVariables->sensor_type)
  {
    if (dali_difftime(currentTime, pOccupancySensorVariables->timeLastMovementEvent) > pOccupancySensorVariables->timeHold &&
        VACANT != pOccupancySensorVariables->state.lastAreaState)
    {
      pOccupancySensorVariables->state.lastAreaState =
          pOccupancySensorVariables->state.areaState = VACANT;

      pOccupancySensorVariables->state.lastMovementState =
          pOccupancySensorVariables->state.movementState = NO_MOVEMENT;

      // Vacant-No-Movement Event
      pCtrlInst->deviceInstVariables.inputValue = 0x00;

      occupancy_build_event_information(pOccupancySensorVariables, 1, &eventValue);
      pOccupancySensorVariables->timeLastMovementEvent = currentTime; // re-trigger hold timer
      pOccupancySensorVariables->timeLastEvent = currentTime; // re-trigger report timer

      ctrl_input_device_create_event(pInst, &newSendFrame, &deviceAdr, &eventValue);
      dali_cb_send(pInst, &newSendFrame);
      return;
    }

    if (pOccupancySensorPersistentVariables->tReport > 0 &&
        dali_difftime(currentTime, pOccupancySensorVariables->timeLastEvent) > pOccupancySensorVariables->timeReport)
    {
      if (VACANT == pOccupancySensorVariables->state.lastAreaState)
      {
        // Vacant-No-Movement Event
          pCtrlInst->deviceInstVariables.inputValue = 0x00;
      }
      else // (OCCUPIED == pOccupancySensorVariables->state.lastAreaState)
      {
        if (NO_MOVEMENT == pOccupancySensorVariables->state.lastMovementState)
        {
          // Occupied-No-Movement Event
            pCtrlInst->deviceInstVariables.inputValue = 0xAA;
        }
        else // MOVEMENT
        {
          // Occupied-Movement Event
            pCtrlInst->deviceInstVariables.inputValue = 0xFF;
        }
      }
      occupancy_build_event_information(pOccupancySensorVariables, 1, &eventValue);
      pOccupancySensorVariables->timeLastEvent = currentTime; // re-trigger report timer

      ctrl_input_device_create_event(pInst, &newSendFrame, &deviceAdr, &eventValue);
      dali_cb_send(pInst, &newSendFrame );

      return;
    }
  }

  if (OCCUPANCY_PRESENCE_SENSOR == pOccupancySensorVariables->sensor_type)
  {
    if (pOccupancySensorPersistentVariables->tReport > 0)
    {
      if (dali_difftime(currentTime, pOccupancySensorVariables->timeLastEvent) > pOccupancySensorVariables->timeReport)
      {
        occupancy_build_event_information(pOccupancySensorVariables, 1, &eventValue);
        pOccupancySensorVariables->timeLastEvent = currentTime;

        ctrl_input_device_create_event(pInst, &newSendFrame, &deviceAdr, &eventValue);
        dali_cb_send(pInst, &newSendFrame );

        return;
      }
    }
  }
}

