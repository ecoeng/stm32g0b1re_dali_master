/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_switching.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"
#include "gear.h"

// eGearSWITCHINGCmd,                                 selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t switching_commands_m[] =
{
  {(GEAR_CMD) SWITCHING_REFERENCE_SYSTEM_POWER,                 1,       0xE0,  0, 1},
  {(GEAR_CMD) SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD,    1,       0xE1,  0, 1},
  {(GEAR_CMD) SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD,   1,       0xE2,  0, 1},
  {(GEAR_CMD) SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD,  1,       0xE3,  0, 1},
  {(GEAR_CMD) SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD, 1,       0xE4,  0, 1},
  {(GEAR_CMD) SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME,       1,       0xE5,  0, 1},
  {(GEAR_CMD) SWITCHING_QUERY_FEATURES,                         1,       0xF0,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_SWITCH_STATUS,                    1,       0xF1,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD,           1,       0xF2,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD,          1,       0xF3,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD,         1,       0xF4,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD,        1,       0xF5,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_ERROR_HOLD_OFF_TIME,              1,       0xF6,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_GEAR_TYPE,                        1,       0xF7,  1, 0},

  {(GEAR_CMD) SWITCHING_QUERY_REFERENCE_RUNNING,                1,       0xF9,  1, 0},
  {(GEAR_CMD) SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED,     1,       0xFA,  1, 0},
  
  {(GEAR_CMD) SWITCHING_QUERY_EXTENDED_VERSION_NUMBER,          1,       0xFF,  1, 0},
  {(GEAR_CMD) SWITCHING_CMD_LAST,                               0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search SWITCHING command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* switching_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(switching_commands_m); idx++)
  {
    if (switching_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&switching_commands_m[idx]);
    }
  }

  return (NULL);
}


//----------------------------------------------------------------------
// initialize the variable after external power cycle
//----------------------------------------------------------------------
void gear_switching_init_after_power_cycle (dali_ctrl_gear_pers_mem_t*   pGearPers,
                                            PCtrlGearVariables           pGearVariables)
{
  (void)pGearVariables;

  // reset load failure, running error delay time
  pGearPers->switchingPersVariables.switchStatus &= 0x8C;
}


//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void gear_switching_by_reset(dali_ctrl_gear_pers_mem_t*   pPersistent)
{
  pPersistent->switchingPersVariables.minLevel          = 0xFE;
  pPersistent->switchingPersVariables.maxLevel          = 0xFE;
  pPersistent->switchingPersVariables.upSwitchOnThr     = 0x01;
  pPersistent->switchingPersVariables.upSwitchOffThr    = 0xFF;
  pPersistent->switchingPersVariables.downSwitchOnThr   = 0xFF;
  pPersistent->switchingPersVariables.downSwitchOffThr  = 0x00;
  pPersistent->switchingPersVariables.errorHoldOffTimer = 0x00;
}


//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t gear_switching_check_nvm_variables(dali_ctrl_gear_pers_mem_t*   pPersistentVariables)
{
  if (0xFE != pPersistentVariables->switchingPersVariables.minLevel)
  {
    return (FALSE);
  }
  if (0xFE != pPersistentVariables->switchingPersVariables.maxLevel)
  {
    return (FALSE);
  }
  if (0x01 != pPersistentVariables->switchingPersVariables.upSwitchOnThr)
  {
    return (FALSE);
  }
  if (0xFF != pPersistentVariables->switchingPersVariables.upSwitchOffThr)
  {
    return (FALSE);
  }
  if (0xFF != pPersistentVariables->switchingPersVariables.downSwitchOnThr)
  {
    return (FALSE);
  }
  if (0x00 != pPersistentVariables->switchingPersVariables.downSwitchOffThr)
  {
    return (FALSE);
  }
  if (0x00 != pPersistentVariables->switchingPersVariables.errorHoldOffTimer)
  {
    return (FALSE);  
  }

  return (TRUE);
}

//----------------------------------------------------------------------
// initialize the default variable of the switching module
//----------------------------------------------------------------------
void gear_switching_init_persistent(PDaliLibInstance             pInst,
                                    dali_ctrl_gear_pers_mem_t*   pGearPers)
{
  pGearPers->switchingPersVariables.phm               = 0xFE;
  pGearPers->switchingPersVariables.minLevel          = 0xFE;
  pGearPers->switchingPersVariables.maxLevel          = 0xFE;
  pGearPers->switchingPersVariables.upSwitchOnThr     = 0x01;
  pGearPers->switchingPersVariables.upSwitchOffThr    = 0xFF;
  pGearPers->switchingPersVariables.downSwitchOnThr   = 0xFF;
  pGearPers->switchingPersVariables.downSwitchOffThr  = 0x00;
  pGearPers->switchingPersVariables.errorHoldOffTimer = 0x00;

  // features
  pGearPers->switchingPersVariables.features          = 0x00;
  if (pInst->config.vendor.switching_params.load_failure)
  {
    pGearPers->switchingPersVariables.features        |= LOAD_FAILURE_FLAG;
  }
  if (pInst->config.vendor.switching_params.threashold_config)
  {
    pGearPers->switchingPersVariables.features        |= THREASHOLD_CONFIG_FLAG;
  }
  if (pInst->config.vendor.switching_params.failure_delay)
  {
    pGearPers->switchingPersVariables.features        |= FAILURE_DELAY_FLAG;
  }
  if (pInst->config.vendor.switching_params.referencing)
  {
    pGearPers->switchingPersVariables.features        |= REFERENCING_FLAG;
  }
  if (pInst->config.vendor.switching_params.phy_choice)
  {
    pGearPers->switchingPersVariables.features        |= PHY_CHOICE_FLAG;
  }

  // gear type
  pGearPers->switchingPersVariables.gearType    = 0x00;
  if (pInst->config.vendor.switching_params.electrical_switch)
  {
    pGearPers->switchingPersVariables.gearType  |= ELECTRICAL_SWITCH_FLAG;
  }
  if (pInst->config.vendor.switching_params.output_open)
  {
    pGearPers->switchingPersVariables.gearType  |= OUTPUT_OPEN_FLAG;
  }
  if (pInst->config.vendor.switching_params.output_closed)
  {
    pGearPers->switchingPersVariables.gearType  |= OUTPUT_CLOSED_FLAG;
  }
  if (pInst->config.vendor.switching_params.overvoltage_protection)
  {
    pGearPers->switchingPersVariables.gearType  |= VOLTAGE_PROTECTION_FLAG;
  }
  if (pInst->config.vendor.switching_params.inrush_current_limiter)
  {
    pGearPers->switchingPersVariables.gearType  |= INRUSH_CURRENT_LIMITER_FLAG;
  }
  
  pGearPers->switchingPersVariables.extendedVersionNumber   = CTRL_GEAR_SWITCHING_EXTENDED_VERSION_NUMBER;
  pGearPers->switchingPersVariables.deviceType              = GEAR_TYPE_SWITCHING_FUNCTION;
}

R_DALILIB_RESULT gear_switching_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                         dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                             rc = R_DALILIB_OK;
  dali_ctrl_gear_switching_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_SWITCHING_ID;
  block.version = DALI_CTRL_GEAR_PERS_SWITCHING_VERSION;

  // populate memory block
  block.phm                   = pGearPersistent->switchingPersVariables.phm;
  block.minLevel              = pGearPersistent->switchingPersVariables.minLevel;
  block.maxLevel              = pGearPersistent->switchingPersVariables.maxLevel;
  block.upSwitchOnThr         = pGearPersistent->switchingPersVariables.upSwitchOnThr;
  block.upSwitchOffThr        = pGearPersistent->switchingPersVariables.upSwitchOffThr;
  block.downSwitchOnThr       = pGearPersistent->switchingPersVariables.downSwitchOnThr;
  block.downSwitchOffThr      = pGearPersistent->switchingPersVariables.downSwitchOffThr;
  block.errorHoldOffTimer     = pGearPersistent->switchingPersVariables.errorHoldOffTimer;
  block.features              = pGearPersistent->switchingPersVariables.features;
  block.gearType              = pGearPersistent->switchingPersVariables.gearType;
  block.switchStatus          = pGearPersistent->switchingPersVariables.switchStatus;
  block.referenceValue        = pGearPersistent->switchingPersVariables.referenceValue;
  block.extendedVersionNumber = pGearPersistent->switchingPersVariables.extendedVersionNumber;
  block.deviceType            = pGearPersistent->switchingPersVariables.deviceType;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_SWITCHING_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_switching_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                          uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                 rc = R_DALILIB_OK;
  char                                             buf[128];
  switching_pers_variables_t*                      pPersistentVariables = &pCtrlInst->gearPersistent.switchingPersVariables;
  PDaliLibInstance                                 pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_gear_switching_pers_variables_block_t* pBlock = (dali_ctrl_gear_switching_pers_variables_block_t*)pDaliMem;

  pPersistentVariables->phm                   = pBlock->phm;
  pPersistentVariables->minLevel              = pBlock->minLevel;
  pPersistentVariables->maxLevel              = pBlock->maxLevel;
  pPersistentVariables->upSwitchOnThr         = pBlock->upSwitchOnThr;
  pPersistentVariables->upSwitchOffThr        = pBlock->upSwitchOffThr;
  pPersistentVariables->downSwitchOnThr       = pBlock->downSwitchOnThr;
  pPersistentVariables->downSwitchOffThr      = pBlock->downSwitchOffThr;
  pPersistentVariables->errorHoldOffTimer     = pBlock->errorHoldOffTimer;
  pPersistentVariables->features              = pBlock->features;
  pPersistentVariables->gearType              = pBlock->gearType;
  pPersistentVariables->switchStatus          = pBlock->switchStatus;
  pPersistentVariables->referenceValue        = pBlock->referenceValue;
  pPersistentVariables->extendedVersionNumber = pBlock->extendedVersionNumber;
  pPersistentVariables->deviceType            = pBlock->deviceType;

  // copy mem block

  // check value range
  if (254 != pPersistentVariables->phm)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable phm is out of bounds. Value: %u Range: [254]",
             pPersistentVariables->phm);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->phm = 0xFE;
  }
  if (pPersistentVariables->maxLevel < pPersistentVariables->minLevel ||
      1                              > pPersistentVariables->minLevel)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable minLevel is out of bounds. Value: %u Range: [1..maxLevel]",
             pPersistentVariables->minLevel);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->minLevel = 0xFE;
  }
  if (0xFE                            < pPersistentVariables->maxLevel ||
      pPersistentVariables->minLevel  > pPersistentVariables->maxLevel)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable maxLevel is out of bounds. Value: %u Range: [minLevel..0xFE]",
             pPersistentVariables->maxLevel);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->maxLevel = 0xFE;
  }
  if (1 > pPersistentVariables->upSwitchOnThr)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable upSwitchOnThr is out of bounds. Value: %u Range: [1..255]",
             pPersistentVariables->upSwitchOnThr);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->upSwitchOnThr = 0x01;
  }
  if (1 > pPersistentVariables->upSwitchOffThr)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable upSwitchOffThr is out of bounds. Value: %u Range: [1..255]",
             pPersistentVariables->upSwitchOffThr);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->upSwitchOffThr = DALI_MASK;
  }
  if (0x26 & pPersistentVariables->features)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable features is out of bounds. Value: %u Range: (xx0x x00x)",
             pPersistentVariables->features);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->features          = 0x00;
    if (pInst->config.vendor.switching_params.load_failure)
    {
      pPersistentVariables->features        |= LOAD_FAILURE_FLAG;
    }
    if (pInst->config.vendor.switching_params.threashold_config)
    {
      pPersistentVariables->features        |= THREASHOLD_CONFIG_FLAG;
    }
    if (pInst->config.vendor.switching_params.failure_delay)
    {
      pPersistentVariables->features        |= FAILURE_DELAY_FLAG;
    }
    if (pInst->config.vendor.switching_params.referencing)
    {
      pPersistentVariables->features        |= REFERENCING_FLAG;
    }
    if (pInst->config.vendor.switching_params.phy_choice)
    {
      pPersistentVariables->features        |= PHY_CHOICE_FLAG;
    }
  }
  if (0x1F & pPersistentVariables->gearType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable gearType is out of bounds. Value: %u Range: [0..0x1F]",
             pPersistentVariables->gearType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->gearType    = 0x00;
    if (pInst->config.vendor.switching_params.electrical_switch)
    {
      pPersistentVariables->gearType  |= ELECTRICAL_SWITCH_FLAG;
    }
    if (pInst->config.vendor.switching_params.output_open)
    {
      pPersistentVariables->gearType  |= OUTPUT_OPEN_FLAG;
    }
    if (pInst->config.vendor.switching_params.output_closed)
    {
      pPersistentVariables->gearType  |= OUTPUT_CLOSED_FLAG;
    }
    if (pInst->config.vendor.switching_params.overvoltage_protection)
    {
      pPersistentVariables->gearType  |= VOLTAGE_PROTECTION_FLAG;
    }
    if (pInst->config.vendor.switching_params.inrush_current_limiter)
    {
      pPersistentVariables->gearType  |= INRUSH_CURRENT_LIMITER_FLAG;
    }
  }
  if (2 < pPersistentVariables->extendedVersionNumber)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable extendedVersionNumber is out of bounds. Value: %u Range: [0..2]",
             pPersistentVariables->extendedVersionNumber);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->extendedVersionNumber = CTRL_GEAR_SWITCHING_EXTENDED_VERSION_NUMBER;
  }
  if (GEAR_TYPE_LED_MODULES != pPersistentVariables->deviceType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable deviceType is out of bounds. Value: %u Range: [7]",
             pPersistentVariables->deviceType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->deviceType = GEAR_TYPE_SWITCHING_FUNCTION;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the SWITCHING forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_switching_process_forward(PCtrlGearInstance      pCtrlInst,
                                                dalilib_frame_t*       pForwardFrame,
                                                ctrl_gear_commands_t** ppGearCmd,
                                                gear_adr_t*            pReceivedAdr,
                                                uint8_t*               pRespValue)
{
  R_DALILIB_RESULT            rc                  = R_IGNORE_FRAME;
  switching_pers_variables_t* pSwitchingPersVar   = NULL;
  switching_variables_t*      pSwitchingVar       = NULL;

  (void)pReceivedAdr;

  pSwitchingPersVar = &pCtrlInst->gearPersistent.switchingPersVariables;
  pSwitchingVar = &pCtrlInst->gearVariables.switchingVariables;

  *ppGearCmd = switching_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  // ignore the forward frames during the measurement, except queries
  if (pSwitchingVar->referenceRunning)
  {
    if ((SWITCHING_CMD)(*ppGearCmd)->eGearCmd > SWITCHING_REFERENCE_SYSTEM_POWER &&
        (SWITCHING_CMD)(*ppGearCmd)->eGearCmd <= SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME)
    {
      rc = R_IGNORE_FRAME;
      return (rc);
    }
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case SWITCHING_REFERENCE_SYSTEM_POWER:
      {
        // check, if this reference supported
        if (pSwitchingPersVar->features & REFERENCE_FLAG)
        {
          pSwitchingVar->referenceRunning = TRUE;
          pSwitchingVar->referenceStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          dalilib_action_t action;

          memset( &action, 0, sizeof(action));

          action.actionType = DALILIB_CTRL_GEAR_REACTION;
          action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_REFERENCE_MEASUREMENT_START;
          dali_cb_reaction(pCtrlInst->pInst, &action);
        }
      }
    break;

    case SWITCHING_STORE_DTR_AS_UP_SWITCH_ON_THRESHOLD:
      {
        if (pCtrlInst->gearVariables.DTR0 != pSwitchingPersVar->upSwitchOnThr)
        {
          pSwitchingPersVar->upSwitchOnThr = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case SWITCHING_STORE_DTR_AS_UP_SWITCH_OFF_THRESHOLD:
      {
        if (pCtrlInst->gearVariables.DTR0 != pSwitchingPersVar->upSwitchOffThr)
        {
          pSwitchingPersVar->upSwitchOffThr = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case SWITCHING_STORE_DTR_AS_DOWN_SWITCH_ON_THRESHOLD:
      {
        if (pCtrlInst->gearVariables.DTR0 != pSwitchingPersVar->downSwitchOnThr)
        {
          pSwitchingPersVar->downSwitchOnThr = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case SWITCHING_STORE_DTR_AS_DOWN_SWITCH_OFF_THRESHOLD:
      {
        if (pCtrlInst->gearVariables.DTR0 != pSwitchingPersVar->downSwitchOffThr)
        {
          pSwitchingPersVar->downSwitchOffThr = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case SWITCHING_STORE_DTR_AS_ERROR_HOLD_OFF_TIME:
      {
        if (pCtrlInst->gearVariables.DTR0 != pSwitchingPersVar->errorHoldOffTimer)
        {
          pSwitchingPersVar->errorHoldOffTimer = pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_SWITCHING_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case SWITCHING_QUERY_FEATURES:
      {
        *pRespValue = pSwitchingPersVar->features;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_SWITCH_STATUS:
      {
        *pRespValue = pSwitchingPersVar->switchStatus;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_UP_SWITCH_ON_THRESHOLD:
      {
        *pRespValue = pSwitchingPersVar->upSwitchOnThr;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_UP_SWITCH_OFF_THRESHOLD:
      {
        *pRespValue = pSwitchingPersVar->upSwitchOffThr;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_DOWN_SWITCH_ON_THRESHOLD:
      {
        *pRespValue = pSwitchingPersVar->downSwitchOnThr;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_DOWN_SWITCH_OFF_THRESHOLD:
      {
        *pRespValue = pSwitchingPersVar->downSwitchOffThr;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_ERROR_HOLD_OFF_TIME:
      {
        *pRespValue = pSwitchingPersVar->errorHoldOffTimer;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_GEAR_TYPE:
      {
        *pRespValue = pSwitchingPersVar->gearType;
        rc = R_DALILIB_OK;
      }
    break;

    case SWITCHING_QUERY_REFERENCE_RUNNING:
      {
        // check, if this option supported
        if (pSwitchingPersVar->features & REFERENCE_FLAG)
        {
          if (pSwitchingVar->referenceRunning)
          {
            rc = R_DALILIB_OK;
            *pRespValue = DALI_YES;
          }
        }
      }
    break;

    case SWITCHING_QUERY_REFERENCE_MEASUREMENT_FAILED:
      {
        // check if referenece-measurement-failed-flag is set in "switch status"
        if (pSwitchingPersVar->switchStatus & 0x80)
        {
          rc = R_DALILIB_OK;
          *pRespValue = DALI_YES;
        }
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_switching_get_cmd(SWITCHING_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(switching_commands_m); idx++)
  {
    if ((SWITCHING_CMD)switching_commands_m[idx].eGearCmd == eCmd)
    {
      return (&switching_commands_m[idx]);
    }
  }

  return (NULL);
}

