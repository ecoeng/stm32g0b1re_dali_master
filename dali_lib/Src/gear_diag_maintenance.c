/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_diag_maintenance.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"

#ifdef DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT


// eGearDiagMaintenanceCmd,                           selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t diag_maintenance_commands_m[] =
{
  {(GEAR_CMD) DIAG_MAINTENANCE_QUERY_EXTENDED_VERSION_NUMBER,   1,       0xFF,  1, 0},
  {(GEAR_CMD) DIAG_MAINTENANCE_CMD_LAST,                        0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search diangnostics and maintenance command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* diag_maintenance_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(diag_maintenance_commands_m); idx++)
  {
    if (diag_maintenance_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&diag_maintenance_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// initialize the memory after external power cycle
//----------------------------------------------------------------------
void gear_diag_maintenance_init_after_power_cycle(PCtrlGearInstance pCtrlInst)
{
  dalilib_vendor_diag_maintenance_t*  pConfig = NULL;

  pConfig = &((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.diag_maintenance_params;

  // init variables
  pCtrlInst->gearVariables.diagMaintenaceVariables.extendedVersionNumber = CTRL_GEAR_DIAG_MAINTENANCE_EXTENDED_VERSION_NUMBER;
  pCtrlInst->gearVariables.diagMaintenaceVariables.deviceType = GEAR_TYPE_CONTROL_GEAR_DIAG_MAINTENANCE;

  // initialize memory bank 205 RAM memory cells
  pCtrlInst->gearMemBank205RAM.lockByte = DALI_MASK;
  pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[0]       = pConfig->controlGearExternalSupplyVoltage >> 8;
  pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[1]       = pConfig->controlGearExternalSupplyVoltage;
  pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltageFrequency = pConfig->controlGearExternalSupplyVoltageFrequency;
  pCtrlInst->gearMemBank205RAM.controlGearPowerFactor                    = pConfig->controlGearPowerFactor;
  pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition        = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage     = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage      = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearOutputPowerLimitation          = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearThermalDerating                = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown                = 0x00;
  pCtrlInst->gearMemBank205RAM.controlGearTemperature                    = pConfig->controlGearTemperature;
  pCtrlInst->gearMemBank205RAM.controlGearOutputCurrentPercent           = pConfig->controlGearOutputCurrentPercent;

  // initialize memory bank 206 RAM memory cells
  pCtrlInst->gearMemBank206RAM.lockByte = DALI_MASK;
  pCtrlInst->gearMemBank206RAM.lightSourceVoltage[0]                = pConfig->lightSourceVoltage >> 8;
  pCtrlInst->gearMemBank206RAM.lightSourceVoltage[1]                = pConfig->lightSourceVoltage;
  pCtrlInst->gearMemBank206RAM.lightSourceCurrent[0]                = pConfig->lightSourceCurrent >> 8;
  pCtrlInst->gearMemBank206RAM.lightSourceCurrent[1]                = pConfig->lightSourceCurrent;
  pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition   = pCtrlInst->gearVariables.lampFailure;
  pCtrlInst->gearMemBank206RAM.lightSourceShortCircuit              = 0x00;
  pCtrlInst->gearMemBank206RAM.lightSourceOpenCircuit               = 0x00;
  pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating           = 0x00;
  pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown           = 0x00;
  pCtrlInst->gearMemBank206RAM.lightSourceTemperature               = pConfig->lightSourceTemperature;

  // initialize memory bank 207 RAM memory cells
  pCtrlInst->gearMemBank207RAM.lockByte = DALI_MASK;
}


//----------------------------------------------------------------------
// initialize the default values of persistent memory
//----------------------------------------------------------------------
void gear_diag_maintenance_init_persistent(PDaliLibInstance              pInst,
                                           dali_ctrl_gear_pers_mem_t*    pGearPersistent)
{
  (void)pInst;

  // memory bank 205 NVM memory cells already initialized with 0x00
  // memory bank 206 NVM memory cells already initialized with 0x00

  // initialize memory bank 207
  pGearPersistent->gearMemBank207NVM.ratedMedianUsefulLifeOfLuminaire = DALI_MASK;
  pGearPersistent->gearMemBank207NVM.internalControlGearReferenceTemperature = DALI_MASK;
  pGearPersistent->gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[0] = DALI_MASK;
  pGearPersistent->gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[1] = DALI_MASK;

}

//----------------------------------------------------------------------
// reset diangnostics and maintenance memory banks
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                         uint8_t           nMembank)
{
  // check whether to reset memory bank 205
  if ((CTRL_GEAR_DIAG_MAINTENANCE_FIRST_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank205RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank205RAM.lockByte = DALI_MASK;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOverallFailureConditionCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOutputPowerLimitationCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalDeratingCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalShutdownCounter = 0x00;
  }

  // check whether to reset memory bank 206
  if ((CTRL_GEAR_DIAG_MAINTENANCE_SECOND_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank206RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank206RAM.lockByte = DALI_MASK;
    pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOverallFailureConditionCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceShortCircuitCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOpenCircuitCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalDeratingCounter = 0x00;
    pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalShutdownCounter = 0x00;
  }

  // check whether to reset memory bank 207
  if ((CTRL_GEAR_DIAG_MAINTENANCE_THIRD_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank207RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank207RAM.lockByte = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read memory bank 205
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank205(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_205_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_205_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank205RAM.lockByte;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_VERSION_BYTE:
        *pRespValue = CTRL_GEAR_MEMORY_BANK_205_VERSION;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_START_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[0];
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_END_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[1];
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_FREQ_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltageFrequency;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_POWER_FACTOR_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearPowerFactor;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOverallFailureConditionCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearOutputPowerLimitation;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_COUNT_BYTE:
        *pRespValue =  pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOutputPowerLimitationCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearThermalDerating;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_COUNT_BYTE:
        *pRespValue =  pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalDeratingCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_COUNT_BYTE:
        *pRespValue =  pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalShutdownCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_TEMPERATURE_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearTemperature;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_CURRENT_PERCENT_BYTE:
        *pRespValue = pCtrlInst->gearMemBank205RAM.controlGearOutputCurrentPercent;
      break;


      default:
        rc = R_IGNORE_FRAME;
        if ((CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank205NVM.
              controlGearOperatingTime[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
        if ((CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
          (CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank205NVM.
              controlGearStartCounter[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write memory bank 205
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank205_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_205_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case 0x00: // Address of last addressable memory location
    case 0x01: // Indicator byte manufacturer specific
      // not writable
      rc = R_IGNORE_FRAME;
    break;

    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
      pCtrlInst->gearMemBank205RAM.lockByte = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_VERSION_BYTE:
      // not writable
      rc = R_IGNORE_FRAME;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_START_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[0] = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_END_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[1] = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_VOLTAGE_FREQ_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltageFrequency = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_POWER_FACTOR_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearPowerFactor = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_OVERALL_FAIL_COND_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOverallFailureConditionCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOverallFailureConditionCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_UNDERVOLTAGE_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_EXT_SUPPLY_OVERVOLTAGE_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearOutputPowerLimitation = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_POWER_LIMIT_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOutputPowerLimitationCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOutputPowerLimitationCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearThermalDerating = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_THERMAL_DERATING_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalDeratingCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalDeratingCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_THERMAL_SHUTDOWN_COUNT_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalShutdownCounter)
      {
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalShutdownCounter = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_205_TEMPERATURE_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearTemperature = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_OUTPUT_CURRENT_PERCENT_BYTE:
      pCtrlInst->gearMemBank205RAM.controlGearOutputCurrentPercent = reqValue;
    break;


    default:
      rc = R_IGNORE_FRAME;
      if ((CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
          (CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
      {
        if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.
                          controlGearOperatingTime[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE)])
        {
          pCtrlInst->gearPersistent.gearMemBank205NVM.
            controlGearOperatingTime[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_OPERATING_TIME_START_BYTE)] =
              reqValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        rc = R_DALILIB_OK;
        break;
      }
      if ((CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
        (CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
      {
        if (reqValue != pCtrlInst->gearPersistent.gearMemBank205NVM.
                          controlGearStartCounter[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE)])
        {
          pCtrlInst->gearPersistent.gearMemBank205NVM.
            controlGearStartCounter[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_205_START_COUNTER_START_BYTE)] =
              reqValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        rc = R_DALILIB_OK;
        break;
      }
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// write memory bank 205
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank205(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue)
{
  uint8_t           nCell;

  nCell = pCtrlInst->gearVariables.DTR0;

  // write memory bank cell
  // check lock byte cell offset
  if(DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell)
  {
    return (R_IGNORE_FRAME);
  }
  // write lock byte memory cell
  pCtrlInst->gearMemBank205RAM.lockByte = reqValue;

  // check whether to reply
  if ((GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read memory bank 206
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank206(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_206_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_206_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank206RAM.lockByte;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_VERSION_BYTE:
        *pRespValue = CTRL_GEAR_MEMORY_BANK_206_VERSION;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_START_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceVoltage[0];
      break;

      case CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_STOP_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceVoltage[1];
      break;

      case CTRL_GEAR_MEMORY_BANK_206_CURRENT_START_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceCurrent[0];
      break;

      case CTRL_GEAR_MEMORY_BANK_206_CURRENT_STOP_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceCurrent[1];
      break;

      case CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOverallFailureConditionCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceShortCircuit;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceShortCircuitCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceOpenCircuit;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOpenCircuitCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalDeratingCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_COUNT_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalShutdownCounter;
      break;

      case CTRL_GEAR_MEMORY_BANK_206_THEMTERATURE_BYTE:
        *pRespValue = pCtrlInst->gearMemBank206RAM.lightSourceTemperature;
      break;

      default:
        rc = R_IGNORE_FRAME;
        if ((CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.
              lightSourceStartCounterResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
        if ((CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.
              lightSourceStartCounter[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
        if ((CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.
              lightSourceOnTimeResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
        if ((CTRL_GEAR_MEMORY_BANK_206_ON_TIME_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
            (CTRL_GEAR_MEMORY_BANK_206_ON_TIME_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
        {
          *pRespValue = pCtrlInst->gearPersistent.gearMemBank206NVM.
              lightSourceOnTime[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_ON_TIME_START_BYTE)];
          rc = R_DALILIB_OK;
          break;
        }
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write memory bank 206
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank206_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_206_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case 0x00:
    case 0x01:
      // not writable
      rc = R_IGNORE_FRAME;
    break;

    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
      pCtrlInst->gearMemBank206RAM.lockByte = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_206_VERSION_BYTE:
      // not writable
      rc = R_IGNORE_FRAME;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_START_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceVoltage[0] = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_VOLTAGE_STOP_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceVoltage[1] = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_CURRENT_START_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceCurrent[0] = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_CURRENT_STOP_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceCurrent[1] = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_OVERALL_FAIL_COND_COUNT_BYTE:
       if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOverallFailureConditionCounter)
       {
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOverallFailureConditionCounter = reqValue;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
       }
     break;

     case CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceShortCircuit = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_SHORT_CIRCUIT_COUNT_BYTE:
       if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceShortCircuitCounter)
       {
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceShortCircuitCounter = reqValue;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
       }
     break;

     case CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceOpenCircuit = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_OPEN_CIRCUIT_COUNT_BYTE:
       if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOpenCircuitCounter)
       {
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOpenCircuitCounter = reqValue;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
       }
     break;

     case CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_THERMAL_DERATING_COUNT_BYTE:
       if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalDeratingCounter)
       {
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalDeratingCounter = reqValue;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
       }
     break;

     case CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown = reqValue;
     break;

     case CTRL_GEAR_MEMORY_BANK_206_THERMAL_SHUTDOWN_COUNT_BYTE:
       if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalShutdownCounter)
       {
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalShutdownCounter = reqValue;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
       }
     break;

     case CTRL_GEAR_MEMORY_BANK_206_THEMTERATURE_BYTE:
       pCtrlInst->gearMemBank206RAM.lightSourceTemperature = reqValue;
     break;

    default:
      rc = R_IGNORE_FRAME;
      if ((CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
          (CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
      {
        if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.
                          lightSourceStartCounterResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE)])
        {
          pCtrlInst->gearPersistent.gearMemBank206NVM.
            lightSourceStartCounterResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE)] =
              reqValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        rc = R_DALILIB_OK;
        break;
      }
      if ((CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
          (CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0))
      {
        if (reqValue != pCtrlInst->gearPersistent.gearMemBank206NVM.
                          lightSourceOnTimeResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE)])
        {
          pCtrlInst->gearPersistent.gearMemBank206NVM.
            lightSourceOnTimeResettable[(pCtrlInst->gearVariables.DTR0 - CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE)] =
              reqValue;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        rc = R_DALILIB_OK;
        break;
      }
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// write memory bank 206
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank206(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue)
{
  uint8_t           nCell;

  nCell = pCtrlInst->gearVariables.DTR0;

  // check whether memory bank is unlocked
  if ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK != pCtrlInst->gearMemBank206RAM.lockByte))
  {
    return (R_IGNORE_FRAME);
  }

  // check writable cell offset
  if ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell) &&
      !((CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
        (CTRL_GEAR_MEMORY_BANK_206_START_COUNTER_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0)) &&
      !((CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_START_BYTE <= pCtrlInst->gearVariables.DTR0) &&
        (CTRL_GEAR_MEMORY_BANK_206_ON_TIME_RESETTABLE_STOP_BYTE  >= pCtrlInst->gearVariables.DTR0)))
  {
    return (R_IGNORE_FRAME);
  }
  // write memory cell
  gear_diag_maintenance_write_memory_bank206_direct(pCtrlInst, nCell, reqValue);

  // check whether to reply
  if ((GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read memory bank 207
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_read_memory_bank207(PCtrlGearInstance     pCtrlInst,
                                                           uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_207_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_205_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank207RAM.lockByte;
      break;

      case CTRL_GEAR_MEMORY_BANK_205_VERSION_BYTE:
        *pRespValue = CTRL_GEAR_MEMORY_BANK_205_VERSION;
      break;

      case CTRL_GEAR_MEMORY_BANK_207_LIFE_OF_LUMINAIRE_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLifeOfLuminaire;
      break;

      case CTRL_GEAR_MEMORY_BANK_207_INTERNAL_REF_TEMPERATURE_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank207NVM.internalControlGearReferenceTemperature;
      break;

      case CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_START_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[0];
      break;

      case CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_STOP_BYTE:
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[1];
      break;

      default:
        rc = R_IGNORE_FRAME;
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write memory bank 207
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank207_direct(PCtrlGearInstance pCtrlInst,
                                                                   uint8_t           nCell,
                                                                   uint8_t           reqValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_207_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case 0x00: // Address of last addressable memory location
    case 0x01: // Indicator byte manufacturer specific
      // not writable
      rc = R_IGNORE_FRAME;
    break;

    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
      pCtrlInst->gearMemBank207RAM.lockByte = reqValue;
    break;

    case CTRL_GEAR_MEMORY_BANK_205_VERSION_BYTE:
      // not writable
      rc = R_IGNORE_FRAME;
    break;

    case CTRL_GEAR_MEMORY_BANK_207_LIFE_OF_LUMINAIRE_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLifeOfLuminaire)
      {
        pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLifeOfLuminaire = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_207_INTERNAL_REF_TEMPERATURE_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank207NVM.internalControlGearReferenceTemperature)
      {
        pCtrlInst->gearPersistent.gearMemBank207NVM.internalControlGearReferenceTemperature = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_START_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[0])
      {
        pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[0] = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case CTRL_GEAR_MEMORY_BANK_207_LIGHT_SOURCE_STARTS_STOP_BYTE:
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[1])
      {
        pCtrlInst->gearPersistent.gearMemBank207NVM.ratedMedianUsefulLightSourceStarts[1] = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    default:
      rc = R_IGNORE_FRAME;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// write memory bank 207
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_write_memory_bank207(PCtrlGearInstance     pCtrlInst,
                                                            ctrl_gear_commands_t* pGearCmd,
                                                            uint8_t               reqValue,
                                                            uint8_t*              pRespValue)
{
  uint8_t           nCell;

  nCell = pCtrlInst->gearVariables.DTR0;

  // check whether memory bank is unlocked
  if ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK != pCtrlInst->gearMemBank206RAM.lockByte))
  {
    return (R_IGNORE_FRAME);
  }

  // check writable cell offset
  if ((CTRL_GEAR_MEMORY_BANK_207_ADDRESS_LAST_CELL < nCell) ||
      ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell) &&
       (CTRL_GEAR_MEMORY_BANK_207_LIFE_OF_LUMINAIRE_BYTE < nCell)))
  {
    return (R_IGNORE_FRAME);
  }
  // write memory cell
  gear_diag_maintenance_write_memory_bank207_direct(pCtrlInst, nCell, reqValue);

  // check whether to reply
  if ((GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (R_DALILIB_OK);
}

R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank205(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank205_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_ID;
  block.version = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_VERSION;

  // populate memory block
  block.controlGearOverallFailureConditionCounter    = pGearPersistent->gearMemBank205NVM.controlGearOverallFailureConditionCounter;
  block.controlGearExternalSupplyUndervoltageCounter = pGearPersistent->gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter;
  block.controlGearExternalSupplyOvervoltageCounter  = pGearPersistent->gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter;
  block.controlGearOutputPowerLimitationCounter      = pGearPersistent->gearMemBank205NVM.controlGearOutputPowerLimitationCounter;
  block.controlGearThermalDeratingCounter            = pGearPersistent->gearMemBank205NVM.controlGearThermalDeratingCounter;
  block.controlGearThermalShutdownCounter            = pGearPersistent->gearMemBank205NVM.controlGearThermalShutdownCounter;
  memcpy(block.controlGearOperatingTime, pGearPersistent->gearMemBank205NVM.controlGearOperatingTime, sizeof(block.controlGearOperatingTime));
  memcpy(block.controlGearStartCounter,  pGearPersistent->gearMemBank205NVM.controlGearStartCounter,  sizeof(block.controlGearStartCounter));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank206(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank206_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_ID;
  block.version = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_VERSION;

  // populate memory block
  block.lightSourceOverallFailureConditionCounter = pGearPersistent->gearMemBank206NVM.lightSourceOverallFailureConditionCounter;
  block.lightSourceShortCircuitCounter            = pGearPersistent->gearMemBank206NVM.lightSourceShortCircuitCounter;
  block.lightSourceOpenCircuitCounter             = pGearPersistent->gearMemBank206NVM.lightSourceOpenCircuitCounter;
  block.lightSourceThermalDeratingCounter         = pGearPersistent->gearMemBank206NVM.lightSourceThermalDeratingCounter;
  block.lightSourceThermalShutdownCounter         = pGearPersistent->gearMemBank206NVM.lightSourceThermalShutdownCounter;
  memcpy(block.lightSourceStartCounterResettable, pGearPersistent->gearMemBank206NVM.lightSourceStartCounterResettable, sizeof(block.lightSourceStartCounterResettable));
  memcpy(block.lightSourceStartCounter,           pGearPersistent->gearMemBank206NVM.lightSourceStartCounter,           sizeof(block.lightSourceStartCounter));
  memcpy(block.lightSourceOnTimeResettable,       pGearPersistent->gearMemBank206NVM.lightSourceOnTimeResettable,       sizeof(block.lightSourceOnTimeResettable));
  memcpy(block.lightSourceOnTime,                 pGearPersistent->gearMemBank206NVM.lightSourceOnTime,                 sizeof(block.lightSourceOnTime));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_diag_maintenance_save_mem_block_memory_bank207(PDaliLibInstance           pInst,
                                                                     dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank207_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_ID;
  block.version = DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_VERSION;

  // populate memory block
  block.ratedMedianUsefulLifeOfLuminaire        = pGearPersistent->gearMemBank207NVM.ratedMedianUsefulLifeOfLuminaire;
  block.internalControlGearReferenceTemperature = pGearPersistent->gearMemBank207NVM.internalControlGearReferenceTemperature;
  memcpy(block.ratedMedianUsefulLightSourceStarts, pGearPersistent->gearMemBank207NVM.ratedMedianUsefulLightSourceStarts, sizeof(block.ratedMedianUsefulLightSourceStarts));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK207_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank205(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_mem_bank205_NVM_t*                        pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank205NVM;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank205_t *pBlock = (dali_ctrl_gear_diag_maintenance_pers_mem_bank205_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->controlGearOverallFailureConditionCounter    = pBlock->controlGearOverallFailureConditionCounter;
  pPersistentVariables->controlGearExternalSupplyUndervoltageCounter = pBlock->controlGearExternalSupplyUndervoltageCounter;
  pPersistentVariables->controlGearExternalSupplyOvervoltageCounter  = pBlock->controlGearExternalSupplyOvervoltageCounter;
  pPersistentVariables->controlGearOutputPowerLimitationCounter      = pBlock->controlGearOutputPowerLimitationCounter;
  pPersistentVariables->controlGearThermalDeratingCounter            = pBlock->controlGearThermalDeratingCounter;
  pPersistentVariables->controlGearThermalShutdownCounter            = pBlock->controlGearThermalShutdownCounter;
  memcpy(pPersistentVariables->controlGearOperatingTime, pBlock->controlGearOperatingTime, sizeof(pPersistentVariables->controlGearOperatingTime));
  memcpy(pPersistentVariables->controlGearStartCounter,  pBlock->controlGearStartCounter,  sizeof(pPersistentVariables->controlGearStartCounter));

  return (rc);
}

R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank206(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_mem_bank206_NVM_t*                        pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank206NVM;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank206_t *pBlock = (dali_ctrl_gear_diag_maintenance_pers_mem_bank206_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->lightSourceOverallFailureConditionCounter = pBlock->lightSourceOverallFailureConditionCounter;
  pPersistentVariables->lightSourceShortCircuitCounter            = pBlock->lightSourceShortCircuitCounter;
  pPersistentVariables->lightSourceOpenCircuitCounter             = pBlock->lightSourceOpenCircuitCounter;
  pPersistentVariables->lightSourceThermalDeratingCounter         = pBlock->lightSourceThermalDeratingCounter;
  pPersistentVariables->lightSourceThermalShutdownCounter         = pBlock->lightSourceThermalShutdownCounter;
  memcpy(pPersistentVariables->lightSourceStartCounterResettable, pBlock->lightSourceStartCounterResettable, sizeof(pPersistentVariables->lightSourceStartCounterResettable));
  memcpy(pPersistentVariables->lightSourceStartCounter,           pBlock->lightSourceStartCounter,           sizeof(pPersistentVariables->lightSourceStartCounter));
  memcpy(pPersistentVariables->lightSourceOnTimeResettable,       pBlock->lightSourceOnTimeResettable,       sizeof(pPersistentVariables->lightSourceOnTimeResettable));
  memcpy(pPersistentVariables->lightSourceOnTime,                 pBlock->lightSourceOnTime,                 sizeof(pPersistentVariables->lightSourceOnTime));

  return (rc);
}

R_DALILIB_RESULT gear_diag_maintenance_apply_mem_block_memory_bank207(PCtrlGearInstance pCtrlInst,
                                                                      uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_mem_bank207_NVM_t*                        pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank207NVM;
  dali_ctrl_gear_diag_maintenance_pers_mem_bank207_t *pBlock = (dali_ctrl_gear_diag_maintenance_pers_mem_bank207_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->ratedMedianUsefulLifeOfLuminaire        = pBlock->ratedMedianUsefulLifeOfLuminaire;
  pPersistentVariables->internalControlGearReferenceTemperature = pBlock->internalControlGearReferenceTemperature;
  memcpy(pPersistentVariables->ratedMedianUsefulLightSourceStarts, pBlock->ratedMedianUsefulLightSourceStarts, sizeof(pPersistentVariables->ratedMedianUsefulLightSourceStarts));

  return (rc);
}


//----------------------------------------------------------------------
// process the diangnostics and maintenance forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_diag_maintenance_process_forward(PCtrlGearInstance      pCtrlInst,
                                                       dalilib_frame_t*       pForwardFrame,
                                                       ctrl_gear_commands_t** ppGearCmd,
                                                       gear_adr_t*            pReceivedAdr,
                                                       uint8_t*               pRespValue)
{
  R_DALILIB_RESULT      rc          = R_DALILIB_OK;

  (void)pReceivedAdr;

  *ppGearCmd = diag_maintenance_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case DIAG_MAINTENANCE_QUERY_EXTENDED_VERSION_NUMBER:
      *pRespValue = pCtrlInst->gearVariables.diagMaintenaceVariables.extendedVersionNumber;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_diag_maintenance_get_cmd(DIAG_MAINTENANCE_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(diag_maintenance_commands_m); idx++)
  {
    if ((DIAG_MAINTENANCE_CMD)diag_maintenance_commands_m[idx].eGearCmd == eCmd)
    {
      return (&diag_maintenance_commands_m[idx]);
    }
  }

  return (NULL);
}

void gear_diag_maintenance_timingHelper(void* pCtrlInstance)
{
  PCtrlGearInstance pCtrlInst = (PCtrlGearInstance)pCtrlInstance;
  PDaliLibInstance  pInst = pCtrlInst->pInst;
  uint32_t          diffTime = 0;
  uint32_t          value = 0;
  uint32_t          value2 = 0;

  diffTime = dali_difftime(pInst->currentTime, pCtrlInst->gearLastTime);
  pCtrlInst->gearOpDiff += diffTime;

  // check whether to update gear operating time
  value2 = (pCtrlInst->gearOpDiff / 1000); // diff in seconds
  if (1 <= value2)
  {
    // decrement opDiff
    pCtrlInst->gearOpDiff -= (value2 * 1000);
    // copy value
    memcpy(&value,
        &pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOperatingTime,
        sizeof(value));
    // increment value
    value += value2;
    // store value
    memcpy(&pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOperatingTime,
        &value,
        sizeof(pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOperatingTime));
    pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
    if (0 == pCtrlInst->gearPersistentChangedTime)
    {
      pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    }
  }

  // check whether to update gear start counter
  if (TRUE == pCtrlInst->gearPowerOnRunning)
  {
    if ((FALSE == pCtrlInst->gearPowerOnCounted) &&
        (CTRL_GEAR_DIAG_MAINTENANCE_START_COUNTER_THRESHOLD < pCtrlInst->gearPowerOnLevelTime))
    {
      pCtrlInst->gearPowerOnCounted = TRUE;
      // copy value
      value = 0;
      memcpy(&value,
          &pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearStartCounter,
          sizeof(pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearStartCounter)); // size of start counter 24 bit
      // increment value
      value++;
      // store value
      memcpy(&pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearStartCounter,
          &value,
          sizeof(pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearStartCounter));
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearPowerOnCounted)
    {
      pCtrlInst->gearPowerOnCounted = FALSE;
    }
  }

  // check whether to update overall failure condition and counter
  if (TRUE == pCtrlInst->gearVariables.controlGearFailure)
  {
    if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition)
    {
      pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition = TRUE;
      pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOverallFailureConditionCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition)
    {
      pCtrlInst->gearMemBank205RAM.controlGearOverallFailureCondition = FALSE;
    }
  }

  // check whether to update external supply undervoltage and counter
  value =  (pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[0] << 8) |
           pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyVoltage[1];
  if (value < pInst->config.vendor.diag_maintenance_params.controlGearExternalSupplyUndervoltageThreshold)
  {
    if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage)
    {
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage = TRUE;
      pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyUndervoltageCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
      pCtrlInst->gearVariables.controlGearFailure = TRUE;
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage)
    {
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyUndervoltage = FALSE;
    }
  }
  // check whether to update external supply overvoltage and counter
  if (value > pInst->config.vendor.diag_maintenance_params.controlGearExternalSupplyOvervoltageThreshold)
  {
    if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage)
    {
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage = TRUE;
      pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearExternalSupplyOvervoltageCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
      pCtrlInst->gearVariables.controlGearFailure = TRUE;
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage)
    {
      pCtrlInst->gearMemBank205RAM.controlGearExternalSupplyOvervoltage = FALSE;
    }
  }


  value =  (pCtrlInst->gearMemBank206RAM.lightSourceCurrent[0] << 8) | pCtrlInst->gearMemBank206RAM.lightSourceCurrent[1];
  value2 = (pCtrlInst->gearMemBank206RAM.lightSourceVoltage[0] << 8) | pCtrlInst->gearMemBank206RAM.lightSourceVoltage[1];
  if (value * value2 > pInst->config.vendor.diag_maintenance_params.controlGearOutputPowerThreshold)
  {
    if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearOutputPowerLimitation)
    {
      pCtrlInst->gearMemBank205RAM.controlGearOutputPowerLimitation = TRUE;
      pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearOutputPowerLimitationCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
      pCtrlInst->gearVariables.controlGearFailure = TRUE;
    }
  }


  // check whether to update external supply thermal derating and counter
  if (pCtrlInst->gearMemBank205RAM.controlGearTemperature > pInst->config.vendor.diag_maintenance_params.controlGearThermalDeratingThreshold)
  {
    if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearThermalDerating)
    {
      pCtrlInst->gearMemBank205RAM.controlGearThermalDerating = TRUE;
      pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalDeratingCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
      pCtrlInst->gearVariables.controlGearFailure = TRUE;
    }
    // check whether to update external supply thermal shutdown and counter
    if (pCtrlInst->gearMemBank205RAM.controlGearTemperature > pInst->config.vendor.diag_maintenance_params.controlGearThermalShutdownThreshold)
    {
      if (FALSE == pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown)
      {
        pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown = TRUE;
        pCtrlInst->gearPersistent.gearMemBank205NVM.controlGearThermalShutdownCounter++;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK205_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
        pCtrlInst->gearVariables.controlGearFailure = TRUE;
      }
    }
    else
    {
      if (TRUE == pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown)
      {
        pCtrlInst->gearMemBank205RAM.controlGearThermalShutdown = FALSE;
      }
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearMemBank205RAM.controlGearThermalDerating)
    {
      pCtrlInst->gearMemBank205RAM.controlGearThermalDerating = FALSE;
    }
  }

  // check whether to update light source start counter, light source start counter resettable,
  // light source on time and light source on time resettable
  if (TRUE == pCtrlInst->gearVariables.lampOn)
  {
    // check whether to update light source start counter and light source start counter resettable
    if (FALSE == pCtrlInst->gearLampOnCounted)
    {
      pCtrlInst->gearLampOnCounted = TRUE;
      value = (pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[0] << 16) |
              (pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[1] << 8) |
              pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[2];
      value++;
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[0] = (value >> 16);
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[1] = (value << 8);
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounter[2] = value;
      value = (pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[0] << 16) |
              (pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[1] << 8) |
              pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[2];
      value++;
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[0] = (value >> 16);
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[1] = (value << 8);
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceStartCounterResettable[2] = value;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    }
    // check whether to update light source on time diff
    if (pCtrlInst->gearVariables.lampOn)
    {
      pCtrlInst->gearOnDiff += diffTime;
    }
    // check whether to update light source on time and light source on time resettable
    value2 = (pCtrlInst->gearOnDiff / 1000); // diff in seconds
    if (1 <= value2)
    {
      pCtrlInst->gearOnDiff -= (value2 * 1000);
      // copy value
      memcpy(&value,
          &pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTimeResettable,
          sizeof(value));
      // increment value
      value += value2;
      // store value
      memcpy(&pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTimeResettable,
          &value,
          sizeof(pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTimeResettable));
      // copy value
      memcpy(&value,
          &pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTime,
          sizeof(value));
      // increment value
      value += value2;
      // store value
      memcpy(&pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTime,
          &value,
          sizeof(pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOnTime));
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearLampOnCounted)
    {
      pCtrlInst->gearLampOnCounted = FALSE;
    }
  }

  // check whether to update lamp failure condition and counter
  if (TRUE == pCtrlInst->gearVariables.lampFailure)
  {
    if (FALSE == pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition)
    {
      pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition = TRUE;
      pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOverallFailureConditionCounter++;
      pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
      if (0 == pCtrlInst->gearPersistentChangedTime)
      {
        pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }

      // check whether to update short/open circuit condition and counter
      if (TRUE == pCtrlInst->gearShortCircuit)
      {
        pCtrlInst->gearMemBank206RAM.lightSourceShortCircuit = TRUE;
        pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceShortCircuitCounter++;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
      else
      {
        pCtrlInst->gearMemBank206RAM.lightSourceOpenCircuit = TRUE;
        pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceOpenCircuitCounter++;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    }
  }
  else
  {
    if (TRUE == pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition)
    {
      pCtrlInst->gearMemBank206RAM.lightSourceOverallFailureCondition = FALSE;
    }
  }
  // check whether to update light source thermal derating and counter
   if (pCtrlInst->gearMemBank206RAM.lightSourceTemperature > pInst->config.vendor.diag_maintenance_params.lightSourceThermalDeratingThreshold)
   {
     if (FALSE == pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating)
     {
       pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating = TRUE;
       pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalDeratingCounter++;
       pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
       if (0 == pCtrlInst->gearPersistentChangedTime)
       {
         pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
       }
       pCtrlInst->gearVariables.lampFailure = TRUE;
     }
     // check whether to update light source thermal shutdown and counter
     if (pCtrlInst->gearMemBank206RAM.lightSourceTemperature > pInst->config.vendor.diag_maintenance_params.lightSourceThermalShutdownThreshold)
     {
       if (FALSE == pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown)
       {
         pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown = TRUE;
         pCtrlInst->gearPersistent.gearMemBank206NVM.lightSourceThermalShutdownCounter++;
         pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_DIAG_MAINTENANCE_MEMBANK206_MASK;
         if (0 == pCtrlInst->gearPersistentChangedTime)
         {
           pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
         }
         pCtrlInst->gearVariables.lampFailure = TRUE;
       }
     }
     else
     {
       if (TRUE == pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown)
       {
         pCtrlInst->gearMemBank206RAM.lightSourceThermalShutdown = FALSE;
       }
     }
   }
   else
   {
     if (TRUE == pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating)
     {
       pCtrlInst->gearMemBank206RAM.lightSourceThermalDerating = FALSE;
     }
   }

  // increment gear last time
  pCtrlInst->gearLastTime = pInst->currentTime;
}

#endif /* DALILIB_GEAR_DIAG_MAINTENANCE_PRESENT */
