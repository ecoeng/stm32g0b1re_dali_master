/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * device.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"

#ifdef DALILIB_DEV_LUMINAIRE_PRESENT


//----------------------------------------------------------------------
// initialize the memory after external power cycle
//----------------------------------------------------------------------
void dev_luminaire_init_after_power_cycle(PCtrlDeviceInstance pCtrlInst)
{
  // initialize memory bank 201 RAM memory cells
  pCtrlInst->deviceMemBank201RAM.lockByte = DALI_MASK;
}

//----------------------------------------------------------------------
// initialize the default variable of the luminaire-mounted control device
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_luminaire_init_persistent(PDaliLibInstance                   pInst,
                                               dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  if ((pInst->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
      ((pInst->config.vendor.control_device_mode == CTRL_DEV_MODE_MULTI) &&
          (pInst->config.vendor.luminaire_params.applicationControllerArbitrationActive)))
  {
    pDevicePersistent->deviceMemBank201NVM.applicationControllerArbitration = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// reset luminaire-mounted memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_luminaire_reset_memory_bank(PCtrlDeviceInstance pCtrlInst,
                                                 uint8_t             nMembank)
{
  // check whether to reset memory bank 201
  if ((CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->deviceMemBank201RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->deviceMemBank201RAM.lockByte = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read luminaire-mounted control devices memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_luminaire_read_memory_bank201(PCtrlDeviceInstance  pCtrlInst,
                                                   uint8_t*             pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->deviceVariables.DTR0 <= CTRL_DEVICE_MEMORY_BANK_201_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->deviceVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_DEVICE_MEMORY_BANK_201_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->deviceMemBank201RAM.lockByte;
      break;

      case 0x03: // Version of the memory bank
        *pRespValue = CTRL_DEVICE_MEMORY_BANK_201_VERSION;
      break;

      case CTRL_DEVICE_MEMORY_BANK_201_TYPE_OF_DEVICE_BYTE:
        if (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_SINGLE)
        {
          *pRespValue = CTRL_DEVICE_LUMINAIRE_TYPE_D;
        }
        else
        {
          if ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
              (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.applicationControllerArbitrationActive))
          {
            *pRespValue = CTRL_DEVICE_LUMINAIRE_TYPE_B;
          }
          else
          {
            // multi-device application controller
            if (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.isBusPoweredDevice)
            {
              *pRespValue = CTRL_DEVICE_LUMINAIRE_TYPE_C;
            }
            else
            {
              *pRespValue = CTRL_DEVICE_LUMINAIRE_TYPE_A;
            }
          }
        }
      break;

      case CTRL_DEVICE_MEMORY_BANK_201_MAX_CURRENT_CONSUMED_BYTE:
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.maxCurrentConsumed;
      break;

      case CTRL_DEVICE_MEMORY_BANK_201_MAX_POWER_CONSUMED_BYTE:
        *pRespValue = ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.maxPowerConsumed;
      break;

      case CTRL_DEVICE_MEMORY_BANK_201_APP_CTRL_ARBITRATION_BYTE:
        if ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
            ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_MULTI) &&
                (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.applicationControllerArbitrationActive)))
        {
          *pRespValue = pCtrlInst->devicePersistent.deviceMemBank201NVM.applicationControllerArbitration;
        }
        else
        {
          pRespValue = 0x00;
        }
      break;

      default:
        rc = R_IGNORE_FRAME;
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_luminaire_write_memory_bank201_direct(PCtrlDeviceInstance  pCtrlInst,
                                                           uint8_t              nCell,
                                                           uint8_t              reqValue)
{
  // check cell offset
  if (CTRL_DEVICE_MEMORY_BANK_201_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE:
      // write memory cell
      pCtrlInst->deviceMemBank201RAM.lockByte = reqValue;
    break;

    case CTRL_DEVICE_MEMORY_BANK_201_APP_CTRL_ARBITRATION_BYTE:

      if ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
          ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_MULTI) &&
              (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.applicationControllerArbitrationActive)))
      {
        // write memory cell
        if (pCtrlInst->devicePersistent.deviceMemBank201NVM.applicationControllerArbitration != reqValue)
        {
          pCtrlInst->devicePersistent.deviceMemBank201NVM.applicationControllerArbitration = reqValue;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }

        }
      }
      else
      {
        // cell not writable
        return (R_IGNORE_FRAME);
      }
    break;

    default:
      // cell not writable
      return (R_IGNORE_FRAME);
    break;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// write power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_luminaire_write_memory_bank201(PCtrlDeviceInstance     pCtrlInst,
                                                    ctrl_device_commands_t* pDevCmd,
                                                    uint8_t                 reqValue,
                                                    uint8_t*                pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;
  uint8_t           offset;
  dalilib_action_t  action;

  offset = pCtrlInst->deviceVariables.DTR0;

  // check cell offset
  if ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE                  != offset) &&
      (CTRL_DEVICE_MEMORY_BANK_201_APP_CTRL_ARBITRATION_BYTE != offset))
  {
    return (R_IGNORE_FRAME);
  }

  // check whether memory bank is unlocked
  if ((DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != offset) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK != pCtrlInst->deviceMemBank201RAM.lockByte))
  {
    return (R_IGNORE_FRAME);
  }

  // check value change
  if ((CTRL_DEVICE_MEMORY_BANK_201_APP_CTRL_ARBITRATION_BYTE == offset) &&
      ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
       ((((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode == CTRL_DEV_MODE_MULTI) &&
        (((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.luminaire_params.applicationControllerArbitrationActive))))
  {
    // notify application
    memset(&action, 0, sizeof(action));
    action.actionType = DALILIB_CTRL_GEAR_REACTION;
    action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_MEMORY_CELL_CHANGED;
    action.gearReact.memoryValue.memoryBankNr = CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER;
    action.gearReact.memoryValue.memoryCellIdx = offset;
    action.gearReact.memoryValue.value = reqValue;
    action.gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
    dali_cb_reaction(pCtrlInst->pInst, &action);
  }

  // write memory bank cell
  rc = dev_luminaire_write_memory_bank201_direct(pCtrlInst, offset, reqValue);

  // check whether to reply
  if ((R_DALILIB_OK == rc) &&
      (GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pDevCmd->eDevCmd))
  {
    *pRespValue = reqValue;
  }

  return (rc);
}

R_DALILIB_RESULT device_luminaire_save_mem_block_pers_vars(PDaliLibInstance                   pInst,
                                                           dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT                                  rc = R_DALILIB_OK;
  dali_ctrl_device_luminaire_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_DEVICE_PERS_LUMINAIRE_ID;
  block.version = DALI_CTRL_DEVICE_PERS_LUMINAIRE_VERSION;

  // populate memory block
  block.applicationControllerArbitration = pDevicePersistent->deviceMemBank201NVM.applicationControllerArbitration;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_DEVICE_PERS_LUMINAIRE_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags &= ~DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT device_luminaire_apply_mem_block_pers_vars(PCtrlDeviceInstance pCtrlInst,
                                                            uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_device_mem_bank201_NVM_t*                pPersistentVariables = &pCtrlInst->devicePersistent.deviceMemBank201NVM;
  dali_ctrl_device_luminaire_pers_variables_block_t* pBlock = (dali_ctrl_device_luminaire_pers_variables_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->applicationControllerArbitration = pBlock->applicationControllerArbitration;

  // check value range
  if (0xFE  > pPersistentVariables->applicationControllerArbitration &&
      0x00 != pPersistentVariables->applicationControllerArbitration)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable applicationControllerArbitration is out of bounds. Value: %u Range: [0x00, 0xFE, 0xFF]",
             pPersistentVariables->applicationControllerArbitration);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->applicationControllerArbitration = DALI_MASK;
  }

  return (rc);
}


//----------------------------------------------------------------------
// luminaire-mounted control device timer function
//----------------------------------------------------------------------
void dev_luminaire_timingHelper(PCtrlDeviceInstance pCtrlInst)
{
  PDaliLibInstance  pInst = pCtrlInst->pInst;
  uint32_t          diffTime = 0;

  if ((pInst->config.vendor.control_device_mode == CTRL_DEV_MODE_INPUT) ||
      (pInst->config.vendor.luminaire_params.applicationControllerArbitrationActive))
  {
    if (0x00 != pCtrlInst->devicePersistent.deviceMemBank201NVM.applicationControllerArbitration)
    {
      // Type B arbitration
      if ((0 == pCtrlInst->deviceLastQueryAppCtrlEnabled) && // first query
          pInst->currentTime > CTRL_DEVICE_LUMINAIRE_FIRST_QUERY_DELAY)
      {
        //pCtrlInst->deviceLastQueryAppCtrlEnabled == pInst->currentTime;
      }
      else
      {
        diffTime = dali_difftime(pInst->currentTime, pCtrlInst->deviceLastQueryAppCtrlEnabled);
        if (diffTime > CTRL_DEVICE_LUMINAIRE_REPEATING_QUERY_DELAY)
        {
          //pCtrlInst->deviceLastQueryAppCtrlEnabled == pInst->currentTime;
        }
      }
    }
  }
}

#endif /* DALILIB_DEV_LUMINAIRE_PRESENT */
