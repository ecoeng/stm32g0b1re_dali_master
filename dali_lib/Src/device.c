/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * device.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"

// control device commands
#define _CTRL_DEVICE_CMDS_
#ifdef _CTRL_DEVICE_CMDS_
// eDevCmd,                                    inst,opcode, a, t
static ctrl_device_commands_t dev_commands_m[] =
{
  // Device commands
  {DEV_CMD_IDENTIFY_DEVICE,                    0xFE, 0x00,  0, 1},
  {DEV_CMD_RESET_POWER_CYCLE_SEEN,             0xFE, 0x01,  0, 1},
  {DEV_CMD_RESET,                              0xFE, 0x10,  0, 1},
  {DEV_CMD_RESET_MEMORY_BANK,                  0xFE, 0x11,  0, 1},
  {DEV_CMD_SET_SHORT_ADDRESS,                  0xFE, 0x14,  0, 1},
  {DEV_CMD_ENABLE_WRITE_MEMORY,                0xFE, 0x15,  0, 1},
  {DEV_CMD_ENABLE_APPLICATION_CONTROLLER,      0xFE, 0x16,  0, 1},
  {DEV_CMD_DISABLE_APPLICATION_CONTROLLER,     0xFE, 0x17,  0, 1},
  {DEV_CMD_SET_OPERATING_MODE,                 0xFE, 0x18,  0, 1},
  {DEV_CMD_ADD_TO_DEVICE_GROUPS_0_15,          0xFE, 0x19,  0, 1},
  {DEV_CMD_ADD_TO_DEVICE_GROUPS_16_31,         0xFE, 0x1A,  0, 1},
  {DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_0_15,     0xFE, 0x1B,  0, 1},
  {DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_16_31,    0xFE, 0x1C,  0, 1},
  {DEV_CMD_START_QUIESCENT_MODE,               0xFE, 0x1D,  0, 1},
  {DEV_CMD_STOP_QUIESCENT_MODE,                0xFE, 0x1E,  0, 1},
  {DEV_CMD_ENABLE_POWER_CYCLE_NOTIFICATION,    0xFE, 0x1F,  0, 1},
  {DEV_CMD_DISABLE_POWER_CYCLE_NOTIFICATION,   0xFE, 0x20,  0, 1},
  {DEV_CMD_SAVE_PERSISTENT_VARIABLES,          0xFE, 0x21,  0, 1},

  {DEV_CMD_QUERY_DEVICE_STATUS,                0xFE, 0x30,  1, 0},
  {DEV_CMD_QUERY_APPLICATION_CONTROLLER_ERROR, 0xFE, 0x31,  1, 0},
  {DEV_CMD_QUERY_INPUT_DEVICE_ERROR,           0xFE, 0x32,  1, 0},
  {DEV_CMD_QUERY_MISSING_SHORT_ADDRESS,        0xFE, 0x33,  1, 0},
  {DEV_CMD_QUERY_VERSION_NUMBER,               0xFE, 0x34,  1, 0},
  {DEV_CMD_QUERY_NUMBER_OF_INSTANCES,          0xFE, 0x35,  1, 0},
  {DEV_CMD_QUERY_CONTENT_DTR0,                 0xFE, 0x36,  1, 0},
  {DEV_CMD_QUERY_CONTENT_DTR1,                 0xFE, 0x37,  1, 0},
  {DEV_CMD_QUERY_CONTENT_DTR2,                 0xFE, 0x38,  1, 0},
  {DEV_CMD_QUERY_RANDOM_ADDRESS_H,             0xFE, 0x39,  1, 0},
  {DEV_CMD_QUERY_RANDOM_ADDRESS_M,             0xFE, 0x3A,  1, 0},
  {DEV_CMD_QUERY_RANDOM_ADDRESS_L,             0xFE, 0x3B,  1, 0},
  {DEV_CMD_READ_MEMORY_LOCATION,               0xFE, 0x3C,  1, 0},
  {DEV_CMD_READ_MEMORY_CELL,                   0xFE, 0x3C,  1, 0},
  {DEV_CMD_QUERY_APPLICATION_CONTROL_ENABLED,  0xFE, 0x3D,  1, 0},
  {DEV_CMD_QUERY_OPERATING_MODE,               0xFE, 0x3E,  1, 0},
  {DEV_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE,   0xFE, 0x3F,  1, 0},
  {DEV_CMD_QUERY_QUIESCENT_MODE,               0xFE, 0x40,  1, 0},
  {DEV_CMD_QUERY_DEVICE_GROUPS_0_7,            0xFE, 0x41,  1, 0},
  {DEV_CMD_QUERY_DEVICE_GROUPS_8_15,           0xFE, 0x42,  1, 0},
  {DEV_CMD_QUERY_DEVICE_GROUPS_16_23,          0xFE, 0x43,  1, 0},
  {DEV_CMD_QUERY_DEVICE_GROUPS_24_31,          0xFE, 0x44,  1, 0},
  {DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION,     0xFE, 0x45,  1, 0},
  {DEV_CMD_QUERY_DEVICE_CAPABILITIES,          0xFE, 0x46,  1, 0},
  {DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER,      0xFE, 0x47,  1, 0},
  {DEV_CMD_QUERY_RESET_STATE,                  0xFE, 0x48,  1, 0},

  // Instance commands
  // DALI_CTRL_DEVICE_FIRST_INSTANCE_CMD_OPCODE     --v
  {DEV_CMD_SET_EVENT_PRIORITY,                 0x00, 0x61,  0, 1},
  {DEV_CMD_ENABLE_INSTANCE,                    0x00, 0x62,  0, 1},
  {DEV_CMD_DISABLE_INSTANCE,                   0x00, 0x63,  0, 1},

  {DEV_CMD_SET_PRIMARY_INSTANCE_GROUP,         0x00, 0x64,  0, 1},
  {DEV_CMD_SET_INSTANCE_GROUP_1,               0x00, 0x65,  0, 1},
  {DEV_CMD_SET_INSTANCE_GROUP_2,               0x00, 0x66,  0, 1},
  {DEV_CMD_SET_EVENT_SCHEME,                   0x00, 0x67,  0, 1},
  {DEV_CMD_SET_EVENT_FILTER,                   0x00, 0x68,  0, 1},

  {DEV_CMD_QUERY_INSTANCE_TYPE,                0x00, 0x80,  1, 0},
  {DEV_CMD_QUERY_RESOLUTION,                   0x00, 0x81,  1, 0},
  {DEV_CMD_QUERY_INSTANCE_ERROR,               0x00, 0x82,  1, 0},
  {DEV_CMD_QUERY_INSTANCE_STATUS,              0x00, 0x83,  1, 0},
  {DEV_CMD_QUERY_EVENT_PRIORITY,               0x00, 0x84,  1, 0},
  {DEV_CMD_QUERY_INSTANCE_ENABLED,             0x00, 0x86,  1, 0},
  {DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP,       0x00, 0x88,  1, 0},
  {DEV_CMD_QUERY_INSTANCE_GROUP_1,             0x00, 0x89,  1, 0},
  {DEV_CMD_QUERY_INSTANCE_GROUP_2,             0x00, 0x8A,  1, 0},
  {DEV_CMD_QUERY_EVENT_SCHEME,                 0x00, 0x8B,  1, 0},
  {DEV_CMD_QUERY_INPUT_VALUE,                  0x00, 0x8C,  1, 0},
  {DEV_CMD_QUERY_INPUT_VALUE_LATCH,            0x00, 0x8D,  1, 0},
  {DEV_CMD_QUERY_FEATURE_TYPE,                 0x00, 0x8E,  1, 0},
  {DEV_CMD_QUERY_NEXT_FEATURE_TYPE,            0x00, 0x8F,  1, 0},
  {DEV_CMD_QUERY_EVENT_FILTER_0_7,             0x00, 0x90,  1, 0},
  {DEV_CMD_QUERY_EVENT_FILTER_8_15,            0x00, 0x91,  1, 0},
  {DEV_CMD_QUERY_EVENT_FILTER_16_23,           0x00, 0x92,  1, 0},
  // DALI_CTRL_DEVICE_LAST_INSTANCE_CMD_OPCODE     ---^

  // Special commands
  {DEV_CMD_TERMINATE,                          0x00, 0xC1,  0, 0},
  {DEV_CMD_INITIALISE,                         0x01, 0xC1,  0, 1},
  {DEV_CMD_RANDOMISE,                          0x02, 0xC1,  0, 1},
  {DEV_CMD_COMPARE,                            0x03, 0xC1,  1, 0},
  {DEV_CMD_WITHDRAW,                           0x04, 0xC1,  0, 0},
  {DEV_CMD_SEARCHADDRH,                        0x05, 0xC1,  0, 0},
  {DEV_CMD_SEARCHADDRM,                        0x06, 0xC1,  0, 0},
  {DEV_CMD_SEARCHADDRL,                        0x07, 0xC1,  0, 0},
  {DEV_CMD_PROGRAM_SHORT_ADDRESS,              0x08, 0xC1,  0, 0},
  {DEV_CMD_VERIFY_SHORT_ADDRESS,               0x09, 0xC1,  1, 0},
  {DEV_CMD_QUERY_SHORT_ADDRESS,                0x0A, 0xC1,  1, 0},
  {DEV_CMD_WRITE_MEMORY_LOCATION,              0x20, 0xC1,  1, 0},
  {DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY,     0x21, 0xC1,  0, 0},
  {DEV_CMD_DTR0,                               0x30, 0xC1,  0, 0},
  {DEV_CMD_DTR1,                               0x31, 0xC1,  0, 0},
  {DEV_CMD_DTR2,                               0x32, 0xC1,  0, 0},
  {DEV_CMD_SEND_TESTFRAME,                     0x33, 0xC1,  0, 0},
  {DEV_CMD_DIRECT_WRITE_MEMORY,                0x00, 0xC5,  1, 0},
  {DEV_CMD_DTR1_DTR0,                          0x00, 0xC7,  0, 0},
  {DEV_CMD_DTR2_DTR1,                          0x00, 0xC9,  0, 0},

  {DEV_CMD_LAST,                               0x00, 0x00,  0, 0},
};
#endif // _CTRL_DEVICE_CMDS_

/**********************************************************************
 * internal helper functions
 **********************************************************************/


//----------------------------------------------------------------------
// execute control device reaction
//----------------------------------------------------------------------

static R_DALILIB_RESULT device_reaction(PCtrlDeviceInstance                pCtrlInst,
                                        DALILIB_CTRL_DEVICE_REACTION_CODES reactionCode,
                                        uint32_t                           value)
{
  dalilib_action_t action;

  memset( &action, 0, sizeof(action));

  action.actionType = DALILIB_CTRL_DEVICE_REACTION;
  action.deviceReact.reactionCode = reactionCode;
  action.deviceReact.reactValue = value;
  action.deviceReact.valueType = DALILIB_RESPONSE_VALUE_VALID;

  return (dali_cb_reaction(pCtrlInst->pInst, &action));
}

//----------------------------------------------------------------------
// check the writeEnableState variable
//----------------------------------------------------------------------

static WRITE_ENABLE_STATE device_check_writeEnableState(PCtrlDevVariables pVariables,
                                                        DEV_CMD           eDeviceCmd)
{
  (void)pVariables;

  switch(eDeviceCmd)
  {
    case DEV_CMD_WRITE_MEMORY_LOCATION:
    case DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
    case DEV_CMD_DIRECT_WRITE_MEMORY:
    case DEV_CMD_DTR0:
    case DEV_CMD_DTR1:
    case DEV_CMD_DTR2:
    case DEV_CMD_QUERY_CONTENT_DTR0:
    case DEV_CMD_QUERY_CONTENT_DTR1:
    case DEV_CMD_QUERY_CONTENT_DTR2:
    case DEV_CMD_DTR1_DTR0:
    case DEV_CMD_DTR2_DTR1:
      return (WRITE_ENABLE_STATE_ENABLED);
    default:
    break;
  }

  return(WRITE_ENABLE_STATE_DISABLED);
}

//----------------------------------------------------------------------
// execute control device event reaction
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_event_reaction(PCtrlDeviceInstance           pCtrlInst,
                                              uint16_t                      eventInfo,
                                              ctrl_device_event_message_t*  pEventMessage)
{
  dalilib_action_t action;

  memset( &action, 0, sizeof(action));

  action.actionType = DALILIB_EVENT_ACTION;
  action.eventReact.rawEventInfo = eventInfo;
  memcpy(&action.eventReact.eventMessage,
         pEventMessage,
         sizeof(ctrl_device_event_message_t));

  return (dali_cb_reaction(pCtrlInst->pInst, &action));
}


//----------------------------------------------------------------------
// generate random address
//----------------------------------------------------------------------
static uint32_t device_create_random_address(PCtrlDeviceInstance pCtrlInst)
{
  uint32_t randomAddress = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[0] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[1] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[2] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[3] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[4] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[5] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[0] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[1] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[2] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[3] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[4] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[5] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[6] +
                             pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[7] +
                             ((PDaliLibInstance)pCtrlInst->pInst)->instanceID;
  srand(randomAddress);
  
  return (rand() % DALI_CTRL_DEVICE_RANDOM_RANGE_MAX);
}


//----------------------------------------------------------------------
// search device command item for opcodebyte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_device_commands_t* device_get_cmd_by_opcode (uint8_t opcodeByte)
{
  uint8_t idx = 0;
  for (idx = 0; idx < COUNTOF(dev_commands_m); idx++)
  {
    if (dev_commands_m[idx].instanceByte == DALI_CTRL_DEVICE_DEFAULT_INSTANCE_BYTE &&
        dev_commands_m[idx].opcodeByte   == opcodeByte)
    {
      return (&dev_commands_m[idx]);
    }
  }
  
  return (NULL);
}

//----------------------------------------------------------------------
// search device command item for instancebyte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_device_commands_t* device_get_cmd_by_instance(uint8_t opcodeByte)
{
  uint8_t idx = 0;
  for (idx = 0; idx < COUNTOF(dev_commands_m)-1; idx++)
  {
    if (dev_commands_m[idx].instanceByte == 0x00 &&
        dev_commands_m[idx].opcodeByte   == opcodeByte)
    {
      return (&dev_commands_m[idx]);
    }
  }
  
  return (NULL);
}

//----------------------------------------------------------------------
// search specific device command
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_device_commands_t* device_get_cmd_by_spec(uint8_t specByte,
                                                      uint8_t instanceByte)
{
  uint8_t idx = 0;

  if (specByte == 0xC5 || specByte == 0xC7 || specByte == 0xC9)
  {
    instanceByte = 0x00;
  }
  
  for (idx = 0; idx < COUNTOF(dev_commands_m); idx++)
  {
    if (dev_commands_m[idx].instanceByte == instanceByte &&
        dev_commands_m[idx].opcodeByte   == specByte)
    {
      return (&dev_commands_m[idx]);
    }
  }
  
  return (NULL);
}

//----------------------------------------------------------------------
// search control device command
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_device_commands_t* device_get_cmd_by_cmd(DEV_CMD dev_cmd)
{
  uint8_t idx = 0;
  
  for (idx = 0; idx < COUNTOF(dev_commands_m); idx++)
  {
    if (dev_commands_m[idx].eDevCmd == dev_cmd)
    {
      return (&dev_commands_m[idx]);
    }
  }
  
  return (NULL);
}

//----------------------------------------------------------------------
// reset memory bank - X
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_reset_memory_bank(PCtrlDeviceInstance pCtrlInst,
                                                 uint8_t             nMembank)
{
  (void) pCtrlInst;
  (void) nMembank;

  // memory bank 0 will not reset.

  // reset control device extensions' memory bank
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    dev_luminaire_reset_memory_bank(pCtrlInst, nMembank);
  #endif

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_init_by_reset(PCtrlDevVariables               pVariables,
                                             PCtrlDevPersistentVariables     pPeristensVariables,
                                             PCtrlDevInstPersistentVariables pInstPersVariables,
                                             PCtrlDevInstVariables           pInstVariables)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  // initialize the RAM - Parameters
  pVariables->searchAddress              = 0x00FFFFFF;
  pVariables->quiescentMode              = QUIESCENT_MODE_DISABLED;
  pVariables->writeEnableState           = WRITE_ENABLE_STATE_DISABLED;
  pVariables->powerCycleSeen             = FALSE;
  pVariables->applicationControllerError = FALSE;
  pVariables->inputDeviceError           = FALSE;
  pVariables->resetState                 = TRUE;

  pInstVariables->instanceError          = FALSE;

  // initialize the persistent (NVM) Parameters
  pPeristensVariables->deviceGroups      = 0x00000000;
  pPeristensVariables->randomAddress     = 0x00FFFFFF;

  pInstPersVariables->instanceGroup0     = DALI_MASK;
  pInstPersVariables->instanceGroup1     = DALI_MASK;
  pInstPersVariables->instanceGroup2     = DALI_MASK;

  pInstPersVariables->eventFilter        = 0x00FFFFFF;
  pInstPersVariables->eventScheme        = 0x00;

  switch(pInstPersVariables->instanceType)
  {
    case INPUT_DEVICE_INST_0:
      rc = R_DALILIB_OK;
    break;
    
    case INPUT_DEVICE_INST_PUSH_BUTTONS:
      dev_pushbtn_by_reset(pInstPersVariables);
      rc = R_DALILIB_OK;
    break;
    
    case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
      dev_occupancy_by_reset(pVariables, pInstPersVariables);
      rc = R_DALILIB_OK;
    break;
    
    case INPUT_DEVICE_INST_LIGHT_SENSOR:
      dev_lightsensor_by_reset(pVariables, pInstPersVariables);
      rc = R_DALILIB_OK;
    break;
    
    default:
      rc = R_DALILIB_INPUT_DEVICE_TYPE_NOT_SUPPORTED;
    break;
  }
  
  return (rc);
}


//----------------------------------------------------------------------
// generate random power notification time
// between 1s, 3s and 5s
// return: [ms]
//----------------------------------------------------------------------
static uint32_t device_create_random_power_notify_time(PCtrlDeviceInstance pCtrlInst)
{
  uint32_t ret = 0;
  uint32_t randomAddress = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[0] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[1] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[2] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[3] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[4] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.gtin[5] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[0] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[1] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[2] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[3] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[4] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[5] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[6] +
                            pCtrlInst->deviceMemBank0.ctrlMemBank0.identify_number[7] +
                            ((PDaliLibInstance)pCtrlInst->pInst)->instanceID;
                            
    srand(randomAddress);
    /* form IEC62386-103:
     * The event message shall be sent once using priority 2 and with a uniformly
     * distributed delay between 1,3s and 5s after completion of the power-on procedure.
     * from Technical Manager DALI Alliance:
     * It should state 1,3 s and 5 s. This correction is also stated in our Clarifications document, section 6.3.
     * The software in your product should implement a shorter time to avoid being on the allowed limit.
     */
    ret = (rand() % 3601 + 1300); // 1300 - 4900 ms

    return (ret);
}


//----------------------------------------------------------------------
// initialize the variable after external power cycle
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_init_after_power_cycle(PCtrlDeviceInstance pCtrlInst)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PCtrlDevVariables pVariables = NULL;
  PCtrlDevInstVariables pInstVariables = NULL;

  pVariables = &pCtrlInst->deviceVariables;
  pInstVariables = &pCtrlInst->deviceInstVariables;

  // initialize the RAM - Parameters
  pVariables->searchAddress              = 0x00FFFFFF;
  pVariables->DTR0                       = 0x00;
  pVariables->DTR1                       = 0x00;
  pVariables->DTR2                       = 0x00;
  pVariables->quiescentMode              = QUIESCENT_MODE_DISABLED;
  pVariables->writeEnableState           = WRITE_ENABLE_STATE_DISABLED;
  pVariables->powerCycleSeen             = TRUE;
  pVariables->initialisationState        = CTRL_DEVICE_INITIALISE_STATE_DISABLED;
  pVariables->applicationControllerError = FALSE;
  pVariables->inputDeviceError           = FALSE;
  pVariables->resetState                 = TRUE;

  pInstVariables->instanceError          = FALSE;

  if (POWER_CYCLE_NOTIFY_ENABLED == pCtrlInst->devicePersistent.persVariables.powerCycleNotification)
  {
    pCtrlInst->devicePowerCycleNotificationSend = 0;
  }

  // initialize control device extensions
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    dev_luminaire_init_after_power_cycle(pCtrlInst);
  #endif
  
  return (rc);
}

//----------------------------------------------------------------------
// return the lowest device group
//----------------------------------------------------------------------
static uint8_t device_get_lowest_device_group(PCtrlDeviceInstance pCtrlInst)
{
  uint8_t bitPos = 0x00;
  uint32_t devGroupAdr = pCtrlInst->devicePersistent.persVariables.deviceGroups;

  for (bitPos = 0x00; bitPos < 32; bitPos++)
  {
    if ( devGroupAdr & 0x01 )
    {
      break;
    }
    devGroupAdr >>= 1;
  }

  return (bitPos + 1);
}

//----------------------------------------------------------------------
// After completing its external power cycle,
// a control device shall generate a power cycle event message
// if “powerCycleNotification” is ENABLED.
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_create_power_cycle_notify(PCtrlDeviceInstance pCtrlInst)
{
  uint16_t notifyAddress = 0x0000;
  send_frame_t notifyFrame;

  memset (&notifyFrame, 0, sizeof(notifyFrame));

  PCtrlDevPersistentVariables pPersVariables = &pCtrlInst->devicePersistent.persVariables;

  if (POWER_CYCLE_NOTIFY_ENABLED != pPersVariables->powerCycleNotification)
  {
    return (R_DALILIB_OK);
  }

  notifyFrame.frame.priority = FRAME_PRIO_FF_P2;
  notifyFrame.frame.datalength = FRAME_CTRL_DEV_FF_LEN;
  notifyFrame.type = SEND_FRAME_TYPE_CTRL_DEV;

  notifyFrame.frame.data = DALI_CTRL_DEVICE_POWER_NOTIFICATION_BYTE;
  notifyFrame.frame.data <<= 13;

  notifyAddress = ( pPersVariables->deviceGroups > 0x00 && pPersVariables->deviceGroups < 0xFFFFFFFF) ? TRUE : FALSE;
  notifyAddress <<= 5;
  if ( pPersVariables->deviceGroups > 0x00 && pPersVariables->deviceGroups < 0xFFFFFFFF)
  {
    notifyAddress |= device_get_lowest_device_group(pCtrlInst);
  }
  notifyAddress <<= 1;
  notifyAddress |= ( pPersVariables->shortAddress != DALI_MASK ? TRUE : FALSE );
  notifyAddress <<= 6;
  if (pPersVariables->shortAddress != DALI_MASK)
  {
    notifyAddress |= pPersVariables->shortAddress;
  }
  notifyFrame.frame.data |= notifyAddress;
  notifyFrame.cmds[0] = DALI_CTRL_DEVICE_POWER_NOTIFICATION_CMD;
  
  return (dali_cb_send(pCtrlInst->pInst, &notifyFrame ));
}

//----------------------------------------------------------------------
// create a new event message forward frame for the control device
// will be triggered from application for control devices
// The event information provides the 10-bit event number and/or event data.
// Event information is instance type specific and is defined in
// the applicable Parts 3xx of this standard that describe the instance type.
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_create_event(PDaliLibInstance pInst,
                                            send_frame_t*    pNewSendFrame,
                                            device_adr_t*    pAdr,
                                            uint16_t*        pEventInfo)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  PCtrlDeviceInstance pCtrlInst = &pInst->ctrlInst.deviceInstance;
  uint32_t eventFrame = 0;
  
  switch(pCtrlInst->devicePersistent.instPersVariables.eventScheme)
  {
    case EVENT_SCHEME_INSTANCE:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x00; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits instance type
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceNumber;
    break;
    
    case EVENT_SCHEME_DEVICE:
      // 22. - 17.bits device short address
      eventFrame = pCtrlInst->devicePersistent.persVariables.shortAddress;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x00; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance types
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    case EVENT_SCHEME_DEVICE_INSTANCE:
      // 22. - 17.bits device short address
      eventFrame = pCtrlInst->devicePersistent.persVariables.shortAddress;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceNumber;
    break;
    
    case EVENT_SCHEME_DEVICE_GROUP:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x00; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits device group
      eventFrame |= pAdr->adr;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x00; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance types
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    case EVENT_SCHEME_INSTANCE_GROUP:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x01; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits instance groups
      eventFrame |= pAdr->instance;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_INVALID_SCHEME;
    break;
  }

  // insert eventInfo data
  eventFrame <<= 10;
  eventFrame |= *pEventInfo;

  //create ff frame
  pNewSendFrame->frame.priority   = pCtrlInst->devicePersistent.instPersVariables.eventPriority;
  pNewSendFrame->frame.datalength = FRAME_CTRL_DEV_FF_LEN;
  pNewSendFrame->frame.data       = eventFrame;
  pNewSendFrame->status           = FRAME_WITHOUT_RESPONSE;
  pNewSendFrame->type             = SEND_FRAME_TYPE_CTRL_EVENT;

  return (rc);
}

//----------------------------------------------------------------------
// create a new forward frame for the control device
// will be trigger from application for other control devices
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_create_ff(PDaliLibInstance        pInst,
                                         ctrl_device_commands_t* pInputDevCmd,
                                         uint32_t                cmd,
                                         send_frame_t*           pNewSendFrame,
                                         device_adr_t*           pAdr, ...)
{
  R_DALILIB_RESULT        rc = R_DALILIB_OK;
  ctrl_device_commands_t* pDevCmd = NULL;
  va_list ap;

  (void)pInst;

  if (DALI_ADRTYPE_SHORT == pAdr->adrType)
  {
    if (pAdr->adr > 63)
    {
      return (R_DALILIB_CTRL_DEVICE_INVALID_ADDRESS);
    }
  }
  else 
  {
    if (DALI_ADRTYPE_GRP == pAdr->adrType)
    {
      if (pAdr->adr == 0x00)
      {
        return (R_DALILIB_CTRL_DEVICE_INVALID_ADDRESS);
      }
    }
  }
  // is base control device command
  if (NULL == pInputDevCmd)
  {
    pDevCmd = device_get_cmd_by_cmd((DEV_CMD)cmd);
    if (NULL == pDevCmd)
    {
      return (R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED);
    }
  }
  // input device command
  else
  {
    pDevCmd = pInputDevCmd;
  }

  // standard commands
  if (DALI_CTRL_DEVICE_DEFAULT_INSTANCE_BYTE == pDevCmd->instanceByte)
  {
    // encode forward frame
    rc = frame_encode_ctrl_device_command(pDevCmd,
                                          pAdr,
                                          FRAME_PRIO_FF_P3,
                                          &pNewSendFrame->frame);
  }
  // instance commands
  else
  {
    if (0x00 == pDevCmd->instanceByte && pDevCmd->opcodeByte < 0xC1)
    {
      // encode forward frame with instance byte
       rc = frame_encode_ctrl_device_command(pDevCmd,
                                             pAdr,
                                             FRAME_PRIO_FF_P3,
                                             &pNewSendFrame->frame);
    }
    // specific command
    else
    {
      uint8_t instanceByte = 0x00;
      uint8_t valueByte = 0x00;

      if (0xC1 == pDevCmd->opcodeByte)
      {
        instanceByte = pDevCmd->instanceByte;
        va_start(ap, pAdr);
        valueByte = (va_arg(ap, int) & 0xFF);
        va_end(ap);
      }
      // DIRECT WRITE MEMORY
      // DTR1:DTR0
      // DTR2:DTR1
      else
      {
        va_start(ap, pAdr);
        instanceByte = (va_arg(ap, int) & 0xFF);
        valueByte = (va_arg(ap, int) & 0xFF);
        va_end(ap);
      }
      // encode specific forward frame
      rc = frame_encode_ctrl_device_command_spec(pDevCmd,
                                                 instanceByte,
                                                 valueByte,
                                                 FRAME_PRIO_FF_P3,
                                                 &pNewSendFrame->frame);
    }
  }

  if (R_DALILIB_OK == rc)
  {
    pNewSendFrame->lastCmd.eDevCmd = (DEV_CMD) pDevCmd->eDevCmd;
    memcpy(&pNewSendFrame->adr.lastDeviceAdr, pAdr, sizeof(device_adr_t));
    pNewSendFrame->type = SEND_FRAME_TYPE_CTRL_DEV;
    pNewSendFrame->status = FRAME_WITHOUT_RESPONSE;
    if (pDevCmd->answer)
    {
      pNewSendFrame->status = FRAME_WITH_RESPONSE;
    }
  }
  
  return (rc);
}

R_DALILIB_RESULT device_save_mem_block_pers_vars_operating(PDaliLibInstance                   pInst,
                                                           dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT                                  rc = R_DALILIB_OK;
  dali_ctrl_device_pers_variables_operating_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_DEVICE_PERS_VARS_OPERATING_ID;
  block.version = DALI_CTRL_DEVICE_PERS_VARS_OPERATING_VERSION;

  // populate memory block
  block.applicationActive = pDevicePersistent->persVariables.applicationActive;
  block.operatingMode     = pDevicePersistent->persVariables.operatingMode;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_DEVICE_PERS_VARS_OPERATING_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags &= ~DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT device_save_mem_block_pers_vars_config(PDaliLibInstance                   pInst,
                                                        dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT                               rc = R_DALILIB_OK;
  dali_ctrl_device_pers_variables_config_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_DEVICE_PERS_VARS_CONFIG_ID;
  block.version = DALI_CTRL_DEVICE_PERS_VARS_CONFIG_VERSION;

  // populate memory block
  block.shortAddress                 = pDevicePersistent->persVariables.shortAddress;
  block.deviceGroups                 = pDevicePersistent->persVariables.deviceGroups;
  block.randomAddress                = pDevicePersistent->persVariables.randomAddress;
  block.numberOfInstance             = pDevicePersistent->persVariables.numberOfInstance;
  block.applicationControllerPresent = pDevicePersistent->persVariables.applicationControllerPresent;
  block.powerCycleNotification       = pDevicePersistent->persVariables.powerCycleNotification;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_DEVICE_PERS_VARS_CONFIG_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags &= ~DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT device_save_mem_block_pers_inst_vars_operating(PDaliLibInstance                   pInst,
                                                                dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT                                       rc = R_DALILIB_OK;
  dali_ctrl_device_inst_pers_variables_operating_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_DEVICE_PERS_INST_OPERATING_ID;
  block.version = DALI_CTRL_DEVICE_PERS_INST_OPERATING_VERSION;

  // populate memory block
  block.instanceActive = pDevicePersistent->instPersVariables.instanceActive;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_DEVICE_PERS_INST_OPERATING_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags &= ~DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT device_save_mem_block_pers_inst_vars_config(PDaliLibInstance                   pInst,
                                                             dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_device_inst_pers_variables_config_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_DEVICE_PERS_INST_CONFIG_ID;
  block.version = DALI_CTRL_DEVICE_PERS_INST_CONFIG_VERSION;

  // populate memory block
  block.instanceGroup0 = pDevicePersistent->instPersVariables.instanceGroup0;
  block.instanceGroup1 = pDevicePersistent->instPersVariables.instanceGroup1;
  block.instanceGroup2 = pDevicePersistent->instPersVariables.instanceGroup2;
  block.resolution     = pDevicePersistent->instPersVariables.resolution;
  block.instanceNumber = pDevicePersistent->instPersVariables.instanceNumber;
  block.eventFilter    = pDevicePersistent->instPersVariables.eventFilter;
  block.eventScheme    = pDevicePersistent->instPersVariables.eventScheme;
  block.eventPriority  = pDevicePersistent->instPersVariables.eventPriority;
  block.instanceType   = pDevicePersistent->instPersVariables.instanceType;

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_DEVICE_PERS_INST_CONFIG_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags &= ~DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT device_save_mem_blocks(PDaliLibInstance                   pInst,
                                        dali_ctrl_device_persistent_mem_t* pDevicePersistent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (pInst->config.internal.savePersistentModules)
  {
    if (pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags & DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK)
    {
      rc = device_save_mem_block_pers_vars_operating(pInst, pDevicePersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags & DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK))
    {
      rc = device_save_mem_block_pers_vars_config(pInst, pDevicePersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags & DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK))
    {
      rc = device_save_mem_block_pers_inst_vars_operating(pInst, pDevicePersistent);
    }

    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags & DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK))
    {
      rc = device_save_mem_block_pers_inst_vars_config(pInst, pDevicePersistent);
    }

    #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    if (R_DALILIB_OK == rc &&
        (pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags & DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK))
    {
      rc = device_luminaire_save_mem_block_pers_vars(pInst, pDevicePersistent);
    }
    #endif
  }
  else
  {
    // preserve backwards compatibility
    rc = dali_cb_save_mem(pInst, pDevicePersistent, sizeof(*pDevicePersistent));
  }

  return (rc);
}

//----------------------------------------------------------------------
// initialize the default variable of the control device
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_init_persistent(PDaliLibInstance                   pInst,
                                               dali_ctrl_device_persistent_mem_t* pPersistentMemory)
{
  R_DALILIB_RESULT                rc                       = R_DALILIB_OK;
  PCtrlDevPersistentVariables     pPersistentVariables     = &pPersistentMemory->persVariables;
  PCtrlDevInstPersistentVariables pInstPersistentVariables = &pPersistentMemory->instPersVariables;
  PCtrlDevVariables               pVariables               = &pInst->ctrlInst.deviceInstance.deviceVariables;

  memset(pPersistentMemory, 0, sizeof(*pPersistentMemory));
  memset(pInstPersistentVariables, 0, sizeof(*pInstPersistentVariables));

  // device persistent variables
  pPersistentVariables->shortAddress = pInst->config.vendor.shortAddress;
  pPersistentVariables->deviceGroups = 0x00000000;
  pPersistentVariables->randomAddress = 0x00FFFFFF;
  pPersistentVariables->operatingMode = DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE;
  if (CTRL_DEV_MODE_SINGLE == pInst->config.vendor.control_device_mode ||
      CTRL_DEV_MODE_MULTI ==  pInst->config.vendor.control_device_mode )
  {
    pPersistentVariables->numberOfInstance = 0;
    pPersistentVariables->applicationControllerPresent = TRUE;
  }
  else
  {
    if (CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
    {
      // only one instance of the input device supported
      pPersistentVariables->numberOfInstance = 1;
      pPersistentVariables->applicationControllerPresent = FALSE;
    }
  }
  pPersistentVariables->applicationActive      = pPersistentVariables->applicationControllerPresent;
  pPersistentVariables->powerCycleNotification = POWER_CYCLE_NOTIFY_DISABLED;

  // instance persistent variables
  pInstPersistentVariables->instanceGroup0 = DALI_MASK;
  pInstPersistentVariables->instanceGroup1 = DALI_MASK;
  pInstPersistentVariables->instanceGroup2 = DALI_MASK;
  pInstPersistentVariables->instanceActive = TRUE;
  pInstPersistentVariables->instanceType = pInst->config.vendor.control_device_instance_type;

  pInstPersistentVariables->resolution     = pInst->config.vendor.control_device_resolution;
  pInstPersistentVariables->instanceNumber = pInst->config.vendor.control_device_instance_number;
  pInstPersistentVariables->eventFilter    = 0x00FFFFFF;
  pInstPersistentVariables->eventScheme    = 0;
  pInstPersistentVariables->eventPriority  = 4;

  switch(pInstPersistentVariables->instanceType)
  {
    case INPUT_DEVICE_INST_0:
      rc = R_DALILIB_OK;
    break;

    case INPUT_DEVICE_INST_PUSH_BUTTONS:
      dev_pushbtn_init_persistent(pInstPersistentVariables,
                                  pInst->config.vendor.push_btn.pushButtonShortMin,
                                  pInst->config.vendor.push_btn.pushButtonDoubleMin);
      rc = R_DALILIB_OK;
    break;

    case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
      dev_occupancy_init_persistent(pVariables, pInstPersistentVariables);
      rc = R_DALILIB_OK;
    break;

    case INPUT_DEVICE_INST_LIGHT_SENSOR:
      rc = dev_lightsensor_init_persistent(pVariables, pInstPersistentVariables);
    break;

    default:
      rc = R_DALILIB_INPUT_DEVICE_TYPE_NOT_SUPPORTED;
    break;
  }

  // initialize control device extensions
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    dev_luminaire_init_persistent(pInst, pPersistentMemory);
  #endif

  return (rc);
}

//----------------------------------------------------------------------
// parse instance addressing type
// return: R_RESULT
//----------------------------------------------------------------------
static void device_parse_instance_adr(uint8_t       instByte,
                                      device_adr_t* pDevAdr)
{
  pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_RESERVED;
  
  // find instance addressing type
  switch(instByte>>5)
  {
    case 0x0: // instance number addressing
      pDevAdr->instance = instByte;
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_NUMBER;
    break;
    
    case 0x4: // instance group addressing
      pDevAdr->instance = (instByte & 0x1F);
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_GRP;
    break;
    
    case 0x6: // instance type addressing
      pDevAdr->instance = (instByte &0x1F);
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_TYPE;
    break;
    
    case 0x1: // feature on instance number level addressing
      pDevAdr->instance = (instByte & 0x1F);
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_NUMBER_LEVEL;
    break;
    
    case 0x5: // feature on instance group level addressing
      pDevAdr->instance = (instByte & 0x1F);
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_GRP_LEVEL;
    break;
    
    case 0x3: // feature on instance type level addressing
      pDevAdr->instance = (instByte & 0x1F);
      pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_TYPE_LEVEL;
    break;

    default :
      switch(instByte)
      {
        case 0xFD: // feature on instance broadcast level addressing
          pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_BROADCAST_LEVEL;
        break;
        
        case 0xFF: // instance broadcast addressing
          pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_BROADCAST;
        break;
        
        case 0xFC: // feature on device level addressing
          pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_DEVICE_LEVEL;
        break;
        
        case 0xFE: // device addressing
          pDevAdr->instanceAdrType = DALI_INSTANCE_ADRTYPE_DEVICE;
        break;
      }
    break;
  }
}

/**********************************************************************
 * Functions for the received forward frames of
 * the input devices (e.g. Push buttons)
 **********************************************************************/
//----------------------------------------------------------------------
// parse the received event message forward-frame
// return: R_OK: success
//         R_XXX : other error
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_parse_event_message(PCtrlDeviceInstance          pCtrlInst,
                                                   dalilib_frame_t*             pForwardFrame,
                                                   ctrl_device_event_message_t* pEventMessage)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t tmpByte     = 0x00;

  (void)pCtrlInst;

  // find eventSheme
  tmpByte = pForwardFrame->data >> 22;
  if (0x03 == tmpByte) // Bit-23:1, Bit-22:1
  {
    // instance group
    pEventMessage->eventScheme = EVENT_SCHEME_INSTANCE_GROUP;
  }
  else
  {
    if (0x02 == tmpByte) // Bit-23:1, Bit-22:0
    {
      // instance or Device group
      tmpByte = pForwardFrame->data >> 15;
      if (tmpByte & 0x01) // Bit-15:1
      {
        pEventMessage->eventScheme = EVENT_SCHEME_INSTANCE;
      }
      else
      {
        pEventMessage->eventScheme = EVENT_SCHEME_DEVICE_GROUP;
      }
    }
    else
    {
      tmpByte = pForwardFrame->data >> 23;
      if (0x00 == tmpByte) // Bit-23:0
      {
        // Device/instance or Device
        tmpByte = pForwardFrame->data >> 15;
        if (tmpByte & 0x01) // Bit-15:1
        {
          pEventMessage->eventScheme = EVENT_SCHEME_DEVICE_INSTANCE;
        }
        else
        {
          pEventMessage->eventScheme = EVENT_SCHEME_DEVICE;
        }
      }
      else
      {
        rc = R_DALILIB_CTRL_DEVICE_INVALID_EVENT_MESSAGE;
      }
    }
  }

  // parse instance type/number/group
  //      device short address/group
  if (R_DALILIB_OK == rc)
  {
    switch(pEventMessage->eventScheme)
    {
      case EVENT_SCHEME_DEVICE:
        pEventMessage->instanceType = (INPUT_DEVICE_INSTANCE_TYPE)((pForwardFrame->data >> 10) & 0x1F);
        pEventMessage->shortAddress = ((pForwardFrame->data >> 17) & 0x3F);
      break;
      
      case EVENT_SCHEME_DEVICE_INSTANCE:
        pEventMessage->instanceNumber = ((pForwardFrame->data >> 10) & 0x1F);
        pEventMessage->shortAddress = ((pForwardFrame->data >> 17) & 0x3F);
      break;
      
      case EVENT_SCHEME_DEVICE_GROUP:
        pEventMessage->instanceType = (INPUT_DEVICE_INSTANCE_TYPE)((pForwardFrame->data >> 10) & 0x1F);
        pEventMessage->deviceGroup = ((pForwardFrame->data >> 17) & 0x1F);
      break;
      
      case EVENT_SCHEME_INSTANCE:
        pEventMessage->instanceNumber = ((pForwardFrame->data >> 10) & 0x1F);
        pEventMessage->instanceType = (INPUT_DEVICE_INSTANCE_TYPE)((pForwardFrame->data >> 17) & 0x1F);
      break;
      
      case EVENT_SCHEME_INSTANCE_GROUP:
        pEventMessage->instanceType = (INPUT_DEVICE_INSTANCE_TYPE)((pForwardFrame->data >> 10) & 0x1F);
        pEventMessage->instanceGroup = ((pForwardFrame->data >> 17) & 0x1F);
      break;
      
      default:
        rc = R_DALILIB_CTRL_DEVICE_INVALID_EVENT_MESSAGE;
      break;
    }
  }

  return (rc);
}


/**********************************************************************
 * Functions for the received forward frames of
 * the control device (application controller)
 **********************************************************************/
//----------------------------------------------------------------------
// parse the received command forward-frame
// and check if it is for me addressed?
// return: R_OK: success
//         R_IGNORE_FRAME: not for me addressed
//         R_XXX : other error
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_parse_forward_cmd(PCtrlDeviceInstance      pCtrlInst,
                                                 dalilib_frame_t*         pForwardFrame,
                                                 device_adr_t*            pDevAdr,
                                                 ctrl_device_commands_t** pDevCmd)
{
  R_DALILIB_RESULT                rc                       = R_FRAME_IS_NOT_FOR_ME;
  PCtrlDevPersistentVariables     pPersistentVariables     = &pCtrlInst->devicePersistent.persVariables;;
  PCtrlDevInstPersistentVariables pInstPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables;
  uint8_t opcodeByte                                       = pForwardFrame->data;
  uint8_t instByte                                         = (pForwardFrame->data>>8);
  uint8_t specByte                                         = (pForwardFrame->data>>16);
  uint8_t rawAddress                                       = (pForwardFrame->data>>16);

  // check for specific command
  if (rawAddress == 0xC1 || rawAddress == 0xC5 ||
      rawAddress == 0xC7 || rawAddress == 0xC9  )
  {
    pDevAdr->adrType = DALI_ADRTYPE_SPEC;
    rc = R_DALILIB_OK;
  }
  else
  {
    rawAddress >>= 1; // remove event bit

    // check for broadcast
    if (rawAddress == DALI_CTRL_DEVICE_BROADCAST_ADR)
    {
      pDevAdr->adrType = DALI_ADRTYPE_BROADCAST;
      rc = R_DALILIB_OK;
    }
    // check for Nbroadcast
    else
    {
      if (rawAddress == DALI_CTRL_DEVICE_NBROADCAST_ADR)
      {
        pDevAdr->adrType = DALI_ADRTYPE_NBROADCAST;
        rc = R_DALILIB_OK;
      }
      // check for group address
      else
      {
        if (rawAddress & (uint8_t)DALI_CTRL_DEVICE_GROUPADR_MASK_1)
        {
          if ( (uint8_t)0x02 == (rawAddress>>5))
          {
            pDevAdr->adrType = DALI_ADRTYPE_GRP;
            pDevAdr->adr = (rawAddress & (uint8_t)DALI_CTRL_DEVICE_GROUPADR_MASK_2);
            rc = R_DALILIB_OK;
          }
        }
        else
        {
          if ( (rawAddress &  (uint8_t)DALI_CTRL_DEVICE_SHORTADR_MASK) || rawAddress == 0x00 )
          {
            pDevAdr->adr = (rawAddress & (uint8_t)DALI_CTRL_DEVICE_SHORTADR_MASK);
            pDevAdr->adrType = DALI_ADRTYPE_SHORT;
            rc = R_DALILIB_OK;
          }
          else
          {
            rc = R_DALILIB_CTRL_DEVICE_INVALID_ADDRESS;
          }
        }
      }
    }
  }

  // check address, is for me?
  if (R_DALILIB_OK == rc)
  {
    rc = R_FRAME_IS_NOT_FOR_ME;
    switch(pDevAdr->adrType)
    {
      case DALI_ADRTYPE_SHORT:
        if (pDevAdr->adr == pPersistentVariables->shortAddress)
        {
          rc = R_DALILIB_OK;
        }
      break;

      case DALI_ADRTYPE_SPEC:
      case DALI_ADRTYPE_BROADCAST:
        rc = R_DALILIB_OK;
      break;

      case DALI_ADRTYPE_NBROADCAST:
        if (DALI_MASK == pPersistentVariables->shortAddress)
        {
          rc = R_DALILIB_OK;
        }
      break;

      case DALI_ADRTYPE_GRP:
        if (pPersistentVariables->deviceGroups & (0x01 << pDevAdr->adr))
        {
          rc = R_DALILIB_OK;
        }
      break;

      default:
      break;
    }
  }

  // find device command
  if (R_DALILIB_OK == rc)
  {
    if (DALI_ADRTYPE_SPEC == pDevAdr->adrType)
    {
      *pDevCmd = device_get_cmd_by_spec(specByte,instByte);
    }
    else
    {
      *pDevCmd = device_get_cmd_by_opcode(opcodeByte);
    }
  }

  // is a instance command?
  if (R_DALILIB_OK == rc &&
    DALI_ADRTYPE_SPEC != pDevAdr->adrType &&
    DALI_CTRL_DEVICE_FIRST_INSTANCE_CMD_OPCODE <= opcodeByte &&
    DALI_CTRL_DEVICE_LAST_INSTANCE_CMD_OPCODE >= opcodeByte &&
    ((CTRL_DEV_MODE_INPUT == ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode) ||
     (0x8E == opcodeByte) ||
     (0x8F == opcodeByte))) // workaround for test 13.1/preamble
  {
    device_parse_instance_adr(instByte, pDevAdr);
    // check instance address, is for me?
    switch(pDevAdr->instanceAdrType)
    {
      case DALI_INSTANCE_ADRTYPE_NUMBER:
        if (pDevAdr->instance != pInstPersistentVariables->instanceNumber)
        {
          rc = R_FRAME_IS_NOT_FOR_ME;
        }
      break;
      
      case DALI_INSTANCE_ADRTYPE_GRP:
        rc = R_FRAME_IS_NOT_FOR_ME;
        if (pDevAdr->instance == pInstPersistentVariables->instanceGroup0)
        {
          rc = R_DALILIB_OK;
        }
        else if (pDevAdr->instance == pInstPersistentVariables->instanceGroup1)
        {
          rc = R_DALILIB_OK;
        }
        else if (pDevAdr->instance == pInstPersistentVariables->instanceGroup2)
        {
          rc = R_DALILIB_OK;
        }
      break;
      
      case DALI_INSTANCE_ADRTYPE_TYPE:
        if (pDevAdr->instance != pInstPersistentVariables->instanceType)
        {
          rc = R_FRAME_IS_NOT_FOR_ME;
        }
      break;
      
      case DALI_INSTANCE_ADRTYPE_BROADCAST:
        rc = R_DALILIB_OK;
      break;
      
      case DALI_INSTANCE_ADRTYPE_DEVICE:
        rc = R_DALILIB_OK;
      break;

      case DALI_INSTANCE_ADRTYPE_NUMBER_LEVEL:
      case DALI_INSTANCE_ADRTYPE_GRP_LEVEL:
      case DALI_INSTANCE_ADRTYPE_TYPE_LEVEL:
      case DALI_INSTANCE_ADRTYPE_BROADCAST_LEVEL:
      case DALI_INSTANCE_ADRTYPE_DEVICE_LEVEL:
        //not supported
        rc = R_FRAME_IS_NOT_FOR_ME;
      break;
      
      case DALI_INSTANCE_ADRTYPE_RESERVED:
      default:
        rc = R_FRAME_IS_NOT_FOR_ME;
      break;
    }

    if (R_DALILIB_OK == rc)
    {
      *pDevCmd = device_get_cmd_by_instance(opcodeByte);
    }

    if (NULL == *pDevCmd)
    {
      rc = R_EXT_INPUT_DEVICE_FRAME;
      // search instance command on other input devices
      switch(pInstPersistentVariables->instanceType)
      {
        case INPUT_DEVICE_INST_PUSH_BUTTONS:
          *pDevCmd = dev_pushbtn_cmd(opcodeByte);
        break;
        
        case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
          *pDevCmd = dev_occupancy_cmd(opcodeByte);
        break;
        
        case INPUT_DEVICE_INST_LIGHT_SENSOR:
          *pDevCmd = dev_lightsensor_cmd(opcodeByte);
        break;
        
        default:
          rc = R_FRAME_IS_NOT_FOR_ME;
        break;
      }
    }
  }
  
  return (rc);
}

//----------------------------------------------------------------------
// process the command forward initialise frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_initialise(PCtrlDeviceInstance     pCtrlInst,
                                                          ctrl_device_commands_t* pDevCmd,
                                                          uint8_t                 reqValue,
                                                          uint8_t*                pRespValue)
{
  R_DALILIB_RESULT                              rc = R_DALILIB_OK;
  PCtrlDevPersistentVariables pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;
  uint8_t                                    *pAdr;
  
  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_INITIALISE:
      // set initialisationState to enabled, if and only if it is disabled (withdrawn remains unchanged)
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // all control devices
        if (DALI_MASK == reqValue)
        {
          pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_ENABLED;
        }
        // all unaddressed control devices
        else if (reqValue == DALI_CTRL_DEVICE_BROADCAST_ADR &&
             DALI_MASK == pPersistentVariables->shortAddress)
        {
          pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_ENABLED;
        }
        // control device with requested short address
        else if (pPersistentVariables->shortAddress ==  reqValue)
        {
          pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_ENABLED;
        }
      }
      // always prolong initialise timer
      pCtrlInst->deviceInitialiseStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    break;
    
    case DEV_CMD_IDENTIFY_DEVICE:
      if (CTRL_DEVICE_INITIALISE_STATE_ENABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        pCtrlInst->deviceIdentifyDeviceStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        pCtrlInst->deviceIdentifyRunning = TRUE;
      }
    break;
    
    case DEV_CMD_TERMINATE:
      pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_DISABLED;
      pCtrlInst->deviceIdentifyRunning = FALSE;
    break;
    
    case DEV_CMD_RANDOMISE:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      uint32_t randomAddress = device_create_random_address(pCtrlInst);
      if (randomAddress != pPersistentVariables->randomAddress)
      {
        pPersistentVariables->randomAddress = randomAddress;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
    
    case DEV_CMD_COMPARE:
      rc = R_IGNORE_FRAME;
      if (CTRL_DEVICE_INITIALISE_STATE_ENABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        if (pPersistentVariables->randomAddress <= pCtrlInst->deviceVariables.searchAddress)
        {
          *pRespValue = DALI_YES;
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    case DEV_CMD_WITHDRAW:
      pCtrlInst->deviceIdentifyRunning = FALSE;
      rc = R_IGNORE_FRAME;
      if (CTRL_DEVICE_INITIALISE_STATE_ENABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        if (pPersistentVariables->randomAddress == pCtrlInst->deviceVariables.searchAddress)
        {
          pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_WITHDRAWN;
          rc = R_DALILIB_OK;
        }
      }
    break;
    
    case DEV_CMD_SEARCHADDRH:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      pAdr = (uint8_t*)&pCtrlInst->deviceVariables.searchAddress;
      pAdr[2] = reqValue;
    break;
    
    case DEV_CMD_SEARCHADDRM:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      pAdr = (uint8_t*)&pCtrlInst->deviceVariables.searchAddress;
      pAdr[1] = reqValue;
    break;
    
    case DEV_CMD_SEARCHADDRL:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      pAdr = (uint8_t*)&pCtrlInst->deviceVariables.searchAddress;
      pAdr[0] = reqValue;
    break;

    case DEV_CMD_PROGRAM_SHORT_ADDRESS:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      if (pPersistentVariables->randomAddress != pCtrlInst->deviceVariables.searchAddress)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      if (reqValue == DALI_MASK)
      {
        if (DALI_MASK != pPersistentVariables->shortAddress)
        {
          pPersistentVariables->shortAddress = DALI_MASK;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
        break;
      }
      if (  reqValue < 0x40 )
      {
        if (reqValue != pPersistentVariables->shortAddress)
        {
          pPersistentVariables->shortAddress = reqValue;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
     
    case DEV_CMD_VERIFY_SHORT_ADDRESS:
      // from part 103 amd1: data given in 00AAAAAAb format -> dismiss if invalid format
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState ||
          (reqValue & ~DALI_CTRL_DEVICE_SHORTADR_MASK))
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      if (reqValue == pPersistentVariables->shortAddress)
      {
        *pRespValue = DALI_YES;
      }
      else
      {
        // NO
        rc = R_IGNORE_FRAME;
      }
    break;
    
    case DEV_CMD_QUERY_SHORT_ADDRESS:
      if (CTRL_DEVICE_INITIALISE_STATE_DISABLED == pCtrlInst->deviceVariables.initialisationState)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      if (pPersistentVariables->randomAddress != pCtrlInst->deviceVariables.searchAddress)
      {
        // ignore frame
        rc = R_IGNORE_FRAME;
        break;
      }
      *pRespValue = pPersistentVariables->shortAddress;
    break;
    
    case DEV_CMD_RESET_POWER_CYCLE_SEEN:
      pCtrlInst->deviceVariables.powerCycleSeen = FALSE;
    break;
  
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return(rc);
}

//----------------------------------------------------------------------
// process the command forward configuration frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_configuration(PCtrlDeviceInstance     pCtrlInst,
                                                             ctrl_device_commands_t* pDevCmd,
                                                             uint8_t*                pRespValue)
{
  R_DALILIB_RESULT            rc                   = R_DALILIB_OK;
  PCtrlDevPersistentVariables pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;

  (void)pRespValue;

  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_RESET:
      pCtrlInst->deviceResetRunning    = TRUE;
      pCtrlInst->deviceIdentifyRunning = FALSE;
      pCtrlInst->deviceResetStartTime  = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      rc = device_init_by_reset(&pCtrlInst->deviceVariables,
                                &pCtrlInst->devicePersistent.persVariables,
                                &pCtrlInst->devicePersistent.instPersVariables,
                                &pCtrlInst->deviceInstVariables);
      pCtrlInst->deviceResetRunning    = TRUE;
    break;
    
    case DEV_CMD_RESET_MEMORY_BANK:
      pCtrlInst->deviceResetMemoryRunning   = TRUE;
      rc  = device_reset_memory_bank(pCtrlInst, pCtrlInst->deviceVariables.DTR0);
      pCtrlInst->deviceResetMemoryStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    break;
    
    case DEV_CMD_SET_SHORT_ADDRESS:
      if (pCtrlInst->deviceVariables.DTR0 == DALI_MASK)
      {
        if (DALI_MASK != pPersistentVariables->shortAddress)
        {
          pPersistentVariables->shortAddress = DALI_MASK;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      else
      {
        if (pCtrlInst->deviceVariables.DTR0 < 0x40)
        {
          if (pCtrlInst->deviceVariables.DTR0 != pPersistentVariables->shortAddress)
          {
            pPersistentVariables->shortAddress = pCtrlInst->deviceVariables.DTR0;
            pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
            if (0 == pCtrlInst->devicePersistentChangedTime)
            {
              pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }
        }
      }
    break;
    
    case DEV_CMD_ENABLE_WRITE_MEMORY:
      pCtrlInst->deviceVariables.writeEnableState = WRITE_ENABLE_STATE_ENABLED;
    break;

    case DEV_CMD_ENABLE_APPLICATION_CONTROLLER:
      if (pPersistentVariables->applicationControllerPresent)
      {
        if (TRUE != pPersistentVariables->applicationActive)
        {
          pPersistentVariables->applicationActive = TRUE;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_DISABLE_APPLICATION_CONTROLLER:
      if (pPersistentVariables->applicationControllerPresent)
      {
        if (FALSE != pPersistentVariables->applicationActive)
        {
          pPersistentVariables->applicationActive = FALSE;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_OPERATING_MODE:
      // only operating mode 0 supported
      if (DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE == pCtrlInst->deviceVariables.DTR0)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pPersistentVariables->operatingMode)
        {
          pPersistentVariables->operatingMode = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      // else ignore command frame
    break;
    
    case DEV_CMD_START_QUIESCENT_MODE:
      pCtrlInst->deviceVariables.quiescentMode = QUIESCENT_MODE_ENABLED;
      pCtrlInst->deviceQuiescentTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    break;
    
    case DEV_CMD_STOP_QUIESCENT_MODE:
      pCtrlInst->deviceVariables.quiescentMode = QUIESCENT_MODE_DISABLED;
    break;
    
    case DEV_CMD_ENABLE_POWER_CYCLE_NOTIFICATION:
      if (POWER_CYCLE_NOTIFY_ENABLED != pPersistentVariables->powerCycleNotification)
      {
        pPersistentVariables->powerCycleNotification = POWER_CYCLE_NOTIFY_ENABLED;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
    
    case DEV_CMD_DISABLE_POWER_CYCLE_NOTIFICATION:
      if (POWER_CYCLE_NOTIFY_DISABLED != pPersistentVariables->powerCycleNotification)
      {
        pPersistentVariables->powerCycleNotification = POWER_CYCLE_NOTIFY_DISABLED;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
    
    case DEV_CMD_SAVE_PERSISTENT_VARIABLES:
      pCtrlInst->deviceSavePersistentStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      pCtrlInst->deviceSavePersistentRunning   = TRUE;
      pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK |
                                                 DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK |
                                                 DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK |
                                                 DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK |
                                                 DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK;
      rc = device_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->devicePersistent);
      pCtrlInst->deviceSavePersistentRunning   = FALSE;
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return(rc);
}

//----------------------------------------------------------------------
// process the command forward query frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_queries(PCtrlDeviceInstance     pCtrlInst,
                                                       ctrl_device_commands_t* pDevCmd,
                                                       uint8_t*                pRespValue)
{
  R_DALILIB_RESULT rc                              = R_DALILIB_OK;
  PCtrlDevPersistentVariables pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;
  uint8_t status                                   = 0x00;
  uint8_t capabilities                             = 0x00;

  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_QUERY_DEVICE_CAPABILITIES:
      capabilities = pPersistentVariables->numberOfInstance;
      capabilities <<= 1;
      capabilities |= pPersistentVariables->applicationControllerPresent;
      *pRespValue = capabilities;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_DEVICE_STATUS:
      status = pCtrlInst->deviceVariables.resetState;
      status <<= 1;
      status |= pCtrlInst->deviceVariables.powerCycleSeen;
      status <<= 1;
      status |= pCtrlInst->deviceVariables.applicationControllerError;
      status <<= 1;
      status |= pPersistentVariables->applicationActive;
      status <<= 1;
      if (DALI_MASK == pPersistentVariables->shortAddress)
      {
        status |= 0x01;
      }
      status <<= 1;
      if (QUIESCENT_MODE_ENABLED == pCtrlInst->deviceVariables.quiescentMode)
      {
        status |= 0x01;
      }
      status <<= 1;
      status |= pCtrlInst->deviceVariables.inputDeviceError;

      *pRespValue = status;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_APPLICATION_CONTROLLER_ERROR:
      if (pCtrlInst->deviceVariables.applicationControllerError)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_INPUT_DEVICE_ERROR:
      if (pCtrlInst->deviceVariables.inputDeviceError)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_MISSING_SHORT_ADDRESS:
      if (DALI_MASK == pPersistentVariables->shortAddress)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_VERSION_NUMBER:
      *pRespValue     = pCtrlInst->deviceMemBank0.ctrlMemBank0.ver_103_number;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_NUMBER_OF_INSTANCES:
      *pRespValue     = pPersistentVariables->numberOfInstance;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_H:
      *pRespValue     = (pPersistentVariables->randomAddress >> 16 & 0xFF);
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_M:
      *pRespValue     = (pPersistentVariables->randomAddress >> 8 & 0xFF);
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_L:
      *pRespValue     = (pPersistentVariables->randomAddress & 0xFF);
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_APPLICATION_CONTROL_ENABLED:
      if (pPersistentVariables->applicationActive)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_OPERATING_MODE:
      *pRespValue     = pPersistentVariables->operatingMode;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE:
      // only operating mode 0 supported!
      // answer: NO
    break;
    
    case DEV_CMD_QUERY_QUIESCENT_MODE:
      if (QUIESCENT_MODE_ENABLED == pCtrlInst->deviceVariables.quiescentMode)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
      break;
    case DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION:
      if (POWER_CYCLE_NOTIFY_ENABLED == pPersistentVariables->powerCycleNotification)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER:
      // one control device supported
      // answer: NO
    break;
    
    case DEV_CMD_QUERY_RESET_STATE:
      if (pCtrlInst->deviceVariables.resetState)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the forward groups frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_groups(PCtrlDeviceInstance     pCtrlInst,
                                                      ctrl_device_commands_t* pDevCmd,
                                                      uint8_t*                pRespValue)
{
  R_DALILIB_RESULT            rc                   = R_DALILIB_OK;
  PCtrlDevPersistentVariables pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;
  uint8_t                     deviceGroupChanged   = 0;

  switch(pDevCmd->eDevCmd)
  {
    // deviceGroups[15-0] - [DTR2-DTR1]
    case DEV_CMD_ADD_TO_DEVICE_GROUPS_0_15:
    {
      uint16_t newGroupBits = 0x0000;
      newGroupBits = pCtrlInst->deviceVariables.DTR2;
      newGroupBits <<= 8;
      newGroupBits |= pCtrlInst->deviceVariables.DTR1;
      if (newGroupBits != (newGroupBits & pPersistentVariables->deviceGroups))
      {
        deviceGroupChanged = 1;
      }
      pPersistentVariables->deviceGroups |= newGroupBits;
    }
    break;
    
    case DEV_CMD_ADD_TO_DEVICE_GROUPS_16_31:
    {
      uint32_t newGroupBits = 0x00000000;
      newGroupBits = pCtrlInst->deviceVariables.DTR2;
      newGroupBits <<= 8;
      newGroupBits |= pCtrlInst->deviceVariables.DTR1;
      newGroupBits <<= 16;
      if (newGroupBits != (newGroupBits & pPersistentVariables->deviceGroups))
      {
        deviceGroupChanged = 1;
      }
      pPersistentVariables->deviceGroups |= newGroupBits;
    }
    break;
    
    case DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_0_15:
    {
      uint32_t delGroupBits = 0x00000000;
      delGroupBits = pCtrlInst->deviceVariables.DTR2;
      delGroupBits <<= 8;
      delGroupBits |= pCtrlInst->deviceVariables.DTR1;
      if (~delGroupBits != (~delGroupBits & pPersistentVariables->deviceGroups))
      {
        deviceGroupChanged = 1;
      }
      pPersistentVariables->deviceGroups &= ~delGroupBits;
    }
    break;
    
    case DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_16_31:
    {
      uint32_t delGroupBits = 0x00000000;
      delGroupBits = pCtrlInst->deviceVariables.DTR2;
      delGroupBits <<= 8;
      delGroupBits |= pCtrlInst->deviceVariables.DTR1;
      delGroupBits <<= 16;
      if (~delGroupBits != (~delGroupBits & pPersistentVariables->deviceGroups))
      {
        deviceGroupChanged = 1;
      }
      pPersistentVariables->deviceGroups &= ~delGroupBits;
    }
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_0_7:
      *pRespValue = pPersistentVariables->deviceGroups;
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_8_15:
      *pRespValue = (pPersistentVariables->deviceGroups>>8);
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_16_23:
      *pRespValue = (pPersistentVariables->deviceGroups>>16);
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_24_31:
      *pRespValue = (pPersistentVariables->deviceGroups>>24);
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  if (deviceGroupChanged)
  {
    pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
    if (0 == pCtrlInst->devicePersistentChangedTime)
    {
      pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
    }
  }
  return(rc);
}

//----------------------------------------------------------------------
// process the forward DTR-X/Memory bank frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_dtr(PCtrlDeviceInstance     pCtrlInst,
                                                   ctrl_device_commands_t* pDevCmd,
                                                   dalilib_frame_t*        pFF, 
                                                   uint8_t                 reqValue,
                                                   uint8_t*                pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t          nMemBank;
  uint8_t          doIncrement;
  uint8_t          offset;
  
  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_QUERY_CONTENT_DTR0:
      *pRespValue = pCtrlInst->deviceVariables.DTR0;
    break;
    
    case DEV_CMD_QUERY_CONTENT_DTR1:
      *pRespValue = pCtrlInst->deviceVariables.DTR1;
    break;
    
    case DEV_CMD_QUERY_CONTENT_DTR2:
      *pRespValue = pCtrlInst->deviceVariables.DTR2;
    break;
    
    case DEV_CMD_DTR0:
      // opcodeByte = data
      pCtrlInst->deviceVariables.DTR0 = pFF->data;
    break;
    
    case DEV_CMD_DTR1:
      // opcodeByte = data
      pCtrlInst->deviceVariables.DTR1 = pFF->data;
    break;
    
    case DEV_CMD_DTR2:
      // opcodeByte = data
      pCtrlInst->deviceVariables.DTR2 = pFF->data;
    break;
    
    case DEV_CMD_DTR1_DTR0:
      // opcodeByte = data0 = DTR0
      // instanceByte = data1 = DTR1
      pCtrlInst->deviceVariables.DTR0 = pFF->data;
      pCtrlInst->deviceVariables.DTR1 = (pFF->data>>8);
    break;
      
    case DEV_CMD_DTR2_DTR1:
      // opcodeByte = data1 = DTR1
      // instanceByte = data2 = DTR2
      pCtrlInst->deviceVariables.DTR1 = pFF->data;
      pCtrlInst->deviceVariables.DTR2 = (pFF->data>>8);
    break;
    
    case DEV_CMD_READ_MEMORY_LOCATION:   
      // only memory bank 0 and other banks which are initialized at startup 
      nMemBank = pCtrlInst->deviceVariables.DTR1;
      doIncrement = TRUE;
      
      switch(nMemBank)
      {
        case 0:
          if ((pCtrlInst->deviceVariables.DTR0 == 0x01) ||
              (pCtrlInst->deviceVariables.DTR0 > pCtrlInst->deviceMemBank0.memBank0[0]))
          {
            // cell not implemented: ignore frame, increment DTR0
            rc = R_IGNORE_FRAME;
          }
          else
          {
            *pRespValue = pCtrlInst->deviceMemBank0.memBank0[pCtrlInst->deviceVariables.DTR0];
          }
        break;

        case CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
            rc = dev_luminaire_read_memory_bank201(pCtrlInst, pRespValue);
          #else
            // bank not implemented: ignore frame, don't increment DTR0
            rc = R_IGNORE_FRAME;
            doIncrement = FALSE;
          #endif
        break;

        default:
          if ((MAXMEMBANKS <= nMemBank) ||
              (!pCtrlInst->deviceMemBanksOther.is_used[nMemBank]))
          {
            // bank not implemented: ignore frame, don't increment DTR0
            rc = R_IGNORE_FRAME;
            doIncrement = FALSE;
          }
          else
          {
            if (pCtrlInst->deviceVariables.DTR0 > pCtrlInst->deviceMemBanksOther.memBank[nMemBank][0])
            {
              // cell not implemented: ignore frame, increment DTR0
              rc = R_IGNORE_FRAME;
            }
            else
            {
              *pRespValue = pCtrlInst->deviceMemBanksOther.memBank[nMemBank][pCtrlInst->deviceVariables.DTR0];
            }
          }
        break;
      }

      // increment DTR0
      if (doIncrement &&
          pCtrlInst->deviceVariables.DTR0 < 0xFF)
      {
        pCtrlInst->deviceVariables.DTR0 += 1;
      }
    break;
    
    case DEV_CMD_DIRECT_WRITE_MEMORY:
      // store offset in DTR0 and execute DEV_CMD_WRITE_MEMORY_LOCATION
      pCtrlInst->deviceVariables.DTR0 = pFF->data_byte[1];
    // fall through
    case DEV_CMD_WRITE_MEMORY_LOCATION:
    case DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
      nMemBank = pCtrlInst->deviceVariables.DTR1;
      doIncrement = TRUE;
      
      if (WRITE_ENABLE_STATE_ENABLED != pCtrlInst->deviceVariables.writeEnableState)
      {
        // write enable state disabled: ignore frame, don't increment DTR0
        rc = R_IGNORE_FRAME;
        break;
      }

      // check whether desired membank exists
      switch(nMemBank)
      {
        case 0:
          // bank not writable: ignore frame, increment DTR0
          rc = R_IGNORE_FRAME;
        break;

        case CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
            rc = dev_luminaire_write_memory_bank201(pCtrlInst, pDevCmd, reqValue, pRespValue);
          #else
            (void)reqValue;
            // bank not implemented: ignore frame, don't increment DTR0
            rc = R_IGNORE_FRAME;
            doIncrement = FALSE;
          #endif
        break;

        default:
          if ((MAXMEMBANKS <= nMemBank) ||
              (!pCtrlInst->deviceMemBanksOther.is_used[nMemBank]))
          {
            // bank not implemented: ignore frame, don't increment DTR0
            rc = R_IGNORE_FRAME;
            doIncrement = FALSE;
          }
          else
          {
            if (pCtrlInst->deviceVariables.DTR0 > pCtrlInst->deviceMemBanksOther.memBank[nMemBank][0])
            {
              // cell not implemented: ignore frame, increment DTR0
              rc = R_IGNORE_FRAME;
            }
            else
            {
              if (DEV_CMD_DIRECT_WRITE_MEMORY == pDevCmd->eDevCmd)
              {
                pCtrlInst->deviceVariables.DTR0 = pFF->data >> 8;
              }
              offset = pCtrlInst->deviceVariables.DTR0;
              if ( (2 < offset) && (offset <= pCtrlInst->deviceMemBanksOther.memBank[nMemBank][0]) )
              {
                pCtrlInst->deviceMemBanksOther.memBank[nMemBank][offset] = pFF->data;
                *pRespValue = pFF->data;
              }
              else
              {
                // cell not implemented/writable: ignore frame, increment DTR0
                rc = R_IGNORE_FRAME;
              }
            }
          }
        break;
      }

      // increment DTR0
      if (doIncrement &&
          pCtrlInst->deviceVariables.DTR0 < 0xFF)
      {
        pCtrlInst->deviceVariables.DTR0 += 1;
      }
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return(rc);
}


//----------------------------------------------------------------------
// process the instance command forward query frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_instance_queries(PCtrlDeviceInstance     pCtrlInst,
                                                                ctrl_device_commands_t* pDevCmd,
                                                                device_adr_t            receivedAdr,
                                                                uint8_t*                pRespValue)
{
  R_DALILIB_RESULT                rc                       = R_DALILIB_OK;
  PCtrlDevInstPersistentVariables pInstPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables;
  PDaliLibInstance                pInst                    = pCtrlInst->pInst;
  static uint8_t                  inputValuePos            = DALI_CTRL_DEVICE_INPUT_BYTE_MAX - 1;

  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_QUERY_INSTANCE_TYPE:
      if (CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
      {
        *pRespValue = pInstPersistentVariables->instanceType;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_RESOLUTION:
      *pRespValue = pInstPersistentVariables->resolution;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INSTANCE_ERROR:
      if (pCtrlInst->deviceInstVariables.instanceError)
      {
        switch(pInst->config.vendor.control_device_instance_type)
        {
          case INPUT_DEVICE_INST_PUSH_BUTTONS:
            *pRespValue = dev_pushbtn_get_error(pCtrlInst);
          break;
          
          case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
            *pRespValue = dev_occupancy_get_error(pCtrlInst);
          break;
          
          case INPUT_DEVICE_INST_LIGHT_SENSOR:
          /* through */
          default:
            *pRespValue = DALI_YES;
          break;
        }
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_INSTANCE_STATUS:
      {
        uint8_t status = 0x00;
        if (pInstPersistentVariables->instanceActive)
        {
          status |= 0x02;
        }
        if (pCtrlInst->deviceInstVariables.instanceError)
        {
          status |= 0x01;
        }
        *pRespValue = status;
        pDevCmd->answer = TRUE;
      }
      break;
      
    case DEV_CMD_QUERY_EVENT_PRIORITY:
      *pRespValue = pInstPersistentVariables->eventPriority;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INSTANCE_ENABLED:
      if (pInstPersistentVariables->instanceActive)
      {
        *pRespValue     = DALI_YES;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP:
      *pRespValue     = pInstPersistentVariables->instanceGroup0;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INSTANCE_GROUP_1:
      *pRespValue     = pInstPersistentVariables->instanceGroup1;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INSTANCE_GROUP_2:
        *pRespValue     = pInstPersistentVariables->instanceGroup2;
        pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_EVENT_SCHEME:
      *pRespValue     = pInstPersistentVariables->eventScheme;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_EVENT_FILTER_0_7:
      *pRespValue     = pInstPersistentVariables->eventFilter;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_EVENT_FILTER_8_15:
      *pRespValue     = (pInstPersistentVariables->eventFilter>>8);
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_EVENT_FILTER_16_23:
      *pRespValue     = (pInstPersistentVariables->eventFilter>>16);
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INPUT_VALUE:
      inputValuePos   = 0;
      *pRespValue     = pCtrlInst->deviceInstVariables.inputValue_byte[inputValuePos];
      inputValuePos++;
      pDevCmd->answer = TRUE;
    break;
    
    case DEV_CMD_QUERY_INPUT_VALUE_LATCH:
      if (inputValuePos < DALI_CTRL_DEVICE_INPUT_BYTE_MAX && inputValuePos > 1)
      {
        *pRespValue     = pCtrlInst->deviceInstVariables.inputValue_byte[inputValuePos];
        inputValuePos++;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_FEATURE_TYPE:
      if ((CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode) ||
         (DALI_INSTANCE_ADRTYPE_DEVICE == receivedAdr.instanceAdrType))
      {
        // no feature implemented
        *pRespValue     = 0xFE;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case DEV_CMD_QUERY_NEXT_FEATURE_TYPE:
      // only one device/instance supported
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the instance configuration forward frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_instance_configuration(PCtrlDeviceInstance     pCtrlInst,
                                                                      ctrl_device_commands_t* pDevCmd,
                                                                      uint8_t*                pRespValue)
{
  R_DALILIB_RESULT                rc                       = R_DALILIB_OK;
  PCtrlDevInstPersistentVariables pInstPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables;;
  uint32_t                        eventFilter;

  (void)pRespValue;

  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case DEV_CMD_ENABLE_INSTANCE:
      // check for input device - application controller has 0 instances to activate
      if (CTRL_DEV_MODE_INPUT == ((PDaliLibInstance)pCtrlInst->pInst)->config.vendor.control_device_mode)
      {
        if (TRUE != pInstPersistentVariables->instanceActive)
        {
          pInstPersistentVariables->instanceActive = TRUE;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_DISABLE_INSTANCE:
      if (FALSE != pInstPersistentVariables->instanceActive)
      {
        pInstPersistentVariables->instanceActive = FALSE;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
    
    case DEV_CMD_SET_EVENT_PRIORITY:
      if (pCtrlInst->deviceVariables.DTR0 >= 2 &&
          pCtrlInst->deviceVariables.DTR0 <= 5)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->eventPriority)
        {
          pInstPersistentVariables->eventPriority = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_PRIMARY_INSTANCE_GROUP:
      if (pCtrlInst->deviceVariables.DTR0 <  DALI_CTRL_DEVICE_INST_GROUPS_MAX)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup0)
        {
          pInstPersistentVariables->instanceGroup0 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      if (DALI_MASK == pCtrlInst->deviceVariables.DTR0)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup0)
        {
          pInstPersistentVariables->instanceGroup0 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_INSTANCE_GROUP_1:
      if (pCtrlInst->deviceVariables.DTR0 <  DALI_CTRL_DEVICE_INST_GROUPS_MAX)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup1)
        {
          pInstPersistentVariables->instanceGroup1 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      if (DALI_MASK == pCtrlInst->deviceVariables.DTR0)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup1)
        {
          pInstPersistentVariables->instanceGroup1 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_INSTANCE_GROUP_2:
      if (pCtrlInst->deviceVariables.DTR0 <  DALI_CTRL_DEVICE_INST_GROUPS_MAX)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup2)
        {
          pInstPersistentVariables->instanceGroup2 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      if (DALI_MASK == pCtrlInst->deviceVariables.DTR0)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->instanceGroup2)
        {
          pInstPersistentVariables->instanceGroup2 = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_EVENT_SCHEME:
      if (pCtrlInst->deviceVariables.DTR0 <= 4)
      {
        if (pCtrlInst->deviceVariables.DTR0 != pInstPersistentVariables->eventScheme)
        {
          pInstPersistentVariables->eventScheme = pCtrlInst->deviceVariables.DTR0;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;
    
    case DEV_CMD_SET_EVENT_FILTER:
      eventFilter = 0;
      eventFilter = pCtrlInst->deviceVariables.DTR2;
      eventFilter <<= 8;
      eventFilter |= pCtrlInst->deviceVariables.DTR1;
      eventFilter <<= 8;
      eventFilter |= pCtrlInst->deviceVariables.DTR0;
      if (eventFilter != pInstPersistentVariables->eventFilter)
      {
        pInstPersistentVariables->eventFilter = eventFilter;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
static uint8_t device_check_nvm_variables(PCtrlDevPersistentVariables     pPeristensVariables,
                                          PCtrlDevInstPersistentVariables pInstPeristensVariables)
{
  uint8_t rc = FALSE;
  
  if ((0          == pPeristensVariables->deviceGroups) &&
      (0x00FFFFFF == pPeristensVariables->randomAddress) &&
      (0x00       == pInstPeristensVariables->eventScheme) &&
      (DALI_MASK  == pInstPeristensVariables->instanceGroup0) &&
      (DALI_MASK  == pInstPeristensVariables->instanceGroup1) &&
      (DALI_MASK  == pInstPeristensVariables->instanceGroup2))
  {
    rc = TRUE;
  }

  switch(pInstPeristensVariables->instanceType)
  {
    case INPUT_DEVICE_INST_PUSH_BUTTONS:
      rc =(dev_pushbtn_check_nvm_variables(pInstPeristensVariables));
    break;
    
    case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
      rc = (dev_occupancy_check_nvm_variables(pInstPeristensVariables));
    break;
    
    case INPUT_DEVICE_INST_LIGHT_SENSOR:
      rc = (dev_lightsensor_check_nvm_variables(pInstPeristensVariables));
    break;
    
    case INPUT_DEVICE_INST_0:
      if (0x00FFFFFF != pInstPeristensVariables->eventFilter)
      {
        rc = (FALSE);
      }
    break;
    
    default:
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the send testframe command
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_send_testframe(PCtrlDeviceInstance pCtrlInst,
                                                              uint8_t             reqValue)
{
  R_DALILIB_RESULT            rc = R_DALILIB_OK;
  dali_test_frame_request_t*  pRequest = (dali_test_frame_request_t*)&reqValue;
  send_frame_t                newSendFrame;
  ctrl_device_commands_t      devCmd;
  ctrl_gear_commands_t        gearCmd;

  // check execution conditions
  if (0 != pRequest->check   ||
      5 <  pRequest->priority ||
      1 >  pRequest->priority ||
      (1 == pRequest->frameType &&
        FALSE == pCtrlInst->devicePersistent.persVariables.applicationControllerPresent))
  {
    // discard frame
    return (R_DALILIB_OK);
  }

  memset(&newSendFrame, 0, sizeof(newSendFrame));

  if (pRequest->frameType)
  {
    // gear forward frame (16 bit)
    gearCmd.opcodeByte = pCtrlInst->deviceVariables.DTR0;
    gearCmd.twice      = FALSE;
    rc = frame_encode_ctrl_gear_spec(&gearCmd,
                                     pCtrlInst->deviceVariables.DTR1,
                                     pRequest->priority,
                                     &newSendFrame.frame);
  }
  else
  {
    // device forward frame (24 bit)
    devCmd.opcodeByte = pCtrlInst->deviceVariables.DTR0;
    devCmd.twice      = FALSE;
    rc = frame_encode_ctrl_device_command_spec(&devCmd,
                                               pCtrlInst->deviceVariables.DTR1,
                                               pCtrlInst->deviceVariables.DTR2,
                                               pRequest->priority,
                                               &newSendFrame.frame);
  }

  if (R_DALILIB_OK == rc)
  {
    newSendFrame.lastCmd.eDevCmd = (DEV_CMD) DEV_CMD_TESTFRAME;
    newSendFrame.adr.lastDeviceAdr.adrType = DALI_ADRTYPE_RESERVED;
    newSendFrame.type = SEND_FRAME_TYPE_CTRL_DEV;
    newSendFrame.status = FRAME_WITHOUT_RESPONSE;
    newSendFrame.lastActionType = DALILIB_ACT_TYPE_STACK_INTERNAL_CTRL_DEVICE;
    newSendFrame.cmdCounter = pRequest->repetitions;
    rc = dali_cb_send(pCtrlInst->pInst, &newSendFrame);
  }

  if (rc == R_DALILIB_OK)
  {
    rc = R_SKIP_NEXT_SEND_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the received command forward-frame for application controller
// return: R_OK: success
//         R_XXX : other error
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_cmd(PCtrlDeviceInstance  pCtrlInst,
                                                   dalilib_frame_t*     pForwardFrame)
{
  R_DALILIB_RESULT        rc            = R_DALILIB_OK;
  uint8_t                 responseValue = 0x00;
  ctrl_device_commands_t* pDevCmd       = NULL;
  PDaliLibInstance        pInst         = pCtrlInst->pInst;
  device_adr_t            receivedAdr;


  memset(&receivedAdr, 0, sizeof(receivedAdr));
  rc = device_parse_forward_cmd(pCtrlInst, pForwardFrame, &receivedAdr, &pDevCmd);
  if ((R_DALILIB_OK != rc && R_EXT_INPUT_DEVICE_FRAME != rc) || NULL == pDevCmd)
  {
    // reset twice frame wait flag
    pInst->last_rx_frame.bWaitTwice = 0;
    // control device command frame not process
    return (rc);
  }
  
  // check writeEnableState
  if (WRITE_ENABLE_STATE_ENABLED == pCtrlInst->deviceVariables.writeEnableState)
  {
    pCtrlInst->deviceVariables.writeEnableState = device_check_writeEnableState(&pCtrlInst->deviceVariables,
                                                                                (DEV_CMD) pDevCmd->eDevCmd);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_device_check_twice(pInst, pForwardFrame, pDevCmd))
  {
    return (R_DALILIB_OK);
  }
  memset(&pInst->last_rx_frame, 0, sizeof(pInst->last_rx_frame));

  // check resetState
  pCtrlInst->deviceVariables.resetState =
             device_check_nvm_variables(&pCtrlInst->devicePersistent.persVariables,
                                        &pCtrlInst->devicePersistent.instPersVariables);

  if (R_EXT_INPUT_DEVICE_FRAME == rc)
  {
    switch(pCtrlInst->devicePersistent.instPersVariables.instanceType)
    {
      case INPUT_DEVICE_INST_PUSH_BUTTONS:
        rc = dev_pushbtn_process_forward(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
        rc = dev_occupancy_process_forward(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      case INPUT_DEVICE_INST_LIGHT_SENSOR:
        rc = dev_lightsensor_process_forward(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      default:
        // control device forward frame not supported
        rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
      break;
    }
  }
  else
  {
    switch(pDevCmd->eDevCmd)
    {
      // Control device initialization frames
      case DEV_CMD_TERMINATE:
      case DEV_CMD_INITIALISE:
      case DEV_CMD_RANDOMISE:
      case DEV_CMD_COMPARE:
      case DEV_CMD_WITHDRAW:
      case DEV_CMD_SEARCHADDRH:
      case DEV_CMD_SEARCHADDRM:
      case DEV_CMD_SEARCHADDRL:
      case DEV_CMD_PROGRAM_SHORT_ADDRESS:
      case DEV_CMD_VERIFY_SHORT_ADDRESS:
      case DEV_CMD_QUERY_SHORT_ADDRESS:
      case DEV_CMD_IDENTIFY_DEVICE:
      case DEV_CMD_RESET_POWER_CYCLE_SEEN:
        rc = device_process_forward_initialise(pCtrlInst,
                                               pDevCmd,
                                               pForwardFrame->data & DALI_CTRL_DEVICE_FF_VALUE_MASK,
                                               &responseValue);
      break;
      
      // Control device configuration frames
      case DEV_CMD_RESET:
      case DEV_CMD_RESET_MEMORY_BANK:
      case DEV_CMD_SET_SHORT_ADDRESS:
      case DEV_CMD_ENABLE_WRITE_MEMORY:
      case DEV_CMD_ENABLE_APPLICATION_CONTROLLER:
      case DEV_CMD_DISABLE_APPLICATION_CONTROLLER:
      case DEV_CMD_SET_OPERATING_MODE:
      case DEV_CMD_START_QUIESCENT_MODE:
      case DEV_CMD_STOP_QUIESCENT_MODE:
      case DEV_CMD_ENABLE_POWER_CYCLE_NOTIFICATION:
      case DEV_CMD_DISABLE_POWER_CYCLE_NOTIFICATION:
      case DEV_CMD_SAVE_PERSISTENT_VARIABLES:
        rc = device_process_forward_configuration(pCtrlInst,
                                                  pDevCmd,
                                                  &responseValue);
      break;
      
      // Control device groups
      case DEV_CMD_ADD_TO_DEVICE_GROUPS_0_15:
      case DEV_CMD_ADD_TO_DEVICE_GROUPS_16_31:
      case DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_0_15:
      case DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_16_31:
      case DEV_CMD_QUERY_DEVICE_GROUPS_0_7:
      case DEV_CMD_QUERY_DEVICE_GROUPS_8_15:
      case DEV_CMD_QUERY_DEVICE_GROUPS_16_23:
      case DEV_CMD_QUERY_DEVICE_GROUPS_24_31:
        rc = device_process_forward_groups(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      // Control device queries
      case DEV_CMD_QUERY_DEVICE_CAPABILITIES:
      case DEV_CMD_QUERY_DEVICE_STATUS:
      case DEV_CMD_QUERY_APPLICATION_CONTROLLER_ERROR:
      case DEV_CMD_QUERY_INPUT_DEVICE_ERROR:
      case DEV_CMD_QUERY_MISSING_SHORT_ADDRESS:
      case DEV_CMD_QUERY_VERSION_NUMBER:
      case DEV_CMD_QUERY_NUMBER_OF_INSTANCES:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_H:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_M:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_L:
      case DEV_CMD_QUERY_APPLICATION_CONTROL_ENABLED:
      case DEV_CMD_QUERY_OPERATING_MODE:
      case DEV_CMD_QUERY_MANUFACTURER_SPECIFIC_MODE:
      case DEV_CMD_QUERY_QUIESCENT_MODE:
      case DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION:
      case DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER:
      case DEV_CMD_QUERY_RESET_STATE:
        rc = device_process_forward_queries(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      // Control device DTR-X/Memory banks
      case DEV_CMD_QUERY_CONTENT_DTR0:
      case DEV_CMD_QUERY_CONTENT_DTR1:
      case DEV_CMD_QUERY_CONTENT_DTR2:
      case DEV_CMD_DTR0:
      case DEV_CMD_DTR1:
      case DEV_CMD_DTR2:
      case DEV_CMD_DTR1_DTR0:
      case DEV_CMD_DTR2_DTR1:
      case DEV_CMD_READ_MEMORY_LOCATION:
      case DEV_CMD_WRITE_MEMORY_LOCATION:
      case DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY:
      case DEV_CMD_DIRECT_WRITE_MEMORY:
        rc = device_process_forward_dtr(pCtrlInst,
                                        pDevCmd,
                                        pForwardFrame,
                                        pForwardFrame->data & DALI_CTRL_DEVICE_FF_VALUE_MASK,
                                        &responseValue);
      break;
      
      case DEV_CMD_SEND_TESTFRAME:
        rc = device_process_forward_send_testframe(pCtrlInst, pForwardFrame->data & DALI_CTRL_DEVICE_FF_VALUE_MASK);
      break;
      
      // Control device instance configuration frames
      case DEV_CMD_ENABLE_INSTANCE:
      case DEV_CMD_DISABLE_INSTANCE:
      case DEV_CMD_SET_EVENT_PRIORITY:
      case DEV_CMD_SET_PRIMARY_INSTANCE_GROUP:
      case DEV_CMD_SET_INSTANCE_GROUP_1:
      case DEV_CMD_SET_INSTANCE_GROUP_2:
      case DEV_CMD_SET_EVENT_SCHEME:
      case DEV_CMD_SET_EVENT_FILTER:
        rc = device_process_forward_instance_configuration(pCtrlInst, pDevCmd, &responseValue);
      break;
      
      // control device instance querie frames
      case DEV_CMD_QUERY_INSTANCE_TYPE:
      case DEV_CMD_QUERY_RESOLUTION:
      case DEV_CMD_QUERY_INSTANCE_ERROR:
      case DEV_CMD_QUERY_INSTANCE_STATUS:
      case DEV_CMD_QUERY_EVENT_PRIORITY:
      case DEV_CMD_QUERY_INSTANCE_ENABLED:
      case DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP:
      case DEV_CMD_QUERY_INSTANCE_GROUP_1:
      case DEV_CMD_QUERY_INSTANCE_GROUP_2:
      case DEV_CMD_QUERY_EVENT_SCHEME:
      case DEV_CMD_QUERY_EVENT_FILTER_0_7:
      case DEV_CMD_QUERY_EVENT_FILTER_8_15:
      case DEV_CMD_QUERY_EVENT_FILTER_16_23:
      case DEV_CMD_QUERY_INPUT_VALUE:
      case DEV_CMD_QUERY_INPUT_VALUE_LATCH:
      case DEV_CMD_QUERY_FEATURE_TYPE:
      case DEV_CMD_QUERY_NEXT_FEATURE_TYPE:
        rc = device_process_forward_instance_queries(pCtrlInst, pDevCmd, receivedAdr, &responseValue);
      break;
  
      default:
        // control device forward frame not supported
        rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
      break;
    } // switch(pDevCmd->eDevCmd)
  } // else
    
  if (R_IGNORE_FRAME == rc)
  {
    return (R_DALILIB_OK);
  }

  if (R_DALILIB_OK == rc && TRUE == pDevCmd->answer)
  {
    send_frame_t newSendFrame;
    memset(&newSendFrame, 0, sizeof(newSendFrame));
    newSendFrame.frame.priority = FRAME_PRIO_BF_P0;
    newSendFrame.frame.datalength = FRAME_BF_LEN;
    newSendFrame.frame.data = responseValue;
    newSendFrame.type = SEND_FRAME_TYPE_CTRL_DEV;
    dali_cb_send(pInst, &newSendFrame);
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the received event message forward-frame from a input device
// e.g. (Push buttons)
// return: R_OK: success
//         R_XXX : other error
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_forward_event_message(PCtrlDeviceInstance pCtrlInst,
                                                             dalilib_frame_t*    pForwardFrame)
{
  R_DALILIB_RESULT                   rc           = R_DALILIB_OK;
  DALILIB_CTRL_DEVICE_REACTION_CODES reActionCode = DALILIB_REACT_EVENT_END;
  uint16_t                           eventInfo    = 0;
  ctrl_device_event_message_t        eventMessage;

  memset(&eventMessage, 0, sizeof(eventMessage));

  rc = device_parse_event_message(pCtrlInst, pForwardFrame, &eventMessage);
  if (R_DALILIB_OK != rc)
  {
    // event message frame not process
    return (rc);
  }

  switch(eventMessage.eventScheme)
  {
    case EVENT_SCHEME_INSTANCE:
    case EVENT_SCHEME_DEVICE:
    case EVENT_SCHEME_DEVICE_GROUP:
    case EVENT_SCHEME_INSTANCE_GROUP:
      switch(eventMessage.instanceType)
      {
        case INPUT_DEVICE_INST_PUSH_BUTTONS:
          rc = dev_pushbtn_reactioncode(pForwardFrame->data, &reActionCode);
        break;
        
        case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
          rc = dev_occupancy_reactioncode(pForwardFrame->data, &reActionCode);
        break;
        
        case INPUT_DEVICE_INST_LIGHT_SENSOR:
          rc = dev_lightsensor_reactioncode(pForwardFrame->data, &reActionCode);
        break;
        
        default:
          rc = R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
        break;
      }
      eventInfo = (uint16_t)reActionCode;
    break;
    
    case EVENT_SCHEME_DEVICE_INSTANCE:
      eventInfo = pForwardFrame->data & 0x3FF;
      rc = R_DALILIB_OK;
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
    break;
  }
  
  if (R_DALILIB_OK == rc)
  {
    device_event_reaction(pCtrlInst, eventInfo, &eventMessage);
  }
  else
  {
    if(R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN == rc)
    {
      device_event_reaction(pCtrlInst, DALILIB_REACT_EVENT_UNKNOWN, &eventMessage);
    }
  }

  return (rc);
}


/**********************************************************************
 * Functions for the other application controller
 * The functions will be triggered from application
 * e.g. application controller/input device configuration
 **********************************************************************/

//----------------------------------------------------------------------
// control device application controller action functions entry
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_controller_actions(PCtrlDeviceInstance        pCtrlInst,
                                                  dalilib_act_ctrl_device_t* pAct,
                                                  send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->controllerAction)
  {
    //initialise actions
    case DALILIB_ACT_CTRL_DEVICE_INIT_TERMINATE:
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_TERMINATE, pNewSendFrame, &pAct->adr, 0x00);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_INITIALISE:
    {
      uint8_t value = 0;
      switch(pAct->actionValue)
      {
        case DALILIB_CTRL_DEVICE_INIT_WITH_SHORTADDRESS:
          value = pAct->adr.adr;
        break;
        
        case DALILIB_CTRL_DEVICE_INIT_WITHOUT_SHORTADDRESS:
          value = DALI_CTRL_DEVICE_BROADCAST_ADR;
        break;
        
        case DALILIB_CTRL_DEVICE_INIT_ALL:
          value = DALI_CTRL_DEVICE_FF_VALUE_MASK;
        break;
        
        default:
          value = pAct->actionValue;
        break;
      }
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_INITIALISE, pNewSendFrame, &pAct->adr, value);
    }
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_RANDOMISE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_RANDOMISE, pNewSendFrame, &pAct->adr, 0x00);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_WITHDRAW:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_WITHDRAW, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_H:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_SEARCHADDRH, pNewSendFrame, &pAct->adr, pAct->actionValue);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_M:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_SEARCHADDRM, pNewSendFrame, &pAct->adr, pAct->actionValue);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_L:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_SEARCHADDRL, pNewSendFrame, &pAct->adr, pAct->actionValue);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_PROG_SHORT_ADDRESS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_PROGRAM_SHORT_ADDRESS, pNewSendFrame, &pAct->adr, pAct->actionValue);
    break;
    
    // response code for this action DALILIB_REACT_CTRL_DEVICE_INITIALISE
    case DALILIB_ACT_CTRL_DEVICE_INIT_COMPARE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_COMPARE, pNewSendFrame, &pAct->adr, 0x00);
    break;
    
    // response code for this action DALILIB_REACT_CTRL_DEVICE_INITIALISE
    case DALILIB_ACT_CTRL_DEVICE_INIT_VERIFY_SHORT_ADDRESS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_VERIFY_SHORT_ADDRESS, pNewSendFrame, &pAct->adr, pAct->actionValue);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_INIT_QUERY_SHORT_ADDRESS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_SHORT_ADDRESS, pNewSendFrame, &pAct->adr);
    break;
    
    // configuration action codes
    case DALILIB_ACT_CTRL_DEVICE_RESET:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_RESET, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_RESET_MEM_BANK:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_RESET_MEMORY_BANK;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_RESET_POWER_CYCLE_SEEN:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_RESET_POWER_CYCLE_SEEN, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_SET_SHORT_ADDRESS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_SHORT_ADDRESS;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_ENABLE_APPLICATION_CONTROLLER:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_ENABLE_APPLICATION_CONTROLLER, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_DISABLE_APPLICATION_CONTROLLER:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DISABLE_APPLICATION_CONTROLLER, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_SET_OPERATING_MODE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_OPERATING_MODE;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_ADD_TO_DEVICE_GROUPS:
      {
        uint8_t dtr2;
        uint8_t dtr1;
        uint8_t cmd;
      
        pAct->actionValue = (1 << pAct->actionValue);
      
        if (pAct->actionValue <= 32768)
        {
          dtr2 = pAct->actionValue >> 8;
          dtr1 = pAct->actionValue;
          cmd = DEV_CMD_ADD_TO_DEVICE_GROUPS_0_15;
        }
        else
        {
          dtr2 = pAct->actionValue >> 24;
          dtr1 = pAct->actionValue >> 16;
          cmd = DEV_CMD_ADD_TO_DEVICE_GROUPS_16_31;
        }
      
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR2_DTR1, pNewSendFrame, &pAct->adr, dtr2, dtr1);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = cmd;
      }
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_REMOVE_FROM_DEVICE_GROUPS:
      {
        uint8_t dtr2;
        uint8_t dtr1;
        uint8_t cmd;
      
        pAct->actionValue = (1 << pAct->actionValue);
      
        if (pAct->actionValue <= 32768)
        {
          dtr2 = pAct->actionValue >> 8;
          dtr1 = pAct->actionValue;
          cmd = DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_0_15;
        }
        else
        {
          dtr2 = pAct->actionValue >> 24;
          dtr1 = pAct->actionValue >> 16;
          cmd = DEV_CMD_REMOVE_FROM_DEVICE_GROUPS_16_31;
        }
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR2_DTR1, pNewSendFrame, &pAct->adr, dtr2, dtr1);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = cmd;
      }
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_START_QUIESCENT_MODE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_START_QUIESCENT_MODE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_STOP_QUIESCENT_MODE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_STOP_QUIESCENT_MODE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_ENABLE_POWER_CYCLE_NOTIFICATION:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_ENABLE_POWER_CYCLE_NOTIFICATION, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_DISABLE_POWER_CYCLE_NOTIFICATION:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DISABLE_POWER_CYCLE_NOTIFICATION, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_SAVE_PERS_VARIABLES:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_SAVE_PERSISTENT_VARIABLES, pNewSendFrame, &pAct->adr);
    break;
    
    // device queries
    case DALILIB_ACT_CTRL_DEVICE_QUERY_STATUS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_STATUS, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_MISSING_SHORT_ADDRESS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_MISSING_SHORT_ADDRESS, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_QUERY_VERSION_NUMBER:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_VERSION_NUMBER, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_NUMBER_OF_INSTANCES:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_NUMBER_OF_INSTANCES, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR0:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_CONTENT_DTR0, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR1:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_CONTENT_DTR1, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR2:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_CONTENT_DTR2, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_H:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_RANDOM_ADDRESS_H, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_M:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_RANDOM_ADDRESS_M, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_L:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_RANDOM_ADDRESS_L, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_OPERATING_MODE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_OPERATING_MODE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_QUIESCENT_MODE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_QUIESCENT_MODE, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_GROUPS:
      if (pAct->actionValue <= 7)
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_GROUPS_0_7, pNewSendFrame, &pAct->adr);
      }
      else
      {
        if (pAct->actionValue > 7 && pAct->actionValue <= 15)
        {
          rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_GROUPS_8_15, pNewSendFrame, &pAct->adr);
        }
        else
        {
          if (pAct->actionValue > 15 && pAct->actionValue <= 23)
          {
            rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_GROUPS_16_23, pNewSendFrame, &pAct->adr);
          }
          else
          {
            rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_GROUPS_24_31, pNewSendFrame, &pAct->adr);
          }
        }
      }
    break;
      
    case DALILIB_ACT_CTRL_DEVICE_QUERY_POWER_CYCLE_NOTIFICATION:
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_CAPABILITIES:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_DEVICE_CAPABILITIES, pNewSendFrame, &pAct->adr);
    break;
    
    // vendor information action codes (memory bank 0)
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_GTIN:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x03);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MAJOR:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x09);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MINOR:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x0A);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_IDENTIFY_NR:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x0B);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MAJOR:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x13);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MINOR:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x14);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_VENDOR_DALI_NORM_VERSION:
      rc = device_create_ff( pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, 0x17);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_LOCATION;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = 0;
    break;
    
    // memory action
    case DALILIB_ACT_CTRL_DEVICE_MEMORY_READ:
      rc = device_create_ff( pCtrlInst->pInst,
                  NULL,
                  DEV_CMD_DTR1_DTR0,
                  pNewSendFrame,
                  &pAct->adr,
                  pAct->memoryParams.memoryBankNr,
                  pAct->memoryParams.memoryCellIdx);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_READ_MEMORY_CELL;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_MEMORY_READ_NEXT:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_READ_MEMORY_CELL, pNewSendFrame, &pAct->adr);
    break;

    case DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE:
      rc = device_create_ff(pCtrlInst->pInst,
                            NULL,
                            DEV_CMD_ENABLE_WRITE_MEMORY,
                            pNewSendFrame,
                            &pAct->adr);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->memoryParams.value;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR0;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->memoryParams.memoryCellIdx;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR1;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter++] = pAct->memoryParams.memoryBankNr;
    break;
    
    case DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE_NEXT:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_WRITE_MEMORY_LOCATION_NO_REPLY, pNewSendFrame, &pAct->adr, pAct->memoryParams.value);
    break;

    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }
  return (rc);
}

//----------------------------------------------------------------------
// input device action functions entry
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_input_device_actions(PCtrlDeviceInstance        pCtrlInst,
                                                    dalilib_act_ctrl_device_t* pAct,
                                                    send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pAct->inputDeviceAction)
    {
    // instance control instructions
    case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_PRIORITY:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_EVENT_PRIORITY;
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_ENABLE_INSTANCE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_ENABLE_INSTANCE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_DISABLE_INSTANCE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DISABLE_INSTANCE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_SET_PRIMARY_GROUP:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_PRIMARY_INSTANCE_GROUP;
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_SET_GROUP_1:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_INSTANCE_GROUP_1;
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_SET_GROUP_2:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_INSTANCE_GROUP_2;
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_SCHEME:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_EVENT_SCHEME;
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_FILTER:
    {
      uint8_t dtr2 = (pAct->actionValue >> 16);
      uint8_t dtr1 = pAct->actionValue >> 8;
      uint8_t dtr0 = pAct->actionValue;
    
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR2_DTR1, pNewSendFrame, &pAct->adr, dtr2, dtr1);
      pNewSendFrame->cmdCounter = 0;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = DEV_CMD_SET_EVENT_FILTER;
      pNewSendFrame->cmds[pNewSendFrame->cmdCounter] = DEV_CMD_DTR0;
      pNewSendFrame->cmdValues[pNewSendFrame->cmdCounter] = dtr0;
      pNewSendFrame->cmdCounter++;
    }
    break;
    
    // instance queries
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_TYPE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INSTANCE_TYPE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_RESOLUTION:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_RESOLUTION, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_STATUS:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INSTANCE_STATUS, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_PRIORITY:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_EVENT_PRIORITY, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_PRIMARY_GROUP:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_1:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INSTANCE_GROUP_1, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_2:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INSTANCE_GROUP_2, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_SCHEME:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_EVENT_SCHEME, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INPUT_VALUE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE_LATCH:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_INPUT_VALUE_LATCH, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_FEATURE_TYPE:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_FEATURE_TYPE, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_1:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_EVENT_FILTER_0_7, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_2:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_EVENT_FILTER_8_15, pNewSendFrame, &pAct->adr);
    break;
    
    case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_3:
      rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_QUERY_EVENT_FILTER_16_23, pNewSendFrame, &pAct->adr);
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }
  
  return (rc);
}

//----------------------------------------------------------------------
// input device push button action functions entry
// (Push buttons DIN 62386-301)
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_push_button_actions(PCtrlDeviceInstance        pCtrlInst,
                                                   dalilib_act_ctrl_device_t* pAct,
                                                   send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT       rc = R_DALILIB_OK;
  ctrl_device_commands_t devCmd;

  memset(&devCmd, 0, sizeof(devCmd));

  switch(pAct->pushButtonAction)
  {
    // actions for the input devices from application controller
    // Set short timer
    case DALILIB_ACT_PUSH_BUTTON_SET_SHORT_TIMER:
      if (R_DALILIB_OK == dev_pushbtn_opcode(pAct->pushButtonAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = PUSH_BTN_CMD_SET_SHORT_TIMER;
      }
    break;
    
    // Set short double timer
    case DALILIB_ACT_PUSH_BUTTON_SET_DOUBLE_TIMER:
      if (R_DALILIB_OK == dev_pushbtn_opcode(pAct->pushButtonAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = PUSH_BTN_CMD_SET_DOUBLE_TIMER;
      }
    break;
    
    // Set repeat timer
    case DALILIB_ACT_PUSH_BUTTON_SET_REPEAT_TIMER:
      if (R_DALILIB_OK == dev_pushbtn_opcode(pAct->pushButtonAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = PUSH_BTN_CMD_SET_REPEAT_TIMER;
      }
    break;
    
    // Set stuck timer
    case DALILIB_ACT_PUSH_BUTTON_SET_STUCK_TIMER:
      if (R_DALILIB_OK == dev_pushbtn_opcode(pAct->pushButtonAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = PUSH_BTN_CMD_SET_STUCK_TIMER;
      }
    break;
    
    // Query short timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER:
    // Query short timer min
    case DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER_MIN:
    // Query short double timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER:
    // Query short double timer min
    case DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER_MIN:
    // Query repeat timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_REPEAT_TIMER:
    // Query stuck timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_STUCK_TIMER:
      if (R_DALILIB_OK == dev_pushbtn_opcode(pAct->pushButtonAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, &devCmd, DEV_CMD_NONE, pNewSendFrame, &pAct->adr);
      }
    break;
  
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }
  
  return (rc);
}

//----------------------------------------------------------------------
// input device occupancy sensor action functions entry
// (Occupancy sensor DIN 62386-303)
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_occupancy_sensor_actions(PCtrlDeviceInstance        pCtrlInst,
                                                        dalilib_act_ctrl_device_t* pAct,
                                                        send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT       rc = R_DALILIB_OK;
  ctrl_device_commands_t devCmd;

  memset(&devCmd, 0, sizeof(devCmd));

  switch(pAct->occupancySensorAction)
  {
    // actions for the input devices from application controller
    // Set the hold timer (if implemented)
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_HOLD_TIMER:
    // Set the repeat timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_REPORT_TIMER:
    // Set the dead time timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_SET_DEADTIME_TIMER:
      if (R_DALILIB_OK == dev_occupancy_opcode(pAct->occupancySensorAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = devCmd.eDevCmd;
      }
    break;

    // Once a movement detected event has been sent.
    case DALILIB_ACT_OCCUPANCY_SENSOR_CATCH_MOVEMENT:
    // Cancel hold timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_CANCEL_HOLD_TIMER:
    // Query the dead time timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_DEADTIME_TIMER:
    // Query the hold timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_HOLD_TIMER:
    // Query the report timer
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_REPORT_TIMER:
    // Query catching status
    case DALILIB_ACT_OCCUPANCY_SENSOR_QUERY_CATCHING:
      if (R_DALILIB_OK == dev_occupancy_opcode(pAct->occupancySensorAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, &devCmd, DEV_CMD_NONE, pNewSendFrame, &pAct->adr);
      }
    break;

    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// input device light sensor action functions entry
// (Light sensor DIN 62386-304)
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_light_sensor_actions(PCtrlDeviceInstance        pCtrlInst,
                                                    dalilib_act_ctrl_device_t* pAct,
                                                    send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT       rc = R_DALILIB_OK;
  ctrl_device_commands_t devCmd;

  memset(&devCmd, 0, sizeof(devCmd));

  switch(pAct->lightSensorAction)
  {
    // actions for the input devices from application controller
    // Set the report timer
    case DALILIB_ACT_LIGHT_SENSOR_SET_REPORT_TIMER:
    // Set hysteresis
    case DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS:
    // Set the dead time timer
    case DALILIB_ACT_LIGHT_SENSOR_SET_DEADTIME_TIMER:
    // Set hysteresis min
    case DALILIB_ACT_LIGHT_SENSOR_SET_HYSTERESIS_MIN:
      if (R_DALILIB_OK == dev_lightsensor_opcode(pAct->lightSensorAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, NULL, DEV_CMD_DTR0, pNewSendFrame, &pAct->adr, pAct->actionValue);
        pNewSendFrame->cmdCounter = 0;
        pNewSendFrame->cmds[pNewSendFrame->cmdCounter++] = devCmd.eDevCmd;
      }
    break;

    // Query the hysteresis
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS_MIN:
    // Query the dead time timer
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_DEADTIME_TIMER:
    // Query the report timer
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_REPORT_TIMER:
    // Query the hysteresis
    case DALILIB_ACT_LIGHT_SENSOR_QUERY_HYSTERESIS:
      if (R_DALILIB_OK == dev_lightsensor_opcode(pAct->lightSensorAction, &devCmd))
      {
        rc = device_create_ff(pCtrlInst->pInst, &devCmd, DEV_CMD_NONE, pNewSendFrame, &pAct->adr);
      }
    break;

    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// input device internal actions
// actions between application and DALI-Library
// triggered from application
//
// (Push buttons IEC 62386-301)
// (Occupancy Sensor IEC 62386-303)
// (Light Sensor IEC 62386-304)
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_internal_actions(PCtrlDeviceInstance        pCtrlInst,
                                                dalilib_act_ctrl_device_t* pAct)
{
  R_DALILIB_RESULT                  rc = R_DALILIB_OK;
  dali_ctrl_device_persistent_mem_t persistentMemory;
  uint8_t                           daliMem[MEM_DATA_MAX_SIZE]; // buffer of persistent data memory block
  dalilib_action_t                  action;

  switch(pAct->internalAction)
  {
    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_SHORT_ADDRESS:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_SHORT_ADDRESS,
                      pCtrlInst->devicePersistent.persVariables.shortAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_SHORT_ADDRESS:
      if ((uint8_t)pAct->actionValue != pCtrlInst->devicePersistent.persVariables.shortAddress)
      {
        // check value range
        if (0x63       < (uint8_t)pAct->actionValue &&
            DALI_MASK != (uint8_t)pAct->actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->devicePersistent.persVariables.shortAddress = (uint8_t)pAct->actionValue;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DEVICE_GROUPS:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DEVICE_GROUPS,
                      pCtrlInst->devicePersistent.persVariables.deviceGroups);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DEVICE_GROUPS:
      if ((uint16_t)pAct->actionValue != pCtrlInst->devicePersistent.persVariables.deviceGroups)
      {
        pCtrlInst->devicePersistent.persVariables.deviceGroups = (uint16_t)pAct->actionValue;
        pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
        if (0 == pCtrlInst->devicePersistentChangedTime)
        {
          pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_RANDOM_ADDRESS:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_RANDOM_ADDRESS,
                      pCtrlInst->devicePersistent.persVariables.randomAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_RANDOM_ADDRESS:
      if (pAct->actionValue != pCtrlInst->devicePersistent.persVariables.randomAddress)
      {
        // check value range
        if (0x00FFFFFF < pAct->actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->devicePersistent.persVariables.randomAddress = pAct->actionValue;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_OPERATING_MODE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_OPERATING_MODE,
                      pCtrlInst->devicePersistent.persVariables.operatingMode);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_OPERATING_MODE:
      if ((uint8_t)pAct->actionValue != pCtrlInst->devicePersistent.persVariables.operatingMode)
      {
        // check value range
        if (0x00 < (uint8_t)pAct->actionValue &&
            0x80 > (uint8_t)pAct->actionValue)
        {
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        }
        else
        {
          pCtrlInst->devicePersistent.persVariables.operatingMode = (uint8_t)pAct->actionValue;
          pCtrlInst->devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK;
          if (0 == pCtrlInst->devicePersistentChangedTime)
          {
            pCtrlInst->devicePersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_ACTIVE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_ACTIVE,
                      pCtrlInst->devicePersistent.persVariables.applicationActive);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_APPLICATION_ACTIVE:
      pCtrlInst->devicePersistent.persVariables.applicationActive = pAct->actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_NOTIFICATION:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_NOTIFICATION,
                      pCtrlInst->devicePersistent.persVariables.powerCycleNotification);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_POWER_CYCLE_NOTIFICATION:
      pCtrlInst->devicePersistent.persVariables.powerCycleNotification = pAct->actionValue ? POWER_CYCLE_NOTIFY_ENABLED : POWER_CYCLE_NOTIFY_DISABLED;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_SEARCH_ADDRESS:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_SEARCH_ADDRESS,
                      pCtrlInst->deviceVariables.searchAddress);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_SEARCH_ADDRESS:
      // check value range
      if (0x00FFFFFF < pAct->actionValue)
      {
        rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
      }
      else
      {
        pCtrlInst->deviceVariables.searchAddress = pAct->actionValue;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR0:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR0,
                      pCtrlInst->deviceVariables.DTR0);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR0:
      pCtrlInst->deviceVariables.DTR0 = (uint8_t)pAct->actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR1:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR1,
                      pCtrlInst->deviceVariables.DTR1);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR1:
      pCtrlInst->deviceVariables.DTR1 = (uint8_t)pAct->actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DTR2:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_DTR2,
                      pCtrlInst->deviceVariables.DTR2);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_DTR2:
      pCtrlInst->deviceVariables.DTR2 = (uint8_t)pAct->actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_QUIESCENT_MODE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_QUIESCENT_MODE,
                      pCtrlInst->deviceVariables.quiescentMode);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_QUIESCENT_MODE:
      if (QUIESCENT_MODE_DISABLED == pAct->actionValue)
      {
        pCtrlInst->deviceVariables.quiescentMode = QUIESCENT_MODE_DISABLED;
      }
      else
      {
        pCtrlInst->deviceVariables.quiescentMode = QUIESCENT_MODE_ENABLED;
        pCtrlInst->deviceQuiescentTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_WRITE_ENABLE_STATE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_WRITE_ENABLE_STATE,
                      pCtrlInst->deviceVariables.writeEnableState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_WRITE_ENABLE_STATE:
      pCtrlInst->deviceVariables.writeEnableState = pAct->actionValue ? WRITE_ENABLE_STATE_ENABLED : WRITE_ENABLE_STATE_DISABLED;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_SEEN:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_POWER_CYCLE_SEEN,
                      pCtrlInst->deviceVariables.powerCycleSeen);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_POWER_CYCLE_SEEN:
      pCtrlInst->deviceVariables.powerCycleSeen = pAct->actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_INITIALISATION_STATE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_INITIALISATION_STATE,
                      pCtrlInst->deviceVariables.initialisationState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_INITIALISATION_STATE:
      switch (pAct->actionValue)
      {
        case CTRL_DEVICE_INITIALISE_STATE_DISABLED:
          pCtrlInst->deviceVariables.initialisationState = pAct->actionValue;
          // stop the initialisation state
          pCtrlInst->deviceInitialiseStartTime = 0;
        break;

        case CTRL_DEVICE_INITIALISE_STATE_ENABLED:
          pCtrlInst->deviceVariables.initialisationState = pAct->actionValue;
          // start or prolong the initialisation state
          pCtrlInst->deviceInitialiseStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        break;

        case CTRL_DEVICE_INITIALISE_STATE_WITHDRAWN:
          pCtrlInst->deviceVariables.initialisationState = pAct->actionValue;
        break;

        default:
          rc = R_DALILIB_VARIABLE_OUT_OF_BOUNDS;
        break;
      }
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_CONTROLLER_ERROR:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_APPLICATION_CONTROLLER_ERROR,
                      pCtrlInst->deviceVariables.applicationControllerError);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_APPLICATION_CONTROLLER_ERROR:
      pCtrlInst->deviceVariables.applicationControllerError = pAct->actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_INPUT_DEVICE_ERROR:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_INPUT_DEVICE_ERROR,
                      pCtrlInst->deviceVariables.inputDeviceError);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_INPUT_DEVICE_ERROR:
      pCtrlInst->deviceVariables.inputDeviceError = pAct->actionValue ? TRUE : FALSE;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_RESET_STATE:
      // notify application
      device_reaction(pCtrlInst,
                      DALILIB_REACT_INTERNAL_CTRL_DEVICE_GET_RESET_STATE,
                      pCtrlInst->deviceVariables.resetState);
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_SET_RESET_STATE:
      pCtrlInst->deviceVariables.resetState = pAct->actionValue ? TRUE : FALSE;
    break;

    // action codes for input device (Push Button)
    case DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_SHORT_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables.tShort = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_DOUBLE_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables.tDouble = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_REPEAT_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables.tRepeat = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_PUSH_BUTTON_SET_STUCK_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables.tStuck = pAct->actionValue;
    break;
  
    // action codes for input device (Light Sensor)
    case DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_REPORT_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables.tReport = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_HYSTERESIS:
      pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables.tHysteresis = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_DEADTIME:
      pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables.tDeadtime = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_LIGHT_SENSOR_SET_HYSTERESIS_MIN:
      pCtrlInst->devicePersistent.instPersVariables.lightSensorPersVariables.tHysteresisMin = pAct->actionValue;
    break;
  
      // action codes for input device (Occupancy Sensor)
    case DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_HOLD_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables.tHold = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_REPORT_TIMER:
      pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables.tReport = pAct->actionValue;
    break;
    
    case DALILIB_ACT_INTERNAL_OCCUPANCY_SENSOR_SET_DEADTIME:
      pCtrlInst->devicePersistent.instPersVariables.occupancySensorPersVariables.tDeadtime = pAct->actionValue;
    break;

    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_WRITE_MEMORY_CELL:
      switch(pAct->memoryParams.memoryBankNr)
      {
        case 0:
           // Membank 0 is readonly
          rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
        break;

        case CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER:
          #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
            if (R_IGNORE_FRAME == dev_luminaire_write_memory_bank201_direct(pCtrlInst,
                                                                            pAct->memoryParams.memoryCellIdx,
                                                                            pAct->memoryParams.value))
            {
              rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
            }
          #else
            rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
          #endif
        break;

        default:
          // application already has pointer to user defined memory banks
          rc = R_DALILIB_CTRL_GEAR_ACTION_INVALID_PARAMS;
        break;
      }
    break;
  
    case DALILIB_ACT_INTERNAL_CTRL_DEVICE_GET_DEFAULT_PERS_MEMORY:
      // get default values of persistent memory
      rc = device_init_persistent(pCtrlInst->pInst, &persistentMemory);
      // store in buffer
      if (R_DALILIB_OK == rc)
      {
        rc = dali_create_mem(pCtrlInst->pInst, &persistentMemory, sizeof(persistentMemory), daliMem);
      }
      // notify application
      if (R_DALILIB_OK == rc)
      {
        memset(&action, 0, sizeof(action));
        action.actionType = DALILIB_CTRL_DEVICE_REACTION;
        action.deviceReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_DEVICE_DEFAULT_PERS_MEMORY;
        action.deviceReact.persMemory.pMemory = &daliMem;
        action.deviceReact.persMemory.nSize = MEM_DATA_MAX_SIZE;
        action.deviceReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
        rc = dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    break;

    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  if (R_DALILIB_OK == rc)
  {
    rc = R_INTERN_ACTION;
  }
  
  return (rc);
}


//----------------------------------------------------------------------
// initialize the memory bank 0 of the control device
//----------------------------------------------------------------------
static void device_init_membank0_data(PDaliLibInstance pInst)
{
  PCtrlDevMemoryBank0 pDeviceMemBank0 = &pInst->ctrlInst.deviceInstance.deviceMemBank0;;

  memset(pDeviceMemBank0, 0, sizeof(pInst->ctrlInst.deviceInstance.deviceMemBank0));

  // memory bank 0
  pDeviceMemBank0->ctrlMemBank0.address_last_cell = 0x1A;
  pDeviceMemBank0->ctrlMemBank0.reserved          = 0x00;
  pDeviceMemBank0->ctrlMemBank0.number_last_bank  = 0;

  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    pDeviceMemBank0->ctrlMemBank0.number_last_bank  = CTRL_DEVICE_LUMINAIRE_MEMORY_BANK_NUMBER;
  #endif

  memcpy(pDeviceMemBank0->ctrlMemBank0.gtin,
         pInst->config.vendor.gtin,
         sizeof(pDeviceMemBank0->ctrlMemBank0.gtin));

  pDeviceMemBank0->ctrlMemBank0.firmware_ver_major = pInst->config.vendor.firmware_ver_major;
  pDeviceMemBank0->ctrlMemBank0.firmware_ver_minor = pInst->config.vendor.firmware_ver_minor;

  memcpy(pDeviceMemBank0->ctrlMemBank0.identify_number,
         pInst->config.vendor.identify_number,
         sizeof(pDeviceMemBank0->ctrlMemBank0.identify_number));

  pDeviceMemBank0->ctrlMemBank0.hw_ver_major     = pInst->config.vendor.hw_ver_major;
  pDeviceMemBank0->ctrlMemBank0.hw_ver_minor     = pInst->config.vendor.hw_ver_minor;

  pDeviceMemBank0->ctrlMemBank0.ver_101_number   = 0x08;
  pDeviceMemBank0->ctrlMemBank0.ver_102_number   = 0x08;
  pDeviceMemBank0->ctrlMemBank0.ver_103_number   = 0x08;

  pDeviceMemBank0->ctrlMemBank0.number_log_ctrl_device = 0x01;
  pDeviceMemBank0->ctrlMemBank0.number_log_ctrl_gear   = 0x00;

  pDeviceMemBank0->ctrlMemBank0.index_log_control_unit = 0x00;
}


/**********************************************************************
 * Backward Frame Functions
 **********************************************************************/

//----------------------------------------------------------------------
// process the response status frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_status(uint8_t           value,
                                                       dalilib_action_t* pAct)  
{
  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;

  if (value & 0x01)
  {
    pAct->deviceReact.deviceStatus.inputDeviceError = TRUE;
  }
  if (value & 0x02)
  {
    pAct->deviceReact.deviceStatus.quiescentMode = TRUE;
  }
  if (value & 0x04)
  {
    pAct->deviceReact.deviceStatus.shortAddress = TRUE;
  }
  if (value & 0x08)
  {
    pAct->deviceReact.deviceStatus.applicationActive = TRUE;
  }
  if (value & 0x10)
  {
    pAct->deviceReact.deviceStatus.applicationCtrlErr = TRUE;
  }
  if (value & 0x20)
  {
    pAct->deviceReact.deviceStatus.powerCycleSeen = TRUE;
  }
  if (value & 0x40)
  {
    pAct->deviceReact.deviceStatus.resetState = TRUE;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response init frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_initialise(DEV_CMD           eDevCmd,
                                                           uint8_t           value,
                                                           dalilib_action_t* pAct)
{
  (void)eDevCmd;

  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_INITIALISE;
  pAct->deviceReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
  pAct->deviceReact.reactValue = value;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response capabilities frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_capabilities(uint8_t           value,
                                                             dalilib_action_t* pAct)
{
  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_CAPABILITIES;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;

  if (value & 0x01)
  {
    pAct->deviceReact.deviceCapabilities.applicationCtrlPresent = TRUE;
  }
  if (value & 0x02)
  {
    pAct->deviceReact.deviceCapabilities.numberOfInstance = TRUE;
  }
  
  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response instance status frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_instancestatus(uint8_t           value,
                                                               dalilib_action_t* pAct)
{
  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_INSTANCE_STATUS;
  pAct->deviceReact.valueType = DALILIB_RESPONSE_VALUE_VALID;

  if (value & 0x01)
  {
    pAct->deviceReact.deviceInstanceStatus.instanceError = TRUE;
  }
  if (value & 0x02)
  {
    pAct->deviceReact.deviceInstanceStatus.instanceActive = TRUE;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response queries frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_queries(DEV_CMD           eDevCmd,
                                                        uint8_t           value,
                                                        dalilib_action_t* pAct)
{
  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_QUERIES;
  pAct->deviceReact.valueType = DALILIB_RESPONSE_VALUE_VALID;

  switch(eDevCmd)
  {
    case DEV_CMD_QUERY_DEVICE_GROUPS_8_15:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 8;
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_16_23:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 16;
    break;
    
    case DEV_CMD_QUERY_DEVICE_GROUPS_24_31:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 24;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_H:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 16;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_M:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 8;
    break;
    
    case DEV_CMD_QUERY_EVENT_FILTER_8_15:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 8;
    break;
    
    case DEV_CMD_QUERY_EVENT_FILTER_16_23:
      pAct->deviceReact.reactValue = value;
      pAct->deviceReact.reactValue <<= 16;
    break;
    
    case DEV_CMD_QUERY_RANDOM_ADDRESS_L:
    case DEV_CMD_QUERY_MISSING_SHORT_ADDRESS:
    case DEV_CMD_QUERY_VERSION_NUMBER:
    case DEV_CMD_QUERY_NUMBER_OF_INSTANCES:
    case DEV_CMD_QUERY_CONTENT_DTR0:
    case DEV_CMD_QUERY_CONTENT_DTR1:
    case DEV_CMD_QUERY_CONTENT_DTR2:
    case DEV_CMD_QUERY_OPERATING_MODE:
    case DEV_CMD_QUERY_QUIESCENT_MODE:
    case DEV_CMD_QUERY_DEVICE_GROUPS_0_7:
    case DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION:
    case DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER:
    case DEV_CMD_QUERY_INSTANCE_TYPE:
    case DEV_CMD_QUERY_RESOLUTION:
    case DEV_CMD_QUERY_EVENT_PRIORITY:
    case DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP:
    case DEV_CMD_QUERY_INSTANCE_GROUP_1:
    case DEV_CMD_QUERY_INSTANCE_GROUP_2:
    case DEV_CMD_QUERY_EVENT_SCHEME:
    case DEV_CMD_QUERY_INPUT_VALUE:
    case DEV_CMD_QUERY_INPUT_VALUE_LATCH:
    case DEV_CMD_QUERY_FEATURE_TYPE:
    case DEV_CMD_QUERY_NEXT_FEATURE_TYPE:
    case DEV_CMD_QUERY_EVENT_FILTER_0_7:
    default:
      pAct->deviceReact.reactValue = value;
    break;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response vendor frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_vendor(uint8_t           value,
                                                       dalilib_action_t* pAct,
                                                       send_frame_t*     pLastTxFrame)
{
  static uint8_t cellCntr = 0;
  static uint8_t cellInfo[8];

  if (0 == cellCntr)
  {
    memset(&cellInfo, 0, sizeof(cellInfo));
  }
  cellInfo[cellCntr++] = value;

  if (pLastTxFrame->cmdCounter <= 0 &&
      pLastTxFrame->lastActionType == DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER &&
      ( pLastTxFrame->lastAction >=  DALILIB_ACT_CTRL_DEVICE_VENDOR_GTIN ||
        pLastTxFrame->lastAction <=  DALILIB_ACT_CTRL_DEVICE_VENDOR_DALI_NORM_VERSION))
  {
    cellCntr = 0;
    pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_VENDOR;
    pAct->deviceReact.valueType  = DALILIB_RESPONSE_VALUE_VALID;
    switch(pLastTxFrame->lastAction)
    {
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_GTIN:
        memcpy(&pAct->deviceReact.gtin, &cellInfo, sizeof(pAct->deviceReact.gtin));
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MAJOR:
        pAct->deviceReact.firmware_ver_major = value;
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MINOR:
        pAct->deviceReact.firmware_ver_minor = value;
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_IDENTIFY_NR:
        memcpy(&pAct->deviceReact.identify_number, &cellInfo, sizeof(pAct->deviceReact.identify_number));
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MAJOR:
        pAct->deviceReact.hardware_ver_major = value;
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MINOR:
        pAct->deviceReact.hardware_ver_minor = value;
      break;
      
      case DALILIB_ACT_CTRL_DEVICE_VENDOR_DALI_NORM_VERSION:
        pAct->deviceReact.dali_norm_version = value;
      break;
      
      default:
      break;
    }
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response memory frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_memory(uint8_t           value,
                                                       dalilib_action_t* pAct)
{
  pAct->deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_MEMORY;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->deviceReact.reactValue   = value;

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// process the response push button frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_push_button(PDaliLibInstance  pInst,
                                                            PUSH_BTN_CMD      ePushBtnCmd,
                                                            uint8_t           responseValue,
                                                            dalilib_action_t* pAct)
{
  (void)pInst;

  if (ePushBtnCmd < PUSH_BTN_CMD_QUERY_SHORT_TIMER ||
      ePushBtnCmd >= PUSH_BTN_CMD_LAST)
  {
    return (R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED);
  }

  pAct->deviceReact.reactionCode = DALILIB_REACT_PUSH_BUTTON;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->deviceReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response occupancy sensor frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_occupancy_sensor(PDaliLibInstance     pInst,
                                                                 OCCUPANCY_SENSOR_CMD eOccupancySensorCmd,
                                                                 uint8_t              responseValue,
                                                                 dalilib_action_t*    pAct)
{
  (void)pInst;

  if (eOccupancySensorCmd < OCCUPANCY_CMD_QUERY_DEADTIME_TIMER ||
      eOccupancySensorCmd >= OCCUPANCY_CMD_LAST)
  {
    return (R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED);
  }

  pAct->deviceReact.reactionCode = DALILIB_REACT_OCCUPANCY_SENSOR;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->deviceReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response light sensor frames
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_light_sensor(PDaliLibInstance  pInst,
                                                             LIGHT_SENSOR_CMD  eLightSensorCmd,
                                                             uint8_t           responseValue,
                                                             dalilib_action_t* pAct)
{
  (void)pInst;

  if (eLightSensorCmd < LIGHT_CMD_QUERY_HYSTERESIS_MIN ||
      eLightSensorCmd >= LIGHT_CMD_LAST)
  {
    return (R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED);
  }

  pAct->deviceReact.reactionCode = DALILIB_REACT_LIGHT_SENSOR;
  pAct->deviceReact.valueType    = DALILIB_RESPONSE_VALUE_VALID;
  pAct->deviceReact.reactValue   = responseValue;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// process the response base dali frame
//----------------------------------------------------------------------
static R_DALILIB_RESULT device_process_response_base(PDaliLibInstance  pInst,
                                                     DEV_CMD           eDevCmd,
                                                     uint8_t           responseValue,
                                                     dalilib_action_t* pAction)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (eDevCmd >= DEV_CMD_LAST)
  {
    rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    // control device response frame not supported
    return (rc);
  }

  if (R_DALILIB_OK == rc)
  {
    switch(eDevCmd)
    {
      case DEV_CMD_QUERY_DEVICE_STATUS:
        //case DEV_CMD_QUERY_INPUT_DEVICE_ERROR:
        //case DEV_CMD_QUERY_QUIESCENT_MODE:
        //case DEV_CMD_QUERY_APPLICATION_CONTROL_ENABLED:
        //case DEV_CMD_QUERY_APPLICATION_CONTROLLER_ERROR:
        //case DEV_CMD_QUERY_RESET_STATE:
        {
          rc = device_process_response_status(responseValue, pAction);
        }
      break;
      
      case DEV_CMD_QUERY_DEVICE_CAPABILITIES:
        rc = device_process_response_capabilities(responseValue, pAction);
      break;
    
      case DEV_CMD_QUERY_INSTANCE_STATUS:
        //case DEV_CMD_QUERY_INSTANCE_ERROR:
        //case DEV_CMD_QUERY_INSTANCE_ENABLED:
        rc = device_process_response_instancestatus(responseValue, pAction);
      break;
    
      case DEV_CMD_QUERY_MISSING_SHORT_ADDRESS:
      case DEV_CMD_QUERY_VERSION_NUMBER:
      case DEV_CMD_QUERY_NUMBER_OF_INSTANCES:
      case DEV_CMD_QUERY_CONTENT_DTR0:
      case DEV_CMD_QUERY_CONTENT_DTR1:
      case DEV_CMD_QUERY_CONTENT_DTR2:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_H:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_M:
      case DEV_CMD_QUERY_RANDOM_ADDRESS_L:
      case DEV_CMD_QUERY_OPERATING_MODE:
      case DEV_CMD_QUERY_QUIESCENT_MODE:
      case DEV_CMD_QUERY_DEVICE_GROUPS_0_7:
      case DEV_CMD_QUERY_DEVICE_GROUPS_8_15:
      case DEV_CMD_QUERY_DEVICE_GROUPS_16_23:
      case DEV_CMD_QUERY_DEVICE_GROUPS_24_31:
      case DEV_CMD_QUERY_POWER_CYCLE_NOTIFICATION:
      case DEV_CMD_QUERY_EXTENDED_VERSION_NUMBER:
      case DEV_CMD_QUERY_INSTANCE_TYPE:
      case DEV_CMD_QUERY_RESOLUTION:
      case DEV_CMD_QUERY_EVENT_PRIORITY:
      case DEV_CMD_QUERY_PRIMARY_INSTANCE_GROUP:
      case DEV_CMD_QUERY_INSTANCE_GROUP_1:
      case DEV_CMD_QUERY_INSTANCE_GROUP_2:
      case DEV_CMD_QUERY_EVENT_SCHEME:
      case DEV_CMD_QUERY_INPUT_VALUE:
      case DEV_CMD_QUERY_INPUT_VALUE_LATCH:
      case DEV_CMD_QUERY_FEATURE_TYPE:
      case DEV_CMD_QUERY_NEXT_FEATURE_TYPE:
      case DEV_CMD_QUERY_EVENT_FILTER_0_7:
      case DEV_CMD_QUERY_EVENT_FILTER_8_15:
      case DEV_CMD_QUERY_EVENT_FILTER_16_23:
        rc = device_process_response_queries(eDevCmd, responseValue, pAction);
      break;
    
      case DEV_CMD_COMPARE:
      case DEV_CMD_VERIFY_SHORT_ADDRESS:
      case DEV_CMD_QUERY_SHORT_ADDRESS:
        rc = device_process_response_initialise(eDevCmd, responseValue, pAction);
      break;
      
      case DEV_CMD_READ_MEMORY_LOCATION:
        rc = device_process_response_vendor(responseValue, pAction, &pInst->last_tx_frame);
      break;
      
      case DEV_CMD_READ_MEMORY_CELL:
        rc = device_process_response_memory(responseValue, pAction);
      break;
      
      // request for this response not found
      default:
        rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
      break;
    }
  }

  return (rc);
}

R_DALILIB_RESULT device_load_mem_blocks(PDaliLibInstance                    pInst,
                                            dali_ctrl_device_persistent_mem_t*  pDevicePersistent)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  if (pInst->config.internal.savePersistentModules)
  {
    // load general device persistent memory
    rc = dali_cb_load_mem_block(pInst, DALI_CTRL_DEVICE_PERS_VARS_CONFIG_ID); // load before operating vars
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_DEVICE_PERS_VARS_OPERATING_ID); // might need config vars
    }

    // load device instance persistent memory
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_DEVICE_PERS_INST_OPERATING_ID);
    }
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_DEVICE_PERS_INST_CONFIG_ID);
    }

    // load D4i extensions persistent memory
    #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    if (R_DALILIB_OK == rc)
    {
      rc = dali_cb_load_mem_block(pInst, DALI_CTRL_DEVICE_PERS_LUMINAIRE_ID);
    }
    #endif
  }
  else
  {
    // preserve backwards compatibility
    rc = dali_cb_load_mem(pInst, pDevicePersistent);
  }

  return (rc);
}

R_DALILIB_RESULT device_apply_mem_block_pers_vars_operating(PCtrlDeviceInstance pCtrlInst,
                                                            uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_device_pers_variables_t*                 pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;
  dali_ctrl_device_pers_variables_operating_block_t* pBlock = (dali_ctrl_device_pers_variables_operating_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->operatingMode     = pBlock->operatingMode;
  pPersistentVariables->applicationActive = pBlock->applicationActive;

  // check value range
  if (0x00 < pPersistentVariables->operatingMode &&
      0x80 > pPersistentVariables->operatingMode)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable operatingMode is out of bounds. Value: %u Range: [0x00, 0x80..0xFF]",
             pPersistentVariables->operatingMode);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->operatingMode = DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE;
  }
  if (0x01 < pPersistentVariables->applicationActive)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable applicationActive is out of bounds. Value: %u Range: [0, 1]",
             pPersistentVariables->applicationActive);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->applicationActive = pPersistentVariables->applicationControllerPresent;
  }

  return (rc);
}

R_DALILIB_RESULT device_apply_mem_block_pers_vars_config(PCtrlDeviceInstance pCtrlInst,
                                                         uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                                   rc = R_DALILIB_OK;
  char                                               buf[128];
  dali_ctrl_device_pers_variables_t*                 pPersistentVariables = &pCtrlInst->devicePersistent.persVariables;
  PDaliLibInstance                                   pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_device_pers_variables_config_block_t*    pBlock = (dali_ctrl_device_pers_variables_config_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->shortAddress                 = pBlock->shortAddress;
  pPersistentVariables->deviceGroups                 = pBlock->deviceGroups;
  pPersistentVariables->randomAddress                = pBlock->randomAddress;
  pPersistentVariables->numberOfInstance             = pBlock->numberOfInstance;
  pPersistentVariables->applicationControllerPresent = pBlock->applicationControllerPresent;
  pPersistentVariables->powerCycleNotification       = pBlock->powerCycleNotification;

  // check value range
  if (0x63       < pPersistentVariables->shortAddress &&
      DALI_MASK != pPersistentVariables->shortAddress)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable shortAddress is out of bounds. Value: %u Range: [0x00..0x63, 0xFF]",
             pPersistentVariables->shortAddress);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->shortAddress = pInst->config.vendor.shortAddress;
  }
  if (0x00FFFFFF < pPersistentVariables->randomAddress)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable randomAddress is out of bounds. Value: %lu Range: [0x00..0x00FFFFFF]",
             pPersistentVariables->randomAddress);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->randomAddress = 0x00FFFFFF;
  }
  if (0x32 < pPersistentVariables->numberOfInstance)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable numberOfInstance is out of bounds. Value: %u Range: [0..32]",
             pPersistentVariables->numberOfInstance);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    if (CTRL_DEV_MODE_SINGLE == pInst->config.vendor.control_device_mode ||
        CTRL_DEV_MODE_MULTI ==  pInst->config.vendor.control_device_mode )
    {
      pPersistentVariables->numberOfInstance = 0;
      pPersistentVariables->applicationControllerPresent = TRUE;
    }
    else
    {
      if (CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
      {
        // only one instance of the input device supported
        pPersistentVariables->numberOfInstance = 1;
        pPersistentVariables->applicationControllerPresent = FALSE;
      }
    }
  }
  if (0x01 < pPersistentVariables->applicationControllerPresent)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable applicationControllerPresent is out of bounds. Value: %u Range: [0, 1]",
             pPersistentVariables->applicationControllerPresent);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->applicationControllerPresent = !pPersistentVariables->numberOfInstance;
  }
  if (0x01 < pPersistentVariables->powerCycleNotification)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable powerCycleNotification is out of bounds. Value: %u Range: [0, 1]",
             pPersistentVariables->powerCycleNotification);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->powerCycleNotification = DALI_CTRL_DEVICE_NORMAL_OPERTING_MODE;
  }

  return (rc);
}

R_DALILIB_RESULT device_apply_mem_block_pers_inst_vars_operating(PCtrlDeviceInstance pCtrlInst,
                                                                 uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                                        rc = R_DALILIB_OK;
  char                                                    buf[128];
  dali_ctrl_device_inst_pers_variables_t*                 pPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables;
  dali_ctrl_device_inst_pers_variables_operating_block_t* pBlock = (dali_ctrl_device_inst_pers_variables_operating_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->instanceActive = pBlock->instanceActive;

  // check value range
  if (0x01 < pPersistentVariables->instanceActive)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceActive is out of bounds. Value: %u Range: [0, 1]",
             pPersistentVariables->instanceActive);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceActive = TRUE;
  }

  return (rc);
}

R_DALILIB_RESULT device_apply_mem_block_pers_inst_vars_config(PCtrlDeviceInstance pCtrlInst,
                                                              uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                                     rc = R_DALILIB_OK;
  char                                                 buf[128];
  dali_ctrl_device_inst_pers_variables_t*              pPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables;
  PDaliLibInstance                                     pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_device_inst_pers_variables_config_block_t* pBlock = (dali_ctrl_device_inst_pers_variables_config_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->instanceGroup0 = pBlock->instanceGroup0;
  pPersistentVariables->instanceGroup1 = pBlock->instanceGroup1;
  pPersistentVariables->instanceGroup2 = pBlock->instanceGroup2;
  pPersistentVariables->resolution     = pBlock->resolution;
  pPersistentVariables->instanceNumber = pBlock->instanceNumber;
  pPersistentVariables->eventFilter    = pBlock->eventFilter;
  pPersistentVariables->eventScheme    = pBlock->eventScheme;
  pPersistentVariables->eventPriority  = pBlock->eventPriority;
  pPersistentVariables->instanceType   = pBlock->instanceType;

  // check value range
  if (31         < pPersistentVariables->instanceGroup0 &&
      DALI_MASK != pPersistentVariables->instanceGroup0)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceGroup0 is out of bounds. Value: %u Range: [0..31, 255]",
             pPersistentVariables->instanceGroup0);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceGroup0 = DALI_MASK;
  }
  if (31         < pPersistentVariables->instanceGroup1 &&
      DALI_MASK != pPersistentVariables->instanceGroup1)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceGroup1 is out of bounds. Value: %u Range: [0..31, 255]",
             pPersistentVariables->instanceGroup1);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceGroup1 = DALI_MASK;
  }
  if (31         < pPersistentVariables->instanceGroup2 &&
      DALI_MASK != pPersistentVariables->instanceGroup2)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceGroup2 is out of bounds. Value: %u Range: [0..31, 255]",
             pPersistentVariables->instanceGroup2);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceGroup2 = DALI_MASK;
  }
  if (pCtrlInst->devicePersistent.persVariables.numberOfInstance <= pPersistentVariables->instanceNumber)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceNumber is out of bounds. Value: %u Range: [0..(numberOfInstances - 1)]",
             pPersistentVariables->instanceNumber);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceNumber = pInst->config.vendor.control_device_instance_number;
  }
  if (0x00FFFFFF < pPersistentVariables->eventFilter)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable eventFilter is out of bounds. Value: %lu Range: [0x00..0x00FFFFFF]",
             pPersistentVariables->eventFilter);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->eventFilter = 0x00FFFFFF;
  }
  if (0x04 < pPersistentVariables->eventScheme)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable eventScheme is out of bounds. Value: %u Range: [0..4]",
             pPersistentVariables->eventScheme);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->eventScheme = 0;
  }
  if (5 < pPersistentVariables->eventPriority ||
      2 > pPersistentVariables->eventPriority)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable eventPriority is out of bounds. Value: %u Range: [2..5]",
             pPersistentVariables->eventPriority);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->eventPriority = 4;
  }
  if (31 < pPersistentVariables->instanceType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable instanceType is out of bounds. Value: %u Range: [0..31]",
             pPersistentVariables->instanceType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->instanceType = pInst->config.vendor.control_device_instance_type;
  }

  return (rc);
}


/**********************************************************************
 * Public Functions
 **********************************************************************/

R_DALILIB_RESULT ctrl_device_apply_mem_block(PCtrlDeviceInstance pCtrlInst,
                                             uint8_t*            pDaliMem)
{
  R_DALILIB_RESULT                    rc = R_DALILIB_OK;
  uint8_t                             blockId   = pDaliMem[0];
  uint8_t                             version   = pDaliMem[1];

  switch(blockId)
  {
    case DALI_CTRL_DEVICE_PERS_VARS_OPERATING_ID:
      if (version == 1)
      {
        rc = device_apply_mem_block_pers_vars_operating(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_DEVICE_PERS_VARS_CONFIG_ID:
      if (version == 1)
      {
        rc = device_apply_mem_block_pers_vars_config(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    case DALI_CTRL_DEVICE_PERS_INST_OPERATING_ID:
      if (version == 1)
      {
        rc = device_apply_mem_block_pers_inst_vars_operating(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    case DALI_CTRL_DEVICE_PERS_INST_CONFIG_ID:
      if (version == 1)
      {
        rc = device_apply_mem_block_pers_inst_vars_config(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;

    #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    case DALI_CTRL_DEVICE_PERS_LUMINAIRE_ID:
      if (version == 1)
      {
        rc = device_luminaire_apply_mem_block_pers_vars(pCtrlInst, pDaliMem);
      }
      else
      {
        rc = R_MEM_NOT_DEFINED;
      }
    break;
    #endif

    default:
      rc = R_MEM_NOT_DEFINED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// System failure triggered by HAL-Driver over status frame.
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_set_systemfailure(PCtrlDeviceInstance pCtrlInst, uint8_t bFailure)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  pCtrlInst->deviceSystemFailure = bFailure;

  return (rc);
}

//----------------------------------------------------------------------
// will be called from any input device functions for a timer event
// create a event message with instance type
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_input_device_create_event(PDaliLibInstance pInst,
                                                send_frame_t*    pNewSendFrame,
                                                device_adr_t*    pAdr,
                                                uint16_t*        pEventInfo)
{
  R_DALILIB_RESULT    rc         = R_DALILIB_OK;
  PCtrlDeviceInstance pCtrlInst  = &pInst->ctrlInst.deviceInstance;
  uint32_t            eventFrame = 0;
  
  switch(pCtrlInst->devicePersistent.instPersVariables.eventScheme)
  {
    case EVENT_SCHEME_INSTANCE:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x00; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits instance type
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceNumber;
    break;
    
    case EVENT_SCHEME_DEVICE:
      // 22. - 17.bits device short address
      eventFrame = pCtrlInst->devicePersistent.persVariables.shortAddress;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x00; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance types
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    case EVENT_SCHEME_DEVICE_INSTANCE:
      // 22. - 17.bits device short address
      eventFrame = pCtrlInst->devicePersistent.persVariables.shortAddress;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceNumber;
    break;
    
    case EVENT_SCHEME_DEVICE_GROUP:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x00; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits device group
      eventFrame |= pAdr->adr;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x00; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance types
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    case EVENT_SCHEME_INSTANCE_GROUP:
      eventFrame = 0x01; // 23. Bit
      eventFrame <<=1;
      eventFrame |= 0x01; // 22. Bit
      eventFrame <<=5;
      // 21. - 17.bits instance groups
      eventFrame |= pAdr->instance;
      eventFrame <<=1;
      eventFrame |= 0x00; // 16.bit always 0
      eventFrame <<=1;
      eventFrame |= 0x01; // 15.bit
      eventFrame <<=5;
      // 14. - 10.bits instance numbers
      eventFrame |= pCtrlInst->devicePersistent.instPersVariables.instanceType;
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_INVALID_SCHEME;
    break;
  }

  // insert eventInfo data
  eventFrame <<= 10;
  eventFrame |= *pEventInfo;

  //create ff frame
  pNewSendFrame->frame.priority   = pCtrlInst->devicePersistent.instPersVariables.eventPriority;
  pNewSendFrame->frame.datalength = FRAME_CTRL_DEV_FF_LEN;
  pNewSendFrame->frame.data       = eventFrame;
  pNewSendFrame->status           = FRAME_WITHOUT_RESPONSE;
  pNewSendFrame->type             = SEND_FRAME_TYPE_CTRL_EVENT;

  return (rc);
}

R_DALILIB_RESULT ctrl_device_check_twice(PDaliLibInstance        pInst,
                                              dalilib_frame_t*        pForwardFrame,
                                              ctrl_device_commands_t* pDevCmd)
{
  // is twice-Frame
  if (pDevCmd->twice)
  {
    // first twice frame
    if (0 == pInst->last_rx_frame.bWaitTwice)
    {
      // wait next frame
      pInst->last_rx_frame.bWaitTwice = 1;
      memcpy(&pInst->last_rx_frame.frame, pForwardFrame, sizeof(dalilib_frame_t));
      return (R_IGNORE_FRAME);
    }
    // check if valid second twice frame within settling time
    if (pInst->last_rx_frame.frame.datalength == pForwardFrame->datalength &&
        pInst->last_rx_frame.frame.data == pForwardFrame->data         &&
        pForwardFrame->priority <= FRAME_PRIO_FF_P6 &&
        dali_difftime(pForwardFrame->timestampStartOfRecv, pInst->last_rx_frame.frame.timestamp) <= DALI_SEND_TWICE_TIMEOUT)
    {
      // twice frames success received
    }
    else
    {
      // new first twice frame, wait next frame
      pInst->last_rx_frame.bWaitTwice = 1;
      memcpy(&pInst->last_rx_frame.frame, pForwardFrame, sizeof(dalilib_frame_t));
      return (R_IGNORE_FRAME);
    }
  }
  pInst->last_rx_frame.bWaitTwice = 0;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// will be called, if cmdCounter >= 0
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_send_more(PDaliLibInstance pInst,
                                       send_frame_t*    pNewSendFrame)
{
  R_DALILIB_RESULT       rc = R_DALILIB_OK;
  ctrl_device_commands_t devCmd;

  switch(pInst->last_tx_frame.lastActionType)
  {
    case DALILIB_ACT_TYPE_PUSH_BUTTON:
      {
        memset(&devCmd, 0, sizeof(devCmd));
        if (R_DALILIB_OK == dev_pushbtn_opcode((DALILIB_ACT_PUSH_BUTTON)pInst->last_tx_frame.lastAction, &devCmd))
        {
          rc = device_create_ff(pInst,
                                &devCmd,
                                DEV_CMD_NONE,
                                pNewSendFrame,
                                &pInst->last_tx_frame.adr.lastDeviceAdr,
                                pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
      }
    break;

    case DALILIB_ACT_TYPE_OCCUPANCY_SENSOR:
      {
        memset(&devCmd, 0, sizeof(devCmd));
        if (R_DALILIB_OK == dev_occupancy_opcode((DALILIB_ACT_OCCUPANCY_SENSOR)pInst->last_tx_frame.lastAction, &devCmd))
        {
          rc = device_create_ff(pInst,
                                &devCmd,
                                DEV_CMD_NONE,
                                pNewSendFrame,
                                &pInst->last_tx_frame.adr.lastDeviceAdr,
                                pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
      }
    break;

    case DALILIB_ACT_TYPE_LIGHT_SENSOR:
      {
        memset(&devCmd, 0, sizeof(devCmd));
        if (R_DALILIB_OK == dev_lightsensor_opcode((DALILIB_ACT_LIGHT_SENSOR)pInst->last_tx_frame.lastAction, &devCmd))
        {
          rc = device_create_ff(pInst,
                                &devCmd,
                                DEV_CMD_NONE,
                                pNewSendFrame,
                                &pInst->last_tx_frame.adr.lastDeviceAdr,
                                pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
        }
      }
    break;
    
    case DALILIB_ACT_TYPE_STACK_INTERNAL_CTRL_DEVICE:
      // stack initiated action sending test frame
      if (0 != pInst->ctrlInst.deviceInstance.deviceTestframeTime)
      {
        // last testframe is still in the process of being sent
        // we received another frame that triggered this function
        rc = R_IGNORE_FRAME;
      }
    break;

    default:
      rc = device_create_ff(pInst,
                            NULL,
                            pInst->last_tx_frame.cmds[pInst->last_tx_frame.cmdCounter-1],
                            pNewSendFrame,
                            &pInst->last_tx_frame.adr.lastDeviceAdr,
                            pInst->last_tx_frame.cmdValues[pInst->last_tx_frame.cmdCounter-1]);
    break;
  }
  
  return (rc);
}

//----------------------------------------------------------------------
// will be called, if send forward frame failed
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_process_send_failed(PDaliLibInstance            pInst,
                                                 send_frame_t*               pFrame,
                                                 DALILIB_RESPONSE_VALUE_TYPE valueType)
{
R_DALILIB_RESULT       rc = R_DALILIB_OK;

  switch(pFrame->lastActionType)
  {
    case DALILIB_ACT_TYPE_STACK_INTERNAL_CTRL_DEVICE:
      // retry sending testframe in timinHelper
      // cmdCounter wasn't reduced in dali_send_next_frame() so we do it here
      if (pInst->last_tx_frame.cmdCounter > 0)
      {
        pInst->last_tx_frame.cmdCounter--;
        pInst->ctrlInst.deviceInstance.deviceTestframeTime = pInst->currentTime;
        rc = R_SEND_AGAIN;
      }
    break;

    default:
      rc = ctrl_device_process_response_timeout(pInst, pFrame, valueType);
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// frame time out
// - no answer
// - control device address does not exists on bus
// - forward frame not sent
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_process_response_timeout(PDaliLibInstance            pInst,
                                                      send_frame_t*               pFrame,
                                                      DALILIB_RESPONSE_VALUE_TYPE valueType)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  dalilib_action_t action;

  memset( &action, 0, sizeof(action));
  action.actionType            = DALILIB_CTRL_DEVICE_REACTION;
  action.deviceReact.valueType = valueType;

  switch(pFrame->lastActionType)
  {
    case DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER:
      switch(pFrame->lastAction)
      {
        // reaction code for next actions DALILIB_REACT_CTRL_DEVICE_INITIALISE
        case DALILIB_ACT_CTRL_DEVICE_INIT_TERMINATE:
        case DALILIB_ACT_CTRL_DEVICE_INIT_INITIALISE:
        case DALILIB_ACT_CTRL_DEVICE_INIT_RANDOMISE:
        case DALILIB_ACT_CTRL_DEVICE_INIT_WITHDRAW:
        case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_H:
        case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_M:
        case DALILIB_ACT_CTRL_DEVICE_INIT_SEARCHADDR_L:
        case DALILIB_ACT_CTRL_DEVICE_INIT_PROG_SHORT_ADDRESS:
        case DALILIB_ACT_CTRL_DEVICE_INIT_COMPARE:
        case DALILIB_ACT_CTRL_DEVICE_INIT_VERIFY_SHORT_ADDRESS:
        case DALILIB_ACT_CTRL_DEVICE_INIT_QUERY_SHORT_ADDRESS:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_INITIALISE;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_CONFIG
        case DALILIB_ACT_CTRL_DEVICE_RESET:
        case DALILIB_ACT_CTRL_DEVICE_RESET_MEM_BANK:
        case DALILIB_ACT_CTRL_DEVICE_RESET_POWER_CYCLE_SEEN:
        case DALILIB_ACT_CTRL_DEVICE_SET_SHORT_ADDRESS:
        case DALILIB_ACT_CTRL_DEVICE_ENABLE_APPLICATION_CONTROLLER:
        case DALILIB_ACT_CTRL_DEVICE_DISABLE_APPLICATION_CONTROLLER:
        case DALILIB_ACT_CTRL_DEVICE_SET_OPERATING_MODE:
        case DALILIB_ACT_CTRL_DEVICE_ADD_TO_DEVICE_GROUPS:
        case DALILIB_ACT_CTRL_DEVICE_REMOVE_FROM_DEVICE_GROUPS:
        case DALILIB_ACT_CTRL_DEVICE_START_QUIESCENT_MODE:
        case DALILIB_ACT_CTRL_DEVICE_STOP_QUIESCENT_MODE:
        case DALILIB_ACT_CTRL_DEVICE_ENABLE_POWER_CYCLE_NOTIFICATION:
        case DALILIB_ACT_CTRL_DEVICE_DISABLE_POWER_CYCLE_NOTIFICATION:
        case DALILIB_ACT_CTRL_DEVICE_SAVE_PERS_VARIABLES:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_CONFIG;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS;
        case DALILIB_ACT_CTRL_DEVICE_QUERY_STATUS:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_QUERY_STATUS;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERIES
        case DALILIB_ACT_CTRL_DEVICE_QUERY_MISSING_SHORT_ADDRESS:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_VERSION_NUMBER:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_NUMBER_OF_INSTANCES:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR0:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR1:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_CONTENT_DTR2:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_H:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_M:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_RANDOM_L:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_OPERATING_MODE:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_QUIESCENT_MODE:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_GROUPS:
        case DALILIB_ACT_CTRL_DEVICE_QUERY_POWER_CYCLE_NOTIFICATION:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_QUERIES;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_CAPABILITIES
        case DALILIB_ACT_CTRL_DEVICE_QUERY_DEVICE_CAPABILITIES:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_CAPABILITIES;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_VENDOR
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_GTIN:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MAJOR:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_FW_VER_MINOR:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_IDENTIFY_NR:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MAJOR:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_HW_VER_MINOR:
        case DALILIB_ACT_CTRL_DEVICE_VENDOR_DALI_NORM_VERSION:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_VENDOR;
        break;

        // reaction code: DALILIB_REACT_CTRL_DEVICE_MEMORY
        case DALILIB_ACT_CTRL_DEVICE_MEMORY_READ:
        case DALILIB_ACT_CTRL_DEVICE_MEMORY_READ_NEXT:
        case DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE:
        case DALILIB_ACT_CTRL_DEVICE_MEMORY_WRITE_NEXT:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_MEMORY;
        break;
        
        default:
          rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
        break;
      }
    break;
    
    case DALILIB_ACT_TYPE_INPUT_DEVICE:
      switch(pFrame->lastAction)
      {
        // instance control instructions
        // reaction code: DALILIB_REACT_CTRL_DEVICE_CONFIG
        case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_PRIORITY:
        case DALILIB_ACT_INPUT_DEVICE_ENABLE_INSTANCE:
        case DALILIB_ACT_INPUT_DEVICE_DISABLE_INSTANCE:
        case DALILIB_ACT_INPUT_DEVICE_SET_PRIMARY_GROUP:
        case DALILIB_ACT_INPUT_DEVICE_SET_GROUP_1:
        case DALILIB_ACT_INPUT_DEVICE_SET_GROUP_2:
        case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_SCHEME:
        case DALILIB_ACT_INPUT_DEVICE_SET_EVENT_FILTER:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_CONFIG;
        break;
        
        // instance queries
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_STATUS:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_INSTANCE_STATUS;
        break;
        
        // reaction code: DALILIB_REACT_CTRL_DEVICE_QUERIES
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_TYPE:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_RESOLUTION:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_PRIORITY:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_PRIMARY_GROUP:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_1:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INSTANCE_GROUP_2:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_SCHEME:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_INPUT_VALUE_LATCH:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_FEATURE_TYPE:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_1:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_2:
        case DALILIB_ACT_INPUT_DEVICE_QUERY_EVENT_FILTER_3:
          action.deviceReact.reactionCode = DALILIB_REACT_CTRL_DEVICE_QUERIES;
        break;
        
        default:
          rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
        break;
      }
    break;
    
    case DALILIB_ACT_TYPE_PUSH_BUTTON:
      action.deviceReact.reactionCode = DALILIB_REACT_PUSH_BUTTON;
    break;

    case DALILIB_ACT_TYPE_OCCUPANCY_SENSOR:
      action.deviceReact.reactionCode = DALILIB_REACT_OCCUPANCY_SENSOR;
    break;

    case DALILIB_ACT_TYPE_LIGHT_SENSOR:
      action.deviceReact.reactionCode = DALILIB_REACT_LIGHT_SENSOR;
    break;

    case DALILIB_ACT_TYPE_STACK_INTERNAL_CTRL_DEVICE:
      if (DALILIB_RESPONSE_VALUE_REQUEST_NOT_SENT == valueType)
      {
        // retry sending testframe in timinHelper
        pInst->ctrlInst.deviceInstance.deviceTestframeTime = pInst->currentTime;
        rc = R_SEND_AGAIN;
      }
      else
      {
        // finished sending testframes
        pInst->ctrlInst.deviceInstance.deviceTestframeTime = 0;
        if (pInst->last_tx_frame.cmdCounter > 0)
        {
          rc = R_SEND_AGAIN;
        }
      }
    break;

    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  // notify the application
  // transmit the response values
  if (R_DALILIB_OK == rc)
  {
    dali_cb_reaction(pInst, &action);
  }

  return (rc);
}

//----------------------------------------------------------------------
// process the response dali frame
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_process_response_frame(PDaliLibInstance pInst,
                                                    dalilib_frame_t* pResponseFrame)
{  
  R_DALILIB_RESULT     rc      = R_DALILIB_OK;
  DEV_CMD              eDevCmd = DEV_CMD_LAST;
  uint8_t              value;
  dalilib_action_t     action;

  memset( &action, 0, sizeof(action));
  action.actionType = DALILIB_CTRL_DEVICE_REACTION;

  eDevCmd = (DEV_CMD)pInst->last_tx_frame.lastCmd.eDevCmd;
  value = (pResponseFrame->data & DALI_RESPONSE_VALUE_MASK);

  switch(pInst->last_tx_frame.lastActionType)
  {
    // response for push button forward frames
    case DALILIB_ACT_TYPE_PUSH_BUTTON:
      rc = device_process_response_push_button(pInst, (PUSH_BTN_CMD)eDevCmd, value, &action);
    break;
    // response for occupancy sensor forward frames
    case DALILIB_ACT_TYPE_OCCUPANCY_SENSOR:
      rc = device_process_response_occupancy_sensor(pInst, (OCCUPANCY_SENSOR_CMD)eDevCmd, value, &action);
    break;
    // response for light sensor forward frames
    case DALILIB_ACT_TYPE_LIGHT_SENSOR:
      rc = device_process_response_light_sensor(pInst, (LIGHT_SENSOR_CMD)eDevCmd, value, &action);
    break;

    // response for base control device forward frames
    case DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER:
    case DALILIB_ACT_TYPE_INPUT_DEVICE:
      rc = device_process_response_base(pInst, eDevCmd, value, &action);
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  // notify the application
  // transmit the response values
  if (R_DALILIB_OK == rc)
  {
    dali_cb_reaction(pInst, &action);
  }

  return (rc);
}

//----------------------------------------------------------------------
// public function
// process the received forward dali frame
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_process_forward_frame(PCtrlDeviceInstance pCtrlInst,
                                                   dalilib_frame_t*    pForwardFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  // check, forward frame type
  // command or instance command frame?
  // if 16. bit == 1 command frame,
  //        == 0 event message frame
  if ((pForwardFrame->data>>16) & 0x01)
  {
    // process command forward frame
    rc = device_process_forward_cmd(pCtrlInst, pForwardFrame);
  }
  else
  {
    // process event message forward frame
    pCtrlInst->deviceVariables.writeEnableState = WRITE_ENABLE_STATE_DISABLED;
    rc = device_process_forward_event_message(pCtrlInst, pForwardFrame);
  }
  return (rc);
}


//----------------------------------------------------------------------
// control device action functions entry
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_action(PCtrlDeviceInstance        pCtrlInst,
                                    dalilib_act_ctrl_device_t* pAct,
                                    send_frame_t*              pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  
  if (DALILIB_ACT_TYPE_INTERNAL_CTRL_DEVICE != pAct->deviceActionType)
  {
    if (FALSE == pCtrlInst->devicePersistent.persVariables.applicationActive)
    {
      return (R_DALILIB_APPLICATION_CONTROLLER_DISABLED);
    }
    if (QUIESCENT_MODE_ENABLED == pCtrlInst->deviceVariables.quiescentMode)
    {
      return (R_DALILIB_CTRL_DEVICE_QUIESCENT_MODE_ENABLED);
    }
  }
  
  switch(pAct->deviceActionType)
  {
    case DALILIB_ACT_TYPE_INTERNAL_CTRL_DEVICE:
      rc = device_internal_actions(pCtrlInst, pAct);
    break;
    
    case DALILIB_ACT_TYPE_CTRL_DEVICE_CONTROLLER:
      rc = device_controller_actions(pCtrlInst, pAct, pNewSendFrame);
    break;
    
    case DALILIB_ACT_TYPE_INPUT_DEVICE:
      rc = device_input_device_actions(pCtrlInst, pAct, pNewSendFrame);
    break;

    case DALILIB_ACT_TYPE_PUSH_BUTTON:
      rc = device_push_button_actions(pCtrlInst, pAct, pNewSendFrame);
    break;
    
    case DALILIB_ACT_TYPE_OCCUPANCY_SENSOR:
      rc = device_occupancy_sensor_actions(pCtrlInst, pAct, pNewSendFrame);
    break;

    case DALILIB_ACT_TYPE_LIGHT_SENSOR:
      rc = device_light_sensor_actions(pCtrlInst, pAct, pNewSendFrame);
    break;

    default:
      dali_cb_log(pCtrlInst->pInst, rc,"not supported action");
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }
  return (rc);
}

//----------------------------------------------------------------------
// control device event actions entry
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_event_action(PCtrlDeviceInstance  pCtrlInst,
                                          dalilib_act_event_t* pAct,
                                          send_frame_t*        pNewSendFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint16_t eventInfo = 0;

  if (FALSE == pCtrlInst->devicePersistent.instPersVariables.instanceActive)
  {
    return (R_DALILIB_INSTANCE_DISABLED);
  }
  if (QUIESCENT_MODE_ENABLED == pCtrlInst->deviceVariables.quiescentMode)
  {
    return (R_DALILIB_CTRL_DEVICE_QUIESCENT_MODE_ENABLED);
  }
  
  switch(pAct->type)
  {
    case DALILIB_EVENT_TYPE_PUSH_BUTTON:
      rc = dev_pushbtn_eventinfo(pCtrlInst, pAct->pushButtonEvent, &eventInfo);
    break;
    
    case DALILIB_EVENT_TYPE_OCCUPANCY_SENSOR:
      rc = dev_occupancy_eventinfo(pCtrlInst, pAct->occupancySensorEvent, &eventInfo);
    break;
    
    case DALILIB_EVENT_TYPE_LIGHT_SENSOR:
      rc = dev_lightsensor_eventinfo(pCtrlInst, pAct->lightSensorEvent, &eventInfo);
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  if (R_DALILIB_OK == rc)
  {
    rc = device_create_event(pCtrlInst->pInst, pNewSendFrame, &pAct->adr, &eventInfo);
  }

  if ( (R_DALILIB_OK != rc) && (R_INTERN_ACTION != rc) )
  {
    dali_cb_log(pCtrlInst->pInst, rc,"not supported event action");
  }
  return (rc);
}

//----------------------------------------------------------------------
// control device timer function
//----------------------------------------------------------------------
void ctrl_device_timingHelper(void* pCtrlInstance)
{
  PCtrlDeviceInstance pCtrlInst = (PCtrlDeviceInstance)pCtrlInstance;
  PDaliLibInstance  pInst = pCtrlInst->pInst;

  if (NULL == pInst)
  {
    return;
  }

  // power notification time
  if (POWER_CYCLE_NOTIFY_ENABLED == pCtrlInst->devicePersistent.persVariables.powerCycleNotification &&
      0 == pCtrlInst->devicePowerCycleNotificationSend )
  {
    if (0 == pCtrlInst->devicePowerCycleNotificationTime)
    {
      pCtrlInst->devicePowerCycleNotificationTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
      pCtrlInst->devicePowerCycleNotificationRandomTime = device_create_random_power_notify_time(pCtrlInst);
    }
    else
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->devicePowerCycleNotificationTime) >= pCtrlInst->devicePowerCycleNotificationRandomTime &&
          pInst->last_tx_frame.cmds[0] != DALI_CTRL_DEVICE_POWER_NOTIFICATION_CMD)
      {
        device_create_power_cycle_notify(pCtrlInst);
      }
    }
    return;
  }
  
  // send ping
  if (CTRL_DEV_MODE_SINGLE == pInst->config.vendor.control_device_mode)
  {
    if (0 == pCtrlInst->deviceLastPingTime)
    {
      pCtrlInst->deviceLastPingTime = pInst->currentTime;
    }
    else
    {
      if (dali_difftime(pInst->currentTime, pCtrlInst->deviceLastPingTime) > DALI_CTRL_DEVICE_PING_INTERVALL_TIME)
      {
        send_frame_t newSendFrame;
        memset(&newSendFrame, 0, sizeof(newSendFrame));

        newSendFrame.frame.priority = FRAME_PRIO_FF_P1;
        newSendFrame.frame.datalength = FRAME_CTRL_GEAR_FF_LEN;
        newSendFrame.frame.data = 0xAD00;       // ping cmd
        newSendFrame.type = SEND_FRAME_TYPE_CTRL_DEV;

        dali_cb_send(pInst, &newSendFrame );
        pCtrlInst->deviceLastPingTime = pInst->currentTime;
      }
    }
  }

  // initialise time
  if (CTRL_DEVICE_INITIALISE_STATE_DISABLED != pCtrlInst->deviceVariables.initialisationState)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->deviceInitialiseStartTime) >
            DALI_CTRL_DEVICE_INITIALISE_TIME_MAX)
    {
      pCtrlInst->deviceVariables.initialisationState = CTRL_DEVICE_INITIALISE_STATE_DISABLED;
      pCtrlInst->deviceInitialiseStartTime = 0;
    }
    if ((TRUE == pCtrlInst->deviceIdentifyRunning) &&
       (dali_difftime(pInst->currentTime, pCtrlInst->deviceInitialiseStartTime) >
                                        DALI_CTRL_DEVICE_IDENTIFY_DEVICE_TIME_MAX))
    {
        pCtrlInst->deviceIdentifyRunning = FALSE;
        pCtrlInst->deviceIdentifyDeviceStartTime = 0;
    }
  }
  
  // Save persistent variables
  if (TRUE == pCtrlInst->deviceSavePersistentRunning)
  {
    // executing command SAVE PERSISTENT VARIABLES
    if (dali_difftime(pInst->currentTime, pCtrlInst->deviceSavePersistentStartTime ) >=
                                                       DALI_CTRL_DEVICE_SAVE_PERS_TIME_MAX)
    {
      pCtrlInst->deviceSavePersistentRunning = FALSE;
      pCtrlInst->deviceSavePersistentStartTime = 0;
    }
    return;
  }
  if (0 < pCtrlInst->devicePersistentChangedTime &&
      dali_difftime(pInst->currentTime, pCtrlInst->devicePersistentChangedTime ) >=
        DALI_SAVE_PERS_DELAY_MAX - ((PDaliLibInstance)pCtrlInst->pInst)->config.internal.savePersistentDelayReduction * 1000)
  {
    // persistent variables changed, but not stored yet
    pCtrlInst->devicePersistentChangedTime = 0;
    device_save_mem_blocks(pCtrlInst->pInst, &pCtrlInst->devicePersistent);
  }

  // send testframe
  if (0 != pCtrlInst->deviceTestframeTime &&
      dali_difftime(pInst->currentTime, pCtrlInst->deviceTestframeTime) >=
                                DALI_CTRL_DEVICE_SEND_TESTFRAME_RETRY_DELAY)
  {
    if (R_DALILIB_OK == dali_cb_send(pInst, &pInst->last_tx_frame))
    {
      pCtrlInst->deviceTestframeTime = 0;
    }
    else
    {
      pCtrlInst->deviceTestframeTime = pInst->currentTime;
    }
  }

  // Reset time
  if (TRUE == pCtrlInst->deviceResetRunning)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->deviceResetStartTime ) >=
                                                  DALI_CTRL_DEVICE_RESET_TIME_MAX)
    {
      pCtrlInst->deviceResetRunning = FALSE;
    }
    return;
  }

  // Reset memory bank time
  if (TRUE == pCtrlInst->deviceResetMemoryRunning)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->deviceResetMemoryStartTime ) >=
                                                    DALI_CTRL_DEVICE_MEM_RESET_TIME_MAX)
    {
      pCtrlInst->deviceResetMemoryRunning = FALSE;
    }
    return;
  }
  // Quiescent Time
  if (QUIESCENT_MODE_ENABLED == pCtrlInst->deviceVariables.quiescentMode)
  {
    if (dali_difftime(pInst->currentTime, pCtrlInst->deviceQuiescentTime ) >=
                                             DALI_CTRL_DEVICE_QUIESCENT_TIME_MAX)
    {
      pCtrlInst->deviceVariables.quiescentMode = QUIESCENT_MODE_DISABLED;
    }
    return;
  }

  if (CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
  {
    switch(pInst->config.vendor.control_device_instance_type)
    {
      case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
        dev_occupancy_timingHelper(pCtrlInst);
      break;
      
      case INPUT_DEVICE_INST_LIGHT_SENSOR:
        dev_lightsensor_timingHelper(pCtrlInst);
      break;
      
      case INPUT_DEVICE_INST_PUSH_BUTTONS:
          dev_pushbtn_timingHelper(pCtrlInst);
      break;
      
      default :
      break;
    }
  }

  // call control device extensions' timing helper
  #ifdef DALILIB_DEV_LUMINAIRE_PRESENT
    dev_luminaire_timingHelper(pCtrlInst);
  #endif
}

//----------------------------------------------------------------------
// initialize the variable of the control device
//----------------------------------------------------------------------
R_DALILIB_RESULT ctrl_device_init(PDaliLibInstance pInst)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;  

  pInst->ctrlInst.deviceInstance.pInst = pInst;

  // time function pointer initialise
  pInst->ctrlInst.deviceInstance.fpDeviceTimingHelper = ctrl_device_timingHelper;

  device_init_membank0_data(pInst);
  // load the persistent variables
  rc = device_load_mem_blocks(pInst, &pInst->ctrlInst.deviceInstance.devicePersistent);
  if (R_DALILIB_OK != rc)
  {
    // first hardware start or the application can't load the saved data
    device_init_persistent(pInst, &pInst->ctrlInst.deviceInstance.devicePersistent);
    if (FALSE == pInst->config.internal.savePersistentOnInit)
    {
      rc = R_DALILIB_OK;
    }
    else
    {
      // save recently initialized persistent memory
      pInst->ctrlInst.deviceInstance.devicePersistentChangedFlags |= DALI_CTRL_DEVICE_PERS_VARS_OPERATING_MASK |
                                                                     DALI_CTRL_DEVICE_PERS_VARS_CONFIG_MASK |
                                                                     DALI_CTRL_DEVICE_PERS_INST_OPERATING_MASK |
                                                                     DALI_CTRL_DEVICE_PERS_INST_CONFIG_MASK |
                                                                     DALI_CTRL_DEVICE_PERS_LUMINAIRE_MASK;
      rc = device_save_mem_blocks(pInst, &pInst->ctrlInst.deviceInstance.devicePersistent);
    }
  }

  // initialize other membanks
  dali_init_other_membanks(&pInst->ctrlInst.deviceInstance.deviceMemBanksOther);
  
  // initialise the control device after power cycle
  device_init_after_power_cycle(&pInst->ctrlInst.deviceInstance);

  // initialize input devices
  if (CTRL_DEV_MODE_INPUT == pInst->config.vendor.control_device_mode)
  {
    switch(pInst->config.vendor.control_device_mode)
    {
      case INPUT_DEVICE_INST_OCCUPANCY_SENSOR:
        pInst->ctrlInst.deviceInstance.deviceVariables.occupancySensorVariables.sensor_type = pInst->config.vendor.occupancy_sensor.type;
        pInst->ctrlInst.deviceInstance.deviceInstVariables.inputValue = 0x00;
      break;
      
      default:
        /* todo : DS-169 */
      break;
    }
  }

  return (rc);
}

