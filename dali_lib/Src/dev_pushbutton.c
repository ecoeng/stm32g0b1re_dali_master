/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * device.c
 *
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"
#include "device.h"

//   eDevCmd,                                               inst,opcode,a, t
static ctrl_device_commands_t pushbutton_commands_m[] =
{
  {PUSH_BTN_CMD_SET_SHORT_TIMER,                            0x00, 0x00, 0, 1},
  {PUSH_BTN_CMD_SET_DOUBLE_TIMER,                           0x00, 0x01, 0, 1},
  {PUSH_BTN_CMD_SET_REPEAT_TIMER,                           0x00, 0x02, 0, 1},
  {PUSH_BTN_CMD_SET_STUCK_TIMER,                            0x00, 0x03, 0, 1},

  {PUSH_BTN_CMD_QUERY_SHORT_TIMER,                          0x00, 0x0A, 1, 0},
  {PUSH_BTN_CMD_QUERY_SHORT_TIMER_MIN,                      0x00, 0x0B, 1, 0},
  {PUSH_BTN_CMD_QUERY_DOUBLE_TIMER,                         0x00, 0x0C, 1, 0},
  {PUSH_BTN_CMD_QUERY_DOUBLE_TIMER_MIN,                     0x00, 0x0D, 1, 0},
  {PUSH_BTN_CMD_QUERY_REPEAT_TIMER,                         0x00, 0x0E, 1, 0},
  {PUSH_BTN_CMD_QUERY_STUCK_TIMER,                          0x00, 0x0F, 1, 0},

  {PUSH_BTN_CMD_LAST,                                       0x00, 0x00, 0, 0},
};

//----------------------------------------------------------------------
// Here it is checked whether the event filter flag is set or not
// Bit-0: Button released event enabled?     default: 0
// Bit-1: Button pressed event enabled?     default: 0
// Bit-2: Short press event enabled?       default: 1
// Bit-3: Double press event enabled?       default: 0
// Bit-4: Long press start event enabled?     default: 1
// Bit-5: Long press repeat event enabled?     default: 1
// Bit-6: Long press stop event enabled?     default: 1
// Bit-7: Button stuck / free event enabled?   default: 1
//
// return: R_DALILIB_OK :
//                  - event flag bit is set
//         R_DALILIB_CTRL_DEVICE_IGNORE_EVENT :
//                  - event flag bit is not set
//         R_DALILIB_NOT_SUPPORTED_ACTION :
//                  - unknown/not supported event action
//-----------------------------------------------------------------------
static R_DALILIB_RESULT pushbtn_check_event(PCtrlDeviceInstance pCtrlInst,
                                            uint8_t             pushButtonActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pushButtonActionCode)
  {
    // event messages
    // The button is released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_RELEASED:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x01) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button is pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_PRESSED:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x02) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button is pressed and released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_SHORT_PRESSED:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x04) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button is double pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x08) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button is pressed without releasing it
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_START:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x10) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button is still pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x20) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // Following a long press start condition, the button is released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x40) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    // The button has been stuck and is now released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_FREE:
    case DALILIB_ACT_EVENT_PUSH_BUTTON_STUCK:
       rc = (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x80) ? R_DALILIB_OK : R_DALILIB_CTRL_DEVICE_IGNORE_EVENT;
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_IDLE:
      // Do nothing return OK
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t dev_pushbtn_check_nvm_variables(PCtrlDevInstPersistentVariables pInstPers)
{
  if (INPUT_DEVICE_PUSH_BUTTON_EVENT_PRIO_DEFAULT != pInstPers->eventPriority)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_PUSH_BUTTON_EVENT_FILTER_DEFAULT != pInstPers->eventFilter)
  {
    return (FALSE);
  }
  
  if (INPUT_DEVICE_PUSH_BUTTON_DOUBLE_TIMER_DEFAULT != pInstPers->pushBtnPersVariables.tDouble)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_PUSH_BUTTON_REPEAT_TIMER_DEFAULT != pInstPers->pushBtnPersVariables.tRepeat)
  {
    return (FALSE);
  }
  if (INPUT_DEVICE_PUSH_BUTTON_STUCK_TIMER_DEFAULT != pInstPers->pushBtnPersVariables.tStuck)
  {
    return (FALSE);
  }
  
  if ( MAX(25,pInstPers->pushBtnPersVariables.tShortMin) !=
       pInstPers->pushBtnPersVariables.tShort)
  {
    return (FALSE);
  }

  return (TRUE);
}

//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void dev_pushbtn_by_reset(PCtrlDevInstPersistentVariables pInstPers)
{
  pInstPers->eventPriority = INPUT_DEVICE_PUSH_BUTTON_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = INPUT_DEVICE_PUSH_BUTTON_EVENT_FILTER_DEFAULT;

  pInstPers->pushBtnPersVariables.tDouble = INPUT_DEVICE_PUSH_BUTTON_DOUBLE_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tRepeat = INPUT_DEVICE_PUSH_BUTTON_REPEAT_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tStuck = INPUT_DEVICE_PUSH_BUTTON_STUCK_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tShort = MAX(25,pInstPers->pushBtnPersVariables.tShortMin);
}

//----------------------------------------------------------------------
// initialize the default variable of the input device push button
//----------------------------------------------------------------------
void dev_pushbtn_init_persistent(PCtrlDevInstPersistentVariables pInstPers,
                                 uint8_t                         shortMin,
                                 uint8_t                         doubleMin)
{
  pInstPers->instanceType = INPUT_DEVICE_INST_PUSH_BUTTONS;
  pInstPers->resolution = INPUT_DEVICE_PUSH_BUTTON_RESOLUTION;
  pInstPers->eventPriority = INPUT_DEVICE_PUSH_BUTTON_EVENT_PRIO_DEFAULT;
  pInstPers->eventFilter = 255;//INPUT_DEVICE_PUSH_BUTTON_EVENT_FILTER_DEFAULT;

  pInstPers->pushBtnPersVariables.tDouble = 10; // INPUT_DEVICE_PUSH_BUTTON_DOUBLE_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tRepeat = INPUT_DEVICE_PUSH_BUTTON_REPEAT_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tStuck = INPUT_DEVICE_PUSH_BUTTON_STUCK_TIMER_DEFAULT;
  pInstPers->pushBtnPersVariables.tShortMin = shortMin;
  pInstPers->pushBtnPersVariables.tDoubleMin = doubleMin;

  pInstPers->pushBtnPersVariables.tShort = MAX(25,shortMin);
}


/******************************************************************************/
/* application function
 * Here only the example call of the action functions is displayed.
 * The application should decide when the action functions should be called
*******************************************************************************/
static R_DALILIB_RESULT dev_pushbtn_event_action(BUTTON_FUNCTION                buttonFunction,
                                                 DALILIB_ACT_EVENT_PUSH_BUTTON* pushButtonActionCode)
{
  switch (buttonFunction)
  {
    case BUTTON_RELEASED:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_RELEASED;
    break;
  
    case BUTTON_PRESSED:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_PRESSED;
    break;
  
    case BUTTON_SHORT_PRESS:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_SHORT_PRESSED;
    break;
  
    case BUTTON_DOUBLE_PRESS:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED;
    break;
  
    case BUTTON_LONG_PRESS_START:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_START;
    break;
  
    case BUTTON_LONG_PRESS_REPEAT:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT;
    break;
  
    case BUTTON_LONG_PRESS_STOP:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP;
    break;
  
    case BUTTON_FREE:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_FREE;
    break;
  
    case BUTTON_STUCK:
      *pushButtonActionCode = DALILIB_ACT_EVENT_PUSH_BUTTON_STUCK;
    break;
  
    default:
      return R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
    break;
  }

  return R_DALILIB_OK;
}

/******************************************************************************/
/* called when Button state changes and every 5 ms
*******************************************************************************/
static R_DALILIB_RESULT dev_pushbtn_interrupt(PCtrlDeviceInstance            pCtrlInst,
                                              DALILIB_ACT_EVENT_PUSH_BUTTON* pPushButtonActionCode)
{
  R_DALILIB_RESULT               rc              = R_DALILIB_OK;
  push_buttons_variables_t*      pPushBtn        = &pCtrlInst->deviceVariables.pushBtnVariables;
  push_buttons_pers_variables_t* pPushBTnTimings = &pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables;
  dalilib_time                   currentTime     = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;

  if (pPushBtn->state != pPushBtn->lastButtonState)
  {
    if(pPushBtn->state == PRESSED)
    {
      pPushBtn->timePressed  = currentTime;
      pPushBtn->timeReleased = 0;
    }
    else
    {
      pPushBtn->timeReleased = currentTime;
      pPushBtn->timePressed  = 0;
    }
    pPushBtn->lastButtonState = pPushBtn->state;
  }

  // check for stuck button
  if (pPushBtn->timePressed &&
      (pPushBTnTimings->tStuck*1000*10) < (currentTime - pPushBtn->timePressed) &&
      pPushBtn->currentFunction != BUTTON_FREE)
  {
    pPushBtn->currentFunction = BUTTON_STUCK;
  }

  switch (pPushBtn->currentFunction)
  {
    case BUTTON_IDLE:
      if (pPushBtn->state == PRESSED)
      {
        pPushBtn->currentFunction = BUTTON_PRESSED;
        rc = dev_pushbtn_event_action(BUTTON_PRESSED, pPushButtonActionCode);
      }
    break;

    case BUTTON_SHORT_PRESS:
    case BUTTON_RELEASED:
      // check for double press
      if(pPushBtn->state == PRESSED && 0 == pPushBtn->timeReleased)
      {
        pPushBtn->currentFunction = BUTTON_DOUBLE_PRESS;
        rc = dev_pushbtn_event_action(BUTTON_DOUBLE_PRESS, pPushButtonActionCode);
      }
      // check for short press
      else
      {
        if((pPushBTnTimings->tDouble*20*10) < (currentTime - pPushBtn->timeReleased))
        {
          pPushBtn->currentFunction = BUTTON_IDLE;
          rc = dev_pushbtn_event_action(BUTTON_SHORT_PRESS, pPushButtonActionCode);
        }
      }
    break;

    case BUTTON_PRESSED:
      if(pPushBtn->state == RELEASED)
      {
        pPushBtn->currentFunction = BUTTON_RELEASED;
        rc = dev_pushbtn_event_action(BUTTON_RELEASED, pPushButtonActionCode);
      }
      //check for long press
      else
      {
        if((pPushBTnTimings->tShort*20*10) < (currentTime - pPushBtn->timePressed))
        {
          pPushBtn->timeLongPressRepeat = currentTime;
          pPushBtn->currentFunction = BUTTON_LONG_PRESS_START;
          rc = dev_pushbtn_event_action(BUTTON_LONG_PRESS_START, pPushButtonActionCode);
        }
      }
    break;

    case BUTTON_DOUBLE_PRESS:
      if(pPushBtn->state == RELEASED)
      {
        pPushBtn->currentFunction = BUTTON_IDLE;
        rc = dev_pushbtn_event_action(BUTTON_RELEASED, pPushButtonActionCode);
      }
    break;

    case BUTTON_LONG_PRESS_START:
    case BUTTON_LONG_PRESS_REPEAT:
    case BUTTON_LONG_PRESS_STOP:
      if(pPushBtn->state == RELEASED) {
        if (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x40)
        {
          rc = dev_pushbtn_event_action(BUTTON_LONG_PRESS_STOP, pPushButtonActionCode);
        }
        else
        {
          rc = dev_pushbtn_event_action(BUTTON_RELEASED, pPushButtonActionCode);
        }
        pPushBtn->currentFunction = BUTTON_IDLE;
      }
      // check for long press repeat
      else
      {
        if((pPushBTnTimings->tRepeat*20*10) <= (currentTime - pPushBtn->timeLongPressRepeat))
        {
          rc = dev_pushbtn_event_action(BUTTON_LONG_PRESS_REPEAT, pPushButtonActionCode);
          pPushBtn->timeLongPressRepeat = currentTime;
        }
      }
    break;

    case BUTTON_STUCK:
      rc = dev_pushbtn_event_action(BUTTON_STUCK, pPushButtonActionCode);
      pPushBtn->currentFunction = BUTTON_FREE;
    break;

    case BUTTON_FREE:
      if(pPushBtn->state == RELEASED) {
        if (pCtrlInst->devicePersistent.instPersVariables.eventFilter & 0x80)
        {
          rc = dev_pushbtn_event_action(BUTTON_FREE, pPushButtonActionCode);
        }
        else
        {
          rc = dev_pushbtn_event_action(BUTTON_RELEASED, pPushButtonActionCode);
        }
        pPushBtn->currentFunction = BUTTON_IDLE;
      }
    break;

    default:
        rc = R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// result event information for push button
// (Push buttons DIN 62386-301)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_pushbtn_eventinfo(PCtrlDeviceInstance           pCtrlInst,
                                       DALILIB_ACT_EVENT_PUSH_BUTTON pushButtonActionCode,
                                       uint16_t*                     pPushButtonEvent)
{
  R_DALILIB_RESULT            rc        = R_DALILIB_OK;
  push_buttons_variables_t*   pPushBtn  = &pCtrlInst->deviceVariables.pushBtnVariables;
  uint8_t                     bInternal = 0;

  DALILIB_ACT_EVENT_PUSH_BUTTON internalPushButtonActionCode = pushButtonActionCode;

  switch(internalPushButtonActionCode)
  {
    // internal event messages
    case DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_RELEASED:
      {
        pPushBtn->state = RELEASED;
        bInternal = 1;
      }
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_PRESSED:
      {
        pPushBtn->state = PRESSED;
        bInternal = 1;
      }
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_IDLE:
      {
        bInternal = 1;
      }
    break;
    
    default:
       bInternal = 0;
    break;
  }

  if (bInternal)
  {
    if ( R_DALILIB_OK != dev_pushbtn_interrupt(pCtrlInst, &internalPushButtonActionCode))
    {
      internalPushButtonActionCode = pushButtonActionCode;
    }
  }

  rc = pushbtn_check_event(pCtrlInst, internalPushButtonActionCode);
  if (R_DALILIB_OK != rc)
  {
    return (rc);
  }

  switch(internalPushButtonActionCode)
  {
    // event messages
    // The button is released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_RELEASED:
       *pPushButtonEvent = 0x00; // 0b
       pCtrlInst->deviceInstVariables.inputValue = 0x00;
    break;
    // The button is pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_PRESSED:
       *pPushButtonEvent = 0x01; // 1b
       pCtrlInst->deviceInstVariables.inputValue = 0xFF;
    break;
    // The button is pressed and released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_SHORT_PRESSED:
       *pPushButtonEvent = 0x02; // 10b
       pCtrlInst->deviceInstVariables.inputValue = 0x00;
    break;
    // The button is double pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED:
       *pPushButtonEvent = 0x05; // 101b
       pCtrlInst->deviceInstVariables.inputValue = 0x00;
    break;
    // The button is pressed without releasing it
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_START:
       *pPushButtonEvent = 0x09; // 1001b
       pCtrlInst->deviceInstVariables.inputValue = 0xFF;
    break;
    // The button is still pressed
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT:
       *pPushButtonEvent = 0x0B; // 1011b
       pCtrlInst->deviceInstVariables.inputValue = 0xFF;
    break;
    // Following a long press start condition, the button is released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP:
       *pPushButtonEvent = 0x0C; // 1100b
       pCtrlInst->deviceInstVariables.inputValue = 0x00;
    break;
    // The button has been stuck and is now released
    case DALILIB_ACT_EVENT_PUSH_BUTTON_FREE:
       *pPushButtonEvent = 0x0E; // 1110b
       pCtrlInst->deviceInstVariables.inputValue = 0x00;
       pCtrlInst->deviceInstVariables.instanceError = FALSE;
    break;
    // The button has been pressed for a very long time and is assumed stuck
    case DALILIB_ACT_EVENT_PUSH_BUTTON_STUCK:
       *pPushButtonEvent = 0x0F; // 1111b
       pCtrlInst->deviceInstVariables.inputValue = 0xFF;
       pCtrlInst->deviceInstVariables.instanceError = TRUE;
    break;
    
    case DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_IDLE:
      // do nothing but return OK
      rc = R_INTERN_ACTION;
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// result action code for event information
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_pushbtn_reactioncode(uint32_t                            frameData,
                                          DALILIB_CTRL_DEVICE_REACTION_CODES* pPushButtonReActionCode)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint16_t eventInfo;

  eventInfo = (frameData & 0x3FF);

  switch(eventInfo)
  {
    case 0x00:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_RELEASED;
    break;
    
    case 0x01:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_PRESSED;
    break;
    
    case 0x02:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_SHORT_PRESSED;
    break;
    
    case 0x05:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_DOUBLE_PRESSED;
    break;
    
    case 0x09:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_START;
    break;
    
    case 0x0B:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_REPEAT;
    break;
    
    case 0x0C:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_LONG_PRESS_STOP;
    break;
    
    case 0x0E:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_FREE;
    break;
    
    case 0x0F:
      *pPushButtonReActionCode = DALILIB_REACT_EVENT_PUSH_BUTTON_STUCK;
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_EVENT_UNKNOWN;
    break;
  }

  return (rc);
}


//----------------------------------------------------------------------
// result opcode byte for push button command
// (Push buttons DIN 62386-301)
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_pushbtn_opcode(DALILIB_ACT_PUSH_BUTTON pushButtonActionCode,
                                    ctrl_device_commands_t* pDevCmd)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;

  switch(pushButtonActionCode)
  {
    // Set short timer
    case DALILIB_ACT_PUSH_BUTTON_SET_SHORT_TIMER:
      pDevCmd->opcodeByte = 0x00;
      pDevCmd->twice = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_SET_SHORT_TIMER;
    break;
    // Set short double timer
    case DALILIB_ACT_PUSH_BUTTON_SET_DOUBLE_TIMER:
      pDevCmd->opcodeByte = 0x01;
      pDevCmd->twice = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_SET_DOUBLE_TIMER;
    break;
    // Set repeat timer
    case DALILIB_ACT_PUSH_BUTTON_SET_REPEAT_TIMER:
      pDevCmd->opcodeByte = 0x02;
      pDevCmd->twice = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_SET_REPEAT_TIMER;
    break;
    // Set stuck timer
    case DALILIB_ACT_PUSH_BUTTON_SET_STUCK_TIMER:
      pDevCmd->opcodeByte = 0x03;
      pDevCmd->twice = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_SET_STUCK_TIMER;
    break;
    // Query short timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER:
      pDevCmd->opcodeByte = 0x0A;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_SHORT_TIMER;
    break;
    // Query short timer min
    case DALILIB_ACT_PUSH_BUTTON_QUERY_SHORT_TIMER_MIN:
      pDevCmd->opcodeByte = 0x0B;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_SHORT_TIMER_MIN;
    break;
    // Query short double timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER:
      pDevCmd->opcodeByte = 0x0C;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_DOUBLE_TIMER;
    break;
    // Query short double timer min
    case DALILIB_ACT_PUSH_BUTTON_QUERY_DOUBLE_TIMER_MIN:
      pDevCmd->opcodeByte = 0x0D;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_DOUBLE_TIMER_MIN;
    break;
    // Query repeat timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_REPEAT_TIMER:
      pDevCmd->opcodeByte = 0x0E;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_REPEAT_TIMER;
    break;
    // Query stuck timer
    case DALILIB_ACT_PUSH_BUTTON_QUERY_STUCK_TIMER:
      pDevCmd->opcodeByte = 0x0F;
      pDevCmd->answer = TRUE;
      pDevCmd->eDevCmd = PUSH_BTN_CMD_QUERY_STUCK_TIMER;
    break;
    
    default:
      rc = R_DALILIB_NOT_SUPPORTED_ACTION;
    break;
  }

  return (rc);
}

uint8_t dev_pushbtn_get_error(PCtrlDeviceInstance pCtrlInst)
{
  uint8_t errorByte = 0xFF;

  if (TRUE == pCtrlInst->deviceVariables.pushBtnVariables.bIsButtonStuck)
  {
    errorByte = 0x01;
  }
  
  return (errorByte);
}


//----------------------------------------------------------------------
// search push button command for instancebyte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
ctrl_device_commands_t* dev_pushbtn_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;
  for (idx = 0; idx < COUNTOF(pushbutton_commands_m); idx++)
  {
    if (pushbutton_commands_m[idx].instanceByte == 0x00 &&
        pushbutton_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&pushbutton_commands_m[idx]);
    }
  }
  
  return (NULL);
}


//----------------------------------------------------------------------
// process the instance configuration forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT dev_pushbtn_process_forward(PCtrlDeviceInstance     pCtrlInst,
                                             ctrl_device_commands_t* pDevCmd,
                                             uint8_t*                pRespValue)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  push_buttons_pers_variables_t* pPushBtnPersistentVariables   = NULL;

  pPushBtnPersistentVariables = &pCtrlInst->devicePersistent.instPersVariables.pushBtnPersVariables;

  pDevCmd->answer = FALSE;

  switch(pDevCmd->eDevCmd)
  {
    case PUSH_BTN_CMD_SET_SHORT_TIMER:
      {
        rc = R_IGNORE_FRAME;
        if (pCtrlInst->deviceVariables.DTR0 >= pPushBtnPersistentVariables->tShortMin)
        {
          pPushBtnPersistentVariables->tShort = pCtrlInst->deviceVariables.DTR0;
          rc = R_DALILIB_OK;
  
          dalilib_action_t action;
          memset( &action, 0, sizeof(action));
  
          action.actionType = DALILIB_CTRL_DEVICE_REACTION;
          action.deviceReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_SHORT_TIMER;
          action.deviceReact.reactValue = pPushBtnPersistentVariables->tShort;
          rc = dali_cb_reaction(pCtrlInst->pInst, &action);
        }
      }
    break;
    
    case PUSH_BTN_CMD_SET_DOUBLE_TIMER:
      {
        rc = R_IGNORE_FRAME;
        if (pCtrlInst->deviceVariables.DTR0 >= pPushBtnPersistentVariables->tDoubleMin &&
            pCtrlInst->deviceVariables.DTR0 <= 100  )
        {
          pPushBtnPersistentVariables->tDouble = pCtrlInst->deviceVariables.DTR0;
          rc = R_DALILIB_OK;
  
          dalilib_action_t action;
          memset( &action, 0, sizeof(action));
  
          action.actionType = DALILIB_CTRL_DEVICE_REACTION;
          action.deviceReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_DOUBLE_TIMER;
          action.deviceReact.reactValue = pPushBtnPersistentVariables->tDouble;
          rc = dali_cb_reaction(pCtrlInst->pInst, &action);
        }
      }
    break;
    
    case PUSH_BTN_CMD_SET_REPEAT_TIMER:
      {
        rc = R_IGNORE_FRAME;
        if (pCtrlInst->deviceVariables.DTR0 >= 5 &&
            pCtrlInst->deviceVariables.DTR0 <= 100)
        {
          pPushBtnPersistentVariables->tRepeat = pCtrlInst->deviceVariables.DTR0;
          rc = R_DALILIB_OK;
  
          dalilib_action_t action;
          memset( &action, 0, sizeof(action));
  
          action.actionType = DALILIB_CTRL_DEVICE_REACTION;
          action.deviceReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_REPEAT_TIMER;
          action.deviceReact.reactValue = pPushBtnPersistentVariables->tRepeat;
          rc = dali_cb_reaction(pCtrlInst->pInst, &action);
        }
      }
    break;
    
    case PUSH_BTN_CMD_SET_STUCK_TIMER:
      {
        rc = R_IGNORE_FRAME;
        if (pCtrlInst->deviceVariables.DTR0 >= 5)
        {
          pPushBtnPersistentVariables->tStuck = pCtrlInst->deviceVariables.DTR0;
          rc = R_DALILIB_OK;
  
          dalilib_action_t action;
          memset( &action, 0, sizeof(action));
  
          action.actionType = DALILIB_CTRL_DEVICE_REACTION;
          action.deviceReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_DEVICE_PUSH_BUTTON_SET_STUCK_TIMER;
          action.deviceReact.reactValue = pPushBtnPersistentVariables->tStuck;
          rc = dali_cb_reaction(pCtrlInst->pInst, &action);
        }
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_SHORT_TIMER:
      {
        *pRespValue = pPushBtnPersistentVariables->tShort;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_SHORT_TIMER_MIN:
      {
        *pRespValue = pPushBtnPersistentVariables->tShortMin;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_DOUBLE_TIMER:
      {
        *pRespValue = pPushBtnPersistentVariables->tDouble;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_DOUBLE_TIMER_MIN:
      {
        *pRespValue = pPushBtnPersistentVariables->tDoubleMin;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_REPEAT_TIMER:
      {
        *pRespValue = pPushBtnPersistentVariables->tRepeat;
        pDevCmd->answer = TRUE;
      }
    break;
    
    case PUSH_BTN_CMD_QUERY_STUCK_TIMER:
      {
        *pRespValue = pPushBtnPersistentVariables->tStuck;
        pDevCmd->answer = TRUE;
      }
    break;
    
    default:
      rc = R_DALILIB_CTRL_DEVICE_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

void dev_pushbtn_timingHelper(PCtrlDeviceInstance pCtrlInst)
{
  dalilib_action_t          actionButton;
  R_DALILIB_RESULT          rc ;
  send_frame_t              newSendFrame;
  push_buttons_variables_t* pPushBtn;

  if (NULL != pCtrlInst->pInst)
  {
    pPushBtn = &pCtrlInst->deviceVariables.pushBtnVariables;
    if (pPushBtn->currentFunction != BUTTON_IDLE)
    {
      memset(&newSendFrame, 0, sizeof(newSendFrame));
      actionButton.actionType               = DALILIB_EVENT_ACTION;
      actionButton.eventAct.type            = DALILIB_EVENT_TYPE_PUSH_BUTTON;
      actionButton.eventAct.pushButtonEvent = DALILIB_ACT_INTERNAL_EVENT_PUSH_BUTTON_IDLE;
      rc = ctrl_device_event_action(&((PDaliLibInstance) (pCtrlInst->pInst))->ctrlInst.deviceInstance, &actionButton.eventAct, &newSendFrame);
      if (R_DALILIB_OK == rc)
      {
        newSendFrame.type = SEND_FRAME_TYPE_CTRL_EVENT;
        rc = dali_cb_send( pCtrlInst->pInst, &newSendFrame);
      }
    }
  }
}
