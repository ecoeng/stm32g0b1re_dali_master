/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * frame.c
 * - encoding/decoding the dali raw frames
 * - encoding/decoding the dali-stack frames
 * - frame queue managemengt
 --------------------------------------------------------------------------<M*/
#include "dalistack.h"

//----------------------------------------------------------------------
// encode specific forward frame for the control gear stack frame
// Parameter: - pointer of the control gear command structure
//            - specific address byte
//            - prio
//            - pointer of the forward frame buffer
// Result:
//            R_OK: success
//            > 0 : see error.h
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_encode_ctrl_gear_spec(ctrl_gear_commands_t* pGearCmd,
                                             uint8_t               value,
                                             uint8_t               prio,
                                             dalilib_frame_t*      pFF)
{
  pFF->data = pGearCmd->opcodeByte;
  pFF->data <<= 8;
  pFF->data |= value; // specific value

  pFF->priority = prio;
  if (pGearCmd->twice)
  {
    pFF->sendtwice = 0x01;
  }
  else
  {
    pFF->sendtwice = 0x00;
  }

  pFF->datalength = FRAME_CTRL_GEAR_FF_LEN;

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// encode forward frame for the control gear stack frame
// Parameter: - pointer of the control gear command structure
//            - control gear address
//            - prio
//            - pointer of the forward frame buffer
// Result:
//            R_OK: success
//            > 0 : see error.h
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_encode_ctrl_gear(ctrl_gear_commands_t* pGearCmd,
                                        gear_adr_t*           pGearAdr,
                                        uint8_t               prio,
                                        dalilib_frame_t*      pFF)
{
  uint8_t adr  = 0x00;

  if (DALI_ADRTYPE_SHORT == pGearAdr->adrType)
  {
    // address byte
    // 0xxxxxxs => short address bits (6-Bits)
    adr = pGearAdr->adr;
    adr = adr << 1;
  }
  if (DALI_ADRTYPE_GRP == pGearAdr->adrType)
  {
    adr |= ((pGearAdr->adr&0x0f)<<1);
    // 100xxxxs => group address bits (4-Bits)
    adr |= 0x80; // 15. bit: 1
  }
  // xxxxxxxs => selector bit (1-Bit)
  adr |= pGearCmd->selectorBit;

  if (DALI_ADRTYPE_BROADCAST == pGearAdr->adrType)
  {
    // 1111111x => broadcast
    adr |= 0xFE;
  }

  pFF->data = adr;
  pFF->data <<= 8;
  pFF->data |= pGearCmd->opcodeByte;

  pFF->priority = prio;
  if (pGearCmd->twice)
  {
    pFF->sendtwice = 0x01;
  }
  else
  {
    pFF->sendtwice = 0x00;
  }

  pFF->datalength = FRAME_CTRL_GEAR_FF_LEN;

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// encode forward frame spec command for the control device
// Parameter: - pointer of the control device command structure
//            - control device address
//            - prio
//            - pointer of the forward frame buffer
// Result:
//            R_OK: success
//            > 0 : see error.h
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_encode_ctrl_device_command_spec(ctrl_device_commands_t* pDevCmd,
                                                       uint8_t                 instanceByte,
                                                       uint8_t                 value,
                                                       uint8_t                 prio,
                                                       dalilib_frame_t*        pFF)
{
  pFF->data = pDevCmd->opcodeByte;
  pFF->data <<= 8;
  pFF->data |= instanceByte;
  pFF->data <<= 8;
  pFF->data |= value;

  pFF->priority = prio;
  if (pDevCmd->twice)
  {
    pFF->sendtwice = 0x01;
  }

  pFF->datalength = FRAME_CTRL_DEV_FF_LEN;

  return (R_DALILIB_OK);
}


//----------------------------------------------------------------------
// encode forward frame command for the control device
// Parameter: - pointer of the control device command structure
//            - control device address
//            - prio
//            - pointer of the forward frame buffer
// Result:
//            R_OK: success
//            > 0 : see error.h
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_encode_ctrl_device_command(ctrl_device_commands_t* pDevCmd,
                                                  device_adr_t*           pDeviceAdr,
                                                  uint8_t                 prio,
                                                  dalilib_frame_t*        pFF)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t adr  = 0x00;
  uint8_t instanceByte = 0x00;

  switch(pDeviceAdr->adrType)
  {
    case DALI_ADRTYPE_SHORT:
      // 0xxxxxxs => short address bits (6-Bits)
      adr = pDeviceAdr->adr;
      adr <<= 1;
      // set 16. Bit for commands
      adr |= 0x01;
    break;

    case DALI_ADRTYPE_GRP:
      adr = pDeviceAdr->adr<<1;
      // 10xxxxxs => group address bits (5-Bits)
      adr |= 0x80; // 23. bit: 1
      // set 16. Bit for commands
      adr |= 0x01;
    break;

    case DALI_ADRTYPE_NBROADCAST:
      adr |= 0xFD;
    break;

    case DALI_ADRTYPE_BROADCAST:
      adr |= 0xFF;
    break;

    default:
      return (R_DALILIB_CTRL_DEVICE_INVALID_ADDRESS);
  }

  pFF->data = adr;
  pFF->data <<= 8;

  if (DALI_CTRL_DEVICE_DEFAULT_INSTANCE_BYTE != pDevCmd->instanceByte &&
      DALI_INSTANCE_ADRTYPE_NONE != pDeviceAdr->instanceAdrType)
  {
    switch(pDeviceAdr->instanceAdrType)
    {
      case DALI_INSTANCE_ADRTYPE_NUMBER:
        instanceByte = pDeviceAdr->instance; // 000xxxxx
      break;

      case DALI_INSTANCE_ADRTYPE_GRP:
        instanceByte = 0x80;
        instanceByte |= pDeviceAdr->instance; // 100xxxxx
      break;

      case DALI_INSTANCE_ADRTYPE_TYPE:
        instanceByte = 0xC0;
        instanceByte |= pDeviceAdr->instance; // 110xxxxx
      break;

      default:
        // other instance address type not supported
        rc = (R_DALILIB_CTRL_DEVICE_INVALID_INSTANCE_ADDRESS);
      break;
    }
  }
  else
  {
    instanceByte = pDevCmd->instanceByte;
  }

  if (R_DALILIB_OK == rc)
  {  
    pFF->data |= instanceByte;
    pFF->data <<= 8;
    pFF->data |= pDevCmd->opcodeByte;

    pFF->priority = prio;

    if (pDevCmd->twice)
    {
      pFF->sendtwice = 0x01;
    }

    pFF->datalength = FRAME_CTRL_DEV_FF_LEN;
  }
  
  return (rc);
}


//----------------------------------------------------------------------
// get the frame type of the received frame
//----------------------------------------------------------------------
FRAME_TYPE frame_get_type(dalilib_frame_t* pFrame)
{
  if (FRAME_TYPE_DALI_FRAME_FLAG == pFrame->type)
  {
    return (FRAME_TYPE_DALI);
  }
  
  return (FRAME_TYPE_STATUS);
}


//----------------------------------------------------------------------
// process the status frame
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_process_status_frame(PDaliLibInstance pInst, dalilib_frame_t* pFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  uint8_t reactionCode = 0;
  dalilib_action_t action;

  if (STACK_STATUS_WAIT_INTERFACE == pInst->status)
  {
    if (pFrame->status.interface_open)
    {
      pInst->status = STACK_STATUS_IDLE;
    }
  }

  if (STACK_MODE_CONTROL_GEAR == pInst->config.mode)
  {
    // first system failure event
    if (DALILIB_BUSPOWER_SYSTEMFAILURE == pFrame->status.bus_power_status &&
        !pInst->ctrlInst.gearInstance.gearSystemFailure)
    {
      rc = ctrl_gear_set_systemfailure(&pInst->ctrlInst.gearInstance, TRUE);
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // after system failure event
    if ((DALILIB_BUSPOWER_ON == pFrame->status.bus_power_status || DALILIB_BUSPOWER_OFF == pFrame->status.bus_power_status) &&
        pInst->ctrlInst.gearInstance.gearSystemFailure)
    {
      rc = ctrl_gear_set_systemfailure(&pInst->ctrlInst.gearInstance, FALSE);
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // first bus power off
    if (DALILIB_BUSPOWER_OFF == pFrame->status.bus_power_status &&
        DALILIB_BUSPOWER_ON == pInst->ctrlInst.gearInstance.gearBusPower)
    {
      pInst->ctrlInst.gearInstance.gearBusPower = DALILIB_BUSPOWER_OFF;
      //rc = dali_cb_save_mem(pInst, &pInst->ctrlInst.gearInstance.gearPersVariables,sizeof(dali_ctrl_gear_pers_variables_t));
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // after bus power off
    if (DALILIB_BUSPOWER_ON == pFrame->status.bus_power_status &&
        DALILIB_BUSPOWER_OFF == pInst->ctrlInst.gearInstance.gearBusPower)
    {
      pInst->ctrlInst.gearInstance.gearBusPower = DALILIB_BUSPOWER_ON;
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // collision detection
    if (DALILIB_BUSCOLLISION_DETECTED == pFrame->status.collision_detect)
    {
      if (DALILIB_BUSCOLLISION_DETECTED == pInst->ctrlInst.gearInstance.gearBusCollision)
      {
        // new collision detected
        // notify application that the last collision is gone
        memset(&action, 0, sizeof(action));
        action.actionType = DALILIB_STATUS_REACTION;
        action.statusReact.reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
        action.statusReact.buscollisionValue = DALILIB_BUSCOLLISION_NONE;

        dali_cb_reaction(pInst, &action);
      }
      pInst->ctrlInst.gearInstance.gearBusCollision = DALILIB_BUSCOLLISION_DETECTED;
      reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
    }
    // after collision
    if (DALILIB_BUSCOLLISION_NONE == pFrame->status.collision_detect &&
        DALILIB_BUSCOLLISION_DETECTED == pInst->ctrlInst.gearInstance.gearBusCollision)
    {
      pInst->ctrlInst.gearInstance.gearBusCollision = DALILIB_BUSCOLLISION_NONE;
      reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
    }
  }

  if (STACK_MODE_CONTROL_DEVICE == pInst->config.mode)
  {
    // first system failure event
    if (DALILIB_BUSPOWER_SYSTEMFAILURE == pFrame->status.bus_power_status &&
        !pInst->ctrlInst.deviceInstance.deviceSystemFailure)
    {
      rc = ctrl_device_set_systemfailure(&pInst->ctrlInst.deviceInstance, TRUE);
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // after system failure event
    if ((DALILIB_BUSPOWER_ON == pFrame->status.bus_power_status || DALILIB_BUSPOWER_OFF == pFrame->status.bus_power_status) &&
        pInst->ctrlInst.deviceInstance.deviceSystemFailure)
    {
      rc = ctrl_device_set_systemfailure(&pInst->ctrlInst.deviceInstance, FALSE);
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // first bus power off
    if (DALILIB_BUSPOWER_OFF == pFrame->status.bus_power_status &&
        DALILIB_BUSPOWER_ON == pInst->ctrlInst.deviceInstance.deviceBusPower)
    {
      pInst->ctrlInst.deviceInstance.deviceBusPower = DALILIB_BUSPOWER_OFF;
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // after bus power off
    if (DALILIB_BUSPOWER_ON == pFrame->status.bus_power_status &&
        DALILIB_BUSPOWER_OFF == pInst->ctrlInst.deviceInstance.deviceBusPower)
    {
      pInst->ctrlInst.deviceInstance.deviceBusPower = DALILIB_BUSPOWER_ON;
      reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
    }
    // collision detection
    if (DALILIB_BUSCOLLISION_DETECTED == pFrame->status.collision_detect)
    {
      if (DALILIB_BUSCOLLISION_DETECTED == pInst->ctrlInst.deviceInstance.deviceBusCollision)
      {
        // new collision detected
        // notify application that the last collision is gone
        memset(&action, 0, sizeof(action));
        action.actionType = DALILIB_STATUS_REACTION;
        action.statusReact.reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
        action.statusReact.buscollisionValue = DALILIB_BUSCOLLISION_NONE;

        dali_cb_reaction(pInst, &action);
      }
      pInst->ctrlInst.deviceInstance.deviceBusCollision = DALILIB_BUSCOLLISION_DETECTED;
      reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
    }
    // after collision
    if (DALILIB_BUSCOLLISION_NONE == pFrame->status.collision_detect &&
        DALILIB_BUSCOLLISION_DETECTED == pInst->ctrlInst.deviceInstance.deviceBusCollision)
    {
      pInst->ctrlInst.deviceInstance.deviceBusCollision = DALILIB_BUSCOLLISION_NONE;
      reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
    }
  }

  // notify the application
  if (reactionCode)
  {
    memset(&action, 0, sizeof(action));
    action.actionType = DALILIB_STATUS_REACTION;
    switch(reactionCode)
    {
      case DALILIB_REACT_STATUS_BUSPOWER:
        action.statusReact.reactionCode = DALILIB_REACT_STATUS_BUSPOWER;
        action.statusReact.buspowerValue = pFrame->status.bus_power_status;
      break;

      case DALILIB_REACT_STATUS_BUSCOLLISION:
        action.statusReact.reactionCode = DALILIB_REACT_STATUS_BUSCOLLISION;
        action.statusReact.buscollisionValue = (dalilib_buscollision_status_t)pFrame->status.collision_detect;
      break;

      default:
        reactionCode = 0;
      break;
    }
  }
  if (reactionCode)
  {
    dali_cb_reaction(pInst, &action);
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the dali-stack frame
//----------------------------------------------------------------------
R_DALILIB_RESULT frame_process_dali_frame(PDaliLibInstance pInst, dalilib_frame_t* pFrame)
{
  R_DALILIB_RESULT rc = R_DALILIB_OK;
  dalilib_action_t action;

  switch(pFrame->datalength)
  {
    case DALI_RESPONSE_FRAME: // 1-Byte
      // reset twice flag, twice flag used only for forward frames
      pInst->last_rx_frame.bWaitTwice = 0;

      // stack does not wait response
      if (STACK_STATUS_WAIT_RESPONSE != pInst->status)
      {
        // ignore frame;
        return (rc);
      }
      if (pFrame->priority > FRAME_PRIO_BF_P0)
      {
        //timeout for backward frame, ignore frame
        rc = R_DALILIB_RESPONSE_FRAME_NOT_ACCEPTED;
      }
      pInst->status = STACK_STATUS_IDLE;
      if (R_DALILIB_OK == rc)
      {
        if (SEND_FRAME_TYPE_CTRL_GEAR == pInst->last_tx_frame.type)
        {
          rc = ctrl_gear_process_response_frame(pInst, pFrame);
        }
        if (SEND_FRAME_TYPE_CTRL_DEV == pInst->last_tx_frame.type)
        {
          rc = ctrl_device_process_response_frame(pInst, pFrame);
        }
      }
      if (0 >= pInst->last_tx_frame.cmdCounter ||
          R_ABORT_ACTION == rc)
      {
        // no more commands to send
        memset(&pInst->last_tx_frame, 0, sizeof(pInst->last_tx_frame));
      }
    break;

    case DALI_CTRL_GEAR_FORWARD_FRAME: // 2-Byte
      if (STACK_MODE_CONTROL_GEAR ==  pInst->config.mode)
      {
        rc = ctrl_gear_process_forward_frame(&pInst->ctrlInst.gearInstance, pFrame);
      }
      else
      {
        rc = R_DALILIB_FRAME_NOT_SUPPORTED;
      }
    break;

    case DALI_CTRL_DEVICE_FORWARD_FRAME: // 3-Byte
      if (STACK_MODE_CONTROL_DEVICE ==  pInst->config.mode)
      {
        rc = ctrl_device_process_forward_frame(&pInst->ctrlInst.deviceInstance, pFrame);
      }
      else
      {
        rc = R_DALILIB_FRAME_NOT_SUPPORTED;
      }
    break;

    case DALI_RESERVED_FORWARD_FRAME_1: // 2.5-Byte
    case DALI_RESERVED_FORWARD_FRAME_2: // 4-Byte
      // reset twice flag
      pInst->last_rx_frame.bWaitTwice = 0;
      rc = R_DALILIB_FRAME_NOT_SUPPORTED;
    break;

    default:
      // proprietary forward frame
      memset(&action, 0, sizeof(action));
      action.actionType = DALILIB_RAW_FRAME_REACTION;
      action.rawFrameReact.reactionCode = DALILIB_REACT_RAW_FRAME_RECEIVE;
      action.rawFrameReact.valueType = DALILIB_RESPONSE_VALUE_VALID;
      action.rawFrameReact.frame.timestamp = pFrame->timestamp;
      action.rawFrameReact.frame.datalength = pFrame->datalength;
      action.rawFrameReact.frame.data = pFrame->data;

      dali_cb_reaction(pInst, &action);

      rc = R_DALILIB_OK;
    break;
}

  return (rc);
}
