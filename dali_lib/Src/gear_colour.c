/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_colour.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"
#include "gear.h"

#define DALI_TRUE  1;
#define DALI_FALSE 0;

#define AUTOMATIC_ACTIVATION  0x01
#define AUTOMATIC_CALIBRATION 0x40

#define COLOUR_TYPE_XY_COORDINATE      0x10
#define COLOUR_TYPE_COLOUR_TEMPERATURE 0x20
#define COLOUR_TYPE_PRIMARY_N          0x40
#define COLOUR_TYPE_RGBWAF             0x80

#define COLOUR_TYPE_FEATURES_XY          0x01
#define COLOUR_TYPE_FEATURES_TEMPERATURE 0x02
#define COLOUR_TYPE_FEATURES_PRIMARY     0x1C
#define COLOUR_TYPE_FEATURES_RGBWAF      0xE0

#define XY_COORDINATE_STEP 0x100
#define COLOUR_MAX_VALUE   65534

// CIE-xy chromaticity for sRGB red at [0,64; 0,33], green at [0,30; 0,60]
// and blue at [0,15; 0,06].
// in units of 1/65536
static uint16_t srgbValue[3][2] = {{41943, 21627}, {19661, 39322}, {9830, 3932}};
// CIE-xy chromaticity for Adobe-RGB red at [0,64; 0,33], green at [0,21; 0,71]
// and blue at [0,15; 0,06].
// in units of 1/65536
static uint16_t adobeRgbValue[3][2] = {{41943, 21627}, {13763, 46531}, {9830, 3932}};

// eGearColourCmd,                                 selectorBit, opcodeByte,  a,  t}
static ctrl_gear_commands_t colour_commands_m[] =
{
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_XCOORDINATE,               1,       0xE0,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_YCOORDINATE,               1,       0xE1,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_ACTIVATE,                           1,       0xE2,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_XCOORDINATE_STEP_UP,                1,       0xE3,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_XCOORDINATE_STEP_DOWN,              1,       0xE4,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_YCOORDINATE_STEP_UP,                1,       0xE5,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_YCOORDINATE_STEP_DOWN,              1,       0xE6,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_COLOUR_TEMPERATURE,        1,       0xE7,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_COLOUR_TEMPERATURE_STEP_COOLER,     1,       0xE8,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_COLOUR_TEMPERATURE_STEP_WARMER,     1,       0xE9,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_PRIMARY_N_DIMLEVEL,        1,       0xEA,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_RGB_DIMLEVEL,              1,       0xEB,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_WAF_DIMLVEL,               1,       0xEC,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_SET_TEMP_RGBWAF_CONTROL,            1,       0xED,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_COPY_REPORT_TO_TEMP,                1,       0xEE,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_STORE_TY_PRIMARY_N,                 1,       0xF0,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_STORE_XY_COORDINATE_PRIMARY_N,      1,       0xF1,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_STORE_COLOUR_TEMPERATURE_LIMIT,     1,       0xF2,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_STORE_GEAR_FEATURES_STATUS,         1,       0xF3,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_ASSIGN_COLOUR_TO_LINKED_CHANNEL,    1,       0xF5,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_START_AUTO_CALIBRATION,             1,       0xF6,  0,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_GEAR_FEATURES_STATUS,         1,       0xF7,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_COLOUR_STATUS,                1,       0xF8,   1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_COLOUR_TYPE_FEAUTRES,         1,       0xF9,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_COLOUR_VALUE,                 1,       0xFA,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_RGBWAF_CONTROL,               1,       0xFB,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_ASSIGNED_COLOUR,              1,       0xFC,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_QUERY_EXTENTED_VERSION_NUMBER,      1,       0xFF,  1,  0},
  {(GEAR_CMD) COLOUR_CMD_LAST,                               0,       0x00,  0,  0}
};

//----------------------------------------------------------------------
// search colour control command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* colour_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(colour_commands_m); idx++)
  {
    if (colour_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&colour_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// Checks point location in relation to line (side of colour triangle)
//
// Point can be located 'left' (above), 'right' (underneath), on line (but beyond distance))
//  return:
// -1 -> left of line
//  0 -> on line (beyond distance)
//  1 -> right of line
//  2 -> on line
//
//  x,y point to test; x1,y1 line point 1; x2,y2 line point 2
//----------------------------------------------------------------------
uint8_t PointRelationToLine(uint16_t x, uint16_t y, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
  uint16_t dx1, dx2, dy1, dy2;

  dx1 = x1 - x;
  dy1 = y1 - y;
  dx2 = x2 - x;
  dy2 = y2 - y;
  if (((dx1 == 0) && (dy1 == 0)) || ((dx2 == 0) && (dy2 == 0)))
  {
    return 2;
  }
  uint16_t dx1dy2 = dx1*dy2;
  uint16_t dy1dx2 = dy1*dx2;
  if (dx1dy2 > dy1dx2)
  {
    return 1;
  }
  if (dx1dy2 < dy1dx2)
  {
    return -1;
  }
  if ((dx1*dx2 < 0) || (dy1*dy2 < 0))
  {
    return -1;
  }
  if ((dx1*dx1+dy1*dy1) < (dx2*dx2+dy2*dy2))
  {
    return 1;
  }
  if (((dx1 > 0) && (dx2 == 0)) || ((dx1 == 0) && (dx2 > 0)))
  {
    return 2;
  }
  if (((dy1 > 0) && (dy2 == 0)) || ((dy1 == 0) && (dy2 > 0)))
  {
    return 2;
  }

  return 0;
}

//----------------------------------------------------------------------
//  x,y point to test; x1,y1 triangle point 1; x2,y2 triangle point 2;
// x3,y3 triangle point 3
//----------------------------------------------------------------------
uint8_t PointInColourTriangel(uint16_t x, uint16_t y, uint16_t colourSpaceValue[3][2])
{
  // The point needs to be located 3 times right or 3 times left of all 3
  // sides of the colour triangle
  uint8_t bRight;

  uint16_t x1, y1;
  uint16_t x2, y2;
  uint16_t y3, x3;

  x1 = colourSpaceValue[0][0];
  y1 = colourSpaceValue[0][1];
  x2 = colourSpaceValue[1][0];
  y2 = colourSpaceValue[1][1];
  x3 = colourSpaceValue[2][0];
  y3 = colourSpaceValue[2][1];

  switch (PointRelationToLine(x,y,x1,y1,x2,y2))
  {
    case 0:
      return DALI_FALSE;
    
    case 2:
      return DALI_TRUE;
    
    case 1:
      bRight = DALI_TRUE;
    break;
    
    default:
      bRight = DALI_FALSE;
    break;
  }
  switch (PointRelationToLine(x,y,x2,y2,x3,y3))
  {
    case 0:
      return DALI_FALSE;

    case 2:
      return DALI_TRUE;

    case 1:
      if (!bRight)
      {
        return DALI_FALSE;
      }
    break;

    default:
      if (bRight)
      {
        return DALI_FALSE;
      }
    break;
  }
  switch (PointRelationToLine(x,y,x3,y3,x1,y1))
  {
    case 0:
      return DALI_FALSE;

    case 2:
      return DALI_TRUE;

    case 1:
      if (!bRight)
      {
        return DALI_FALSE;
      }
    break;

    default:
      if (bRight)
      {
        return DALI_FALSE;
      }
    break;
  }

  return DALI_TRUE;
}

//----------------------------------------------------------------------
// initialize the variable after external power cycle
//----------------------------------------------------------------------
void gear_colour_init_after_power_cycle(dali_ctrl_gear_pers_mem_t*   pGearPers,
                                        PCtrlGearVariables           pGearVariables)
{
  // keep Bit 7 and 8, automatic calibration, restoration of auto calibration
  pGearPers->colourPersVariables.gearFeatureStatus &= 0xC0;

  // set power on level based on colour type
  switch (pGearPers->colourPersVariables.powerOnColourType)
  {
    case COLOUR_TYPE_FEATURES_XY:
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.xyCoordinateValue.xCoordinate)
      {
        pGearVariables->colourVariables.xyCoordinate.value.xCoordinate =
          pGearPers->colourPersVariables.powerOnColourValue.xyCoordinateValue.xCoordinate;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.xyCoordinateValue.yCoordinate)
      {
        pGearVariables->colourVariables.xyCoordinate.value.yCoordinate =
          pGearPers->colourPersVariables.powerOnColourValue.xyCoordinateValue.yCoordinate;
      }
      pGearVariables->colourVariables.colourStatus.xyCoordinateActive = 1;
    break;

    case COLOUR_TYPE_COLOUR_TEMPERATURE:
      if (DALI_MASK2 > pGearPers->colourPersVariables.powerOnColourValue.ColourTemperature)
      {
        pGearVariables->colourVariables.colourTemperature.ColourTemperature =
          pGearPers->colourPersVariables.powerOnColourValue.ColourTemperature;
      }
      pGearVariables->colourVariables.colourStatus.temperatureActive = 1;
    break;

    case COLOUR_TYPE_PRIMARY_N:
      for (int i = 0; i < 6; i++)
      {
        if (DALI_MASK2 > pGearPers->colourPersVariables.powerOnColourValue.PrimeNdimlevel[i])
        {
          pGearVariables->colourVariables.primaryN.primeNdimlevel[i] =
            pGearPers->colourPersVariables.powerOnColourValue.PrimeNdimlevel[i];
        }
      }
      pGearVariables->colourVariables.colourStatus.primaryNactive = 1;
    break;

    case COLOUR_TYPE_RGBWAF:
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.red)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.red =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.red;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.green)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.green =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.green;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.blue)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.blue =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.blue;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.white)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.white =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.white;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.amber)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.amber =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.amber;
      }
      if (DALI_MASK > pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.free)
      {
        pGearVariables->colourVariables.rgbwaf.dimlevel.free =
          pGearPers->colourPersVariables.powerOnColourValue.rgbwafDimlevel.free;
      }
      pGearVariables->colourVariables.colourStatus.RGBWAFactive = 1;
    break;
  }
}

//----------------------------------------------------------------------
// reset temporary values
//----------------------------------------------------------------------
void gear_colour_reset_temp_values(colour_pers_variables_t* pColourPersVar,
                                   colour_variables_t*      pColourVar)
{
  switch (pColourPersVar->powerOnColourType)
  {
    case COLOUR_TYPE_XY_COORDINATE:
      pColourVar->xyCoordinate.temp.xCoordinate = DALI_MASK2;
      pColourVar->xyCoordinate.temp.yCoordinate = DALI_MASK2;
    break;

    case COLOUR_TYPE_COLOUR_TEMPERATURE:
      pColourVar->colourTemperature.tempColourTemperature = DALI_MASK2;
    break;

    case COLOUR_TYPE_PRIMARY_N:
      pColourVar->primaryN.temp.xCoordinate = DALI_MASK2;
      pColourVar->primaryN.temp.yCoordinate = DALI_MASK2;
      memset(pColourVar->primaryN.tempPrimeNdimlevel, DALI_MASK,
             sizeof(pColourVar->primaryN.tempPrimeNdimlevel) - 1);
    break;

    case COLOUR_TYPE_RGBWAF:
      pColourVar->rgbwaf.tempDimlevel.RGBWAFcontrol = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.red           = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.green         = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.blue          = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.white         = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.amber         = DALI_MASK;
      pColourVar->rgbwaf.tempDimlevel.free          = DALI_MASK;
    break;

    default:
      // invalid colour type
    break;
  }

  pColourVar->tempColourType = DALI_MASK;
}

//----------------------------------------------------------------------
// initialize the variable by reset
//----------------------------------------------------------------------
void gear_colour_by_reset(dali_ctrl_gear_pers_mem_t*   pGearPers,
                          PCtrlGearVariables           pGearVariables)
{
  gear_colour_reset_temp_values(&pGearPers->colourPersVariables, &pGearVariables->colourVariables);

  switch (pGearPers->colourPersVariables.powerOnColourType)
  {
    case COLOUR_TYPE_XY_COORDINATE:
      pGearVariables->colourVariables.xyCoordinate.report.xCoordinate = DALI_MASK2;
      pGearVariables->colourVariables.xyCoordinate.report.yCoordinate = DALI_MASK2;
    break;

    case COLOUR_TYPE_COLOUR_TEMPERATURE:
      pGearVariables->colourVariables.colourTemperature.reportColourTemperature = DALI_MASK2;
    break;

    case COLOUR_TYPE_PRIMARY_N:
      memset(pGearVariables->colourVariables.primaryN.reportPrimeNdimlevel, DALI_MASK,
             sizeof(pGearVariables->colourVariables.primaryN.reportPrimeNdimlevel) - 1);
    break;

    case COLOUR_TYPE_RGBWAF:
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.RGBWAFcontrol = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.red           = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.green         = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.blue          = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.white         = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.amber         = DALI_MASK;
      pGearVariables->colourVariables.rgbwaf.reportDimlevel.free          = DALI_MASK;
    break;

    default:
      // invalid colour type
    break;
    }

    pGearVariables->colourVariables.reportColourType = DALI_MASK;
    // reset Gear Feature/Status except automatic calibration
    // and restoration of auto calibration
    pGearPers->colourPersVariables.gearFeatureStatus &= 0xC1;
}

//----------------------------------------------------------------------
// check the persistent variable for "resetState" Flag
//----------------------------------------------------------------------
uint8_t gear_colour_check_nvm_variables(dali_ctrl_gear_pers_mem_t* pPersistentMemory)
{
  if (0 != pPersistentMemory->ledPersVariables.fft)
  {
    return (FALSE);
  }
  if (0 != pPersistentMemory->ledPersVariables.dimmingCurve)
  {
    return (FALSE);
  }
  if (pPersistentMemory->ledPersVariables.operatingMode & 0x10)
  {
    return (FALSE);
  }
  return (TRUE);
}

//----------------------------------------------------------------------
// initialize the default variable of the input device colour control
//----------------------------------------------------------------------
void gear_colour_init_persistent(PDaliLibInstance             pInst,
                                 dali_ctrl_gear_pers_mem_t*   pGearPers)
{
  pGearPers->colourPersVariables.powerOnColourType =
    pInst->config.vendor.colour_params.powerOnColourType;
  memcpy(pGearPers->colourPersVariables.powerOnColourValue.PrimeNdimlevel,
         pInst->config.vendor.colour_params.powerOnColourValue.PrimeNdimlevel,
         sizeof(pGearPers->colourPersVariables.powerOnColourValue.PrimeNdimlevel) - 1);

  pGearPers->colourPersVariables.colourTypeFeatures = 0x00;

  if (pInst->config.vendor.colour_params.colourTypeFeatures.xyCoordinate)
  {
    pGearPers->colourPersVariables.colourTypeFeatures |= 0x01; // Bit 0
  }

  if (pInst->config.vendor.colour_params.colourTypeFeatures.temperature)
  {
    pGearPers->colourPersVariables.colourTypeFeatures |= 0x02; // Bit 1
  }

  if (pInst->config.vendor.colour_params.colourTypeFeatures.primaryN)
  {
    pGearPers->colourPersVariables.colourTypeFeatures |=
      (pInst->config.vendor.colour_params.colourTypeFeatures.primaryN << 2);
  }

  if (pInst->config.vendor.colour_params.colourTypeFeatures.RGBWAF)
  {
    pInst->config.vendor.colour_params.colourTypeFeatures.primaryN |=
      (pInst->config.vendor.colour_params.colourTypeFeatures.RGBWAF << 4);
  }

  pGearPers->colourPersVariables.gearFeatureStatus = 0x00;

  if (pInst->config.vendor.colour_params.featuresStatus.autoCalibration)
  {
    pGearPers->colourPersVariables.gearFeatureStatus |= 0x01;
  }

  if (pInst->config.vendor.colour_params.featuresStatus.automaticActivation)
  {
    pGearPers->colourPersVariables.gearFeatureStatus |= 0x40;
  }

  if(pInst->config.vendor.colour_params.featuresStatus.restorationAutoCalibration)
  {
    pGearPers->colourPersVariables.gearFeatureStatus |= 0x80;
  }

  pGearPers->colourPersVariables.SystemFailureColourType =
    pInst->config.vendor.colour_params.systemFailureColourType;
  memcpy(pGearPers->colourPersVariables.SystemFailureColourValue.PrimeNdimlevel,
         pInst->config.vendor.colour_params.systemFailureColourValue.PrimeNdimlevel,
         sizeof(pGearPers->colourPersVariables.SystemFailureColourValue.PrimeNdimlevel));

  switch (pInst->config.vendor.colour_params.powerOnColourType)
  {
    case COLOUR_TYPE_XY_COORDINATE:
      pGearPers->colourPersVariables.xyCoordinate.colourSpaceType =
        pInst->config.vendor.colour_params.xyCoordinateParam.colourSpaceType;
      if (pGearPers->colourPersVariables.xyCoordinate.colourSpaceType == COLOUR_SPACE_SRGB)
      {
        memcpy(pGearPers->colourPersVariables.xyCoordinate.colourSpaceValue,
               srgbValue,
               sizeof(pGearPers->colourPersVariables.xyCoordinate.colourSpaceValue));
      }
      else
      {
        if (pGearPers->colourPersVariables.xyCoordinate.colourSpaceType == COLOUR_SPACE_ADOBE_RGB)
        {
          memcpy(pGearPers->colourPersVariables.xyCoordinate.colourSpaceValue,
                 adobeRgbValue,
                 sizeof(pGearPers->colourPersVariables.xyCoordinate.colourSpaceValue));
        }
        else
        {
          for (int i=0; i < 3; i++)
          {
            for (int j=0; j < 2; j++)
            {
              // convert to uints of 1/65536
              pGearPers->colourPersVariables.xyCoordinate.colourSpaceValue[i][j] =
                (uint16_t)(pInst->config.vendor.colour_params.xyCoordinateParam.colourSpaceValue[i][j] * 65536);
            }
          }
        }
      }
      memcpy(pGearPers->colourPersVariables.xyCoordinate.tyPrime,
             pInst->config.vendor.colour_params.xyCoordinateParam.primary.tyPrime,
             sizeof(pGearPers->colourPersVariables.xyCoordinate.tyPrime));
      memcpy(pGearPers->colourPersVariables.xyCoordinate.xCoordinatePrime,
             pInst->config.vendor.colour_params.xyCoordinateParam.primary.xCoordinatePrime,
             sizeof(pGearPers->colourPersVariables.xyCoordinate.xCoordinatePrime));
      memcpy(pGearPers->colourPersVariables.xyCoordinate.yCoordinatePrime,
             pInst->config.vendor.colour_params.xyCoordinateParam.primary.yCoordinatePrime,
             sizeof(pGearPers->colourPersVariables.xyCoordinate.yCoordinatePrime));
    break;

    case COLOUR_TYPE_COLOUR_TEMPERATURE:
      pGearPers->colourPersVariables.colourTemperature.ColourTemperatureCoolest =
        pInst->config.vendor.colour_params.temperatureParam.range.ColourTemperatureCoolest;
      pGearPers->colourPersVariables.colourTemperature.ColourTemperaturePhyCoolest =
        pInst->config.vendor.colour_params.temperatureParam.range.ColourTemperaturePhyCoolest;
      pGearPers->colourPersVariables.colourTemperature.ColourTemperaturePhyWarmest =
        pInst->config.vendor.colour_params.temperatureParam.range.ColourTemperaturePhyWarmest;
      pGearPers->colourPersVariables.colourTemperature.ColourTemperatureWarmest =
        pInst->config.vendor.colour_params.temperatureParam.range.ColourTemperatureWarmest;
    break;

    case COLOUR_TYPE_PRIMARY_N:
      memcpy(pGearPers->colourPersVariables.primaryN.tyPrime,
             pInst->config.vendor.colour_params.primaryNParam.primary.tyPrime,
             sizeof(pGearPers->colourPersVariables.primaryN.tyPrime));
      memcpy(pGearPers->colourPersVariables.primaryN.xCoordinatePrime,
             pInst->config.vendor.colour_params.primaryNParam.primary.xCoordinatePrime,
             sizeof(pGearPers->colourPersVariables.primaryN.xCoordinatePrime));
      memcpy(pGearPers->colourPersVariables.primaryN.yCoordinatePrime,
             pInst->config.vendor.colour_params.primaryNParam.primary.yCoordinatePrime,
             sizeof(pGearPers->colourPersVariables.primaryN.yCoordinatePrime));
    break;

    case COLOUR_TYPE_RGBWAF:
      memcpy(pGearPers->colourPersVariables.assignedColour,
             pInst->config.vendor.colour_params.rgbwafDimlevel.assignedColour,
             sizeof(pGearPers->colourPersVariables.assignedColour));
    break;

    default:
      // invalid colour type
    break;
  }
}

R_DALILIB_RESULT gear_colour_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                      dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                             rc = R_DALILIB_OK;
  dali_ctrl_gear_colour_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_COLOUR_ID;
  block.version = DALI_CTRL_GEAR_PERS_COLOUR_VERSION;

  // populate memory block
  block.powerOnColourType        = pGearPersistent->colourPersVariables.powerOnColourType;
  block.powerOnColourValue       = pGearPersistent->colourPersVariables.powerOnColourValue;
  block.SystemFailureColourType  = pGearPersistent->colourPersVariables.SystemFailureColourType;
  block.SystemFailureColourValue = pGearPersistent->colourPersVariables.SystemFailureColourValue;
  block.gearFeatureStatus        = pGearPersistent->colourPersVariables.gearFeatureStatus;
  block.colourTypeFeatures       = pGearPersistent->colourPersVariables.colourTypeFeatures;
  memcpy(block.sceneColourType, pGearPersistent->colourPersVariables.sceneColourType, sizeof(block.sceneColourType));
  memcpy(block.sceneColourValue, pGearPersistent->colourPersVariables.sceneColourValue, sizeof(block.sceneColourValue));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_COLOUR_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_COLOUR_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_colour_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                       uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                              rc = R_DALILIB_OK;
  char                                          buf[128];
  colour_pers_variables_t*                      pPersistentVariables = &pCtrlInst->gearPersistent.colourPersVariables;
  PDaliLibInstance                              pInst = (PDaliLibInstance)pCtrlInst->pInst;
  dali_ctrl_gear_colour_pers_variables_block_t* pBlock = (dali_ctrl_gear_colour_pers_variables_block_t*)pDaliMem;
  uint8_t                                       i;

  // copy mem block
  pPersistentVariables->powerOnColourType = pBlock->powerOnColourType;
  pPersistentVariables->powerOnColourValue = pBlock->powerOnColourValue;
  pPersistentVariables->SystemFailureColourType = pBlock->SystemFailureColourType;
  pPersistentVariables->SystemFailureColourValue = pBlock->SystemFailureColourValue;
  pPersistentVariables->gearFeatureStatus = pBlock->gearFeatureStatus;
  pPersistentVariables->colourTypeFeatures = pBlock->colourTypeFeatures;
  memcpy(pPersistentVariables->sceneColourType,  pBlock->sceneColourType,  sizeof(pPersistentVariables->sceneColourType));
  memcpy(pPersistentVariables->sceneColourValue, pBlock->sceneColourValue, sizeof(pPersistentVariables->sceneColourValue));

  // check value range
  if (0x10 != pPersistentVariables->powerOnColourType &&
      0x20 != pPersistentVariables->powerOnColourType &&
      0x40 != pPersistentVariables->powerOnColourType &&
      0x80 != pPersistentVariables->powerOnColourType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable powerOnColourType is out of bounds. Value: %u Range: [0x10, 0x20, 0x40, 0x80]",
             pPersistentVariables->powerOnColourType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->powerOnColourType = pInst->config.vendor.colour_params.powerOnColourType;
  }
  if (0x10 != pPersistentVariables->SystemFailureColourType &&
      0x20 != pPersistentVariables->SystemFailureColourType &&
      0x40 != pPersistentVariables->SystemFailureColourType &&
      0x80 != pPersistentVariables->SystemFailureColourType)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable SystemFailureColourType is out of bounds. Value: %u Range: [0x10, 0x20, 0x40, 0x80]",
             pPersistentVariables->SystemFailureColourType);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->SystemFailureColourType = pInst->config.vendor.colour_params.systemFailureColourType;
  }
  if (0xC1 & pPersistentVariables->gearFeatureStatus)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable gearFeatureStatus is out of bounds. Value: %u Range: (xx00 000x)",
             pPersistentVariables->gearFeatureStatus);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->gearFeatureStatus = 0x00;

    if (pInst->config.vendor.colour_params.featuresStatus.autoCalibration)
    {
      pPersistentVariables->gearFeatureStatus |= 0x01;
    }

    if (pInst->config.vendor.colour_params.featuresStatus.automaticActivation)
    {
      pPersistentVariables->gearFeatureStatus |= 0x40;
    }

    if(pInst->config.vendor.colour_params.featuresStatus.restorationAutoCalibration)
    {
      pPersistentVariables->gearFeatureStatus |= 0x80;
    }
  }
  for (i = 0; i < DALI_GEAR_COLOUR_MAX_SCENES; i++)
  {
    if (COLOUR_TYPE_XY_COORDINATE      != pPersistentVariables->sceneColourType[i] &&
        COLOUR_TYPE_COLOUR_TEMPERATURE != pPersistentVariables->sceneColourType[i] &&
        COLOUR_TYPE_PRIMARY_N          != pPersistentVariables->sceneColourType[i] &&
        COLOUR_TYPE_RGBWAF             != pPersistentVariables->sceneColourType[i])
    {
      snprintf(buf, sizeof(buf),
               "dali library variable sceneColourType[%u] is out of bounds. Value: %u Range: [0x10, 0x20, 0x30, 0x40]",
               i,
               pPersistentVariables->sceneColourType[i]);
      dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
      // set default value
      pPersistentVariables->sceneColourType[i] = pInst->config.vendor.colour_params.powerOnColourType;
    }
    if (COLOUR_TYPE_COLOUR_TEMPERATURE == pPersistentVariables->sceneColourType[i] &&
        0                              == pPersistentVariables->sceneColourValue[i].ColourTemperature)
    {
      snprintf(buf, sizeof(buf),
               "dali library variable sceneColourValue[%u].ColourTemperature is out of bounds. Value: %u Range: [1..65535]",
               i,
               pPersistentVariables->sceneColourValue[i].ColourTemperature);
      dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
      // set default value
      pPersistentVariables->sceneColourValue[i].ColourTemperature = 1;
    }
  }

  return (rc);
}


//----------------------------------------------------------------------
// process Colour Control forward query frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_colour_process_forward_querys(PCtrlGearInstance      pCtrlInst,
                                                    dalilib_frame_t*       pForwardFrame,
                                                    ctrl_gear_commands_t** ppGearCmd,
                                                    gear_adr_t*            pReceivedAdr,
                                                    uint8_t*               pRespValue)
{
  R_DALILIB_RESULT         rc             = R_IGNORE_FRAME;
  colour_pers_variables_t* pColourPersVar = NULL;
  colour_variables_t*      pColourVar     = NULL;
  uint8_t                  sceneIdx       = 0;

  (void)pCtrlInst;
  (void)pReceivedAdr;
  (void)pRespValue;

  switch((*ppGearCmd)->eGearCmd)
  {
    case GEAR_CMD_QUERY_POWER_ON_LEVEL:
      pColourVar->reportColourType = pColourPersVar->powerOnColourType;
      switch (pColourPersVar->powerOnColourType)
      {
        case COLOUR_TYPE_XY_COORDINATE:
          pColourVar->xyCoordinate.report.xCoordinate =
            pColourPersVar->powerOnColourValue.xyCoordinateValue.xCoordinate;
          pColourVar->xyCoordinate.report.yCoordinate =
            pColourPersVar->powerOnColourValue.xyCoordinateValue.yCoordinate;
        break;

        case COLOUR_TYPE_COLOUR_TEMPERATURE:
          pColourVar->colourTemperature.reportColourTemperature =
            pColourPersVar->powerOnColourValue.ColourTemperature;
        break;

        case COLOUR_TYPE_PRIMARY_N:
          memcpy(pColourVar->primaryN.reportPrimeNdimlevel,
                 pColourPersVar->powerOnColourValue.PrimeNdimlevel,
                 sizeof(pColourVar->primaryN.reportPrimeNdimlevel));
        break;

        case COLOUR_TYPE_RGBWAF:
          pColourVar->rgbwaf.reportDimlevel =
          pColourPersVar->powerOnColourValue.rgbwafDimlevel;
        break;

        default:
          rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
        break;
      }
    break;

    case GEAR_CMD_QUERY_SYSTEM_FAILURE_LEVEL:
      pColourVar->reportColourType = pColourPersVar->SystemFailureColourType;
      switch (pColourPersVar->SystemFailureColourType)
      {
        case COLOUR_TYPE_XY_COORDINATE:
          pColourVar->xyCoordinate.report.xCoordinate =
            pColourPersVar->SystemFailureColourValue.xyCoordinateValue.xCoordinate;
          pColourVar->xyCoordinate.report.yCoordinate =
            pColourPersVar->SystemFailureColourValue.xyCoordinateValue.yCoordinate;
        break;

        case COLOUR_TYPE_COLOUR_TEMPERATURE:
          pColourVar->colourTemperature.reportColourTemperature =
            pColourPersVar->SystemFailureColourValue.ColourTemperature;
        break;

        case COLOUR_TYPE_PRIMARY_N:
          memcpy(pColourVar->primaryN.reportPrimeNdimlevel,
                 pColourPersVar->SystemFailureColourValue.PrimeNdimlevel,
                 sizeof(pColourVar->primaryN.reportPrimeNdimlevel));
        break;

        case COLOUR_TYPE_RGBWAF:
          pColourVar->rgbwaf.reportDimlevel =
          pColourPersVar->SystemFailureColourValue.rgbwafDimlevel;
        break;

        default:
          rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
        break;
      }
    break;

    case GEAR_CMD_QUERY_SCENE_LEVEL:
      sceneIdx = (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK) -
                 (*ppGearCmd)->opcodeByte;
      pColourVar->reportColourType = pColourPersVar->sceneColourType[sceneIdx];
      switch (pColourPersVar->sceneColourType[sceneIdx])
      {
        case COLOUR_TYPE_XY_COORDINATE:
          pColourVar->xyCoordinate.report.xCoordinate =
            pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.xCoordinate;
          pColourVar->xyCoordinate.report.yCoordinate =
            pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.yCoordinate;
        break;

        case COLOUR_TYPE_COLOUR_TEMPERATURE:
          pColourVar->colourTemperature.reportColourTemperature =
            pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature;
        break;

        case COLOUR_TYPE_PRIMARY_N:
          memcpy(pColourVar->primaryN.reportPrimeNdimlevel,
                 pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel,
                 sizeof(pColourVar->primaryN.reportPrimeNdimlevel));
        break;

        case COLOUR_TYPE_RGBWAF:
          pColourVar->rgbwaf.reportDimlevel =
            pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel;
        break;

        default:
          rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
        break;
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process Colour Control forward level frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_colour_process_forward_level(PCtrlGearInstance      pCtrlInst,
                                                   dalilib_frame_t*       pForwardFrame,
                                                   ctrl_gear_commands_t** ppGearCmd,
                                                   gear_adr_t*            pReceivedAdr,
                                                   uint8_t*               pRespValue)
{
  R_DALILIB_RESULT         rc             = R_IGNORE_FRAME;
  colour_pers_variables_t* pColourPersVar = NULL;
  colour_variables_t*      pColourVar     = NULL;
  uint8_t sceneIdx                        = 0;

  (void)pReceivedAdr;
  (void)pRespValue;

  pColourPersVar = &pCtrlInst->gearPersistent.colourPersVariables;
  pColourVar = &pCtrlInst->gearVariables.colourVariables;

  if( GEAR_CMD_GO_TO_SCENE)
  {
    sceneIdx = (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK) -
               (*ppGearCmd)->opcodeByte;
    pColourVar->tempColourType = pColourPersVar->sceneColourType[sceneIdx];
    switch (pColourPersVar->sceneColourType[sceneIdx])
    {
      case COLOUR_TYPE_XY_COORDINATE:
        pColourVar->xyCoordinate.temp.xCoordinate =
          pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.xCoordinate;
        pColourVar->xyCoordinate.temp.yCoordinate =
          pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.yCoordinate;
      break;

      case COLOUR_TYPE_COLOUR_TEMPERATURE:
        pColourVar->colourTemperature.tempColourTemperature =
          pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature;
      break;

      case COLOUR_TYPE_PRIMARY_N:
        memcpy(pColourVar->primaryN.tempPrimeNdimlevel,
               pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel,
               sizeof(pColourVar->primaryN.tempPrimeNdimlevel));
      break;

      case COLOUR_TYPE_RGBWAF:
        pColourVar->rgbwaf.tempDimlevel =
        pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel;
      break;

      default:
      break;
    }
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case GEAR_CMD_DAPC:
    case GEAR_CMD_OFF:
    case GEAR_CMD_UP:
    case GEAR_CMD_DOWN:
    case GEAR_CMD_STEP_UP:
    case GEAR_CMD_STEP_DOWN:
    case GEAR_CMD_RECALL_MAX_LEVEL:
    case GEAR_CMD_RECALL_MIN_LEVEL:
    case GEAR_CMD_STEP_DOWN_AND_OFF:
    case GEAR_CMD_ON_AND_STEP_UP:
    //case GEAR_CMD_ENABLE_DAPC_SEQUENCE:
    //case GEAR_CMD_GO_TO_LAST_ACTIVE_LEVEL:
    case GEAR_CMD_GO_TO_SCENE:
      if ((pColourPersVar->gearFeatureStatus & AUTOMATIC_ACTIVATION) &&
          pColourVar->tempColourType != DALI_MASK)
      {
        switch (pColourVar->tempColourType)
        {
          case COLOUR_TYPE_XY_COORDINATE:
            pColourVar->xyCoordinate.value = pColourVar->xyCoordinate.temp;

            // reset Bit 1,5,6,7 of colour status, set Bit 4
            pColourVar->colourStatus.temperatureOutOfRange = 0;
            pColourVar->colourStatus.temperatureActive     = 0;
            pColourVar->colourStatus.primaryNactive        = 0;
            pColourVar->colourStatus.RGBWAFactive          = 0;
            pColourVar->colourStatus.xyCoordinateActive    = 1;
          break;

          case COLOUR_TYPE_COLOUR_TEMPERATURE:
            // reset Bit 0,4,6,7 of colour status, set Bit 5
            pColourVar->colourTemperature.ColourTemperature =
              pColourVar->colourTemperature.tempColourTemperature;

            pColourVar->colourStatus.xyCoordinateOutOfRange = 0;
            pColourVar->colourStatus.xyCoordinateActive     = 0;
            pColourVar->colourStatus.primaryNactive         = 0;
            pColourVar->colourStatus.RGBWAFactive           = 0;
            pColourVar->colourStatus.temperatureActive      = 1;
          break;

          case COLOUR_TYPE_PRIMARY_N:
            memcpy(pColourVar->primaryN.primeNdimlevel,
                   pColourVar->primaryN.tempPrimeNdimlevel,
                   sizeof(pColourVar->primaryN.primeNdimlevel));

            // reset Bit 0,1,4,5,7 of colour status, set Bit 6
            pColourVar->colourStatus.xyCoordinateOutOfRange = 0;
            pColourVar->colourStatus.temperatureOutOfRange  = 0;
            pColourVar->colourStatus.xyCoordinateActive     = 0;
            pColourVar->colourStatus.temperatureActive      = 0;
            pColourVar->colourStatus.RGBWAFactive           = 0;
            pColourVar->colourStatus.primaryNactive         = 1;
          break;

          case COLOUR_TYPE_RGBWAF:
            // reset Bit 0,1,4,5,6 of colour status, set Bit 7
            pColourVar->colourStatus.xyCoordinateOutOfRange = 0;
            pColourVar->colourStatus.temperatureOutOfRange  = 0;
            pColourVar->colourStatus.xyCoordinateActive     = 0;
            pColourVar->colourStatus.temperatureActive      = 0;
            pColourVar->colourStatus.primaryNactive         = 0;
            pColourVar->colourStatus.RGBWAFactive           = 1;
          break;

          default:
          break;
        }
        gear_colour_reset_temp_values(pColourPersVar, pColourVar);
      }
    break;

    default:
    break;
  }
  return (rc);
}

//----------------------------------------------------------------------
// process Colour Control forward configuration frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_colour_process_forward_configuration(PCtrlGearInstance      pCtrlInst,
                                                           dalilib_frame_t*       pForwardFrame,
                                                           ctrl_gear_commands_t** ppGearCmd,
                                                           gear_adr_t*            pReceivedAdr,
                                                           uint8_t*               pRespValue)
{
  R_DALILIB_RESULT         rc             = R_IGNORE_FRAME;
  colour_pers_variables_t* pColourPersVar = NULL;
  colour_variables_t*      pColourVar     = NULL;
  uint8_t sceneIdx                        = 0;

  (void)pReceivedAdr;
  (void)pRespValue;

  pColourPersVar = &pCtrlInst->gearPersistent.colourPersVariables;
  pColourVar = &pCtrlInst->gearVariables.colourVariables;

  switch((*ppGearCmd)->eGearCmd)
  {
    // control gear level frames
    case GEAR_CMD_SET_SCENE:
      {
        sceneIdx = (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK) -
          (*ppGearCmd)->opcodeByte;
        switch (pColourVar->tempColourType)
        {
          case COLOUR_TYPE_XY_COORDINATE:
            if (pColourVar->tempColourType != pColourPersVar->sceneColourType[sceneIdx] ||
                0 != memcmp(&pColourVar->xyCoordinate.temp,
                            &pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue,
                            sizeof(pColourVar->xyCoordinate.temp)))
            {
              pColourPersVar->sceneColourType[sceneIdx] = pColourVar->tempColourType;
              pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue =
                pColourVar->xyCoordinate.temp;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_COLOUR_TEMPERATURE:
            if (pColourVar->tempColourType != pColourPersVar->sceneColourType[sceneIdx] ||
                pColourVar->colourTemperature.tempColourTemperature != pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature)
            {
              pColourPersVar->sceneColourType[sceneIdx] = pColourVar->tempColourType;
              pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature =
                pColourVar->colourTemperature.tempColourTemperature;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_PRIMARY_N:
            if (pColourVar->tempColourType != pColourPersVar->sceneColourType[sceneIdx] ||
                0 != memcmp(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel,
                                     pColourVar->primaryN.tempPrimeNdimlevel,
                                     sizeof(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel)))
            {
              pColourPersVar->sceneColourType[sceneIdx] = pColourVar->tempColourType;
              memcpy(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel,
                     pColourVar->primaryN.tempPrimeNdimlevel,
                     sizeof(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel));
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_RGBWAF:
            if (pColourVar->tempColourType != pColourPersVar->sceneColourType[sceneIdx] ||
                0 != memcmp(&pColourVar->rgbwaf.tempDimlevel,
                            &pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel,
                            sizeof(pColourVar->rgbwaf.tempDimlevel)))
            {
              pColourPersVar->sceneColourType[sceneIdx] = pColourVar->tempColourType;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel =
                pColourVar->rgbwaf.tempDimlevel;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          default:
          break;
        }
      }
    break;

    case GEAR_CMD_REMOVE_FROM_SCENE:
      {
        sceneIdx = (pForwardFrame->data & DALI_CTRL_GEAR_FF_VALUE_MASK) -
                   (*ppGearCmd)->opcodeByte;
        switch (pColourVar->tempColourType)
        {
          case COLOUR_TYPE_XY_COORDINATE:
            if (DALI_MASK != pColourPersVar->sceneColourType[sceneIdx] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.xCoordinate ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.yCoordinate)
            {
              pColourPersVar->sceneColourType[sceneIdx] = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.xCoordinate = DALI_MASK2;
              pColourPersVar->sceneColourValue[sceneIdx].xyCoordinateValue.yCoordinate = DALI_MASK2;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_COLOUR_TEMPERATURE:
            if (DALI_MASK != pColourPersVar->sceneColourType[sceneIdx] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature)
            {
              pColourPersVar->sceneColourType[sceneIdx] = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].ColourTemperature = DALI_MASK2;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_PRIMARY_N:
            if (DALI_MASK != pColourPersVar->sceneColourType[sceneIdx] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[0] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[1] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[2] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[3] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[4] ||
                DALI_MASK2 != pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel[5])
            {
              pColourPersVar->sceneColourType[sceneIdx] = DALI_MASK;
              memset(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel,
                     DALI_MASK2,
                     sizeof(pColourPersVar->sceneColourValue[sceneIdx].PrimeNdimlevel));
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_RGBWAF:
            if (DALI_MASK != pColourPersVar->sceneColourType[sceneIdx] ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.RGBWAFcontrol ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.red ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.green ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.blue ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.white ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.amber ||
                DALI_MASK != pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.free)
            {
              pColourPersVar->sceneColourType[sceneIdx] = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.RGBWAFcontrol = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.red   = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.green = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.blue  = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.white = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.amber = DALI_MASK;
              pColourPersVar->sceneColourValue[sceneIdx].rgbwafDimlevel.free  = DALI_MASK;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }

          break;

          default:
          break;
        }
      }
    break;

    case GEAR_CMD_SET_SYSTEM_FAILURE_LEVEL:
      {
        switch (pColourVar->tempColourType)
        {
          case COLOUR_TYPE_XY_COORDINATE:
            if (pColourVar->tempColourType != pColourPersVar->SystemFailureColourType ||
                0 != memcmp(&pColourVar->xyCoordinate.temp,
                            &pColourPersVar->SystemFailureColourValue.xyCoordinateValue,
                            sizeof(pColourVar->xyCoordinate.temp)))
            {
              pColourPersVar->SystemFailureColourType = pColourVar->tempColourType;
              pColourPersVar->SystemFailureColourValue.xyCoordinateValue =
                pColourVar->xyCoordinate.temp;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_COLOUR_TEMPERATURE:
            if (pColourVar->tempColourType != pColourPersVar->SystemFailureColourType ||
                pColourVar->colourTemperature.tempColourTemperature != pColourPersVar->SystemFailureColourValue.ColourTemperature)
            {
              pColourPersVar->SystemFailureColourType = pColourVar->tempColourType;
              pColourPersVar->SystemFailureColourValue.ColourTemperature =
                pColourVar->colourTemperature.tempColourTemperature;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_PRIMARY_N:
            if (pColourVar->tempColourType != pColourPersVar->SystemFailureColourType ||
                0 != memcmp(pColourPersVar->SystemFailureColourValue.PrimeNdimlevel,
                            pColourVar->primaryN.tempPrimeNdimlevel,
                            sizeof(pColourPersVar->SystemFailureColourValue.PrimeNdimlevel)))
            {
              pColourPersVar->SystemFailureColourType = pColourVar->tempColourType;
              memcpy(pColourPersVar->SystemFailureColourValue.PrimeNdimlevel,
                     pColourVar->primaryN.tempPrimeNdimlevel,
                     sizeof(pColourPersVar->SystemFailureColourValue.PrimeNdimlevel));
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_RGBWAF:
            if (pColourVar->tempColourType != pColourPersVar->SystemFailureColourType ||
                0 != memcmp(&pColourVar->rgbwaf.tempDimlevel,
                            &pColourPersVar->SystemFailureColourValue.rgbwafDimlevel,
                            sizeof(pColourVar->rgbwaf.tempDimlevel)))
            {
              pColourPersVar->SystemFailureColourType = pColourVar->tempColourType;
              pColourPersVar->SystemFailureColourValue.rgbwafDimlevel =
                pColourVar->rgbwaf.tempDimlevel;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          default:
          break;
        }
      }
    break;

    case GEAR_CMD_SET_POWER_ON_LEVEL:
      {
        switch (pColourVar->tempColourType)
        {
          case COLOUR_TYPE_XY_COORDINATE:
            if (pColourVar->tempColourType != pColourPersVar->powerOnColourType ||
                0 != memcmp(&pColourVar->xyCoordinate.temp,
                            &pColourPersVar->powerOnColourValue.xyCoordinateValue,
                            sizeof(pColourVar->xyCoordinate.temp)))
            {
              pColourPersVar->powerOnColourType = pColourVar->tempColourType;
              pColourPersVar->powerOnColourValue.xyCoordinateValue =
                pColourVar->xyCoordinate.temp;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_COLOUR_TEMPERATURE:
            if (pColourVar->tempColourType != pColourPersVar->powerOnColourType ||
                pColourVar->colourTemperature.tempColourTemperature != pColourPersVar->powerOnColourValue.ColourTemperature)
            {
              pColourPersVar->powerOnColourType = pColourVar->tempColourType;
              pColourPersVar->powerOnColourValue.ColourTemperature =
                pColourVar->colourTemperature.tempColourTemperature;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_PRIMARY_N:
            if (pColourVar->tempColourType != pColourPersVar->powerOnColourType ||
                0 != memcmp(pColourPersVar->powerOnColourValue.PrimeNdimlevel,
                            pColourVar->primaryN.tempPrimeNdimlevel,
                            sizeof(pColourPersVar->powerOnColourValue.PrimeNdimlevel)))
            {
              pColourPersVar->powerOnColourType = pColourVar->tempColourType;
              memcpy(pColourPersVar->powerOnColourValue.PrimeNdimlevel,
                     pColourVar->primaryN.tempPrimeNdimlevel,
                     sizeof(pColourPersVar->powerOnColourValue.PrimeNdimlevel));
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          case COLOUR_TYPE_RGBWAF:
            if (pColourVar->tempColourType != pColourPersVar->powerOnColourType ||
                0 != memcmp(&pColourVar->rgbwaf.tempDimlevel,
                            &pColourPersVar->powerOnColourValue.rgbwafDimlevel,
                            sizeof(pColourVar->rgbwaf.tempDimlevel)))
            {
              pColourPersVar->powerOnColourType = pColourVar->tempColourType;
              pColourPersVar->powerOnColourValue.rgbwafDimlevel =
                pColourVar->rgbwaf.tempDimlevel;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          break;

          default:
          break;
        }
      }
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

//----------------------------------------------------------------------
// process Colour Control forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_colour_process_forward(PCtrlGearInstance      pCtrlInst,
                                             dalilib_frame_t*       pForwardFrame,
                                             ctrl_gear_commands_t** ppGearCmd,
                                             gear_adr_t*            pReceivedAdr,
                                             uint8_t*               pRespValue)
{
  R_DALILIB_RESULT         rc             = R_DALILIB_OK;
  colour_pers_variables_t* pColourPersVar = NULL;
  colour_variables_t*      pColourVar     = NULL;

  (void)pReceivedAdr;

  pColourPersVar = &pCtrlInst->gearPersistent.colourPersVariables;
  pColourVar = &pCtrlInst->gearVariables.colourVariables;

  *ppGearCmd = colour_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case COLOUR_CMD_SET_TEMP_XCOORDINATE:
      if (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_XY)
      {
        // set temp x-Coordinate, DTR0 = LSB, DTR1 = MSB
        pColourVar->xyCoordinate.temp.xCoordinate = (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
        pColourVar->xyCoordinate.temp.xCoordinate  &= pCtrlInst->gearVariables.DTR0;
        pColourVar->tempColourType = COLOUR_TYPE_XY_COORDINATE;
      }
      else
      {
        if  (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_PRIMARY)
        {
          // set temp x-Coordinate, DTR0 = LSB, DTR1 = MSB
          pColourVar->xyCoordinate.temp.xCoordinate = (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
          pColourVar->xyCoordinate.temp.xCoordinate  &= pCtrlInst->gearVariables.DTR0;
          pColourVar->tempColourType = COLOUR_TYPE_PRIMARY_N;
        }
      }
    break;

    case COLOUR_CMD_SET_TEMP_YCOORDINATE:
      if (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_XY)
      {
        // set temp x-Coordinate, DTR0 = LSB, DTR1 = MSB
        pColourVar->xyCoordinate.temp.yCoordinate = (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
        pColourVar->xyCoordinate.temp.yCoordinate  &= pCtrlInst->gearVariables.DTR0;
        pColourVar->tempColourType = COLOUR_TYPE_XY_COORDINATE;
      }
      else
      {
        if  (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_PRIMARY)
        {
          // set temp x-Coordinate, DTR0 = LSB, DTR1 = MSB
          pColourVar->xyCoordinate.temp.yCoordinate = (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
          pColourVar->xyCoordinate.temp.yCoordinate  &= pCtrlInst->gearVariables.DTR0;
          pColourVar->tempColourType = COLOUR_TYPE_PRIMARY_N;
        }
      }
    break;

    case COLOUR_CMD_ACTIVATE:
      // TODO: activate
    break;

    case COLOUR_CMD_XCOORDINATE_STEP_UP:
      if (pColourVar->colourStatus.xyCoordinateActive)
      {
        if (pColourVar->xyCoordinate.value.xCoordinate <= COLOUR_MAX_VALUE-XY_COORDINATE_STEP)
        {
          pColourVar->xyCoordinate.value.xCoordinate += XY_COORDINATE_STEP;
        }
        else
        {
          pColourVar->xyCoordinate.value.xCoordinate = COLOUR_MAX_VALUE;
        }
      }
      pColourVar->colourStatus.xyCoordinateOutOfRange =
        PointInColourTriangel(pColourVar->xyCoordinate.value.xCoordinate,
                              pColourVar->xyCoordinate.value.yCoordinate,
                              pColourPersVar->xyCoordinate.colourSpaceValue);
    break;

    case COLOUR_CMD_XCOORDINATE_STEP_DOWN:
      if (pColourVar->colourStatus.xyCoordinateActive)
      {
        if (pColourVar->xyCoordinate.value.xCoordinate >= XY_COORDINATE_STEP)
        {
          pColourVar->xyCoordinate.value.xCoordinate -= XY_COORDINATE_STEP;
        }
        else
        {
          pColourVar->xyCoordinate.value.xCoordinate = 0;
        }
      }
      pColourVar->colourStatus.xyCoordinateOutOfRange =
        PointInColourTriangel(pColourVar->xyCoordinate.value.xCoordinate,
                              pColourVar->xyCoordinate.value.yCoordinate,
                              pColourPersVar->xyCoordinate.colourSpaceValue);
    break;

    case COLOUR_CMD_YCOORDINATE_STEP_UP:
      if (pColourVar->colourStatus.xyCoordinateActive)
      {
        if (pColourVar->xyCoordinate.value.yCoordinate <= COLOUR_MAX_VALUE-XY_COORDINATE_STEP)
        {
          pColourVar->xyCoordinate.value.yCoordinate += XY_COORDINATE_STEP;
        }
        else
        {
          pColourVar->xyCoordinate.value.yCoordinate = COLOUR_MAX_VALUE;
        }
      }
      pColourVar->colourStatus.xyCoordinateOutOfRange =
        PointInColourTriangel(pColourVar->xyCoordinate.value.xCoordinate,
                              pColourVar->xyCoordinate.value.yCoordinate,
                              pColourPersVar->xyCoordinate.colourSpaceValue);
    break;

    case COLOUR_CMD_YCOORDINATE_STEP_DOWN:
      if (pColourVar->colourStatus.xyCoordinateActive)
      {
        if (pColourVar->xyCoordinate.value.yCoordinate >= XY_COORDINATE_STEP)
        {
          pColourVar->xyCoordinate.value.yCoordinate -= XY_COORDINATE_STEP;
        }
        else
        {
          pColourVar->xyCoordinate.value.yCoordinate = 0;
        }
      }
      pColourVar->colourStatus.xyCoordinateOutOfRange =
        PointInColourTriangel(pColourVar->xyCoordinate.value.xCoordinate,
                              pColourVar->xyCoordinate.value.yCoordinate,
                              pColourPersVar->xyCoordinate.colourSpaceValue);
    break;

    case COLOUR_CMD_SET_TEMP_COLOUR_TEMPERATURE:
      if ((pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_TEMPERATURE) &&
          (0 < pCtrlInst->gearVariables.DTR0))
      {
        pColourVar->colourTemperature.tempColourTemperature  = (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
        pColourVar->colourTemperature.tempColourTemperature  &= pCtrlInst->gearVariables.DTR0;
        pColourVar->tempColourType = COLOUR_TYPE_COLOUR_TEMPERATURE;
      }
    break;

    case COLOUR_CMD_COLOUR_TEMPERATURE_STEP_COOLER:
      if ((pColourVar->colourStatus.temperatureActive) &&
          pColourPersVar->colourTemperature.ColourTemperatureCoolest <
          pColourVar->colourTemperature.ColourTemperature)
      {
        pColourVar->colourTemperature.ColourTemperature -= 1;
        pColourVar->colourStatus.temperatureOutOfRange = DALI_FALSE;
      }
      else 
      {
        if (pColourVar->colourStatus.temperatureActive)
        {
          pColourVar->colourStatus.temperatureOutOfRange = DALI_TRUE;
        }
      }
    break;

    case COLOUR_CMD_COLOUR_TEMPERATURE_STEP_WARMER:
      if ((pColourVar->colourStatus.temperatureActive) &&
          pColourPersVar->colourTemperature.ColourTemperatureWarmest >
          pColourVar->colourTemperature.ColourTemperature)
      {
        pColourVar->colourTemperature.ColourTemperature += 1;
        pColourVar->colourStatus.temperatureOutOfRange = DALI_FALSE;
      }
      else
      {
        if (pColourVar->colourStatus.temperatureActive)
        {
          pColourVar->colourStatus.temperatureOutOfRange = DALI_TRUE;
        }
      }
    break;

    case COLOUR_CMD_SET_TEMP_PRIMARY_N_DIMLEVEL:
      if ((pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_PRIMARY) &&
          (6 > pCtrlInst->gearVariables.DTR2))
      {
        pColourVar->primaryN.tempPrimeNdimlevel[pCtrlInst->gearVariables.DTR2] =
          (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
        pColourVar->primaryN.tempPrimeNdimlevel[pCtrlInst->gearVariables.DTR2] &=
          pCtrlInst->gearVariables.DTR0;
        pColourVar->tempColourType = COLOUR_TYPE_PRIMARY_N;
      }
    break;

    case COLOUR_CMD_SET_TEMP_RGB_DIMLEVEL:
      if (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_RGBWAF)
      {
        pColourVar->rgbwaf.tempDimlevel.red   = pCtrlInst->gearVariables.DTR0;
        pColourVar->rgbwaf.tempDimlevel.green = pCtrlInst->gearVariables.DTR1;
        pColourVar->rgbwaf.tempDimlevel.blue  = pCtrlInst->gearVariables.DTR2;
        pColourVar->tempColourType = COLOUR_TYPE_RGBWAF;
      }
    break;

    case COLOUR_CMD_SET_TEMP_WAF_DIMLVEL:
      if (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_RGBWAF)
      {
        pColourVar->rgbwaf.tempDimlevel.white = pCtrlInst->gearVariables.DTR0;
        pColourVar->rgbwaf.tempDimlevel.amber = pCtrlInst->gearVariables.DTR1;
        pColourVar->rgbwaf.tempDimlevel.free  = pCtrlInst->gearVariables.DTR2;
        pColourVar->tempColourType = COLOUR_TYPE_RGBWAF;
      }
    break;

    case COLOUR_CMD_SET_TEMP_RGBWAF_CONTROL:
      if (pColourPersVar->colourTypeFeatures & COLOUR_TYPE_FEATURES_RGBWAF)
      {
        pColourVar->rgbwaf.dimlevel.RGBWAFcontrol = pCtrlInst->gearVariables.DTR0;
        pColourVar->tempColourType = COLOUR_TYPE_RGBWAF;
      }
    break;

    case COLOUR_CMD_COPY_REPORT_TO_TEMP:
      if(pColourVar->colourStatus.xyCoordinateActive)
      {
        pColourVar->xyCoordinate.temp = pColourVar->xyCoordinate.report;
        break;
      } 
      if (pColourVar->colourStatus.temperatureActive)
      {
        pColourVar->colourTemperature.tempColourTemperature =
          pColourVar->colourTemperature.reportColourTemperature;
        break;
      }
      if (pColourVar->colourStatus.primaryNactive)
      {
        memcpy(pColourVar->primaryN.tempPrimeNdimlevel,
               pColourVar->primaryN.reportPrimeNdimlevel,
               sizeof(pColourVar->primaryN.primeNdimlevel));
        break;
      }
      if (pColourVar->colourStatus.RGBWAFactive)
      {
        pColourVar->rgbwaf.tempDimlevel = pColourVar->rgbwaf.reportDimlevel;
      }
    break;

    case COLOUR_CMD_STORE_TY_PRIMARY_N:
      if (6 > pCtrlInst->gearVariables.DTR2)
      {
        if (((uint16_t)pCtrlInst->gearVariables.DTR1 << 8) != pColourPersVar->primaryN.tyPrime[pCtrlInst->gearVariables.DTR2] ||
            pCtrlInst->gearVariables.DTR0 != (pColourPersVar->primaryN.tyPrime[pCtrlInst->gearVariables.DTR2] | pCtrlInst->gearVariables.DTR0))
        {
          pColourPersVar->primaryN.tyPrime[pCtrlInst->gearVariables.DTR2] =
            (uint16_t)pCtrlInst->gearVariables.DTR1 << 8;
          pColourPersVar->primaryN.tyPrime[pCtrlInst->gearVariables.DTR2] &=
            pCtrlInst->gearVariables.DTR0;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case COLOUR_CMD_STORE_XY_COORDINATE_PRIMARY_N:
      if (6 > pCtrlInst->gearVariables.DTR2)
      {
        if (pColourVar->primaryN.temp.xCoordinate != pColourPersVar->primaryN.xCoordinatePrime[pCtrlInst->gearVariables.DTR2] ||
            pColourVar->primaryN.temp.yCoordinate != pColourPersVar->primaryN.yCoordinatePrime[pCtrlInst->gearVariables.DTR2])
        {
          pColourPersVar->primaryN.xCoordinatePrime[pCtrlInst->gearVariables.DTR2] =
            pColourVar->primaryN.temp.xCoordinate;
          pColourPersVar->primaryN.yCoordinatePrime[pCtrlInst->gearVariables.DTR2] =
            pColourVar->primaryN.temp.yCoordinate;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case COLOUR_CMD_STORE_COLOUR_TEMPERATURE_LIMIT:
      switch (pCtrlInst->gearVariables.DTR2)
      {
        // Colour Temperature Coolest
        case 0:
          if ((pCtrlInst->gearVariables.DTR0 | (pCtrlInst->gearVariables.DTR1 << 8)) != pColourPersVar->colourTemperature.ColourTemperatureCoolest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureCoolest =
              pCtrlInst->gearVariables.DTR0;
            pColourPersVar->colourTemperature.ColourTemperatureCoolest |=
              pCtrlInst->gearVariables.DTR1 << 8;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }

          if (pColourPersVar->colourTemperature.ColourTemperatureCoolest <
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureCoolest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest;
          }
        break;

        // Colour Temperature Warmest
        case 1:
          if ((pCtrlInst->gearVariables.DTR0 | (pCtrlInst->gearVariables.DTR1 << 8)) != pColourPersVar->colourTemperature.ColourTemperatureWarmest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureWarmest =
              pCtrlInst->gearVariables.DTR0;
            pColourPersVar->colourTemperature.ColourTemperatureWarmest |=
              pCtrlInst->gearVariables.DTR1 << 8;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }

          if (pColourPersVar->colourTemperature.ColourTemperatureWarmest >
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureWarmest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest;
          }
        break;

        // Colour Temperature Physical Coolest
        case 2:
          if ((pCtrlInst->gearVariables.DTR0 | (pCtrlInst->gearVariables.DTR1 << 8)) != pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest)
          {
            pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest =
              pCtrlInst->gearVariables.DTR1 << 8;
            pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest |=
              pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }

          if (pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest >
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest)
          {
            pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest;
          }

          if (pColourPersVar->colourTemperature.ColourTemperatureCoolest <
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureCoolest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest;
          }
        break;

        // Colour Temperature Physical Warmest
        case 3:
          if ((pCtrlInst->gearVariables.DTR0 | (pCtrlInst->gearVariables.DTR1 << 8)) != pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest)
          {
            pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest =
              pCtrlInst->gearVariables.DTR1 << 8;
            pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest |=
              pCtrlInst->gearVariables.DTR0;
            pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
            if (0 == pCtrlInst->gearPersistentChangedTime)
            {
              pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
            }
          }

          if (pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest <
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest)
          {
            pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest;
          }

          if (pColourPersVar->colourTemperature.ColourTemperatureWarmest >
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest)
          {
            pColourPersVar->colourTemperature.ColourTemperatureWarmest =
              pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest;
          }
        break;

        default:
        break;
      }
    break;

    case COLOUR_CMD_STORE_GEAR_FEATURES_STATUS:
      if (pCtrlInst->gearVariables.DTR0 & AUTOMATIC_ACTIVATION)
      {
        if (AUTOMATIC_ACTIVATION != (pColourPersVar->gearFeatureStatus | AUTOMATIC_ACTIVATION))
        {
          pColourPersVar->gearFeatureStatus &= AUTOMATIC_ACTIVATION;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
      else
      {
        // reset Bit 0 (Automatic Activation)
        if (0xFE != (pColourPersVar->gearFeatureStatus | 0xFE))
        {
          pColourPersVar->gearFeatureStatus &= 0xFE;
          pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
          if (0 == pCtrlInst->gearPersistentChangedTime)
          {
            pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
          }
        }
      }
    break;

    case COLOUR_CMD_ASSIGN_COLOUR_TO_LINKED_CHANNEL:
      if (6 >= pCtrlInst->gearVariables.DTR0)
      {
        for(int i = 0; i < 6; i++)
        {
          if (0x01 & (pColourVar->rgbwaf.tempDimlevel.RGBWAFcontrol >> i))
          {
            if (pCtrlInst->gearVariables.DTR0 != pColourPersVar->assignedColour[i])
            {
              pColourPersVar->assignedColour[i] = pCtrlInst->gearVariables.DTR0;
              pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_COLOUR_MASK;
              if (0 == pCtrlInst->gearPersistentChangedTime)
              {
                pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
              }
            }
          }
        }
      }
    break;

    case COLOUR_CMD_START_AUTO_CALIBRATION:
      // TODO: auto Calibration
      if (pColourPersVar->gearFeatureStatus & AUTOMATIC_CALIBRATION)
      {
        pColourVar->calibrationStartTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        pColourVar->colourStatus.calibrationRunning = DALI_TRUE;
        pColourVar->colourStatus.calibrationsucceeded = DALI_FALSE;

        dalilib_action_t action;

        memset( &action, 0, sizeof(action));

        action.actionType = DALILIB_CTRL_GEAR_REACTION;
        action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_COLOUR_CALIBRATION_START;
        dali_cb_reaction(pCtrlInst->pInst, &action);
      }
    break;

    case COLOUR_CMD_QUERY_GEAR_FEATURES_STATUS:
      *pRespValue = pColourPersVar->gearFeatureStatus;
    break;

    case COLOUR_CMD_QUERY_COLOUR_STATUS:
      *pRespValue |= pColourVar->colourStatus.xyCoordinateOutOfRange;
      *pRespValue |= pColourVar->colourStatus.temperatureOutOfRange  <<1;
      *pRespValue |= pColourVar->colourStatus.calibrationRunning     <<2;
      *pRespValue |= pColourVar->colourStatus.calibrationsucceeded   <<3;
      *pRespValue |= pColourVar->colourStatus.xyCoordinateActive     <<4;
      *pRespValue |= pColourVar->colourStatus.temperatureActive      <<5;
      *pRespValue |= pColourVar->colourStatus.primaryNactive         <<6;
      *pRespValue |= pColourVar->colourStatus.RGBWAFactive           <<7;
    break;

    case COLOUR_CMD_QUERY_COLOUR_TYPE_FEAUTRES:
      *pRespValue = pColourPersVar->colourTypeFeatures;
    break;

    case COLOUR_CMD_QUERY_COLOUR_VALUE:
      switch (pCtrlInst->gearVariables.DTR0)
      {
        case 0:
          if (pColourVar->colourStatus.xyCoordinateActive)
          {
            *pRespValue = pColourVar->xyCoordinate.value.xCoordinate;
            pCtrlInst->gearVariables.DTR0 = pColourVar->xyCoordinate.value.xCoordinate >>8;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 1:
          if (pColourVar->colourStatus.xyCoordinateActive)
          {
            *pRespValue = pColourVar->xyCoordinate.value.yCoordinate;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 2:
          if (pColourVar->colourStatus.temperatureActive)
          {
            *pRespValue = pColourVar->colourTemperature.ColourTemperature;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 3:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[0];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 4:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[1];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 5:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[2];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 6:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[3];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 7:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[4];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 8:
          if (pColourVar->colourStatus.primaryNactive)
          {
            *pRespValue = pColourVar->primaryN.primeNdimlevel[5];
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 9:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.red;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 10:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.green;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 11:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.blue;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 12:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.white;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 13:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.amber;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 14:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.free;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 15:
          if (pColourVar->colourStatus.RGBWAFactive)
          {
            *pRespValue = pColourVar->rgbwaf.dimlevel.RGBWAFcontrol;
          }
          else
          {
            *pRespValue = DALI_MASK;
          }
        break;

        case 64:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[0];
        break;

        case 65:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[0];
        break;

        case 66:
          *pRespValue = pColourPersVar->primaryN.tyPrime[0];
        break;

        case 67:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[1];
        break;

        case 68:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[1];
        break;

        case 69:
          *pRespValue = pColourPersVar->primaryN.tyPrime[1];
        break;

        case 70:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[2];
        break;

        case 71:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[2];
        break;

        case 72:
          *pRespValue = pColourPersVar->primaryN.tyPrime[2];
        break;

        case 73:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[3];
        break;

        case 74:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[3];
        break;

        case 75:
          *pRespValue = pColourPersVar->primaryN.tyPrime[3];
        break;

        case 76:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[4];
        break;

        case 77:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[4];
        break;

        case 78:
          *pRespValue = pColourPersVar->primaryN.tyPrime[4];
        break;

        case 79:
          *pRespValue = pColourPersVar->primaryN.xCoordinatePrime[5];
        break;

        case 80:
          *pRespValue = pColourPersVar->primaryN.yCoordinatePrime[5];
        break;

        case 81:
          *pRespValue = pColourPersVar->primaryN.tyPrime[5];
        break;

        case 82:
          *pRespValue = DALI_MASK; // Number of Primaries?
        break;

        case 128:
          *pRespValue = pColourPersVar->colourTemperature.ColourTemperatureCoolest >> 8;
          pCtrlInst->gearVariables.DTR0 = pColourPersVar->colourTemperature.ColourTemperatureCoolest;
        break;

        case 129:
          *pRespValue = pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest >> 8;
          pCtrlInst->gearVariables.DTR0 = pColourPersVar->colourTemperature.ColourTemperaturePhyCoolest;
        break;

        case 130:
          *pRespValue = pColourPersVar->colourTemperature.ColourTemperatureWarmest >> 8;
          pCtrlInst->gearVariables.DTR0 = pColourPersVar->colourTemperature.ColourTemperatureWarmest;
        break;

        case 131:
          *pRespValue = pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest >> 8;
          pCtrlInst->gearVariables.DTR0 = pColourPersVar->colourTemperature.ColourTemperaturePhyWarmest;
        break;

        case 192:
          *pRespValue = pColourVar->xyCoordinate.temp.xCoordinate;
        break;

        case 193:
          *pRespValue = pColourVar->xyCoordinate.temp.yCoordinate;
        break;

        case 194:
          *pRespValue = pColourVar->colourTemperature.tempColourTemperature;
        break;

        case 195:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[0];
        break;

        case 196:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[1];
        break;

        case 197:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[2];
        break;

        case 198:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[3];
        break;

        case 199:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[4];
        break;

        case 200:
          *pRespValue = pColourVar->primaryN.tempPrimeNdimlevel[5];
        break;

        case 201:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.red;
        break;

        case 202:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.green;
        break;

        case 203:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.blue;
        break;

        case 204:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.white;
        break;

        case 205:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.amber;
        break;

        case 206:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.free;
        break;

        case 207:
          *pRespValue = pColourVar->rgbwaf.tempDimlevel.RGBWAFcontrol;
        break;

        case 208:
          *pRespValue = pColourVar->tempColourType;
        break;

        case 224:
          *pRespValue = pColourVar->xyCoordinate.report.xCoordinate;
        break;

        case 225:
          *pRespValue = pColourVar->xyCoordinate.report.yCoordinate;
        break;

        case 226:
          *pRespValue = pColourVar->colourTemperature.reportColourTemperature;
        break;

        case 227:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[0];
        break;

        case 228:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[1];
        break;

        case 229:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[2];
        break;

        case 230:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[3];
        break;

        case 231:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[4];
        break;

        case 232:
          *pRespValue = pColourVar->primaryN.reportPrimeNdimlevel[5];
        break;

        case 233:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.red;
        break;

        case 234:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.green;
        break;

        case 235:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.blue;
        break;

        case 236:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.white;
        break;

        case 237:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.amber;
        break;

        case 238:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.free;
        break;

        case 239:
          *pRespValue = pColourVar->rgbwaf.reportDimlevel.RGBWAFcontrol;
        break;

        case 240:
          *pRespValue = pColourVar->reportColourType;
        break;

        default:
        break;
      }
    break;

    case COLOUR_CMD_QUERY_RGBWAF_CONTROL:
      *pRespValue = pColourVar->rgbwaf.dimlevel.RGBWAFcontrol;
    break;

    case COLOUR_CMD_QUERY_ASSIGNED_COLOUR:
      switch (pCtrlInst->gearVariables.DTR0)
      {
        // channel 0
        case 0x01:
          *pRespValue = pColourPersVar->assignedColour[0];
        break;

        // channel 1
        case 0x02:
          *pRespValue = pColourPersVar->assignedColour[1];
        break;

        // channel 2
        case 0x04:
          *pRespValue = pColourPersVar->assignedColour[2];
        break;

        // channel 3
        case 0x08:
          *pRespValue = pColourPersVar->assignedColour[3];
        break;

        // channel 4
        case 0x10:
          *pRespValue = pColourPersVar->assignedColour[4];
        break;

        // channel 5
        case 0x20:
          *pRespValue = pColourPersVar->assignedColour[5];
        break;

        default:
          *pRespValue = DALI_MASK;
        break;
      }
    break;

    case COLOUR_CMD_QUERY_EXTENTED_VERSION_NUMBER:
      *pRespValue = 0x02;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }
  return (rc);
}

ctrl_gear_commands_t* gear_colour_get_cmd(COLOUR_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(colour_commands_m); idx++)
  {
    if ((COLOUR_CMD)colour_commands_m[idx].eGearCmd == eCmd)
    {
      return (&colour_commands_m[idx]);
    }
  }

  return (NULL);
}

