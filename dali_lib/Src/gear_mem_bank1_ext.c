/*M>---------------------------------------------------------------------------
 * Project:       DALI-Stack
 * Description:   DALI-Stack for control gear and control devices
 *
 * Copyright (c) by mbs GmbH, Krefeld, info@mbs-software.de
 * All rights reserved.
 *-------------------------------- implementation -----------------------------
 *
 * gear_mem_bank1_ext.c
 *
 --------------------------------------------------------------------------<M*/

#include "dalistack.h"

#ifdef DALILIB_GEAR_MEM_BANK1_EXT_PRESENT


// eGearMembank1ExtCmd,                        selectorBit, opcodeByte,  a, t}
static ctrl_gear_commands_t mem_bank1_ext_commands_m[] =
{
  {(GEAR_CMD) MEM_BANK1_EXT_QUERY_EXTENDED_VERSION_NUMBER,   1,       0xFF,  1, 0},
  {(GEAR_CMD) MEM_BANK1_EXT_CMD_LAST,                        0,       0x00,  0, 0}
};

//----------------------------------------------------------------------
// search memory bank 1 extension command by opcode byte
// return: success: pointer of the Item
//         error: NULL
//----------------------------------------------------------------------
static ctrl_gear_commands_t* mem_bank1_ext_get_cmd(uint8_t opcodeByte)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(mem_bank1_ext_commands_m); idx++)
  {
    if (mem_bank1_ext_commands_m[idx].opcodeByte == opcodeByte)
    {
      return (&mem_bank1_ext_commands_m[idx]);
    }
  }

  return (NULL);
}

//----------------------------------------------------------------------
// initialize the memory after external power cycle
//----------------------------------------------------------------------
void gear_mem_bank1_ext_init_after_power_cycle(PCtrlGearInstance pCtrlInst)
{
  // init variables
  pCtrlInst->gearVariables.memBank1ExtVariables.extendedVersionNumber = CTRL_GEAR_POWER_SUPPLY_EXTENDED_VERSION_NUMBER;
  pCtrlInst->gearVariables.memBank1ExtVariables.deviceType = GEAR_TYPE_CONTROL_GEAR_POWER_SUPPLY;

  // initialize memory bank 1 RAM memory cells
  pCtrlInst->gearMemBank1RAM.lockByte = DALI_MASK;
}


//----------------------------------------------------------------------
// initialize the default values of persistent memory
//----------------------------------------------------------------------
void gear_mem_bank1_ext_init_persistent(PDaliLibInstance              pInst,
                                        dali_ctrl_gear_pers_mem_t*    pGearPersistent)
{
  ctrl_gear_mem_bank1_NVM_t*      pMembank = NULL;

  pMembank = &pGearPersistent->gearMemBank1NVM.ctrl;

  // initialize memory bank 1 NVM memory cells
  memcpy(pMembank->gtin,
         pInst->config.vendor.gtin,
         sizeof(pMembank->gtin));
  memcpy(pMembank->identify_number,
         pInst->config.vendor.identify_number,
         sizeof(pMembank->identify_number));
  pMembank->content_format_id[0] = CTRL_GEAR_MEMORY_BANK_1_CONTENT_FORMAT_ID0;
  pMembank->content_format_id[1] = CTRL_GEAR_MEMORY_BANK_1_CONTENT_FORMAT_ID1;
  pMembank->year_manufactured = DALI_MASK;
  pMembank->week_manufactured = DALI_MASK;
  pMembank->nom_input_power[0] = DALI_MASK;
  pMembank->nom_input_power[1] = DALI_MASK;
  pMembank->min_dim_level_power[0] = DALI_MASK;
  pMembank->min_dim_level_power[1] = DALI_MASK;
  pMembank->nom_min_ac_voltage[0] = DALI_MASK;
  pMembank->nom_min_ac_voltage[1] = DALI_MASK;
  pMembank->nom_max_ac_voltage[0] = DALI_MASK;
  pMembank->nom_max_ac_voltage[1] = DALI_MASK;
  pMembank->nom_light_output[0] = DALI_MASK;
  pMembank->nom_light_output[1] = DALI_MASK;
  pMembank->nom_light_output[2] = DALI_MASK;
  pMembank->cri = DALI_MASK;
  pMembank->cct[0] = DALI_MASK;
  pMembank->cct[1] = DALI_MASK;
  pMembank->light_distribution_type = DALI_MASK;
  memset(pMembank->luminaire_color, 0x00, sizeof(pMembank->luminaire_color));
  memset(pMembank->identify, 0x00, sizeof(pMembank->identify));
}

//----------------------------------------------------------------------
// reset power supply memory bank
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_mem_bank1_ext_reset_memory_bank(PCtrlGearInstance pCtrlInst,
                                                      uint8_t           nMembank)
{
  // check whether to reset memory bank 1
  if ((CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER == nMembank || 0 == nMembank) &&
      DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK == pCtrlInst->gearMemBank1RAM.lockByte)
  {
    // reset memory cells
    pCtrlInst->gearMemBank1RAM.lockByte = DALI_MASK;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// read memory bank 1
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_mem_bank1_ext_read_memory_bank1(PCtrlGearInstance     pCtrlInst,
                                                      uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;

  if (pCtrlInst->gearVariables.DTR0 <= CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL)
  {
    switch (pCtrlInst->gearVariables.DTR0)
    {
      case 0x00: // Address of last addressable memory location
        *pRespValue = CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL;
      break;

      case 0x01: // Indicator byte manufacturer specific
        // not implemented
        rc = R_IGNORE_FRAME;
      break;

      case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE: // Lock byte
        *pRespValue = pCtrlInst->gearMemBank1RAM.lockByte;
      break;

      default:
        // all cells > DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE are stored in non-volatile memory
        *pRespValue = pCtrlInst->gearPersistent.gearMemBank1NVM.data[pCtrlInst->gearVariables.DTR0 - 3];
      break;
    }
  }
  else
  {
    rc = R_IGNORE_FRAME;
  }

  return (rc);
}

//----------------------------------------------------------------------
// direct write memory bank 1
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_mem_bank1_ext_write_memory_bank1_direct(PCtrlGearInstance  pCtrlInst,
                                                              uint8_t            nCell,
                                                              uint8_t            reqValue)
{
  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL < nCell)
  {
    return (R_IGNORE_FRAME);
  }

  switch(nCell)
  {
    case 0x00: // Address of last addressable memory location
    case 0x01: // Indicator byte manufacturer specific
      // not writable
      return (R_IGNORE_FRAME);
    break;

    case DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE:
      pCtrlInst->gearMemBank1RAM.lockByte = reqValue;
    break;

    default:
      // The first 3 cells of memory bank 1 are stored in volatile memory
      if (reqValue != pCtrlInst->gearPersistent.gearMemBank1NVM.data[nCell - 3])
      {
        pCtrlInst->gearPersistent.gearMemBank1NVM.data[nCell - 3] = reqValue;
        pCtrlInst->gearPersistentChangedFlags |= DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK;
        if (0 == pCtrlInst->gearPersistentChangedTime)
        {
          pCtrlInst->gearPersistentChangedTime = ((PDaliLibInstance)pCtrlInst->pInst)->currentTime;
        }
      }
    break;
  }

  return (R_DALILIB_OK);
}

//----------------------------------------------------------------------
// write memory bank 1
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_mem_bank1_ext_write_memory_bank1(PCtrlGearInstance     pCtrlInst,
                                                       ctrl_gear_commands_t* pGearCmd,
                                                       uint8_t               reqValue,
                                                       uint8_t*              pRespValue)
{
  R_DALILIB_RESULT  rc = R_DALILIB_OK;
  uint8_t           nCell;
  dalilib_action_t  action;

  nCell = pCtrlInst->gearVariables.DTR0;

  // check cell offset
  if (CTRL_GEAR_MEMORY_BANK_1_ADDRESS_LAST_CELL < nCell)
  {
    rc = R_IGNORE_FRAME;
  }

  // check whether memory bank is unlocked
  if ((R_DALILIB_OK == rc) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE != nCell) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_UNLOCK_MASK != pCtrlInst->gearMemBank1RAM.lockByte))
  {
    rc = R_IGNORE_FRAME;
  }

  // check value change
  if ((R_DALILIB_OK == rc) &&
      (DALI_CTRL_GEAR_MEMORY_BANK_LOCK_BYTE < nCell) &&
      (pCtrlInst->gearPersistent.gearMemBank1NVM.data[nCell - 3] != reqValue))
  {
    // notify application
    memset(&action, 0, sizeof(action));

    action.actionType = DALILIB_CTRL_GEAR_REACTION;
    action.gearReact.reactionCode = DALILIB_REACT_INTERNAL_CTRL_GEAR_MEMORY_CELL_CHANGED;
    action.gearReact.memoryValue.memoryBankNr = CTRL_GEAR_MEM_BANK1_EXT_MEMORY_BANK_NUMBER;
    action.gearReact.memoryValue.memoryCellIdx = nCell;
    action.gearReact.memoryValue.value = reqValue;
    action.gearReact.valueType = DALILIB_RESPONSE_VALUE_VALID;

    dali_cb_reaction(pCtrlInst->pInst, &action);
  }

  // write memory bank cell
  if (R_DALILIB_OK == rc)
  {
    rc = gear_mem_bank1_ext_write_memory_bank1_direct(pCtrlInst, nCell, reqValue);
  }

  // check whether to reply
  if ((R_DALILIB_OK == rc) &&
      (GEAR_CMD_WRITE_MEMORY_LOCATION_NO_REPLY != pGearCmd->eGearCmd))
  {
    *pRespValue = reqValue;
  }

  return (rc);
}

R_DALILIB_RESULT gear_mem_bank1_ext_save_mem_block_pers_vars(PDaliLibInstance           pInst,
                                                             dali_ctrl_gear_pers_mem_t* pGearPersistent)
{
  R_DALILIB_RESULT                                    rc = R_DALILIB_OK;
  dali_ctrl_gear_mem_bank1_ext_pers_variables_block_t block;

  // populate meta data
  block.id      = DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_ID;
  block.version = DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_VERSION;

  // populate memory block
  block.year_manufactured       = pGearPersistent->gearMemBank1NVM.ctrl.year_manufactured;
  block.week_manufactured       = pGearPersistent->gearMemBank1NVM.ctrl.week_manufactured;
  block.cri                     = pGearPersistent->gearMemBank1NVM.ctrl.cri;
  block.light_distribution_type = pGearPersistent->gearMemBank1NVM.ctrl.light_distribution_type;
  memcpy(block.gtin,                pGearPersistent->gearMemBank1NVM.ctrl.gtin,                sizeof(block.gtin));
  memcpy(block.identify_number,     pGearPersistent->gearMemBank1NVM.ctrl.identify_number,     sizeof(block.identify_number));
  memcpy(block.content_format_id,   pGearPersistent->gearMemBank1NVM.ctrl.content_format_id,   sizeof(block.content_format_id));
  memcpy(block.nom_input_power,     pGearPersistent->gearMemBank1NVM.ctrl.nom_input_power,     sizeof(block.nom_input_power));
  memcpy(block.min_dim_level_power, pGearPersistent->gearMemBank1NVM.ctrl.min_dim_level_power, sizeof(block.min_dim_level_power));
  memcpy(block.nom_min_ac_voltage,  pGearPersistent->gearMemBank1NVM.ctrl.nom_min_ac_voltage,  sizeof(block.nom_min_ac_voltage));
  memcpy(block.nom_max_ac_voltage,  pGearPersistent->gearMemBank1NVM.ctrl.nom_max_ac_voltage,  sizeof(block.nom_max_ac_voltage));
  memcpy(block.nom_light_output,    pGearPersistent->gearMemBank1NVM.ctrl.nom_light_output,    sizeof(block.nom_light_output));
  memcpy(block.cct,                 pGearPersistent->gearMemBank1NVM.ctrl.cct,                 sizeof(block.cct));
  memcpy(block.luminaire_color,     pGearPersistent->gearMemBank1NVM.ctrl.luminaire_color,     sizeof(block.luminaire_color));
  memcpy(block.identify,            pGearPersistent->gearMemBank1NVM.ctrl.identify,            sizeof(block.identify));

  // save memory block
  rc = dali_cb_save_mem_block(pInst,
                              &block,
                              sizeof(block),
                              DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_ID);
  if (R_DALILIB_OK == rc)
  {
    pInst->ctrlInst.gearInstance.gearPersistentChangedFlags &= ~DALI_CTRL_GEAR_PERS_MEM_BANK1_EXT_MASK;
  }

  return (rc);
}

R_DALILIB_RESULT gear_mem_bank1_ext_apply_mem_block_pers_vars(PCtrlGearInstance pCtrlInst,
                                                              uint8_t*          pDaliMem)
{
  R_DALILIB_RESULT                                     rc = R_DALILIB_OK;
  char                                                 buf[128];
  ctrl_gear_mem_bank1_NVM_t*                           pPersistentVariables = &pCtrlInst->gearPersistent.gearMemBank1NVM.ctrl;
  dali_ctrl_gear_mem_bank1_ext_pers_variables_block_t *pBlock = (dali_ctrl_gear_mem_bank1_ext_pers_variables_block_t*)pDaliMem;

  // copy mem block
  pPersistentVariables->year_manufactured       = pBlock->year_manufactured;
  pPersistentVariables->week_manufactured       = pBlock->week_manufactured;
  pPersistentVariables->cri                     = pBlock->cri;
  pPersistentVariables->light_distribution_type = pBlock->light_distribution_type;
  memcpy(pPersistentVariables->gtin,                pBlock->gtin,                sizeof(pPersistentVariables->gtin));
  memcpy(pPersistentVariables->identify_number,     pBlock->identify_number,     sizeof(pPersistentVariables->identify_number));
  memcpy(pPersistentVariables->content_format_id,   pBlock->content_format_id,   sizeof(pPersistentVariables->content_format_id));
  memcpy(pPersistentVariables->nom_input_power,     pBlock->nom_input_power,     sizeof(pPersistentVariables->nom_input_power));
  memcpy(pPersistentVariables->min_dim_level_power, pBlock->min_dim_level_power, sizeof(pPersistentVariables->min_dim_level_power));
  memcpy(pPersistentVariables->nom_min_ac_voltage,  pBlock->nom_min_ac_voltage,  sizeof(pPersistentVariables->nom_min_ac_voltage));
  memcpy(pPersistentVariables->nom_max_ac_voltage,  pBlock->nom_max_ac_voltage,  sizeof(pPersistentVariables->nom_max_ac_voltage));
  memcpy(pPersistentVariables->cct,                 pBlock->cct,                 sizeof(pPersistentVariables->cct));
  memcpy(pPersistentVariables->luminaire_color,     pBlock->luminaire_color,     sizeof(pPersistentVariables->luminaire_color));
  memcpy(pPersistentVariables->identify,            pBlock->identify,            sizeof(pPersistentVariables->identify));

  // check value range
  if (5          < pPersistentVariables->light_distribution_type &&
      DALI_MASK  > pPersistentVariables->light_distribution_type)
  {
    snprintf(buf, sizeof(buf),
             "dali library variable light_distribution_type is out of bounds. Value: %u Range: [0, minLevel..maxLevel]",
             pPersistentVariables->light_distribution_type);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->light_distribution_type = 0xFE;
  }
  if (0x03 != pPersistentVariables->content_format_id[0] ||
      0x00 != pPersistentVariables->content_format_id[1])
  {
    snprintf(buf, sizeof(buf),
             "dali library variable content_format_id is out of bounds. Value: %0x%02X02X Range: [0x0003]",
             pPersistentVariables->content_format_id[1],
             pPersistentVariables->content_format_id[0]);
    dali_cb_log(pCtrlInst->pInst, R_DALILIB_VARIABLE_OUT_OF_BOUNDS, buf);
    // set default value
    pPersistentVariables->content_format_id[0] = 0x03;
    pPersistentVariables->content_format_id[1] = 0x00;
  }

  return (rc);
}


//----------------------------------------------------------------------
// process the power supply forward frames
//----------------------------------------------------------------------
R_DALILIB_RESULT gear_mem_bank1_ext_process_forward(PCtrlGearInstance      pCtrlInst,
                                                    dalilib_frame_t*       pForwardFrame,
                                                    ctrl_gear_commands_t** ppGearCmd,
                                                    gear_adr_t*            pReceivedAdr,
                                                    uint8_t*               pRespValue)
{
  R_DALILIB_RESULT      rc          = R_DALILIB_OK;

  (void)pReceivedAdr;

  *ppGearCmd = mem_bank1_ext_get_cmd(pForwardFrame->data & DALI_CTRL_GEAR_FF_OPCODE_BYTE);

  if (NULL == *ppGearCmd)
  {
    return (R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED);
  }

  // is twice-Frame
  if (R_IGNORE_FRAME == ctrl_gear_check_twice(pCtrlInst->pInst, pForwardFrame, *ppGearCmd))
  {
    return (R_DALILIB_OK);
  }

  switch((*ppGearCmd)->eGearCmd)
  {
    case POWER_SUPPLY_QUERY_EXTENDED_VERSION_NUMBER:
      *pRespValue = pCtrlInst->gearVariables.memBank1ExtVariables.extendedVersionNumber;
    break;

    default:
      rc = R_DALILIB_CTRL_GEAR_CMD_NOT_SUPPORTED;
    break;
  }

  return (rc);
}

ctrl_gear_commands_t* gear_mem_bank1_ext_get_cmd(MEM_BANK1_EXT_CMD eCmd)
{
  uint8_t idx = 0;

  for (idx = 0; idx < COUNTOF(mem_bank1_ext_commands_m); idx++)
  {
    if ((MEM_BANK1_EXT_CMD)mem_bank1_ext_commands_m[idx].eGearCmd == eCmd)
    {
      return (&mem_bank1_ext_commands_m[idx]);
    }
  }

  return (NULL);
}

#endif /* DALILIB_GEAR_MEM_BANK1_EXT_PRESENT */
